<?php
/**
 * @file
 * Autoloader for Drupal Bootstrap.
 */

// @codingStandardsIgnoreFile
$autoloadFile = __DIR__ . '/../vendor/autoload.php';
if (!file_exists($autoloadFile)) {
  throw new RuntimeException('Install dependencies to run phpunit.');
}
require_once $autoloadFile;

$loader = new \Composer\Autoload\ClassLoader();
$loader->addPsr4('Drupal\\dss_magdalena\\Test\\', 'docroot/profiles/dss_magdalena/modules/dss_magdalena/dss/test');
$loader->register();

$_SERVER['REMOTE_ADDR'] = '127.0.0.1';
$_SERVER['REQUEST_METHOD'] = 'GET';

define('DRUPAL_ROOT', getcwd() . '/docroot');

require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
