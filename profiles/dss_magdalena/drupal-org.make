api = 2
core = 7.x

; Modules
projects[composer_manager][type] = "module"
projects[composer_manager][subdir] = "contrib"

projects[date][type] = "module"
projects[date][subdir] = "contrib"
projects[date][version] = 2.9

projects[admin_menu][type] = "module"
projects[admin_menu][subdir] = "contrib"

projects[ctools][type] = "module"
projects[ctools][subdir] = "contrib"
projects[ctools][version] = 1.12

projects[entity][type] = "module"
projects[entity][subdir] = "contrib"
projects[entity][download][full_version] = 7.x-1.7
projects[entity][patch][2341611] = https://www.drupal.org/files/issues/entity-multivalue-token-replacement-fix-2341611-0.patch
projects[entity][patch][2564119] = https://www.drupal.org/files/issues/Use-array-in-foreach-statement-2564119-1.patch
; Added to avoid warnings issue by multifield 1.0-alpha4 with entity 1.7
projects[entity][patch][] = patches/entity-1.7-compatibility-with-multifield-alpha4.patch

projects[entityreference][type] = "module"
projects[entityreference][subdir] = "contrib"
projects[entityreference][version] = 1.1

projects[views][type] = "module"
projects[views][subdir] = "contrib"
projects[views][version] = 3.14

projects[autocomplete_deluxe][type] = "module"
projects[autocomplete_deluxe][subdir] = "contrib"
projects[autocomplete_deluxe][version] = 2.1

projects[file_entity][type] = "module"
projects[file_entity][subdir] = "contrib"

projects[media][type] = "module"
projects[media][subdir] = "contrib"

projects[ckeditor][type] = "module"
projects[ckeditor][subdir] = "contrib"

projects[monolog][type] = "module"
projects[monolog][subdir] = "contrib"
projects[monolog][version] = 1.1

projects[module_filter][type] = "module"
projects[module_filter][subdir] = "contrib"

projects[token][type] = "module"
projects[token][subdir] = "contrib"
projects[token][version] = 1.6

projects[i18n][type] = "module"
projects[i18n][subdir] = "contrib"

projects[potx][type] = "module"
projects[potx][subdir] = "contrib"

projects[l10n_update][type] = "module"
projects[l10n_update][subdir] = "contrib"
projects[l10n_update][version] = 2.1
projects[l10n_update][patch][] = "patches/l10n_update 7.x-2.1-remove_string_validation.patch"
; Added local patch to remove string validation watchdog during string translation.

projects[rules][type] = "module"
projects[rules][subdir] = "contrib"

projects[media][type] = "module"
projects[media][subdir] = "contrib"

projects[migrate][type] = "module"
projects[migrate][subdir] = "contrib"
projects[migrate][version] = 2.8
projects[migrate][patch][] = "patches/migrate-2.8-startime-bigint-bmul.patch"
; This should be the path but does not work from a private repo.
; projects[migrate][patch][] = "https://gitlab.com/dsstnccolombia/dss-magdalena/raw/DATA-001/patches/migrate-2.8-startime-bigint-bmul.patch"

projects[migrate_extras][type] = "module"
projects[migrate_extras][subdir] = "contrib"

projects[node_clone][type] = "module"
projects[node_clone][subdir] = "contrib"
projects[node_clone][version] = 1.0
; Patch fix to work with og_moderation: https://www.drupal.org/node/2368293
projects[node_clone][patch][] = "patches/node_clone_accessfix.patch"
projects[node_clone][patch][] = "patches/node_clone-7.x-1.0-panelizer-compatilibity.patch"

projects[node_clone_tab][type] = "module"
projects[node_clone_tab][subdir] = "contrib"
projects[node_clone_tab][version] = 1.1
projects[node_clone_tab][patch][] = "patches/node_clone_tab-7.x-1.1-panelizer_compatibility.patch"

projects[import][type] = "module"
projects[import][subdir] = "contrib"

projects[libraries][type] = "module"
projects[libraries][subdir] = "contrib"
projects[libraries][version] = 2.2

projects[jquery_update][type] = "module"
projects[jquery_update][subdir] = "contrib"
projects[jquery_update][version] = 2.7

projects[d3][type] = "module"
projects[d3][subdir] = "contrib"
projects[d3][version] = 1.0-alpha1

projects[uuid][type] = "module"
projects[uuid][subdir] = "contrib"
projects[uuid][version] = 1.0-beta2

projects[views_bulk_operations][type] = "module"
projects[views_bulk_operations][subdir] = "contrib"
projects[views_bulk_operations][version] = 3.5
; Integrate Multipage selection https://www.drupal.org/node/1207348
projects[views_bulk_operations][patch][] = "https://www.drupal.org/files/issues/2018-12-21/vbo-multipage_selection-1207348-78.patch"
projects[views_bulk_operations][patch][] = "patches/views_bulk_operations-redirect-after-process-7.x-3.5.patch"

projects[views_megarow][type] = "module"
projects[views_megarow][subdir] = "contrib"
projects[views_megarow][version] = 1.7
projects[views_megarow][patch][] = "patches/views_megarow-7.x-1.7-url_query_params.patch"

projects[faq][type] = "module"
projects[faq][subdir] = "contrib"
projects[faq][version] = 1.1

projects[eva][type] = "module"
projects[eva][subdir] = "contrib"
projects[eva][version] = 1.3

projects[entityreference_prepopulate][type] = "module"
projects[entityreference_prepopulate][subdir] = "contrib"
projects[entityreference_prepopulate][version] = 1.7

projects[admin_views][type] = "module"
projects[admin_views][subdir] = "contrib"

projects[inline_entity_form][type] = "module"
projects[inline_entity_form][subdir] = "contrib"
projects[inline_entity_form][version] = "1.x-dev"
projects[inline_entity_form][download][type] = "git"
projects[inline_entity_form][download][url] = "http://git.drupal.org/project/inline_entity_form.git"
projects[inline_entity_form][download][revision] = "087106c5e5c2db7edd41428176a8e6511fa2a69c"

projects[inline_entity_display][type] = "module"
projects[inline_entity_display][subdir] = "contrib"

projects[xautoload][type] = "module"
projects[xautoload][subdir] = "contrib"
projects[xautoload][version] = 5.0-beta3

projects[strongarm][type] = "module"
projects[strongarm][subdir] = "contrib"
projects[strongarm][version] = 2.0

projects[features][type] = "module"
projects[features][subdir] = "contrib"
projects[features][version] = 2.10

projects[features_override][type] = "module"
projects[features_override][subdir] = "contrib"

projects[conditional_fields][type] = "module"
projects[conditional_fields][subdir] = "contrib"
projects[conditional_fields][patch][] = "https://www.drupal.org/files/issues/hide_empty_fieldsets-1161314-131.patch"

projects[field_permissions][type] = "module"
projects[field_permissions][subdir] = "contrib"

projects[machine_name][type] = "module"
projects[machine_name][subdir] = "contrib"
projects[machine_name][version] = 1.0
; Taken from https://www.drupal.org/node/2813785. Adapted to fit this version.
projects[machine_name][patch][] = "patches/machine_name-integrate_with_entity_metadata_wrapper-2813785-3.patch"

projects[ajax_throbber][type] = "module"
projects[ajax_throbber][subdir] = "contrib"
projects[ajax_throbber][version] = "1.x-dev"
projects[ajax_throbber][download][type] = "git"
projects[ajax_throbber][download][url] = "https://git.drupal.org/project/ajax_throbber.git"
projects[ajax_throbber][download][revision] = "caa4628a2ba1911aa25310b369822ace0528be8d"

projects[color_field][type] = "module"
projects[color_field][subdir] = "contrib"

projects[multifield][type] = "module"
projects[multifield][subdir] = "contrib"
projects[multifield][version] = 1.0-alpha4
; Patch https://www.drupal.org/node/2041533
projects[multifield][patch][] = "https://www.drupal.org/files/issues/multifield-2041533-96.patch"
; Patch https://www.drupal.org/node/2041531
projects[multifield][patch][] = "https://www.drupal.org/files/issues/2041531-23-entity-api-support.patch"
; Patch https://www.drupal.org/node/2665634
projects[multifield][patch][] = "https://www.drupal.org/files/issues/warning_expects_array-2665634-2.patch"
; Very slow multifield_load() function when many multifield entities.
projects[multifield][patch][] = "https://www.drupal.org/files/issues/multifield-2783909-multifield_load_slow-2.patch"
; Allows to handle data migration for multifield subfields https://www.drupal.org/node/2217353
projects[multifield][patch][] = "patches/multifield-migrate_handler_sima.patch"

projects[multifield_table][type] = "module"
projects[multifield_table][subdir] = "contrib"
projects[multifield_table][version] = 1.0

projects[multifield_table][type] = "module"
projects[multifield_table][subdir] = "contrib"
projects[multifield_table][version] = 1.0

projects[multiple_value_widget][type] = "module"
projects[multiple_value_widget][subdir] = "contrib"
projects[multiple_value_widget][version] = 1.0-beta4

projects[entityreference_view_widget][type] = "module"
projects[entityreference_view_widget][subdir] = "contrib"

projects[simple_field_formatter][type] = "module"
projects[simple_field_formatter][subdir] = "contrib"

projects[field_formatter_settings][type] = "module"
projects[field_formatter_settings][subdir] = "contrib"

projects[better_exposed_filters][type] = "module"
projects[better_exposed_filters][subdir] = "contrib"

projects[custom_add_another][type] = "module"
projects[custom_add_another][subdir] = "contrib"

projects[range][type] = "module"
projects[range][subdir] = "contrib"

projects[machine_name][type] = "module"
projects[machine_name][subdir] = "contrib"

projects[shs][type] = "module"
projects[shs][subdir] = "contrib"

projects[queue_ui][type] = "module"
projects[queue_ui][subdir] = "contrib"

projects[leaflet][type] = "module"
projects[leaflet][subdir] = "contrib"
projects[leaflet][version] = 1.4
; Allows to not clash with existent leaflet library used by DKAN.
projects[leaflet][patch][] = "patches/leaflet-7.x-1.4-leafletjs.patch"

projects[leaflet_more_maps][type] = "module"
projects[leaflet_more_maps][subdir] = "contrib"
projects[leaflet_more_maps][version] = 1.17

; Needed by DKAN.
projects[draggableviews][type] = "module"
projects[draggableviews][subdir] = "contrib"

projects[path_breadcrumbs][type] = "module"
projects[path_breadcrumbs][subdir] = "contrib"

projects[radix_layouts][type] = "module"
projects[radix_layouts][subdir] = "contrib"
projects[radix_layouts][version] = 3.4

projects[panels][type] = "module"
projects[panels][subdir] = "contrib"
projects[panels][version] = 3.9

projects[panelizer][type] = "module"
projects[panelizer][subdir] = "contrib"
projects[panelizer][version] = 3.4

projects[panels_style_collapsible][type] = "module"
projects[panels_style_collapsible][subdir] = "contrib"
projects[panels_style_collapsible][version] = 1.3

projects[facetapi_taxonomy_folding][type] = "module"
projects[facetapi_taxonomy_folding][subdir] = "contrib"
projects[facetapi_taxonomy_folding][version] = 1.0

; This module has problems when enabled in the site.
;projects[fieldable_panels_panes][type] = "module"
;projects[fieldable_panels_panes][subdir] = "contrib"
;projects[fieldable_panels_panes][version] = 1.10

projects[entityreference_filter][type] = "module"
projects[entityreference_filter][subdir] = "contrib"
projects[entityreference_filter][version] = 1.5

projects[restws][type] = "module"
projects[restws][subdir] = "contrib"
projects[restws][version] = 2.6

projects[field_reference_delete][type] = "module"
projects[field_reference_delete][subdir] = "contrib"
projects[field_reference_delete][version] = 1.0-beta1

projects[defaultconfig][type] = "module"
projects[defaultconfig][subdir] = "contrib"
projects[defaultconfig][version] = 1.0-alpha11

projects[conditional_styles][type] = "module"
projects[conditional_styles][subdir] = "contrib"
projects[conditional_styles][version] = 2.2

projects[roleassign][type] = "module"
projects[roleassign][subdir] = "contrib"
projects[roleassign][version] = 1.1

projects[country_field][type] = "module"
projects[country_field][subdir] = "contrib"
projects[country_field][version] = 1.0

projects[features_roles_permissions][type] = "module"
projects[features_roles_permissions][subdir] = "contrib"
projects[features_roles_permissions][version] = 1.2

projects[colorizer][type] = "module"
projects[colorizer][subdir] = "contrib"
projects[colorizer][version] = 1.10
projects[colorizer][patch][] = "https://www.drupal.org/files/issues/colorizer-add-rgb-vars-2227651-4b.patch"
projects[colorizer][patch][] = "https://www.drupal.org/files/issues/colorizer-bug_system_cron_delete_current_css-2599298-9.patch"

projects[fontyourface][type] = "module"
projects[fontyourface][subdir] = "contrib"
projects[fontyourface][version] = 2.8
projects[fontyourface][patch][] = "patches/fontyourface-no-ajax-browse-view.patch"
projects[fontyourface][patch][] = "patches/fontyourface-clear-css-cache.patch"
projects[fontyourface][patch][] = "https://www.drupal.org/files/issues/browse-fonts-page-uses-disabled-font-2644694.patch"

; This is defined in DKAN, but we are excluding the og_examples module.
projects[og][type] = "module"
projects[og][subdir] = "contrib"
projects[og][version] = 2.9
projects[og][patch][] = "patches/og-remove_og_example_module-7.x-2.9.patch"

projects[og_moderation][type] = "module"
projects[og_moderation][subdir] = "contrib"
projects[og_moderation][version] = 2.3
; Patch fix for node_clone: https://www.drupal.org/node/2368293
projects[og_moderation][patch][] = "https://www.drupal.org/files/issues/overwrites_clone_access-2368293-2.patch"

projects[advanced_help][type] = "module"
projects[advanced_help][subdir] = "contrib"
projects[advanced_help][version] = 1.3

projects[shield][type] = "module"
projects[shield][subdir] = "contrib"
projects[shield][version] = 1.3

;Custom Contextual Help
projects[cch][type] = "module"
projects[cch][subdir] = "contrib"
projects[cch][patch][] = "patches/custom_contextual_help_read_sima_help_files.patch"
projects[cch][patch][] = "patches/custom_contextual_help_js.patch"

; Obtaining the visualization_entity module
projects[visualization_entity][type] = "module"
projects[visualization_entity][subdir] = "contrib"
projects[visualization_entity][download][type] = git
projects[visualization_entity][download][url] = "https://github.com/NuCivic/visualization_entity.git"
projects[visualization_entity][download][tag] = 7.x-1.0-beta1
projects[visualization_entity][patch][] = "patches/visualization_entity-7.x-1.0-beta1-patch-geo_file_entity.patch"

; Spam Protection modules
projects[honeypot][type] = "module"
projects[honeypot][subdir] = "contrib"

; DKAN Dataset & DKAN Datastore Modules.
projects[dkan_dataset][type] = "module"
projects[dkan_dataset][subdir] = "dkan"
projects[dkan_dataset][download][type] = get
;projects[dkan_dataset][download][url] = "https://github.com/NuCivic/dkan_dataset/archive/7.x-1.12.2.zip"
projects[dkan_dataset][download][url] = "https://github.com/NuCivic/dkan_dataset/archive/7.x-1.12.9.zip"
projects[dkan_dataset][patch][] = "patches/dkan_dataset.forms.inc.patch"
;projects[dkan_dataset][patch][] = "patches/dkan_dataset.make-7.x-1.12.2.patch"
projects[dkan_dataset][patch][] = "patches/dkan_dataset.make-7.x-1.12.9.patch"
projects[dkan_dataset][patch][] = "patches/dkan_dataset-recline-show-warning-messages.patch"
projects[dkan_dataset][patch][] = "patches/dkan_dataset-display-resource-updates.patch"

projects[dkan_datastore][type] = "module"
projects[dkan_datastore][subdir] = "dkan"
projects[dkan_datastore][download][type] = get
;projects[dkan_datastore][download][url] = "https://github.com/NuCivic/dkan_datastore/archive/7.x-1.12.zip"
projects[dkan_datastore][download][url] = "https://github.com/NuCivic/dkan_datastore/archive/7.x-1.12.9.zip"
projects[dkan_datastore][patch][] = "patches/dkan_datastore-warning-resource-not-added-to-datastore.patch"

; DKAN Sidewide Modules.
projects[dkan_sitewide][type] = "module"
projects[dkan_sitewide][subdir] = "dkan"
projects[dkan_sitewide][download][type] = file
projects[dkan_sitewide][download][url] = "https://github.com/NuCivic/dkan/archive/7.x-1.12.9.zip"
projects[dkan_sitewide][download][subtree] = "dkan-7.x-1.12.9/modules/dkan/dkan_sitewide"
projects[dkan_sitewide][patch][] = "patches/dkan_sitewide-7.x-1.12.9.patch"
projects[dkan_sitewide][patch][] = "patches/dkan_sitewide-remove-dkan_default_content-7.x-1.12.9.patch"
projects[dkan_sitewide][patch][] = "patches/dkan_sitewide-remove-dkan_sitewide_context-7.x-1.12.9.patch"
projects[dkan_sitewide][patch][] = "patches/dkan_sitewide-remove-dkan_sitewide_demo_front-7.x-1.12.9.patch"
projects[dkan_sitewide][patch][] = "patches/dkan_sitewide-remove-dkan_sitewide_menu-7.x-1.12.9.patch"
projects[dkan_sitewide][patch][] = "patches/dkan_sitewide-remove-dkan_sitewide_roles_perms-7.x-1.12.9.patch"

; TB Mega Menu Module.
projects[tb_megamenu][type] = "module"
projects[tb_megamenu][subdir] = "contrib"
projects[tb_megamenu][download][type] = file
projects[tb_megamenu][download][url] = "https://www.drupal.org/files/issues/tb_megamenu-7.x-1.x-dev-bootstrap3.3.4..zip"
projects[tb_megamenu][patch][] = "patches/tb_megamenu-7.x-1.x-dev-bootstrap3.3.4.patch"

; Development modules
projects[devel][type] = "module"
projects[devel][subdir] = "contrib"

; Contributed themes.
projects[bootstrap][type] = "theme"
projects[bootstrap][subdir] = "contrib"

; This revision corresponds to version 7.x-1.12.3, but zip file has problems.
; This revision corresponds to version 7.x-1.12.9, but zip file has problems.
projects[nuboot_radix][type] = "theme"
projects[nuboot_radix][subdir] = "contrib"
;projects[nuboot_radix][version] = "7.x-1.12.3"
projects[nuboot_radix][version] = "7.x-1.12.9"
projects[nuboot_radix][download][type] = "git"
projects[nuboot_radix][download][url] = "https://github.com/NuCivic/nuboot_radix.git"
; This line is for version 7.x-1.12.3.
;projects[nuboot_radix][download][revision] = "2d2e4b4cb4a9dd0e52c34d561e22ddc29b2c4787"
; Change to this one for version 7.x-1.12.9.
projects[nuboot_radix][download][revision] = "b43c6561188712f8910b1c607e5d0c163fc26d5f"
projects[nuboot_radix][patch][] = "patches/tb_megamenu-nuboot_radix-7.x-1.0-rc2.patch"
projects[nuboot_radix][patch][] = "patches/nuboot_radix-change-dataset-resource-icons-7.x-1.0-rc2.patch"
projects[nuboot_radix][patch][] = "patches/nuboot_radix-change-hero-image-7.x-1.0-rc2.patch"

projects[radix][type] = "theme"
projects[radix][subdir] = "contrib"

projects[profile2][type] = "module"
projects[profile2][subdir] = "contrib"
projects[profile2][version] = 1.3

projects[site_disclaimer][type] = "module"
projects[site_disclaimer][subdir] = "contrib"
projects[site_disclaimer][version] = 1.3

projects[views_current_path][type] = "module"
projects[views_current_path][subdir] = "contrib"
projects[views_current_path][version] = 1.0

; Libraries

; This is included in the visualization_entity module.
;libraries[d3][download][type] = get
;libraries[d3][download][url] = "https://github.com/mbostock/d3/releases/download/v3.5.16/d3.zip"
;libraries[d3][destination] = libraries
;libraries[d3][directory_name] = d3

libraries[c3][download][type] = get
libraries[c3][download][url] = "https://github.com/c3js/c3/archive/v0.4.13.zip"
libraries[c3][destination] = libraries
libraries[c3][directory_name] = c3

libraries[nvd3js][download][type] = get
libraries[nvd3js][download][url] = "https://github.com/novus/nvd3/archive/v1.8.2.zip"
libraries[nvd3js][destination] = libraries
libraries[nvd3js][directory_name] = nvd3js

libraries[d3plus][download][type] = get
libraries[d3plus][download][url] = "https://github.com/alexandersimoes/d3plus/releases/download/v1.9.5/d3plus.zip"
libraries[d3plus][destination] = libraries
libraries[d3plus][directory_name] = d3plus

libraries[dimple][download][type] = get
libraries[dimple][download][url] = "https://github.com/PMSI-AlignAlytics/dimple/releases/download/2.2.0/dimple.v2.2.0.min.js"
libraries[dimple][destination] = libraries
libraries[dimple][directory_name] = dimple

libraries[jquery-simple-color][download][type] = get
libraries[jquery-simple-color][download][url] = "https://github.com/recurser/jquery-simple-color/releases/tag/v1.2.1"
libraries[jquery-simple-color][destination] = libraries
libraries[jquery-simple-color][directory_name] = jquery-simple-color

libraries[leafletjs][download][type] = get
libraries[leafletjs][download][url] = "http://cdn.leafletjs.com/leaflet/v1.0.3/leaflet.zip"
libraries[leafletjs][destination] = libraries
libraries[leafletjs][directory_name] = leafletjs

libraries[dc][download][type] = get
libraries[dc][download][url] = "https://github.com/dc-js/dc.js/archive/2.0.1.zip"
libraries[dc][destination] = libraries
libraries[dc][directory_name] = dc

libraries[crossfilter][download][type] = get
libraries[crossfilter][download][url] = "https://github.com/square/crossfilter/archive/v1.3.12.zip"
libraries[crossfilter][destination] = libraries
libraries[crossfilter][directory_name] = crossfilter

libraries[dc_leaflet][download][type] = get
libraries[dc_leaflet][download][url] = "https://github.com/dc-js/dc.leaflet.js/archive/0.3.1.zip"
libraries[dc_leaflet][destination] = libraries
libraries[dc_leaflet][directory_name] = dc_leaflet

libraries[bootstrap][download][type] = get
libraries[bootstrap][download][url] = "https://github.com/twbs/bootstrap/archive/v3.3.7.zip"
libraries[bootstrap][destination] = libraries
libraries[bootstrap][directory_name] = bootstrap

libraries[bootstrap-slider][download][type] = get
libraries[bootstrap-slider][download][url] = "https://github.com/seiyria/bootstrap-slider/archive/v9.7.2.zip"
libraries[bootstrap-slider][destination] = libraries
libraries[bootstrap-slider][directory_name] = bootstrap-slider

libraries[lz_string][download][type] = get
libraries[lz_string][download][url] = "https://github.com/pieroxy/lz-string/archive/1.4.4.zip"
libraries[lz_string][destination] = libraries
libraries[lz_string][directory_name] = lz_string

libraries[spectrum][download][type] = get
libraries[spectrum][download][url] = "https://github.com/bgrins/spectrum/archive/1.8.0.zip"
libraries[spectrum][destination] = libraries
libraries[spectrum][directory_name] = spectrum

libraries[leaflet_fullscreen][download][type] = get
libraries[leaflet_fullscreen][download][url] = "https://github.com/brunob/leaflet.fullscreen/archive/1.1.4.zip"
libraries[leaflet_fullscreen][destination] = libraries
libraries[leaflet_fullscreen][directory_name] = leaflet_fullscreen
