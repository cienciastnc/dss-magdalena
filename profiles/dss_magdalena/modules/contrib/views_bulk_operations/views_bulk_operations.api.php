<?php

/**
 * @file
 * Hooks provided by Views Bulk Operations.
 */

/**
 * Perform alterations on the VBO form before it is rendered.
 *
 * Usually, if a module wanted to alter the VBO form through hook_form_alter(),
 * it would need to duplicate the views form checks from
 * views_bulk_operations_form_alter(), while making sure that the hook
 * runs after VBO's hook (by increasing the weight of the altering module's
 * system entry). In order to reduce that complexity, VBO provides this hook.
 *
 * @param $form
 *  A step of the VBO form to be altered.
 * @param $form_state
 *  Form state. Contains the name of the current step in $form_state['step'].
 * @param $vbo
 *   The VBO views field. Contains a reference to the view in $vbo->view.
 */
function hook_views_bulk_operations_form_alter(&$form, &$form_state, $vbo) {
  if ($form_state['step'] == 'views_form_views_form') {
    // Alter the first step of the VBO form (the selection page).
    $form['select']['#title'] = t('Bulk operations');
  }
  elseif ($form_state['step'] == 'views_bulk_operations_config_form') {
    // Alter the configuration step of the VBO form.
  }
  elseif ($form_state['step'] == 'views_bulk_operations_confirm_form') {
    // Alter the confirmation step of the VBO form.
  }
}

/**
 * Allow other modules to notify VBO that the selection needs to be adjusted.
 *
 * @param bool $adjust_selection
 *   Flag determining whether selection should be adjusted.
 * @param array $selection
 *   An array in the form of $row_index => $entity_id representing the submitted
 *   selection.
 * @param views_bulk_operations_handler_field_operations $vbo
 *   The current VBO field.
 *
 * @see views_bulk_operations_multipage_views_bulk_operations_adjust_selection_alter()
 */
function hook_views_bulk_operations_adjust_selection_alter(&$adjust_selection, $selection, $vbo) {
  if (!$adjust_selection && !empty($vbo->view->my_module_key) && count($selection) > 1) {
    $adjust_selection = TRUE;
  }
}
