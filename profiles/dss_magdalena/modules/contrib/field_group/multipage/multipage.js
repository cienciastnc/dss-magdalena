
!function(a) {
    a(document).ready(function() {
       //remplazar contador
       if(window.location.pathname=="/node/add/scenario"||window.location.pathname=="/node/add/integral-plan" ){
        jQuery(".contador").text("5");
        //jQuery(".contador").remove();
       }

        ocultarBoton = document.getElementById("edit-field-creation-date-und-0-value-datepicker-popup-0");
        if (void 0 == ocultarBoton) {
            document.getElementById("popup-link").style.display = "none";
            document.getElementById("popup").style.display = "none";
        }
        document.getElementById("paso1M").style.backgroundColor = "#104659";
        document.getElementById("paso1M").style.color = "#DFF1F2";
        document.getElementById("paso2M").style.backgroundColor = "#FFFFFF";
        document.getElementById("paso2M").style.color = "#104659";
        document.getElementById("edit-submit").style.display = "none";
        document.getElementById("edit-preview").style.display = "none";
    });
    Drupal.behaviors.MultiPage = {
        attach: function(b) {
            a(".multipage-panes", b).once("multipage", function() {
                var b = a(":hidden.multipage-active-control", this).val();
                var c;
                var d = a("> div.field-group-multipage", this);
                var e = d.parents("form");
                if (0 == d.length) return;
                var f;
                d.each(function() {
                    f = a('<div class="multipage-controls-list clearfix"></div>');
                    a(this).append(f);
                    if (Drupal.settings.field_group.multipage_move_submit && a(".form-actions").length) a(".form-actions", e).remove().appendTo(a(this, d.last()));
                    var g = new Drupal.multipageControl({
                        title: a("> .multipage-pane-title", this).text(),
                        wrapper: a(this),
                        has_next: a(this).next().length,
                        has_previous: a(this).prev().length
                    });
                    f.append(g.item);
                    a(this).addClass("multipage-pane").data("multipageControl", g);
                    if (this.id == b) c = a(this);
                });
                if (void 0 === c) {
                    var g = window.location.hash.replace(/[=%;,\/]/g, "");
                    if ("#" !== g && a(g, this).length) c = a(window.location.hash, this).closest(".multipage-pane"); else c = a("multipage-open", this).length ? a("multipage-open", this) : a("> .multipage-pane:first", this);
                }
                if (void 0 !== c) c.data("multipageControl").focus();
            });
        }
    };
    var b = 1;
    function c(a) {
        //formulario Escenario
        if(window.location.pathname=="/node/add/scenario"){
            if(jQuery("#edit-field-climate-scenario-und").length & b==3){           
                if(jQuery('input[name="field_climate_scenario[und]"]:checked').val()!=undefined ){
                    pasoVali=true;
                    return pasoVali;
                }
                else{
                    alert("por favor seleccione una opción");
                    pasoVali=false;
                    b--;
                    return pasoVali;    
                }
            }
            else if(jQuery("#edit-field-energy-scenario").length & b==4){
                if(jQuery('input[name="field_energy_scenario[und]"]:checked').val()!=undefined ){
                    pasoVali=true;
                    return pasoVali;
                }
                else{
                    alert("por favor seleccione una opción");
                    pasoVali=false;
                    b--;
                    return pasoVali;    
                }
            }
            else if(jQuery("#edit-field-population-scenario-und").length & b==5){
                if(jQuery('input[name="field_population_scenario[und]"]:checked').val()!=undefined ){
                    pasoVali=true;
                    document.getElementById("edit-submit").style.display = "inline-block";
                    return pasoVali;
                }
                else{
                    alert("por favor seleccione una opción");
                    pasoVali=false;
                    b--;
                    return pasoVali;
                 }
            }
        }


        //formulario plan integral
        if(window.location.pathname=="/node/add/integral-plan"){
            if(jQuery("#edit-field-hydropower-alternatives").length & b==3){           
                if(jQuery('input[name="field_hydropower_alternatives[und]"]:checked').val()!=undefined ){
                    pasoVali=true;
                    return pasoVali;
                }
                else{
                    alert("por favor seleccione una opción");
                    pasoVali=false;
                    b--;
                    return pasoVali;    
                }
            }
            else if(jQuery("#edit-field-prod-landuse-alternatives").length & b==4){
                if(jQuery('input[name="field_prod_landuse_alternatives[und]"]:checked').val()!=undefined ){
                    pasoVali=true;
                    return pasoVali;
                }
                else{
                    alert("por favor seleccione una opción");
                    pasoVali=false;
                    b--;
                    return pasoVali;    
                }
            }
            else if(jQuery("#edit-field-connectivity-alternatives").length & b==5){
                if(jQuery('input[name="field_connectivity_alternatives[und]"]:checked').val()!=undefined ){
                    pasoVali=true;
                    document.getElementById("edit-submit").style.display = "inline-block";
                    return pasoVali;
                }
                else{
                    alert("por favor seleccione una opción");
                    pasoVali=false;
                    b--;
                    return pasoVali;
                    }
            }
        }



        //formulario casos de esudio

        pasoVali = true;
        if (2 == b) {
            campoOblig = document.getElementById("edit-title").value;
            if ("" != campoOblig) {
                pasoVali = true;
                document.getElementById("paso1M").style.backgroundColor = "#2ca13a";
                document.getElementById("paso2M").style.backgroundColor = "#104659";
                document.getElementById("paso2M").style.color = "#DFF1F2";
            } else {
                b = 1;
                alert("Complete el campo Título");
                pasoVali = false;
                return pasoVali;
            }
        }
        if (3 == b) {
            nexValidatio1 = document.getElementById("edit-field-scenario-und-form-field-climate-scenario-und-301");
            if (void 0 == nexValidatio1) {
                campoOblig = document.getElementById("edit-field-energy-scenario-und-309");
                if (void 0 == campoOblig) {
                    nexValidatio = document.getElementById("edit-field-prod-landuse-alternatives");
                    if (void 0 == nexValidatio) {
                        nexValidatio = document.getElementById("ief-entity-table-edit-field-scenario-und-entities");
                        if (void 0 == nexValidatio) {
                            b = 2;
                            alert("seleccione un escenario");
                            pasoVali = false;
                            return pasoVali;
                        } else {
                            document.getElementById("paso2M").style.backgroundColor = "#2ca13a";
                            document.getElementById("paso2M1").style.backgroundColor = "#2ca13a";
                            document.getElementById("paso2M1").style.color = "#DFF1F2";
                            document.getElementById("paso2M2").style.backgroundColor = "#2ca13a";
                            document.getElementById("paso2M2").style.color = "#DFF1F2";
                            document.getElementById("paso2M3").style.backgroundColor = "#2ca13a";
                            document.getElementById("paso2M3").style.color = "#DFF1F2";
                            document.getElementById("paso3M").style.backgroundColor = "#104659";
                            document.getElementById("paso3M").style.color = "#DFF1F2";
                        }
                    }
                } else {
                    document.getElementById("paso2M").style.backgroundColor = "#2ca13a";
                    document.getElementById("paso2M1").style.backgroundColor = "#2ca13a";
                    document.getElementById("paso2M1").style.color = "#DFF1F2";
                    document.getElementById("paso2M2").style.backgroundColor = "#2ca13a";
                    document.getElementById("paso2M2").style.color = "#DFF1F2";
                    document.getElementById("paso2M3").style.backgroundColor = "#2ca13a";
                    document.getElementById("paso2M3").style.color = "#DFF1F2";
                    document.getElementById("paso3M").style.backgroundColor = "#104659";
                    document.getElementById("paso3M").style.color = "#DFF1F2";
                }
            }
            if (void 0 != nexValidatio1) {
                b = 5;
                if (5 == b) {
                    campoOblig = document.getElementById("edit-field-scenario-und-form-title").value;
                    if ("" != campoOblig) {
                        pasoVali = true;
                        document.getElementById("paso2M1").style.backgroundColor = "#104659";
                        document.getElementById("paso2M1").style.color = "#DFF1F2";
                        document.getElementById("edit-field-scenario-und-form-actions-ief-add-save").style.display = "none";
                    } else {
                        b = 4;
                        alert("Complete el campo Nombre del Escenario");
                        pasoVali = false;
                        return pasoVali;
                    }
                }
            }
        }
        if (5 == b) {
            campoOblig = document.getElementById("edit-field-scenario-und-form-title").value;
            if ("" != campoOblig) {
                pasoVali = true;
                document.getElementById("paso2M1").style.backgroundColor = "#104659";
                document.getElementById("paso2M1").style.color = "#DFF1F2";
                document.getElementById("edit-field-scenario-und-form-actions-ief-add-save").style.display = "none";
            } else {
                b = 4;
                alert("Complete el campo Nombre del Escenario");
                pasoVali = false;
                return pasoVali;
            }
        }
        if (6 == b) {
            nexValidatio5 = document.getElementById("edit-field-scenario-und-form-field-energy-scenario-und-none");
            if (void 0 != nexValidatio5) {
                document.getElementById("paso2M1").style.backgroundColor = "#2ca13a";
                document.getElementById("paso2M2").style.backgroundColor = "#104659";
                document.getElementById("paso2M2").style.color = "#DFF1F2";
            }
            if (void 0 == nexValidatio5) {
                document.getElementById("paso2M").style.backgroundColor = "#2ca13a";
                document.getElementById("paso2M1").style.backgroundColor = "#2ca13a";
                document.getElementById("paso2M1").style.color = "#DFF1F2";
                document.getElementById("paso2M2").style.backgroundColor = "#2ca13a";
                document.getElementById("paso2M2").style.color = "#DFF1F2";
                document.getElementById("paso2M3").style.backgroundColor = "#2ca13a";
                document.getElementById("paso2M3").style.color = "#DFF1F2";
                document.getElementById("paso3M").style.backgroundColor = "#104659";
                document.getElementById("paso3M").style.color = "#DFF1F2";
                b = 3;
            }
        }
        if (7 == b) {
            nexValidatio6 = document.getElementById("edit-field-scenario-und-form-field-energy-scenario-und-none");
            if (void 0 != nexValidatio6) {
                document.getElementById("paso2M2").style.backgroundColor = "#2ca13a";
                document.getElementById("paso2M3").style.backgroundColor = "#104659";
                document.getElementById("paso2M3").style.color = "#DFF1F2";
                document.getElementById("edit-field-scenario-und-form-actions-ief-add-save").style.display = "inline-block";
                b = 2;
            }
            if (void 0 == nexValidatio6) {
                document.getElementById("paso2M").style.backgroundColor = "#2ca13a";
                document.getElementById("paso2M1").style.backgroundColor = "#2ca13a";
                document.getElementById("paso2M1").style.color = "#DFF1F2";
                document.getElementById("paso2M2").style.backgroundColor = "#2ca13a";
                document.getElementById("paso2M2").style.color = "#DFF1F2";
                document.getElementById("paso2M3").style.backgroundColor = "#2ca13a";
                document.getElementById("paso2M3").style.color = "#DFF1F2";
                document.getElementById("paso3M").style.backgroundColor = "#104659";
                document.getElementById("paso3M").style.color = "#DFF1F2";
                b = 3;
            }
        }
        if (4 == b) {
            nexValidatio2 = document.getElementById("edit-field-integral-plan-und-form-field-hydropower-alternatives-und");
            if (void 0 != nexValidatio2) {
                campoOblig = document.getElementById("edit-field-integral-plan-und-form-title").value;
                if ("" != campoOblig) {
                    pasoVali = true;
                    document.getElementById("paso2M").style.backgroundColor = "#2ca13a";
                    document.getElementById("paso2M1").style.backgroundColor = "#2ca13a";
                    document.getElementById("paso2M1").style.color = "#DFF1F2";
                    document.getElementById("paso2M2").style.backgroundColor = "#2ca13a";
                    document.getElementById("paso2M2").style.color = "#DFF1F2";
                    document.getElementById("paso2M3").style.backgroundColor = "#2ca13a";
                    document.getElementById("paso2M3").style.color = "#DFF1F2";
                    document.getElementById("paso3M").style.backgroundColor = "#104659";
                    document.getElementById("paso3M").style.color = "#DFF1F2";
                    document.getElementById("paso3M1").style.backgroundColor = "#104659";
                    document.getElementById("paso3M1").style.color = "#DFF1F2";
                    document.getElementById("edit-field-integral-plan-und-form-actions-ief-add-save").style.display = "none";
                    b = 7;
                } else {
                    b = 3;
                    alert("Complete el campo Nombre de Plan Integral ");
                    pasoVali = false;
                    return pasoVali;
                }
            }
            if (void 0 == nexValidatio2) {
                campoOblig = document.getElementById("ief-entity-table-edit-field-integral-plan-und-entities");
                if (void 0 != campoOblig) {
                    document.getElementById("paso3M").style.backgroundColor = "#2ca13a";
                    document.getElementById("paso3M1").style.backgroundColor = "#2ca13a";
                    document.getElementById("paso3M1").style.color = "#DFF1F2";
                    document.getElementById("paso3M2").style.backgroundColor = "#2ca13a";
                    document.getElementById("paso3M2").style.color = "#DFF1F2";
                    document.getElementById("paso3M3").style.backgroundColor = "#2ca13a";
                    document.getElementById("paso3M3").style.color = "#DFF1F2";
                    document.getElementById("edit-submit").style.display = "inline-block";
                    document.getElementById("edit-preview").style.display = "inline-block";
                } else {
                    nexValidatio2 = document.getElementById("edit-field-connectivity-alternatives");
                    if (void 0 == nexValidatio2) {
                        nexValidatio2 = document.getElementById("edit-field-population-scenario");
                        if (void 0 == nexValidatio2) {
                            b = 3;
                            alert("seleccione un plan integral");
                            pasoVali = false;
                            return pasoVali;
                        }
                    }
                    document.getElementById("edit-submit").style.display = "inline-block";
                    document.getElementById("edit-preview").style.display = "inline-block";
                }
            }
        }
        if (8 == b) {
            nexValidatio7 = document.getElementById("edit-field-integral-plan-und-form-status-1");
            if (void 0 != nexValidatio7) {
                document.getElementById("paso3M1").style.backgroundColor = "#2ca13a";
                document.getElementById("paso3M2").style.backgroundColor = "#104659";
                document.getElementById("paso3M2").style.color = "#DFF1F2";
            }
            if (void 0 == nexValidatio7) {
                document.getElementById("paso3M").style.backgroundColor = "#2ca13a";
                document.getElementById("paso3M1").style.backgroundColor = "#2ca13a";
                document.getElementById("paso3M1").style.color = "#DFF1F2";
                document.getElementById("paso3M2").style.backgroundColor = "#2ca13a";
                document.getElementById("paso3M2").style.color = "#DFF1F2";
                document.getElementById("paso3M3").style.backgroundColor = "#2ca13a";
                document.getElementById("paso3M3").style.color = "#DFF1F2";
                b = 4;
            }
        }
        if (9 == b) {
            nexValidatio8 = document.getElementById("edit-field-integral-plan-und-form-field-prod-landuse-alternatives-und");
            if (void 0 != nexValidatio8) {
                document.getElementById("paso3M2").style.backgroundColor = "#2ca13a";
                document.getElementById("paso3M3").style.backgroundColor = "#104659";
                document.getElementById("paso3M3").style.color = "#DFF1F2";
                document.getElementById("edit-field-integral-plan-und-form-actions-ief-add-save").style.display = "inline-block";
                b = 3;
            }
            if (void 0 == nexValidatio8) {
                document.getElementById("paso3M").style.backgroundColor = "#2ca13a";
                document.getElementById("paso3M1").style.backgroundColor = "#2ca13a";
                document.getElementById("paso3M1").style.color = "#DFF1F2";
                document.getElementById("paso3M2").style.backgroundColor = "#2ca13a";
                document.getElementById("paso3M2").style.color = "#DFF1F2";
                document.getElementById("paso3M3").style.backgroundColor = "#2ca13a";
                document.getElementById("paso3M3").style.color = "#DFF1F2";
                b = 4;
            }
        }
    }
    function d(a) {
        if (1 == b) {
            nexValidatio1 = document.getElementById("edit-field-scenario-und-form-field-energy-scenario-und-309");
            if (void 0 == nexValidatio1) {
                document.getElementById("paso1M").style.backgroundColor = "#104659";
                document.getElementById("paso2M").style.backgroundColor = "#FFFFFF";
                document.getElementById("paso2M").style.color = "#104659";
                document.getElementById("paso2M1").style.backgroundColor = "#FFFFFF";
                document.getElementById("paso2M1").style.color = "#104659";
                document.getElementById("paso2M2").style.backgroundColor = "#FFFFFF";
                document.getElementById("paso2M2").style.color = "#104659";
                document.getElementById("paso2M3").style.backgroundColor = "#FFFFFF";
                document.getElementById("paso2M3").style.color = "#104659";
            }
            if (void 0 != nexValidatio1) {
                document.getElementById("paso2M2").style.backgroundColor = "#104659";
                document.getElementById("paso2M3").style.backgroundColor = "#FFFFFF";
                document.getElementById("paso2M3").style.color = "#104659";
                document.getElementById("edit-field-scenario-und-form-actions-ief-add-save").style.display = "none";
                b = 6;
                return false;
            }
        }
        if (2 == b) {
            nexValidatio2 = document.getElementById("edit-field-integral-plan-und-form-field-prod-landuse-alternatives-und");
            if (void 0 == nexValidatio2) {
                document.getElementById("paso2M").style.backgroundColor = "#104659";
                document.getElementById("paso2M1").style.color = "#DFF1F2";
                document.getElementById("paso2M1").style.backgroundColor = "#FFFFFF";
                document.getElementById("paso2M1").style.color = "#104659";
                document.getElementById("paso2M2").style.backgroundColor = "#FFFFFF";
                document.getElementById("paso2M2").style.color = "#104659";
                document.getElementById("paso2M3").style.backgroundColor = "#FFFFFF";
                document.getElementById("paso2M3").style.color = "#104659";
                document.getElementById("paso3M").style.backgroundColor = "#FFFFFF";
                document.getElementById("paso3M").style.color = "#104659";
                document.getElementById("paso3M1").style.backgroundColor = "#FFFFFF";
                document.getElementById("paso3M1").style.color = "#104659";
                document.getElementById("paso3M2").style.backgroundColor = "#FFFFFF";
                document.getElementById("paso3M2").style.color = "#104659";
                document.getElementById("paso3M3").style.backgroundColor = "#FFFFFF";
                document.getElementById("paso3M3").style.color = "#104659";
            }
            if (void 0 != nexValidatio2) {
                b = 8;
                document.getElementById("paso3M2").style.backgroundColor = "#104659";
                document.getElementById("paso3M3").style.backgroundColor = "#FFFFFF";
                document.getElementById("paso3M3").style.color = "#104659";
                document.getElementById("edit-field-integral-plan-und-form-actions-ief-add-save").style.display = "none";
            }
        }
        if (3 == b) {
            document.getElementById("paso3M").style.backgroundColor = "#104659";
            document.getElementById("paso3M1").style.backgroundColor = "#FFFFFF";
            document.getElementById("paso3M1").style.color = "#104659";
            document.getElementById("paso3M2").style.backgroundColor = "#FFFFFF";
            document.getElementById("paso3M2").style.color = "#104659";
            document.getElementById("paso3M3").style.backgroundColor = "#FFFFFF";
            document.getElementById("paso3M3").style.color = "#104659";
            document.getElementById("edit-submit").style.display = "none";
            document.getElementById("edit-preview").style.display = "none";
        }
        if (4 == b) {
            nexValidatio7 = document.getElementById("edit-field-scenario-und-form-field-climate-scenario-und-none");
            if (void 0 != nexValidatio7) {
                b = 2;
                document.getElementById("paso2M1").style.backgroundColor = "#FFFFFF";
                document.getElementById("paso2M1").style.color = "#104659";
            }
            if (void 0 == nexValidatio7) {
                b = 1;
                document.getElementById("paso1M").style.backgroundColor = "#104659";
                document.getElementById("paso2M").style.backgroundColor = "#FFFFFF";
                document.getElementById("paso2M").style.color = "#104659";
                document.getElementById("paso2M1").style.backgroundColor = "#FFFFFF";
                document.getElementById("paso2M1").style.color = "#104659";
                document.getElementById("paso2M2").style.backgroundColor = "#FFFFFF";
                document.getElementById("paso2M2").style.color = "#104659";
            }
        }
        if (5 == b) {
            nexValidatio8 = document.getElementById("edit-field-scenario-und-form-field-energy-scenario-und-none");
            if (void 0 != nexValidatio8) {
                document.getElementById("paso2M1").style.backgroundColor = "#104659";
                document.getElementById("paso2M2").style.backgroundColor = "#FFFFFF";
                document.getElementById("paso2M2").style.color = "#104659";
            }
            if (void 0 == nexValidatio8) {
                b = 1;
                document.getElementById("paso1M").style.backgroundColor = "#104659";
                document.getElementById("paso2M").style.backgroundColor = "#FFFFFF";
                document.getElementById("paso2M").style.color = "#104659";
                document.getElementById("paso2M1").style.backgroundColor = "#FFFFFF";
                document.getElementById("paso2M1").style.color = "#104659";
                document.getElementById("paso2M2").style.backgroundColor = "#FFFFFF";
                document.getElementById("paso2M2").style.color = "#104659";
                document.getElementById("paso2M3").style.backgroundColor = "#FFFFFF";
                document.getElementById("paso2M3").style.color = "#104659";
            }
        }
        if (6 == b) {
            nexValidatio9 = document.getElementById("edit-field-integral-plan-und-form-field-hydropower-alternatives-und");
            if (void 0 != nexValidatio9) {
                document.getElementById("paso3M1").style.backgroundColor = "#FFFFFF";
                document.getElementById("paso3M1").style.color = "#104659";
                b = 3;
            }
            if (void 0 == nexValidatio9) {
                alert("aqui es");
                b = 2;
                document.getElementById("paso2M").style.backgroundColor = "#104659";
                document.getElementById("paso2M1").style.backgroundColor = "#FFFFFF";
                document.getElementById("paso2M1").style.color = "#104659";
                document.getElementById("paso2M2").style.backgroundColor = "#FFFFFF";
                document.getElementById("paso2M2").style.color = "#104659";
                document.getElementById("paso2M3").style.backgroundColor = "#FFFFFF";
                document.getElementById("paso2M3").style.color = "#104659";
                document.getElementById("paso3M").style.backgroundColor = "#FFFFFF";
                document.getElementById("paso3M").style.color = "#104659";
                document.getElementById("paso3M1").style.backgroundColor = "#FFFFFF";
                document.getElementById("paso3M1").style.color = "#104659";
            }
        }
        if (7 == b) {
            nexValidatio10 = document.getElementById("edit-field-integral-plan-und-form-field-prod-landuse-alternatives-und");
            if (void 0 != nexValidatio10) {
                document.getElementById("paso3M1").style.backgroundColor = "#104659";
                document.getElementById("paso3M2").style.backgroundColor = "#FFFFFF";
                document.getElementById("paso3M2").style.color = "#104659";
            }
            if (void 0 == nexValidatio10) {
                b = 2;
                document.getElementById("paso2M").style.backgroundColor = "#104659";
                document.getElementById("paso2M1").style.backgroundColor = "#FFFFFF";
                document.getElementById("paso2M1").style.color = "#104659";
                document.getElementById("paso2M2").style.backgroundColor = "#FFFFFF";
                document.getElementById("paso2M2").style.color = "#104659";
                document.getElementById("paso2M3").style.backgroundColor = "#FFFFFF";
                document.getElementById("paso2M3").style.color = "#104659";
                document.getElementById("paso3M").style.backgroundColor = "#FFFFFF";
                document.getElementById("paso3M").style.color = "#104659";
                document.getElementById("paso3M1").style.backgroundColor = "#FFFFFF";
                document.getElementById("paso3M1").style.color = "#104659";
                document.getElementById("paso3M2").style.backgroundColor = "#FFFFFF";
                document.getElementById("paso3M2").style.color = "#104659";
            }
        }
        if (8 == b) document.getElementById("paso3M2").style.backgroundColor = "#104659";
    }
    Drupal.multipageControl = function(e) {
        var f = this;
        var g = Drupal.theme("multipage", e);
        a.extend(f, e, g);
        this.nextLink.click(function(a) {
            b++;
            c(b);
            a.preventDefault();
            if (false != pasoVali) f.nextPage();
        });
        this.previousLink.click(function(a) {
            b--;
            d(b);
            a.preventDefault();
            f.previousPage();
        });
    };
    Drupal.multipageControl.prototype = {
        focus: function() {
            this.wrapper.show().siblings("div.multipage-pane").each(function() {
                var b = a(this).data("multipageControl");
                b.wrapper.hide();
            }).end().siblings(":hidden.multipage-active-control").val(this.wrapper.attr("id"));
            a("#active-multipage-control").remove();
            this.nextLink.after('<span id="active-multipage-control" class="element-invisible">' + Drupal.t("(active page)") + "</span>");
        },
        nextPage: function() {
            this.wrapper.next().data("multipageControl").focus();
            a("html, body").scrollTop(this.wrapper.parents(".field-group-multipage-group-wrapper").offset().top);
        },
        previousPage: function() {
            this.wrapper.prev().data("multipageControl").focus();
            a("html, body").scrollTop(this.wrapper.parents(".field-group-multipage-group-wrapper").offset().top);
        },
        tabShow: function() {
            this.item.show();
            this.item.parent().children(".multipage-control").removeClass("first").filter(":visible:first").addClass("first");
            this.wrapper.removeClass("multipage-control-hidden").show();
            this.focus();
            return this;
        },
        tabHide: function() {
            this.item.hide();
            this.item.parent().children(".multipage-control").removeClass("first").filter(":visible:first").addClass("first");
            this.wrapper.addClass("horizontal-tab-hidden").hide();
            var a = this.wrapper.siblings(".multipage-pane:not(.multipage-control-hidden):first");
            if (a.length) a.data("multipageControl").focus();
            return this;
        }
    };
    Drupal.theme.prototype.multipage = function(b) {
        var c = {};
        c.item = a('<span class="multipage-button"></span>');
        c.previousLink = a('<input type="button" class="form-submit multipage-link-previous" value="" />');
        c.previousTitle = Drupal.t("Previous page");
        c.item.append(c.previousLink.val(c.previousTitle));
        c.nextLink = a('<input type="button" id="" class="form-submit multipage-link-next" value="" />');
        c.nextTitle = Drupal.t("Next page");
        c.item.append(c.nextLink.val(c.nextTitle));
        if (!b.has_next) c.nextLink.hide();
        if (!b.has_previous) c.previousLink.hide();
        return c;
    };
    Drupal.FieldGroup = Drupal.FieldGroup || {};
    Drupal.FieldGroup.Effects = Drupal.FieldGroup.Effects || {};
    Drupal.FieldGroup.Effects.processMultipage = {
        execute: function(b, c, d) {
            if ("form" == d) {
                var e = false;
                a("div.multipage-pane").each(function(b) {
                    if (a(".error", a(this)).length) {
                        if (!e) e = a(this).data("multipageControl");
                        Drupal.FieldGroup.setGroupWithfocus(a(this));
                        a(this).data("multipageControl").focus();
                    }
                });
                if (e) e.focus();
            }
        }
    };
}(jQuery);