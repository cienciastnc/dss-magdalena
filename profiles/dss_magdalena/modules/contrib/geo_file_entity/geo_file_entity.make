core = 7.x
api = 2

projects[eck][version] = 2.0-rc3
projects[eck][subdir] = contrib

; Libraries
libraries[leaflet_zoomtogeometries][download][type] = "file"
libraries[leaflet_zoomtogeometries][download][url] = "https://github.com/NuCivic/leaflet.map.zoomToGeometries.js/zipball/master"
