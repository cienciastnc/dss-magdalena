<?php
/**
 * @file
 * CCH module class to determine context.
 */

class CCHContext {

  /**
   * Implements class __construct().
   */
  public function __construct() {
    // Assigne argument.
	  $arg = arg();
	  // Define Class.
	  $this->name = 'CCHContext Class';
	  $this->type = 0;
	  $this->id = 0;
	  $this->entity = 0;

	  // Is this a front page with no node assigned.
	  if ($arg[0] == 'node' && empty($arg[1])) {
	    $this->type = 'page';
	    $this->id = 'front';
	  }
	  // // Is this a page.
	  else if (empty($arg[1])) {
	    $this->type = 'page';
	    $this->id = $arg[0];
	  }
	  // Is this a node.
	  else if ($arg[0] == 'node' && is_numeric($arg[1])) {
	    $this->type = $arg[0];
	    $this->id = $arg[1];
	  }
	  // Is this a taxonomy page.
	  else if ($arg[0] == 'taxonomy' && is_numeric($arg[2])) {
	    $this->type = $arg[1];
	    $this->id = $arg[2];
	  }
	  // Is this a user page.
	  else if ($arg[0] == 'user' && is_numeric($arg[1])) {
	    $this->type = $arg[0];
	    $this->id = $arg[1];
	  }
	  // This is something else.
	  else {
	  	$this->type = 'page';
	    $this->id = $arg[1];
	  }
	  // Is this a page? If so check for view object.
	  if ($this->type == 'page') {
	    if(function_exists('views_get_page_view') && is_object(views_get_page_view())) {
	      $this->type = 'view';
	      $this->id = views_get_page_view()->name;
	    }
	  }

	  // Supported bundles.
  	  $types = array('node', 'user', 'term');
	  // Find entity type.
	  if (in_array($this->type, $types)) {
	  	$entity = entity_load($this->type, array($this->id));
		$entity = current($entity);
		if (isset($entity->type)) {
		  $this->entity = $entity->type;
		}
	  }
	  else if (($this->id == '<front>') OR ($this->type == 'page')) {
		$this->entity = '<front>';
	  }
  }

  public function __destruct() {}
}

class CCHSelect extends CCHContext {

  /**
   * Implements class __construct() with parent.
   * TODO: Update query to use placeholders for variables.
   *       and possibly convert to db_select.
   */
  public function __construct() {
    parent::__construct();
	  // Is this the front page.
	  if ($this->type == 'node' && (empty($this->id) OR $this->id=='<front>')) {
        $query = db_select('field_data_cch_args', 'cch');
        $query->LeftJoin('node', 'n', 'n.nid = cch.entity_id');
        $query->LeftJoin('field_data_cch_type', 'ccht', 'n.nid = ccht.entity_id');
        $query->fields('cch', array('entity_id'))
        	->condition('n.type', 'cch', '=')
        	->condition('cch.cch_args_value', 'front', '=');
        $this->results = $query->execute();
      }
      // If this is a node lets include a check for content types.
      else if ($this->type == 'node') {
        $query = db_select('field_data_cch_args', 'cch');
        $query->LeftJoin('node', 'n', 'n.nid = cch.entity_id');
        $query->LeftJoin('field_data_cch_type', 'ccht', 'n.nid = ccht.entity_id');
        $query->fields('cch', array('entity_id'))
        	->condition('n.type', 'cch', '=')
        	->condition(db_or()->condition('cch.cch_args_value', $this->id)->condition('cch.cch_args_value', $this->entity))
        	 ->condition(db_or()->condition('ccht.cch_type_value', $this->type)->condition('ccht.cch_type_value', 'content_type'));
        $this->results = $query->execute();
      }
      // Check for page, user, term and view.
      else if ($this->type == 'page' || $this->type == 'user' || $this->type == 'term' || $this->type == 'view') {
      	$query = db_select('field_data_cch_args', 'cch');
        $query->LeftJoin('node', 'n', 'n.nid = cch.entity_id');
        $query->LeftJoin('field_data_cch_type', 'ccht', 'n.nid = ccht.entity_id');
        $query->fields('cch', array('entity_id'))
        	->condition('n.type', 'cch', '=')
        	->condition('cch.cch_args_value', $this->id, '=')
        	->condition('ccht.cch_type_value', $this->type, '=');
        $this->results = $query->execute();
      }
      else {
      	$this->results = 0;
      }
  }

  public function __destruct() {}
}