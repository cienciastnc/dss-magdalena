(function ($) {
  Drupal.behaviors.advancedHelpDialog = {    
    attach: function (context) {
      $('.block-cch .custom_context_help_content').hide();
      $('.block-cch .custom_context_help_title').click(function() {
	$('.block-cch .custom_context_help_content').slideToggle("slow");
      });
    }
  };
})(jQuery);
