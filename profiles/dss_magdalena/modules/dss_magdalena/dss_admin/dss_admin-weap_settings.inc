<?php
/**
 * @file
 * WEAP Admin Settings.
 */

use Drupal\dss_magdalena\WeapController;

/**
 * WEAP Settings.
 */
function dss_admin_settings_form($form, &$form_state) {
  $form = array();
  $settings = dss_engine_weap_client_load_settings();
  $form['settings'] = array(
    '#title' => t('WebDAV Settings'),
    '#type' => 'fieldset',
    'settings' => array('#tree' => TRUE),
  );

  $form['settings']['webdav_server'] = array(
    '#type' => 'textfield',
    '#title' => t('Server'),
    '#description' => t('The name of the S-WEAP WebDAV server, e.g. "@dw_server". Make sure to include the "https://". Note that ONLY "https" protocol is allowed.', array('@dw_server' => 'https://s-weap.example.com')),
    '#default_value' => $settings['webdav_server'],
    '#required' => TRUE,
  );

  $form['settings']['webdav_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#description' => t('The S-WEAP WebDAV username.'),
    '#default_value' => $settings['webdav_username'],
    '#required' => TRUE,
    // '#disabled' => isset($overrides['webdav_username']),.
  );

  $form['settings']['webdav_password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#description' => t('The S-WEAP WebDAV password.'),
    // '#disabled' => isset($overrides['webdav_password']),.
  );

  $form['settings']['webdav_ssl_verifypeer'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enables SSL Peer Verification for Secure-WebDAV'),
    '#description' => t('When checked, the SSL connection will verify that the server has a SSL certificate issued by a trusted authority. When unchecked, the WebDAV server could use self-signed certificates.'),
    '#default_value' => $settings['webdav_ssl_verifypeer'],
    '#required' => FALSE,
  // '#disabled' => isset($overrides['webdav_ssl_verifypeer']),.
  );

  $form['settings']['webdav_enable_2fa'] = array(
    '#title' => t('Enable WebDAV Two-Factor Authentication?'),
    '#type' => 'radios',
    '#options' => array('no' => t('No'), 'yes' => t('Yes')),
    '#description' => t('When enabled, the SSL connection to the WebDAV server requires an HTTPS client certificate.'),
    '#default_value' => !empty($settings['webdav_ssl_certificate']) ? 'yes' : 'no',
    '#required' => FALSE,
    // '#disabled' => isset($overrides['webdav_ssl_certificate']),.
  );

  $form['settings']['webdav_ssl_certificate'] = array(
    '#title' => t('Full filesystem path to an HTTPS Client Certificate'),
    '#type' => 'textfield',
    '#description' => t('Provide the full filesystem path to an HTTPS Client Certificate in PEM format.'),
    '#default_value' => $settings['webdav_ssl_certificate'],
    '#states' => array(
      'visible' => array(
        ':input[name="webdav_enable_2fa"]' => array('value' => 'yes'),
      ),
      'required' => array(
        ':input[name="webdav_enable_2fa"]' => array('value' => 'yes'),
      ),
    ),
    // '#disabled' => isset($overrides['webdav_ssl_certificate']),.
  );

  $form['settings']['webdav_ssl_certificate_password'] = array(
    '#title' => t('Password for the HTTPS Client Certificate'),
    '#type' => 'password',
    '#description' => t('Provide the password to unlock the HTTPS Client Certificate.'),
    '#states' => array(
      'visible' => array(
        ':input[name="webdav_enable_2fa"]' => array('value' => 'yes'),
      ),
    ),
    // '#disabled' => isset($overrides['webdav_ssl_certificate_password']),.
  );

  $form['settings']['webdav_impex_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Import/Export (Impex) Path'),
    '#description' => t('The Impex path relative to the server URL, e.g. "/WebDAV".'),
    '#default_value' => $settings['webdav_impex_path'],
    '#required' => TRUE,
    // '#disabled' => isset($overrides['webdav_impex_path']),.
  );

  $form['#validate'][] = 'dss_admin_webdav_settings_form_validate';
  $form['#submit'][] = 'dss_admin_webdav_settings_form_submit';
  return system_settings_form($form);
}

/**
 * Implements hook_form_validate().
 */
function dss_admin_webdav_settings_form_validate($form, $form_state) {
  $config = $form_state['values'];
  $client = dss_engine_weap_client_load($config);

  // Start WEAP Controller driver and use it to verify connection settings.
  $weap = new WeapController();
  if ($weap->checkConnectionSettings() === FALSE) {
    form_set_error('webdav_server', t('WebDAV settings have been incorrectly set. Please try again.'));
  }

}

/**
 * Submit handler for WEAP Settings.
 */
function dss_admin_webdav_settings_form_submit($form, $form_state) {
  $weap = new WeapController();
  if ($weap->initialize()) {
    // If connection settings are correct, then initialize directories.
    $weap->initializeRemoteDirectories();
  }
  else {
    drupal_set_message('WEAP Configuration is correctly set, but we cannot initialize local directories. Please verify your filesystem permissions.', 'error');
  }
}
