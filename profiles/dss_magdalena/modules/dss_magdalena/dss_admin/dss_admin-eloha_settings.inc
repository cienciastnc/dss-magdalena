<?php
/**
 * @file
 * ELOHA Admin Settings.
 */

use Drupal\dss_magdalena\DSS\Entity\SimaController;
use Drupal\dss_magdalena\DSS\Entity\SimaCaseStudy;
use Drupal\dss_magdalena\DSS\Entity\File\SimaElohaQEcoTable;

/**
 * ELOHA Model Settings.
 *
 * @param array $form
 *   The form.
 * @param array $form_state
 *   The form state.
 *
 * @return array
 *   The array.
 */
function dss_admin_eloha_settings($form, $form_state) {
  // Obtain list of Case Studies.
  $controller = new SimaController();
  /** @var \Drupal\dss_magdalena\DSS\Entity\SimaCaseStudy[] $cases */
  $cases = $controller->listEntities(SimaCaseStudy::BUNDLE);
  $options = [];
  foreach ($cases as $case_study) {
    $options[$case_study->getId()] = $case_study->getDrupalEntity()->label();
  }

  // Building form.
  $form = array();
  $form['eloha'] = array(
    '#title' => t('ELOHA Settings'),
    '#type' => 'fieldset',
    'settings' => array('#tree' => TRUE),
  );
  $form['eloha']['dss_engine_reference_case_study_cid'] = array(
    '#type' => 'select',
    '#title' => t('Select Reference Case Study for ELOHA (Biota Fluvial) Model'),
    '#options' => $options,
    '#default_value' => variable_get('dss_engine_eloha_ref_case_study_cid', NULL),
    '#description' => t('Select the Case Study that will be used as a reference case for calculations of <em>ELOHA (Biota Fluvial)</em> model.'),
  );

  $form['eloha']['dashboard'] = array(
    '#title' => t('Upload ELOHA Q-Eco Table file in CSV Format'),
    '#type' => 'fieldset',
    'settings' => array('#tree' => TRUE),
  );

  $eloha_q_eco_table = SimaElohaQEcoTable::loadFromSystem();
  if (is_object($eloha_q_eco_table)) {
    $link = l($eloha_q_eco_table->getFileName(), file_create_url($eloha_q_eco_table->getUri()), array(
      'attributes' => array(
        'class' => array(
          'csv-file',
        ),
      ),
    ));
    $eloha_q_eco_table_current = t('File uploaded: !link', array(
      '!link' => $link,
    ));
  }

  $form['eloha']['dashboard']['q_eco_table_file'] = array(
    '#type' => 'value',
    '#value' => is_object($eloha_q_eco_table) ? $eloha_q_eco_table->getFileObject() : NULL,
  );

  $form['eloha']['dashboard']['q_eco_table_file_current'] = array(
    '#type' => 'markup',
    '#markup' => is_object($eloha_q_eco_table) ? $eloha_q_eco_table_current : '',
  );

  $form['eloha']['dashboard']['q_eco_table_file_upload'] = array(
    '#type' => 'file',
    '#title' => t('ELOHA Dashboard Prototype'),
    '#description' => t('Please upload the ELOHA Q-Eco Table File in CSV Format. If you have an excel spreadsheet, save it as CSV before uploading.'),
  );

  $form['eloha']['dashboard']['autodetect_line_endings'] = array(
    '#type' => 'radios',
    '#title' => t('Auto detect line endings'),
    '#description' => t('Autodetect line endings for each row in the CSV File? Select yes if the system it is running from is Mac-OSX.'),
    '#default_value' => variable_get('sima_dashboard_autodetect_line_endings', 1),
    '#options' => array(
      t('No'),
      t('Yes'),
    ),
  );

  $form['eloha']['weighted_indexes'] = array(
    '#title' => t('Weighted Parameters to calculate the global impact as a weighted sum'),
    '#type' => 'fieldset',
    'settings' => array('#tree' => TRUE),
  );

  $form['eloha']['weighted_indexes']['notes'] = array(
    '#type' => 'markup',
    '#markup' => t('<p>Calcula el indice de impacto global "v" como suma ponderada (sobre la importancia de la intensidad de impacto de sensible a grave: pesos wi modifcables por el usuario) de la numerosidad y de casos de superación de umbrales en las dos categorías de intensidad (de sensible a grave) de cada mes:</p><p> v = ws ns + wg ng, donde: </p><ul><li>ns: suma sobre los meses de la suma sobre las categorias h y sobre exceso y deficit de los n=n(tau,h,exceso/duracion) que caben en el intervalo de impacto "sensible";</li><li>analogamente para ng en el intervalo "grave".</li></ul>'),
  );

  $form['eloha']['weighted_indexes']['ws'] = array(
    '#type' => 'textfield',
    '#title' => t('Ws'),
    '#default_value' => variable_get('dss_engine_eloha_parameter_ws', 0.5),
    '#size' => 6,
    '#maxlength' => 10,
    '#description' => t('Insert the decimal value for parameter: Ws.'),
  );

  $form['eloha']['weighted_indexes']['wg'] = array(
    '#type' => 'textfield',
    '#title' => t('Wg'),
    '#default_value' => variable_get('dss_engine_eloha_parameter_wg', 0.5),
    '#size' => 6,
    '#maxlength' => 10,
    '#description' => t('Insert the decimal value for parameter: Wg.'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Configuration'),
  );

  return $form;
}

/**
 * Implements hook_form_validate().
 */
function dss_admin_eloha_settings_validate($form, &$form_state) {
  // Add some validation logic here.
  $q_eco_table = SimaElohaQEcoTable::loadFromSystem($form_state['values']['autodetect_line_endings']);
  if ($q_eco_table === FALSE) {
    form_set_error('q_eco_table_file_upload', t('Cannot create directory. Please correct Filesystem settings.'));
  }

  // Ws and Wg must be numeric values.
  $ws = $form_state['values']['ws'];
  if (!is_numeric($ws)) {
    form_set_error('ws', t('<b>Ws</b> must be a numeric decimal value. Please insert it again.'));
  }
  $wg = $form_state['values']['wg'];
  if (!is_numeric($wg)) {
    form_set_error('wg', t('<b>Wg</b> must be a numeric decimal value. Please insert it again.'));
  }

  // Validating QEco Table CSV File.
  $validators = array('file_validate_extensions' => array('csv'));
  // Save the file as a temporary file.
  $file = file_save_upload('q_eco_table_file_upload', $validators);

  if ($file === FALSE) {
    form_set_error('q_eco_table_file_upload', t('Failed to upload ELOHA Q-Eco Table File. The directory does not exist or is not writable. Please try again.'));
  }
  elseif ($file !== NULL) {
    $form_state['values']['q_eco_table_file_upload'] = $file;
  }

  // Verify that the CSV contains the required headers.
  if (!empty($file)) {
    $possible_q_eco_table = new SimaElohaQEcoTable($file);
    if ($possible_q_eco_table->verifyRequiredHeaders() === FALSE) {
      form_set_error('q_eco_table_file_upload', t('Failed to upload ELOHA Q-Eco Table File. The CSV file does not contain the required headers. Please try again.'));
    }
  }

  // If there is not CSV file, do not continue.
  if (empty($form_state['values']['q_eco_table_file']) && empty($form_state['values']['q_eco_table_file_upload'])) {
    form_set_error('q_eco_table_file_upload', t('There is no ELOHA Q-Eco Table file uploaded yet. Please upload this file in CSV format to start the import process.'));
  }
}

/**
 * Implements hook_form_submit().
 */
function dss_admin_eloha_settings_submit($form, &$form_state) {
  // Saving data values first.
  $values = $form_state['values'];

  if (!empty($values['dss_engine_reference_case_study_cid'])) {
    variable_set('dss_engine_eloha_ref_case_study_cid', $values['dss_engine_reference_case_study_cid']);
  }

  // Saving the file first.
  $file = $values['q_eco_table_file_upload'];
  if (!empty($file)) {
    // Move the temporary file into the final location.
    SimaElohaQEcoTable::saveFromTemporalFile($file);
  }

  // Saving Ws and Wg parameters.
  $ws = $form_state['values']['ws'];
  variable_set('dss_engine_eloha_parameter_ws', $ws);
  $wg = $form_state['values']['wg'];
  variable_set('dss_engine_eloha_parameter_wg', $wg);
}
