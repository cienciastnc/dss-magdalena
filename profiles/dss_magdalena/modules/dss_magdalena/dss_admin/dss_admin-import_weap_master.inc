<?php
/**
 * @file
 * All logic related to importing a WEAP Master File.
 */

use Drupal\dss_magdalena\DSS\SimaImportWeapDataboxCSV as SimaImportWeapDataboxCSV;
use Drupal\dss_magdalena\DSS\Utils\SimaImportProjects;

/**
 * Form to input the Master File.
 *
 * @return array
 *   The form array.
 */
function dss_admin_import_weap_master_form($form, &$form_state) {
  $form = array();
  $form['weap_master'] = array(
    '#title' => t('WEAP Master File Import'),
    '#type' => 'fieldset',
    'settings' => array('#tree' => TRUE),
  );

  if ($weap_masterfile = SimaImportWeapDataboxCSV::getWeapMasterFile()) {
    $link = l($weap_masterfile->filename, file_create_url($weap_masterfile->uri), array(
      'attributes' => array(
        'class' => array(
          'csv-file',
        ),
      ),
    ));
    $weap_masterfile_current = t('File uploaded: !link', array(
      '!link' => $link,
    ));
  }

  $form['weap_master']['weap_master_file'] = array(
    '#type' => 'value',
    '#value' => $weap_masterfile,
  );

  $form['weap_master']['weap_master_file_current'] = array(
    '#type' => 'markup',
    '#markup' => $weap_masterfile ? $weap_masterfile_current : '',
  );

  $form['weap_master']['weap_master_file_upload'] = array(
    '#type' => 'file',
    '#title' => t('WEAP CSV Master File'),
    '#description' => t('Please upload the WEAP Master File in CSV format. Please produce the Excel version of this file by doing the "Export Expressions to Excel" in WEAP and then convert that file to CSV format.'),
  );

  $form['weap_master']['autodetect_line_endings'] = array(
    '#type' => 'radios',
    '#title' => t('Auto detect line endings'),
    '#description' => t('Autodetect line endings for each row in the CSV File? Select yes if the system it is running from is Mac-OSX.'),
    '#default_value' => variable_get('weap_master_autodetect_line_endings', 1),
    '#options' => array(
      t('No'),
      t('Yes'),
    ),
  );

  $form['weap_master']['model_version'] = array(
    '#type' => 'textfield',
    '#title' => t('WEAP Model Version'),
    '#default_value' => variable_get('weap_master_model_version', 'MAGDALENA_V2.1.6.1'),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );

  $form['weap_master']['create_resources_databox'] = array(
    '#type' => 'radios',
    '#title' => t('Import CSV Files to Dataset -> Resources'),
    '#description' => t('Should we import the CSV Databox files into the DSS Variables -> Databoxes? Notice that this will <b>overwrite</b> the Default Case Study.'),
    '#default_value' => 1,
    '#options' => array(
      t('No'),
      t('Yes'),
    ),
  );

  $form['weap_master']['create_hidroplants_and_dams'] = array(
    '#type' => 'radios',
    '#title' => t('Import Hidroplants and Dams from Master File'),
    '#description' => t('Should we import the Hidroplants and Dams from the Master File? Notice that this will <b>overwrite</b> the Default Case Study.'),
    '#default_value' => 1,
    '#options' => array(
      t('No'),
      t('Yes'),
    ),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import WEAP Master File'),
  );

  return $form;
}

/**
 * Implements hook_form_validate().
 */
function dss_admin_import_weap_master_form_validate($form, &$form_state) {
  // Add some validation logic here.
  $sima_master_csv = new SimaImportWeapDataboxCSV($form_state['values']['autodetect_line_endings']);
  if ($sima_master_csv->initializeDirectory() === FALSE) {
    form_set_error('weap_master_file_upload', t('Cannot create directory. Please correct Filesystem settings.'));
  }

  // Validating WEAP Master CSV File.
  $validators = array('file_validate_extensions' => array('csv'));
  // Save the file as a temporary file.
  $file = file_save_upload('weap_master_file_upload', $validators);

  if ($file === FALSE) {
    form_set_error('weap_master_file_upload', t('Failed to upload Master File. The directory does not exist or is not writable. Please try again.'));
  }
  elseif ($file !== NULL) {
    $form_state['values']['weap_master_file_upload'] = $file;
  }

  // If there is not CSV file, do not continue.
  if (empty($form_state['values']['weap_master_file']) && empty($form_state['values']['weap_master_file_upload'])) {
    form_set_error('weap_master_file_upload', t('There is no WEAP Master file uploaded yet. Please upload this file in CSV format to start import process.'));
  }
}

/**
 * Implements hook_form_submit().
 */
function dss_admin_import_weap_master_form_submit($form, &$form_state) {
  // Saving data values first.
  $values = $form_state['values'];

  // Saving the file first.
  if ($file = $values['weap_master_file_upload']) {
    // Move the temporary file into the final location.
    if ($file = file_move($file, SimaImportWeapDataboxCSV::WEAP_MASTER_DIR_FILES, FILE_EXISTS_REPLACE)) {
      $file->status = FILE_STATUS_PERMANENT;
      $file = file_save($file);

      // Now that the file is properly saved. Notify the class to save it.
      SimaImportWeapDataboxCSV::saveWeapMasterFile($file);
    }
  }
  variable_set('weap_master_autodetect_line_endings', $values['autodetect_line_endings']);
  variable_set('weap_master_model_version', $values['model_version']);

  // Implementing Batch API.
  $batch = array(
    'title' => t('Importing Data from WEAP Master File.'),
    'operations' => array(
      array('dss_admin_master_key_assumptions_batch_process', array($values['autodetect_line_endings'])),
      array('dss_admin_master_variable_import_batch_process', array($values['autodetect_line_endings'])),
    ),
    'finished' => 'dss_admin_master_input_import_finished',
    'init_message' => t('Starting Variable Import from WEAP Master File to SIMA DSS'),
    'progress_message' => t('Processed @current tasks out of @total.'),
    'error_message' => t('Error importing Variable from the WEAP Master File.'),
    'file' => drupal_get_path('module', 'dss_admin') . '/dss_admin-import_weap_master.inc',
  );

  if ((bool) $values['create_resources_databox']) {
    $batch['operations'][] = array(
      'dss_admin_databox_csv_import_resources_batch_process',
      array($values['autodetect_line_endings'], $values['model_version']),
    );
  }

  if ((bool) $values['create_hidroplants_and_dams']) {
    $batch['operations'][] = array(
      'dss_admin_databox_csv_import_hidroplants_and_dams_batch_process',
      array($values['autodetect_line_endings']),
    );
  }

  batch_set($batch);
}

/**
 * Processes the Key Assumptions.
 *
 * Obtains the Key Assumptions from the master file and saves them in a
 * variable.
 *
 * @param int $autodetect_line_endings
 *   Pass 1 to autodetect line endings, 0 otherwise.
 * @param array $context
 *   Context Array.
 */
function dss_admin_master_key_assumptions_batch_process($autodetect_line_endings, &$context) {
  $context['message'] = t('Obtaining the Key Assumptions from the WEAP Master File.');
  $sima_master_csv = new SimaImportWeapDataboxCSV($autodetect_line_endings);
  $sima_master_csv->extractHeader();
  $sima_master_csv->extractKeyAssumptions();
}

/**
 * Generates a Databox CSV file for a variable in the CSV WEAP Master File.
 *
 * @param int $autodetect_line_endings
 *   Pass 1 to autodetect line endings, 0 otherwise.
 * @param array $context
 *   Context Array.
 */
function dss_admin_master_variable_import_batch_process($autodetect_line_endings, &$context) {
  $sima_master_csv = new SimaImportWeapDataboxCSV($autodetect_line_endings);
  $variables = $sima_master_csv->getVariables();

  // Initializing the sandbox.
  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current_variable'] = 0;
    $context['sandbox']['max'] = count($variables);
  }

  // Processing the CSV of a variable.
  $variable = $variables[$context['sandbox']['current_variable']];
  $success = $sima_master_csv->setKeyAssumptions()->buildDataboxCsvFileForVariable($variable);

  // Store some result for post-processing in the finished callback.
  $context['results']['weap_to_csv'][] = array(
    'variable' => $variable,
    'success' => $success,
  );

  // Updating progress information.
  $context['sandbox']['progress']++;
  $context['sandbox']['current_variable']++;
  $context['message'] = t('Now processing DSS variable "%var" (WEAP Variable = "%weap_var").', array(
    '%var' => $variable,
    '%weap_var' => $sima_master_csv->getWeapVariableName($variable),
  ));

  // Inform the batch engine that we are not finished,
  // and provide an estimation of the completion level we reached.
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Imports the CSV Databox files generated for each Variable into Resources.
 *
 * @param int $autodetect_line_endings
 *   Pass 1 to autodetect line endings, 0 otherwise.
 * @param string $weap_model_version
 *   The WEAP Model version.
 * @param array $context
 *   Context Array.
 */
function dss_admin_databox_csv_import_resources_batch_process($autodetect_line_endings, $weap_model_version, &$context) {
  $sima_master_csv = new SimaImportWeapDataboxCSV($autodetect_line_endings);
  $variables = $sima_master_csv->getVariables();
  // Initializing the sandbox.
  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current_variable'] = 0;
    $context['sandbox']['max'] = count($variables);
  }

  // Processing the CSV of a variable.
  $variable = $variables[$context['sandbox']['current_variable']];
  $resource = $sima_master_csv->importCsvDataboxToResources($variable, $weap_model_version);

  // Store some result for post-processing in the finished callback.
  $context['results']['csv_to_resources'][] = array(
    'variable' => $variable,
    'success' => $resource ? TRUE : FALSE,
  );

  // Updating progress information.
  $context['sandbox']['progress']++;
  $context['sandbox']['current_variable']++;
  $context['message'] = t('Importing Databox CSV File for DSS Variable "%var" (WEAP Variable = "%weap_var") into Resource "%resource".', array(
    '%var' => $variable,
    '%resource' => $resource ? $resource->title : 'None',
    '%weap_var' => $sima_master_csv->getWeapVariableName($variable),
  ));

  // Inform the batch engine that we are not finished,
  // and provide an estimation of the completion level we reached.
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Imports Hidroplants and Dams into the DSS.
 *
 * @param int $autodetect_line_endings
 *   Pass 1 to autodetect line endings, 0 otherwise.
 * @param array $context
 *   Context Array.
 */
function dss_admin_databox_csv_import_hidroplants_and_dams_batch_process($autodetect_line_endings, &$context) {
  $sima_master_csv = new SimaImportProjects(SimaImportProjects::TYPE_HYDROPOWER_PLANT_OR_DAM, $autodetect_line_endings);
  $projects = $sima_master_csv->getListOfItems();

  // Generate the joined File.
  $sima_master_csv->generateProjectCsvFile();

  // Initializing the sandbox.
  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current_project'] = 0;
    $context['sandbox']['max'] = count($projects);
  }

  // Processing a single Hydroplant.
  $project = $projects[$context['sandbox']['current_project']];
  $hidroplant = $sima_master_csv->importFichaProject($project);

  // Store some result for post-processing in the finished callback.
  $context['results']['hidroplants'][] = array(
    'variable' => $hidroplant,
    'success' => $hidroplant ? TRUE : FALSE,
  );

  // Updating progress information.
  $full_name = implode('\\', $project);
  $context['sandbox']['progress']++;
  $context['sandbox']['current_project']++;
  $context['message'] = t('Importing Hidroplant "%project" into hidroplant "%project".', array(
    '%project' => $full_name,
    '%resource' => $hidroplant ? $hidroplant->getDrupalEntity()->title : 'None',
  ));

  // Inform the batch engine that we are not finished,
  // and provide an estimation of the completion level we reached.
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Old Batch process.
 *
 * @deprecated
 */
function dss_admin_databox_csv_import_hidroplants_and_dams_batch_process_old($autodetect_line_endings, &$context) {
  $sima_master_csv = new SimaImportWeapDataboxCSV($autodetect_line_endings);
  $projects = $sima_master_csv->getNumberOfPowerPlantsAndDams();
  // Initializing the sandbox.
  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current_project'] = 0;
    $context['sandbox']['max'] = count($projects);
  }

  // Processing a single Hydroplant.
  $project = $projects[$context['sandbox']['current_project']];
  $hidroplant = $sima_master_csv->importHydroPowerPlantOrDam($project);

  // Store some result for post-processing in the finished callback.
  $context['results']['hidroplants'][] = array(
    'variable' => $hidroplant,
    'success' => $hidroplant ? TRUE : FALSE,
  );

  // Updating progress information.
  $full_name = implode('\\', $project);
  $context['sandbox']['progress']++;
  $context['sandbox']['current_project']++;
  $context['message'] = t('Importing Hidroplant "%project" into hidroplant "%project".', array(
    '%project' => $full_name,
    '%resource' => $hidroplant ? $hidroplant->title : 'None',
  ));

  // Inform the batch engine that we are not finished,
  // and provide an estimation of the completion level we reached.
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Batch 'finished' callback.
 */
function dss_admin_master_input_import_finished($success, $results, $operations) {
  if ($success) {
    // Counting all the variables that were processed successfully:
    $results_ok = array();
    $results_error = array();
    $results['weap_to_csv'] = isset($results['weap_to_csv']) ? $results['weap_to_csv'] : [];
    foreach ($results['weap_to_csv'] as $key => $result) {
      if ($result['success']) {
        $results_ok[] = t('Generating CSV FIle for variable "%var": Successful', array(
          '%var' => $result['variable'],
        ));
      }
      else {
        $results_error[] = t('Generating CSV FIle for variable "%var": Error.', array(
          '%var' => $result['variable'],
        ));
      }
    }

    // Here we do something meaningful with the results.
    if (count($results_ok) > 0) {
      $message = t('@count DSS/WEAP variables successfully processed.', array('@count' => count($results_ok)));
      $message .= theme('item_list', array('items' => $results_ok));
      drupal_set_message($message, 'status');
    }
    if (count($results_error) > 0) {
      $message = t('@count DSS/WEAP variables unsuccessfully processed.', array('@count' => count($results_error)));
      $message .= theme('item_list', array('items' => $results_error));
      drupal_set_message($message, 'error');
    }
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array(
      '%error_operation' => $error_operation[0],
      '@arguments' => print_r($error_operation[1], TRUE),
    ));
    drupal_set_message($message, 'error');
  }
}
