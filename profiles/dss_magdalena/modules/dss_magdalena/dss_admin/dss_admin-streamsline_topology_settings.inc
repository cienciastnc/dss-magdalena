<?php
/**
 * @file
 * ELOHA Admin Settings.
 */

use Drupal\dss_magdalena\DSS\Entity\SimaController;
use Drupal\dss_magdalena\DSS\Entity\SimaCaseStudy;
use Drupal\dss_magdalena\DSS\Entity\File\SimaStreamslineTopologyFile;

/**
 * ELOHA Model Settings.
 *
 * @param array $form
 *   The form.
 * @param array $form_state
 *   The form state.
 *
 * @return array
 *   The array.
 */
function dss_admin_streamsline_topology_settings($form, $form_state) {
  // Obtain list of Case Studies.
  $controller = new SimaController();
  /** @var \Drupal\dss_magdalena\DSS\Entity\SimaCaseStudy[] $cases */
  $cases = $controller->listEntities(SimaCaseStudy::BUNDLE);
  $options = [];
  foreach ($cases as $case_study) {
    $options[$case_study->getId()] = $case_study->getDrupalEntity()->label();
  }

  // Building form.
  $form = array();

  $form['streamsline'] = array(
    '#title' => t('Upload Sima Streamsline Topology File in CSV Format'),
    '#type' => 'fieldset',
    'settings' => array('#tree' => TRUE),
  );

  $streamsline_topology_file = SimaStreamslineTopologyFile::loadFromSystem();
  if (is_object($streamsline_topology_file)) {
    $link = l($streamsline_topology_file->getFileName(), file_create_url($streamsline_topology_file->getUri()), array(
      'attributes' => array(
        'class' => array(
          'csv-file',
        ),
      ),
    ));
    $streamsline_topology_file_current = t('File uploaded: !link', array(
      '!link' => $link,
    ));
  }

  $form['streamsline']['streamsline_topology_file'] = array(
    '#type' => 'value',
    '#value' => is_object($streamsline_topology_file) ? $streamsline_topology_file->getFileObject() : NULL,
  );

  $form['streamsline']['streamsline_topology_file_current'] = array(
    '#type' => 'markup',
    '#markup' => is_object($streamsline_topology_file) ? $streamsline_topology_file_current : '',
  );

  $form['streamsline']['streamsline_topology_file_upload'] = array(
    '#type' => 'file',
    '#title' => t('Sima Streamsline Topology File'),
    '#description' => t('Please upload the Sima Streamsline Topology File in CSV Format. If you have an excel spreadsheet, save it as CSV before uploading.'),
  );

  $form['streamsline']['autodetect_line_endings'] = array(
    '#type' => 'radios',
    '#title' => t('Auto detect line endings'),
    '#description' => t('Autodetect line endings for each row in the CSV File? Select yes if the system it is running from is Mac-OSX.'),
    '#default_value' => variable_get('sima_dashboard_autodetect_line_endings', 1),
    '#options' => array(
      t('No'),
      t('Yes'),
    ),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Upload File'),
  );

  return $form;
}

/**
 * Implements hook_form_validate().
 */
function dss_admin_streamsline_topology_settings_validate($form, &$form_state) {
  // Add some validation logic here.
  $streamsline_topology_file = SimaStreamslineTopologyFile::loadFromSystem($form_state['values']['autodetect_line_endings']);
  if ($streamsline_topology_file === FALSE) {
    form_set_error('streamsline_topology_file_upload', t('Cannot create directory. Please correct Filesystem settings.'));
  }

  // Validating QEco Table CSV File.
  $validators = array('file_validate_extensions' => array('csv'));
  // Save the file as a temporary file.
  $file = file_save_upload('streamsline_topology_file_upload', $validators);

  if ($file === FALSE) {
    form_set_error('streamsline_topology_file_upload', t('Failed to upload StreamsLine Topology File. The directory does not exist or is not writable. Please try again.'));
  }
  elseif ($file !== NULL) {
    $form_state['values']['streamsline_topology_file_upload'] = $file;
  }

  // Verify that the CSV contains the required headers.
  if (!empty($file)) {
    $possible_streamsline_topology_file = new SimaStreamslineTopologyFile($file);
    if ($possible_streamsline_topology_file->verifyRequiredHeaders() === FALSE) {
      form_set_error('streamsline_topology_file_upload', t('Failed to upload StreamsLine Topology File. The CSV file does not contain the required headers. Please try again.'));
    }
  }

  // If there is not CSV file, do not continue.
  if (empty($form_state['values']['streamsline_topology_file']) && empty($form_state['values']['streamsline_topology_file_upload'])) {
    form_set_error('streamsline_topology_file_upload', t('There is no StreamsLine Topology file uploaded yet. Please upload this file in CSV format to start the import process.'));
  }
}

/**
 * Implements hook_form_submit().
 */
function dss_admin_streamsline_topology_settings_submit($form, &$form_state) {
  // Saving data values first.
  $values = $form_state['values'];

  // Saving the file first.
  $file = $values['streamsline_topology_file_upload'];
  if (!empty($file)) {
    // Move the temporary file into the final location.
    SimaStreamslineTopologyFile::saveFromTemporalFile($file);
  }
}
