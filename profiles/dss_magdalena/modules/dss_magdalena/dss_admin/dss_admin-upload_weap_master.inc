<?php
/**
 * @file
 * All logic related to importing a WEAP Master File.
 */

use Drupal\dss_magdalena\DSS\SimaWeapMasterFileExcel;

/**
 * Form to input the Master File.
 *
 * @return array
 *   The form array.
 */
function dss_admin_upload_weap_master_form($form, &$form_state) {
  $form = array();
  $form['weap_master'] = array(
    '#title' => t('Upload WEAP Master File'),
    '#type' => 'fieldset',
    'settings' => array('#tree' => TRUE),
  );

  $weap_masterfile = new SimaWeapMasterFileExcel();
  if ($weap_masterfile = $weap_masterfile->getWeapMasterFile()) {
    $link = l($weap_masterfile->filename, file_create_url($weap_masterfile->uri), array(
      'attributes' => array(
        'class' => array(
          'xls-file',
        ),
      ),
    ));
    $weap_masterfile_current = t('File uploaded: !link', array(
      '!link' => $link,
    ));
  }

  $form['weap_master']['weap_master_file'] = array(
    '#type' => 'value',
    '#value' => $weap_masterfile,
  );

  $form['weap_master']['weap_master_file_current'] = array(
    '#type' => 'markup',
    '#markup' => $weap_masterfile ? $weap_masterfile_current : '',
  );

  $form['weap_master']['weap_master_file_upload'] = array(
    '#type' => 'file',
    '#title' => t('WEAP Master File'),
    '#description' => t('Please upload the WEAP Master File. Please produce the Excel version of this file by doing the "Export Expressions to Excel" in WEAP.'),
  );

  $form['weap_master']['model_version'] = array(
    '#type' => 'textfield',
    '#title' => t('WEAP Model Version'),
    '#default_value' => variable_get('weap_master_model_version', 'MAGDALENA_V2.1.6.1'),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import WEAP Master File'),
  );

  return $form;
}

/**
 * Implements hook_form_validate().
 */
function dss_admin_upload_weap_master_form_validate($form, &$form_state) {
  // Add some validation logic here.
  $sima_master = new SimaWeapMasterFileExcel();
  if ($sima_master === FALSE) {
    form_set_error('weap_master_file_upload', t('Cannot create directory. Please correct Filesystem settings.'));
  }

  // Validating WEAP Master CSV File.
  $validators = array('file_validate_extensions' => array('xlsx xls'));
  // Save the file as a temporary file.
  $file = file_save_upload('weap_master_file_upload', $validators);

  if ($file === FALSE) {
    form_set_error('weap_master_file_upload', t('Failed to upload Master File. The directory does not exist or is not writable. Please try again.'));
  }
  elseif ($file !== NULL) {
    $form_state['values']['weap_master_file_upload'] = $file;
  }

  // If there is not CSV file, do not continue.
  if (empty($form_state['values']['weap_master_file']) && empty($form_state['values']['weap_master_file_upload'])) {
    form_set_error('weap_master_file_upload', t('There is no WEAP Master file uploaded yet. Please upload this file in Excel format exactly as it was exported from WEAP without any changes.'));
  }
}

/**
 * Implements hook_form_submit().
 */
function dss_admin_upload_weap_master_form_submit($form, &$form_state) {
  // Saving data values first.
  $values = $form_state['values'];

  // Saving the file first.
  if ($file = $values['weap_master_file_upload']) {
    // Move the temporary file into the final location.
    if ($file = file_move($file, SimaWeapMasterFileExcel::WEAP_MASTER_DIR, FILE_EXISTS_REPLACE)) {
      $file->status = FILE_STATUS_PERMANENT;
      $file = file_save($file);

      // Now that the file is properly saved. Notify the class to save it.
      $weap_masterfile = new SimaWeapMasterFileExcel();
      $weap_masterfile->saveWeapMasterFile($file, TRUE);
    }
  }
  if (!empty($values['model_version'])) {
    variable_set('weap_master_model_version', $values['model_version']);
  }
}
