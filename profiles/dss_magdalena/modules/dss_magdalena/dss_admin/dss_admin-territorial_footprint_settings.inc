<?php
/**
 * @file
 * Territorial Footprint Admin Settings.
 */

use Drupal\dss_magdalena\DSS\Entity\SimaController;
use Drupal\dss_magdalena\DSS\Entity\SimaCaseStudy;
use Drupal\dss_magdalena\DSS\Entity\File\SimaTerritorialFootprintFile;

/**
 * Territorial Footprint Model Settings.
 *
 * @param array $form
 *   The form.
 * @param array $form_state
 *   The form state.
 *
 * @return array
 *   The array.
 */
function dss_admin_territorial_footprint_settings($form, $form_state) {
  // Obtain list of Case Studies.
  $controller = new SimaController();
  /** @var \Drupal\dss_magdalena\DSS\Entity\SimaCaseStudy[] $cases */
  $cases = $controller->listEntities(SimaCaseStudy::BUNDLE);
  $options = [];
  foreach ($cases as $case_study) {
    $options[$case_study->getId()] = $case_study->getDrupalEntity()->label();
  }

  // Building form.
  $form = array();

  $form['territorial_footprint'] = array(
    '#title' => t('Upload Sima Footprint Analysis - Projected Reservoirs File in CSV Format'),
    '#type' => 'fieldset',
    'settings' => array('#tree' => TRUE),
  );

  $territorial_footprint_file = SimaTerritorialFootprintFile::loadFromSystem();
  if (is_object($territorial_footprint_file)) {
    $link = l($territorial_footprint_file->getFileName(), file_create_url($territorial_footprint_file->getUri()), array(
      'attributes' => array(
        'class' => array(
          'csv-file',
        ),
      ),
    ));
    $territorial_footprint_file_current = t('File uploaded: !link', array(
      '!link' => $link,
    ));
  }

  $form['territorial_footprint']['territorial_footprint_file'] = array(
    '#type' => 'value',
    '#value' => is_object($territorial_footprint_file) ? $territorial_footprint_file->getFileObject() : NULL,
  );

  $form['territorial_footprint']['territorial_footprint_file_current'] = array(
    '#type' => 'markup',
    '#markup' => is_object($territorial_footprint_file) ? $territorial_footprint_file_current : '',
  );

  $form['territorial_footprint']['territorial_footprint_file_upload'] = array(
    '#type' => 'file',
    '#title' => t('Sima Footprint Analysis - Projected Reservoirs File'),
    '#description' => t('Please upload the Sima Footprint Analysis - Projected Reservoirs File in CSV Format. If you have an excel spreadsheet, save it as CSV before uploading.'),
  );

  $form['territorial_footprint']['autodetect_line_endings'] = array(
    '#type' => 'radios',
    '#title' => t('Auto detect line endings'),
    '#description' => t('Autodetect line endings for each row in the CSV File? Select yes if the system it is running from is Mac-OSX.'),
    '#default_value' => variable_get('sima_dashboard_autodetect_line_endings', 1),
    '#options' => array(
      t('No'),
      t('Yes'),
    ),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Upload File'),
  );

  return $form;
}

/**
 * Implements hook_form_validate().
 */
function dss_admin_territorial_footprint_settings_validate($form, &$form_state) {
  // Add some validation logic here.
  $territorial_footprint_file = SimaTerritorialFootprintFile::loadFromSystem($form_state['values']['autodetect_line_endings']);
  if ($territorial_footprint_file === FALSE) {
    form_set_error('territorial_footprint_file_upload', t('Cannot create directory. Please correct Filesystem settings.'));
  }

  // Validating QEco Table CSV File.
  $validators = array('file_validate_extensions' => array('csv'));
  // Save the file as a temporary file.
  $file = file_save_upload('territorial_footprint_file_upload', $validators);

  if ($file === FALSE) {
    form_set_error('territorial_footprint_file_upload', t('Failed to upload Sima Footprint Analysis - Projected Reservoirs File. The directory does not exist or is not writable. Please try again.'));
  }
  elseif ($file !== NULL) {
    $form_state['values']['territorial_footprint_file_upload'] = $file;
  }

  // Verify that the CSV contains the required headers.
  if (!empty($file)) {
    $possible_territorial_footprint_file = new SimaTerritorialFootprintFile($file);
    if ($possible_territorial_footprint_file->verifyRequiredHeaders() === FALSE) {
      form_set_error('territorial_footprint_file_upload', t('Failed to upload Sima Footprint Analysis - Projected Reservoirs File. The CSV file does not contain the required headers. Please try again.'));
    }
  }

  // If there is not CSV file, do not continue.
  if (empty($form_state['values']['territorial_footprint_file']) && empty($form_state['values']['territorial_footprint_file_upload'])) {
    form_set_error('territorial_footprint_file_upload', t('There is no Sima Footprint Analysis - Projected Reservoirs File uploaded yet. Please upload this file in CSV format to start the import process.'));
  }
}

/**
 * Implements hook_form_submit().
 */
function dss_admin_territorial_footprint_settings_submit($form, &$form_state) {
  // Saving data values first.
  $values = $form_state['values'];

  // Saving the file first.
  $file = $values['territorial_footprint_file_upload'];
  if (!empty($file)) {
    // Move the temporary file into the final location.
    SimaTerritorialFootprintFile::saveFromTemporalFile($file);
  }
}
