<?php
/**
 * @file
 * Footprint  Layers of TIER1 in SIMA
 */

namespace Drupal\dss_tier1\DSS\Entity;

/**
 * Footprints Layers
 *
 * Class SimaFootprintLayer.
 *
 * @package Drupal\dss_tier1\DSS\Entity;
 */
class SimaFootprintLayer extends EntityNodeAbstract {

  const BUNDLE = 'footprint_layers';
  const NAME='title';
  const MATLAB_ID='field_code';
  const UNITY_MEASURE='field_unidad_de_medida';
  
  /**
   * Loads a SimaFootprintLayer from a Drupal entity or a Drupal entity ID.
   *
   * @param mixed $layer
   *   The nid or loaded object.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaFootprintLayer|FALSE
   *   The loaded SimaProject object if exists, FALSE otherwise.
   */
  public static function load($layer) {
    if ($layer instanceof SimaFootprintLayer) {
      return $layer;
    }
    else {
      if ($entity = parent::load($layer)) {
        return ($entity->getDrupalBundle() == self::BUNDLE) ? $entity : FALSE;
      }
      return FALSE;
    }
  }

  /**
   * Returns a new Project.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaProject
   *   The loaded SimaProject.
   */
  static public function newLayer() {
    global $user;
    // Create the new Project Node.
    return self::newEntity('node', self::BUNDLE, $user->uid);
  }
  
  public function getLayerId() {
    return $this->getId();
  }
  
  public function getLayer() {
    return $this->getDrupalEntity();
  }

  public function getName(){
    return $this->get(self::NAME);
  }

  public function getMatlabId(){
    return $this->get(self::MATLAB_ID);
  }

  public function getUnityMeasure(){
    return $this->get(self::UNITY_MEASURE);
  }

  public function getCoordX(){
    return $this->get(self::X_COORD);
  }

  public function getCoordY(){
    return $this->get(self::Y_COORD);
  }

  public function getArcId(){
    return $this->get(self::ARC_ID);
  }

  public function getOperationYear(){
    return $this->get(self::OPERATION_YEAR);
  }

  public function getSedimentRetentionRate(){
    return $this->get(self::RETENTION_RATE);
  }

  public function getReservoirHight(){
    return $this->get(self::HEIHGT_DAM);
  }

  public function getDammingVolume(){
    return $this->get(self::DAMMING_VOLUME);
  }

  public function getInstalledPower(){
    return $this->get(self::INSTALLED_POWER);
  }
 
}
