<?php

/**
 * @file
 * Execution of TIER1 SIMA module
 */

namespace Drupal\dss_tier1\DSS\Entity;

use EntityFieldQuery;

/**
 * Narratives of TIER1 SIMA module
 *
 * Class SimaExecution.
 *
 * @package Drupal\dss_magdalena\DSS\Entity
 */
class SimaExecution extends EntityNodeAbstract
{

  const BUNDLE = 'execution';
  const TITLE = 'title';
  const DESCRIPTION = 'body';
  const NARRATIVES = 'field_narratives';
  const PRECIPITATION = 'field_precipitation';
  const TEMPERATURE = 'field_temperature';
  const EVAPORATION = 'field_evaporation';
  const STREAMFLOW = 'field_streamflow';
  const SEDIMENTS = 'field_sediments';
  const MODELS = 'field_models';
  const FOOTPRINT = 'field_footprint_model';
  const FOOTPRINT_LAYERS = 'field_footprint_areas';
  const OUTPUTS = 'field_salidas';

  /**
   * Loads a TIER1 Execution from a Drupal entity or a Drupal entity ID.
   *
   * @param mixed $project
   *   The nid or loaded object.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaProject|FALSE
   *   The loaded SimaProject object if exists, FALSE otherwise.
   */
  public static function load($execution)
  {
    if ($execution instanceof SimaExecution) {
      return $execution;
    } else {
      if ($entity = parent::load($execution)) {
        return ($entity->getDrupalBundle() == self::BUNDLE) ? $entity : FALSE;
      }
      return FALSE;
    }
  }

  public function getTitle()
  {
    return $this->get(self::TITLE);
  }
  public function getExecutionId()
  {
    return $this->getId();
  }
  public function getExecution()
  {
    return $this->getDrupalEntity();
  }

  public function getDescription()
  {
    return $this->get(self::DESCRIPTION);
  }

  public function getNarratives()
  {
    return $this->get(self::NARRATIVES);
  }

  public function getPrecipitation()
  {
    return $this->get(self::PRECIPITATION);
  }

  public function getTemperature()
  {
    return $this->get(self::TEMPERATURE);
  }

  public function getEvotranspiration()
  {
    return $this->get(self::EVAPORATION);
  }

  public function getStreamflow()
  {
    return $this->get(self::STREAMFLOW);
  }

  public function getSediments()
  {
    return $this->get(self::SEDIMENTS);
  }

  public function getModels()
  {
    return $this->get(self::MODELS);
  }

  public function getFootprintStatus()
  {
    return $this->get(self::FOOTPRINT);
  }

  public function getFootprintLayers()
  {
    return $this->get(self::FOOTPRINT_LAYERS);
  }

  public function getOutputs()
  {
    return $this->get(self::OUTPUTS);
  }


  /**
   * Returns a new Execution.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaProject
   *   The loaded SimaProject.
   */
  static public function newExecution()
  {
    global $user;
    // Create the new Project Node.
    return self::newEntity('node', self::BUNDLE, $user->uid);
  }
}
