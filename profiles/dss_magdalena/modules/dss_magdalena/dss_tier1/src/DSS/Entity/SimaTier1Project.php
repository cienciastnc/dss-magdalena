<?php
/**
 * @file
 * Projects of TIER1 in SIMA
 */

namespace Drupal\dss_tier1\DSS\Entity;

/**
 * Projects in TIER1
 *
 * Class SimaTier1Project.
 *
 * @package Drupal\dss_tier1\DSS\Entity;
 */
class SimaTier1Project extends EntityNodeAbstract {

  const BUNDLE = 'tier1_project';
  const NAME='title';
  const DESCRIPTION='field_description';
  const PROJECT_TYPE='field_project_type';
  const X_COORD='field_x_coord';
  const Y_COORD='field_y_coord';
  const ARC_ID='field_arc_id';
  const OPERATION_YEAR='field_operation_year';
  const RETENTION_RATE='field_sediment_retention';
  const HEIHGT_DAM='field_reservoir_height';
  const DAMMING_VOLUME='field_reservoir_total_volume';
  const INSTALLED_POWER='field_installed_power';

  /**
   * Loads a SimaTier1Project from a Drupal entity or a Drupal entity ID.
   *
   * @param mixed $project
   *   The nid or loaded object.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaProject|FALSE
   *   The loaded SimaProject object if exists, FALSE otherwise.
   */
  public static function load($project) {
    if ($project instanceof SimaTier1Project) {
      return $project;
    }
    else {
      if ($entity = parent::load($project)) {
        return ($entity->getDrupalBundle() == self::BUNDLE) ? $entity : FALSE;
      }
      return FALSE;
    }
  }

  /**
   * Returns a new Project.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaProject
   *   The loaded SimaProject.
   */
  static public function newProject() {
    global $user;
    // Create the new Project Node.
    return self::newEntity('node', self::BUNDLE, $user->uid);
  }
  
  public function getProjectId() {
    return $this->getId();
  }
  
  public function getProject() {
    return $this->getDrupalEntity();
  }

  public function getName(){
    return $this->get(self::NAME);
  }

  public function getDescription(){
    return $this->get(self::DESCRIPTION);
  }

  public function getType(){
    return $this->get(self::PROJECT_TYPE);
  }

  public function getCoordX(){
    return $this->get(self::X_COORD);
  }

  public function getCoordY(){
    return $this->get(self::Y_COORD);
  }

  public function getArcId(){
    return $this->get(self::ARC_ID);
  }

  public function getOperationYear(){
    return $this->get(self::OPERATION_YEAR);
  }

  public function getSedimentRetentionRate(){
    return $this->get(self::RETENTION_RATE);
  }

  public function getReservoirHight(){
    return $this->get(self::HEIHGT_DAM);
  }

  public function getDammingVolume(){
    return $this->get(self::DAMMING_VOLUME);
  }

  public function getInstalledPower(){
    return $this->get(self::INSTALLED_POWER);
  }
 
}
