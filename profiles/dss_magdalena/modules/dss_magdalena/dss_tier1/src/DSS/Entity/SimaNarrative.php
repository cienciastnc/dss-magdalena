<?php

/**
 * @file
 * Narratives of TIER1 SIMA module
 */

namespace Drupal\dss_tier1\DSS\Entity;

use EntityFieldQuery;

/**
 * Narratives of TIER1 SIMA module
 *
 * Class SimaNarrative.
 *
 * @package Drupal\dss_magdalena\DSS\Entity
 */
class SimaNarrative extends EntityNodeAbstract
{

  const BUNDLE = 'narrative';
  const TITLE = 'title';
  const DESCRIPTION = 'field_narrative_description';
  const PROJECTS = 'field_narrative_projects';
  const ITERATIONS = 'field_randomizations';
  /**
   * Loads a TIER1 Narrative from a Drupal entity or a Drupal entity ID.
   *
   * @param mixed $project
   *   The nid or loaded object.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaProject|FALSE
   *   The loaded SimaProject object if exists, FALSE otherwise.
   */
  public static function load($narrativas)
  {
    if ($narrativas instanceof SimaNarrative) {
      return $narrativas;
    } else {
      if ($entity = parent::load($narrativas)) {
        return ($entity->getDrupalBundle() == self::BUNDLE) ? $entity : FALSE;
      }
      return FALSE;
    }
  }


  public function getNarrativeId()
  {
    return $this->getId();
  }
  public function getNarrative()
  {
    return $this->getDrupalEntity();
  }
  public function getIterations()
  {
    return  $this->get(self::ITERATIONS);
  }

  public function getTitle()
  {
    return $this->get(self::TITLE);
  }

  public function getDescription()
  {
    return $this->get(self::DESCRIPTION);
  }

  public function getProjects()
  {
    return $this->get(self::PROJECTS);
  }

  public function getModels()
  {
    return $this->get(self::MODELS);
  }

  /**
   * Returns a new Narrative.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaProject
   *   The loaded SimaProject.
   */
  static public function newNarrative()
  {
    global $user;
    // Create the new Project Node.
    return self::newEntity('node', self::BUNDLE, $user->uid);
  }
}
