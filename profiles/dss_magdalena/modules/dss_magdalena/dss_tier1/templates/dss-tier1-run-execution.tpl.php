<?php
/**
 * @file
 * Template for Case Readiness.
 */
?>
<div id="dss-tier1-run-execution" class="dss-case-study-group">
  <ul>
    <li><b><?php print t('Case Study') ?>: </b><?php print $case ?></li>
    <li><b><?php print t('Model') ?>: </b><?php print $model ?></li>
    <li><b><?php print t('Status') ?>: </b><?php print $status ?></li>
  </ul>
</div>
