<?php

/**
 * @file
 * Handler for List of execution Views Field: 'Run Execution ': 'run_execution'.
 * This filed allows to run a selected execution
 */


use Drupal\dss_tier1\DSS\Entity\SimaNarrative;

/**
 * Class definition.
 */
// @codingStandardsIgnoreStart
class dss_tier1_views_handler_field_run_execution extends views_handler_field
{
  // @codingStandardsIgnoreEnd

  /**
   * Render function for views field "run_execution".
   */
  public function render($values)
  {
    global $base_url;
    return '<a href="'.$base_url.'/run_execution/' . $values->nid . '">Ejecutar</a>';
  }
  /**
   * Views query method.
   */
  public function query()
  {
    // Do nothing, leave query blank, let views render the contents.
  }
}
