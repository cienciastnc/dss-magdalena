<?php

/**
 * @file
 * Handler for List of execution Views Field: 'Run Execution ': 'run_execution'.
 * This filed allows to run a selected execution
 */


use Drupal\dss_tier1\DSS\Entity\SimaExecution;

/**
 * Class definition.
 */
// @codingStandardsIgnoreStart
class dss_tier1_views_handler_field_parallel_resume extends views_handler_field
{
  // @codingStandardsIgnoreEnd

  /**
   * Render function for views field "run_execution".
   */
  public function render($values)
  {
    $output = '';
    if (class_exists('\Drupal\dss_magdalena\DSS\Entity\SimaExecution')) {
      if ($execution = SimaExecution::load($values->nid)) {
        $items = [];
        $executionName = $execution->getTitle();
      }
    }
    global $base_url;
    return '<a href="'.$base_url . '/tier1_parallel/'. $values->nid .'">Ejes Paralelos</a>';
  }
  /**
   * Views query method.
   */
  public function query()
  {
    // Do nothing, leave query blank, let views render the contents.
  }
}
