<?php

/**
 * @file
 * Interface to the Views API from the dss_engine.
 */

/**
 * Alter Views Data to create a new field: "model_status".
 *
 * @param array $data
 *   The views data array.
 */
function dss_tier1_views_data_alter(&$data)
{

  /**
   * Alter Views Data to create a new field: "run_execution".
   *
   * @param array $data
   *   The views data array.
   */
  $data['node']['run_execution'] = array(
    'title' => t('run_execution'),
    'help' => t('Take atributes of an execution an run it into MATLAB.'),
    'field' => array(
      'handler' => 'dss_tier1_views_handler_field_run_execution',
      'group' => 'Content',
      'click sortable' => FALSE,
    ),
  );

  /**
   * Alter Views Data to create a new field: "model_status".
   *
   * @param array $data
   *   The views data array.
   */
  $data['node']['execution_status'] = array(
    'title' => t('execution_status'),
    'help' => t('Take atributes of an execution an run it into MATLAB.'),
    'field' => array(
      'handler' => 'dss_tier1_views_handler_field_execution_status',
      'group' => 'Content',
      'click sortable' => FALSE,
    ),
  );
}
