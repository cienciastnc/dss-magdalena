<?php

/**
 * @file
 * Create required input files for cronjob
 * & executions of models by MATLAB's executable
 */

use Drupal\dss_magdalena\DSS\Entity\SimaResource;
use Drupal\dss_tier1\DSS\Entity\SimaExecution;
use Drupal\dss_tier1\DSS\Entity\SimaNarrative;
use Drupal\dss_tier1\DSS\Entity\SimaTier1Project;

/*
 * dss_engine_run_execution
 *
 * Take execution atributes to create input
 * files required for models run in MATLAB
 *
 * @param String $execution_id execution ID
 *
 */

function dss_tier1_run_execution($execution_id)
{

    drupal_add_js(drupal_get_path('module', 'dss_tier1') . '/js/execution_bar.js');

    //Get execution
    $execution = SimaExecution::load($execution_id);

    //Create input projects csv
    createInputProjects($execution);

    //Create input topological network csv
    createInputNetwork($execution);

    //Create control file required for cronjob
    createControlFile($execution);
    $output = '<div>';
    $output .= '<h3 align="center">';
    $output .= '<strong> La ejecución se encuentra en proceso, regrese en aproximadamente 5 minutos al listado para ver las salidas importadas </strong> </h3></div><br><a href="/execution-list"><button align="center" type="button" class="btn">Volver</button> </a>';
    return $output;
}

/*
 * Create input file with topological network
 * & temperature, precipitation, evotranspiration,
 * streamflow & sediments
 *
 * @param Object $execution execution.
 *
 */
function createInputNetwork($execution)
{
    $execution_id = $execution->getId();
    $executionPrecipitation = $execution->getPrecipitation();
    $executionTemperature = $execution->getTemperature();
    $executionEvotransp = $execution->getEvotranspiration();
    $executionStreamflow = $execution->getStreamflow();
    $executionSediments = $execution->getSediments();
    //Topological network
    $topologicalNetwork = SimaResource::load("3819");
    $topologicalNetwCsv = $topologicalNetwork->getResourceCsvFile();
    $networkCsvUri = drupal_realpath($topologicalNetwCsv->uri);
    $precipitationStatus = checkEmptyVariable($executionPrecipitation);
    $temperatureStatus = checkEmptyVariable($executionTemperature);
    $evapotranspStatus = checkEmptyVariable($executionEvotransp);
    $streamflowStatus = checkEmptyVariable($executionStreamflow);
    $sedimentsStatus = checkEmptyVariable($executionSediments);
    //print_r($temperatureStatus);
    if (!$precipitationStatus) {
        //Precipitation
        $precipitation = SimaResource::load($executionPrecipitation->nid);
        $precipitationCsv = $precipitation->getResourceCsvFile();
        $precipitationCsvUri = drupal_realpath($precipitationCsv->uri);
        $precipitationRows = extractCsvRows($precipitationCsvUri);
    }
    if (!$temperatureStatus) {
        //Temperature
        $temperature = SimaResource::load($executionTemperature->nid);
        $temperatureCsv = $temperature->getResourceCsvFile();
        $temperatureCsvUri = drupal_realpath($temperatureCsv->uri);
        $temperatureRows = extractCsvRows($temperatureCsvUri);
    }
    if (!$evapotranspStatus) {
        //Evotranspiration
        $evotransp = SimaResource::load($executionEvotransp->nid);
        $evotranspCsv = $evotransp->getResourceCsvFile();
        $evotranspCsvuri = drupal_realpath($evotranspCsv->uri);
        $evotranspRows = extractCsvRows($evotranspCsvuri);
    }
    if (!$streamflowStatus) {
        //Streamflow
        $streamflow = SimaResource::load($executionStreamflow->nid);
        $streamflowCsv = $streamflow->getResourceCsvFile();
        $streamflowCsvUri = drupal_realpath($streamflowCsv->uri);
        $streamflowRows = extractCsvRows($streamflowCsvUri);
    }
    if (!$sedimentsStatus) {
        //Sediments
        $sediments = SimaResource::load($executionSediments->nid);
        $sedimentsCsv = $sediments->getResourceCsvFile();
        $sedimentsCsvUri = drupal_realpath($sedimentsCsv->uri);
        $sedimentsRows = extractCsvRows($sedimentsCsvUri);
    }
    //Handle network file
    $networkHandle = fopen($networkCsvUri, 'r');
    $networkRow = fgetcsv($networkHandle);
    $networkColumns = array();
    //Set topological network headers
    array_push($networkColumns, 'ArcID'); //0
    array_push($networkColumns, 'FromNode'); //1
    array_push($networkColumns, 'ToNode'); //2
    array_push($networkColumns, 'River_Mouth'); //3
    array_push($networkColumns, 'Length_Drenage(km)'); //4
    array_push($networkColumns, 'Area(km2)'); //5
    array_push($networkColumns, 'Factor_Balance_Streamflow(Ad)'); //6
    array_push($networkColumns, 'Point_Interes'); //7
    array_push($networkColumns, 'Precipitation_Average_Annual(mm)'); //8
    array_push($networkColumns, 'Temperature(ºC)'); //9
    array_push($networkColumns, 'ETR(mm)'); //10
    array_push($networkColumns, 'Streamflow(m3/seg)'); //11
    array_push($networkColumns, 'Sediments(Ton/Yr)'); //12
    $currentUser = getCurrentUser();
    $file_save_path_stream_directory = 'private://' . $currentUser->uid . '-' . $currentUser->name . '/Inputs/execution-' . $execution_id;
    file_prepare_directory($file_save_path_stream_directory, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
    $fileLocation = $file_save_path_stream_directory . '/Topological_Network.csv';
    $output = "";
    $file = file_save_data($output, $fileLocation, FILE_EXISTS_REPLACE);
    $fh = fopen($fileLocation, 'w');
    fputcsv($fh, array($networkColumns[0], $networkColumns[1], $networkColumns[2], $networkColumns[3], $networkColumns[4], $networkColumns[5], $networkColumns[6], $networkColumns[7], $networkColumns[8], $networkColumns[9], $networkColumns[10], $networkColumns[11], $networkColumns[12]));
    $j = 0;
    $record = array();
    while ($networkRow = fgetcsv($networkHandle, 1000, ",")) {
        $precipitationValue = -999;
        $temperatureValue = -999;
        $evapotranspValue = -999;
        $streamflowValue = -999;
        $sedimentsValue = -999;
        foreach ($networkRow as $i => $field) {
            $record[$j][$networkColumns[$i]] = $field . " ";
        }
        if (!$precipitationStatus) {
            $precipitationValue = $precipitationRows[$j][$networkColumns[8]];
        }
        if (!$temperatureStatus) {
            $temperatureValue = $temperatureRows[$j][$networkColumns[9]];
        }
        if (!$evapotranspStatus) {
            $evapotranspValue = $evotranspRows[$j][$networkColumns[10]];
        }
        if (!$streamflowStatus) {
            $streamflowValue = $streamflowRows[$j][$networkColumns[11]];
        }
        if (!$sedimentsStatus) {
            $sedimentsValue = $sedimentsRows[$j][$networkColumns[12]];
        }
        fputcsv($fh, array((int) $record[$j][$networkColumns[0]], (int) $record[$j][$networkColumns[1]], (int) $record[$j][$networkColumns[2]], (float) $record[$j][$networkColumns[3]], (float) $record[$j][$networkColumns[4]], (float) $record[$j][$networkColumns[5]], (float) $record[$j][$networkColumns[6]], (float) $record[$j][$networkColumns[7]], (float) $precipitationValue, (float) $temperatureValue, (float) $evapotranspValue, (float) $streamflowValue, (float) $sedimentsValue));
        $j++;
    }
    fclose($fh);
}
/*
 * Create control file required
 * for cronjob create the control
 * file expected by MATLAB's executable
 *
 * @param Object $execution execution.
 *
 */
function createControlFile($execution)
{
    $execution_id = $execution->getId();
    $executionModels = $execution->getModels();
    $currentUser = getCurrentUser();
    $footprintStatus = $execution->getFootprintStatus();
    $file_save_path_stream_directory = 'private://' . $currentUser->uid . '-' . $currentUser->name . '/Inputs/execution-' . $execution_id;
    //Get footprint status
    //$footprintStatus = checkFootprintStatus($executionModels);
    /*
     * Validate if footpring model
     * is checked
     */
    if ($footprintStatus) { //Create footprint control file for footprints
        $fileLocation = $file_save_path_stream_directory . '/Footprint.csv';
        $fh = fopen($fileLocation, 'w');
        $footprintLayers = $execution->getFootprintLayers();
        foreach ($footprintLayers as $layer) {
            fputcsv($fh, array('Microfocalizacion', '1'));
            //$layer->field_code['und'][0]['value']
        }
        fclose($fh);
    } else {
        $footprintStatus = 0;
    }
    file_prepare_directory($file_save_path_stream_directory, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
    $fileLocation = $file_save_path_stream_directory . '/Control_File_SIMA-execution.txt';
    $fh = fopen($fileLocation, 'w');
    //Get model code
    $modelCode = getModelsCode($executionModels);
    fwrite($fh, '#UserName');
    fwrite($fh, "\n");
    fwrite($fh, "#-------------------------------------------");
    fwrite($fh, "\n");
    fwrite($fh, "* " . $currentUser->uid . "-" . $currentUser->name);
    fwrite($fh, "\n");
    fwrite($fh, "\n");
    fwrite($fh, '#Execution name');
    fwrite($fh, "\n");
    fwrite($fh, "#-------------------------------------------");
    fwrite($fh, "\n");
    fwrite($fh, '* execution-' . $execution_id);
    fwrite($fh, "\n");
    fwrite($fh, "\n");
    fwrite($fh, '#Analysis Code');
    fwrite($fh, "\n");
    fwrite($fh, "#-------------------------------------------");
    fwrite($fh, "\n");
    fwrite($fh, "* " . $modelCode);
    fwrite($fh, "\n");
    fwrite($fh, "\n");
    fwrite($fh, '#Footprint Status');
    fwrite($fh, "\n");
    fwrite($fh, "#-------------------------------------------");
    fwrite($fh, "\n");
    fwrite($fh, '* ' . $footprintStatus);
    fwrite($fh, "\n");
    fwrite($fh, "\n");
    fwrite($fh, '#Explorer Status');
    fwrite($fh, "\n");
    fwrite($fh, "#-------------------------------------------");
    fwrite($fh, "\n");
    fwrite($fh, '* 0');
    fclose($fh);
    /*
     * Validate if footpring model
     * is checked
     */
    if ($footprintStatus) { //Create footprint control file for footprints
        $fileLocation = $file_save_path_stream_directory . '/Footprint.csv';
        $fh = fopen($fileLocation, 'w');
        $footprintLayers = $execution->getFootprintLayers();
        foreach ($footprintLayers as $layer) {
            //print_r($layer->field_spatial_file['und'][0]['target_id']);
            $resources = SimaResource::listByDataset($layer->field_spatial_file['und'][0]['target_id']);
            $resourceFile = "";
            foreach ($resources as $key => $resource) {
                //array_push($resourcesId, $key);
                $resourceLoaded = SimaResource::load($key);
                $resourceFile = $resourceLoaded->getResourceCsvFile()->filename;
            } //END FOREACH
            //print_r($resourceFile);
            $resourceFileSplit = explode(".", $resourceFile);
            fputcsv($fh, array($resourceFileSplit[0], '1'));
        }
        fclose($fh);
    }
}

/*
 * Get the models code in sima &
 * homologate with models code that
 * MATLAB's excected
 *
 * @param Array $executionModels models selected.
 *
 * @return Int $modelCode code of model in MATLAB
 */
function getModelsCode($executionModels)
{
    $modelsSum = 0;
    foreach ($executionModels as $models => $model) {
        $modelsSum = $modelsSum + $model;
    }
    switch ($modelsSum) {
        case 3: //DOR - DORw
            $modelCode = 1;
            break;
        case 5: //SAI
            $modelCode = 2;
            break;
        case 7: //Fragmentation
            $modelCode = 4;
            break;
        case 8: //DOR -DORw+SAI
            $modelCode = 3;
            break;
        case 10: //Fragmentation+DOR -DORw
            $modelCode = 5;
            break;
        case 12: //Fragmentation+SAI
            $modelCode = 6;
            break;
        case 15: //Fragmentation + DOR + DORw + SAI
            $modelCode = 7;
            break;
    }
    return $modelCode;
}
/*
 * Check if footprint model
 * has been selected in the
 * execution
 *
 * @param Array $executionModels models* selected.
 *
 * @return Int $status 0=no footprint 1=footprint selected
 */
function checkFootprintStatus($executionModels)
{
    //9 analysys code for footprint
    $footprintStatus = in_array(9, $executionModels, false);
    if ($footprintStatus == true) {
        $status = 1;
    } else {
        $status = 0;
    }
    return $status;
}
/*
 * Extract the rows of
 * dataset for temperature
 * precipitation
 *
 * @param Object $execution execution.
 *
 * @return Array $rowRecords rows of the CSV
 */
function extractCsvRows($file)
{
    $fileHandle = fopen($file, 'r');
    $fileRows = fgetcsv($fileHandle);
    $fileColumns = array();

    foreach ($fileRows as $i => $header) {
        $fileColumns[$i] = trim($header);
    }

    $j = 0;
    while ($fileRow = fgetcsv($fileHandle, 1000, ",")) {
        foreach ($fileRow as $i => $field) {
            $rowRecords[$j][$fileColumns[$i]] = $field;
        }
        $j++;
    }
    return $rowRecords;
}

/*
 * Create input file with current
 * projects in the narratives
 * selected for the execution
 *
 * @param Object $execution execution.
 *
 */
function createInputProjects($execution)
{
    $execNarratives = $execution->getNarratives();
    $execution_id = $execution->getId();
    $executionModels = $execution->getModels();

    /** @var Array|null Project's list of execution */
    $projectList = array();

    /** @var Array|null List of iterations of different narrative's execution */
    $iterationsList = array();

    $currentUser = getCurrentUser();
    /*
     * Loop into execution's narrative
     * & load every narrative object to
     * obtain narrative's projects
     */
    foreach ($execNarratives as $narrativeCount => $narrative) {
        $narrativeId = $narrative->nid;
        $name = $narrative->title;
        $replaceSpace = str_replace(" ", "_", $name);
        $replaceHyphen = str_replace("-", "_", $replaceSpace);
        $replaceSlash = str_replace("/", "_", $replaceHyphen);
        $narrativeName = $replaceSlash;
        $narrativeLoad = SimaNarrative::load($narrativeId);
        $narrativeProjects = $narrativeLoad->getProjects();
        $randomizations = $narrativeLoad->getIterations();
        $output = "";
        //Validate if execution is random or not
        if ($randomizations > 0) //Random
        {
            $randomizationsFlag = 1;
        } else //Not random
        {
            $randomizationsFlag = 0;
        }

        $iteration = array("narrativeCode" => $narrativeId, "narrativeName" => $narrativeName, "iterationState" => $randomizationsFlag, "iterationNum" => $randomizations);
        $narrativeNum = sizeof($execNarratives);
        array_push($iterationsList, $iteration);
        foreach ($narrativeProjects as $projects) {
            $projectRow = array();
            $projectId = $projects->nid;
            $project = SimaTier1Project::load($projectId);
            // The position in the array is greater than 9
            if ($narrativeCount > 0) {
                // The array is static until position 8
                $projectRow[0] = $project->getProjectId();
                $projectRow[1] = $project->getCoordX();
                $projectRow[2] = $project->getCoordY();
                $projectRow[3] = $project->getArcId();
                $projectRow[4] = $project->getInstalledPower();
                $projectRow[5] = $project->getDammingVolume();
                $projectRow[6] = $project->getReservoirHight();
                $projectRow[7] = $project->getSedimentRetentionRate();
                $projectRow[8] = $project->getOperationYear();
                /* If narrative counter greater than 0, then
                fill the previous colum with 0*/
                if ($narrativeCount > 1) {
                    $projectRow[8 + $narrativeCount - 1] = 0;
                }
                // Fill next column with 0
                $projectRow[8 + $narrativeCount] = 0;

                //Fill narrative position with 1
                $projectRow[$narrativeCount + 1 + 8] = 1;

                if ($narrativeCount != $narrativeNum - 1) {
                    $projectRow[$narrativeCount + 2 + 8] = 0;
                }

                array_push($projectList, $projectRow);
            }
            /* First narrative of counter, position [9] of
            the array, others remain inactive*/else {
                $projectRow[0] = $project->getProjectId();
                $projectRow[1] = $project->getCoordX();
                $projectRow[2] = $project->getCoordY();
                $projectRow[3] = $project->getArcId();
                $projectRow[4] = $project->getInstalledPower();
                $projectRow[5] = $project->getDammingVolume();
                $projectRow[6] = $project->getReservoirHight();
                $projectRow[7] = $project->getSedimentRetentionRate();
                $projectRow[8] = $project->getOperationYear();
                $projectRow[9] = 1;
                for ($i = 1; $i <= $narrativeNum - 1; $i++) {
                    $projectRow[9 + $i] = 0;
                }
                array_push($projectList, $projectRow);
            }
        }
    }

    $file_save_path_stream_directory = 'private://' . $currentUser->uid . '-' . $currentUser->name . '/Inputs/execution-' . $execution_id;
    file_prepare_directory($file_save_path_stream_directory, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
    $fileLocation = $file_save_path_stream_directory . '/Projects.csv';
    $file = file_save_data($output, $fileLocation, FILE_EXISTS_REPLACE);
    $fh = fopen($fileLocation, 'w');

    /*************************************/
    /* Headers of CSV Input project file */
    /************************************/
    $projectIdHeader = "ProjectID";
    $xCoordinateHeader = "X";
    $yCoordinateHeader = "Y";
    $arcIdHeader = "ArcID";
    $installPowerHeader = "Installed_Power";
    $totalVolumeHeader = "Total_Volume";
    $azudHeightHeader = "Azud_Height";
    $sedimentsHeader = "Sediment_Retention";
    $yearHeader = "Year";

    $iterationsHeader = array($projectIdHeader, $xCoordinateHeader, $yCoordinateHeader, $arcIdHeader, $installPowerHeader, $totalVolumeHeader, $azudHeightHeader, $sedimentsHeader, $yearHeader);
    foreach ($iterationsList as $key => $iteration) {

        $iterationField = $iteration['narrativeCode'] . '|Narrative_' . $iteration['narrativeName'] . '|' . $iteration['iterationState'] . '|' . $iteration['iterationNum'];
        array_push($iterationsHeader, $iterationField);
    }

    //Write file's headers
    fputcsv($fh, $iterationsHeader);
    //Write the rows in file
    foreach ($projectList as $count => $project) {
        fputcsv($fh, $project);
    }
    fclose($fh);
}
/*
 * Return the current user login in Drupal
 *
 * @return $user current logged Drupal user.
 */
function getCurrentUser()
{
    require_once './includes/bootstrap.inc';
    drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
    global $user;
    return $user;
}
/*
 * Check for empty input exection's variable
 *
 * @param Object $input execution's input (precipitation, etc)
 *
 * @return boolean $result
 */
function checkEmptyVariable($input)
{
    if (empty($input)) {
        $result = true;
    } else {
        $result = false;
    }
    return $result;
}
