<?php
/**
 * @file
 * dss_variable_dataset.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function dss_variable_dataset_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_algorithm|node|resource|form';
  $field_group->group_name = 'group_algorithm';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resource';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_indicator';
  $field_group->data = array(
    'label' => 'If Based on an Algorithm',
    'weight' => '76',
    'children' => array(
      0 => 'field_calculation_algorithm',
      1 => 'field_indicator_variable',
      2 => 'field_indicator_databox',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-algorithm field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_algorithm|node|resource|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_allocated|node|resource|form';
  $field_group->group_name = 'group_allocated';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resource';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_indicator';
  $field_group->data = array(
    'label' => 'If Allocated Outside the DSS',
    'weight' => '80',
    'children' => array(
      0 => 'field_indicator_data_source',
      1 => 'field_indicator_observations',
      2 => 'field_indicator_value',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-allocated field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_allocated|node|resource|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_assigned_value|node|resource|form';
  $field_group->group_name = 'group_assigned_value';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resource';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_index';
  $field_group->data = array(
    'label' => 'Index Assigned',
    'weight' => '70',
    'children' => array(
      0 => 'field_index_value',
      1 => 'field_index_observations',
      2 => 'field_index_data_source',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-assigned-value field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_assigned_value|node|resource|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_assigned|node|resource|form';
  $field_group->group_name = 'group_assigned';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resource';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_natural';
  $field_group->data = array(
    'label' => 'If Assigned',
    'weight' => '45',
    'children' => array(
      0 => 'field_dss_data_source',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-assigned field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_assigned|node|resource|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_attributes|node|dataset|form';
  $field_group->group_name = 'group_attributes';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'dataset';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Dataset Attributes',
    'weight' => '2',
    'children' => array(
      0 => 'field_decision_group',
      1 => 'field_key_alternatives',
      2 => 'field_dimensions_labels',
      3 => 'field_machine_name',
      4 => 'field_variable_status',
      5 => 'field_dimensions',
      6 => 'field_temporality',
      7 => 'field_unit_of_measure',
      8 => 'field_decision_type',
      9 => 'field_variable_vocabulary',
      10 => 'field_model_family',
      11 => 'field_value_range',
      12 => 'field_variable_category',
      13 => 'field_node_category',
      14 => 'field_geometry',
      15 => 'field_type',
      16 => 'field_appropriate_spatial_scale',
      17 => 'field_default_value',
      18 => 'field_numeric_scale',
      19 => 'field_is_weap_variable',
      20 => 'field_context',
      21 => 'field_water_balance',
      22 => 'field_subsystem',
      23 => 'group_index_type',
      24 => 'group_thematic_tree',
      25 => 'group_weap_variable_attributes',
      26 => 'group_objective_type',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-attributes field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_attributes|node|dataset|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_calculated|node|resource|form';
  $field_group->group_name = 'group_calculated';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resource';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_natural';
  $field_group->data = array(
    'label' => 'If Calculated within the DSS ',
    'weight' => '60',
    'children' => array(
      0 => 'field_algorithm_variable',
      1 => 'field_algorithm_databox_',
      2 => 'field_algorithm_description',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-calculated field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_calculated|node|resource|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_index_type|node|dataset|form';
  $field_group->group_name = 'group_index_type';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'dataset';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_attributes';
  $field_group->data = array(
    'label' => 'Tipo Indice',
    'weight' => '38',
    'children' => array(
      0 => 'field_child_variable',
      1 => 'field_aggregation_tree',
      2 => 'field_reference_state',
      3 => 'field_aggregation_algorithm',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Tipo Indice',
      'instance_settings' => array(
        'required_fields' => 0,
        'id' => '',
        'classes' => 'group-index-type field-group-fieldset',
        'description' => '<strong>Aun no implementado.</strong>',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $field_groups['group_index_type|node|dataset|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_index|node|resource|form';
  $field_group->group_name = 'group_index';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resource';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Index Processing',
    'weight' => '15',
    'children' => array(
      0 => 'group_leaves',
      1 => 'group_assigned_value',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-index field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_index|node|resource|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_indicator|node|resource|form';
  $field_group->group_name = 'group_indicator';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resource';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Indicator Processing ',
    'weight' => '16',
    'children' => array(
      0 => 'group_algorithm',
      1 => 'group_allocated',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-indicator field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_indicator|node|resource|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_leaves|node|resource|form';
  $field_group->group_name = 'group_leaves';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resource';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_index';
  $field_group->data = array(
    'label' => 'Index From Leaves',
    'weight' => '66',
    'children' => array(
      0 => 'field_index_databox',
      1 => 'field_index_variable',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-leaves field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_leaves|node|resource|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_measured|node|resource|form';
  $field_group->group_name = 'group_measured';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resource';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_natural';
  $field_group->data = array(
    'label' => 'If Measured',
    'weight' => '48',
    'children' => array(
      0 => 'field_campaign_file',
      1 => 'field_campaign_measurement',
      2 => 'field_manual_measurement',
      3 => 'field_measurement_method',
      4 => 'field_measurement_stations',
      5 => 'field_measured_data_source',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-measured field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_measured|node|resource|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_modeling|node|resource|form';
  $field_group->group_name = 'group_modeling';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resource';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_natural';
  $field_group->data = array(
    'label' => 'If result of WRS modeling',
    'weight' => '55',
    'children' => array(
      0 => 'field_model_declaration',
      1 => 'field_model_family',
      2 => 'field_model_case_study_id',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-modeling field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_modeling|node|resource|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_natural|node|resource|form';
  $field_group->group_name = 'group_natural';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resource';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Natural processing',
    'weight' => '14',
    'children' => array(
      0 => 'group_measured',
      1 => 'group_assigned',
      2 => 'group_calculated',
      3 => 'group_modeling',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-natural field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_natural|node|resource|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_spatialization|node|resource|form';
  $field_group->group_name = 'group_spatialization';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resource';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Spatialization',
    'weight' => '11',
    'children' => array(
      0 => 'field_spatial_layer',
      1 => 'field_time_interval',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-spatialization field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_spatialization|node|resource|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_thematic_tree|node|dataset|form';
  $field_group->group_name = 'group_thematic_tree';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'dataset';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_attributes';
  $field_group->data = array(
    'label' => 'Thematic Tree Default Values',
    'weight' => '30',
    'children' => array(
      0 => 'field_dpsir',
      1 => 'field_po',
      2 => 'field_swot',
      3 => 'field_associated_thematic_tree',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-thematic-tree field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_thematic_tree|node|dataset|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_uncertainty|node|resource|form';
  $field_group->group_name = 'group_uncertainty';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resource';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Uncertainty',
    'weight' => '12',
    'children' => array(
      0 => 'field_qualitative_uncertainty',
      1 => 'field_quantitative_uncertainty',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-uncertainty field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_uncertainty|node|resource|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_weap_variable_attributes|node|dataset|form';
  $field_group->group_name = 'group_weap_variable_attributes';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'dataset';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_attributes';
  $field_group->data = array(
    'label' => 'WEAP Variable  Attributes',
    'weight' => '29',
    'children' => array(
      0 => 'field_level_2',
      1 => 'field_level_3',
      2 => 'field_level_4',
      3 => 'field_weap_level',
      4 => 'field_weap_object_type',
      5 => 'field_level_1',
      6 => 'field_weap_name',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-weap-variable-attributes field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_weap_variable_attributes|node|dataset|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Dataset Attributes');
  t('If Allocated Outside the DSS');
  t('If Assigned');
  t('If Based on an Algorithm');
  t('If Calculated within the DSS ');
  t('If Measured');
  t('If result of WRS modeling');
  t('Index Assigned');
  t('Index From Leaves');
  t('Index Processing');
  t('Indicator Processing ');
  t('Natural processing');
  t('Spatialization');
  t('Thematic Tree Default Values');
  t('Tipo Indice');
  t('Uncertainty');
  t('WEAP Variable  Attributes');

  return $field_groups;
}
