<?php
/**
 * @file
 * dss_variable_dataset.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function dss_variable_dataset_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_dataset';
  $strongarm->value = '0';
  $export['comment_dataset'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_resource';
  $strongarm->value = '0';
  $export['comment_resource'] = $strongarm;

  return $export;
}
