<?php
/*
 * dss_engine_run_execution
 *
 * Take execution atributes to create input
 * files required for models run in MATLAB
 *
 * @param String $execution_id execution ID
 *
 */

use Drupal\dss_magdalena\DSS\Entity\SimaDataset;
use Drupal\dss_magdalena\DSS\Entity\SimaResource;
use Drupal\dss_tier1\DSS\Entity\SimaExecution;
use Drupal\dss_tier1\DSS\Entity\SimaTier1Project;

function dss_tier1_visualizations_parallel($execution_id)
{
    global $base_url;
    $narrativeList = [];
    $projects = [];
    $execution = SimaExecution::load($execution_id);
    $executionTitle = $execution->getTitle();
    $executionOutputs = $execution->getOutputs();
    $execution_id = $execution->getId();
    $models = $execution->getModels();
    $modelsCode = getModelsCode($models);
    $footprintModel = $execution->getFootprintStatus();
    $layers = $execution->getFootprintLayers();
    $footprintLayers = array();
    $executionOutputs = $execution->getOutputs();
    $narratives = $execution->getNarratives();
    $narrativeCount = sizeof($execution->getNarratives());

    // Topological network csv
    $topoNetwork = SimaDataset::load(3817);
    $topoNetworkNid = SimaResource::listByDataset(3817);
    $topoNetworkGeoJson = SimaDataset::load(3850);
    $topoGeoJsonNid = SimaResource::listByDataset(3850);
    $colorList = [];
    $output = "";
    if (empty($executionOutputs)) {
        $output .= '<h4 align="center">';
        $output .= '<strong> La ejecución no ha sido procesada, no existen salidas disponibles ni archivo de control de errores </strong> </h4>';
        $output .= '<a href="/execution-list"><button align="center" type="button" class="btn">Volver</button> </a>';
        return $output;
    } else {
        $resourceList = SimaResource::listByDataset($executionOutputs->nid);
        foreach ($narratives as $key => $narrative) {
            $name = $narrative->title;
            $replaceSpace = str_replace(" ", "_", $name);
            $replaceHyphen = str_replace("-", "_", $replaceSpace);
            $replaceSlash = str_replace("/", "_", $replaceHyphen);
            $fullname = $replaceSlash;
            $list = array("nid" => $narrative->nid, "name" => $narrative->title, "fullname" => $fullname);
            array_push($narrativeList, $list);
            $narrativeProjects = $narrative->field_narrative_projects;
            for ($i = 0; $i <= sizeof($narrativeProjects['und']) - 1; $i++) {
                $projectId = $narrativeProjects['und'][$i]['target_id'];
                $project = SimaTier1Project::load($projectId);
                $projectObject = array("id" => $project->getProjectId(), "name" => $project->getName(), "narrativaNid" => $narrative->nid, "narrativa" => $narrative->title, "power" => $project->getInstalledPower(), "reservoirHight" => $project->getReservoirHight(), "xcoord" => $project->getCoordX(), "ycoord" => $project->getCoordY());
                array_push($projects, $projectObject);
            }
        }
        foreach ($topoNetworkNid as $key => $resource) {
            $resourceLoaded = SimaResource::load($key);
            $resourceFullname = $resourceLoaded->getFullname();
            $resourceDataset = $resourceLoaded->getReferencedVariables();
            $topoNetworkUuid = $resourceLoaded->getUuid();
            $topoNetworkDataset = $resourceLoaded->getReferencedVariables();
        }
        foreach ($topoGeoJsonNid as $key => $resource) {
            $resourceLoaded = SimaResource::load($key);
            $resourceFullname = $resourceLoaded->getFullname();
            $resourceDataset = $resourceLoaded->getReferencedVariables();
            $topoGeoJsonNid = $resourceLoaded->getUuid();
            $topoGeoJsonDataset = $resourceLoaded->getReferencedVariables();
        }
        switch ($modelsCode) {
            case 1: //DOR - DORw
                $outputs = [];
                foreach ($resourceList as $key => $resource) {
                    //array_push($resourcesId, $key);
                    $resourceLoaded = SimaResource::load($key);
                    $resourceFullname = $resourceLoaded->getFullname();
                    $resourceUuid = $resourceLoaded->getUuid();
                    $resourceDataset = $resourceLoaded->getReferencedVariables();
                    array_push($outputs, $resource = array("fullname" => $resourceFullname, "datasetUuid" => $resourceDataset[0]->uuid, "resourceUuid" => $resourceUuid));
                } //END FOREACH

                $topologicalNetwork = [];
                $network = array("datasetUuid" => $topoNetworkDataset[0]->uuid, "resourceUuid" => $topoNetworkUuid);
                array_push($topologicalNetwork, $network);
                $topologicalNetworkGeoJson = [];
                $datasetUuid = $resourceDataset[0]->uuid;
                $outputsEncode = json_encode($outputs);
                $topologicalNetworkEncode = json_encode($topologicalNetwork);
                $topologicalNetworkGeoJsonEncode = json_encode($topologicalNetworkGeoJson);
                $narrativeListEncode = json_encode($narrativeList);
                $projectsEncode = json_encode($projects);
                $footprintLayersEncode = json_encode($footprintLayers);
                drupal_add_js(
                    "
                var projects='$projectsEncode';
                var narrativeList='$narrativeListEncode';
                var outputs='$outputsEncode';
                var topoNetwork='$topologicalNetworkEncode';
                var datasetUuid='$datasetUuid';
                var narrativeCount='$narrativeCount';
                var footprintNid='$footprintLayersEncode';
                ",
                    'inline'
                );
                Drupal_add_css(drupal_get_path('module', 'dss_tier1_visualizations') . '/css/leaflet.css');
                Drupal_add_css(drupal_get_path('module', 'dss_tier1_visualizations') . '/css/select2.min.css');
                Drupal_add_css(drupal_get_path('module', 'dss_tier1_visualizations') . '/css/styles.css');
                Drupal_add_css("https://unpkg.com/leaflet@1.5.1/dist/leaflet.css");
                Drupal_add_js("https://unpkg.com/leaflet@1.5.1/dist/leaflet.js");
                Drupal_add_js("https://cdn.plot.ly/plotly-latest.min.js");
                Drupal_add_js("https://unpkg.com/georaster");
                Drupal_add_js("https://unpkg.com/georaster-layer-for-leaflet/georaster-layer-for-leaflet.browserify.min.js");
                Drupal_add_js(drupal_get_path('module', 'dss_tier1_visualizations') . '/js/select2.min.js');
                Drupal_add_js(drupal_get_path('module', 'dss_tier1_visualizations') . '/js/moment.min.js');

                $output = "";
                drupal_add_css(drupal_get_path('module', 'dss_tier1_visualizations') . '/css/dor_parallel_chart.css');
                Drupal_add_js("https://cdn.plot.ly/plotly-latest.min.js");
                Drupal_add_js(drupal_get_path('module', 'dss_tier1_visualizations') . '/js/parallel_chart/dor_parallel_chart.js');
                $output .= "<div id='chart'></div>";
                $output = '<div>';
                $output .= '<h3 align="center">';
                $output .= '<strong>Gráfico de ejes paralelos para la ' . $executionTitle . '</strong> </h3></div><br>';
                $htmlTemplate =  fopen(drupal_get_path('module', 'dss_tier1_visualizations') . '/html/dorAndDorW.html', "r") or die("No existe el archivo .html");
                $myHtml = fread($htmlTemplate, fstat($htmlTemplate)['size']);
                fclose($htmlTemplate);
                $output .= $myHtml;
                $output .= "<div id='chart'></div>";
                break; //END DOR CASE
            case 2: //SAI 
                if ($footprintModel) {
                    $footprintStatus = true;
                    foreach ($layers as $key => $layer) {
                        $layerName = $layer->title;
                        $layerMatlabCode = $layer->field_code['und'][0]['value'];
                        $layerNid = $layer->field_spatial_file['und'][0]['target_id'];
                        $layerUnity = $layer->field_unidad_de_medida['und'][0]['value'];
                        $layers = SimaResource::listByDataset($layerNid);
                        foreach ($layers as $key => $layer) {
                            $resourceLoaded = SimaResource::load($key);
                            $layerDataset = $resourceLoaded->getReferencedVariables();
                            $layerResourceResponse = json_decode(getFootrpintLayerUrl($layerDataset[0]->uuid));
                            $url = $layerResourceResponse->result[0]->resources[0]->url;
                            $splitUrl = explode("/", $url);
                            $layerUrl = $base_url . "/" . $splitUrl[3] . "/" . $splitUrl[4] . "/" . $splitUrl[5] . "/" . $splitUrl[6];
                            $layerObject = array("layerName" => $layerName, "layerMatlabCode" => $layerMatlabCode, "layerUrl" => $layerUrl, "layerUnity" => $layerUnity);
                            array_push($footprintLayers, $layerObject);
                        }
                    }
                } else {
                    $footprintStatus = false;
                    $outputs = [];
                    $output = '<div>';
                    $output .= '<h3 align="center">';
                    $output .= '<strong> La ejecución no tiene mas de un modelo, no es posible visualizar el gráfico de ejes paralelos</strong> </h3></div><br><a href="/execution-list"><button align="center" type="button" class="btn">Volver</button> </a>';
                    break; //END DOR CASE
                }
            case 3: //DOR - DORw - SAI
                $outputs = [];
                foreach ($resourceList as $key => $resource) {
                    //array_push($resourcesId, $key);
                    $resourceLoaded = SimaResource::load($key);
                    $resourceFullname = $resourceLoaded->getFullname();
                    $resourceUuid = $resourceLoaded->getUuid();
                    $resourceDataset = $resourceLoaded->getReferencedVariables();
                    array_push($outputs, $resource = array("fullname" => $resourceFullname, "datasetUuid" => $resourceDataset[0]->uuid, "resourceUuid" => $resourceUuid));
                } //END FOREACH

                $topologicalNetwork = [];
                $network = array("datasetUuid" => $topoNetworkDataset[0]->uuid, "resourceUuid" => $topoNetworkUuid);
                array_push($topologicalNetwork, $network);
                $topologicalNetworkGeoJson = [];
                $datasetUuid = $resourceDataset[0]->uuid;
                $outputsEncode = json_encode($outputs);
                $topologicalNetworkEncode = json_encode($topologicalNetwork);
                $topologicalNetworkGeoJsonEncode = json_encode($topologicalNetworkGeoJson);
                $narrativeListEncode = json_encode($narrativeList);
                $projectsEncode = json_encode($projects);
                $footprintLayersEncode = json_encode($footprintLayers);
                drupal_add_js(
                    "
                    var projects='$projectsEncode';
                    var narrativeList='$narrativeListEncode';
                    var outputs='$outputsEncode';
                    var topoNetwork='$topologicalNetworkEncode';
                    var datasetUuid='$datasetUuid';
                    var narrativeCount='$narrativeCount';
                    var footprintNid='$footprintLayersEncode';
                    ",
                    'inline'
                );
                Drupal_add_css(drupal_get_path('module', 'dss_tier1_visualizations') . '/css/leaflet.css');
                Drupal_add_css(drupal_get_path('module', 'dss_tier1_visualizations') . '/css/select2.min.css');
                Drupal_add_css(drupal_get_path('module', 'dss_tier1_visualizations') . '/css/styles.css');
                Drupal_add_css("https://unpkg.com/leaflet@1.5.1/dist/leaflet.css");
                Drupal_add_js("https://unpkg.com/leaflet@1.5.1/dist/leaflet.js");
                Drupal_add_js("https://cdn.plot.ly/plotly-latest.min.js");
                Drupal_add_js("https://unpkg.com/georaster");
                Drupal_add_js("https://unpkg.com/georaster-layer-for-leaflet/georaster-layer-for-leaflet.browserify.min.js");
                Drupal_add_js(drupal_get_path('module', 'dss_tier1_visualizations') . '/js/select2.min.js');
                Drupal_add_js(drupal_get_path('module', 'dss_tier1_visualizations') . '/js/moment.min.js');

                $output = "";
                drupal_add_css(drupal_get_path('module', 'dss_tier1_visualizations') . '/css/dor_parallel_chart.css');
                Drupal_add_js("https://cdn.plot.ly/plotly-latest.min.js");
                Drupal_add_js(drupal_get_path('module', 'dss_tier1_visualizations') . '/js/parallel_chart/dor_sai_parallel.js');
                $output .= "<div id='chart'></div>";
                $output = '<div>';
                $output .= '<h3 align="center">';
                $output .= '<strong>Gráfico de ejes paralelos para la  ' . $executionTitle . '</strong> </h3></div><br>';
                $htmlTemplate =  fopen(drupal_get_path('module', 'dss_tier1_visualizations') . '/html/DorDorWandSAI.html', "r") or die("No existe el archivo .html");
                $myHtml = fread($htmlTemplate, fstat($htmlTemplate)['size']);
                fclose($htmlTemplate);
                $output .= $myHtml;
                $output .= "<div id='chart'></div>";


                break; //END DOR CASE
                /****************************/
                /* FRAGMENTATION + FOOTPRINT
                /****************************/
            case 4:
                if ($footprintModel) {
                    $footprintStatus = true;
                    foreach ($layers as $key => $layer) {
                        $layerName = $layer->title;
                        $layerMatlabCode = $layer->field_code['und'][0]['value'];
                        $layerNid = $layer->field_spatial_file['und'][0]['target_id'];
                        $layerUnity = $layer->field_unidad_de_medida['und'][0]['value'];
                        $layers = SimaResource::listByDataset($layerNid);
                        foreach ($layers as $key => $layer) {
                            $resourceLoaded = SimaResource::load($key);
                            $layerDataset = $resourceLoaded->getReferencedVariables();
                            $layerResourceResponse = json_decode(getFootrpintLayerUrl($layerDataset[0]->uuid));
                            $url = $layerResourceResponse->result[0]->resources[0]->url;
                            $splitUrl = explode("/", $url);
                            $layerUrl = $base_url . "/" . $splitUrl[3] . "/" . $splitUrl[4] . "/" . $splitUrl[5] . "/" . $splitUrl[6];
                            $layerObject = array("layerName" => $layerName, "layerMatlabCode" => $layerMatlabCode, "layerUrl" => $layerUrl, "layerUnity" => $layerUnity);
                            array_push($footprintLayers, $layerObject);
                        }
                    }
                    $outputs = [];
                    foreach ($resourceList as $key => $resource) {
                        //array_push($resourcesId, $key);
                        $resourceLoaded = SimaResource::load($key);
                        $resourceFullname = $resourceLoaded->getFullname();
                        $resourceUuid = $resourceLoaded->getUuid();
                        $resourceDataset = $resourceLoaded->getReferencedVariables();
                        array_push($outputs, $resource = array("fullname" => $resourceFullname, "datasetUuid" => $resourceDataset[0]->uuid, "resourceUuid" => $resourceUuid));
                    } //END FOREACH

                    $topologicalNetwork = [];
                    $network = array("datasetUuid" => $topoNetworkDataset[0]->uuid, "resourceUuid" => $topoNetworkUuid);
                    array_push($topologicalNetwork, $network);
                    $topologicalNetworkGeoJson = [];
                    $datasetUuid = $resourceDataset[0]->uuid;
                    $outputsEncode = json_encode($outputs);
                    $topologicalNetworkEncode = json_encode($topologicalNetwork);
                    $topologicalNetworkGeoJsonEncode = json_encode($topologicalNetworkGeoJson);
                    $narrativeListEncode = json_encode($narrativeList);
                    $projectsEncode = json_encode($projects);
                    $footprintLayersEncode = json_encode($footprintLayers);
                    drupal_add_js(
                        "
                    var projects='$projectsEncode';
                    var narrativeList='$narrativeListEncode';
                    var outputs='$outputsEncode';
                    var topoNetwork='$topologicalNetworkEncode';
                    var datasetUuid='$datasetUuid';
                    var narrativeCount='$narrativeCount';
                    var footprintNid='$footprintLayersEncode';
                    ",
                        'inline'
                    );
                    Drupal_add_css(drupal_get_path('module', 'dss_tier1_visualizations') . '/css/leaflet.css');
                    Drupal_add_css(drupal_get_path('module', 'dss_tier1_visualizations') . '/css/select2.min.css');
                    Drupal_add_css(drupal_get_path('module', 'dss_tier1_visualizations') . '/css/styles.css');
                    Drupal_add_css("https://unpkg.com/leaflet@1.5.1/dist/leaflet.css");
                    Drupal_add_js("https://unpkg.com/leaflet@1.5.1/dist/leaflet.js");
                    Drupal_add_js("https://cdn.plot.ly/plotly-latest.min.js");
                    Drupal_add_js("https://unpkg.com/georaster");
                    Drupal_add_js("https://unpkg.com/georaster-layer-for-leaflet/georaster-layer-for-leaflet.browserify.min.js");
                    Drupal_add_js(drupal_get_path('module', 'dss_tier1_visualizations') . '/js/select2.min.js');
                    Drupal_add_js(drupal_get_path('module', 'dss_tier1_visualizations') . '/js/moment.min.js');

                    $output = "";
                    drupal_add_css(drupal_get_path('module', 'dss_tier1_visualizations') . '/css/dor_parallel_fragmentation_chart.css');
                    Drupal_add_js("https://cdn.plot.ly/plotly-latest.min.js");
                    Drupal_add_js(drupal_get_path('module', 'dss_tier1_visualizations') . '/js/parallel_chart/dor_dorw_fragmentation.js');
                    $output .= "<div id='chart'></div>";
                    $output = '<div>';
                    $output .= '<h3 align="center">';
                    $output .= '<strong>Gráfico de ejes paralelos para ' . $executionTitle . '</strong> </h3></div><br>';
                    $htmlTemplate =  fopen(drupal_get_path('module', 'dss_tier1_visualizations') . '/html/dorDorwFragmentation.html', "r") or die("No existe el archivo .html");
                    $myHtml = fread($htmlTemplate, fstat($htmlTemplate)['size']);
                    fclose($htmlTemplate);
                    $output .= $myHtml;
                    $output .= "<div id='chart'></div>";
                    break; //END DOR CASE
                } else {
                    $footprintStatus = false;
                    $outputs = [];
                    $output = '<div>';
                    $output .= '<h3 align="center">';
                    $output .= '<strong> La ejecución no tiene mas de un modelo, no es posible visualizar el gráfico de ejes paralelos</strong> </h3></div><br><a href="/execution-list"><button align="center" type="button" class="btn">Volver</button> </a>';
                    $output .= "<div id='chart'></div>";
                    break; //END DOR CASE
                }
                /****************************/
                /* DOR+DORW + FRAGMENTATION
                /****************************/
            case 5:
                if ($footprintModel) {
                    $footprintStatus = true;
                    foreach ($layers as $key => $layer) {
                        $layerName = $layer->title;
                        $layerMatlabCode = $layer->field_code['und'][0]['value'];
                        $layerNid = $layer->field_spatial_file['und'][0]['target_id'];
                        $layerUnity = $layer->field_unidad_de_medida['und'][0]['value'];
                        $layers = SimaResource::listByDataset($layerNid);
                        foreach ($layers as $key => $layer) {
                            $resourceLoaded = SimaResource::load($key);
                            $layerDataset = $resourceLoaded->getReferencedVariables();
                            $layerResourceResponse = json_decode(getFootrpintLayerUrl($layerDataset[0]->uuid));
                            $url = $layerResourceResponse->result[0]->resources[0]->url;
                            $splitUrl = explode("/", $url);
                            $layerUrl = $base_url . "/" . $splitUrl[3] . "/" . $splitUrl[4] . "/" . $splitUrl[5] . "/" . $splitUrl[6];
                            $layerObject = array("layerName" => $layerName, "layerMatlabCode" => $layerMatlabCode, "layerUrl" => $layerUrl, "layerUnity" => $layerUnity);
                            array_push($footprintLayers, $layerObject);
                        }
                    }
                } else {
                    $footprintStatus = false;
                }
                $outputs = [];
                foreach ($resourceList as $key => $resource) {
                    //array_push($resourcesId, $key);
                    $resourceLoaded = SimaResource::load($key);
                    $resourceFullname = $resourceLoaded->getFullname();
                    $resourceUuid = $resourceLoaded->getUuid();
                    $resourceDataset = $resourceLoaded->getReferencedVariables();
                    array_push($outputs, $resource = array("fullname" => $resourceFullname, "datasetUuid" => $resourceDataset[0]->uuid, "resourceUuid" => $resourceUuid));
                } //END FOREACH

                $topologicalNetwork = [];
                $network = array("datasetUuid" => $topoNetworkDataset[0]->uuid, "resourceUuid" => $topoNetworkUuid);
                array_push($topologicalNetwork, $network);
                $topologicalNetworkGeoJson = [];
                $geoJson = array("datasetUuid" => $topoGeoJsonDataset[0]->uuid, "resourceUuid" => $topoGeoJsonNid);
                array_push($topologicalNetworkGeoJson, $geoJson);
                $datasetUuid = $resourceDataset[0]->uuid;
                $outputsEncode = json_encode($outputs);
                $topologicalNetworkEncode = json_encode($topologicalNetwork);
                $topologicalNetworkGeoJsonEncode = json_encode($topologicalNetworkGeoJson);
                $narrativeListEncode = json_encode($narrativeList);
                $projectsEncode = json_encode($projects);
                $footprintLayersEncode = json_encode($footprintLayers);
                drupal_add_js(
                    "
                var projects='$projectsEncode';
                var narrativeList='$narrativeListEncode';
                var topoNetworkGeoJson='$topologicalNetworkGeoJsonEncode';
                var topoNetwork='$topologicalNetworkEncode';
                var outputs='$outputsEncode';
                var datasetUuid='$datasetUuid';
                var narrativeCount='$narrativeCount';
                var footprintStatus='$footprintStatus';
                var footprints='$footprintLayersEncode';
              ",
                    'inline'
                );
                Drupal_add_css(drupal_get_path('module', 'dss_tier1_visualizations') . '/css/leaflet.css');
                Drupal_add_css(drupal_get_path('module', 'dss_tier1_visualizations') . '/css/select2.min.css');
                Drupal_add_css(drupal_get_path('module', 'dss_tier1_visualizations') . '/css/styles.css');
                Drupal_add_css("https://unpkg.com/leaflet@1.5.1/dist/leaflet.css");
                Drupal_add_js("https://unpkg.com/leaflet@1.5.1/dist/leaflet.js");
                Drupal_add_js("https://cdn.plot.ly/plotly-latest.min.js");
                Drupal_add_js("https://unpkg.com/georaster");
                Drupal_add_js("https://unpkg.com/georaster-layer-for-leaflet/georaster-layer-for-leaflet.browserify.min.js");
                Drupal_add_js(drupal_get_path('module', 'dss_tier1_visualizations') . '/js/select2.min.js');
                Drupal_add_js(drupal_get_path('module', 'dss_tier1_visualizations') . '/js/moment.min.js');

                $output = "";
                drupal_add_css(drupal_get_path('module', 'dss_tier1_visualizations') . '/css/dor_parallel_fragmentation_chart.css');
                Drupal_add_js("https://cdn.plot.ly/plotly-latest.min.js");
                Drupal_add_js(drupal_get_path('module', 'dss_tier1_visualizations') . '/js/parallel_chart/dor_dorw_fragmentation.js');
                $output .= "<div id='chart'></div>";
                $output = '<div>';
                $output .= '<h3 align="center">';
                $output .= '<strong>Gráfico de ejes paralelos para ' . $executionTitle . '</strong> </h3></div><br>';
                $htmlTemplate =  fopen(drupal_get_path('module', 'dss_tier1_visualizations') . '/html/dorDorwFragmentation.html', "r") or die("No existe el archivo .html");
                $myHtml = fread($htmlTemplate, fstat($htmlTemplate)['size']);
                fclose($htmlTemplate);
                $output .= $myHtml;
                $output .= "<div id='chart'></div>";
                break; //END DOR CASE
                /****************************/
                /* SAI + FRAGMENTATION
                /****************************/
            case 6:
                if ($footprintModel) {
                    $footprintStatus = true;
                    foreach ($layers as $key => $layer) {
                        $layerName = $layer->title;
                        $layerMatlabCode = $layer->field_code['und'][0]['value'];
                        $layerNid = $layer->field_spatial_file['und'][0]['target_id'];
                        $layerUnity = $layer->field_unidad_de_medida['und'][0]['value'];
                        $layers = SimaResource::listByDataset($layerNid);
                        foreach ($layers as $key => $layer) {
                            $resourceLoaded = SimaResource::load($key);
                            $layerDataset = $resourceLoaded->getReferencedVariables();
                            $layerResourceResponse = json_decode(getFootrpintLayerUrl($layerDataset[0]->uuid));
                            $url = $layerResourceResponse->result[0]->resources[0]->url;
                            $splitUrl = explode("/", $url);
                            $layerUrl = $base_url . "/" . $splitUrl[3] . "/" . $splitUrl[4] . "/" . $splitUrl[5] . "/" . $splitUrl[6];
                            $layerObject = array("layerName" => $layerName, "layerMatlabCode" => $layerMatlabCode, "layerUrl" => $layerUrl, "layerUnity" => $layerUnity);
                            array_push($footprintLayers, $layerObject);
                        }
                    }
                } else {
                    $footprintStatus = false;
                }
                $outputs = [];
                foreach ($resourceList as $key => $resource) {
                    //array_push($resourcesId, $key);
                    $resourceLoaded = SimaResource::load($key);
                    $resourceFullname = $resourceLoaded->getFullname();
                    $resourceUuid = $resourceLoaded->getUuid();
                    $resourceDataset = $resourceLoaded->getReferencedVariables();
                    array_push($outputs, $resource = array("fullname" => $resourceFullname, "datasetUuid" => $resourceDataset[0]->uuid, "resourceUuid" => $resourceUuid));
                } //END FOREACH

                $topologicalNetwork = [];
                $network = array("datasetUuid" => $topoNetworkDataset[0]->uuid, "resourceUuid" => $topoNetworkUuid);
                array_push($topologicalNetwork, $network);
                $topologicalNetworkGeoJson = [];
                $geoJson = array("datasetUuid" => $topoGeoJsonDataset[0]->uuid, "resourceUuid" => $topoGeoJsonNid);
                array_push($topologicalNetworkGeoJson, $geoJson);
                $datasetUuid = $resourceDataset[0]->uuid;
                $outputsEncode = json_encode($outputs);
                $topologicalNetworkEncode = json_encode($topologicalNetwork);
                $topologicalNetworkGeoJsonEncode = json_encode($topologicalNetworkGeoJson);
                $narrativeListEncode = json_encode($narrativeList);
                $projectsEncode = json_encode($projects);
                $footprintLayersEncode = json_encode($footprintLayers);

                drupal_add_js(
                    "
              var projects='$projectsEncode';
              var narrativeList='$narrativeListEncode';
              var topoNetworkGeoJson='$topologicalNetworkGeoJsonEncode';
              var topoNetwork='$topologicalNetworkEncode';
              var outputs='$outputsEncode';
              var datasetUuid='$datasetUuid';
              var narrativeCount='$narrativeCount';
              var footprintStatus='$footprintStatus';
              var footprints='$footprintLayersEncode';
              ",
                    'inline'
                );
                Drupal_add_css(drupal_get_path('module', 'dss_tier1_visualizations') . '/css/leaflet.css');
                Drupal_add_css(drupal_get_path('module', 'dss_tier1_visualizations') . '/css/select2.min.css');
                Drupal_add_css(drupal_get_path('module', 'dss_tier1_visualizations') . '/css/styles.css');
                Drupal_add_css("https://unpkg.com/leaflet@1.5.1/dist/leaflet.css");
                Drupal_add_js("https://unpkg.com/leaflet@1.5.1/dist/leaflet.js");
                Drupal_add_js("https://cdn.plot.ly/plotly-latest.min.js");
                Drupal_add_js("https://unpkg.com/georaster");
                Drupal_add_js("https://unpkg.com/georaster-layer-for-leaflet/georaster-layer-for-leaflet.browserify.min.js");
                Drupal_add_js(drupal_get_path('module', 'dss_tier1_visualizations') . '/js/select2.min.js');
                Drupal_add_js(drupal_get_path('module', 'dss_tier1_visualizations') . '/js/moment.min.js');

                $output = "";
                drupal_add_css(drupal_get_path('module', 'dss_tier1_visualizations') . '/css/dor_parallel_fragmentation_chart.css');
                Drupal_add_js("https://cdn.plot.ly/plotly-latest.min.js");
                Drupal_add_js(drupal_get_path('module', 'dss_tier1_visualizations') . '/js/parallel_chart/sai_fragmentation_parallel_chart.js');
                $output .= "<div id='chart'></div>";
                $output = '<div>';
                $output .= '<h3 align="center">';
                $output .= '<strong>Gráfico de ejes paralelos para ' . $executionTitle . '</strong> </h3></div><br>';
                $htmlTemplate =  fopen(drupal_get_path('module', 'dss_tier1_visualizations') . '/html/saiFragmentation.html', "r") or die("No existe el archivo .html");
                $myHtml = fread($htmlTemplate, fstat($htmlTemplate)['size']);
                fclose($htmlTemplate);
                $output .= $myHtml;
                $output .= "<div id='chart'></div>";
                break; //END DOR CASE
            case 7:
                $outputs = [];
                foreach ($resourceList as $key => $resource) {
                    //array_push($resourcesId, $key);
                    $resourceLoaded = SimaResource::load($key);
                    $resourceFullname = $resourceLoaded->getFullname();
                    $resourceUuid = $resourceLoaded->getUuid();
                    $resourceDataset = $resourceLoaded->getReferencedVariables();
                    array_push($outputs, $resource = array("fullname" => $resourceFullname, "datasetUuid" => $resourceDataset[0]->uuid, "resourceUuid" => $resourceUuid));
                } //END FOREACH
                $topologicalNetwork = [];
                $network = array("datasetUuid" => $topoNetworkDataset[0]->uuid, "resourceUuid" => $topoNetworkUuid);
                array_push($topologicalNetwork, $network);
                $topologicalNetworkGeoJson = [];
                $datasetUuid = $resourceDataset[0]->uuid;
                $outputsEncode = json_encode($outputs);
                $topologicalNetworkEncode = json_encode($topologicalNetwork);
                $topologicalNetworkGeoJsonEncode = json_encode($topologicalNetworkGeoJson);
                $narrativeListEncode = json_encode($narrativeList);
                $projectsEncode = json_encode($projects);
                $footprintLayersEncode = json_encode($footprintLayers);
                drupal_add_js(
                    "
                    var projects='$projectsEncode';
                    var narrativeList='$narrativeListEncode';
                    var outputs='$outputsEncode';
                    var topoNetwork='$topologicalNetworkEncode';
                    var datasetUuid='$datasetUuid';
                    var narrativeCount='$narrativeCount';
                    var footprintNid='$footprintLayersEncode';
                    ",
                    'inline'
                );
                Drupal_add_css(drupal_get_path('module', 'dss_tier1_visualizations') . '/css/leaflet.css');
                Drupal_add_css(drupal_get_path('module', 'dss_tier1_visualizations') . '/css/select2.min.css');
                Drupal_add_css(drupal_get_path('module', 'dss_tier1_visualizations') . '/css/styles.css');
                Drupal_add_css("https://unpkg.com/leaflet@1.5.1/dist/leaflet.css");
                Drupal_add_js("https://unpkg.com/leaflet@1.5.1/dist/leaflet.js");
                Drupal_add_js("https://cdn.plot.ly/plotly-latest.min.js");
                Drupal_add_js("https://unpkg.com/georaster");
                Drupal_add_js("https://unpkg.com/georaster-layer-for-leaflet/georaster-layer-for-leaflet.browserify.min.js");
                Drupal_add_js(drupal_get_path('module', 'dss_tier1_visualizations') . '/js/select2.min.js');
                Drupal_add_js(drupal_get_path('module', 'dss_tier1_visualizations') . '/js/moment.min.js');

                $output = "";
                drupal_add_css(drupal_get_path('module', 'dss_tier1_visualizations') . '/css/dor_parallel_sai_frag_chart.css');
                Drupal_add_js("https://cdn.plot.ly/plotly-latest.min.js");
                Drupal_add_js(drupal_get_path('module', 'dss_tier1_visualizations') . '/js/parallel_chart/dor_sai_frag_parallel.js');
                $output .= "<div id='chart'></div>";
                $output = '<div>';
                $output .= '<h3 align="center">';
                $output .= '<strong>Gráfico de ejes paralelos para la  ' . $executionTitle . '</strong> </h3></div><br>';
                $htmlTemplate =  fopen(drupal_get_path('module', 'dss_tier1_visualizations') . '/html/DorDorWandSAIFrag.html', "r") or die("No existe el archivo .html");
                $myHtml = fread($htmlTemplate, fstat($htmlTemplate)['size']);
                fclose($htmlTemplate);
                $output .= $myHtml;
                $output .= "<div id='chart'></div>";
                break;
        }
    }
    return $output;
}

/*
 * Get the models code in sima &
 * homologate with models code that
 * MATLAB's excected
 *
 * @param Array $executionModels models selected.
 *
 * @return Int $modelCode code of model in MATLAB
 */
function getModelsCode($executionModels)
{
    $modelsSum = 0;
    foreach ($executionModels as $models => $model) {
        $modelsSum = $modelsSum + $model;
    }
    switch ($modelsSum) {
        case 3: //DOR - DORw
            $modelCode = 1;
            break;
        case 5: //SAI
            $modelCode = 2;
            break;
        case 7: //Fragmentation
            $modelCode = 4;
            break;
        case 8: //DOR -DORw+SAI
            $modelCode = 3;
            break;
        case 10: //Fragmentation+DOR -DORw
            $modelCode = 5;
            break;
        case 12: //Fragmentation+SAI
            $modelCode = 6;
            break;
        case 15: //Fragmentation + DOR + DORw + SAI
            $modelCode = 7;
            break;
    }
    return $modelCode;
}
/*
 * Get the tiff definitive URL
 * of the layer consulting the API

 *
 * @param string $uuid layer datset UUID.
 *
 * @return Array $response response with URL attribute.
 */
function getFootrpintLayerUrl($uuid)
{
    // Get cURL resource
    $curl = curl_init();
    // Set some options - we are passing in a useragent too here
    curl_setopt_array($curl, [
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => 'localhost/api/3/action/package_show?id=' . $uuid,
        CURLOPT_USERAGENT => 'Codular Sample cURL Request',
    ]);
    // Send the request & save response to $resp
    $response = curl_exec($curl);
    // Close request to clear up some resources
    curl_close($curl);
    return $response;
}
