<?php

/**
 * @file
 * Interface to the Views API from the dss_engine.
 */

/**
 * Alter Views Data to create a new field: "model_status".
 *
 * @param array $data
 *   The views data array.
 */
function dss_tier1_visualizations_views_data_alter(&$data)
{

/**
   * Alter Views Data to create a new field: "paraller_resume".
   *
   * @param array $data
   *   The views data array.
   */
  $data['node']['parallel_resume'] = array(
    'title' => t('Parallel Resume'),
    'help' => t('Take atributes of an execution an run it into MATLAB.'),
    'field' => array(
      'handler' => 'dss_tier1_visualizations_views_handler_field_parallel_resume',
      'group' => 'Content',
      'click sortable' => FALSE,
    ),
  );

  /**
   * Alter Views Data to create a new field: "paraller_resume".
   *
   * @param array $data
   *   The views data array.
   */
  $data['node']['error_status'] = array(
    'title' => t('Parallel Resume'),
    'help' => t('Take atributes of an execution an run it into MATLAB.'),
    'field' => array(
      'handler' => 'dss_tier1_visualizations_views_handler_field_error_status',
      'group' => 'Content',
      'click sortable' => FALSE,
    ),
  );
}
