<?php
/*
 * dss_engine_run_execution
 *
 * Take execution atributes to create input
 * files required for models run in MATLAB
 *
 * @param String $execution_id execution ID
 *
 */
use Drupal\dss_magdalena\DSS\Entity\SimaResource;
use Drupal\dss_tier1\DSS\Entity\SimaExecution;

function dss_tier1_visualizations_execution_status($execution_id)
{
    $execution = SimaExecution::load($execution_id);
    $executionTitle = $execution->getTitle();
    $executionOutputs = $execution->getOutputs();
    $output = "";
    if (empty($executionOutputs)) {
        $output .= '<div>';
        $output .= '<h4 align="center">';
        $output .= '<strong> La ejecución no ha sido procesada, no existen salidas disponibles ni archivo de control de errores </strong> </h4>';
    } else {
        $execution_id = $execution->getId();
        $resourceList = SimaResource::listByDataset($executionOutputs->nid);
        $statusFile = false;

        foreach ($resourceList as $key => $resource) {
            $resourceLoaded = SimaResource::load($key);
            $resourceFullname = $resourceLoaded->getFullname();
            $resourceUuid = $resourceLoaded->getUuid();
            $resourceDataset = $resourceLoaded->getReferencedVariables();
            if ($resourceFullname == "Status_Error") {
                $statusFile = true;
                $layerDataset = $resourceLoaded->getReferencedVariables();
                $response = json_decode(getFootrpintLayerUrl($layerDataset[0]->uuid));
                $resourceResponse = $response->result[0]->resources;
                if (sizeof($resourceResponse) < 1) {
                    $output .= '<div>';
                    $output .= '<h4 align="center">';
                    $output .= '<strong> El archivo de control de errores no ha sido cargado correctamente para las salidas de esta ejecución </strong> </h4>';
                } else {
                    $output .= '<h4 align="justify"> <strong>';
                    foreach ($resourceResponse as $key => $result) {
                        if ($resourceUuid == $result->id) {
                            $url = $result->url;
                            $statusResponse = getStatusError($url);
                            $textLines = explode(".", $statusResponse);
                            foreach ($textLines as $key2 => $line) {
                                $output .= $line . '<br><br>';
                            }
                        }

                    }
                    $output .= '</strong></h4>';
                }
            }

        }
        //END FOREACH
        if (!$statusFile) {
            $output .= '<div>';
            $output .= '<h4 align="center">';
            $output .= '<strong> El archivo de control de errores no ha sido cargado correctamente, o la ejecución no tiene salidas disponibles </strong> </h4>';
        }
    }
    $output .= '<a href="/execution-list"><button align="center" type="button" class="btn">Volver</button> </a>';
    return $output;
}
/*
 * Get the tiff definitive URL
 * of the layer consulting the API

 *
 * @param string $uuid layer datset UUID.
 *
 * @return Array $response response with URL attribute.
 */
function getFootrpintLayerUrl($uuid)
{
    // Get cURL resource
    $curl = curl_init();
    // Set some options - we are passing in a useragent too here
    curl_setopt_array($curl, [
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => 'http://localhost/api/3/action/package_show?id=' . $uuid,
        CURLOPT_USERAGENT => 'Codular Sample cURL Request',
    ]);
    // Send the request & save response to $resp
    $response = curl_exec($curl);
    // Close request to clear up some resources
    curl_close($curl);
    return $response;
}

/*
 * Get the tiff definitive URL
 * of the layer consulting the API

 *
 * @param string $uuid layer datset UUID.
 *
 * @return Array $response response with URL attribute.
 */
function getStatusError($url)
{
    // Get cURL resource
    $curl = curl_init();
    // Set some options - we are passing in a useragent too here
    curl_setopt_array($curl, [
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $url,
        CURLOPT_USERAGENT => 'Codular Sample cURL Request',
    ]);
    // Send the request & save response to $resp
    $response = curl_exec($curl);
    // Close request to clear up some resources
    curl_close($curl);
    return $response;
}
