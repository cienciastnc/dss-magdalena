/**
 * @file
 * DOR Visualizations
 * Read outputs of dor models & generate map
 * & graphic visualizations.
 * @since 2019
 */
jQuery(document).ready(function ($) {
  $('#cover-spin').show();
  var narratives = JSON.parse(narrativeList);
  $(".mi-selector").select2();
  var narrativesProjects = JSON.parse(projects)
  var narrativeDropdown = $("#narrativeMode");
  var footprintLayers = JSON.parse(footprints);

  narratives.forEach(function (narrative) {
    narrativeDropdown.append('<option value=' + narrative.nid + '> Narrativa ' + narrative.name + '</option>');
  });
  var dorOutputs = JSON.parse(outputs);
  var baseLineResources = dorOutputs.filter(file => file.fullname.includes("Base_Line"));

  if (baseLineResources.length > 0)
    baseLine = true
  else
    baseLine = false;

  var topologicalNetwork = JSON.parse(topoNetwork);
  var topologicalNetworkGeoJson = JSON.parse(topoNetworkGeoJson);
  var baseUrl = window.location.protocol + "//" + window.location.hostname;
  //Consultar variable y caja de datos
  var dkanUrl = "/api/3/action/package_show?id=";
  var fullUrlDataset = baseUrl + dkanUrl + datasetUuid;
  var fullUrlNetwork = baseUrl + dkanUrl + topologicalNetwork[0].datasetUuid;
  var fullUrlNetworkGeoJson = baseUrl + dkanUrl + topologicalNetworkGeoJson[0].datasetUuid;
  var networkResourceUrl;
  var networkGeoJsonResourceUrl;
  var map;
  var layerControl;
  let info;
  let mapLegend;
  var footprintsResponse = [];
  var layersResponse = [];
  var georasterResponse = [];
  var arrayBufferResponse = [];
  var geoTiffResponse = [];
  var datasetRequest = $.ajax({
    url: fullUrlDataset,
    type: 'GET',
  });

  var networkRequest = $.ajax({
    url: fullUrlNetwork,
    type: 'GET',
  });

  var networkGeoJsonRequest = $.ajax({
    url: fullUrlNetworkGeoJson,
    type: 'GET',
  });

  /*
  * Get response of output's datase request
  *
  * @return Object response response of request
  */
  datasetRequest.then(function (response) {
    var noBaseLineResources = [];
    var baseLineResources = [];
    var noBaseLineOutputs = [];
    var nobaseLineFootprint = [];
    /*
    * Filter no base line outputs by
    * word "narrativa" & the number
    */
    for (i = 0; i <= narratives.length - 1; i++) {
      noBaseLineOutputs.push(dorOutputs.filter(out => out.fullname == "DOR_" + narratives[i].nid + "_Narrative_" + narratives[i].name));
      noBaseLineOutputs.push(dorOutputs.filter(out => out.fullname == "Install_Power_" + narratives[i].nid + "_Narrative_" + narratives[i].name));
      noBaseLineOutputs.push(dorOutputs.filter(out => out.fullname == "DORw_" + narratives[i].nid + "_Narrative_" + narratives[i].name));
      noBaseLineOutputs.push(dorOutputs.filter(out => out.fullname == "Aleatory_Definition_" + narratives[i].nid + "_Narrative_" + narratives[i].name));
      if (footprintStatus) {
        noBaseLineOutputs.push(dorOutputs.filter(out => out.fullname == "Area_Volumen_" + narratives[i].nid + "_Narrative_" + narratives[i].name));
        noBaseLineOutputs.push(dorOutputs.filter(out => out.fullname == "Footprints_" + narratives[i].nid + "_Narrative_" + narratives[i].name));
        noBaseLineOutputs.push(dorOutputs.filter(out => out.fullname == "Watermirror_" + narratives[i].nid + "_Narrative_" + narratives[i].name));
      }
    }
    /*
    * Filter in dataset response, the no base
    * line resources & get the file URL
    */
    noBaseLineOutputs.forEach(function (output) {
      // Resources result of dataset request
      var resources = response.result[0].resources;
      // UUID of dataset's resource
      var resourceUuid = output[0].resourceUuid;
      var filterResource = resources.filter(r => r.id == resourceUuid);
      var resourceUrl = filterResource[0].url;
      var resource = { "name": output[0].fullname, "url": resourceUrl };
      noBaseLineResources.push(resource);
    });

    // Loop for every output that exists
    dorOutputs.forEach(function (output) {
      var fullname = output.fullname;
      var resourceUuid = output.resourceUuid;
      var resources = response.result[0].resources;
      var filterResource = resources.filter(r => r.id == resourceUuid);
      var resourceUrl = filterResource[0].url;
      /*
      * Footprint model is enable
      */
      if (footprintStatus) {
        switch (fullname) {
          case 'Watermirror_Base_Line':
            var resource = { "name": fullname, "url": resourceUrl };
            baseLineResources.push(resource);
            break;
          case 'Footprints_Base_Line':
            var resource = { "name": fullname, "url": resourceUrl };
            baseLineResources.push(resource);
            break;
          case 'Area_Volumen_Base_Line':
            var resource = { "name": fullname, "url": resourceUrl };
            baseLineResources.push(resource);
            break;

        }
      }
      /* Identify by name if is no baseline
    */
      switch (fullname) {
        case 'DOR_Base_Line':
          var resource = { "name": fullname, "url": resourceUrl };
          baseLineResources.push(resource);
          break;
        case 'DORw_Base_Line':
          var resource = { "name": fullname, "url": resourceUrl };
          baseLineResources.push(resource);
          break;
        case 'Install_Power_Base_Line':
          var resource = { "name": fullname, "url": resourceUrl };
          baseLineResources.push(resource);
          break;
      }
    });

    /*
    * Get response of topological network
    * dataset request & get topological
    * network resource URL
    *
    * @return Object response response of request
    */
    networkRequest.then(function (response) {
      // Resources attah to network dataset
      var resources = response.result[0].resources;

      // Filter reource by UUID
      var filterResource = resources.filter(r => r.id == topologicalNetwork[0].resourceUuid);

      // Resource url
      networkResourceUrl = filterResource[0].url;
    });

    /*
    * Get response of topological network GeoJson
    * dataset request & get topological
    * network resource URL
    *
    * @return Object response response of request
    */
    networkGeoJsonRequest.then(function (response) {
      // Resources attah to network dataset
      var resources = response.result[0].resources;

      // Filter reource by UUID
      var filterResource = resources.filter(r => r.id == topologicalNetworkGeoJson[0].resourceUuid);

      // Resource url
      networkGeoJsonResourceUrl = filterResource[0].url;
    });
    var installPowerNoBaseLine = [];
    /* Filter no base installed powers
    */
    for (i = 0; i <= narratives.length - 1; i++) {
      installPowerNoBaseLine.push(noBaseLineResources.filter(out => out.name == "Install_Power_" + narratives[i].nid + "_Narrative_" + narratives[i].name));
    }
    if (footprintStatus) {
      var footprintNoBaseLine = [];
      /* Filter no base installed powers
      */
      for (i = 0; i <= narratives.length - 1; i++) {
        footprintNoBaseLine.push(noBaseLineResources.filter(out => out.name == "Footprints_" + narratives[i].nid + "_Narrative_" + narratives[i].name));
      }

      var areaVolumeNoBaseLine = [];
      /* Filter no base installed powers
      */
      for (i = 0; i <= narratives.length - 1; i++) {
        areaVolumeNoBaseLine.push(noBaseLineResources.filter(out => out.name == "Area_Volumen_" + narratives[i].nid + "_Narrative_" + narratives[i].name));
      }

      var waterMirrorNoBaseLine = [];
      /* Filter no base installed powers
      */
      for (i = 0; i <= narratives.length - 1; i++) {
        waterMirrorNoBaseLine.push(noBaseLineResources.filter(out => out.name == "Watermirror_" + narratives[i].nid + "_Narrative_" + narratives[i].name));
      }

      var footprintNoBaseLineRequest = [];
      footprintNoBaseLine.forEach(function (resource) {
        var request = $.ajax({
          url: resource[0].url,
          type: 'GET',
        });
        var properties = { "name": resource[0].name, "request": request };
        footprintNoBaseLineRequest.push(properties);
      });

      var areaVolumeNoBaseLineRequest = [];
      areaVolumeNoBaseLine.forEach(function (resource) {
        var request = $.ajax({
          url: resource[0].url,
          type: 'GET',
        });
        var nameSplit = resource[0].name.split("_");
        var properties = { "name": resource[0].name, "request": request, "nid": nameSplit[1] };
        areaVolumeNoBaseLineRequest.push(properties);
      });

      var watermirrorNoBaseLineRequest = [];
      waterMirrorNoBaseLine.forEach(function (resource) {
        var request = $.ajax({
          url: resource[0].url,
          type: 'GET',
        });
        var nameSplit = resource[0].name.split("_");
        var properties = { "name": resource[0].name, "request": request, "url": resource[0].url, "nid": nameSplit[1] };
        watermirrorNoBaseLineRequest.push(properties);
      });
    }
    var installPowerNoBaseLineRequest = [];
    installPowerNoBaseLine.forEach(function (resource) {
      var request = $.ajax({
        url: resource[0].url,
        type: 'GET',
      });
      var properties = { "name": resource[0].name, "request": request };
      installPowerNoBaseLineRequest.push(properties);
    });




    // Aleatory definition outputs
    var aleatoryDefinition = [];
    for (i = 0; i <= narratives.length - 1; i++) {
      aleatoryDefinition.push(noBaseLineResources.filter(out => out.name == "Aleatory_Definition_" + narratives[i].nid + "_Narrative_" + narratives[i].name));
    }

    var dorNoBaseLine = [];
    // Filter no base DOR
    for (i = 0; i <= narratives.length - 1; i++) {
      dorNoBaseLine.push(noBaseLineResources.filter(out => out.name == "DOR_" + narratives[i].nid + "_Narrative_" + narratives[i].name));
    }
    // Filter no base DORw
    var dorwNoBaseLine = [];
    for (i = 0; i <= narratives.length - 1; i++) {
      dorwNoBaseLine.push(noBaseLineResources.filter(out => out.name == "DORw_" + narratives[i].nid + "_Narrative_" + narratives[i].name));
    }

    // Filter no base DORw
    var dorwNoBaseLine = [];
    for (i = 0; i <= narratives.length - 1; i++) {
      dorwNoBaseLine.push(noBaseLineResources.filter(out => out.name == "DORw_" + narratives[i].nid + "_Narrative_" + narratives[i].name));
    }
    //no base dor request
    var dorNoBaseLineRequest = [];
    dorNoBaseLine.forEach(function (resource) {
      var request = $.ajax({
        url: resource[0].url,
        type: 'GET',
      });
      var properties = { "name": resource[0].name, "request": request };
      dorNoBaseLineRequest.push(properties);
    });
    var aleatoryDefRequest = [];

    aleatoryDefinition.forEach(function (resource) {
      var request = $.ajax({
        url: resource[0].url,
        type: 'GET',
      });
      var properties = { "name": resource[0].name, "request": request };
      aleatoryDefRequest.push(properties);
    });

    var dorwNoBaseLineRequest = [];
    dorwNoBaseLine.forEach(function (resource) {
      var request = $.ajax({
        url: resource[0].url,
        type: 'GET',
      });
      var properties = { "name": resource[0].name, "request": request };
      dorwNoBaseLineRequest.push(properties);
    });


    // BaseLine install power URL request
    var installPowerBaseLine = baseLineResources.filter(r => r.name == 'Install_Power_Base_Line');
    if (installPowerBaseLine.length > 0)
      var installPowerUrlBaseLine = installPowerBaseLine[0].url;

    // BaseLine DOR URL request
    var dorBaseLine = baseLineResources.filter(r => r.name == 'DOR_Base_Line');
    if (dorBaseLine.length > 0)
      var dorUrlBaseLine = dorBaseLine[0].url;

    // BaseLine DOR URL request
    var dorwBaseLine = baseLineResources.filter(r => r.name == 'DORw_Base_Line');
    if (dorwBaseLine.length > 0)
      var dorwUrlBaseLine = dorwBaseLine[0].url;

    // BaseLine WaterMirror URL request
    var waterMirrorBaseLine = baseLineResources.filter(r => r.name == 'Watermirror_Base_Line');
    if (waterMirrorBaseLine.length > 0)
      var waterMirrorUrlBaseLine = waterMirrorBaseLine[0].url;

    // BaseLine Area Volumen URL request
    var areaVolumenBaseLine = baseLineResources.filter(r => r.name == 'Area_Volumen_Base_Line');
    if (areaVolumenBaseLine.length > 0)
      var areaVolumenUrlBaseLine = areaVolumenBaseLine[0].url;

    // BaseLine Footprint URL request
    var footprintsBaseLine = baseLineResources.filter(r => r.name == 'Footprints_Base_Line');
    if (areaVolumenBaseLine.length > 0)
      var footprintsUrlBaseLine = footprintsBaseLine[0].url;


    // BaseLine Install Power AJAX request
    var installPowerRequestBaseLine = $.ajax({
      url: installPowerUrlBaseLine,
      type: 'GET',
    });

    // BaseLine Water Mirror Request
    var waterMirrorRequestBaseLine = [];
    waterMirrorRequestBaseLine.push(waterMirrorBaseLine);

    // BaseLine Area Volumen Request
    var areaVolumenRequestBaseLine = $.ajax({
      url: areaVolumenUrlBaseLine,
      type: 'GET',
    });

    // BaseLine Footprint Request
    var footprintsRequestBaseLine = $.ajax({
      url: footprintsUrlBaseLine,
      type: 'GET',
    });

    // BaseLine DOR AJAX request
    var dorRequestBaseLine = $.ajax({
      url: dorUrlBaseLine,
      type: 'GET',
    });

    // BaseLine DORw AJAX request
    var dorwRequestBaseLine = $.ajax({
      url: dorwUrlBaseLine,
      type: 'GET',
    });

    var networkResourceRequest = $.ajax({
      url: networkResourceUrl,
      type: 'GET',
    });

    var networkGeoJsonResourceRequest = $.ajax({
      url: networkGeoJsonResourceUrl,
      type: 'GET',
    });

    /* Global variables section */
    var installPowerLines;
    var dorLines;
    var networkLines;
    var baseLinePower;
    var baseLineDor;
    var baseLineDorw;
    var baseLineAreaVolumen;
    var baseLineFootprint;
    var baseLineWaterMirrorRequest;
    var baseLineAreaVolumen;
    var noBaseLineDorwCsv
    var noBaseLineDorCsv;
    var noBaseLinePowerCsv;
    var riverMouthArc;
    var dorRowFilter;
    var installPowerFilter = [];
    var geoJsonResponse;
    var narrativeNid;
    var narrativeName;
    var geoJsonProjects;
    var legendRanks;
    var featuresArray = [];
    var tiffLayers = [];
    var noBaseLineWaterMirrorRequest = [];
    var noBaseLineFootprintResponse = [];
    var noBaseLineAreaVolumeResponse = [];
    /*
    * Get response of topological network
    * resource & filter river mouth arc
    *
    * @return Object response response of request
    */
    networkResourceRequest.then(function (response) {
      // Extract lines of csv in array
      networkLines = processCSv(response);
      /*
      * Loop every line & search river mouth arc
      */
      networkLines.forEach(function (row) {
        rowSplit = row[0].split(",");
        if (rowSplit[3] == '1')
          riverMouthArc = rowSplit[0];
      });

      /*
      * Get response of topological network
      * dataset request & get topological
      * network resource URL
      *
      * @return Object response response of request
      */
      networkGeoJsonResourceRequest.then(function (response) {
        geoJsonResponse = response;
        /* Loop every install power resource
        */
        installPowerNoBaseResponse = [];

        installPowerNoBaseLineRequest.forEach(function (resource) {
          var name = resource.name.split("_");
          narrativeNid = name[2];
          narrativeName = name[4];
          resource.request.then(function (response) {
            var noBaseResponse = Object();
            noBaseResponse.response = processCSv(response);
            noBaseResponse.nid = narrativeNid;
            noBaseResponse.name = narrativeName;
            installPowerNoBaseResponse.push(noBaseResponse);
          })
        });

        var dorNoBaseLineResponse = [];
        dorNoBaseLineRequest.forEach(function (resource) {
          var name = resource.name.split("_");
          narrativeNid = name[1];
          narrativeName = name[3];
          resource.request.then(function (response) {
            var noBaseResponse = Object();
            noBaseResponse.nid = narrativeNid;
            noBaseResponse.name = narrativeName;
            noBaseResponse.response = processCSv(response);
            dorNoBaseLineResponse.push(noBaseResponse);
          })
        });

        var dorwNoBaseLineResponse = [];
        dorwNoBaseLineRequest.forEach(function (resource) {
          var name = resource.name.split("_");
          narrativeNid = name[1];
          narrativeName = name[3];
          resource.request.then(function (response) {
            var noBaseResponse = Object();
            noBaseResponse.nid = narrativeNid;
            noBaseResponse.name = narrativeName;
            noBaseResponse.response = processCSv(response);
            dorwNoBaseLineResponse.push(noBaseResponse);
          })
        });

        var aleatoryDefResponse = [];
        aleatoryDefRequest.forEach(function (resource) {
          var name = resource.name.split("_");
          narrativeNid = name[2];
          narrativeName = name[4];
          resource.request.then(function (response) {
            var aleatoryResponse = Object();
            aleatoryResponse.nid = narrativeNid;
            aleatoryResponse.name = narrativeName;
            aleatoryResponse.response = processCSv(response);
            aleatoryDefResponse.push(aleatoryResponse);
          })
        });
        if (footprintStatus) {
          var footprintResponse = [];
          footprintNoBaseLineRequest.forEach(function (resource) {
            var name = resource.name.split("_");
            narrativeNid = name[1];
            narrativeName = name[3];
            resource.request.then(function (response) {
              var noBaseResponse = Object();
              noBaseResponse.nid = narrativeNid;
              noBaseResponse.name = narrativeName;
              noBaseResponse.response = processCSv(response);
              footprintResponse.push(noBaseResponse);
            })
          });

          var areaVolumeResponse = [];
          areaVolumeNoBaseLineRequest.forEach(function (resource) {
            var name = resource.name.split("_");
            narrativeNid = name[2];
            narrativeName = name[4];
            resource.request.then(function (response) {
              var noBaseResponse = Object();
              noBaseResponse.nid = narrativeNid;
              noBaseResponse.name = narrativeName;
              noBaseResponse.response = processCSv(response);
              areaVolumeResponse.push(noBaseResponse);
            })
          });
        }

        installPowerRequestBaseLine.then(function (response) {
          // Extract lines of csv in array
          baseLinePower = processCSv(response);
        });

        dorRequestBaseLine.then(function (response) {
          // Extract lines of csv in array
          baseLineDor = processCSv(response);
        });

        dorwRequestBaseLine.then(function (response) {
          // Extract lines of csv in array
          baseLineDorw = processCSv(response);
        });

        dorwRequestBaseLine.then(function (response) {
          // Extract lines of csv in array
          baseLineDorw = processCSv(response);
          var year = baseLinePower[0];
          var baseLineYears = year[0].split(",");
          var baseLineDorAxis = filterByDorArc(riverMouthArc, baseLineDor);
          var basePowerAxis = filterInstallPower(baseLinePower);


          var noBaseLineDorAxis = [];
          dorNoBaseLineResponse.forEach(function (dor) {
            var dorAxis = Object();
            dorAxis.axis = filterByDorArc(riverMouthArc, dor.response);
            dorAxis.name = dor.name;
            dorAxis.model = "DOR";
            dorAxis.nid = dor.nid;
            noBaseLineDorAxis.push(dorAxis);
          });


          var noBasePowerAxis = [];
          installPowerNoBaseResponse.forEach(function (power) {
            var powerAxis = Object();
            powerAxis.axis = filterInstallPower(power.response);
            powerAxis.name = power.name;
            noBasePowerAxis.push(powerAxis);
          });

          var geoJsonProjects = new Object();
          // Type properties set
          geoJsonProjects.type = geoJsonResponse.type;
          var propertiesName = Object();

          //Name key inside of properties
          propertiesName.name = geoJsonResponse.crs.properties.name;

          var properties = Object();

          // Properties key inside of crs
          properties.properties = propertiesName;


          //Crs properties set
          geoJsonProjects.crs = properties;
          geoJsonProjects.crs.type = geoJsonResponse.crs.type;

          var projectsRange = getProjectsRank(narrativesProjects);
          narrativesProjects.forEach(function (project) {
            // Object than contains coordinates X,Y
            var coordinates = Object();

            // Array with the coordinates
            var coordinatesArray = [project.xcoord, project.ycoord];

            // Asign coordinates to the object
            coordinates.coordinates = coordinatesArray;

            coordinates.type = "Point";

            // Global objecto for each feature in the GeoJSON
            var featureObject = Object();

            // Geometry property of features
            featureObject.geometry = coordinates;

            var projectRank = setProjectRadius(parseFloat(project.power), projectsRange);
            // Properties object for properties in features
            var properties = Object();
            properties.name = project.name;
            properties.power = project.power;
            properties.reservoirHight = project.reservoirHight;
            properties.rank = projectRank;
            properties.color = project.color;
            properties.narrativa = project.narrativa;
            properties.nid = project.narrativaNid;
            properties.id = project.id;

            // Asign  type to features
            featureObject.type = geoJsonResponse.features[0].type;

            // Asign  type to features
            featureObject.properties = properties;

            // Add feature objecto to array
            featuresArray.push(featureObject);
          });

          var features = Object();
          features = featuresArray;
          geoJsonProjects.features = features;
          if (baseLine)
            var topologicalRanks = getTopologicalRank(baseLineDor);
          //Set DOR CSV & Install Power CSV response
          setGlobalSources(geoJsonResponse, geoJsonProjects, dorNoBaseLineResponse, dorwNoBaseLineResponse, installPowerNoBaseResponse, baseLineDor, baseLineDorw, baseLinePower, riverMouthArc, topologicalRanks, watermirrorNoBaseLineRequest, footprintResponse, areaVolumeResponse, waterMirrorRequestBaseLine, baseLineAreaVolumen, baseLineFootprint, tiffLayers);
          var num = 0;

          if (baseLine) {
            /*
            * Set ArcID value in base of
            base line DOR last year values
            */
            baseLineDor.forEach(function (dor, index) {
              if (index == 0) { //Header's row
                row = dor[0].split(",");
                baseLineYear = row[row.length - 1];
              }
              else {
                dorRow = dor[0].split(",");
                rating = setTopoColor(parseFloat(dorRow[row.length - 1]), topologicalRanks);
                geoJsonResponse.features[num].properties.color = rating.color;
                geoJsonResponse.features[num].properties.rank = rating.rank;
                geoJsonResponse.features[num].properties.value = rating.value;
                //var topoLine = geoJsonResponse.features.filter(line => line.properties.ID == dorRow.)
                num++;
              }
            });

            actualYAxis = noBaseLineDorAxis;
            callMap(geoJsonResponse, geoJsonProjects);
            //modelChart(noBasePowerAxis, noBaseLineDorAxis);
            modelChart(basePowerAxis, baseLineDorAxis, baseLineYears, noBasePowerAxis, noBaseLineDorAxis);
          }
        });

        areaVolumenRequestBaseLine.then(function (response) {
          // Extract lines of csv in array
          baseLineAreaVolumen = processCSv(response);
        });

        footprintsRequestBaseLine.then(function (response) {
          // Extract lines of csv in array
          baseLineFootprint = processCSv(response);
        });


      }); //TOPOLOGICAL GEOJSON NETWORK REQUEST END

    });  //TOPOLOGICAL NETWORK REQUEST END

  });   //DATASET REQUEST END

  /*
  * Get install power of all projects
  * & set quintiles & configure ranks
  * network resource URL
  *
  * @param  Array projects  project list
  * @return Array rankValues list of rank
  */
  function getProjectsRank(projects) {
    var power = [];
    var rankValues = [];
    projects.forEach(function (project) {
      power.push(parseInt(project.power));
    })
    maxPower = Math.max.apply(null, power);
    minPower = Math.min.apply(null, power);
    promValue = maxPower / 5;
    var rank = Object();
    rank.min = parseFloat(minPower);
    rank.max = parseFloat(promValue);
    rankValues[0] = rank;
    for (i = 1; i <= 4; i++) {
      var rank = Object();
      rankValue = rankValues[i - 1].max + promValue;
      rank.min = parseFloat(rankValues[i - 1].max);
      rank.max = parseFloat(rankValue);
      rankValues.push(rank);
    }
    return rankValues;
  }

  /*
  * Get install power of all projects
  * & set quintiles & configure ranks
  * network resource URL
  *
  * @param  Array projects  project list
  * @return Array rankValues list of rank
  */
  function getTopologicalRank(arcs) {
    var values = [];
    var rankValues = [];
    arcs.forEach(function (arc, index) {
      if (index > 0) {
        var row = arc[0].split(",");
        values.push(parseFloat(row[row.length - 1]));
      }
    });
    maxValue = Math.max.apply(null, values);
    minValue = Math.min.apply(null, values);
    promValue = maxValue / 4;
    var rank = Object();
    rank.min = parseFloat(minValue);
    rank.max = parseFloat(promValue);
    rankValues[0] = rank;
    for (i = 1; i <= 3; i++) {
      var rank = Object();
      rankValue = rankValues[i - 1].max + promValue;
      rank.min = parseFloat(rankValues[i - 1].max);
      rank.max = parseFloat(rankValue);
      rankValues.push(rank);
    }
    return rankValues;
  }

  /*
  * Get install power of all projects
  * & set quintiles & configure ranks
  * network resource URL
  *
  * @param  Array projects  project list
  * @return Array rankValues list of rank
  */
  function getModelRanks(arcs) {
    var values = [];
    var rankValues = [];
    maxValue = Math.max.apply(null, arcs);
    minValue = Math.min.apply(null, arcs);
    promValue = maxValue / 4;
    var rank = Object();
    rank.min = parseFloat(minValue);
    rank.max = parseFloat(promValue);
    rankValues[0] = rank;
    for (i = 1; i <= 3; i++) {
      var rank = Object();
      rankValue = rankValues[i - 1].max + promValue;
      rank.min = parseFloat(rankValues[i - 1].max);
      rank.max = parseFloat(rankValue);
      rankValues.push(rank);
    }
    return rankValues;
  }


  /*
  * In base of projects rank, assign
  * five size of radius for projects
  *
  * @param  Float power      project install power
  * @param  Array rankValues list of rank
  * @return Int   rank       radius value
  */
  function setProjectRadius(power, rankValues) {
    var rank = 0;
    rankValues.forEach(function (r, index) {
      if (power >= r.min && power <= r.max)
        rank = index;
    });
    switch (rank) {
      case 0:
        rank = 3;
        break;
      case 1:
        rank = 5;
        break;
      case 2:
        rank = 7;
        break;
      case 3:
        rank = 9;
        break;
      case 4:
        rank = 11;
        break;
      default:
        break;
    }
    return rank;
  }

  /*
  * In base of DOR value rank, assign
  * four rank of color for topo network
  *
  * @param  Float power      project install power
  * @param  Array rankValues list of rank
  * @return Int   rank       radius value
  */
  function setTopoColor(value, rankValues) {
    var rank = 0;
    rankValues.forEach(function (r, index) {
      if (value >= r.min && value <= r.max) {
        rank = index;
      }
    });
    switch (rank) {
      case 0:
        var rating = {};
        rating.color = "#0f0";
        rating.rank = "Bueno"
        rating.value = value;
        break;
      case 1:
        var rating = {};
        rating.color = "#00f";
        rating.rank = "Regular"
        rating.value = value;
        break;
      case 2:
        var rating = {};
        rating.color = "#ff0";
        rating.rank = "Escaso"
        rating.value = value;
        break;
      case 3:
        var rating = {};
        rating.color = "#f00";
        rating.rank = "Malo"
        rating.value = value;
        break;
      default:
        break;
    }
    return rating;
  }

  function checkModelMode(selector) {
    selectValue = parseInt(selector.val());
    return selectValue;
  }
  /* Mode no base line or base line
  *  listener
  */
  var narrativeSelect = $('#narrativeMode');
  narrativeSelect.change(changeMode);

  /* Mode no base line or base line
  *  listener
  */
  var modelSelect = $('#modelSelect');
  modelSelect.change(changeMode);

  function changeMode() {
    var narrativeSelector = $('#narrativeMode');
    var narrativeSelect = narrativeSelector[0].value;
    var modelSelect = $('#modelSelect');
    var selectModel = checkModelMode(modelSelect);
    // Validate if is BaseLine or not
    if (narrativeSelect == "1") { //All narratives (BaseLine)
      switch (selectModel) {
        /**************** */
        /**** DORw MODEL  */
        /*****************/
        case 1:
          var noBaseLineDorwAxis = [];
          noBaseLineDorwCsv.forEach(function (dorw) {
            var dorwAxis = Object();
            dorwAxis.axis = filterByDorArc(riverMouthArc, dorw.response);
            dorwAxis.name = dorw.name;
            noBaseLineDorwAxis.push(dorwAxis);
          });
          var noBasePowerAxis = [];
          installPowerNoBaseResponse.forEach(function (power) {
            var powerAxis = Object();
            powerAxis.axis = filterInstallPower(power.response);
            powerAxis.name = power.name;
            noBasePowerAxis.push(powerAxis);
          });
          var baseLineDorwAxis = filterByDorArc(riverMouthArc, baseLineDorwCsv);
          var basePowerAxis = filterInstallPower(baseLinePower);
          var year = baseLinePower[0];
          var baseLineYears = year[0].split(",");
          var num = 0;
          var topologicalRanks = getTopologicalRank(baseLineDorwCsv);

          /*
          * Set ArcID value in base of
          base line DOR last year values
          */
          baseLineDorwCsv.forEach(function (dor, index) {
            if (index == 0) { //Header's row
              row = dor[0].split(",");
              baseLineYear = row[row.length - 1];
            }
            else {
              dorRow = dor[0].split(",");
              rating = setTopoColor(parseFloat(dorRow[row.length - 1]), topologicalRanks);
              networkGeoJson.features[num]
              networkGeoJson.features[num].properties.color = rating.color;
              networkGeoJson.features[num].properties.rank = rating.rank;
              networkGeoJson.features[num].properties.value = rating.value;
              num++;
            }
          });
          modelChart(basePowerAxis, baseLineDorwAxis, baseLineYears, noBasePowerAxis, noBaseLineDorwAxis);
          updateMap(networkGeoJson, projectsGeoJson);
          break;
        /**************** */
        /**** DOR MODEL  */
        /*****************/
        case 2:
          var noBaseLineDorAxis = [];
          noBaseLineDorCsv.forEach(function (dor) {
            var dorAxis = Object();
            dorAxis.axis = filterByDorArc(riverMouthArc, dor.response);
            dorAxis.name = dor.name;
            noBaseLineDorAxis.push(dorAxis);
          });


          var noBasePowerAxis = [];
          installPowerNoBaseResponse.forEach(function (power) {
            var powerAxis = Object();
            powerAxis.axis = filterInstallPower(power.response);
            powerAxis.name = power.name;
            noBasePowerAxis.push(powerAxis);
          });

          var baseLineDorAxis = filterByDorArc(riverMouthArc, baseLineDorCsv);
          var basePowerAxis = filterInstallPower(baseLinePower);
          var year = baseLinePower[0];
          var baseLineYears = year[0].split(",");
          var num = 0;
          var topologicalRanks = getTopologicalRank(baseLineDorwCsv);
          /*
          * Set ArcID value in base of
          base line DOR last year values
          */
          baseLineDorCsv.forEach(function (dor, index) {
            if (index == 0) { //Header's row
              row = dor[0].split(",");
              baseLineYear = row[row.length - 1];
            }
            else {
              dorRow = dor[0].split(",");
              rating = setTopoColor(parseFloat(dorRow[row.length - 1]), topologicalRanks);
              networkGeoJson.features[num]
              networkGeoJson.features[num].properties.color = rating.color;
              networkGeoJson.features[num].properties.rank = rating.rank;
              //var topoLine = geoJsonResponse.features.filter(line => line.properties.ID == dorRow.)
              num++;
            }
          });
          modelChart(basePowerAxis, baseLineDorAxis, baseLineYears, noBasePowerAxis, noBaseLineDorAxis);
          updateMap(networkGeoJson, projectsGeoJson);
          break;
        /**************** */
        /*** FOOTPRINT  **/
        /*****************/
        case 3:
          $('#cover-spin').show();
          console.log(baseLineWaterMirrorRequest);
          var waterMirrorBaseLineResponse = [];
          baseLineWaterMirrorRequest.forEach(function (resource, index) {
            fetch(resource[0].url)
              .then(response => response.arrayBuffer())
              .then(arrayBuffer => {
                parseGeoraster(arrayBuffer).then(georaster => {
                  var baseLineResponse = Object();
                  baseLineResponse.response = georaster;
                  waterMirrorBaseLineResponse.push(baseLineResponse);
                  if (baseLineWaterMirrorRequest.length - 1 == index) {
                    processFootprintLayers(footprintLayers, waterMirrorBaseLineResponse);
                  }

                });
              });
          });
          break;
        default:
      }
    }
    /************************/
    /**** NO BASE LINE *****/
    /************************/
    else {
      switch (selectModel) {
        /**************** */
        /**** DOR MODEL  */
        /*****************/
        case 1:
          var noBaseLineDorwAxis = [];
          noBaseDorwFilter = noBaseLineDorwCsv.filter(dorw => dorw.nid == narrativeSelect);
          noBaseDorwFilter.forEach(function (dorw) {
            var dorwAxis = Object();
            dorwAxis.axis = filterByDorArc(riverMouthArc, dorw.response);
            dorwAxis.name = dorw.name;
            noBaseLineDorwAxis.push(dorwAxis);
          });
          var noBasePowerAxis = [];
          noBasePowerFilter = installPowerNoBaseResponse.filter(power => power.nid == narrativeSelect);
          noBasePowerFilter.forEach(function (power) {
            var powerAxis = Object();
            powerAxis.axis = filterInstallPower(power.response);
            powerAxis.name = power.name;
            noBasePowerAxis.push(powerAxis);
          });
          var baseLineDorwAxis = filterByDorArc(riverMouthArc, baseLineDorwCsv);
          var basePowerAxis = filterInstallPower(baseLinePower);
          var year = baseLinePower[0];
          var baseLineYears = year[0].split(",");
          var projects = projectsGeoJson.features.filter(project => project.properties.nid == narrativeSelect);
          updateMap(networkGeoJson, projects);
          modelChart(basePowerAxis, baseLineDorwAxis, baseLineYears, noBasePowerAxis, noBaseLineDorwAxis);

          break;
        /**************** */
        /**** DOR MODEL  */
        /*****************/
        case 2:
          var noBaseLineDorAxis = [];
          noBaseDorFilter = noBaseLineDorCsv.filter(dor => dor.nid == narrativeSelect);
          noBaseDorFilter.forEach(function (dor) {
            var dorAxis = Object();
            dorAxis.axis = filterByDorArc(riverMouthArc, dor.response);
            dorAxis.name = dor.name;
            noBaseLineDorAxis.push(dorAxis);
          });
          var noBasePowerAxis = [];
          noBasePowerFilter = installPowerNoBaseResponse.filter(power => power.nid == narrativeSelect);
          noBasePowerFilter.forEach(function (power) {
            var powerAxis = Object();
            powerAxis.axis = filterInstallPower(power.response);
            powerAxis.name = power.name;
            noBasePowerAxis.push(powerAxis);
          });
          var baseLineDorAxis = filterByDorArc(riverMouthArc, baseLineDorCsv);
          var basePowerAxis = filterInstallPower(baseLinePower);
          var year = baseLinePower[0];
          var baseLineYears = year[0].split(",");
          var projects = projectsGeoJson.features.filter(project => project.properties.nid == narrativeSelect);
          updateMap(networkGeoJson, projects);
          modelChart(basePowerAxis, baseLineDorAxis, baseLineYears, noBasePowerAxis, noBaseLineDorAxis);
          break;
        /*****************/
        /** FOOTPRINT  */
        /*****************/
        case 3:
          $('#cover-spin').show();
          waterMirrorNoBaseLineFilter = noBaseLineWaterMirrorRequest.filter(request => request.nid == narrativeSelect);
          var waterMirrorNoBaseLineResponse = [];
          var footprintNarrativeResponse = [];
          var areaVolumen = [];
          var areaVolumenNarrativeResponse = [];
          areaVolumen = noBaseLineAreaVolumeResponse.filter(request => request.nid == narrativeSelect);
          areaVolumenNarrativeResponse.push(areaVolumen[0].response);
          footprintNarrativeResponse = noBaseLineFootprintResponse.filter(request => request.nid == narrativeSelect);
          waterMirrorNoBaseLineFilter.forEach(function (resource, index) {
            fetch(resource.url)
              .then(response => response.arrayBuffer())
              .then(arrayBuffer => {
                parseGeoraster(arrayBuffer).then(georaster => {
                  var noBaseLineResponse = Object();
                  noBaseLineResponse.response = georaster;
                  waterMirrorNoBaseLineResponse.push(noBaseLineResponse);
                  if (waterMirrorNoBaseLineResponse.length - 1 == index) {
                    footprintMap(waterMirrorNoBaseLineResponse, tiffFootprints, networkGeoJson, projectsGeoJson);
                    footprintTable(areaVolumenNarrativeResponse[0], footprintNarrativeResponse[0].response, tiffFootprints);
                  }
                });
              });
          });
          break;
        default:
      }
    }
  }

  async function processFootprintLayers(layers, watermirrorResponse) {
    for (const layer of layers) {
      var layerPromise = fetch(layer.layerUrl, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        }
      });
      await getLayerResponse(layerPromise);
    }
    console.log(layersResponse);

    for (const raster of layersResponse) {
      var rasterPromise = raster.arrayBuffer();
      await getArrayBufferResponse(rasterPromise);
    }

    console.log(arrayBufferResponse);

    for (const buffer of arrayBufferResponse) {
      var bufferPromise = parseGeoraster(buffer);
      await getGeoTiffResponse(bufferPromise);
    }
    console.log(geoTiffResponse);

    layers.forEach(function (layer, index) {
      layer.tiffResponse = geoTiffResponse[index];
      tiffFootprints.push(layer);
    });
    footprintMap(watermirrorResponse, tiffFootprints, networkGeoJson, projectsGeoJson);
    footprintTable(baseLineAreaCsv, baseLineFootrpintCsv, tiffFootprints);

  }


  async function getLayerResponse(promise) {
    await promise.then(function (response) {
      layersResponse.push(response);
    });
  }

  async function getArrayBufferResponse(promise) {
    await promise.then(function (response) {
      arrayBufferResponse.push(response);
    });
  }

  async function getGeoTiffResponse(promise) {
    await promise.then(function (response) {
      geoTiffResponse.push(response);
    });
  }

  /*
  * Update chart Yaxis when user's
  * clic on Arc in the Map
  *
  * @param    Object  e   object selected
  */
  function updateChart(e) {
    var layer = e.target;
    var arcId = layer.feature.properties.ID;
    var selectorModel = $('#modelSelect');
    var selectorNarrative = $('#narrativeMode');
    var narrativeSelect = selectorNarrative[0].value;
    var selectModel = parseInt(selectorModel[0].value);
    // Validate if is BaseLine or not
    if (narrativeSelect == "1") { //All narratives
      switch (selectModel) {
        case 1: //DORw
          var noBaseLineDorwAxis = [];
          noBaseLineDorwCsv.forEach(function (dorw) {
            var dorwAxis = Object();
            dorwAxis.axis = filterByDorArc(arcId, dorw.response);
            dorwAxis.name = dorw.name;
            noBaseLineDorwAxis.push(dorwAxis);
          });


          var noBasePowerAxis = [];
          installPowerNoBaseResponse.forEach(function (power) {
            var powerAxis = Object();
            powerAxis.axis = filterInstallPower(power.response);
            powerAxis.name = power.name;
            noBasePowerAxis.push(powerAxis);
          });

          var baseLineDorwAxis = filterByDorArc(arcId, baseLineDorwCsv);
          var basePowerAxis = filterInstallPower(baseLinePower);
          var year = baseLinePower[0];
          var baseLineYears = year[0].split(",");
          modelChart(basePowerAxis, baseLineDorwAxis, baseLineYears, noBasePowerAxis, noBaseLineDorwAxis);

          break;
        case 2: //DOR
          var noBaseLineDorAxis = [];
          noBaseLineDorCsv.forEach(function (dor) {
            var dorAxis = Object();
            dorAxis.axis = filterByDorArc(arcId, dor.response);
            dorAxis.name = dor.name;
            noBaseLineDorAxis.push(dorAxis);
          });

          var noBasePowerAxis = [];
          installPowerNoBaseResponse.forEach(function (power) {
            var powerAxis = Object();
            powerAxis.axis = filterInstallPower(power.response);
            powerAxis.name = power.name;
            noBasePowerAxis.push(powerAxis);
          });
          var baseLineDorAxis = filterByDorArc(arcId, baseLineDorCsv);
          var basePowerAxis = filterInstallPower(baseLinePower);
          var year = baseLinePower[0];
          var baseLineYears = year[0].split(",");
          modelChart(basePowerAxis, baseLineDorAxis, baseLineYears, noBasePowerAxis, noBaseLineDorAxis);
          break;
        default:
      }
    }
    else {
      switch (selectModel) {
        case 1: //DORw
          var noBaseLineDorwAxis = [];
          noBaseDorwFilter = noBaseLineDorwCsv.filter(dorw => dorw.nid == narrativeSelect);
          noBaseDorwFilter.forEach(function (dorw) {
            var dorwAxis = Object();
            dorwAxis.axis = filterByDorArc(arcId, dorw.response);
            dorwAxis.name = dorw.name;
            noBaseLineDorwAxis.push(dorwAxis);
          });
          var noBasePowerAxis = [];
          noBasePowerFilter = installPowerNoBaseResponse.filter(power => power.nid == narrativeSelect);
          noBasePowerFilter.forEach(function (power) {
            var powerAxis = Object();
            powerAxis.axis = filterInstallPower(power.response);
            powerAxis.name = power.name;
            noBasePowerAxis.push(powerAxis);
          });
          var baseLineDorwAxis = filterByDorArc(arcId, baseLineDorwCsv);
          var basePowerAxis = filterInstallPower(baseLinePower);
          var year = baseLinePower[0];
          var baseLineYears = year[0].split(",");
          modelChart(basePowerAxis, baseLineDorwAxis, baseLineYears, noBasePowerAxis, noBaseLineDorwAxis);

          break;
        case 2: //DOR
          var noBaseLineDorAxis = [];
          noBaseDorFilter = noBaseLineDorCsv.filter(dor => dor.nid == narrativeSelect);
          noBaseDorFilter.forEach(function (dor) {
            var dorAxis = Object();
            dorAxis.axis = filterByDorArc(arcId, dor.response);
            dorAxis.name = dor.name;
            noBaseLineDorAxis.push(dorAxis);
          });
          var noBasePowerAxis = [];
          noBasePowerFilter = installPowerNoBaseResponse.filter(power => power.nid == narrativeSelect);
          noBasePowerFilter.forEach(function (power) {
            var powerAxis = Object();
            powerAxis.axis = filterInstallPower(power.response);
            powerAxis.name = power.name;
            noBasePowerAxis.push(powerAxis);
          });
          var baseLineDorAxis = filterByDorArc(arcId, baseLineDorCsv);
          var basePowerAxis = filterInstallPower(baseLinePower);
          var year = baseLinePower[0];
          var baseLineYears = year[0].split(",");
          modelChart(basePowerAxis, baseLineDorAxis, baseLineYears, noBasePowerAxis, noBaseLineDorAxis);
          break;
        default:
      }
    }
  }

  /*
  * Take xAxis & yAxis from model's outputs
  * & draw a graph with projects whitout
  * operation year
  *
  * @param    Array   baseLineXAxis    install power range for base line
  * @param    Array   baseLineYAxis    models file (DOR)
  * @param    Array   baseLineYears    years range in base line
  * @param    Array   noBaseLineXaxis  install power range for no base line
  * @param    Array   noBaseLineYaxis  models file (DOR) for no base line
  */
  function modelChart(baseLineXAxis, baseLineYAxis, baseLineYears, noBaseLineXaxis, noBaseLineYaxis) {
    var traces = [];
    var data = [];
    for (i = 0; i <= noBaseLineXaxis.length - 1; i++) {
      var trace = {
        x: noBaseLineXaxis[i].axis,
        y: noBaseLineYaxis[i].axis,
        uid: noBaseLineYaxis[i].nid,
        mode: 'markers',
        name: "Narrativa " + noBaseLineXaxis[i].name,
        marker: { size: 12 }
      };
      traces.push(trace);
    }

    var traceBaseLine = {
      x: baseLineXAxis,
      y: baseLineYAxis,
      name: "Linea Base",
      mode: 'lines+markers',
      text: baseLineYears,
      marker: { size: 12 }
    };
    traces.push(traceBaseLine);
    traces.forEach(function (trace) {
      data.push(trace);
    });

    // Source of the graph
    //var data = [trace1];

    var layout = {
      title: {
        text: 'Salidas del Modelo DOR',
        font: {
          family: 'Poppins, sans-serif',
          size: 24
        }
      },
      showlegend: true,
      xaxis: {
        title: {
          text: 'Potencia Instalada (MW)',
          font: {
            family: 'Poppins, sans-serif',
            size: 18,
            color: '#000000'
          }
        },
      },
      yaxis: {
        title: {
          text: 'Grado de Regulación (%)',
          font: {
            family: 'Poppins, sans-serif',
            size: 18,
            color: '#000000'
          }
        }
      }
    };
    Plotly.newPlot('chart', data, layout, { showSendToCloud: true });
    var myPlot = document.getElementById("chart");
    myPlot.on('plotly_click', chartClick);
    $('#cover-spin').hide();

  }
  /*
    * Take Area Volumen csv & show areas
    * with volume
    *
    * @param    Array   areaFootprint   footprint's area volume
    */
  function footprintTable(areaFootprint, footprintCsv, tiffLayers) {
    var areaObject = {};
    console.log(areaFootprint);
    tableRecord = [];
    recordId = [];
    recordName = [];
    recordArea = [];
    recordVolume = [];
    footprintCsv.forEach(function (area, index) {
      console.log(area);
      if (index > 0) {
        areaSplit = area[0].split(",");
        areaObject.id = areaSplit[0];
        areaObject.id = areaSplit[0];
        var filteredProject = narrativesProjects.filter(project => project.id == areaSplit[0]);
        areaObject.projectId = areaSplit[0];
        areaObject.projectName = filteredProject[0].name;
        areaObject.area = areaSplit[1];
        recordId.push(areaObject.projectId);
        recordName.push(areaObject.projectName);
        recordArea.push(areaObject.area);
      }
    });
    var array1 = [];
    var array2 = [];

    array2.push("<b>ID del Proyecto</b>");
    array1.push(array2);
    var array2 = [];
    array2.push("<b>Nombre del Proyecto</b>");
    array1.push(array2);
    tiffLayers.forEach(function (layer) {
      var array2 = [];
      array2.push("<b>" + layer.layerName + " (" + layer.layerUnity + ")</b>");
      array1.push(array2);
    });
    //tableColumns.push(tableValues);
    console.log(tiffLayers);
    tableRecord[0] = recordId;
    tableRecord[1] = recordName;
    tableRecord[2] = recordArea;
    console.log(narrativesProjects);
    console.log(tableRecord);
    var areas = filterInstallPower(areaFootprint);
    var headerColor = "rgb(0, 118, 186)";
    var rowEvenColor = "lightgrey";
    var rowOddColor = "white";

    var data = [{
      type: 'table',
      header: {
        values: array1,
        align: "center",
        line: { width: 1, color: 'black' },
        fill: { color: headerColor },
        font: { family: "Arial", size: 12, color: "white" }
      },
      cells: {
        values: tableRecord,
        align: "center",
        line: { color: "black", width: 1 },
        fill: {
          color: [[rowOddColor, rowEvenColor, rowOddColor,
            rowEvenColor, rowOddColor]]
        },
        font: { family: "Arial", size: 11, color: ["black"] }
      }
    }]
    var layout = {
      title: {
        text: 'Salidas del Modelo de Huella',
        font: {
          family: 'Courier New, monospace',
          size: 24
        }
      }
    }
    Plotly.newPlot('chart', data, layout);
    $('#chart').height(250);
    $('#cover-spin').hide();
  }

  function chartClick(evt) {
    var narrativeNid = evt.points[0].data.uid;
    var pointPosition = evt.points[0].pointIndex;
    console.log(evt);
    var modelSelect = $('#modelSelect');
    var selectModel = checkModelMode(modelSelect);
    // Validate if is BaseLine or not
    switch (selectModel) {
      case 1: //DORw
        var noBaseLineDorwAxis = [];
        noBaseLineDorwCsv.forEach(function (dorw) {
          var dorwAxis = Object();
          axis = filterByNarrative(pointPosition);
          dorwAxis.name = dorw.name;
          noBaseLineDorwAxis.push(dorwAxis);
        });
        var noBasePowerAxis = [];
        installPowerNoBaseResponse.forEach(function (power) {
          var powerAxis = Object();
          powerAxis.axis = filterInstallPower(power.response);
          powerAxis.name = power.name;
          noBasePowerAxis.push(powerAxis);
        });
        var baseLineDorwAxis = filterByDorArc(riverMouthArc, baseLineDorwCsv);
        var basePowerAxis = filterInstallPower(baseLinePower);
        var year = baseLinePower[0];
        var baseLineYears = year[0].split(",");
        var num = 0;
        var topologicalRanks = getTopologicalRank(baseLineDorwCsv);

        /*
        * Set ArcID value in base of
        base line DOR last year values
        */
        baseLineDorwCsv.forEach(function (dor, index) {
          if (index == 0) { //Header's row
            row = dor[0].split(",");
            baseLineYear = row[row.length - 1];
          }
          else {
            dorRow = dor[0].split(",");
            rating = setTopoColor(parseFloat(dorRow[row.length - 1]), topologicalRanks);
            networkGeoJson.features[num]
            networkGeoJson.features[num].properties.color = rating.color;
            networkGeoJson.features[num].properties.rank = rating.rank;
            //var topoLine = geoJsonResponse.features.filter(line => line.properties.ID == dorRow.)
            num++;
          }
        });
        modelChart(basePowerAxis, baseLineDorwAxis, baseLineYears, noBasePowerAxis, noBaseLineDorwAxis);
        updateMap(networkGeoJson, projectsGeoJson);
        break;
      case 2: //DOR
        var noBaseLineDorAxis = [];
        var noBaseDorFilter = noBaseLineDorCsv.filter(dor => dor.nid == narrativeNid)
        noBaseDorFilter.forEach(function (dor) {
          var dorAxis = Object();
          dorAxis.axis = filterByNarrative(pointPosition, dor.response);
          dorAxis.name = dor.name;
          noBaseLineDorAxis.push(dorAxis);
        });
        var ranks = getModelRanks(noBaseLineDorAxis[0].axis);
        console.log(noBaseLineDorAxis);
        var num = 0;
        /*
        * Set ArcID value in base of
        base line DOR last year values
        */
        noBaseLineDorAxis[0].axis.forEach(function (dor, index) {

          rating = setTopoColor(dor, ranks);
          networkGeoJson.features[index]
          networkGeoJson.features[index].properties.color = rating.color;
          networkGeoJson.features[index].properties.rank = rating.rank;
          //var topoLine = geoJsonResponse.features.filter(line => line.properties.ID == dorRow.)
          num++;
        });
        //modelChart(basePowerAxis, baseLineDorAxis, baseLineYears, noBasePowerAxis, noBaseLineDorAxis);
        updateMap(networkGeoJson, projectsGeoJson);
        break;
      default:
    }

  }
  /*
  * Set global variables for DOR csv file
  * & Install Power csv to use it outside
  * asyncronous callback
  *
  * @param    Object   dorLines           DOR response
  * @param    Object   installPowerLines  Install Power response
  */
  function setGlobalSources(geoJsonResponse, geoJsonProjects, noBaseDorLines, noBaseDorwLines, noBasePowerLines, baseLineDor, baseLineDorw, baseLinePowerLines, arcId, topologicalRanks, watermirrorNoBaseLineRequest, footprintResponse, areaVolumeResponse, waterMirrorRequestBaseLine, baseLineAreaVolumen, baseLineFootprint, tiffLayers) {
    // No BaseLine global CSV's files
    noBaseLineDorCsv = noBaseDorLines;
    noBaseLinePowerCsv = noBasePowerLines;
    noBaseLineDorwCsv = noBaseDorwLines;
    // BaseLine global CSV's files
    baseLineAreaCsv = baseLineAreaVolumen;
    baseLineDorCsv = baseLineDor;
    baseLineDorwCsv = baseLineDorw;
    baseLineFootrpintCsv = baseLineFootprint;
    baseLinePower = baseLinePowerLines;
    baseLineWaterMirrorRequest = waterMirrorRequestBaseLine;
    // Topological Network
    networkGeoJson = geoJsonResponse;
    projectsGeoJson = geoJsonProjects;
    riverMouthArc = arcId;
    legendRanks = topologicalRanks;
    tiffFootprints = tiffLayers;
    noBaseLineWaterMirrorRequest = watermirrorNoBaseLineRequest;
    noBaseLineFootprintResponse = footprintResponse;
    noBaseLineAreaVolumeResponse = areaVolumeResponse;
  }

  /*
  * Get row in Installed Power Csv file
  *
  * @param  Array   installPower    install power file
  */
  function filterInstallPower(installPower) {
    var rowSplit;
    var installRow = installPower[1];
    var row = JSON.stringify(installRow);
    var row = row.replace(/['"]+/g, '');
    row = row.replace(/[[\]]/g, '');
    installPowerFilter = row.split(",");
    installPowerFilter.forEach(function (value, index) {
      installPowerFilter[index] = parseFloat(value);
    });
    return installPowerFilter;
  }

  /* Get row in DOR Csv file
  *  filtering by ArcID
  *
  * @param    String   riverMouthArc    ArcID
  * @param    Array    dorFile          DOR file
  */
  function filterByDorArc(riverMouthArc, dorFile) {
    var dorRow = [];
    var dorRowFilter = [];
    /*
    * Loop every line &
    * search river mouth in DOR output
    */
    dorFile.forEach(function (row) {
      // Split columns in the string
      rowSplit = row[0].split(",");
      dorArc = rowSplit[0];
      if (dorArc == riverMouthArc) {
        dorRowFilter = rowSplit;

        // Convert object to string
        var row = JSON.stringify(dorRowFilter);

        // Replace double quotes
        var row = row.replace(/['"]+/g, '');
        // Replace brackets
        row = row.replace(/[[\]]/g, '');

        // Split each column by comma
        dorRowFilter = row.split(",");

        // Loop and convert every value in float
        dorRowFilter.forEach(function (value, index) {
          dorRowFilter[index] = parseFloat(value);
        });
      }
    });
    /* First position in array is ArcID, is no necesary
    *  so recreate array whitout the first position
    */
    for (i = 1; i <= dorRowFilter.length - 1; i++) {
      dorRow.push(dorRowFilter[i]);
    }
    return dorRow;
  }

  /* Get row in DOR Csv file
  *  filtering by ArcID
  *
  * @param    String   riverMouthArc    ArcID
  * @param    Array    dorFile          DOR file
  */
  function filterByNarrative(positionIndex, dorFile) {
    var dorRow = [];
    var dorRowFilter = [];
    /*
    * Loop every line &
    * search river mouth in DOR output
    */
    dorFile.forEach(function (row, index) {
      // Split columns in the string
      rowSplit = row[0].split(",");

      if (index > 0) {
        dorRowFilter = rowSplit;

        // Convert object to string
        var row = JSON.stringify(dorRowFilter);

        // Replace double quotes
        var row = row.replace(/['"]+/g, '');
        // Replace brackets
        row = row.replace(/[[\]]/g, '');

        // Split each column by comma
        dorRowFilter = row.split(",");


        dorRow.push(parseFloat(dorRowFilter[positionIndex + 1]));
        // Loop and convert every value in float

      }
    });
    /* First position in array is ArcID, is no necesary
    *  so recreate array whitout the first position
    */
    console.log(dorRow);
    return dorRow;
  }

  /*
  * Create map and read GeoJSON topological
  * network dataset and add it to the map
  *
  * @param    Object   geoJson  Topological Network
  */
  function callMap(geoJson, geoJsonProjects) {
    if (map) {
      map.eachLayer(function (layer) {
        map.removeLayer(layer);
      });
    }
    /*
    * Reset line of toplogical network
    * style when mouse out of network
    *
    * @param    Object  e   object selected
    */
    function resetHighlight(e) {
      let layer = e.target;
      topoNetwork.resetStyle(layer);
      info.update();
    }

    /*
    * Highlight line selected in
    * mouseover
    *
    * @param    Object  e   object selected
    */
    function highlightFeature(e) {
      let layer = e.target;
      let current_style = {
        weight: 3,
        color: '#050666',
        fillOpacity: 0.5
      };

      layer.setStyle(current_style);

      if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
        layer.bringToFront();
      } e

      info.update(layer.feature.properties);
    }
    // Topological Network layer
    var topoNetwork;
    topoNetwork = L.geoJson(geoJson, {
      onEachFeature: onEachFeature,
      style: function (feature) {
        return { color: feature.properties.color };
      }
    });

    //Projects layer
    var projectsLayer;
    projectsLayer = L.geoJson(geoJsonProjects, {
      onEachFeature: onEachPoint,
      pointToLayer: function (feature, latlng) {
        return L.circleMarker(latlng, {
          radius: feature.properties.rank,
          fillColor: "#993333",
          color: "#2F2D2D",
          weight: 1,
          opacity: 1,
          fillOpacity: 1
        });
      }
    });

    // Web map code goes here
    map = L.map('map', { center: [5.547631, -74.945643], zoom: 10, layers: [topoNetwork, projectsLayer] });
    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', { attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>' }).addTo(map);
    var baseLayers = {
      "Red topológica": topoNetwork,
    };
    var overlays = {
      "Proyectos": projectsLayer
    };
    //Control box
    info = L.control();
    layerControl = L.control.layers(baseLayers, overlays, { position: 'topleft' }).addTo(map);

    //Creates the control label
    info.onAdd = function (map) {
      this._div = L.DomUtil.create('div', 'info'); // create a div with a
      // class "info"
      this.update();
      return this._div;
    };

    //Method that we will use to update the control based on feature properties
    // passed
    info.update = function (props) {
      this._div.innerHTML = '<h4>Arcos de la Red</h4>' + (props ?
        'ArcID: <b>' + props.ID + '</b><br>Valor: <b>' + props.value
        : '<b>Haga clic sobre un arco de la red para actualizar');
    };
    info.addTo(map);
    /*Legend specific*/
    legend = L.control({ position: "bottomright" });
    var div = L.DomUtil.create("div", "legend");
    div.innerHTML = "<h4>Valor de la Red</h4>";
    table_string = '<table class="tableLegend">';
    var colorLegend = [];
    legendRanks.forEach(function (rank, index) {
      switch (index) {
        case 0:
          var rating = {};
          rating.color = "#0f0";
          rating.category = "Bueno"
          rating.rank = rank.min + " - " + rank.max;
          break;
        case 1:
          var rating = {};
          rating.color = "#00f";
          rating.category = "Regular"
          rating.rank = rank.min + " - " + rank.max;
          break;
        case 2:
          var rating = {};
          rating.color = "#ff0";
          rating.category = "Escaso"
          rating.rank = rank.min + " - " + rank.max;
          break;
        case 3:
          var rating = {};
          rating.color = "#f00";
          rating.category = "Malo"
          rating.rank = rank.min + " - " + rank.max;
          break;
        default:
          break;
      }

      colorLegend.push(rating);
    });
    colorLegend.forEach(function (color) {
      table_string += '<tr><td><i style="background:' + color.color + '"></i><span>' + color.rank + '</span></td><td>' + color.category + '</td></tr>';
    });
    table_string += '</table>';
    legend.onAdd = function (map) {
      div.innerHTML += table_string;
      return div;
    };

    legend.addTo(map);
    console.log(legend);
    mapLegend = legend;
    function onEachFeature(feature, layer) {
      layer.on({
        mouseover: highlightFeature,
        mouseout: resetHighlight,
        click: updateChart
      });
    }
    function onEachPoint(feature, layer) {
      var popupContent = "<h5>Proyecto </h5>" +
        "<b>Nombre: </b>" + feature.properties.name + "<br />" +
        "<b>Potencia Instalada: </b>" + feature.properties.power + "<br />" +
        "<b>Altura de la presa: </b>" + feature.properties.reservoirHight + "<br />" +
        "<b>Narrativa: </b>" + feature.properties.narrativa;

      if (feature.properties && feature.properties.popupContent) {
        popupContent += feature.properties.popupContent;
      }
      layer.bindPopup(popupContent);
    }
  }

  /*
  * Create map and read GeoJSON topological
  * network dataset and add it to the map
  *
  * @param    Object   geoJson  Topological Network
  */
  function updateMap(geoJson, geoJsonProjects) {
    if (map) {
      map.eachLayer(function (layer) {
        map.removeLayer(layer);
      });
    }
    /*
    * Reset line of toplogical network
    * style when mouse out of network
    *
    * @param    Object  e   object selected
    */
    function resetHighlight(e) {
      let layer = e.target;
      topoNetwork.resetStyle(layer);
      info.update();
    }

    /*
    * Highlight line selected in
    * mouseover
    *
    * @param    Object  e   object selected
    */
    function highlightFeature(e) {
      let layer = e.target;
      let current_style = {
        weight: 3,
        color: '#050666',
        fillOpacity: 0.5
      };

      layer.setStyle(current_style);

      if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
        layer.bringToFront();
      } e

      info.update(layer.feature.properties);
    }
    // Topological Network layer
    var topoNetwork;
    topoNetwork = L.geoJson(geoJson, {
      onEachFeature: onEachFeature,
      style: function (feature) {
        return { color: feature.properties.color };
      }
    }).addTo(map);

    //Projects layer
    var projectsLayer;
    projectsLayer = L.geoJson(geoJsonProjects, {
      onEachFeature: onEachPoint,
      pointToLayer: function (feature, latlng) {
        return L.circleMarker(latlng, {
          radius: feature.properties.rank,
          fillColor: feature.properties.color,
          color: "#2F2D2D",
          weight: 1,
          opacity: 1,
          fillOpacity: 1
        });
      }
    }).addTo(map);

    // Web map code goes here
    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', { attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>' }).addTo(map);
    var baseLayers = {
      "Red topológica": topoNetwork,

    };
    var overlays = {
      "Proyectos": projectsLayer
    };
    layerControl.remove();
    info.remove();
    mapLegend.remove();
    //Control box
    info = L.control();
    layerControl = L.control.layers(baseLayers, overlays, { position: 'topleft' }).addTo(map);

    //Creates the control label
    info.onAdd = function (map) {
      this._div = L.DomUtil.create('div', 'info'); // create a div with a
      // class "info"
      this.update();
      return this._div;
    };

    //Method that we will use to update the control based on feature properties
    // passed
    info.update = function (props) {
      this._div.innerHTML = '<h4>Arcos de la Red</h4>' + (props ?
        'ArcID: <b>' + props.ID + '</b><br>Valor: <b>' + props.value
        : '<b>Haga clic sobre un arco de la red para actualizar');
    };
    info.addTo(map);

    /*Legend specific*/
    var legend = L.control({ position: "bottomright" });
    var div = L.DomUtil.create("div", "legend");
    div.innerHTML = "<h4>Valor de la Red</h4>";
    table_string = '<table class="tableLegend">';
    var colorLegend = [];
    legendRanks.forEach(function (rank, index) {
      switch (index) {
        case 0:
          var rating = {};
          rating.color = "#0f0";
          rating.category = "Bueno"
          rating.rank = rank.min + " - " + rank.max;
          break;
        case 1:
          var rating = {};
          rating.color = "#00f";
          rating.category = "Regular"
          rating.rank = rank.min + " - " + rank.max;
          break;
        case 2:
          var rating = {};
          rating.color = "#ff0";
          rating.category = "Escaso"
          rating.rank = rank.min + " - " + rank.max;
          break;
        case 3:
          var rating = {};
          rating.color = "#f00";
          rating.category = "Malo"
          rating.rank = rank.min + " - " + rank.max;
          break;
        default:
          break;
      }

      colorLegend.push(rating);
    });
    colorLegend.forEach(function (color) {
      table_string += '<tr><td><i style="background:' + color.color + '"></i><span>' + color.rank + '</span></td><td>' + color.category + '</td></tr>';
    });
    table_string += '</table>';
    legend.onAdd = function (map) {
      div.innerHTML += table_string;
      return div;
    };

    legend.addTo(map);
    mapLegend = legend;
    function onEachFeature(feature, layer) {
      layer.on({
        mouseover: highlightFeature,
        mouseout: resetHighlight,
        click: updateChart
      });
    }
    function onEachPoint(feature, layer) {
      var popupContent = "<h5>Proyecto </h5>" +
        "<b>Nombre: </b>" + feature.properties.name + "<br />" +
        "<b>Potencia Instalada: </b>" + feature.properties.power + "<br />" +
        "<b>Altura de la presa: </b>" + feature.properties.reservoirHight + "<br />" +
        "<b>Narrativa: </b>" + feature.properties.narrativa;

      if (feature.properties && feature.properties.popupContent) {
        popupContent += feature.properties.popupContent;
      }

      layer.bindPopup(popupContent);
    }
  }

  /*
    * Create map and read GeoJSON topological
    * network dataset and add it to the map
    *
    * @param    Object   geoJson  Topological Network
    */
  function footprintMap(waterMirrorResponse, tiffLayers, geoJson, geoJsonProjects) {
    if (map) {
      map.eachLayer(function (layer) {
        map.removeLayer(layer);
      });
    }
    /*
    * Reset line of toplogical network
    * style when mouse out of network
    *
    * @param    Object  e   object selected
    */
    function resetHighlight(e) {
      let layer = e.target;
      topoNetwork.resetStyle(layer);
      info.update();
    }

    /*
    * Highlight line selected in
    * mouseover
    *
    * @param    Object  e   object selected
    */
    function highlightFeature(e) {
      let layer = e.target;
      let current_style = {
        weight: 3,
        color: '#050666',
        fillOpacity: 0.5
      };

      layer.setStyle(current_style);

      if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
        layer.bringToFront();
      } e

      info.update(layer.feature.properties);
    }

    //Projects layer
    var projectsLayer;
    projectsLayer = L.geoJson(geoJsonProjects, {
      onEachFeature: onEachPoint,
      pointToLayer: function (feature, latlng) {
        return L.circleMarker(latlng, {
          radius: feature.properties.rank,
          fillColor: feature.properties.color,
          color: "#2F2D2D",
          weight: 1,
          opacity: 1,
          fillOpacity: 1
        });
      }
    }).addTo(map);


    var waterMirror = new GeoRasterLayer({
      georaster: waterMirrorResponse[0].response,
      opacity: 0.5,
      pixelValuesToColorFn: values => values[0] > 100 ? '#ff0000' : ' rgb(255, 255, 255,0.1) ',
      resolution: 128
    }).addTo(map);
    map.fitBounds(waterMirror.getBounds());

    // Web map code goes here
    var baseMap = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', { attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>' }).addTo(map);
    var baseLayers = {
      "Mapa base": baseMap,
    };

    //Layer list
    var overlays = {
      "Proyectos": projectsLayer,
      "Watermirror": waterMirror
    };

    /* Loop into tiff layers & add it
    to the map*/
    tiffLayers.forEach(function (layer) {
      var tiffLayer = new GeoRasterLayer({
        georaster: layer.tiffResponse,
        opacity: 0.5,
        pixelValuesToColorFn: values => values[0] < 100 ? '#35ad28' : ' rgb(255, 255, 255,0.1) ',
        resolution: 128
      }).addTo(map);
      map.fitBounds(tiffLayer.getBounds());
      overlays[layer.layerName] = tiffLayer;
    });


    layerControl.remove();
    info.remove();
    mapLegend.remove();
    //Control box

    layerControl = L.control.layers(baseLayers, overlays, { position: 'topleft' }).addTo(map);


    function onEachFeature(feature, layer) {
      layer.on({
        mouseover: highlightFeature,
        mouseout: resetHighlight,
        click: updateChart
      });
    }

    function onEachPoint(feature, layer) {
      var popupContent = "<h5>Proyecto </h5>" +
        "<b>ID: </b>" + feature.properties.id + "<br />" +
        "<b>Nombre: </b>" + feature.properties.name + "<br />" +
        "<b>Potencia Instalada: </b>" + feature.properties.power + "<br />" +
        "<b>Altura de la presa: </b>" + feature.properties.reservoirHight + "<br />" +
        "<b>Narrativa: </b>" + feature.properties.narrativa;

      if (feature.properties && feature.properties.popupContent) {
        popupContent += feature.properties.popupContent;
      }

      layer.bindPopup(popupContent);
    }
  }

  /*
  * Read a CSV file & extract the content in rows
  *
  * @param String csv    CSV File
  *
  * @return Array lines  Rows of csv readed
  */
  function processCSv(csv) {
    var allTextLines = csv.split(/\r\n|\n/);
    var lines = [];
    for (var i = 0; i < allTextLines.length - 1; i++) {
      var data = allTextLines[i].split(';');
      var tarr = [];
      for (var j = 0; j < data.length; j++) {
        tarr.push(data[j]);
      }
      lines.push(tarr);
    }
    return lines;
  }
});
