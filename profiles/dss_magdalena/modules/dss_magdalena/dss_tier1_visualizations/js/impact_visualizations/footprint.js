/**
 * @file
 * Fragmentation Visualizations
 * Read outputs of sai models & generate map
 * & graphic visualizations.
 * @since 2019
 */

jQuery(document).ready(function ($) {
    var narratives = JSON.parse(narrativeList);
    var narrativesProjects = JSON.parse(projects)
    var $narrativeDropdown = $("#narrativeMode");
    var $arcDropdown = $("#funcArc");
    //Create narratives dropdown
    narratives.forEach(function (narrative) {
        $narrativeDropdown.append('<option value=' + narrative.nid + '> Narrativa ' + narrative.name + '</option>');
    });
    var funcOutputs = JSON.parse(outputs);
    var arcOutputs = funcOutputs.filter(file => file.fullname.includes("Arc"));
    var arcNarratives = arcOutputs.filter(file => file.fullname.includes("Narrative"));
    var noBaseLineOutputs = funcOutputs.filter(file => file.fullname.includes("Narrative"));

    var funcArcs = [];
    // Create arc's dropdown select
    arcNarratives.forEach(function (arc) {
        nameSplit = arc.fullname.split("_");
        arcNum = nameSplit[1];
        $arcDropdown.append('<option value=' + arc.fullname + '>' + "Arco " + arcNum + '</option>');
    });
    var topologicalNetwork = JSON.parse(topoNetwork);
    var topologicalNetworkGeoJson = JSON.parse(topoNetworkGeoJson);
    var baseUrl = window.location.protocol + "//" + window.location.hostname;
    var dkanUrl = "/api/3/action/package_show?id=";
    var fullUrlDataset = baseUrl + dkanUrl + datasetUuid;
    var fullUrlNetwork = baseUrl + dkanUrl + topologicalNetwork[0].datasetUuid;
    var fullUrlNetworkGeoJson = baseUrl + dkanUrl + topologicalNetworkGeoJson[0].datasetUuid;
    var networkResourceUrl;
    var networkGeoJsonResourceUrl;
    var map;
    var layerControl;
    let info;

    var datasetRequest = $.ajax({
        url: fullUrlDataset,
        type: 'GET',
    });

    var networkRequest = $.ajax({
        url: fullUrlNetwork,
        type: 'GET',
    });

    var networkGeoJsonRequest = $.ajax({
        url: fullUrlNetworkGeoJson,
        type: 'GET',
    });

    /*
    * Get response of output's datase request
    *
    * @return Object response response of request
    */
    datasetRequest.then(function (response) {
        var noBaseLineResources = [];
        var baseLineResources = [];
        var baseLineResources = funcOutputs.filter(file => file.fullname.includes("Base_Line"));
        /*
        * Filter in dataset response, the no base
        * line resources & get the file URL
        */
        noBaseLineOutputs.forEach(function (output) {
            // Resources result of dataset request
            var resources = response.result[0].resources;
            // UUID of dataset's resource
            var resourceUuid = output.resourceUuid;
            var filterResource = resources.filter(r => r.id == resourceUuid);
            var resourceUrl = filterResource[0].url;
            var resource = { "name": output.fullname, "url": resourceUrl };
            noBaseLineResources.push(resource);
        });

        // Loop for every output that exists
        funcOutputs.forEach(function (output) {
            var fullname = output.fullname;
            var resourceUuid = output.resourceUuid;
            var resources = response.result[0].resources;
            var filterResource = resources.filter(r => r.id == resourceUuid);
            var resourceUrl = filterResource[0].url;
            /* Identify by name if is no baseline
            */
            switch (fullname) {
                case 'Func_Network_Base_Line':
                    var resource = { "name": fullname, "url": resourceUrl };
                    baseLineResources.push(resource);
                    break;
                case 'Arc_0_Base_Line':
                    var resource = { "name": fullname, "url": resourceUrl };
                    baseLineResources.push(resource);
                    break;
                case 'Install_Power_Base_Line':
                    var resource = { "name": fullname, "url": resourceUrl };
                    baseLineResources.push(resource);
                    break;
            }
        });


        /*
        * Get response of topological network
        * dataset request & get topological
        * network resource URL
        *
        * @return Object response response of request
        */
        networkRequest.then(function (response) {
            // Resources attah to network dataset
            var resources = response.result[0].resources;

            // Filter reource by UUID
            var filterResource = resources.filter(r => r.id == topologicalNetwork[0].resourceUuid);

            // Resource url
            networkResourceUrl = filterResource[0].url;
        });

        /*
        * Get response of topological network GeoJson
        * dataset request & get topological
        * network resource URL
        *
        * @return Object response response of request
        */
        networkGeoJsonRequest.then(function (response) {
            // Resources attah to network dataset
            var resources = response.result[0].resources;

            // Filter reource by UUID
            var filterResource = resources.filter(r => r.id == topologicalNetworkGeoJson[0].resourceUuid);

            // Resource url
            networkGeoJsonResourceUrl = filterResource[0].url;
        });
        var installPowerNoBaseLine = [];
        /* Filter no base installed powers
        */
        for (i = 0; i <= narratives.length - 1; i++) {
            installPowerNoBaseLine.push(noBaseLineResources.filter(out => out.name == "Install_Power_" + narratives[i].nid + "_Narrative_" + narratives[i].name));
        }

        var installPowerNoBaseLineRequest = [];
        installPowerNoBaseLine.forEach(function (resource) {
            var request = $.ajax({
                url: resource[0].url,
                type: 'GET',
            });
            var properties = { "name": resource[0].name, "request": request };
            installPowerNoBaseLineRequest.push(properties);
        });

        // Aleatory definition outputs
        var aleatoryDefinition = [];
        for (i = 0; i <= narratives.length - 1; i++) {
            aleatoryDefinition.push(noBaseLineResources.filter(out => out.name == "Aleatory_Definition_" + narratives[i].nid + "_Narrative_" + narratives[i].name));
        }
        var funcNoBaseLine = [];
        // Filter no base sai
        for (i = 0; i <= narratives.length - 1; i++) {
            funcNoBaseLine.push(noBaseLineResources.filter(out => out.name == "Arc_0_" + narratives[i].nid + "_Narrative_" + narratives[i].name));
        }
        //no base sai request
        var funcNoBaseLineRequest = [];
        funcNoBaseLine.forEach(function (resource) {
            var request = $.ajax({
                url: resource[0].url,
                type: 'GET',
            });
            var properties = { "name": resource[0].name, "request": request };
            funcNoBaseLineRequest.push(properties);
        });
        console.log(funcNoBaseLineRequest);


        // BaseLine install power URL request
        var installPowerBaseLine = baseLineResources.filter(r => r.name == 'Install_Power_Base_Line');
        var installPowerUrlBaseLine = installPowerBaseLine[0].url;

        // BaseLine sai URL request
        var funcBaseLine = baseLineResources.filter(r => r.name == 'Func_Network_Base_Line');
        var funcUrlBaseLine = funcBaseLine[0].url;

         // BaseLine sai URL request
         var arc0BaseLine = baseLineResources.filter(r => r.name == 'Arc_0_Base_Line');
         var arc0UrlBaseLine = arc0BaseLine[0].url;

        // BaseLine Install Power AJAX request
        var installPowerRequestBaseLine = $.ajax({
            url: installPowerUrlBaseLine,
            type: 'GET',
        });

        // BaseLine Func AJAX request
        var FuncRequestBaseLine = $.ajax({
            url: funcUrlBaseLine,
            type: 'GET',
        });

         // BaseLine Arc0 AJAX request
         var arc0RequestBaseLine = $.ajax({
            url: arc0UrlBaseLine,
            type: 'GET',
        });

        var networkResourceRequest = $.ajax({
            url: networkResourceUrl,
            type: 'GET',
        });

        var networkGeoJsonResourceRequest = $.ajax({
            url: networkGeoJsonResourceUrl,
            type: 'GET',
        });


        /* Global variables section */
        var installPowerLines;
        var funcLines;
        var networkLines;
        var baseLinePower;
        var baseLineFunc;
        var nobaseLineFuncCsv;
        var noBaseLinePowerCsv;
        var riverMouthArc;
        var FuncRowFilter;
        var installPowerFilter = [];
        var geoJsonResponse;
        var narrativeNid;
        var narrativeName;
        var geoJsonProjects;
        var featuresArray = [];
        var baseLineArc0=[];
        /*
        * Get response of topological network
        * resource & filter river mouth arc
        *
        * @return Object response response of request
        */
        networkResourceRequest.then(function (response) {
            // Extract lines of csv in array
            networkLines = processCSv(response);
            /*
            * Loop every line & search river mouth arc
            */
            networkLines.forEach(function (row) {
                rowSplit = row[0].split(",");
                if (rowSplit[3] == '1')
                    riverMouthArc = rowSplit[0];
            });

            /*
            * Get response of topological network
            * dataset request & get topological
            * network resource URL
            *
            * @return Object response response of request
            */
            networkGeoJsonResourceRequest.then(function (response) {
                geoJsonResponse = response;
                /* Loop every install power resource
                */
                installPowerNoBaseResponse = [];

                installPowerNoBaseLineRequest.forEach(function (resource) {
                    var name = resource.name.split("_");
                    narrativeNid = name[2];
                    narrativeName = name[4];
                    resource.request.then(function (response) {
                        var noBaseResponse = Object();
                        noBaseResponse.response = processCSv(response);
                        noBaseResponse.nid = narrativeNid;
                        noBaseResponse.name = narrativeName;
                        installPowerNoBaseResponse.push(noBaseResponse);
                    })
                });

                var funcNoBaseLineResponse = [];
                funcNoBaseLineRequest.forEach(function (resource) {
                    var name = resource.name.split("_");
                    narrativeNid = name[1];
                    narrativeName = name[3];
                    resource.request.then(function (response) {
                        var noBaseResponse = Object();
                        noBaseResponse.nid = narrativeNid;
                        noBaseResponse.name = narrativeName;
                        noBaseResponse.response = processCSv(response);
                        funcNoBaseLineResponse.push(noBaseResponse);
                    })
                });

                installPowerRequestBaseLine.then(function (response) {
                    // Extract lines of csv in array
                    baseLinePower = processCSv(response);
                });

                FuncRequestBaseLine.then(function (response) {
                    // Extract lines of csv in array
                    baseLineFunc = processCSv(response);
                });

                arc0RequestBaseLine.then(function (response) {
                    // Extract lines of csv in array
                    baseLineArc0 = processCSv(response);
                });

                //Set sai CSV & Install Power CSV response
                setGlobalSources(funcNoBaseLineResponse, installPowerNoBaseResponse, baseLineFunc,baseLineArc0, baseLinePower, riverMouthArc);
                var year = baseLinePower[0];
                var baseLineYears = year[0].split(",");
                riverMouthArc = 0;
                var baseLineFuncAxis = filterBySaiArc(riverMouthArc, baseLineArc0);
                var basePowerAxis = filterInstallPower(baseLinePower);

                var nobaseLineFuncAxis = [];
                funcNoBaseLineResponse.forEach(function (sai) {
                    var saiAxis = Object();
                    saiAxis.axis = filterBySaiArc(riverMouthArc, sai.response);
                    saiAxis.name = sai.name;
                    saiAxis.model = "SAI";
                    saiAxis.nid = sai.nid;
                    nobaseLineFuncAxis.push(saiAxis);
                });


                var noBasePowerAxis = [];
                installPowerNoBaseResponse.forEach(function (power) {
                    var powerAxis = Object();
                    powerAxis.axis = filterInstallPower(power.response);
                    powerAxis.name = power.name;
                    noBasePowerAxis.push(powerAxis);
                });

                var geoJsonProjects = new Object();
                // Type properties set
                geoJsonProjects.type = geoJsonResponse.type;
                var propertiesName = Object();

                //Name key inside of properties
                propertiesName.name = geoJsonResponse.crs.properties.name;

                var properties = Object();

                // Properties key inside of crs
                properties.properties = propertiesName;


                //Crs properties set
                geoJsonProjects.crs = properties;
                geoJsonProjects.crs.type = geoJsonResponse.crs.type;

                var projectsRange = getProjectsRank(narrativesProjects);
                narrativesProjects.forEach(function (project) {
                    // Object than contains coordinates X,Y
                    var coordinates = Object();

                    // Array with the coordinates
                    var coordinatesArray = [project.xcoord, project.ycoord];

                    // Asign coordinates to the object
                    coordinates.coordinates = coordinatesArray;

                    coordinates.type = "Point";

                    // Global objecto for each feature in the GeoJSON
                    var featureObject = Object();

                    // Geometry property of features
                    featureObject.geometry = coordinates;

                    var projectRank = setProjectRadius(parseFloat(project.power), projectsRange);
                    // Properties object for properties in features
                    var properties = Object();
                    properties.name = project.name;
                    properties.power = project.power;
                    properties.reservoirHight = project.reservoirHight;
                    properties.rank = projectRank;
                    properties.color = project.color;
                    properties.narrativa = project.narrativa;
                    properties.nid = project.narrativaNid;

                    // Asign  type to features
                    featureObject.type = geoJsonResponse.features[0].type;

                    // Asign  type to features
                    featureObject.properties = properties;

                    // Add feature objecto to array
                    featuresArray.push(featureObject);
                });

                var features = Object();
                features = featuresArray;
                geoJsonProjects.features = features;
                //Set DOR CSV & Install Power CSV response
                setGlobalSources(geoJsonResponse, geoJsonProjects, funcNoBaseLineResponse, installPowerNoBaseResponse, baseLineFunc, baseLinePower, riverMouthArc);
                var num = 0;
                var topologicalRanks = getTopologicalRank(baseLineFunc);

                /*
                * Set ArcID value in base of
                base line DOR last year values
                */
                baseLineFunc.forEach(function (sai, index) {
                    if (index == 0) { //Header's row
                        row = sai[0].split(",");
                        baseLineYear = row[row.length - 1];
                    }
                    else {
                        saiRow = sai[0].split(",");
                        rating = setTopoColor(parseFloat(saiRow[row.length - 1]), topologicalRanks);
                        geoJsonResponse.features[num].properties.color = rating.color;
                        geoJsonResponse.features[num].properties.rank = rating.rank;
                        //var topoLine = geoJsonResponse.features.filter(line => line.properties.ID == dorRow.)
                        num++;
                    }
                });
                actualYAxis = nobaseLineFuncAxis;
                callMap(geoJsonResponse, geoJsonProjects);
                modelChart(basePowerAxis, baseLineFuncAxis, baseLineYears, noBasePowerAxis, nobaseLineFuncAxis);
            }); //TOPOLOGICAL GEOJSON NETWORK REQUEST END

        });  //TOPOLOGICAL NETWORK REQUEST END

    });   //DATASET REQUEST END

    /*
    * Get install power of all projects
    * & set quintiles & configure ranks
    * network resource URL
    *
    * @param  Array projects  project list
    * @return Array rankValues list of rank
    */
    function getProjectsRank(projects) {
        var power = [];
        var rankValues = [];
        projects.forEach(function (project) {
            power.push(parseInt(project.power));
        })
        maxPower = Math.max.apply(null, power);
        minPower = Math.min.apply(null, power);
        promValue = maxPower / 5;
        var rank = Object();
        rank.min = parseFloat(minPower);
        rank.max = parseFloat(promValue);
        rankValues[0] = rank;
        for (i = 1; i <= 4; i++) {
            var rank = Object();
            rankValue = rankValues[i - 1].max + promValue;
            rank.min = parseFloat(rankValues[i - 1].max);
            rank.max = parseFloat(rankValue);
            rankValues.push(rank);
        }
        return rankValues;
    }

    /*
    * Get install power of all projects
    * & set quintiles & configure ranks
    * network resource URL
    *
    * @param  Array projects  project list
    * @return Array rankValues list of rank
    */
    function getTopologicalRank(arcs) {
        var values = [];
        var rankValues = [];
        arcs.forEach(function (arc, index) {
            if (index > 0) {
                var row = arc[0].split(",");
                if (parseFloat(row[4]) < 0)
                    row[4] = 0;
                values.push(parseFloat(row[4]));
            }
        });
        maxValue = Math.max.apply(null, values);
        minValue = Math.min.apply(null, values);
        promValue = maxValue / 4;
        var rank = Object();
        rank.min = parseFloat(minValue);
        rank.max = parseFloat(promValue);
        rankValues[0] = rank;
        for (i = 1; i <= 3; i++) {
            var rank = Object();
            rankValue = rankValues[i - 1].max + promValue;
            rank.min = parseFloat(rankValues[i - 1].max);
            rank.max = parseFloat(rankValue);
            rankValues.push(rank);
        }
        return rankValues;
    }

    /*
    * Get install power of all projects
    * & set quintiles & configure ranks
    * network resource URL
    *
    * @param  Array projects  project list
    * @return Array rankValues list of rank
    */
    function getModelRanks(arcs) {
        var values = [];
        var rankValues = [];
        maxValue = Math.max.apply(null, arcs);
        minValue = Math.min.apply(null, arcs);
        promValue = maxValue / 4;
        var rank = Object();
        rank.min = parseFloat(minValue);
        rank.max = parseFloat(promValue);
        rankValues[0] = rank;
        for (i = 1; i <= 3; i++) {
            var rank = Object();
            rankValue = rankValues[i - 1].max + promValue;
            rank.min = parseFloat(rankValues[i - 1].max);
            rank.max = parseFloat(rankValue);
            rankValues.push(rank);
        }
        return rankValues;
    }


    /*
    * In base of projects rank, assign
    * five size of radius for projects
    *
    * @param  Float power      project install power
    * @param  Array rankValues list of rank
    * @return Int   rank       radius value
    */
    function setProjectRadius(power, rankValues) {
        var rank = 0;
        rankValues.forEach(function (r, index) {
            if (power >= r.min && power <= r.max)
                rank = index;
        });
        switch (rank) {
            case 0:
                rank = 3;
                break;
            case 1:
                rank = 5;
                break;
            case 2:
                rank = 7;
                break;
            case 3:
                rank = 9;
                break;
            case 4:
                rank = 11;
                break;
            default:
                break;
        }
        return rank;
    }

    /*
    * In base of DOR value rank, assign
    * four rank of color for topo network
    *
    * @param  Float power      project install power
    * @param  Array rankValues list of rank
    * @return Int   rank       radius value
    */
    function setTopoColor(value, rankValues) {
        var rank = 0;
        rankValues.forEach(function (r, index) {
            if (value >= r.min && value <= r.max) {
                rank = index;
            }
        });
        switch (rank) {
            case 0:
                var rating = {};
                rating.color = "#0f0";
                rating.rank = "Bueno"
                break;
            case 1:
                var rating = {};
                rating.color = "#00f";
                rating.rank = "Regular"
                break;
            case 2:
                var rating = {};
                rating.color = "#ff0";
                rating.rank = "Escaso"
                break;
            case 3:
                var rating = {};
                rating.color = "#f00";
                rating.rank = "Malo"
                break;
            default:
                break;
        }
        return rating;
    }

    function checkModelMode(selector) {
        selectValue = parseInt(selector.val());
        return selectValue;
    }
    /* Mode no base line or base line
    *  listener
    */
    var narrativeSelect = $('#narrativeMode');
    narrativeSelect.change(changeMode);

    /* Mode no base line or base line
    *  listener
    */
    var modelSelect = $('#modelSelect');
    modelSelect.change(changeMode);

    function changeMode() {
        var narrativeSelector = $('#narrativeMode');
        var narrativeSelect = narrativeSelector[0].value;
        // Validate if is BaseLine or not
        if (narrativeSelect == "1") { //All narratives
            var nobaseLineFuncAxis = [];
            nobaseLineFuncCsv.forEach(function (sai) {
                var saiAxis = Object();
                saiAxis.axis = filterBySaiArc(riverMouthArc, sai.response);
                saiAxis.name = sai.name;
                nobaseLineFuncAxis.push(saiAxis);
            });


            var noBasePowerAxis = [];
            installPowerNoBaseResponse.forEach(function (power) {
                var powerAxis = Object();
                powerAxis.axis = filterInstallPower(power.response);
                powerAxis.name = power.name;
                noBasePowerAxis.push(powerAxis);
            });

            var baseLineFuncAxis = filterBySaiArc(riverMouthArc, baseLineFuncCsv);
            var basePowerAxis = filterInstallPower(baseLinePower);
            var year = baseLinePower[0];
            var baseLineYears = year[0].split(",");
            var num = 0;
            var topologicalRanks = getTopologicalRank(baseLineFuncCsv);
            /*
            * Set ArcID value in base of
            base line DOR last year values
            */
            baseLineFuncCsv.forEach(function (sai, index) {
                if (index == 0) { //Header's row
                    row = sai[0].split(",");
                    baseLineYear = row[row.length - 1];
                }
                else {
                    saiRow = sai[0].split(",");
                    rating = setTopoColor(parseFloat(saiRow[row.length - 1]), topologicalRanks);
                    networkGeoJson.features[num]
                    networkGeoJson.features[num].properties.color = rating.color;
                    networkGeoJson.features[num].properties.rank = rating.rank;
                    //var topoLine = geoJsonResponse.features.filter(line => line.properties.ID == dorRow.)
                    num++;
                }
            });
            modelChart(basePowerAxis, baseLineFuncAxis, baseLineYears, noBasePowerAxis, nobaseLineFuncAxis);
            updateMap(networkGeoJson, projectsGeoJson);
        }
        else {
            var nobaseLineFuncAxis = [];
            noBaseSaiFilter = nobaseLineFuncCsv.filter(sai => sai.nid == narrativeSelect);
            noBaseSaiFilter.forEach(function (sai) {
                var saiAxis = Object();
                saiAxis.axis = filterBySaiArc(riverMouthArc, sai.response);
                saiAxis.name = sai.name;
                nobaseLineFuncAxis.push(saiAxis);
            });
            var noBasePowerAxis = [];
            noBasePowerFilter = installPowerNoBaseResponse.filter(power => power.nid == narrativeSelect);
            noBasePowerFilter.forEach(function (power) {
                var powerAxis = Object();
                powerAxis.axis = filterInstallPower(power.response);
                powerAxis.name = power.name;
                noBasePowerAxis.push(powerAxis);
            });
            var baseLineFuncAxis = filterBySaiArc(riverMouthArc, baseLineFuncCsv);
            var basePowerAxis = filterInstallPower(baseLinePower);
            var year = baseLinePower[0];
            var baseLineYears = year[0].split(",");
            var projects = projectsGeoJson.features.filter(project => project.properties.nid == narrativeSelect);
            updateMap(networkGeoJson, projects);
            modelChart(basePowerAxis, baseLineFuncAxis, baseLineYears, noBasePowerAxis, nobaseLineFuncAxis);

        }
    }


    /*
    * Update chart Yaxis when user's
    * clic on Arc in the Map
    *
    * @param    Object  e   object selected
    */
    function updateChart(e) {
        var layer = e.target;
        var arcId = layer.feature.properties.ID;
        var selectorNarrative = $('#narrativeMode');
        var narrativeSelect = selectorNarrative[0].value;
        // Validate if is BaseLine or not
        if (narrativeSelect == "1") { //All narratives
            var nobaseLineFuncAxis = [];
            nobaseLineFuncCsv.forEach(function (sai) {
                var saiAxis = Object();
                saiAxis.axis = filterBySaiArc(arcId, sai.response);
                saiAxis.name = sai.name;
                nobaseLineFuncAxis.push(saiAxis);
            });

            var noBasePowerAxis = [];
            installPowerNoBaseResponse.forEach(function (power) {
                var powerAxis = Object();
                powerAxis.axis = filterInstallPower(power.response);
                powerAxis.name = power.name;
                noBasePowerAxis.push(powerAxis);
            });
            var baseLineFuncAxis = filterBySaiArc(arcId, baseLineFuncCsv);
            var basePowerAxis = filterInstallPower(baseLinePower);
            var year = baseLinePower[0];
            var baseLineYears = year[0].split(",");
            modelChart(basePowerAxis, baseLineFuncAxis, baseLineYears, noBasePowerAxis, nobaseLineFuncAxis);
        }
        else {
            var nobaseLineFuncAxis = [];
            noBaseSaiFilter = nobaseLineFuncCsv.filter(sai => sai.nid == narrativeSelect);
            noBaseSaiFilter.forEach(function (sai) {
                var saiAxis = Object();
                saiAxis.axis = filterBySaiArc(arcId, sai.response);
                saiAxis.name = sai.name;
                nobaseLineFuncAxis.push(saiAxis);
            });
            var noBasePowerAxis = [];
            noBasePowerFilter = installPowerNoBaseResponse.filter(power => power.nid == narrativeSelect);
            noBasePowerFilter.forEach(function (power) {
                var powerAxis = Object();
                powerAxis.axis = filterInstallPower(power.response);
                powerAxis.name = power.name;
                noBasePowerAxis.push(powerAxis);
            });
            var baseLineFuncAxis = filterBySaiArc(arcId, baseLineDorCsv);
            var basePowerAxis = filterInstallPower(baseLinePower);
            var year = baseLinePower[0];
            var baseLineYears = year[0].split(",");
            modelChart(basePowerAxis, baseLineFuncAxis, baseLineYears, noBasePowerAxis, nobaseLineFuncAxis);

        }
    }

    /*
    * Take xAxis & yAxis from model's outputs
    * & draw a graph with projects whitout
    * operation year
    *
    * @param    Array   baseLineXAxis    install power range for base line
    * @param    Array   baseLineYAxis    models file (DOR)
    * @param    Array   baseLineYears    years range in base line
    * @param    Array   noBaseLineXaxis  install power range for no base line
    * @param    Array   noBaseLineYaxis  models file (DOR) for no base line
    */
    function modelChart(baseLineXAxis, baseLineYAxis, baseLineYears, noBaseLineXaxis, noBaseLineYaxis) {
        var traces = [];
        var data = [];
        for (i = 0; i <= noBaseLineXaxis.length - 1; i++) {
            var trace = {
                x: noBaseLineXaxis[i].axis,
                y: noBaseLineYaxis[i].axis,
                uid: noBaseLineYaxis[i].nid,
                mode: 'markers',
                name: "Narrativa " + noBaseLineXaxis[i].name,
                marker: { size: 12 }
            };
            traces.push(trace);
        }

        var traceBaseLine = {
            x: baseLineXAxis,
            y: baseLineYAxis,
            name: "Linea Base",
            mode: 'lines+markers',
            text: baseLineYears,
            marker: { size: 12 }
        };
        traces.push(traceBaseLine);
        traces.forEach(function (trace) {
            data.push(trace);
        });

        // Source of the graph
        //var data = [trace1];

        var layout = {
            title: {
                text: 'Salidas Modelo Fragmentación',
                font: {
                    family: 'Courier New, monospace',
                    size: 24
                }
            },
            showlegend: true,
            xaxis: {
                title: {
                    text: 'Potencia Instalada (MW)',
                    font: {
                        family: 'Courier New, monospace',
                        size: 18,
                        color: '#7f7f7f'
                    }
                },
            },
            yaxis: {
                title: {
                    text: 'Alteración de Seimentos (%)',
                    font: {
                        family: 'Courier New, monospace',
                        size: 18,
                        color: '#7f7f7f'
                    }
                }
            }
        };
        Plotly.newPlot('chart', data, layout, { showSendToCloud: true });
        var myPlot = document.getElementById("chart");
        myPlot.on('plotly_click', chartClick);

    }
    function chartClick(evt) {
        var narrativeNid = evt.points[0].data.uid;
        var pointPosition = evt.points[0].pointIndex;
        console.log(evt);
        var nobaseLineFuncAxis = [];
        var noBaseSaiFilter = nobaseLineFuncCsv.filter(sai => sai.nid == narrativeNid)
        noBaseSaiFilter.forEach(function (sai) {
            var saiAxis = Object();
            saiAxis.axis = filterByNarrative(pointPosition, sai.response);
            saiAxis.name = sai.name;
            nobaseLineFuncAxis.push(saiAxis);
        });
        var ranks = getModelRanks(nobaseLineFuncAxis[0].axis);
        console.log(nobaseLineFuncAxis);
        var num = 0;
        /*
        * Set ArcID value in base of
        base line DOR last year values
        */
        nobaseLineFuncAxis[0].axis.forEach(function (sai, index) {
            rating = setTopoColor(sai, ranks);
            networkGeoJson.features[index]
            networkGeoJson.features[index].properties.color = rating.color;
            networkGeoJson.features[index].properties.rank = rating.rank;
            //var topoLine = geoJsonResponse.features.filter(line => line.properties.ID == dorRow.)
            num++;
        });
        //modelChart(basePowerAxis, baseLineFuncAxis, baseLineYears, noBasePowerAxis, nobaseLineFuncAxis);
        updateMap(networkGeoJson, projectsGeoJson);
    }

    /*
    * Set global variables for DOR csv file
    * & Install Power csv to use it outside
    * asyncronous callback
    *
    * @param    Object   dorLines           DOR response
    * @param    Object   installPowerLines  Install Power response
    */
    function setGlobalSources(geoJsonResponse, geoJsonProjects, noBaseFuncLines, noBasePowerLines, baseLineFunc,baseLine0Arc, baseLinePowerLines, arcId) {
        // No BaseLine global CSV's files
        nobaseLineFuncCsv = noBaseFuncLines;
        noBaseLinePowerCsv = noBasePowerLines;
        // BaseLine global CSV's files
        baseLineFuncCsv = baseLineFunc;
        baseLineArc0Csv=baseLine0Arc;
        baseLinePower = baseLinePowerLines;
        networkGeoJson = geoJsonResponse;
        projectsGeoJson = geoJsonProjects;
        riverMouthArc = arcId;
    }

    /*
    * Get row in Installed Power Csv file
    *
    * @param  Array   installPower    install power file
    */
    function filterInstallPower(installPower) {
        var rowSplit;
        var installRow = installPower[1];
        var row = JSON.stringify(installRow);
        var row = row.replace(/['"]+/g, '');
        row = row.replace(/[[\]]/g, '');
        installPowerFilter = row.split(",");
        installPowerFilter.forEach(function (value, index) {
            installPowerFilter[index] = parseFloat(value);
        });
        return installPowerFilter;
    }

    /* Get row in DOR Csv file
    *  filtering by ArcID
    *
    * @param    String   riverMouthArc    ArcID
    * @param    Array    dorFile          DOR file
    */
    function filterBySaiArc(riverMouthArc, dorFile) {
        var dorRow = [];
        var dorRowFilter = [];
        /*
        * Loop every line &
        * search river mouth in DOR output
        */
        dorFile.forEach(function (row) {
            // Split columns in the string
            rowSplit = row[0].split(",");
            dorArc = rowSplit[0];
            if (dorArc == riverMouthArc) {
                dorRowFilter = rowSplit;

                // Convert object to string
                var row = JSON.stringify(dorRowFilter);

                // Replace double quotes
                var row = row.replace(/['"]+/g, '');
                // Replace brackets
                row = row.replace(/[[\]]/g, '');

                // Split each column by comma
                dorRowFilter = row.split(",");

                // Loop and convert every value in float
                dorRowFilter.forEach(function (value, index) {
                    dorRowFilter[index] = parseFloat(value);
                });
            }
        });
        /* First position in array is ArcID, is no necesary
        *  so recreate array whitout the first position
        */
        for (i = 1; i <= dorRowFilter.length - 1; i++) {
            dorRow.push(dorRowFilter[i]);
        }
        return dorRow;
    }

    /* Get row in SAI Csv file
    *  filtering by ArcID
    *
    * @param    String   riverMouthArc    ArcID
    * @param    Array    dorFile          DOR file
    */
    function filterByNarrative(positionIndex, dorFile) {
        var dorRow = [];
        var dorRowFilter = [];
        /*
        * Loop every line &
        * search river mouth in DOR output
        */
        dorFile.forEach(function (row, index) {
            // Split columns in the string
            rowSplit = row[0].split(",");

            if (index > 0) {
                dorRowFilter = rowSplit;

                // Convert object to string
                var row = JSON.stringify(dorRowFilter);

                // Replace double quotes
                var row = row.replace(/['"]+/g, '');
                // Replace brackets
                row = row.replace(/[[\]]/g, '');

                // Split each column by comma
                dorRowFilter = row.split(",");


                dorRow.push(parseFloat(dorRowFilter[positionIndex + 1]));
                // Loop and convert every value in float

            }
        });
        /* First position in array is ArcID, is no necesary
        *  so recreate array whitout the first position
        */
        console.log(dorRow);
        return dorRow;
    }

    /*
    * Create map and read GeoJSON topological
    * network dataset and add it to the map
    *
    * @param    Object   geoJson  Topological Network
    */
    function callMap(geoJson, geoJsonProjects) {
        // initalize leaflet map
      var map = L.map('map');
      // add OpenStreetMap basemap
      L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
          attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
      }).addTo(map);
      var url_to_geotiff_file = "http://test.sima/sites/default/files/Area_1_4326_0.tiff";
      fetch(url_to_geotiff_file)
        .then(response => response.arrayBuffer())
        .then(arrayBuffer => {
          parseGeoraster(arrayBuffer).then(georaster => {
            console.log("georaster:", georaster);
            /*
                GeoRasterLayer is an extension of GridLayer,
                which means can use GridLayer options like opacity.
                Just make sure to include the georaster option!
                http://leafletjs.com/reference-1.2.0.html#gridlayer
            */
            var layer = new GeoRasterLayer({
                georaster: georaster,
                opacity: 0.7
            });
            layer.addTo(map);
            map.fitBounds(layer.getBounds());
        });
      });

    }

    /*
    * Create map and read GeoJSON topological
    * network dataset and add it to the map
    *
    * @param    Object   geoJson  Topological Network
    */
    function updateMap(geoJson, geoJsonProjects) {
        if (map) {
            map.eachLayer(function (layer) {
                map.removeLayer(layer);
            });
        }
        /*
        * Reset line of toplogical network
        * style when mouse out of network
        *
        * @param    Object  e   object selected
        */
        function resetHighlight(e) {
            let layer = e.target;
            topoNetwork.resetStyle(layer);
            info.update();
        }

        /*
        * Highlight line selected in
        * mouseover
        *
        * @param    Object  e   object selected
        */
        function highlightFeature(e) {
            let layer = e.target;
            let current_style = {
                weight: 3,
                color: '#050666',
                fillOpacity: 0.5
            };

            layer.setStyle(current_style);

            if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
                layer.bringToFront();
            } e

            info.update(layer.feature.properties);
        }
        // Topological Network layer
        var topoNetwork;
        topoNetwork = L.geoJson(geoJson, {
            onEachFeature: onEachFeature,
            style: function (feature) {
                return { color: feature.properties.color };
            }
        }).addTo(map);

        //Projects layer
        var projectsLayer;
        projectsLayer = L.geoJson(geoJsonProjects, {
            onEachFeature: onEachPoint,
            pointToLayer: function (feature, latlng) {
                return L.circleMarker(latlng, {
                    radius: feature.properties.rank,
                    fillColor: feature.properties.color,
                    color: "#2F2D2D",
                    weight: 1,
                    opacity: 1,
                    fillOpacity: 1
                });
            }
        }).addTo(map);

        // Web map code goes here
        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', { attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>' }).addTo(map);
        var baseLayers = {
            "Red topológica": topoNetwork,

        };
        var overlays = {
            "Proyectos": projectsLayer
        };
        layerControl.remove();
        info.remove();
        //Control box
        info = L.control();
        layerControl = L.control.layers(baseLayers, overlays, { position: 'topleft' }).addTo(map);

        //Creates the control label
        info.onAdd = function (map) {
            this._div = L.DomUtil.create('div', 'info'); // create a div with a
            // class "info"
            this.update();
            return this._div;
        };

        //Method that we will use to update the control based on feature properties
        // passed
        info.update = function (props) {
            this._div.innerHTML = '<h4>Arcos de la Red</h4>' + (props ?
                'ArcID: <b>' + props.ID
                : '<b>Haga clic sobre un arco de la red para actualizar');
        };
        info.addTo(map);
        function onEachFeature(feature, layer) {
            layer.on({
                mouseover: highlightFeature,
                mouseout: resetHighlight,
                click: updateChart
            });
        }
        function onEachPoint(feature, layer) {
            var popupContent = "<h5>Proyecto </h5>" +
                "<b>Nombre: </b>" + feature.properties.name + "<br />" +
                "<b>Potencia Instalada: </b>" + feature.properties.power + "<br />" +
                "<b>Altura de la presa: </b>" + feature.properties.reservoirHight + "<br />" +
                "<b>Narrativa: </b>" + feature.properties.narrativa;

            if (feature.properties && feature.properties.popupContent) {
                popupContent += feature.properties.popupContent;
            }

            layer.bindPopup(popupContent);
        }

    }
    /*
    * Read a CSV file & extract the content in rows
    *
    * @param String csv    CSV File
    *
    * @return Array lines  Rows of csv readed
    */
    function processCSv(csv) {
        var allTextLines = csv.split(/\r\n|\n/);
        var lines = [];
        for (var i = 0; i < allTextLines.length - 1; i++) {
            var data = allTextLines[i].split(';');
            var tarr = [];
            for (var j = 0; j < data.length; j++) {
                tarr.push(data[j]);
            }
            lines.push(tarr);
        }
        return lines;
    }
});
