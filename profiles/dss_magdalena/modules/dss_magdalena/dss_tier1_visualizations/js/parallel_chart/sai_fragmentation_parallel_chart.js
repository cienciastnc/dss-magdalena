jQuery(document).ready(function ($) {
  var narratives = JSON.parse(narrativeList);
  $(".mi-selector").select2();
  var narrativeDropdown = $("#narrativeMode");
  narrativeDropdown.change(changeMode);
  var modelSelect = $('#modelSelect');
  modelSelect.change(changeMode);
  narrativeDropdown.change(changeMode);
  narratives.forEach(function (narrative) {
    narrativeDropdown.append('<option selected value=' + narrative.fullname + '> Narrativa ' + narrative.name + '</option>');
  });
  var Outputs = JSON.parse(outputs);
  var baseLineResources = Outputs.filter(file => file.fullname.includes("Base_Line"));

  if (baseLineResources.length > 0)
    baseLine = true
  else
    baseLine = false;

 

  
  var topologicalNetwork = JSON.parse(topoNetwork);
  var baseUrl = window.location.protocol + "//" + window.location.hostname+":"+window.location.port;
  var dkanUrl = "/api/3/action/package_show?id=";
  var fullUrlDataset = baseUrl + dkanUrl + datasetUuid;
  var fullUrlNetwork = baseUrl + dkanUrl + topologicalNetwork[0].datasetUuid;
  var scale=['#DA77BF','#0477BF','#1ABE39','#F0BE39','#F092EC','#E4EF26','#DDC406','#8872C9','#DC1E90','#DCF890'];
  var networkResourceUrl;
  var noBaseLineFragwAxis = []
  var noBaseLineSaiAxis = []
  var layout = {
    font: {
      size: 16,
    }
  }; 
 

  var datasetRequest = $.ajax({
    url: fullUrlDataset,
    type: 'GET',
  });

  var networkRequest = $.ajax({
    url: fullUrlNetwork,
    type: 'GET',
  });

  /*
  * Get response of output's datase request
  *
  * @return Object response response of request
  */
  datasetRequest.then(function (response) {
    var noBaseLineResources = [];
    var noBaseLineOutputs = [];
    /*
    * Filter no base line outputs by
    * word "narrativa" & the number
    */
   //console.log("Narrativas",narratives);
    for (i = 0; i <= narratives.length - 1; i++) {
      noBaseLineOutputs.push(Outputs.filter(out => out.fullname == "SAI_" + narratives[i].nid + "_Narrative_" + narratives[i].fullname));
      noBaseLineOutputs.push(Outputs.filter(out => out.fullname == "Arc_0_" + narratives[i].nid + "_Narrative_" + narratives[i].fullname));
    }
    /*
    * Filter in dataset response, the no base
    * line resources & get the file URL
    */
    noBaseLineOutputs.forEach(function (output) {
      // Resources result of dataset request
      var resources = response.result[0].resources;
      //console.log("Resources",resources);
      // UUID of dataset's resource
      var resourceUuid = output[0].resourceUuid;
      var filterResource = resources.filter(r => r.id == resourceUuid);
      var resourceUrl = filterResource[0].url;
      var resource = { "name": output[0].fullname, "url": resourceUrl };
      noBaseLineResources.push(resource);
    });

    /*
   * Get response of topological network
   * dataset request & get topological
   * network resource URL
   *
   * @return Object response response of request
   */
    networkRequest.then(function (response) {
      // Resources attah to network dataset
      var resources = response.result[0].resources;

      // Filter reource by UUID
      var filterResource = resources.filter(r => r.id == topologicalNetwork[0].resourceUuid);

      // Resource url
      networkResourceUrl = filterResource[0].url;
    });

    var networkResourceRequest = $.ajax({
      url: networkResourceUrl,
      type: 'GET',
    });
    
    var saiNoBaseline = [];
    for (i = 0; i <= narratives.length - 1; i++) {
      saiNoBaseline.push(noBaseLineResources.filter(out => out.name == "SAI_" + narratives[i].nid + "_Narrative_" + narratives[i].fullname));
    }
    var funcNoBaseLine = [];
    // Filter no base sai
    for (i = 0; i <= narratives.length - 1; i++) {
      funcNoBaseLine.push(noBaseLineResources.filter(out => out.name == "Arc_0_" + narratives[i].nid + "_Narrative_" + narratives[i].fullname));
    }
    //console.log("funcNoBaseLine",funcNoBaseLine)
    var saiNoBaseLineRequest = [];
    saiNoBaseline.forEach(function (resource) {
      var request = $.ajax({
        url: resource[0].url,
        type: 'GET',
      });
      var properties = { "name": resource[0].name, "request": request };
      saiNoBaseLineRequest.push(properties);
    });
    var funcNoBaseLineRequest = [];
    funcNoBaseLine.forEach(function (resource) {
      var request = $.ajax({
        url: resource[0].url,
        type: 'GET',
      });
      var properties = { "name": resource[0].name, "request": request };
      funcNoBaseLineRequest.push(properties);
    });
    //console.log("funcNoBaseLineRequest",funcNoBaseLineRequest);
    /* Global variables section */
    var networkLines;
    var riverMouthArc;
    var narrativeNid;
    var narrativeName;

    /*
   * Get response of topological network
   * resource & filter river mouth arc
   *
   * @return Object response response of request
   */
  //lectura de red topologica
    networkResourceRequest.then(function (response) {
      // Extract lines of csv in array
      networkLines = processCSv(response);
      /*
      * Loop every line & search river mouth arc
      */
      networkLines.forEach(function (row) {
        rowSplit = row[0].split(",");
        if (rowSplit[3] == '1')
          riverMouthArc = rowSplit[0];
      });
      var promisesSai = [];
      var saiNoLine = [];
      saiNoBaseLineRequest.forEach(function (resource) {
        var nid = resource.name.split("_");
        narrativeNid = nid[1];
        var name = resource.name.split("_Narrative_");
        narrativeName = name[1];
        promisesSai.push(resource.request);
        var noBaseResponse = Object();
        noBaseResponse.nid = narrativeNid;
        noBaseResponse.name = narrativeName;
        saiNoLine.push(noBaseResponse);
      });
      var funcNoBaseLineResponse = [];
        funcNoBaseLineRequest.forEach(function (resource) {
          var nid = resource.name.split("_");
          narrativeNid = nid[1];
          var name = resource.name.split("_Narrative_");
          narrativeName = name[1];
          resource.request.then(function (response) {
            var noBaseResponse = Object();
            noBaseResponse.nid = narrativeNid;
            noBaseResponse.name = narrativeName;
            noBaseResponse.response = processCSv(response);
            funcNoBaseLineResponse.push(noBaseResponse);
          })
        });
        //console.log("funcNoBaseLineResponse",funcNoBaseLineResponse)


      // When all promises all resolved
    
      Promise.all(promisesSai).then(promiseResponse => {
        //console.log(promiseResponse);
        saiNoLine.forEach(function (response, index) {
          response.response = processCSv(promiseResponse[index]);
          var saiAxis = Object();
          saiAxis.axis = filterByDorArc(riverMouthArc, response.response);
          saiAxis.name = response.name;
          saiAxis.model = "SAI";
          saiAxis.nid = response.nid;
          noBaseLineSaiAxis.push(saiAxis);
        });
        //console.log('noBaseLineSaiAxis',noBaseLineSaiAxis);
        Promise.all(funcNoBaseLineResponse).then(promiseResponse => {
          //console.log(promiseResponse);
          funcNoBaseLineResponse.forEach(function (response, index) {
            //console.log("response",response)
            var FragmentacionAxis = Object();
            FragmentacionAxis.axis = filterByDorArc("0", response.response);
            FragmentacionAxis.name = response.name;
            FragmentacionAxis.model = "Fragmentación";
            FragmentacionAxis.nid = response.nid;
            noBaseLineFragwAxis.push(FragmentacionAxis);
          });
          //console.log(noBaseLineFragwAxis);
          parallelChart(noBaseLineSaiAxis,noBaseLineFragwAxis);
        });
      });
      


    });
  });
  function changeMode(){

    var narrativasselected=[];
    var narrativa;
    var finalsai=[];
    var finalfrag=[];
    var selectednarra=narrativeDropdown[0].selectedOptions;
    
    for(var i=0; i<selectednarra.length;i++){
      narrativa=[];
      narrativasselected.push(selectednarra[i].value);
    }
    if(narrativasselected.length==0){
      alert("Se requiere de mínimo 1 narrativa para mostrar el diagrama");
      
    }
    else{
      for(var i=0; i<narrativasselected.length;i++){
        //console.log(narrativasselected,i);
        noBaseLineSaiAxis.filter(res=> {
          if(res.name==narrativasselected[i]){
            finalsai.push(res);
          }
          else{
          }});
      }
      for(var i=0; i<narrativasselected.length;i++){
        console.log(narrativasselected,i);
        noBaseLineFragwAxis.filter(res=> {
          if(res.name==narrativasselected[i]){
            finalfrag.push(res);
          }
          else{
          }});
      }
      parallelChart(finalsai,finalfrag);
    }
  }
  function unpack(rows, key) {
    
    return rows.value  ;
  }
  function parallelChart(finalsai, finalfrag) {
    var values = [];
    var ticktext = [];
    var tickvals = [];
    var valuesSaiAxis = [];
    var valuesFragAxis = [];
    var valuesNum = finalsai[0].axis.length;
    for (i = 1; i <=finalsai.length; i++) {
      for (j = 1; j <= valuesNum; j++) {
        values.push(i);
      }
    }
    finalsai.forEach(function (sai, indexDor) {
     
      sai.axis.forEach(function (axis, indexAxis) {
        valuesSaiAxis.push(axis);
        valuesFragAxis.push(finalfrag[indexDor].axis[indexAxis]);
      });
      ticktext.push(sai.name);
      tickvals.push(indexDor + 1);
    });
    var dimensionsObject = {};
    var dimensions = [];
    dimensionsObject.label = "Narrativas";
    dimensionsObject.ticktext = ticktext;
    dimensionsObject.tickvals = tickvals;
    dimensionsObject.value = values;
    dimensions.push(dimensionsObject);
    var  scalecolor=[];
    scalecolor = new Array(dimensions[0].ticktext.length);
    for(let indexi=0;indexi<scalecolor.length;indexi++){
      scalecolor[indexi] = [];
      scalecolor[indexi][0]=indexi;
      scalecolor[indexi][1]=scale[indexi]
    }
   
    console.log("Dimensiones",dimensions[0]);                                                     
    var trace = {
      type: 'parcoords',                                                                                          
      line: {
        color:unpack(dimensions[0], 'tickvals'),
        colorscale: scalecolor
       
      },
      dimensions: [{
        label: 'Narrativas',
        values: values,
        tickvals: tickvals,
        ticktext: ticktext,
      }, {
        label: 'SAI',
        values: valuesSaiAxis,

      }, {
        label: 'Fragmentación',
        values: valuesFragAxis,
      }]
    };
    var data = [trace]

    Plotly.newPlot('chart', data,layout);
  }

  /*
  * Read a CSV file & extract the content in rows
  *
  * @param String csv    CSV File
  *
  * @return Array lines  Rows of csv readed
  */
  function processCSv(csv) {
    var allTextLines = csv.split(/\r\n|\n/);
    var lines = [];
    for (var i = 0; i < allTextLines.length - 1; i++) {
      var data = allTextLines[i].split(';');
      var tarr = [];
      for (var j = 0; j < data.length; j++) {
        tarr.push(data[j]);
      }
      lines.push(tarr);
    }
    return lines;
  }

  /* Get row in DOR Csv file
  *  filtering by ArcID
  *
  * @param    String   riverMouthArc    ArcID
  * @param    Array    dorFile          DOR file
  */
  function filterByDorArc(riverMouthArc, dorFile) {
    var dorRow = [];
    var dorRowFilter = [];
    /*
    * Loop every line &
    * search river mouth in DOR output
    */
    dorFile.forEach(function (row) {
      // Split columns in the string
      rowSplit = row[0].split(",");
      dorArc = rowSplit[0];
      if (dorArc == riverMouthArc) {
        dorRowFilter = rowSplit;

        // Convert object to string
        var row = JSON.stringify(dorRowFilter);

        // Replace double quotes
        var row = row.replace(/['"]+/g, '');
        // Replace brackets
        row = row.replace(/[[\]]/g, '');

        // Split each column by comma
        dorRowFilter = row.split(",");

        // Loop and convert every value in float
        dorRowFilter.forEach(function (value, index) {
          dorRowFilter[index] = parseFloat(value);
        });
      }
    });
    /* First position in array is ArcID, is no necesary
    *  so recreate array whitout the first position
    */
    for (i = 1; i <= dorRowFilter.length - 1; i++) {
      dorRow.push(dorRowFilter[i]);
    }
    return dorRow;
  }

});