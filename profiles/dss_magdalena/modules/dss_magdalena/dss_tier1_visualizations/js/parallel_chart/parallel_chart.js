Plotly.d3.csv('https://raw.githubusercontent.com/bcdunbar/datasets/master/parcoords_data.csv', function(err, rows){
      
function unpack(rows, key) {
  return rows.map(function(row) { 
    return row[key]; 
  });
}

var data = [{
  type: 'parcoords',
  line: {
    showscale: true,
    reversescale: true,
    colorscale: 'Jet',
    cmin: -4000,
    cmax: -100,
    color: unpack(rows, 'colorVal')
  },
	
  dimensions: [{
    constraintrange: [100000, 150000],
    range: [32000, 227900],
    label: 'DOR',
    values: unpack(rows, 'blockHeight')
  }, {
    range: [0, 700000],
    label: 'DORw',
    values: unpack(rows, 'blockWidth')
  }, {
    label: 'SAI',
    tickvals: [0, 0.5, 1, 2, 3],
    ticktext: ['A', 'AB', 'B', 'Y', 'Z'],
    values: unpack(rows, 'cycMaterial')
  }, {
    label: 'Fragmentación',
    tickvals: [0, 1, 2, 3],
    range: [-1, 4],
    values: unpack(rows, 'blockMaterial')
  }]
}];

Plotly.plot('chart', data);

});