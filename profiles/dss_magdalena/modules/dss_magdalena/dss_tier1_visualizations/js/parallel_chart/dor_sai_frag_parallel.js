jQuery(document).ready(function ($) {
  console.log(footprintNid);
  var narratives = JSON.parse(narrativeList);
  $(".mi-selector").select2();
  var narrativeDropdown = $("#narrativeMode");
  var elements = document.getElementById("narrativeMode").options;
  console.log("Elementos cargados:",elements)
  for(var i = 0; i < elements.length; i++){
    elements[i].selected = true;
    
  }
  narrativeDropdown.change(changeMode);
  var modelSelect = $('#modelSelect');
  modelSelect.change(changeMode);
  narrativeDropdown.change(changeMode);
  narratives.forEach(function (narrative) {
    narrativeDropdown.append('<option selected value=' + narrative.fullname + '> Narrativa ' + narrative.name + '</option>');
  });
  var dorOutputs = JSON.parse(outputs);
  var baseLineResources = dorOutputs.filter(file => file.fullname.includes("Base_Line"));

  if (baseLineResources.length > 0)
    baseLine = true
  else
    baseLine = false;


  var topologicalNetwork = JSON.parse(topoNetwork);
  var baseUrl = window.location.protocol + "//" + window.location.hostname+":"+window.location.port;
  var dkanUrl = "/api/3/action/package_show?id=";
  var fullUrlDataset = baseUrl + dkanUrl + datasetUuid;
  var fullUrlNetwork = baseUrl + dkanUrl + topologicalNetwork[0].datasetUuid;
  var scale=['#DA77BF','#0477BF','#1ABE39','#F0BE39','#F092EC','#E4EF26','#DDC406','#8872C9','#DC1E90','#DCF890'];
  var layout = {
    font: {
      size: 16,
    }
  }; 
  var networkResourceUrl;
  var noBaseLineDorAxis = [];
  var noBaseLineSaiAxis = []
  var noBaseLineDorwAxis = []
  var noBaseLineFragwAxis = []
  var datasetRequest = $.ajax({
    url: fullUrlDataset,
    type: 'GET',
  });

  var networkRequest = $.ajax({
    url: fullUrlNetwork,
    type: 'GET',
  });

  /*
  * Get response of output's datase request
  *
  * @return Object response response of request
  */
    var noBaseLineResources = [];
    var baseLineResources = [];
    var noBaseLineOutputs = [];
  datasetRequest.then(function (response) {
    
    /*
    * Filter no base line outputs by
    * word "narrativa" & the number
    */
   console.log("Narrativas",narratives);
   console.log("dorOuptuts",dorOutputs);
    for (i = 0; i <= narratives.length - 1; i++) {
      console.log(i);
      noBaseLineOutputs.push(dorOutputs.filter(out => out.fullname == "DOR_" + narratives[i].nid + "_Narrative_" + narratives[i].fullname));
      noBaseLineOutputs.push(dorOutputs.filter(out => out.fullname == "Install_Power_" + narratives[i].nid + "_Narrative_" + narratives[i].fullname));
      noBaseLineOutputs.push(dorOutputs.filter(out => out.fullname == "DORw_" + narratives[i].nid + "_Narrative_" + narratives[i].fullname));
      noBaseLineOutputs.push(dorOutputs.filter(out => out.fullname == "Aleatory_Definition_" + narratives[i].nid + "_Narrative_" + narratives[i].fullname));
      noBaseLineOutputs.push(dorOutputs.filter(out => out.fullname == "SAI_" + narratives[i].nid + "_Narrative_" + narratives[i].fullname));
      noBaseLineOutputs.push(dorOutputs.filter(out => out.fullname == "Arc_0_" + narratives[i].nid + "_Narrative_" + narratives[i].fullname));
    }
    /*
    * Filter in dataset response, the no base
    * line resources & get the file URL
    */
    console.log("No base line outputs",noBaseLineOutputs)
    noBaseLineOutputs.forEach(function (output) {
      // Resources result of dataset request
      var resources = response.result[0].resources;
      console.log("Resources",resources);
      console.log("output",output);
      // UUID of dataset's resource
      var resourceUuid = output[0].resourceUuid;
      var filterResource = resources.filter(r => r.id == resourceUuid);
      var resourceUrl = filterResource[0].url;
      var resource = { "name": output[0].fullname, "url": resourceUrl };
      noBaseLineResources.push(resource);
    });

    /*
   * Get response of topological network
   * dataset request & get topological
   * network resource URL
   *
   * @return Object response response of request
   */
    networkRequest.then(function (response) {
      // Resources attah to network dataset
      var resources = response.result[0].resources;

      // Filter reource by UUID
      var filterResource = resources.filter(r => r.id == topologicalNetwork[0].resourceUuid);

      // Resource url
      networkResourceUrl = filterResource[0].url;
    });

    var networkResourceRequest = $.ajax({
      url: networkResourceUrl,
      type: 'GET',
    });

    var dorNoBaseLine = [];
    // Filter no base DOR
    for (i = 0; i <= narratives.length - 1; i++) {
      dorNoBaseLine.push(noBaseLineResources.filter(out => out.name == "DOR_" + narratives[i].nid + "_Narrative_" + narratives[i].fullname));
    }
    var dorwNoBaseLine = [];
    for (i = 0; i <= narratives.length - 1; i++) {
      dorwNoBaseLine.push(noBaseLineResources.filter(out => out.name == "DORw_" + narratives[i].nid + "_Narrative_" + narratives[i].fullname));
    }

    var saiNoBaseline = [];
    for (i = 0; i <= narratives.length - 1; i++) {
      saiNoBaseline.push(noBaseLineResources.filter(out => out.name == "SAI_" + narratives[i].nid + "_Narrative_" + narratives[i].fullname));
    }
    var funcNoBaseLine = [];
    // Filter no base sai
    for (i = 0; i <= narratives.length - 1; i++) {
      funcNoBaseLine.push(noBaseLineResources.filter(out => out.name == "Arc_0_" + narratives[i].nid + "_Narrative_" + narratives[i].fullname));
    }
    //no base dor request
    var dorNoBaseLineRequest = [];
    dorNoBaseLine.forEach(function (resource) {
      var request = $.ajax({
        url: resource[0].url,
        type: 'GET',
      });
      var properties = { "name": resource[0].name, "request": request };
      dorNoBaseLineRequest.push(properties);
    });

    var dorwNoBaseLineRequest = [];
    dorwNoBaseLine.forEach(function (resource) {
      var request = $.ajax({
        url: resource[0].url,
        type: 'GET',
      });
      var properties = { "name": resource[0].name, "request": request };
      dorwNoBaseLineRequest.push(properties);
    });

    var saiNoBaseLineRequest = [];
    saiNoBaseline.forEach(function (resource) {
      var request = $.ajax({
        url: resource[0].url,
        type: 'GET',
      });
      var properties = { "name": resource[0].name, "request": request };
      saiNoBaseLineRequest.push(properties);
    });

    var funcNoBaseLineRequest = [];
    funcNoBaseLine.forEach(function (resource) {
      var request = $.ajax({
        url: resource[0].url,
        type: 'GET',
      });
      var properties = { "name": resource[0].name, "request": request };
      funcNoBaseLineRequest.push(properties);
    });
    console.log("funcNoBaseLineRequest",funcNoBaseLineRequest);

    /* Global variables section */
    var networkLines;
    var riverMouthArc;
    var narrativeNid;
    var narrativeName;

    /*
   * Get response of topological network
   * resource & filter river mouth arc
   *
   * @return Object response response of request
   */
    networkResourceRequest.then(function (response) {
      // Extract lines of csv in array
      networkLines = processCSv(response);
      /*
      * Loop every line & search river mouth arc
      */
      networkLines.forEach(function (row) {
        rowSplit = row[0].split(",");
        if (rowSplit[3] == '1')
          riverMouthArc = rowSplit[0];
      });

      var promisesDor = [];
      var dorNoBaseline = [];
      dorNoBaseLineRequest.forEach(function (resource) {
        var nid = resource.name.split("_");
        narrativeNid = nid[1];
        var name = resource.name.split("_Narrative_");
        narrativeName = name[1];
        promisesDor.push(resource.request);
        var noBaseResponse = Object();
        noBaseResponse.nid = narrativeNid;
        noBaseResponse.name = narrativeName;
        dorNoBaseline.push(noBaseResponse);
      });
      
      var promisesDorw = [];
      var dorwNoBaseline = [];
      dorwNoBaseLineRequest.forEach(function (resource) {
        var nid = resource.name.split("_");
        narrativeNid = nid[1];
        var name = resource.name.split("_Narrative_");
        narrativeName = name[1];
        promisesDorw.push(resource.request);
        var noBaseResponse = Object();
        noBaseResponse.nid = narrativeNid;
        noBaseResponse.name = narrativeName;
        dorwNoBaseline.push(noBaseResponse);
      });

      var promisesSai = [];
      var saiNoLine = [];
      saiNoBaseLineRequest.forEach(function (resource) {
        var nid = resource.name.split("_");
        narrativeNid = nid[1];
        var name = resource.name.split("_Narrative_");
        narrativeName = name[1];
        promisesSai.push(resource.request);
        var noBaseResponse = Object();
        noBaseResponse.nid = narrativeNid;
        noBaseResponse.name = narrativeName;
        saiNoLine.push(noBaseResponse);
      });
      var funcNoBaseLineResponse = [];
      funcNoBaseLineRequest.forEach(function (resource) {
        var nid = resource.name.split("_");
        narrativeNid = nid[1];
        var name = resource.name.split("_Narrative_");
        narrativeName = name[1];
        resource.request.then(function (response) {
          var noBaseResponse = Object();
          noBaseResponse.nid = narrativeNid;
          noBaseResponse.name = narrativeName;
          noBaseResponse.response = processCSv(response);
          funcNoBaseLineResponse.push(noBaseResponse);
        })
      });
      console.log("funcNoBaseLineResponse",funcNoBaseLineResponse)


      // When all promises all resolved
      

      Promise.all(promisesDor).then(promiseResponse => {
        //console.log(promiseResponse);
        dorNoBaseline.forEach(function (response, index) {
          response.response = processCSv(promiseResponse[index]);
          var dorAxis = Object();
          dorAxis.axis = filterByDorArc(riverMouthArc, response.response);
          dorAxis.name = response.name;
          dorAxis.model = "DOR";
          dorAxis.nid = response.nid;
          noBaseLineDorAxis.push(dorAxis);
          
        });
        console.log('noBaseLineDorAxis',noBaseLineDorAxis);
        
        Promise.all(promisesDorw).then(promiseResponse => {
          //console.log(promiseResponse);
          dorwNoBaseline.forEach(function (response, index) {
            response.response = processCSv(promiseResponse[index]);
            var dorwAxis = Object();
            dorwAxis.axis = filterByDorArc(riverMouthArc, response.response);
            dorwAxis.name = response.name;
            dorwAxis.model = "DORw";
            dorwAxis.nid = response.nid;
            noBaseLineDorwAxis.push(dorwAxis);
            
          });
          console.log('noBaseLineDorwAxis',noBaseLineDorwAxis);
          
          Promise.all(promisesSai).then(promiseResponse => {
            //console.log(promiseResponse);
            saiNoLine.forEach(function (response, index) {
              response.response = processCSv(promiseResponse[index]);
              var saiAxis = Object();
              saiAxis.axis = filterByDorArc(riverMouthArc, response.response);
              saiAxis.name = response.name;
              saiAxis.model = "SAI";
              saiAxis.nid = response.nid;
              noBaseLineSaiAxis.push(saiAxis);
            });
            console.log('noBaseLineSaiAxis',noBaseLineSaiAxis);
            Promise.all(funcNoBaseLineResponse).then(promiseResponse => {
              console.log(promiseResponse);
              funcNoBaseLineResponse.forEach(function (response, index) {
                
                var FragmentacionAxis = Object();
                FragmentacionAxis.axis = filterByDorArc("0", response.response);
                FragmentacionAxis.name = response.name;
                FragmentacionAxis.model = "Fragmentación";
                FragmentacionAxis.nid = response.nid;
                noBaseLineFragwAxis.push(FragmentacionAxis);
              });
              console.log(noBaseLineFragwAxis);
              parallelChart(noBaseLineDorAxis, noBaseLineDorwAxis, noBaseLineSaiAxis,noBaseLineFragwAxis,3);
            });
          });
        });
      });


    });
  });

  function parallelChart(dorAxis, dorwAxis, saiAxis,fragAxis,number) {
    //console.log(number);
    //console.log("DOR",dorAxis);
    //console.log("Dorw",dorwAxis);
    //console.log("SAI",saiAxis);

    if(number ==4){
      var values = [];
      var ticktext = [];
      var tickvals = [];
      var valuesDorAxis = [];
      var valuesDorwAxis = [];
      var valuesNum = dorAxis[0].axis.length;
      for (i = 1; i <=dorAxis.length; i++) {
        for (j = 1; j <= valuesNum; j++) {
          values.push(i);
        }
      }

      dorAxis.forEach(function (dor, indexDor) {
        //console.log(dor);
        dor.axis.forEach(function (axis, indexAxis) {
          //console.log(axis);
          valuesDorAxis.push(axis);
          valuesDorwAxis.push(dorwAxis[indexDor].axis[indexAxis]);
        });
        ticktext.push(dor.name);
        tickvals.push(indexDor + 1);
        
      });
      var dimensionsObject = {};
      var dimensions = [];
      dimensionsObject.label = "Narrativas";
      dimensionsObject.ticktext = ticktext;
      dimensionsObject.tickvals = tickvals;
      dimensionsObject.value = values;
      dimensions.push(dimensionsObject);
      var  scalecolor=[];
      scalecolor = new Array(dimensions[0].ticktext.length);
      for(let indexi=0;indexi<scalecolor.length;indexi++){
        scalecolor[indexi] = [];
        scalecolor[indexi][0]=indexi;
        scalecolor[indexi][1]=scale[indexi]
      }
     
      console.log("Dimensiones",dimensions[0]);  
      var trace = {
        type: 'parcoords',
        line: {
          color:unpack(dimensions[0], 'tickvals'),
          colorscale: scalecolor
          
        },
        dimensions: [{
          label: 'Narrativas',
          values: values,
          tickvals: tickvals,
          ticktext: ticktext,
        }, {
          label: 'DOR',
          values: valuesDorAxis,
  
        }, {
          label: 'DORw',
          values: valuesDorwAxis,
        }]
      };
      var data = [trace]
      Plotly.newPlot('chart', data,layout);
      
    }
    else if(number ==5){

      console.log(dorAxis);
      var values = [];
      var ticktext = [];
      var tickvals = [];
      var valuesDorAxis = [];
      var valuesSaiAxis = [];
      var valuesNum = dorAxis[0].axis.length;
      for (i = 1; i <=dorAxis.length; i++) {
        for (j = 1; j <= valuesNum; j++) {
          values.push(i);
        }
      }
      dorAxis.forEach(function (dor, indexDor) {
        //console.log(dor);
        dor.axis.forEach(function (axis, indexAxis) {
          //console.log(axis);
          valuesDorAxis.push(axis);
          valuesSaiAxis.push(saiAxis[indexDor].axis[indexAxis]);
        });
        ticktext.push(dor.name);
        tickvals.push(indexDor + 1);
      });
     
      var dimensionsObject = {};
      var dimensions = [];
      dimensionsObject.label = "Narrativas";
      dimensionsObject.ticktext = ticktext;
      dimensionsObject.tickvals = tickvals;
      dimensionsObject.value = values;
      dimensions.push(dimensionsObject);
      var  scalecolor=[];
    scalecolor = new Array(dimensions[0].ticktext.length);
    for(let indexi=0;indexi<scalecolor.length;indexi++){
      scalecolor[indexi] = [];
      scalecolor[indexi][0]=indexi;
      scalecolor[indexi][1]=scale[indexi]
    }
   
    console.log("Dimensiones",dimensions[0]);  
      var trace = {
        type: 'parcoords',
        line: {
          color:unpack(dimensions[0], 'tickvals'),
          colorscale: scalecolor
        },
        dimensions: [{
          label: 'Narrativas',
          values: values,
          tickvals: tickvals,
          ticktext: ticktext,
        }, {
          label: 'DOR',
          values: valuesDorAxis,
  
        },
        {
          label: 'SAI',
          values: valuesSaiAxis,
        }]
      };
      var data = [trace]
  
      Plotly.newPlot('chart', data,layout);

    }
    else if( number ==6){

      //console.log(dorwAxis);
      var values = [];
      var ticktext = [];
      var tickvals = [];
      var valuesDorwAxis = [];
      var valuesSaiAxis = [];
      var valuesNum = dorwAxis[0].axis.length;
      for (i = 1; i <=dorwAxis.length; i++) {
        for (j = 1; j <= valuesNum; j++) {
          values.push(i);
        }
      }
      dorwAxis.forEach(function (dorw, indexDorw) {
        //console.log(dorw);
        dorw.axis.forEach(function (axis, indexAxis) {
          //console.log(axis);
          valuesDorwAxis.push(axis);
          valuesSaiAxis.push(saiAxis[indexDorw].axis[indexAxis]);
        });
        ticktext.push(dorw.name);
        tickvals.push(indexDorw + 1);
      });
      var dimensionsObject = {};
      var dimensions = [];
      dimensionsObject.label = "Narrativas";
      dimensionsObject.ticktext = ticktext;
      dimensionsObject.tickvals = tickvals;
      dimensionsObject.value = values;
      dimensions.push(dimensionsObject);
      var  scalecolor=[];
    scalecolor = new Array(dimensions[0].ticktext.length);
    for(let indexi=0;indexi<scalecolor.length;indexi++){
      scalecolor[indexi] = [];
      scalecolor[indexi][0]=indexi;
      scalecolor[indexi][1]=scale[indexi]
    }
   
    console.log("Dimensiones",dimensions[0]);
      var trace = {
        type: 'parcoords',
        line: {
          color:unpack(dimensions[0], 'tickvals'),
        colorscale: scalecolor

        },
        dimensions: [{
          label: 'Narrativas',
          values: values,
          tickvals: tickvals,
          ticktext: ticktext,
        }, 
        {
          label: 'DORw',
          values: valuesDorwAxis,
        },
        {
          label: 'SAI',
          values: valuesSaiAxis,
        }]
      };
      var data = [trace]
  
      Plotly.newPlot('chart', data,layout);



    }
    else if( number ==7){

      //console.log(dorwAxis);
      var values = [];
      var ticktext = [];
      var tickvals = [];
      var valuesDorwAxis = [];
      var valuesSaiAxis = [];
      var valuesFragAxis = [];
      var valuesNum = dorwAxis[0].axis.length;
      for (i = 1; i <=dorwAxis.length; i++) {
        for (j = 1; j <= valuesNum; j++) {
          values.push(i);
        }
      }
      dorwAxis.forEach(function (dorw, indexDorw) {
        //console.log(dorw);
        dorw.axis.forEach(function (axis, indexAxis) {
          //console.log(axis);
          valuesDorwAxis.push(axis);
          valuesFragAxis.push(fragAxis[indexDorw].axis[indexAxis]);
        });
        ticktext.push(dorw.name);
        tickvals.push(indexDorw + 1);
      });
      var dimensionsObject = {};
      var dimensions = [];
      dimensionsObject.label = "Narrativas";
      dimensionsObject.ticktext = ticktext;
      dimensionsObject.tickvals = tickvals;
      dimensionsObject.value = values;
      dimensions.push(dimensionsObject);
      var  scalecolor=[];
      scalecolor = new Array(dimensions[0].ticktext.length);
      for(let indexi=0;indexi<scalecolor.length;indexi++){
        scalecolor[indexi] = [];
        scalecolor[indexi][0]=indexi;
        scalecolor[indexi][1]=scale[indexi]
      }
     
      console.log("Dimensiones",dimensions[0]);
      var trace = {
        type: 'parcoords',
        line: {
          color:unpack(dimensions[0], 'tickvals'),
          colorscale: scalecolor

        },
        dimensions: [{
          label: 'Narrativas',
          values: values,
          tickvals: tickvals,
          ticktext: ticktext,
        }, 
        {
          label: 'DORw',
          values: valuesDorwAxis,
        },
        {
          label: 'Fragmentación',
          values: valuesFragAxis,
        }]
      };
      var data = [trace]
  
      Plotly.newPlot('chart', data,layout);



    }
    else if( number ==8){

     //console.log(dorwAxis);
     var values = [];
     var ticktext = [];
     var tickvals = [];
     var valuesDorAxis = [];
     var valuesSaiAxis = [];
     var valuesFragAxis = [];
     var valuesNum = dorAxis[0].axis.length;
     for (i = 1; i <=dorAxis.length; i++) {
       for (j = 1; j <= valuesNum; j++) {
         values.push(i);
       }
     }
     dorAxis.forEach(function (dor, indexDor) {
       //console.log(dorw);
       dor.axis.forEach(function (axis, indexAxis) {
         //console.log(axis);
         valuesDorAxis.push(axis);
         valuesFragAxis.push(fragAxis[indexDor].axis[indexAxis]);
       });
       ticktext.push(dor.name);
       tickvals.push(indexDor + 1);
     });
     var dimensionsObject = {};
     var dimensions = [];
     dimensionsObject.label = "Narrativas";
     dimensionsObject.ticktext = ticktext;
     dimensionsObject.tickvals = tickvals;
     dimensionsObject.value = values;
     dimensions.push(dimensionsObject);
     var  scalecolor=[];
     scalecolor = new Array(dimensions[0].ticktext.length);
     for(let indexi=0;indexi<scalecolor.length;indexi++){
       scalecolor[indexi] = [];
       scalecolor[indexi][0]=indexi;
       scalecolor[indexi][1]=scale[indexi]
     }
    
     console.log("Dimensiones",dimensions[0]); 
     var trace = {
       type: 'parcoords',
       line: {
        color:unpack(dimensions[0], 'tickvals'),
        colorscale: scalecolor

       },
       dimensions: [{
         label: 'Narrativas',
         values: values,
         tickvals: tickvals,
         ticktext: ticktext,
       }, 
       {
         label: 'DOR',
         values: valuesDorAxis,
       },
       {
         label: 'Fragmentación',
         values: valuesFragAxis,
       }]
     };
     var data = [trace]
 
     Plotly.newPlot('chart', data,layout);



    }
    else if( number ==9){

     //console.log(dorwAxis);
     var values = [];
     var ticktext = [];
     var tickvals = [];
     var valuesDorAxis = [];
     var valuesSaiAxis = [];
     var valuesFragAxis = [];
     var valuesNum = saiAxis[0].axis.length;
     for (i = 1; i <=saiAxis.length; i++) {
       for (j = 1; j <= valuesNum; j++) {
         values.push(i);
       }
     }
     saiAxis.forEach(function (sai, indexSai) {
       //console.log(dorw);
       sai.axis.forEach(function (axis, indexAxis) {
         //console.log(axis);
         valuesSaiAxis.push(saiAxis[indexSai].axis[indexAxis]);
         valuesFragAxis.push(fragAxis[indexSai].axis[indexAxis]);
       });
       ticktext.push(sai.name);
       tickvals.push(indexSai + 1);
     });
     var dimensionsObject = {};
     var dimensions = [];
     dimensionsObject.label = "Narrativas";
     dimensionsObject.ticktext = ticktext;
     dimensionsObject.tickvals = tickvals;
     dimensionsObject.value = values;
     dimensions.push(dimensionsObject);
     var  scalecolor=[];
    scalecolor = new Array(dimensions[0].ticktext.length);
    for(let indexi=0;indexi<scalecolor.length;indexi++){
      scalecolor[indexi] = [];
      scalecolor[indexi][0]=indexi;
      scalecolor[indexi][1]=scale[indexi]
    }
   
    console.log("Dimensiones",dimensions[0]);  
     var trace = {
       type: 'parcoords',
       line: {
        color:unpack(dimensions[0], 'tickvals'),
        colorscale: scalecolor

       },
       dimensions: [{
         label: 'Narrativas',
         values: values,
         tickvals: tickvals,
         ticktext: ticktext,
       }, 
       {
         label: 'Sai',
         values: valuesSaiAxis,
       },
       {
         label: 'Fragmentación',
         values: valuesFragAxis,
       }]
     };
     var data = [trace]
 
     Plotly.newPlot('chart', data,layout);



    }
    else if( number ==10){

      //console.log(dorwAxis);
      var values = [];
      var ticktext = [];
      var tickvals = [];
      var valuesDorAxis = [];
      var valuesDorwAxis = [];
      var valuesSaiAxis = [];
      var valuesFragAxis = [];
      var valuesNum = dorwAxis[0].axis.length;
      for (i = 1; i <=dorwAxis.length; i++) {
        for (j = 1; j <= valuesNum; j++) {
          values.push(i);
        }
      }
      dorwAxis.forEach(function (dorw, indexDorw) {
        //console.log(dorw);
        dorw.axis.forEach(function (axis, indexAxis) {
          //console.log(axis);
          valuesDorAxis.push(dorAxis[indexDorw].axis[indexAxis]);
          valuesDorwAxis.push(axis);
          valuesFragAxis.push(fragAxis[indexDorw].axis[indexAxis]);
        });
        ticktext.push(dorw.name);
        tickvals.push(indexDorw + 1);
      });
      var dimensionsObject = {};
      var dimensions = [];
      dimensionsObject.label = "Narrativas";
      dimensionsObject.ticktext = ticktext;
      dimensionsObject.tickvals = tickvals;
      dimensionsObject.value = values;
      dimensions.push(dimensionsObject);
      var  scalecolor=[];
    scalecolor = new Array(dimensions[0].ticktext.length);
    for(let indexi=0;indexi<scalecolor.length;indexi++){
      scalecolor[indexi] = [];
      scalecolor[indexi][0]=indexi;
      scalecolor[indexi][1]=scale[indexi]
    }
   
    console.log("Dimensiones",dimensions[0]);
    
      var trace = {
        type: 'parcoords',
        line: {
          color:unpack(dimensions[0], 'tickvals'),
        colorscale: scalecolor
 
        },
        dimensions: [{
          label: 'Narrativas',
          values: values,
          tickvals: tickvals,
          ticktext: ticktext,
        }, 
        {
          label: 'Dor',
          values: valuesDorAxis,
        },
        {
          label: 'Dorw',
          values: valuesDorwAxis,
        },
        {
          label: 'Fragmentación',
          values: valuesFragAxis,
        }]
      };
      var data = [trace]
  
      Plotly.newPlot('chart', data,layout);
 
 
 
     }
     else if( number ==11){

      //console.log(dorwAxis);
      var values = [];
      var ticktext = [];
      var tickvals = [];
      var valuesDorAxis = [];
      var valuesDorwAxis = [];
      var valuesSaiAxis = [];
      var valuesFragAxis = [];
      var valuesNum = dorwAxis[0].axis.length;
      for (i = 1; i <=dorwAxis.length; i++) {
        for (j = 1; j <= valuesNum; j++) {
          values.push(i);
        }
      }
      dorwAxis.forEach(function (dorw, indexDorw) {
        //console.log(dorw);
        dorw.axis.forEach(function (axis, indexAxis) {
          //console.log(axis);
          valuesDorAxis.push(dorAxis[indexDorw].axis[indexAxis]);
          valuesDorwAxis.push(axis);
          valuesSaiAxis.push(saiAxis[indexDorw].axis[indexAxis]);
        });
        ticktext.push(dorw.name);
        tickvals.push(indexDorw + 1);
      });
      var dimensionsObject = {};
      var dimensions = [];
      dimensionsObject.label = "Narrativas";
      dimensionsObject.ticktext = ticktext;
      dimensionsObject.tickvals = tickvals;
      dimensionsObject.value = values;
      dimensions.push(dimensionsObject);
      var  scalecolor=[];
      scalecolor = new Array(dimensions[0].ticktext.length);
      for(let indexi=0;indexi<scalecolor.length;indexi++){
        scalecolor[indexi] = [];
        scalecolor[indexi][0]=indexi;
        scalecolor[indexi][1]=scale[indexi]
      }
     
      console.log("Dimensiones",dimensions[0]); 
      
      var trace = {
        type: 'parcoords',
        line: {
          color:unpack(dimensions[0], 'tickvals'),
        colorscale: scalecolor
 
        },
        dimensions: [{
          label: 'Narrativas',
          values: values,
          tickvals: tickvals,
          ticktext: ticktext,
        }, 
        {
          label: 'Dor',
          values: valuesDorAxis,
        },
        {
          label: 'Dorw',
          values: valuesDorwAxis,
        },
        {
          label: 'SAI',
          values: valuesSaiAxis,
        }]
      };
      var data = [trace]
  
      Plotly.newPlot('chart', data,layout);
 
 
 
     }
     else if( number ==12){

      //console.log(dorwAxis);
      var values = [];
      var ticktext = [];
      var tickvals = [];
      var valuesDorAxis = [];
      var valuesDorwAxis = [];
      var valuesSaiAxis = [];
      var valuesFragAxis = [];
      var valuesNum = dorwAxis[0].axis.length;
      for (i = 1; i <=dorwAxis.length; i++) {
        for (j = 1; j <= valuesNum; j++) {
          values.push(i);
        }
      }
      dorwAxis.forEach(function (dorw, indexDorw) {
        //console.log(dorw);
        dorw.axis.forEach(function (axis, indexAxis) {
          //console.log(axis);
          valuesSaiAxis.push(saiAxis[indexDorw].axis[indexAxis]);
          valuesDorwAxis.push(axis);
          valuesFragAxis.push(fragAxis[indexDorw].axis[indexAxis]);
        });
        ticktext.push(dorw.name);
        tickvals.push(indexDorw + 1);
      });
      var dimensionsObject = {};
      var dimensions = [];
      dimensionsObject.label = "Narrativas";
      dimensionsObject.ticktext = ticktext;
      dimensionsObject.tickvals = tickvals;
      dimensionsObject.value = values;
      dimensions.push(dimensionsObject);
      var  scalecolor=[];
    scalecolor = new Array(dimensions[0].ticktext.length);
    for(let indexi=0;indexi<scalecolor.length;indexi++){
      scalecolor[indexi] = [];
      scalecolor[indexi][0]=indexi;
      scalecolor[indexi][1]=scale[indexi]
    }
   
    console.log("Dimensiones",dimensions[0]);
    
      var trace = {
        type: 'parcoords',
        line: {
          color:unpack(dimensions[0], 'tickvals'),
        colorscale: scalecolor
 
        },
        dimensions: [{
          label: 'Narrativas',
          values: values,
          tickvals: tickvals,
          ticktext: ticktext,
        }, 
        {
          label: 'Dorw',
          values: valuesDorwAxis,
        },
        {
          label: 'SAI',
          values: valuesSaiAxis,
        },
        {
          label: 'Fragmentación',
          values: valuesFragAxis,
        }]
      };
      var data = [trace]
  
      Plotly.newPlot('chart', data,layout);
 
 
 
     }
     else if( number ==13){

      //console.log(dorwAxis);
      var values = [];
      var ticktext = [];
      var tickvals = [];
      var valuesDorAxis = [];
      var valuesDorwAxis = [];
      var valuesSaiAxis = [];
      var valuesFragAxis = [];
      var valuesNum = dorAxis[0].axis.length;
      for (i = 1; i <=dorAxis.length; i++) {
        for (j = 1; j <= valuesNum; j++) {
          values.push(i);
        }
      }
      dorAxis.forEach(function (dor, indexDor) {
        //console.log(dorw);
        dor.axis.forEach(function (axis, indexAxis) {
          //console.log(axis);
          valuesSaiAxis.push(saiAxis[indexDor].axis[indexAxis]);
          valuesDorAxis.push(axis);
          valuesFragAxis.push(fragAxis[indexDor].axis[indexAxis]);
        });
        ticktext.push(dor.name);
        tickvals.push(indexDor + 1);
      });
      var dimensionsObject = {};
      var dimensions = [];
      dimensionsObject.label = "Narrativas";
      dimensionsObject.ticktext = ticktext;
      dimensionsObject.tickvals = tickvals;
      dimensionsObject.value = values;
      dimensions.push(dimensionsObject);
      var  scalecolor=[];
    scalecolor = new Array(dimensions[0].ticktext.length);
    for(let indexi=0;indexi<scalecolor.length;indexi++){
      scalecolor[indexi] = [];
      scalecolor[indexi][0]=indexi;
      scalecolor[indexi][1]=scale[indexi]
    }
   
    console.log("Dimensiones",dimensions[0]); 
    
      var trace = {
        type: 'parcoords',
        line: {
          color:unpack(dimensions[0], 'tickvals'),
        colorscale: scalecolor
 
        },
        dimensions: [{
          label: 'Narrativas',
          values: values,
          tickvals: tickvals,
          ticktext: ticktext,
        }, 
        {
          label: 'Dor',
          values: valuesDorAxis,
        },
        {
          label: 'SAI',
          values: valuesSaiAxis,
        },
        {
          label: 'Fragmentación',
          values: valuesFragAxis,
        }]
      };
      var data = [trace]
  
      Plotly.newPlot('chart', data,layout);
 
 
 
     }
     
    else if(dorAxis!=[] && dorwAxis!=[] && saiAxis!=[] && fragAxis!=[] && number == 3){
      //console.log("DorAxis",dorAxis);
      //console.log("DorwAxis",dorwAxis);
      //console.log("saiAxis",saiAxis);
      var values = [];
      var ticktext = [];
      var tickvals = [];
      var valuesDorAxis = [];
      var valuesDorwAxis = [];
      var valuesSaiAxis = [];
      var valuesFragAxis = [];
      var valuesNum = dorAxis[0].axis.length;
      for (i = 1; i <=dorAxis.length; i++) {
        for (j = 1; j <= valuesNum; j++) {
          values.push(i);
        }
      }
      dorAxis.forEach(function (dor, indexDor) {
        //console.log(dor);
        dor.axis.forEach(function (axis, indexAxis) {
          //console.log(axis);
          valuesDorAxis.push(axis);
          valuesDorwAxis.push(dorwAxis[indexDor].axis[indexAxis]);
          valuesSaiAxis.push(saiAxis[indexDor].axis[indexAxis]);
          valuesFragAxis.push(fragAxis[indexDor].axis[indexAxis]);
        });
        ticktext.push(dor.name);
        tickvals.push(indexDor + 1);
      });
      var dimensionsObject = {};
      var dimensions = [];
      dimensionsObject.label = "Narrativas";
      dimensionsObject.ticktext = ticktext;
      dimensionsObject.tickvals = tickvals;
      dimensionsObject.value = values;
      dimensions.push(dimensionsObject);
      var  scalecolor=[];
      scalecolor = new Array(dimensions[0].ticktext.length);
      for(let indexi=0;indexi<scalecolor.length;indexi++){
        scalecolor[indexi] = [];
        scalecolor[indexi][0]=indexi;
        scalecolor[indexi][1]=scale[indexi]
      }
     
      console.log("Dimensiones",dimensions[0]);  
      //console.log("Dimensiones",dimensions)
      console.log("valores",values);
      
      var trace = {
        type: 'parcoords',
        line: {
          color:unpack(dimensions[0], 'tickvals'),
          colorscale: scalecolor
        },
        dimensions: [{
          label: 'Narrativas',
          values: values,
          tickvals: tickvals,
          ticktext: ticktext,
        }, {
          label: 'DOR',
          values: valuesDorAxis,
  
        }, {
          label: 'DORw',
          values: valuesDorwAxis,
        },
        {
          label: 'SAI',
          values: valuesSaiAxis,
        },
        {
          label: 'Fragmentación',
          values: valuesFragAxis,
        }]
      };
      var data = [trace]
  
      Plotly.newPlot('chart', data,layout);
    }
    
  }
  function unpack(rows, key) {
    
    return rows.value  ;
  }
  /*
  * Read a CSV file & extract the content in rows
  *
  * @param String csv    CSV File
  *
  * @return Array lines  Rows of csv readed
  */
  function processCSv(csv) {
    var allTextLines = csv.split(/\r\n|\n/);
    var lines = [];
    for (var i = 0; i < allTextLines.length - 1; i++) {
      var data = allTextLines[i].split(';');
      var tarr = [];
      for (var j = 0; j < data.length; j++) {
        tarr.push(data[j]);
      }
      lines.push(tarr);
    }
    return lines;
  }

  /* Get row in DOR Csv file
  *  filtering by ArcID
  *
  * @param    String   riverMouthArc    ArcID
  * @param    Array    dorFile          DOR file
  */
  function filterByDorArc(riverMouthArc, dorFile) {
    var dorRow = [];
    var dorRowFilter = [];
    /*
    * Loop every line &
    * search river mouth in DOR output
    */
    dorFile.forEach(function (row) {
      // Split columns in the string
      rowSplit = row[0].split(",");
      dorArc = rowSplit[0];
      if (dorArc == riverMouthArc) {
        dorRowFilter = rowSplit;

        // Convert object to string
        var row = JSON.stringify(dorRowFilter);

        // Replace double quotes
        var row = row.replace(/['"]+/g, '');
        // Replace brackets
        row = row.replace(/[[\]]/g, '');

        // Split each column by comma
        dorRowFilter = row.split(",");

        // Loop and convert every value in float
        dorRowFilter.forEach(function (value, index) {
          dorRowFilter[index] = parseFloat(value);
        });
      }
    });
    /* First position in array is ArcID, is no necesary
    *  so recreate array whitout the first position
    */
    for (i = 1; i <= dorRowFilter.length - 1; i++) {
      dorRow.push(dorRowFilter[i]);
    }
    return dorRow;
  }

  function changeMode(){

    //console.log(noBaseLineDorAxis);
    //console.log(noBaseLineDorwAxis);
    //console.log(noBaseLineSaiAxis);
   
    var seleccionados = [];
    var narrativasselected=[];
    var finaldor =[];
    var finaldorw=[];
    var finalsai=[];
    var finalfrag=[];
    var selectednarra=narrativeDropdown[0].selectedOptions;
    var narrativa;
    //console.log("Selected DOR",modelSelect[0].options[0].selected);
    //console.log("Selected DORw",modelSelect[0].options[1].selected);
    //console.log("Selected SAI",modelSelect[0].options[2].selected);
   console.log(selectednarra);
   console.log("NoBaseLineDorAxis",noBaseLineDorAxis);
  console.log(selectednarra);
    for(var i=0; i<selectednarra.length;i++){
      narrativa=[];
      narrativasselected.push(selectednarra[i].value);
    }

    if(narrativasselected.length==0){
      alert("Se requiere de almenos 1 narrativa para mostrar el diagrama");
      
    }
    else{
      for(var i=0; i<narrativasselected.length;i++){
        console.log(narrativasselected,i);
        noBaseLineDorAxis.filter(res=> {
          if(res.name==narrativasselected[i]){
            finaldor.push(res);
            //console.log("Filtro dor",finaldor);
          }
          else{
          }});
      }
      for(var i=0; i<narrativasselected.length;i++){
        console.log(narrativasselected,i);
        noBaseLineDorwAxis.filter(res=> {
          if(res.name==narrativasselected[i]){
            finaldorw.push(res);
            //console.log("Filtro dorw",finaldorw);
          }
          else{
          }});
      }
      for(var i=0; i<narrativasselected.length;i++){
        console.log(narrativasselected,i);
        noBaseLineSaiAxis.filter(res=> {
          if(res.name==narrativasselected[i]){
            finalsai.push(res);
            //console.log("Filtro sai",finalsai);
          }
          else{
          }});
      }
      for(var i=0; i<narrativasselected.length;i++){
        console.log(narrativasselected,i);
        noBaseLineFragwAxis.filter(res=> {
          if(res.name==narrativasselected[i]){
            finalfrag.push(res);
            //console.log("Filtro sai",finalsai);
          }
          else{
          }});
      }
   
  
        for(var i=0;i<modelSelect[0].options.length;i++){
          if(modelSelect[0].options[i].selected==true){
            seleccionados.push(modelSelect[0].options[i].value);
            //console.log(seleccionados)
           
          }
        }
          if(seleccionados.length>=2){
            if(seleccionados.length==2){


              seleccionados.sort(sortNumber);
              if(seleccionados[0]==1 && seleccionados[1]==2){
                console.log("Dor y DorW");
                parallelChart(finaldor, finaldorw, finalsai,finalfrag,4);
              }
              else if(seleccionados[0]==2 && seleccionados[1]==3){
                console.log("Dor y SAI");
                parallelChart(finaldor, finaldorw, finalsai,finalfrag,5);
              }
              else if(seleccionados[0]==1 && seleccionados[1]==3){
                console.log("DorW y SAI");
                parallelChart(finaldor, finaldorw, finalsai,finalfrag,6);
              }
              else if(seleccionados[0]==1 && seleccionados[1]==4){
                console.log("DorW y Frag");
                parallelChart(finaldor, finaldorw, finalsai,finalfrag,7);
              }
              else if(seleccionados[0]==2 && seleccionados[1]==4){
                console.log("Dor y Frag");
                parallelChart(finaldor, finaldorw, finalsai,finalfrag,8);
              }
              else if(seleccionados[0]==3 && seleccionados[1]==4){
                console.log("Frag y SAI");
                parallelChart(finaldor, finaldorw, finalsai,finalfrag,9);
              }
            }else if(seleccionados.length==3){
      
              seleccionados.sort(sortNumber);
             if(seleccionados[0]==1 && seleccionados[1]==2 && seleccionados[2]==4){
                parallelChart(finaldor, finaldorw, finalsai,finalfrag,10);
              }
              else if(seleccionados[0]==1 && seleccionados[1]==2 && seleccionados[2]==3){
                //console.log("DorW y SAI");
                parallelChart(finaldor, finaldorw, finalsai,finalfrag,11);
              }
              else if(seleccionados[0]==1 && seleccionados[1]==3 && seleccionados[2]==4){
                //console.log("DorW y SAI");
                parallelChart(finaldor, finaldorw, finalsai,finalfrag,12);
              }
              else if(seleccionados[0]==2 && seleccionados[1]==3 && seleccionados[2]==4){
                //console.log("DorW y SAI");
                parallelChart(finaldor, finaldorw, finalsai,finalfrag,13);
              }

            }
            else if(seleccionados.length==4){
              //Mostrando todos los datos
              parallelChart(finaldor, finaldorw, finalsai,finalfrag,3);
            }
  
  
        }
      else{
        alert("Se requiere de almenos 2 modelos seleccionados para mostrar el diagrama")
      }
    }
    
    
  }

  function sortNumber(a, b) {
    return a - b;
  }

});