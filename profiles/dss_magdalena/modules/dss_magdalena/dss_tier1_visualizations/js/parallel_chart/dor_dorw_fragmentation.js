jQuery(document).ready(function ($) {
    //console.log(footprintNid);
    var narratives = JSON.parse(narrativeList);
    $(".mi-selector").select2();
    $('#cover-spin').show();
    //var narrativesProjects = JSON.parse(projects)
    var narrativeDropdown = $("#narrativeMode");
    narrativeDropdown.change(changeMode);
    var modelSelect = $('#modelSelect');
    modelSelect.change(changeMode);
    narrativeDropdown.change(changeMode);
    //Cargando las narrativas de la ejecución en la lista del proyecto
    narratives.forEach(function (narrative) {
      narrativeDropdown.append('<option selected value=' + narrative.fullname + '> Narrativa ' + narrative.name + '</option>');
    });
    var dorOutputs = JSON.parse(outputs);
    console.log("Outputs",dorOutputs)
    var baseLineResources = dorOutputs.filter(file => file.fullname.includes("Base_Line"));
  
    if (baseLineResources.length > 0)
      baseLine = true
    else
      baseLine = false;
  
    var topologicalNetwork = JSON.parse(topoNetwork);
    var baseUrl = window.location.protocol + "//" + window.location.hostname+":"+window.location.port;
    var dkanUrl = "/api/3/action/package_show?id=";
    var fullUrlDataset = baseUrl + dkanUrl + datasetUuid;
    var fullUrlNetwork = baseUrl + dkanUrl + topologicalNetwork[0].datasetUuid;
    var scale=['#DA77BF','#0477BF','#1ABE39','#F0BE39','#F092EC','#E4EF26','#DDC406','#8872C9','#DC1E90','#DCF890'];
    var networkResourceUrl;
    var noBaseLineDorAxis = [];
    var noBaseLineDorwAxis = []
    var noBaseLineFragwAxis = []
    var layout = {
      font: {
        size: 16,
      }
    }; 
    //llamada a los dataset y las network
    var datasetRequest = $.ajax({
      url: fullUrlDataset,
      type: 'GET',
    });
  
    var networkRequest = $.ajax({
      url: fullUrlNetwork,
      type: 'GET',
    });
  
    /*
    * Get response of output's datase request
    *
    * @return Object response response of request
    */
    datasetRequest.then(function (response) {
      var noBaseLineResources = [];
      var noBaseLineOutputs = [];
      /*
      * Filter no base line outputs by
      * word "narrativa" & the number
      */
     //console.log("Narrativas",narratives);

     //Filtra las outputs que no tengan linea base y los pone en noBaseLineOutputs
      for (i = 0; i <= narratives.length - 1; i++) {
        noBaseLineOutputs.push(dorOutputs.filter(out => out.fullname == "DOR_" + narratives[i].nid + "_Narrative_" + narratives[i].fullname));
        noBaseLineOutputs.push(dorOutputs.filter(out => out.fullname == "Install_Power_" + narratives[i].nid + "_Narrative_" + narratives[i].fullname));
        noBaseLineOutputs.push(dorOutputs.filter(out => out.fullname == "DORw_" + narratives[i].nid + "_Narrative_" + narratives[i].fullname));
        noBaseLineOutputs.push(dorOutputs.filter(out => out.fullname == "Aleatory_Definition_" + narratives[i].nid + "_Narrative_" + narratives[i].fullname));
        noBaseLineOutputs.push(dorOutputs.filter(out => out.fullname == "Arc_0_" + narratives[i].nid + "_Narrative_" + narratives[i].fullname));  
    }
      /*
      * Filter in dataset response, the no base
      * line resources & get the file URL
      */
      console.log("No baseline Outputs",noBaseLineOutputs)
      noBaseLineOutputs.forEach(function (output) {
        // Resources result of dataset request
        var resources = response.result[0].resources;
        console.log("Resources",resources);
        // UUID of dataset's resource
        var resourceUuid = output[0].resourceUuid;
        var filterResource = resources.filter(r => r.id == resourceUuid);
        var resourceUrl = filterResource[0].url;
        var resource = { "name": output[0].fullname, "url": resourceUrl };
        noBaseLineResources.push(resource);
      });

  
      /*
     * Get response of topological network
     * dataset request & get topological
     * network resource URL
     *
     * @return Object response response of request
     */
      networkRequest.then(function (response) {
        // Resources attah to network dataset
        var resources = response.result[0].resources;
  
        // Filter reource by UUID
        var filterResource = resources.filter(r => r.id == topologicalNetwork[0].resourceUuid);
  
        // Resource url
        networkResourceUrl = filterResource[0].url;
      });
  
      var networkResourceRequest = $.ajax({
        url: networkResourceUrl,
        type: 'GET',
      });
      //Filtro Fragmentación
      var installPowerNoBaseLine = [];
      for (i = 0; i <= narratives.length - 1; i++) {
          installPowerNoBaseLine.push(noBaseLineResources.filter(out => out.name == "Install_Power_" + narratives[i].nid + "_Narrative_" + narratives[i].fullname));
        }
     console.log("installPowerNoBaseLine",installPowerNoBaseLine)
         // Aleatory definition outputs
    var aleatoryDefinition = [];
    for (i = 0; i <= narratives.length - 1; i++) {
      aleatoryDefinition.push(noBaseLineResources.filter(out => out.name == "Aleatory_Definition_" + narratives[i].nid + "_Narrative_" + narratives[i].fullname));
    }
    var funcNoBaseLine = [];
    // Filter no base sai
    for (i = 0; i <= narratives.length - 1; i++) {
      funcNoBaseLine.push(noBaseLineResources.filter(out => out.name == "Arc_0_" + narratives[i].nid + "_Narrative_" + narratives[i].fullname));
    }
    console.log("funcNoBaseLine",funcNoBaseLine)
        /* Filter no base installed powers
    */
    var installPowerNoBaseLineRequest = [];
    installPowerNoBaseLine.forEach(function (resource) {
        var request = $.ajax({
          url: resource[0].url,
          type: 'GET',
        });
        var properties = { "name": resource[0].name, "request": request };
        installPowerNoBaseLineRequest.push(properties);
      });
      console.log("installPowerNoBaseLineRequest",installPowerNoBaseLineRequest)    
      
    //no base sai request
    var funcNoBaseLineRequest = [];
    funcNoBaseLine.forEach(function (resource) {
      var request = $.ajax({
        url: resource[0].url,
        type: 'GET',
      });
      var properties = { "name": resource[0].name, "request": request };
      funcNoBaseLineRequest.push(properties);
    });
    console.log("funcNoBaseLineRequest",funcNoBaseLineRequest);



      
      // Filter no base DOR
      //Que narrativa se trae
      var dorNoBaseLine = [];
      for (i = 0; i <= narratives.length - 1; i++) {
        dorNoBaseLine.push(noBaseLineResources.filter(out => out.name == "DOR_" + narratives[i].nid + "_Narrative_" + narratives[i].fullname));
        //console.log("Narrativas:",dorNoBaseLine);
      }
      var dorwNoBaseLine = [];
      for (i = 0; i <= narratives.length - 1; i++) {
        dorwNoBaseLine.push(noBaseLineResources.filter(out => out.name == "DORw_" + narratives[i].nid + "_Narrative_" + narratives[i].fullname));
      }
      //no base dor request
      var dorNoBaseLineRequest = [];
      dorNoBaseLine.forEach(function (resource) {
        var request = $.ajax({
          url: resource[0].url,
          type: 'GET',
        });
        var properties = { "name": resource[0].name, "request": request };
        dorNoBaseLineRequest.push(properties);
      });
  
      var dorwNoBaseLineRequest = [];
      dorwNoBaseLine.forEach(function (resource) {
        var request = $.ajax({
          url: resource[0].url,
          type: 'GET',
        });
        var properties = { "name": resource[0].name, "request": request };
        dorwNoBaseLineRequest.push(properties);
      });
      /* Global variables section */
      var networkLines;
      var riverMouthArc;
      var narrativeNid;
      var narrativeName;
      
  
      /*
     * Get response of topological network
     * resource & filter river mouth arc
     *
     * @return Object response response of request
     */
    //lectura de red topologica
      networkResourceRequest.then(function (response) {
        // Extract lines of csv in array
        networkLines = processCSv(response);
        /*
        * Loop every line & search river mouth arc
        */
        networkLines.forEach(function (row) {
          rowSplit = row[0].split(",");
          if (rowSplit[3] == '1')
            riverMouthArc = rowSplit[0];
        });
  
  
        var promisesDor = [];
        var dorNoBaseline = [];
        //Varias narrativas
        dorNoBaseLineRequest.forEach(function (resource) {
          var nid = resource.name.split("_");
          narrativeNid = nid[1];
          var name = resource.name.split("_Narrative_");
          narrativeName = name[1];
          //Agregar la promesa 
          promisesDor.push(resource.request);
          console.log(promisesDor);
          var noBaseResponse = Object();
          noBaseResponse.nid = narrativeNid;
          noBaseResponse.name = narrativeName;
          dorNoBaseline.push(noBaseResponse);
        });
        console.log("dorNoBaseline",dorNoBaseline)
        var promisesDorw = [];
        var dorwNoBaseline = [];
        dorwNoBaseLineRequest.forEach(function (resource) {
          var nid = resource.name.split("_");
          narrativeNid = nid[1];
          var name = resource.name.split("_Narrative_");
          narrativeName = name[1];
          promisesDorw.push(resource.request);
          var noBaseResponse = Object();
          noBaseResponse.nid = narrativeNid;
          noBaseResponse.name = narrativeName;
          dorwNoBaseline.push(noBaseResponse);
        });
        console.log("dorwNoBaseline",dorwNoBaseline)

        //installpower, fragmentación

        installPowerNoBaseResponse = [];

        installPowerNoBaseLineRequest.forEach(function (resource) {
          var name = resource.name.split("_");
          narrativeNid = name[2];
          narrativeName = name[4];
          resource.request.then(function (response) {
            var noBaseResponse = Object();
            noBaseResponse.response = processCSv(response);
            noBaseResponse.nid = narrativeNid;
            noBaseResponse.name = narrativeName;
            installPowerNoBaseResponse.push(noBaseResponse);
          })
        });
        console.log("installPowerNoBaseResponse",installPowerNoBaseResponse)

        //func
        var funcNoBaseLineResponse = [];
        funcNoBaseLineRequest.forEach(function (resource) {
          var nid = resource.name.split("_");
          narrativeNid = nid[1];
          var name = resource.name.split("_Narrative_");
          narrativeName = name[1];
          resource.request.then(function (response) {
            var noBaseResponse = Object();
            noBaseResponse.nid = narrativeNid;
            noBaseResponse.name = narrativeName;
            noBaseResponse.response = processCSv(response);
            funcNoBaseLineResponse.push(noBaseResponse);
          })
        });
        console.log("funcNoBaseLineResponse",funcNoBaseLineResponse)


        // When all promises all resolved


      
  
        Promise.all(promisesDor).then(promiseResponse => {
          console.log(promiseResponse);
          dorNoBaseline.forEach(function (response, index) {
            response.response = processCSv(promiseResponse[index]);
            var dorAxis = Object();
            dorAxis.axis = filterByDorArc(riverMouthArc, response.response);
            dorAxis.name = response.name;
            dorAxis.model = "DOR";
            dorAxis.nid = response.nid;
            noBaseLineDorAxis.push(dorAxis);
          });
          console.log(noBaseLineDorAxis);
          
          Promise.all(promisesDorw).then(promiseResponse => {
            console.log(promiseResponse);
            dorwNoBaseline.forEach(function (response, index) {
              response.response = processCSv(promiseResponse[index]);
              var dorwAxis = Object();
              dorwAxis.axis = filterByDorArc(riverMouthArc, response.response);
              dorwAxis.name = response.name;
              dorwAxis.model = "DORw";
              dorwAxis.nid = response.nid;
              noBaseLineDorwAxis.push(dorwAxis);
            });
            console.log(noBaseLineDorwAxis);

            Promise.all(funcNoBaseLineResponse).then(promiseResponse => {
              console.log(promiseResponse);
              funcNoBaseLineResponse.forEach(function (response, index) {
                
                var FragmentacionAxis = Object();
                FragmentacionAxis.axis = filterByDorArc("0", response.response);
                FragmentacionAxis.name = response.name;
                FragmentacionAxis.model = "Fragmentación";
                FragmentacionAxis.nid = response.nid;
                noBaseLineFragwAxis.push(FragmentacionAxis);
              });
              console.log(noBaseLineFragwAxis);
              parallelChart(noBaseLineDorAxis, noBaseLineDorwAxis,noBaseLineFragwAxis,3);
            });
            
            
  
          });
        });
  
  
      });
    });
    function changeMode(){
      var seleccionados = [];
      var narrativasselected=[];
      var narrativa;
      var finaldor =[];
      var finaldorw=[];
      var finalfrag=[];
      var selectednarra=narrativeDropdown[0].selectedOptions;
      console.log("Narrativas seleccionadas:",selectednarra)
      //console.log("Selected DOR",modelSelect[0].options[0].selected);
      //console.log("Selected DORw",modelSelect[0].options[1].selected);
      
      for(var i=0; i<selectednarra.length;i++){
        narrativa=[];
        narrativasselected.push(selectednarra[i].value); 
      }
  
      if(narrativasselected.length==0){
        alert("Se requiere de almenos 1 narrativa para mostrar el diagrama");
        
      }
      else{
        for(var i=0; i<narrativasselected.length;i++){
          //console.log(narrativasselected,i);
          noBaseLineDorAxis.filter(res=> {
            if(res.name==narrativasselected[i]){
              finaldor.push(res);
              //console.log("Filtro dor",finaldor);
            }
            else{
            }});
        }
        for(var i=0; i<narrativasselected.length;i++){
          //console.log(narrativasselected,i);
          noBaseLineDorwAxis.filter(res=> {
            if(res.name==narrativasselected[i]){
              finaldorw.push(res);
              //console.log("Filtro dorw",finaldorw);
            }
            else{
            }});
        }

        for(var i=0; i<narrativasselected.length;i++){
          //console.log(narrativasselected,i);
          noBaseLineFragwAxis.filter(res=> {
            if(res.name==narrativasselected[i]){
              finalfrag.push(res);
              //console.log("Filtro dorw",finaldorw);
            }
            else{
            }});
        }

        for(var i=0;i<modelSelect[0].options.length;i++){
          if(modelSelect[0].options[i].selected==true){
            seleccionados.push(modelSelect[0].options[i].value);
            //console.log(seleccionados)
           
          }
        }
        //console.log(seleccionados);

        if(seleccionados.length>=2){
          if(seleccionados.length==2){
   
            if(seleccionados[0]==2 && seleccionados[1]==1 || seleccionados[0]==1 && seleccionados[1]==2){
              //console.log("Dor y DorW");
              parallelChart(finaldor, finaldorw, finalfrag,4);
            }else if(seleccionados[0]==2 && seleccionados[1]==3 || seleccionados[0]==3 && seleccionados[1]==2){
              //console.log("Dor y Fragmentación");
              parallelChart(finaldor, finaldorw, finalfrag,5);
            }else if(seleccionados[0]==1 && seleccionados[1]==3 || seleccionados[0]==3 && seleccionados[1]==1){
              //console.log("DorW y Fragmentación");
              parallelChart(finaldor, finaldorw, finalfrag,6);
            }

          }else if(seleccionados.length==3){
            //Mostrando todos los datos
            parallelChart(finaldor, finaldorw, finalfrag,3);
          }


      }
      else{
        alert("Se requiere de almenos 2 modelos seleccionados para mostrar el diagrama")
      }
      }
    }
    function unpack(rows, key) {
    
      return rows.value  ;
    }
    function parallelChart(dorAxis, dorwAxis,noBaseLineFragwAxis,number) {
      if(number==4){
        console.log(noBaseLineFragwAxis);
        var values = [];
        var ticktext = [];
        var tickvals = [];
        var valuesDorAxis = [];
        var valuesDorwAxis = [];
        var valuesFragAxis=[];
        var valuesNum = dorAxis[0].axis.length;
        for (i = 1; i <=dorAxis.length; i++) {
          for (j = 1; j <= valuesNum; j++) {
            values.push(i);
          }
        }
        dorAxis.forEach(function (dor, indexDor) {
          console.log(dor);
          dor.axis.forEach(function (axis, indexAxis) {
            console.log(axis);
            valuesDorAxis.push(axis);
            valuesDorwAxis.push(dorwAxis[indexDor].axis[indexAxis]);
            valuesFragAxis.push(noBaseLineFragwAxis[indexDor].axis[indexAxis])
          });
          ticktext.push(dor.name);
          tickvals.push(indexDor + 1);
        });
        var dimensionsObject = {};
        var dimensions = [];
        dimensionsObject.label = "Narrativas";
        dimensionsObject.ticktext = ticktext;
        dimensionsObject.tickvals = tickvals;
        dimensionsObject.value = values;
        dimensions.push(dimensionsObject);
        var  scalecolor=[];
        scalecolor = new Array(dimensions[0].ticktext.length);
        for(let indexi=0;indexi<scalecolor.length;indexi++){
          scalecolor[indexi] = [];
          scalecolor[indexi][0]=indexi;
          scalecolor[indexi][1]=scale[indexi]
        }
       
        console.log("Dimensiones",dimensions[0]);  
        var trace = {
          type: 'parcoords',
          line: {
            color:unpack(dimensions[0], 'tickvals'),
            colorscale: scalecolor
          },
          dimensions: [{
            label: 'Narrativas',
            values: values,
            tickvals: tickvals,
            ticktext: ticktext,
          }, {
            label: 'DOR',
            values: valuesDorAxis,
    
          }, {
            label: 'DORw',
            values: valuesDorwAxis,
          }]
        };
        var data = [trace]
    
        Plotly.newPlot('chart', data,layout);
      }
      else if(number ==5){
        console.log(noBaseLineFragwAxis);
        var values = [];
        var ticktext = [];
        var tickvals = [];
        var valuesDorAxis = [];
        var valuesDorwAxis = [];
        var valuesFragAxis=[];
        var valuesNum = dorAxis[0].axis.length;
        for (i = 1; i <=dorAxis.length; i++) {
          for (j = 1; j <= valuesNum; j++) {
            values.push(i);
          }
        }
        dorAxis.forEach(function (dor, indexDor) {
          console.log(dor);
          dor.axis.forEach(function (axis, indexAxis) {
            console.log(axis);
            valuesDorAxis.push(axis);
            valuesDorwAxis.push(dorwAxis[indexDor].axis[indexAxis]);
            valuesFragAxis.push(noBaseLineFragwAxis[indexDor].axis[indexAxis])
          });
          ticktext.push(dor.name);
          tickvals.push(indexDor + 1);
        });
        var dimensionsObject = {};
        var dimensions = [];
        dimensionsObject.label = "Narrativas";
        dimensionsObject.ticktext = ticktext;
        dimensionsObject.tickvals = tickvals;
        dimensionsObject.value = values;
        dimensions.push(dimensionsObject);
        var  scalecolor=[];
        scalecolor = new Array(dimensions[0].ticktext.length);
        for(let indexi=0;indexi<scalecolor.length;indexi++){
          scalecolor[indexi] = [];
          scalecolor[indexi][0]=indexi;
          scalecolor[indexi][1]=scale[indexi]
        }
      
        console.log("Dimensiones",dimensions[0]);    
        var trace = {
          type: 'parcoords',
          line: {
            color:unpack(dimensions[0], 'tickvals'),
            colorscale: scalecolor
          },
          dimensions: [{
            label: 'Narrativas',
            values: values,
            tickvals: tickvals,
            ticktext: ticktext,
          }, {
            label: 'DOR',
            values: valuesDorAxis,
    
          }, {
            label: 'Fragmentación',
            values: valuesFragAxis,
          }]
        };
        var data = [trace]
    
        Plotly.newPlot('chart', data,layout);
      }
      else if( number ==6){
        console.log(noBaseLineFragwAxis);
        var values = [];
        var ticktext = [];
        var tickvals = [];
        var valuesDorAxis = [];
        var valuesDorwAxis = [];
        var valuesFragAxis=[];
        var valuesNum = dorAxis[0].axis.length;
        for (i = 1; i <=dorAxis.length; i++) {
          for (j = 1; j <= valuesNum; j++) {
            values.push(i);
          }
        }
        dorAxis.forEach(function (dor, indexDor) {
          console.log(dor);
          dor.axis.forEach(function (axis, indexAxis) {
            console.log(axis);
            valuesDorAxis.push(axis);
            valuesDorwAxis.push(dorwAxis[indexDor].axis[indexAxis]);
            valuesFragAxis.push(noBaseLineFragwAxis[indexDor].axis[indexAxis])
          });
          ticktext.push(dor.name);
          tickvals.push(indexDor + 1);
        });
        var dimensionsObject = {};
        var dimensions = [];
        dimensionsObject.label = "Narrativas";
        dimensionsObject.ticktext = ticktext;
        dimensionsObject.tickvals = tickvals;
        dimensionsObject.value = values;
        dimensions.push(dimensionsObject);
        var  scalecolor=[];
        scalecolor = new Array(dimensions[0].ticktext.length);
        for(let indexi=0;indexi<scalecolor.length;indexi++){
          scalecolor[indexi] = [];
          scalecolor[indexi][0]=indexi;
          scalecolor[indexi][1]=scale[indexi]
        }
      
        console.log("Dimensiones",dimensions[0]); 
        var trace = {
          type: 'parcoords',
          line: {
            color:unpack(dimensions[0], 'tickvals'),
            colorscale: scalecolor
          },
          dimensions: [{
            label: 'Narrativas',
            values: values,
            tickvals: tickvals,
            ticktext: ticktext,
          }, {
            label: 'DORw',
            values: valuesDorwAxis,
          }, {
            label: 'Fragmentación',
            values: valuesFragAxis,
          }]
        };
        var data = [trace]
    
        Plotly.newPlot('chart', data,layout);
      }
      else if(dorAxis!=[] && dorwAxis!=[] && noBaseLineFragwAxis!=[] && number == 3){
        console.log(noBaseLineFragwAxis);
        var values = [];
        var ticktext = [];
        var tickvals = [];
        var valuesDorAxis = [];
        var valuesDorwAxis = [];
        var valuesFragAxis=[];
        var valuesNum = dorAxis[0].axis.length;
        for (i = 1; i <=dorAxis.length; i++) {
          for (j = 1; j <= valuesNum; j++) {
            values.push(i);
          }
        }
        dorAxis.forEach(function (dor, indexDor) {
          console.log(dor);
          dor.axis.forEach(function (axis, indexAxis) {
            console.log(axis);
            valuesDorAxis.push(axis);
            valuesDorwAxis.push(dorwAxis[indexDor].axis[indexAxis]);
            valuesFragAxis.push(noBaseLineFragwAxis[indexDor].axis[indexAxis])
          });
          ticktext.push(dor.name);
          tickvals.push(indexDor + 1);
        });
        var dimensionsObject = {};
        var dimensions = [];
        dimensionsObject.label = "Narrativas";
        dimensionsObject.ticktext = ticktext;
        dimensionsObject.tickvals = tickvals;
        dimensionsObject.value = values;
        dimensions.push(dimensionsObject);
        var  scalecolor=[];
        scalecolor = new Array(dimensions[0].ticktext.length);
        for(let indexi=0;indexi<scalecolor.length;indexi++){
          scalecolor[indexi] = [];
          scalecolor[indexi][0]=indexi;
          scalecolor[indexi][1]=scale[indexi]
        }
      
        console.log("Dimensiones",dimensions[0]); 
        var trace = {
          type: 'parcoords',
          line: {
            color:unpack(dimensions[0], 'tickvals'),
            colorscale: scalecolor
          },
          dimensions: [{
            label: 'Narrativas',
            values: values,
            tickvals: tickvals,
            ticktext: ticktext,
          }, {
            label: 'DOR',
            values: valuesDorAxis,
    
          }, {
            label: 'DORw',
            values: valuesDorwAxis,
          }, {
            label: 'Fragmentación',
            values: valuesFragAxis,
          }]
        };
        var data = [trace]
    
        Plotly.newPlot('chart', data,layout);
      }
     
    }
  
    /*
    * Read a CSV file & extract the content in rows
    *
    * @param String csv    CSV File
    *
    * @return Array lines  Rows of csv readed
    */
    function processCSv(csv) {
      var allTextLines = csv.split(/\r\n|\n/);
      var lines = [];
      for (var i = 0; i < allTextLines.length - 1; i++) {
        var data = allTextLines[i].split(';');
        var tarr = [];
        for (var j = 0; j < data.length; j++) {
          tarr.push(data[j]);
        }
        lines.push(tarr);
      }
      return lines;
    }
  
    /* Get row in DOR Csv file
    *  filtering by ArcID
    *
    * @param    String   riverMouthArc    ArcID
    * @param    Array    dorFile          DOR file
    */
    function filterByDorArc(riverMouthArc, dorFile) {
      var dorRow = [];
      var dorRowFilter = [];
      /*
      * Loop every line &
      * search river mouth in DOR output
      */
      dorFile.forEach(function (row) {
        // Split columns in the string
        rowSplit = row[0].split(",");
        dorArc = rowSplit[0];
        if (dorArc == riverMouthArc) {
          dorRowFilter = rowSplit;
  
          // Convert object to string
          var row = JSON.stringify(dorRowFilter);
  
          // Replace double quotes
          var row = row.replace(/['"]+/g, '');
          // Replace brackets
          row = row.replace(/[[\]]/g, '');
  
          // Split each column by comma
          dorRowFilter = row.split(",");
  
          // Loop and convert every value in float
          dorRowFilter.forEach(function (value, index) {
            dorRowFilter[index] = parseFloat(value);
          });
        }
      });
      /* First position in array is ArcID, is no necesary
      *  so recreate array whitout the first position
      */
      for (i = 1; i <= dorRowFilter.length - 1; i++) {
        dorRow.push(dorRowFilter[i]);
      }
      return dorRow;
    }
  
  });