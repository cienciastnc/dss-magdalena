<?php
/**
 * @file
 * dss_tier1_entities.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function dss_tier1_entities_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create execution content'.
  $permissions['create execution content'] = array(
    'name' => 'create execution content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'create narrative content'.
  $permissions['create narrative content'] = array(
    'name' => 'create narrative content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'create project_catalog content'.
  $permissions['create project_catalog content'] = array(
    'name' => 'create project_catalog content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'create tier1_project content'.
  $permissions['create tier1_project content'] = array(
    'name' => 'create tier1_project content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any execution content'.
  $permissions['delete any execution content'] = array(
    'name' => 'delete any execution content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any narrative content'.
  $permissions['delete any narrative content'] = array(
    'name' => 'delete any narrative content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any project_catalog content'.
  $permissions['delete any project_catalog content'] = array(
    'name' => 'delete any project_catalog content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any tier1_project content'.
  $permissions['delete any tier1_project content'] = array(
    'name' => 'delete any tier1_project content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own execution content'.
  $permissions['delete own execution content'] = array(
    'name' => 'delete own execution content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own narrative content'.
  $permissions['delete own narrative content'] = array(
    'name' => 'delete own narrative content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own project_catalog content'.
  $permissions['delete own project_catalog content'] = array(
    'name' => 'delete own project_catalog content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own tier1_project content'.
  $permissions['delete own tier1_project content'] = array(
    'name' => 'delete own tier1_project content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit any execution content'.
  $permissions['edit any execution content'] = array(
    'name' => 'edit any execution content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit any narrative content'.
  $permissions['edit any narrative content'] = array(
    'name' => 'edit any narrative content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit any project_catalog content'.
  $permissions['edit any project_catalog content'] = array(
    'name' => 'edit any project_catalog content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit any tier1_project content'.
  $permissions['edit any tier1_project content'] = array(
    'name' => 'edit any tier1_project content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own execution content'.
  $permissions['edit own execution content'] = array(
    'name' => 'edit own execution content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own narrative content'.
  $permissions['edit own narrative content'] = array(
    'name' => 'edit own narrative content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own project_catalog content'.
  $permissions['edit own project_catalog content'] = array(
    'name' => 'edit own project_catalog content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own tier1_project content'.
  $permissions['edit own tier1_project content'] = array(
    'name' => 'edit own tier1_project content',
    'roles' => array(),
    'module' => 'node',
  );

  return $permissions;
}
