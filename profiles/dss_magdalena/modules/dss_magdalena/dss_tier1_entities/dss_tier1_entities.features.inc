<?php
/**
 * @file
 * dss_tier1_entities.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function dss_tier1_entities_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function dss_tier1_entities_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function dss_tier1_entities_node_info() {
  $items = array(
    'execution' => array(
      'name' => t('Ejecuciones'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Nombre'),
      'help' => '',
    ),
    'narrative' => array(
      'name' => t('Narrativas'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Nombre'),
      'help' => '',
    ),
    'project_catalog' => array(
      'name' => t('Catálogo de Proyectos'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Nombre'),
      'help' => '',
    ),
    'tier1_project' => array(
      'name' => t('Proyectos'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Nombre'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
