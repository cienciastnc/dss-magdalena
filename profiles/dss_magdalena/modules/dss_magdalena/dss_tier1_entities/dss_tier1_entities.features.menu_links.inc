<?php
/**
 * @file
 * dss_test.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function dss_test_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_catlogo-de-proyectos-tier1:catalog_project_list_TIER1/.
  $menu_links['main-menu_catlogo-de-proyectos-tier1:catalog_project_list_TIER1/'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'catalog_project_list_TIER1/',
    'router_path' => 'catalog_project_list_TIER1',
    'link_title' => 'Catálogo de Proyectos TIER1',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_catlogo-de-proyectos-tier1:catalog_project_list_TIER1/',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -44,
    'customized' => 1,
    'parent_identifier' => 'main-menu_future:dss/future',
  );
  // Exported menu link: main-menu_ejecuciones-tier1:execution-list.
  $menu_links['main-menu_ejecuciones-tier1:execution-list'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'execution-list',
    'router_path' => 'execution-list',
    'link_title' => 'Ejecuciones TIER1',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_ejecuciones-tier1:execution-list',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -40,
    'customized' => 1,
    'parent_identifier' => 'main-menu_future:dss/future',
  );
  // Exported menu link: main-menu_narrativas-tier1:lista-de-narrativas-tier1.
  $menu_links['main-menu_narrativas-tier1:lista-de-narrativas-tier1'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'lista-de-narrativas-tier1',
    'router_path' => 'lista-de-narrativas-tier1',
    'link_title' => 'Narrativas TIER1',
    'options' => array(
      'attributes' => array(
        'title' => 'Listado de narrativas existentes en TIER1',
      ),
      'identifier' => 'main-menu_narrativas-tier1:lista-de-narrativas-tier1',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -42,
    'customized' => 1,
    'parent_identifier' => 'main-menu_future:dss/future',
  );
  // Exported menu link: navigation_catlogo-de-proyectos:node/add/project-catalog.
  $menu_links['navigation_catlogo-de-proyectos:node/add/project-catalog'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'node/add/project-catalog',
    'router_path' => 'node/add/project-catalog',
    'link_title' => 'Catálogo de Proyectos',
    'options' => array(
      'identifier' => 'navigation_catlogo-de-proyectos:node/add/project-catalog',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'navigation_aadir-contenido:node/add',
  );
  // Exported menu link: navigation_ejecuciones:node/add/execution.
  $menu_links['navigation_ejecuciones:node/add/execution'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'node/add/execution',
    'router_path' => 'node/add/execution',
    'link_title' => 'Ejecuciones',
    'options' => array(
      'identifier' => 'navigation_ejecuciones:node/add/execution',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'navigation_aadir-contenido:node/add',
  );
  // Exported menu link: navigation_narrativas:node/add/narrative.
  $menu_links['navigation_narrativas:node/add/narrative'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'node/add/narrative',
    'router_path' => 'node/add/narrative',
    'link_title' => 'Narrativas',
    'options' => array(
      'identifier' => 'navigation_narrativas:node/add/narrative',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'navigation_aadir-contenido:node/add',
  );
  // Exported menu link: navigation_proyectos:node/add/tier1-project.
  $menu_links['navigation_proyectos:node/add/tier1-project'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'node/add/tier1-project',
    'router_path' => 'node/add/tier1-project',
    'link_title' => 'Proyectos',
    'options' => array(
      'identifier' => 'navigation_proyectos:node/add/tier1-project',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'navigation_aadir-contenido:node/add',
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Catálogo de Proyectos');
  t('Catálogo de Proyectos TIER1');
  t('Ejecuciones');
  t('Ejecuciones TIER1');
  t('Narrativas');
  t('Narrativas TIER1');
  t('Proyectos');

  return $menu_links;
}
