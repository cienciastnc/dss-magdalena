<?php
/**
 * @file
 * dss_datamodel.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function dss_datamodel_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['cch-custom_context_help'] = array(
    'cache' => 1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'custom_context_help',
    'module' => 'cch',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'nuboot_radix' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'nuboot_radix',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
