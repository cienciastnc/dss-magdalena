<?php
/**
 * @file
 * dss_datamodel.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function dss_datamodel_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create field_project'.
  $permissions['create field_project'] = array(
    'name' => 'create field_project',
    'roles' => array(
      'Sima User' => 'Sima User',
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_project'.
  $permissions['edit field_project'] = array(
    'name' => 'edit field_project',
    'roles' => array(
      'Sima User' => 'Sima User',
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_project'.
  $permissions['edit own field_project'] = array(
    'name' => 'edit own field_project',
    'roles' => array(
      'Sima User' => 'Sima User',
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_project'.
  $permissions['view field_project'] = array(
    'name' => 'view field_project',
    'roles' => array(
      'Sima User' => 'Sima User',
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_project'.
  $permissions['view own field_project'] = array(
    'name' => 'view own field_project',
    'roles' => array(
      'Sima User' => 'Sima User',
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  return $permissions;
}
