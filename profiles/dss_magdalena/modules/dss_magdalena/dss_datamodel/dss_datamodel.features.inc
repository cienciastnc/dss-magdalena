<?php
/**
 * @file
 * dss_datamodel.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function dss_datamodel_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_field_default_field_bases_alter().
 */
function dss_datamodel_field_default_field_bases_alter(&$data) {
  if (isset($data['field_dor_w_status'])) {
    $data['field_dor_w_status']['settings']['allowed_values'][0] = 'OK'; /* WAS: 'PROCESSING' */
    $data['field_dor_w_status']['settings']['allowed_values'][1] = 'OK'; /* WAS: 0 */
    $data['field_dor_w_status']['settings']['allowed_values'][2] = 0; /* WAS: 'OK' */
  }
  if (isset($data['field_sediments_status'])) {
    $data['field_sediments_status']['settings']['allowed_values'][0] = 'OK'; /* WAS: 'PROCESSING' */
    $data['field_sediments_status']['settings']['allowed_values'][1] = 'OK'; /* WAS: 0 */
    $data['field_sediments_status']['settings']['allowed_values'][2] = 0; /* WAS: 'OK' */
  }
}

/**
 * Implements hook_node_info().
 */
function dss_datamodel_node_info() {
  $items = array(
    'alternatives_resources' => array(
      'name' => t('Alternatives Resources'),
      'base' => 'node_content',
      'description' => t('Resources linked to Alternatives (Development Projects)'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'campaign' => array(
      'name' => t('Campaign File'),
      'base' => 'node_content',
      'description' => t('<em>Campaign File</em> available within a \'Resource\''),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'case_outputs' => array(
      'name' => t('Case Outputs'),
      'base' => 'node_content',
      'description' => t('Stores the relationships between a Case Study and its outputs from the different models.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'case_study' => array(
      'name' => t('Case Study'),
      'base' => 'node_content',
      'description' => t('Case Study'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'climate_scenario' => array(
      'name' => t('Climate Scenario'),
      'base' => 'node_content',
      'description' => t('<em>Climate Scenarios</em> available within an \'Scenario\''),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'demand_sites_and_catchments' => array(
      'name' => t('Demand Sites and Catchments'),
      'base' => 'node_content',
      'description' => t('WEAP Object: Demand Sites and Catchments'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'document' => array(
      'name' => t('Document File'),
      'base' => 'node_content',
      'description' => t('<em>Document File</em> available within a \'Project\' '),
      'has_title' => '1',
      'title_label' => t('Short Name'),
      'help' => '',
    ),
    'energy_scenario' => array(
      'name' => t('Energy Scenario'),
      'base' => 'node_content',
      'description' => t('<em>Energy Scenarios</em> available within an \'Scenario\''),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'hydropower' => array(
      'name' => t('Dam & Hydropower Plant File'),
      'base' => 'node_content',
      'description' => t('<em>Dam & Hydropower Plant File</em> available within a \'Project\' '),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'hydropower_alternatives' => array(
      'name' => t('Hydropower Alternatives'),
      'base' => 'node_content',
      'description' => t('<em>Hydropower Alternatives</em> available within an \'Integral Plan\''),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'integral_plan' => array(
      'name' => t('Integral Plan'),
      'base' => 'node_content',
      'description' => t('<em>Integral Plans</em> available within a \'Case Study\''),
      'has_title' => '1',
      'title_label' => t('Integral Plan Name'),
      'help' => '',
    ),
    'layers' => array(
      'name' => t('Layers'),
      'base' => 'node_content',
      'description' => t('All map layers needed by the DSS.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'model' => array(
      'name' => t('Model'),
      'base' => 'node_content',
      'description' => t('<em>Model</em> available within the \'Decision Support System\''),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'organization' => array(
      'name' => t('Organization File'),
      'base' => 'node_content',
      'description' => t('<em>Organization File</em> available within a Project'),
      'has_title' => '1',
      'title_label' => t('Organization Name'),
      'help' => '',
    ),
    'persona' => array(
      'name' => t('Persona File'),
      'base' => 'node_content',
      'description' => t('<em>Persona File</em> available within a Project'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'photography' => array(
      'name' => t('Photography File'),
      'base' => 'node_content',
      'description' => t('<em>Photography File</em> available within a Project'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'population_scenario' => array(
      'name' => t('Population Scenario'),
      'base' => 'node_content',
      'description' => t('<em>Population Scenarios</em> available within an \'Scenario\''),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'productive_land_use_alternatives' => array(
      'name' => t('Productive Land Use Alternatives'),
      'base' => 'node_content',
      'description' => t('Productive Land Use Alternatives'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'productive_land_use_variations' => array(
      'name' => t('Productive Land Use Variations'),
      'base' => 'node_content',
      'description' => t('Productive Land Use Project Variations'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'project' => array(
      'name' => t('Project File'),
      'base' => 'node_content',
      'description' => t('<em>Project File / Working Plan</em> available within the \'Decision Support System\''),
      'has_title' => '1',
      'title_label' => t('Identifying Project Name'),
      'help' => '',
    ),
    'relation' => array(
      'name' => t('Relation'),
      'base' => 'node_content',
      'description' => t('<em>Relations / Functions (y)x</em> |  These are not variables | These aren\'t associated with \'Resources´'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'river_floodplain_connectivity' => array(
      'name' => t('River-Floodplain Connectivity Alternatives'),
      'base' => 'node_content',
      'description' => t('<em>River-Floodplain Connectivity Alternatives</em> available within an \'Integral Plan\''),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'river_reaches' => array(
      'name' => t('River Reaches'),
      'base' => 'node_content',
      'description' => t('WEAP objects: River Reaches'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'ror_hydropower_plant' => array(
      'name' => t('Run of River Hydropower Plant File'),
      'base' => 'node_content',
      'description' => t('<em>Run of River Hydropower Plant File</em> available within a \'Project\''),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'scenario' => array(
      'name' => t('Scenario'),
      'base' => 'node_content',
      'description' => t('<em>Scenarios</em> available within a \'Case Study\''),
      'has_title' => '1',
      'title_label' => t('Scenario Name'),
      'help' => '',
    ),
    'selected_objectives' => array(
      'name' => t('Selected Objectives'),
      'base' => 'node_content',
      'description' => t('List of selected objectives'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'variable_vocabulary' => array(
      'name' => t('Variable Vocabulary'),
      'base' => 'node_content',
      'description' => t('Variable Vocabulary
'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'water_balance' => array(
      'name' => t('Water Balance'),
      'base' => 'node_content',
      'description' => t('<em>Water Balances</em> available within a \'Resource\''),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'water_balance_template' => array(
      'name' => t('Water Balance Template'),
      'base' => 'node_content',
      'description' => t('<em>Template</em> available to create Water Balances associated with \'Variables\''),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'wetlands_management' => array(
      'name' => t('Wetlands Management File'),
      'base' => 'node_content',
      'description' => t('<em>Wetlands Management File</em> available within a \'Project\''),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'wetlands_management_variations' => array(
      'name' => t('Wetlands Management Variations'),
      'base' => 'node_content',
      'description' => t('Wetlands Management Project Variations'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
