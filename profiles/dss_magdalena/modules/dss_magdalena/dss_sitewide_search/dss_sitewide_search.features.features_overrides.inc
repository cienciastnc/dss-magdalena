<?php
/**
 * @file
 * dss_sitewide_search.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function dss_sitewide_search_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: page_manager_pages
  $overrides["page_manager_pages.datasets.default_handlers|page_datasets__without-topics|conf|display|content|new-0541c9a5-2e8c-488f-8db4-4cd064bdc903|position"] = 13;
  $overrides["page_manager_pages.datasets.default_handlers|page_datasets__without-topics|conf|display|content|new-0ab212ba-7219-47a7-88b2-317bbc8789f7"] = (object) array(
      'pid' => 'new-0ab212ba-7219-47a7-88b2-317bbc8789f7',
      'panel' => 'sidebar',
      'type' => 'block',
      'subtype' => 'facetapi-rC3TulGxNlY6YCq1491Ur1FILVi4u20c',
      'shown' => TRUE,
      'access' => array(),
      'configuration' => array(
        'override_title' => 1,
        'override_title_text' => 'PO',
        'override_title_heading' => 'h2',
      ),
      'cache' => array(),
      'style' => array(
        'settings' => array(
          'pane_title' => '%title',
          'pane_collapsed' => 1,
          'pane_empty_check' => 0,
        ),
        'style' => 'collapsible',
      ),
      'css' => array(),
      'extras' => array(),
      'position' => 9,
      'locks' => array(),
      'uuid' => '0ab212ba-7219-47a7-88b2-317bbc8789f7',
    );
  $overrides["page_manager_pages.datasets.default_handlers|page_datasets__without-topics|conf|display|content|new-1946db71-2c78-4c13-8d68-fa4eb5d7b8fd|configuration|override_title_text"] = t('Type of Information');
  $overrides["page_manager_pages.datasets.default_handlers|page_datasets__without-topics|conf|display|content|new-1946db71-2c78-4c13-8d68-fa4eb5d7b8fd|position"] = 1;
  $overrides["page_manager_pages.datasets.default_handlers|page_datasets__without-topics|conf|display|content|new-1ee5d84c-1ea4-4726-ba4d-291779f9c493|position"] = 1;
  $overrides["page_manager_pages.datasets.default_handlers|page_datasets__without-topics|conf|display|content|new-2c4dafec-e8c8-49bd-b6d8-8b1fbf291a3a"] = (object) array(
      'pid' => 'new-2c4dafec-e8c8-49bd-b6d8-8b1fbf291a3a',
      'panel' => 'sidebar',
      'type' => 'block',
      'subtype' => 'facetapi-ZgTGSuyeJLEOY70MR1cRQYm8WTrnoNHa',
      'shown' => TRUE,
      'access' => array(),
      'configuration' => array(
        'override_title' => 1,
        'override_title_text' => 'Data Source',
        'override_title_heading' => 'h2',
      ),
      'cache' => array(),
      'style' => array(
        'settings' => array(
          'pane_title' => '%title',
          'pane_collapsed' => 1,
          'pane_empty_check' => 0,
        ),
        'style' => 'collapsible',
      ),
      'css' => array(),
      'extras' => array(),
      'position' => 10,
      'locks' => array(),
      'uuid' => '2c4dafec-e8c8-49bd-b6d8-8b1fbf291a3a',
    );
  $overrides["page_manager_pages.datasets.default_handlers|page_datasets__without-topics|conf|display|content|new-3bb6aa2e-1444-4913-bf39-e0bcc884ec90"] = (object) array(
      'pid' => 'new-3bb6aa2e-1444-4913-bf39-e0bcc884ec90',
      'panel' => 'sidebar',
      'type' => 'block',
      'subtype' => 'facetapi-D6CEkwF1v7VcMwFgoYTeAMCes1vY0uPd',
      'shown' => TRUE,
      'access' => array(),
      'configuration' => array(
        'override_title' => 1,
        'override_title_text' => 'PO',
        'override_title_heading' => 'h2',
      ),
      'cache' => array(),
      'style' => array(
        'settings' => array(
          'pane_title' => '%title',
          'pane_collapsed' => 1,
          'pane_empty_check' => 0,
        ),
        'style' => 'collapsible',
      ),
      'css' => array(),
      'extras' => array(),
      'position' => 11,
      'locks' => array(),
      'uuid' => '3bb6aa2e-1444-4913-bf39-e0bcc884ec90',
    );
  $overrides["page_manager_pages.datasets.default_handlers|page_datasets__without-topics|conf|display|content|new-42b5b05b-8c20-4f0d-9553-3cd3e38f4a0d"] = (object) array(
      'pid' => 'new-42b5b05b-8c20-4f0d-9553-3cd3e38f4a0d',
      'panel' => 'sidebar',
      'type' => 'block',
      'subtype' => 'facetapi-x73sRdbqht0H0ZxIetFaMMN3Zigp1xOX',
      'shown' => TRUE,
      'access' => array(),
      'configuration' => array(
        'override_title' => 1,
        'override_title_text' => 'SWOT',
        'override_title_heading' => 'h2',
      ),
      'cache' => array(),
      'style' => array(
        'settings' => array(
          'pane_title' => '%title',
          'pane_collapsed' => 1,
          'pane_empty_check' => 0,
        ),
        'style' => 'collapsible',
      ),
      'css' => array(),
      'extras' => array(),
      'position' => 12,
      'locks' => array(),
      'uuid' => '42b5b05b-8c20-4f0d-9553-3cd3e38f4a0d',
    );
  $overrides["page_manager_pages.datasets.default_handlers|page_datasets__without-topics|conf|display|content|new-51f2a5b5-ab6a-474a-b0f8-4a0b297160a8|position"] = 15;
  $overrides["page_manager_pages.datasets.default_handlers|page_datasets__without-topics|conf|display|content|new-549159dd-e073-4e10-9307-b13fad742409|position"] = 16;
  $overrides["page_manager_pages.datasets.default_handlers|page_datasets__without-topics|conf|display|content|new-5bccceaa-94dd-44df-bf96-e8ee8de3e620"] = (object) array(
      'pid' => 'new-5bccceaa-94dd-44df-bf96-e8ee8de3e620',
      'panel' => 'sidebar',
      'type' => 'block',
      'subtype' => 'facetapi-Fk58xAf1BreM8XXBRzGwOSdhyZUgoU1Y',
      'shown' => TRUE,
      'access' => array(),
      'configuration' => array(
        'override_title' => 1,
        'override_title_text' => 'Spatial Scale',
        'override_title_heading' => 'h2',
      ),
      'cache' => array(),
      'style' => array(
        'settings' => array(
          'pane_title' => '%title',
          'pane_collapsed' => 1,
          'pane_empty_check' => 0,
        ),
        'style' => 'collapsible',
      ),
      'css' => array(),
      'extras' => array(),
      'position' => 14,
      'locks' => array(),
      'uuid' => '5bccceaa-94dd-44df-bf96-e8ee8de3e620',
    );
  $overrides["page_manager_pages.datasets.default_handlers|page_datasets__without-topics|conf|display|content|new-6462d5a0-7cba-4625-8366-67665d121909"] = (object) array(
      'pid' => 'new-6462d5a0-7cba-4625-8366-67665d121909',
      'panel' => 'sidebar',
      'type' => 'block',
      'subtype' => 'facetapi-qeKKWhSat4MTTWver7VTdAJo8uaQNpah',
      'shown' => TRUE,
      'access' => array(),
      'configuration' => array(
        'override_title' => 1,
        'override_title_text' => 'Thematic Tree',
        'override_title_heading' => 'h2',
      ),
      'cache' => array(),
      'style' => array(
        'settings' => array(
          'pane_title' => '%title',
          'pane_collapsed' => 0,
          'pane_empty_check' => 0,
        ),
        'style' => 'collapsible',
      ),
      'css' => array(),
      'extras' => array(),
      'position' => 4,
      'locks' => array(),
      'uuid' => '6462d5a0-7cba-4625-8366-67665d121909',
    );
  $overrides["page_manager_pages.datasets.default_handlers|page_datasets__without-topics|conf|display|content|new-97159d57-3248-4fec-9b92-d9e77ced29fe"] = (object) array(
      'pid' => 'new-97159d57-3248-4fec-9b92-d9e77ced29fe',
      'panel' => 'sidebar',
      'type' => 'block',
      'subtype' => 'facetapi-rNap6LsW0S2bWWxIsB1OD8d1u0UEMrZQ',
      'shown' => TRUE,
      'access' => array(),
      'configuration' => array(
        'override_title' => 1,
        'override_title_text' => 'Case Study',
        'override_title_heading' => 'h2',
      ),
      'cache' => array(),
      'style' => array(
        'settings' => array(
          'pane_title' => '%title',
          'pane_collapsed' => 1,
          'pane_empty_check' => 0,
        ),
        'style' => 'collapsible',
      ),
      'css' => array(),
      'extras' => array(),
      'position' => 2,
      'locks' => array(),
      'uuid' => '97159d57-3248-4fec-9b92-d9e77ced29fe',
    );
  $overrides["page_manager_pages.datasets.default_handlers|page_datasets__without-topics|conf|display|content|new-9ce6ff04-333c-40f7-8164-c58abb2b9fc6"] = (object) array(
      'pid' => 'new-9ce6ff04-333c-40f7-8164-c58abb2b9fc6',
      'panel' => 'contentmain',
      'type' => 'block',
      'subtype' => 'dss_interface-sima_evaluation',
      'shown' => TRUE,
      'access' => array(),
      'configuration' => array(
        'override_title' => 1,
        'override_title_text' => '',
        'override_title_heading' => 'h2',
      ),
      'cache' => array(),
      'style' => array(),
      'css' => array(),
      'extras' => array(),
      'position' => 0,
      'locks' => array(),
      'uuid' => '9ce6ff04-333c-40f7-8164-c58abb2b9fc6',
    );
  $overrides["page_manager_pages.datasets.default_handlers|page_datasets__without-topics|conf|display|content|new-af542600-62f2-4baf-b2c3-12b9ee1d59ba|position"] = 3;
  $overrides["page_manager_pages.datasets.default_handlers|page_datasets__without-topics|conf|display|content|new-afef7d51-d03b-4cdb-a6c2-f6925370f363"] = (object) array(
      'pid' => 'new-afef7d51-d03b-4cdb-a6c2-f6925370f363',
      'panel' => 'sidebar',
      'type' => 'block',
      'subtype' => 'facetapi-TZ623xwBQAfCqF8l1NSozLy1NJrixl5E',
      'shown' => TRUE,
      'access' => array(),
      'configuration' => array(
        'override_title' => 1,
        'override_title_text' => 'DPSIR',
        'override_title_heading' => 'h2',
      ),
      'cache' => array(),
      'style' => array(
        'settings' => array(
          'pane_title' => '%title',
          'pane_collapsed' => 1,
          'pane_empty_check' => 0,
        ),
        'style' => 'collapsible',
      ),
      'css' => array(),
      'extras' => array(),
      'position' => 8,
      'locks' => array(),
      'uuid' => 'afef7d51-d03b-4cdb-a6c2-f6925370f363',
    );
  $overrides["page_manager_pages.datasets.default_handlers|page_datasets__without-topics|conf|display|content|new-b0fb8ba0-c78f-4b2e-9ae3-e4cc61f06345|position"] = 5;
  $overrides["page_manager_pages.datasets.default_handlers|page_datasets__without-topics|conf|display|content|new-c1fe0a6b-de21-4134-af35-8982fc1dd2d1"] = (object) array(
      'pid' => 'new-c1fe0a6b-de21-4134-af35-8982fc1dd2d1',
      'panel' => 'sidebar',
      'type' => 'block',
      'subtype' => 'facetapi-ypiSIuG3qS4eUM34KnNF8Bm0402PyhmL',
      'shown' => TRUE,
      'access' => array(),
      'configuration' => array(
        'override_title' => 1,
        'override_title_text' => 'Pertinencia de Análisis',
        'override_title_heading' => 'h2',
      ),
      'cache' => array(),
      'style' => array(
        'settings' => NULL,
      ),
      'css' => array(),
      'extras' => array(),
      'position' => 0,
      'locks' => array(),
      'uuid' => 'c1fe0a6b-de21-4134-af35-8982fc1dd2d1',
    );
  $overrides["page_manager_pages.datasets.default_handlers|page_datasets__without-topics|conf|display|content|new-d8e85726-d7c6-4488-aeca-8ec2e9ee56a4"] = (object) array(
      'pid' => 'new-d8e85726-d7c6-4488-aeca-8ec2e9ee56a4',
      'panel' => 'sidebar',
      'type' => 'block',
      'subtype' => 'facetapi-PH8UNGwk3FshpSIGgpXR4SFLU28Vu5X4',
      'shown' => TRUE,
      'access' => array(),
      'configuration' => array(
        'override_title' => 1,
        'override_title_text' => 'Variable Type',
        'override_title_heading' => 'h2',
      ),
      'cache' => array(),
      'style' => array(
        'settings' => NULL,
      ),
      'css' => array(),
      'extras' => array(),
      'position' => 6,
      'locks' => array(),
      'uuid' => 'd8e85726-d7c6-4488-aeca-8ec2e9ee56a4',
    );
  $overrides["page_manager_pages.datasets.default_handlers|page_datasets__without-topics|conf|display|content|new-f6a55ab1-2781-4648-9b03-5176e1d6e635"] = (object) array(
      'pid' => 'new-f6a55ab1-2781-4648-9b03-5176e1d6e635',
      'panel' => 'sidebar',
      'type' => 'block',
      'subtype' => 'facetapi-B7eU0K0YDLFsnKhG0Notwme9hCyashZd',
      'shown' => TRUE,
      'access' => array(),
      'configuration' => array(
        'override_title' => 1,
        'override_title_text' => 'Modeling Variables',
        'override_title_heading' => 'h2',
      ),
      'cache' => array(),
      'style' => array(
        'settings' => array(
          'pane_title' => '%title',
          'pane_collapsed' => 1,
          'pane_empty_check' => 0,
        ),
        'style' => 'collapsible',
      ),
      'css' => array(),
      'extras' => array(),
      'position' => 7,
      'locks' => array(),
      'uuid' => 'f6a55ab1-2781-4648-9b03-5176e1d6e635',
    );
  $overrides["page_manager_pages.datasets.default_handlers|page_datasets__without-topics|conf|display|panels|contentmain|0"] = 'new-9ce6ff04-333c-40f7-8164-c58abb2b9fc6';
  $overrides["page_manager_pages.datasets.default_handlers|page_datasets__without-topics|conf|display|panels|contentmain|1"] = 'new-1ee5d84c-1ea4-4726-ba4d-291779f9c493';
  $overrides["page_manager_pages.datasets.default_handlers|page_datasets__without-topics|conf|display|panels|sidebar|0"] = 'new-c1fe0a6b-de21-4134-af35-8982fc1dd2d1';
  $overrides["page_manager_pages.datasets.default_handlers|page_datasets__without-topics|conf|display|panels|sidebar|1"] = 'new-1946db71-2c78-4c13-8d68-fa4eb5d7b8fd';
  $overrides["page_manager_pages.datasets.default_handlers|page_datasets__without-topics|conf|display|panels|sidebar|10"] = 'new-2c4dafec-e8c8-49bd-b6d8-8b1fbf291a3a';
  $overrides["page_manager_pages.datasets.default_handlers|page_datasets__without-topics|conf|display|panels|sidebar|11"] = 'new-3bb6aa2e-1444-4913-bf39-e0bcc884ec90';
  $overrides["page_manager_pages.datasets.default_handlers|page_datasets__without-topics|conf|display|panels|sidebar|12"] = 'new-42b5b05b-8c20-4f0d-9553-3cd3e38f4a0d';
  $overrides["page_manager_pages.datasets.default_handlers|page_datasets__without-topics|conf|display|panels|sidebar|13"] = 'new-0541c9a5-2e8c-488f-8db4-4cd064bdc903';
  $overrides["page_manager_pages.datasets.default_handlers|page_datasets__without-topics|conf|display|panels|sidebar|14"] = 'new-5bccceaa-94dd-44df-bf96-e8ee8de3e620';
  $overrides["page_manager_pages.datasets.default_handlers|page_datasets__without-topics|conf|display|panels|sidebar|15"] = 'new-51f2a5b5-ab6a-474a-b0f8-4a0b297160a8';
  $overrides["page_manager_pages.datasets.default_handlers|page_datasets__without-topics|conf|display|panels|sidebar|16"] = 'new-549159dd-e073-4e10-9307-b13fad742409';
  $overrides["page_manager_pages.datasets.default_handlers|page_datasets__without-topics|conf|display|panels|sidebar|2"] = 'new-97159d57-3248-4fec-9b92-d9e77ced29fe';
  $overrides["page_manager_pages.datasets.default_handlers|page_datasets__without-topics|conf|display|panels|sidebar|3"] = 'new-af542600-62f2-4baf-b2c3-12b9ee1d59ba';
  $overrides["page_manager_pages.datasets.default_handlers|page_datasets__without-topics|conf|display|panels|sidebar|4"] = 'new-6462d5a0-7cba-4625-8366-67665d121909';
  $overrides["page_manager_pages.datasets.default_handlers|page_datasets__without-topics|conf|display|panels|sidebar|5"] = 'new-b0fb8ba0-c78f-4b2e-9ae3-e4cc61f06345';
  $overrides["page_manager_pages.datasets.default_handlers|page_datasets__without-topics|conf|display|panels|sidebar|6"] = 'new-d8e85726-d7c6-4488-aeca-8ec2e9ee56a4';
  $overrides["page_manager_pages.datasets.default_handlers|page_datasets__without-topics|conf|display|panels|sidebar|7"] = 'new-f6a55ab1-2781-4648-9b03-5176e1d6e635';
  $overrides["page_manager_pages.datasets.default_handlers|page_datasets__without-topics|conf|display|panels|sidebar|8"] = 'new-afef7d51-d03b-4cdb-a6c2-f6925370f363';
  $overrides["page_manager_pages.datasets.default_handlers|page_datasets__without-topics|conf|display|panels|sidebar|9"] = 'new-0ab212ba-7219-47a7-88b2-317bbc8789f7';

  // Exported overrides for: search_api_index
  $overrides["search_api_index.datasets.options|data_alter_callbacks|panelizer"] = array(
    'status' => 0,
    'weight' => 0,
    'settings' => array(),
  );
  $overrides["search_api_index.datasets.options|data_alter_callbacks|search_api_alter_add_hierarchy|settings|fields|field_associated_thematic_tree:parent"] = 'field_associated_thematic_tree:parent';
  $overrides["search_api_index.datasets.options|data_alter_callbacks|search_api_alter_add_hierarchy|status"] = 1;
  $overrides["search_api_index.datasets.options|data_alter_callbacks|search_api_alter_bundle_filter"] = array(
    'status' => 0,
    'weight' => -10,
    'settings' => array(
      'default' => 1,
      'bundles' => array(),
    ),
  );
  $overrides["search_api_index.datasets.options|fields|field_appropriate_spatial_scale"] = array(
    'type' => 'integer',
    'entity_type' => 'taxonomy_term',
  );
  $overrides["search_api_index.datasets.options|fields|field_associated_thematic_tree"] = array(
    'type' => 'list<integer>',
    'entity_type' => 'taxonomy_term',
  );
  $overrides["search_api_index.datasets.options|fields|field_dpsir"] = array(
    'type' => 'integer',
    'entity_type' => 'taxonomy_term',
  );
  $overrides["search_api_index.datasets.options|fields|field_dss_data_source"] = array(
    'type' => 'text',
  );
  $overrides["search_api_index.datasets.options|fields|field_numeric_scale"] = array(
    'type' => 'integer',
    'entity_type' => 'taxonomy_term',
  );
  $overrides["search_api_index.datasets.options|fields|field_po"] = array(
    'type' => 'integer',
    'entity_type' => 'taxonomy_term',
  );
  $overrides["search_api_index.datasets.options|fields|field_relevance_analysis"] = array(
    'type' => 'string',
  );
  $overrides["search_api_index.datasets.options|fields|field_resources:field_dpsir"] = array(
    'type' => 'list<integer>',
    'entity_type' => 'taxonomy_term',
  );
  $overrides["search_api_index.datasets.options|fields|field_resources:field_model_case_study_id"] = array(
    'type' => 'list<integer>',
    'entity_type' => 'node',
  );
  $overrides["search_api_index.datasets.options|fields|field_resources:field_po"] = array(
    'type' => 'list<integer>',
    'entity_type' => 'taxonomy_term',
  );
  $overrides["search_api_index.datasets.options|fields|field_swot"] = array(
    'type' => 'integer',
    'entity_type' => 'taxonomy_term',
  );
  $overrides["search_api_index.datasets.options|fields|field_topic"]["DELETED"] = TRUE;
  $overrides["search_api_index.datasets.options|fields|field_type"] = array(
    'type' => 'integer',
    'entity_type' => 'taxonomy_term',
  );
  $overrides["search_api_index.datasets.options|fields|field_variable_status"] = array(
    'type' => 'integer',
    'entity_type' => 'taxonomy_term',
  );

  // Exported overrides for: search_api_server
  $overrides["search_api_server.datasets.options|indexes|datasets|field_appropriate_spatial_scale"] = array(
    'table' => 'search_api_db_datasets',
    'column' => 'field_appropriate_spatial_scale',
    'type' => 'integer',
    'boost' => '1.0',
  );
  $overrides["search_api_server.datasets.options|indexes|datasets|field_associated_thematic_tree"] = array(
    'table' => 'search_api_db_datasets_field_associated_thematic_tree',
    'column' => 'value',
    'type' => 'list<integer>',
    'boost' => '1.0',
  );
  $overrides["search_api_server.datasets.options|indexes|datasets|field_dpsir"] = array(
    'table' => 'search_api_db_datasets',
    'column' => 'field_dpsir',
    'type' => 'integer',
    'boost' => '1.0',
  );
  $overrides["search_api_server.datasets.options|indexes|datasets|field_dss_data_source"] = array(
    'table' => 'search_api_db_datasets_text',
    'type' => 'text',
    'boost' => '1.0',
  );
  $overrides["search_api_server.datasets.options|indexes|datasets|field_numeric_scale"] = array(
    'table' => 'search_api_db_datasets',
    'column' => 'field_numeric_scale',
    'type' => 'integer',
    'boost' => '1.0',
  );
  $overrides["search_api_server.datasets.options|indexes|datasets|field_po"] = array(
    'table' => 'search_api_db_datasets',
    'column' => 'field_po',
    'type' => 'integer',
    'boost' => '1.0',
  );
  $overrides["search_api_server.datasets.options|indexes|datasets|field_relevance_analysis"] = array(
    'table' => 'search_api_db_datasets',
    'column' => 'field_relevance_analysis',
    'type' => 'string',
    'boost' => '1.0',
  );
  $overrides["search_api_server.datasets.options|indexes|datasets|field_resources:field_dpsir"] = array(
    'table' => 'search_api_db_datasets_field_resources_field_dpsir',
    'column' => 'value',
    'type' => 'list<integer>',
    'boost' => '1.0',
  );
  $overrides["search_api_server.datasets.options|indexes|datasets|field_resources:field_model_case_study_id"] = array(
    'table' => 'search_api_db_datasets_field_resources_field_model_case_study_',
    'column' => 'value',
    'type' => 'list<integer>',
    'boost' => '1.0',
  );
  $overrides["search_api_server.datasets.options|indexes|datasets|field_resources:field_po"] = array(
    'table' => 'search_api_db_datasets_field_resources_field_po',
    'column' => 'value',
    'type' => 'list<integer>',
    'boost' => '1.0',
  );
  $overrides["search_api_server.datasets.options|indexes|datasets|field_swot"] = array(
    'table' => 'search_api_db_datasets',
    'column' => 'field_swot',
    'type' => 'integer',
    'boost' => '1.0',
  );
  $overrides["search_api_server.datasets.options|indexes|datasets|field_topic"]["DELETED"] = TRUE;
  $overrides["search_api_server.datasets.options|indexes|datasets|field_type"] = array(
    'table' => 'search_api_db_datasets',
    'column' => 'field_type',
    'type' => 'integer',
    'boost' => '1.0',
  );
  $overrides["search_api_server.datasets.options|indexes|datasets|field_variable_status"] = array(
    'table' => 'search_api_db_datasets',
    'column' => 'field_variable_status',
    'type' => 'integer',
    'boost' => '1.0',
  );
  $overrides["search_api_server.datasets.options|indexes|datasets|og_group_ref|table"] = 'search_api_db_datasets_og_group_ref';

 return $overrides;
}
