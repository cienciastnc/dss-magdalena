/**
 * @file
 * Dashboard
 * Read outputs of app pesacadores and show
 * graphs and tables
 * @author Daniel Parra.
 * @since 2019
 */

jQuery(document).ready(function($) {
  //
  $("#dtpini").datetimepicker({
    icons: {
      previous: "fa fa-chevron-left",
      next: "fa fa-chevron-right"
    },
    viewMode: "months",
    format: "YYYY-MM-DD"
  });

  $("#dtpfin").datetimepicker({
    icons: {
      previous: "fa fa-chevron-left",
      next: "fa fa-chevron-right"
    },
    viewMode: "months",
    format: "YYYY-MM-DD"
  });

  $("#dtpini").on("dp.change", function(e) {
    $("#dtpfin")
      .data("DateTimePicker")
      .minDate(e.date);
  });
  $("#dtpfin").on("dp.change", function(e) {
    $("#dtpini")
      .data("DateTimePicker")
      .maxDate(e.date);
  });

  //Construccion de las URLs
  var BaseUrl = window.location.protocol + "//test.sima-magdalena.co";
  //URLs
  var PescadoresUrl =
    "/api/fishermanApp/entity_node.json?parameters[type]=fisherman&fields=field_document,field_firstname,field_lastname,nid&page=0&pagesize=1000";
  var DepartamentosUrl =
    "/api/fishermanApp/entity_node.json?parameters[type]=department&fields=field_name,nid&page=0&pagesize=1000";
  var MunicipiosUrl =
    "/api/fishermanApp/entity_node.json?parameters[type]=municipality&fields=field_id_municipality,field_name_municipality,field_id_department_sima&page=0&pagesize=1000";
  var VentasUrl =
    "/api/fishermanApp/entity_node.json?parameters[type]=sale&fields=field_sale_date,field_id_fisherman_sima,field_total,field_sale_total_kg_&page=0&pagesize=1000";
  var EspeciesUrl =
    "/api/fishermanApp/entity_node.json?parameters[type]=species&fields=nid,field_name&page=0&pagesize=1000";
  var FaenasUrl =
    "/api/fishermanApp/entity_node.json?parameters[type]=labor&fields=nid,field_name,field_start_date,field_abor_time,field_id_fisherman_sima,field_fuel_value,field_oil_value,field_ice_value,field_personal_value,field_renting_value,field_other_value&page=0&pagesize=1000";
  var FaenasEspeciesUrl =
    "/api/fishermanApp/entity_node.json?parameters[type]=labor_specie&fields=title,field_id_specie_sima,field_id_labor_sima,field_date_labor,field_weight&page=0&pagesize=1000";

  //Creación de las URls completas
  var FullUrlPescadores = BaseUrl + PescadoresUrl;
  var FullUrlDepartamentos = BaseUrl + DepartamentosUrl;
  var FullUrlMunicipios = BaseUrl + MunicipiosUrl;
  var FullUrlVentas = BaseUrl + VentasUrl;
  var FullUrlEspecies = BaseUrl + EspeciesUrl;
  var FullUrlFaenas = BaseUrl + FaenasUrl;
  var FullUrlFaenasEspecies = BaseUrl + FaenasEspeciesUrl;

  //funciones para obtener los datos de las urls (Archivos .Json)
  var PescadoresRequest = $.ajax({
    url: FullUrlPescadores,
    type: "GET"
  });
  var DepartamentosRequest = $.ajax({
    url: FullUrlDepartamentos,
    type: "GET"
  });
  var MunicipiosRequest = $.ajax({
    url: FullUrlMunicipios,
    type: "GET"
  });
  var VentasRequest = $.ajax({
    url: FullUrlVentas,
    type: "GET"
  });
  var EspeciesRequest = $.ajax({
    url: FullUrlEspecies
  });
  var FaenasRequest = $.ajax({
    url: FullUrlFaenas,
    type: "GET"
  });
  var FaenasEspeciesRequest = $.ajax({
    url: FullUrlFaenasEspecies,
    type: "GET"
  });

  //Respuestas de los metodos tipo GET

  var ResponsePescadores;
  var ResponseDepartamentos;
  var ResponseMunicipios;
  var ResponseVentas;
  var ResponseEspecies;
  var ResponseFaenas;
  var ResponseFaenasEspecies;
  /*
   * Get response of output's datase request
   *
   * @return Object response response of request
   */

  DepartamentosRequest.then(function(ResponseDepartamentos) {
    MunicipiosRequest.then(function(ResponseMunicipios) {
      /**
       * Departamentos
       *
       * Se realiza el recorrido del archivo json para llenar
       * los select con nombres(field_name) y nid (identificador)
       */

      for (let item of ResponseDepartamentos) {
        $(".mi-selector").select2();
        $("#SelectedDepartamento").append(
          "<option value=" +
            item.nid +
            ">" +
            item.field_name.und[0].value +
            "</option>"
        );
      }
      /**
       * Función para llenar el select de municipio segun el departamento elegido
       */
      var llenarmunicipio = function() {
        $("#SelectedMunicipio").empty();
        for (const item of ResponseMunicipios) {
          if (
            $("#SelectedDepartamento").val() ===
            item.field_id_department_sima.und[0].target_id
          ) {
            $("#SelectedMunicipio").append(
              "<option value= '" +
                item.field_id_municipality.und[0].value +
                "'>" +
                item.field_name_municipality.und[0].value +
                "</option>"
            );
          } else {
          }
        }
      };

      PescadoresRequest.then(function(ResponsePescadores) {
        EspeciesRequest.then(function(ResponseEspecies) {
          FaenasRequest.then(function(ResponseFaenas) {
            FaenasEspeciesRequest.then(function(ResponseFaenasEspecies) {
              VentasRequest.then(function(ResponseVentas) {
                //Evento que se activa al detectar un cambio en el select
                $("#SelectedDepartamento").change(llenarmunicipio);

                for (let item of ResponsePescadores) {
                  /**
                   * se realiza el recorrido del archivo json para llenar
                   * los select con nombres(field_firstname field_lastname) y Documento (Docfisherman)
                   */
                  $("#SelectedPescadores").append(
                    "<option value=" +
                      item.field_document.und[0].value +
                      ">" +
                      item.field_firstname.und[0].value +
                      " " +
                      item.field_lastname.und[0].value +
                      " - " +
                      item.field_document.und[0].value +
                      "</option>"
                  );
                }

                for (let item of ResponseEspecies) {
                  /**
                   * se realiza el recorrido del archivo json para llenar
                   * los select con nombres(field_name) y nid (identificador)
                   */
                  $("#SelectedEspecie").append(
                    "<option value=" +
                      item.nid +
                      ">" +
                      item.field_name.und[0].value +
                      "</option>"
                  );
                }

                /**
                 * Este Evento crea el reporte de venta
                 */
                $("#btnVentas").click(consultaVenta);
                function consultaVenta() {
                  //filtro de ventas por fecha
                  let buscar = ResponseVentas.filter(
                    n =>
                      n.field_sale_date.und[0].value >= $("#dtpini").val() &&
                      n.field_sale_date.und[0].value <= $("#dtpfin").val()
                  );
                  //Creacion de los arreglos  extraidos de la api filtrada por fecha
                  var FechaVenta = [];
                  var TotalVenta = [];
                  var Kg = [];
                  var VentaTotalMes = [];
                  for (let item of buscar) {
                    if (item.field_total.und[0].value != "0") {
                      var dato1 = ResponsePescadores.filter(function(data) {
                        return (
                          data.nid ==
                          item.field_id_fisherman_sima.und[0].target_id
                        );
                      });
                      FechaVenta.push(item.field_sale_date.und[0].value);
                      TotalVenta.push(
                        parseInt(item.field_total.und[0].value, 10)
                      );
                      Kg.push(item.field_sale_total_kg_.und[0].value);
                      TotalMes = {
                        date: [
                          new Date(
                            item.field_sale_date.und[0].value
                          ).getFullYear() +
                            "-" +
                            (1 +
                              new Date(
                                item.field_sale_date.und[0].value
                              ).getMonth() +
                              "-" +
                              new Date(
                                item.field_sale_date.und[0].value
                              ).getDate())
                        ],
                        total: [parseInt(item.field_total.und[0].value, 10)]
                      };
                      VentaTotalMes.push(TotalMes);
                    }
                  }

                  var groupBy = function(miarray, prop) {
                    return miarray.reduce(function(groups, item) {
                      var val = item[prop];
                      groups[val] = groups[val] || {
                        date: item.date,
                        total: 0
                      };
                      groups[val].total += item.total[0];
                      return groups;
                    }, {});
                  };

                  //DATOS DE LA TABLA VENTA
                  var ValTablaVenta = [FechaVenta, TotalVenta, Kg];

                  var datatableventa = [
                    {
                      type: "table",
                      header: {
                        values: [
                          ["<b>Ventas por mes</b>"],
                          ["<b>Total</b>"],
                          ["<b>Kg</b>"]
                        ],
                        align: "center",
                        line: { width: 1, color: "black" },
                        fill: { color: "grey" },
                        font: {
                          family: "Arial",
                          size: 12,
                          color: "white"
                        }
                      },
                      cells: {
                        values: ValTablaVenta,
                        align: "center",
                        line: { color: "black", width: 1 },
                        font: {
                          family: "Arial",
                          size: 11,
                          color: ["black"]
                        }
                      }
                    }
                  ]; //Fin de la tabla

                  var LayoutTablaVenta = {};

                  try {
                    Plotly.plot(
                      "TablaVenta",
                      datatableventa,
                      LayoutTablaVenta,
                      {
                        responsive: true
                      }
                    );
                  } catch (error) {}
                  /*
                  var Xvalores = [];
                  var Yvalores = [];
                  for (
                    let i = 0;
                    i < Object.values(groupBy(VentaTotalMes, "date")).length;
                    i++
                  ) {
                    Xvalores.push(
                      Object.values(groupBy(VentaTotalMes, "date"))[i].date[0]
                    );
                    Yvalores.push(
                      Object.values(groupBy(VentaTotalMes, "date"))[i].total
                    );
                  }
                  //DATOS DE LA GRAFICA VENTAS
                  var DatosVenta = {
                    type: "bar",
                    x: Xvalores,
                    y: Yvalores
                  };
                  var dataventa = [DatosVenta];

                  var LayoutVenta = {
                    font: { size: 15 }
                  };

                  try {
                    Plotly.newPlot("graficoVentas", dataventa, LayoutVenta, {
                      responsive: true
                    });
                  } catch (error) {}*/
                }

                /**
                 * Este Evento crea el reporte de venta por pescador
                 */
                $("#btnVentaPescador").click(consultavenxpescador);
                function consultavenxpescador() {
                  var selectedVentas = $("#SelectedPescadores").val();
                  let buscar = ResponseVentas.filter(
                    n =>
                      n.field_sale_date.und[0].value >= $("#dtpini").val() &&
                      n.field_sale_date.und[0].value <= $("#dtpfin").val()
                  );
                  var FiltroPescadores = ResponsePescadores.filter(function(
                    data
                  ) {
                    return data.field_document.und[0].value == selectedVentas;
                  });
                  if (selectedVentas != "Todos") {
                    var IDVentaU = [];
                    var NombreVentaU = [];
                    var FechaVentaU = [];
                    var TotalVentaU = [];
                    var VentaTotalMesU = [];
                    for (let item of buscar) {
                      if (item.field_total.und[0].value != "0") {
                        if (
                          FiltroPescadores[0].nid ==
                          item.field_id_fisherman_sima.und[0].target_id
                        ) {
                          IDVentaU.push(
                            FiltroPescadores[0].field_document.und[0].value
                          );
                          NombreVentaU.push(
                            FiltroPescadores[0].field_lastname.und[0].value +
                              " " +
                              FiltroPescadores[0].field_firstname.und[0].value
                          );
                          FechaVentaU.push(item.field_sale_date.und[0].value);
                          TotalVentaU.push(
                            parseInt(item.field_total.und[0].value, 10)
                          );
                          TotalMesU = {
                            date: [
                              new Date(
                                item.field_sale_date.und[0].value
                              ).getFullYear() +
                                "-" +
                                (1 +
                                  new Date(
                                    item.field_sale_date.und[0].value
                                  ).getMonth())
                            ],
                            total: [parseInt(item.field_total.und[0].value, 10)]
                          };
                          VentaTotalMesU.push(TotalMesU);
                        }
                      }
                    }

                    var groupBy = function(miarray, prop) {
                      return miarray.reduce(function(groups, item) {
                        var val = item[prop];
                        groups[val] = groups[val] || {
                          date: item.date,
                          total: 0
                        };
                        groups[val].total += item.total[0];
                        return groups;
                      }, {});
                    };
                    var Xvalores = [];
                    var Yvalores = [];
                    for (
                      let i = 0;
                      i < Object.values(groupBy(VentaTotalMesU, "date")).length;
                      i++
                    ) {
                      Xvalores.push(
                        Object.values(groupBy(VentaTotalMesU, "date"))[i]
                          .date[0]
                      );
                      Yvalores.push(
                        Object.values(groupBy(VentaTotalMesU, "date"))[i].total
                      );
                    }
                    //DATOS DE LA GRAFICA VENTA PESCADORES
                    var DatosPescadorVenta = {
                      type: "bar",
                      x: Xvalores,
                      y: Yvalores
                    };
                    var DataPescadorVenta = [DatosPescadorVenta];

                    var LayoutPescadorVenta = {
                      font: { size: 18 }
                    };
/*
                    try {
                      Plotly.newPlot(
                        "graficoVentas",
                        DataPescadorVenta,
                        LayoutPescadorVenta,
                        { responsive: true }
                      );
                    } catch (error) {}*/

                    //DATOS DE LA TABLA VENTA PESCADORES

                    var ValTablaVenta = [
                      IDVentaU,
                      NombreVentaU,
                      FechaVentaU,
                      TotalVentaU
                    ];

                    var datatableventa = [
                      {
                        type: "table",
                        header: {
                          values: [
                            ["<b>Cedula</b>"],
                            ["<b>Nombre</b>"],
                            ["<b>Ventas por mes</b>"],
                            ["<b>Total</b>"]
                          ],
                          align: "center",
                          line: { width: 1, color: "black" },
                          fill: { color: "grey" },
                          font: {
                            family: "Arial",
                            size: 12,
                            color: "white"
                          }
                        },
                        cells: {
                          values: ValTablaVenta,
                          align: "center",
                          line: { color: "black", width: 1 },
                          font: {
                            family: "Arial",
                            size: 11,
                            color: ["black"]
                          }
                        }
                      }
                    ]; //Fin de la tabla

                    var LayoutTablaVenta = {};

                    try {
                      Plotly.plot(
                        "TablaVenta",
                        datatableventa,
                        LayoutTablaVenta,
                        {
                          responsive: true
                        }
                      );
                    } catch (error) {}
                  } else {
                    consultaVenta();
                  }
                }

                /**
                 * Este Evento crea el reporte de gastos
                 */
                $("#btnGastos").click(consultaGastos);
                function consultaGastos() {
                  var GastosCombustiblep = [];
                  var GastosAceitep = [];
                  var GastosPersonalp = [];
                  var GastosHielop = [];
                  var GastosRentap = [];
                  var GastosOtrosp = [];
                  var FechaFaenaIni = [];
                  var Pescadorname = [];

                  let busca = ResponseFaenas.filter(
                    n =>
                      n.field_start_date.und[0].value >= $("#dtpini").val() &&
                      n.field_start_date.und[0].value <= $("#dtpfin").val()
                  );

                  for (const item of busca) {
                    FechaFaenaIni.push(item.field_start_date.und[0].value);

                    FiltroIDPescador = ResponsePescadores.filter(function(
                      data
                    ) {
                      return (
                        data.nid ===
                        item.field_id_fisherman_sima.und[0].target_id
                      );
                    });
                    if (
                      FiltroIDPescador[0].nid ===
                      item.field_id_fisherman_sima.und[0].target_id
                    ) {
                      Pescadorname.push(
                        FiltroIDPescador[0].field_firstname.und[0].value +
                          " " +
                          FiltroIDPescador[0].field_lastname.und[0].value
                      );
                      GastosCombustiblep.push(
                        parseInt(item.field_fuel_value.und[0].value, 10)
                      );
                      GastosAceitep.push(
                        parseInt(item.field_oil_value.und[0].value, 10)
                      );
                      GastosPersonalp.push(
                        parseInt(item.field_personal_value.und[0].value, 10)
                      );
                      GastosHielop.push(
                        parseInt(item.field_ice_value.und[0].value, 10)
                      );
                      GastosRentap.push(
                        parseInt(item.field_renting_value.und[0].value, 10)
                      );
                      GastosOtrosp.push(
                        parseInt(item.field_other_value.und[0].value, 10)
                      );
                    }
                  }
                  var values = [
                    Pescadorname,
                    FechaFaenaIni,
                    GastosCombustiblep,
                    GastosAceitep,
                    GastosPersonalp,
                    GastosHielop,
                    GastosRentap,
                    GastosOtrosp
                  ];
                  console.log(values);

                  var data = [
                    {
                      type: "table",
                      header: {
                        values: [
                          ["Pescador"],
                          ["Periodo"],
                          ["Combustible"],
                          ["Aceite"],
                          ["Apoyo Personal"],
                          ["Hielo"],
                          ["Renta"],
                          ["Otros"]
                        ],
                        align: "center",
                        line: { width: 1, color: "black" },
                        fill: { color: "grey" },
                        font: {
                          family: "Arial",
                          size: 12,
                          color: "white"
                        }
                      },
                      cells: {
                        values: values,
                        align: "center",
                        line: { color: "black", width: 1 },
                        font: {
                          family: "Arial",
                          size: 11,
                          color: ["black"]
                        }
                      }
                    }
                  ];

                  var totalhielo = 0;
                  var totalaceite = 0;
                  var totalcombustible = 0;
                  var totalpersonal = 0;
                  var totalrenta = 0;
                  var totalotro = 0;
                  for (let i of GastosCombustiblep) totalcombustible += i;
                  for (let j of GastosHielop) totalhielo += j;
                  for (let i of GastosAceitep) totalaceite += i;
                  for (let i of GastosPersonalp) totalpersonal += i;
                  for (let i of GastosRentap) totalrenta += i;
                  for (let i of GastosOtrosp) totalotro += i;

                  //DATOS DE LA GRAFICA GASTOS
                  var trace = {
                    x: [
                      totalcombustible,
                      totalhielo,
                      totalaceite,
                      totalpersonal,
                      totalrenta,
                      totalotro
                    ],
                    y: [
                      "Combustible",
                      "Hielo",
                      "Aceite",
                      "Personal",
                      "Renta",
                      "Otros"
                    ],
                    name: "holis",
                    type: "bar",
                    orientation: "h"
                  };
/*
                  try {
                    Plotly.newPlot("GraficoGastos", [trace]);
                  } catch (error) {}
*/
                  try {
                    Plotly.newPlot("TablaGastos", data);
                  } catch (error) {}
                }

                /**
                 * Este Evento crea el reporte de gastos del usuario
                 */
                $("#btnGastosUser").click(consultaGastosUser);
                function consultaGastosUser() {
                  if ($("#SelectedPescadores").val() != "Todos") {
                    let FiltroFecha = ResponseFaenas.filter(
                      n =>
                        n.field_start_date.und[0].value >= $("#dtpini").val() &&
                        n.field_start_date.und[0].value <= $("#dtpfin").val()
                    );

                    FiltroFisherman = ResponsePescadores.filter(function(data) {
                      return (
                        data.field_document.und[0].value ==
                        $("#SelectedPescadores").val()
                      );
                    });

                    var pers = [];
                    var GastosCombustiblep = [];
                    var GastosAceitep = [];
                    var GastosPersonalp = [];
                    var GastosHielop = [];
                    var GastosRentap = [];
                    var GastosOtrosp = [];
                    var FechaFaenaIni = [];
                    var Pescadorname = [];

                    for (const item of FiltroFecha) {
                      if (
                        item.field_id_fisherman_sima.und[0].target_id ==
                        FiltroFisherman[0].nid
                      ) {
                        FechaFaenaIni.push(item.field_start_date.und[0].value);

                        Pescadorname.push(
                          FiltroFisherman[0].field_firstname.und[0].value +
                            " " +
                            FiltroFisherman[0].field_lastname.und[0].value
                        );
                        GastosCombustiblep.push(
                          item.field_fuel_value.und[0].value
                        );
                        GastosAceitep.push(item.field_oil_value.und[0].value);
                        GastosPersonalp.push(
                          item.field_personal_value.und[0].value
                        );
                        GastosHielop.push(item.field_ice_value.und[0].value);
                        GastosRentap.push(
                          item.field_renting_value.und[0].value
                        );
                        GastosOtrosp.push(item.field_other_value.und[0].value);

                        //DATOS DE LA GRAFICA GASTOS
                        trace = {
                          x: [
                            item.field_fuel_value.und[0].value,
                            item.field_ice_value.und[0].value,
                            item.field_oil_value.und[0].value,
                            item.field_personal_value.und[0].value,
                            item.field_renting_value.und[0].value,
                            item.field_other_value.und[0].value
                          ],
                          y: [
                            "Combustible",
                            "Hielo",
                            "Aceite",
                            "Personal",
                            "Renta",
                            "Otros"
                          ],
                          name:
                            FiltroFisherman[0].field_firstname.und[0].value +
                            " " +
                            FiltroFisherman[0].field_lastname.und[0].value,
                          type: "bar",
                          orientation: "h"
                        };

                        pers.push(trace);

                        var datagastos = pers;

                        var layoutgraficagastos = {
                          barmode: "relative",
                          height: 400
                        };
/*
                        Plotly.newPlot(
                          "GraficoGastos",
                          datagastos,
                          layoutgraficagastos
                        );*/

                        var values = [
                          Pescadorname,
                          FechaFaenaIni,
                          GastosCombustiblep,
                          GastosAceitep,
                          GastosPersonalp,
                          GastosHielop,
                          GastosRentap,
                          GastosOtrosp
                        ];

                        var data = [
                          {
                            type: "table",
                            header: {
                              values: [
                                ["Pescador"],
                                ["Periodo"],
                                ["Combustible"],
                                ["Aceite"],
                                ["Apoyo Personal"],
                                ["Hielo"],
                                ["Renta"],
                                ["Otros"]
                              ],
                              align: "center",
                              line: { width: 1, color: "black" },
                              fill: { color: "grey" },
                              font: {
                                family: "Arial",
                                size: 12,
                                color: "white"
                              }
                            },
                            cells: {
                              values: values,
                              align: "center",
                              line: { color: "black", width: 1 },
                              font: {
                                family: "Arial",
                                size: 11,
                                color: ["black"]
                              }
                            }
                          }
                        ];

                        Plotly.newPlot("TablaGastos", data);
                      }
                    }
                  } else {
                    consultaGastos();
                  }
                  $("#dtpini").val("");
                  $("#dtpfin").val("");
                }

                /**
                 * Este Evento crea el reporte de Actividad Pesquera por municipio
                 */

                $("#btnEsfuerzoPesca").click(consultaEsfuerzo);
                function consultaEsfuerzo() {
                  let FiltroFecha = ResponseFaenas.filter(
                    v =>
                      v.field_start_date.und[0].value >= $("#dtpini").val() &&
                      v.field_start_date.und[0].value <= $("#dtpfin").val()
                  );
                  var Esfuerzo = [];
                  var NombreFaena = [];
                  var Libras = [];
                  var Tiempo = [];
                  var FaenaNum = [];
                  var NombrePescador = [];
                  var FechaFaena = [];
                  for (const item of FiltroFecha) {
                    FiltroFaenaEspecie = ResponseFaenasEspecies.filter(function(
                      dataId
                    ) {
                      return (
                        dataId.field_id_labor_sima.und[0].target_id == item.nid
                      );
                    });

                    for (const iteme of FiltroFaenaEspecie) {
                      FiltroFisherman = ResponsePescadores.filter(function(
                        data
                      ) {
                        return (
                          data.nid ==
                          item.field_id_fisherman_sima.und[0].target_id
                        );
                      });
                      for (const pes of FiltroFisherman) {
                        NombrePescador.push(
                          pes.field_firstname.und[0].value +
                            " " +
                            pes.field_lastname.und[0].value
                        );
                      }

                      Esfuerzo.push(
                        parseInt(
                          iteme.field_weight.und[0].value /
                            moment
                              .duration(item.field_abor_time.und[0].value)
                              .asHours(),
                          10
                        )
                      );
                      NombreFaena.push(item.field_name.und[0].value);
                      Libras.push(iteme.field_weight.und[0].value);
                      Tiempo.push(item.field_abor_time.und[0].value);
                      FechaFaena.push(item.field_start_date.und[0].value);
                    }
                  }
                  for (let i = 0; i < NombreFaena.length; i++) {
                    const t = 1 + i;
                    FaenaNum.push("Faena #" + t);
                  }

                  var DatosEsfuerzo = [
                    {
                      x: FaenaNum,
                      y: Esfuerzo,
                      type: "bar"
                    }
                  ];
                  try {
                    Plotly.newPlot("GraficaEsfuerzo", DatosEsfuerzo, {
                      responsive: true
                    });
                  } catch (error) {}

                  //DATOS DE LA TABLA VENTA
                  var ValTablaEsfuerzo = [
                    FaenaNum,
                    FechaFaena,
                    NombrePescador,
                    Libras,
                    Tiempo,
                    Esfuerzo
                  ];

                  var datatableesfuerzo = [
                    {
                      type: "table",
                      columnorder: [1, 2, 3, 4, 5, 6],
                      columnwidth: [150, 350, 220, 400, 200, 300],
                      header: {
                        values: [
                          ["<b>Faena #</b>"],
                          ["<b>Periodo</b>"],
                          ["<b>Pescador</b>"],
                          ["<b>Peso (Lb)</b>"],
                          ["<b>Tiempo (horas)</b>"],
                          ["<b>Unidades de esfuerzo de pesca</b>"]
                        ],
                        align: "center",
                        line: { width: 1, color: "black" },
                        fill: { color: "grey" },
                        font: {
                          family: "Arial",
                          size: 12,
                          color: "white"
                        }
                      },
                      cells: {
                        values: ValTablaEsfuerzo,
                        align: "center",
                        line: { color: "black", width: 1 },
                        font: {
                          family: "Arial",
                          size: 11,
                          color: ["black"]
                        }
                      }
                    }
                  ]; //Fin de la tabla

                  var LayoutTablaEsfuerzo = {
                    height: 350
                  };
                  /*
                  try {
                    Plotly.plot(
                      "TablaEsfuerzo",
                      datatableesfuerzo,
                      LayoutTablaEsfuerzo,
                      {
                        responsive: true
                      }
                    );
                  } catch (error) {}*/

                  $("#dtpini").val("");
                  $("#dtpfin").val("");
                }

                /**
                 * Este Evento crea el reporte de Actividad Pesquera por municipio
                 */

                $("#btnEsfuerzoPescaUser").click(consultaEsfuerzoUser);
                function consultaEsfuerzoUser() {
                  if ($("#SelectedPescadores").val() != "Todos") {
                    let FiltroFecha = ResponseFaenas.filter(
                      v =>
                        v.field_start_date.und[0].value >= $("#dtpini").val() &&
                        v.field_start_date.und[0].value <= $("#dtpfin").val()
                    );

                    var Esfuerzo = [];
                    var NombreFaena = [];
                    var Libras = [];
                    var Tiempo = [];
                    var FaenaNum = [];
                    var FechaFaena = [];
                    for (const item of FiltroFecha) {
                      var FiltroPescadores = ResponsePescadores.filter(function(
                        data
                      ) {
                        return (
                          data.nid ==
                          item.field_id_fisherman_sima.und[0].target_id
                        );
                      });

                      FiltroFaenaEspecie = ResponseFaenasEspecies.filter(
                        function(dataId) {
                          return (
                            dataId.field_id_labor_sima.und[0].target_id ==
                            item.nid
                          );
                        }
                      );
                      if (
                        FiltroPescadores[0].nid ==
                        item.field_id_fisherman_sima.und[0].target_id
                      ) {
                        for (const [i, iteme] of FiltroFaenaEspecie.entries()) {
                          Esfuerzo.push(
                            parseInt(
                              iteme.field_weight.und[0].value /
                                moment
                                  .duration(item.field_abor_time.und[0].value)
                                  .asHours()
                            )
                          );
                          var t = 1 + i;
                          NombreFaena.push(item.field_name.und[0].value);
                          Libras.push(iteme.field_weight.und[0].value);
                          Tiempo.push(item.field_abor_time.und[0].value);
                          FaenaNum.push("Faena #" + t);
                          FechaFaena.push(item.field_start_date.und[0].value);
                        }
                      }

                      var DatosEsfuerzo = [
                        {
                          x: FaenaNum,
                          y: Esfuerzo,
                          type: "bar"
                        }
                      ];
                      /*
                      try {
                        Plotly.newPlot("GraficaEsfuerzo", DatosEsfuerzo, {
                          responsive: true
                        });
                      } catch (error) {}*/

                      //DATOS DE LA TABLA VENTA
                      var ValTablaEsfuerzo = [
                        FaenaNum,
                        FechaFaena,
                        Libras,
                        Tiempo,
                        Esfuerzo
                      ];

                      var datatableesfuerzo = [
                        {
                          type: "table",
                          columnorder: [1, 2, 3, 4, 5],
                          columnwidth: [220, 400, 200, 300, 400],
                          header: {
                            values: [
                              ["<b>Faena #</b>"],
                              ["<b>Periodo</b>"],
                              ["<b>Peso (Lb)</b>"],
                              ["<b>Tiempo (horas)</b>"],
                              ["<b>Unidades de esfuerzo de pesca</b>"]
                            ],
                            align: "center",
                            line: { width: 1, color: "black" },
                            fill: { color: "grey" },
                            font: {
                              family: "Arial",
                              size: 12,
                              color: "white"
                            }
                          },
                          cells: {
                            values: ValTablaEsfuerzo,
                            align: "center",
                            line: { color: "black", width: 1 },
                            font: {
                              family: "Arial",
                              size: 11,
                              color: ["black"]
                            }
                          }
                        }
                      ]; //Fin de la tabla

                      var LayoutTablaEsfuerzo = {};

                      try {
                        Plotly.plot(
                          "TablaEsfuerzo",
                          datatableesfuerzo,
                          LayoutTablaEsfuerzo,
                          {
                            responsive: true
                          }
                        );
                      } catch (error) {}
                    }
                  } else {
                    consultaEsfuerzo();
                  }
                }

                /**
                 * Este Evento crea el reporte de Especies
                 */

                $("#btnEspecie").click(consultaEspecie);
                function consultaEspecie() {
                  let FiltroFecha = ResponseFaenasEspecies.filter(
                    v =>
                      v.field_date_labor.und[0].value >=
                        $("#dtpini").val() + " " + "00:00:00" &&
                      v.field_date_labor.und[0].value <=
                        $("#dtpfin").val() + " " + "23:59:59"
                  );

                  var IDFaenaES = [];
                  for (const itemfe of FiltroFecha) {
                    FaenaEspe = {
                      PesosEspecies: [
                        parseInt(itemfe.field_weight.und[0].value, 10)
                      ],
                      IDEspecieF: [
                        itemfe.field_id_specie_sima.und[0].target_id
                      ],
                      FechaFaenaINi: [itemfe.field_date_labor.und[0].value]
                    };
                    IDFaenaES.push(FaenaEspe);
                  }

                  var NombreIDEspecie = [];
                  for (const item of IDFaenaES) {
                    FiltroFaenaEspecie = ResponseEspecies.filter(function(
                      dataId
                    ) {
                      return dataId.nid == item.IDEspecieF;
                    });
                    NombreIDEspecie.push(FiltroFaenaEspecie);
                  }

                  var Nombre = [];
                  var Peso = [];
                  var Fecha = [];
                  for (let i = 0; i < NombreIDEspecie.length; i++) {
                    Nombre.push(NombreIDEspecie[i][0].field_name.und[0].value);
                    Peso.push(IDFaenaES[i].PesosEspecies[0]);
                    Fecha.push(IDFaenaES[i].FechaFaenaINi[0]);
                  }

                  //DATOS DE LA GRAFICA Especies (pie)
                  var DatosEspecies = [
                    {
                      values: Peso,
                      labels: Nombre,
                      type: "pie"
                    }
                  ];

                  var layout = {
                    font: { size: 18 }
                  };
                  try {
                    Plotly.newPlot("GraficoEspeciesPie", DatosEspecies, layout);
                  } catch (error) {}

                  //DATOS DE LA TABLA VENTA
                  var ValTablaEspecies = [Nombre, Peso, Fecha];

                  var datatableespecies = [
                    {
                      type: "table",
                      columnorder: [1, 2, 3],
                      columnwidth: [220, 400, 200],
                      header: {
                        values: [
                          ["<b>Nombre #</b>"],
                          ["<b>Peso (Lb)</b>"],
                          ["<b>Fecha</b>"]
                        ],
                        align: "center",
                        line: { width: 1, color: "black" },
                        fill: { color: "#104659" },
                        font: {
                          family: "Arial",
                          size: 12,
                          color: "white"
                        }
                      },
                      cells: {
                        values: ValTablaEspecies,
                        align: "center",
                        line: { color: "black", width: 1 },
                        font: {
                          family: "Arial",
                          size: 11,
                          color: ["black"]
                        }
                      }
                    }
                  ]; //Fin de la tabla

                  var LayoutTablaEsfuerzo = {};

                  try {
                    Plotly.plot(
                      "TablaEspecies",
                      datatableespecies,
                      LayoutTablaEsfuerzo,
                      {
                        responsive: true
                      }
                    );
                  } catch (error) {}

                  /**
                   * DATOS DE LA GRAFICA Especies (barras)
                  var DatosEspeciesbar = {
                    type: "bar",
                    x: [FiltroP[0].field_name.und[0].value],
                    y: PesosEspecies
                  };

                  console.log(DatosEspeciesbar);
                  var DataEspeciesBar = [DatosEspeciesbar];

                  var LayoutEspeciesBar = {
                    font: { size: 18 }
                  };

                  try {
                    Plotly.newPlot(
                      "GraficoEspeciesBar",
                      DataEspeciesBar,
                      LayoutEspeciesBar,
                      { showSendToCloud: true }
                    );
                  } catch (error) {}



                  $("#dtpini").val("");
                  $("#dtpfin").val("");*/
                }
              });
            });
          });
        });
      });
    });
  });

  /**
   * Desde aca debo realizar los nuevos reportes
   */
});

/**
                 * Este Evento crea el reporte de Ventas vs Gastos


                $("#btnVenvsGastos").click(consultaVentavsGastos);
                function consultaVentavsGastos() {
                  let FiltroFechaG = ResponseFaenas.filter(
                    g =>
                      g.field_start_date.und[0].value >= $("#dtpini").val() &&
                      g.field_start_date.und[0].value <= $("#dtpfin").val()
                  );

                  let FiltroFechaV = ResponseVentas.filter(
                    v =>
                      v.field_sale_date.und[0].value >= $("#dtpini").val() &&
                      v.field_sale_date.und[0].value <= $("#dtpfin").val()
                  );

                  var GastosTotalMes = [];
                  for (let itemG of FiltroFechaG) {
                    TotalMes = {
                      dategrafica: [
                        new Date(
                          itemG.field_start_date.und[0].value
                        ).getFullYear() +
                          "-" +
                          (1 +
                            new Date(
                              itemG.field_start_date.und[0].value
                            ).getMonth())
                      ],
                      date: [
                        new Date(
                          itemG.field_start_date.und[0].value
                        ).getFullYear() +
                          "-" +
                          (1 +
                            new Date(
                              itemG.field_start_date.und[0].value
                            ).getMonth()) +
                          "-" +
                          new Date(
                            itemG.field_start_date.und[0].value
                          ).getDate()
                      ],
                      total: [
                        parseInt(itemG.field_fuel_value.und[0].value, 10) +
                          parseInt(itemG.field_oil_value.und[0].value, 10) +
                          parseInt(
                            itemG.field_personal_value.und[0].value,
                            10
                          ) +
                          parseInt(itemG.field_ice_value.und[0].value, 10) +
                          parseInt(itemG.field_renting_value.und[0].value, 10) +
                          parseInt(itemG.field_other_value.und[0].value, 10)
                      ]
                    };
                    GastosTotalMes.push(TotalMes);
                  }

                  var VentasTotalMes = [];
                  for (const itemv of FiltroFechaV) {
                    if (itemv.field_total.und[0].value != "0") {
                      TotalMes = {
                        dategrafica: [
                          new Date(
                            itemv.field_sale_date.und[0].value
                          ).getFullYear() +
                            "-" +
                            (1 +
                              new Date(
                                itemv.field_sale_date.und[0].value
                              ).getMonth())
                        ],
                        date: [
                          new Date(
                            itemv.field_sale_date.und[0].value
                          ).getFullYear() +
                            "-" +
                            (1 +
                              new Date(
                                itemv.field_sale_date.und[0].value
                              ).getMonth()) +
                            "-" +
                            new Date(
                              itemv.field_sale_date.und[0].value
                            ).getDate()
                        ],
                        total: [parseInt(itemv.field_total.und[0].value, 10)]
                      };
                      VentasTotalMes.push(TotalMes);
                    }
                  }
                  var groupBy = function(miarray, prop) {
                    return miarray.reduce(function(groups, item) {
                      var val = item[prop];
                      groups[val] = groups[val] || {
                        dategrafica: item.dategrafica,
                        date: item.date,
                        total: 0
                      };
                      groups[val].total += item.total[0];
                      return groups;
                    }, {});
                  };

                  var PVentas = [];
                  var XVentas = [];
                  var YVentas = [];
                  for (
                    let i = 0;
                    i <
                    Object.values(groupBy(VentasTotalMes, "dategrafica"))
                      .length;
                    i++
                  ) {
                    PVentas.push(
                      Object.values(groupBy(VentasTotalMes, "date"))[i].date[0]
                    );
                    XVentas.push(
                      Object.values(groupBy(VentasTotalMes, "dategrafica"))[i]
                        .dategrafica[0]
                    );
                    YVentas.push(
                      Object.values(groupBy(VentasTotalMes, "date"))[i].total
                    );
                  }
                  //DATOS DE LA GRAFICA VENTA VS GASTOS
                  var DatoVentas = {
                    x: XVentas,
                    y: YVentas,
                    name: "Ventas",
                    type: "bar"
                  };

                  var PGastos = [];
                  var XGastos = [];
                  var YGastos = [];
                  for (
                    let i = 0;
                    i <
                    Object.values(groupBy(GastosTotalMes, "dategrafica"))
                      .length;
                    i++
                  ) {
                    PGastos.push(
                      Object.values(groupBy(GastosTotalMes, "date"))[i].date[0]
                    );
                    XGastos.push(
                      Object.values(groupBy(GastosTotalMes, "dategrafica"))[i]
                        .dategrafica[0]
                    );
                    YGastos.push(
                      Object.values(groupBy(GastosTotalMes, "date"))[i].total
                    );
                  }
                  var DatoGastos = {
                    x: XGastos,
                    y: YGastos,
                    name: "Gastos",
                    type: "bar"
                  };

                  var DataVentaVsGastos = [DatoVentas, DatoGastos];

                  var LayoutVentaVsGastos = {
                    font: { size: 18 },
                    barmode: "group"
                  };

                  try {
                    Plotly.newPlot(
                      "GraficoVentasVsGastos",
                      DataVentaVsGastos,
                      LayoutVentaVsGastos,
                      { responsive: true }
                    );
                  } catch (error) {}

                  //DATOS DE LA TABLA VENTAS VS GASTOS
                  var values = [PVentas, YVentas, PGastos, YGastos];

                  var data = [
                    {
                      type: "table",
                      header: {
                        values: [
                          ["Periodo ventas"],
                          ["Ventas"],
                          ["Periodo gastos"],
                          ["Gastos"]
                        ],
                        align: "center",
                        line: { width: 1, color: "black" },
                        fill: { color: "grey" },
                        font: {
                          family: "Arial",
                          size: 12,
                          color: "white"
                        }
                      },
                      cells: {
                        values: values,
                        align: "center",
                        line: { color: "black", width: 1 },
                        font: {
                          family: "Arial",
                          size: 11,
                          color: ["black"]
                        }
                      }
                    }
                  ];

                  Plotly.newPlot("TablaVentasVsGastos", data);

                  $("#dtpini").val("");
                  $("#dtpfin").val("");
                }

                /**
                 * Este Evento crea el reporte de Ventas vs Gastos del usuario


                $("#btnVenvsGastosUser").click(consultaVentavsGastosUser);
                function consultaVentavsGastosUser() {
                  if ($("#SelectedPescadores").val() != "Todos") {
                    FiltroFisherman = ResponsePescadores.filter(function(data) {
                      return (
                        data.field_document.und[0].value ==
                        $("#SelectedPescadores").val()
                      );
                    });

                    let FiltroFechaV = ResponseVentas.filter(
                      v =>
                        v.field_sale_date.und[0].value >= $("#dtpini").val() &&
                        v.field_sale_date.und[0].value <= $("#dtpfin").val()
                    );

                    let FiltroFechaG = ResponseFaenas.filter(
                      g =>
                        g.field_start_date.und[0].value >= $("#dtpini").val() &&
                        g.field_start_date.und[0].value <= $("#dtpfin").val()
                    );
                    var GastosTotalMes = [];
                    for (let itemG of FiltroFechaG) {
                      if (
                        itemG.field_id_fisherman_sima.und[0].target_id ==
                        FiltroFisherman[0].nid
                      ) {
                        TotalMes = {
                          dategrafica: [
                            new Date(
                              itemG.field_start_date.und[0].value
                            ).getFullYear() +
                              "-" +
                              (1 +
                                new Date(
                                  itemG.field_start_date.und[0].value
                                ).getMonth())
                          ],
                          date: [
                            new Date(
                              itemG.field_start_date.und[0].value
                            ).getFullYear() +
                              "-" +
                              (1 +
                                new Date(
                                  itemG.field_start_date.und[0].value
                                ).getMonth()) +
                              "-" +
                              new Date(
                                itemG.field_start_date.und[0].value
                              ).getDate()
                          ],
                          total: [
                            parseInt(itemG.field_fuel_value.und[0].value, 10) +
                              parseInt(itemG.field_oil_value.und[0].value, 10) +
                              parseInt(
                                itemG.field_personal_value.und[0].value,
                                10
                              ) +
                              parseInt(itemG.field_ice_value.und[0].value, 10) +
                              parseInt(
                                itemG.field_renting_value.und[0].value,
                                10
                              ) +
                              parseInt(itemG.field_other_value.und[0].value, 10)
                          ]
                        };
                        GastosTotalMes.push(TotalMes);
                      }
                    }
                    var VentasTotalMes = [];
                    for (const itemv of FiltroFechaV) {
                      if (itemv.field_total.und[0].value != "0") {
                        if (
                          itemv.field_id_fisherman_sima.und[0].target_id ==
                          FiltroFisherman[0].nid
                        ) {
                          TotalMes = {
                            dategrafica: [
                              new Date(
                                itemv.field_sale_date.und[0].value
                              ).getFullYear() +
                                "-" +
                                (1 +
                                  new Date(
                                    itemv.field_sale_date.und[0].value
                                  ).getMonth())
                            ],
                            date: [
                              new Date(
                                itemv.field_sale_date.und[0].value
                              ).getFullYear() +
                                "-" +
                                (1 +
                                  new Date(
                                    itemv.field_sale_date.und[0].value
                                  ).getMonth()) +
                                "-" +
                                new Date(
                                  itemv.field_sale_date.und[0].value
                                ).getDate()
                            ],
                            total: [
                              parseInt(itemv.field_total.und[0].value, 10)
                            ]
                          };
                          VentasTotalMes.push(TotalMes);
                        }
                      }
                    }
                    var groupBy = function(miarray, prop) {
                      return miarray.reduce(function(groups, item) {
                        var val = item[prop];
                        groups[val] = groups[val] || {
                          dategrafica: item.dategrafica,
                          date: item.date,
                          total: 0
                        };
                        groups[val].total += item.total[0];
                        return groups;
                      }, {});
                    };

                    var PVentas = [];
                    var XVentas = [];
                    var YVentas = [];
                    for (
                      let i = 0;
                      i <
                      Object.values(groupBy(VentasTotalMes, "dategrafica"))
                        .length;
                      i++
                    ) {
                      PVentas.push(
                        Object.values(groupBy(VentasTotalMes, "date"))[i]
                          .date[0]
                      );
                      XVentas.push(
                        Object.values(groupBy(VentasTotalMes, "dategrafica"))[i]
                          .dategrafica[0]
                      );
                      YVentas.push(
                        Object.values(groupBy(VentasTotalMes, "date"))[i].total
                      );
                    }
                    //DATOS DE LA GRAFICA VENTA VS GASTOS
                    var DatoVentas = {
                      x: XVentas,
                      y: YVentas,
                      name: "Ventas",
                      type: "bar"
                    };

                    var PGastos = [];
                    var XGastos = [];
                    var YGastos = [];
                    for (
                      let i = 0;
                      i <
                      Object.values(groupBy(GastosTotalMes, "dategrafica"))
                        .length;
                      i++
                    ) {
                      PGastos.push(
                        Object.values(groupBy(GastosTotalMes, "date"))[i]
                          .date[0]
                      );
                      XGastos.push(
                        Object.values(groupBy(GastosTotalMes, "dategrafica"))[i]
                          .dategrafica[0]
                      );
                      YGastos.push(
                        Object.values(groupBy(GastosTotalMes, "date"))[i].total
                      );
                    }
                    var DatoGastos = {
                      x: XGastos,
                      y: YGastos,
                      name: "Gastos",
                      type: "bar"
                    };

                    var DataVentaVsGastos = [DatoVentas, DatoGastos];

                    var LayoutVentaVsGastos = {
                      font: { size: 18 },
                      barmode: "group"
                    };

                    try {
                      Plotly.newPlot(
                        "GraficoVentasVsGastos",
                        DataVentaVsGastos,
                        LayoutVentaVsGastos,
                        { responsive: true }
                      );
                    } catch (error) {}

                    //DATOS DE LA TABLA VENTAS VS GASTOS
                    var values = [PVentas, YVentas, PGastos, YGastos];

                    var data = [
                      {
                        type: "table",
                        header: {
                          values: [
                            ["Periodo ventas"],
                            ["Ventas"],
                            ["Periodo gastos"],
                            ["Gastos"]
                          ],
                          align: "center",
                          line: { width: 1, color: "black" },
                          fill: { color: "grey" },
                          font: {
                            family: "Arial",
                            size: 12,
                            color: "white"
                          }
                        },
                        cells: {
                          values: values,
                          align: "center",
                          line: { color: "black", width: 1 },
                          font: {
                            family: "Arial",
                            size: 11,
                            color: ["black"]
                          }
                        }
                      }
                    ];

                    Plotly.newPlot("TablaVentasVsGastos", data);
                  } else {
                    consultaVentavsGastos();
                    $("#dtpini").val("");
                    $("#dtpfin").val("");
                  }

                  $("#dtpini").val("");
                  $("#dtpfin").val("");
                }
*/
/**
                 * Este Evento crea el reporte de Actividad Pesquera por municipio


                $("#btnConvsVen").click(consultaConvsVen);
                function consultaConvsVen() {
                  let FiltroFecha = ResponseVentas.filter(
                    v =>
                      v.field_sale_date.und[0].value >= $("#dtpini").val() &&
                      v.field_sale_date.und[0].value <= $("#dtpfin").val()
                  );

                  var ConsumoTotalMes = [];
                  var VentasTotalMes = [];

                  for (const itemv of FiltroFecha) {
                    if (itemv.field_total.und[0].value != "0") {
                      TotalMes = {
                        date: [
                          new Date(
                            itemv.field_sale_date.und[0].value
                          ).getFullYear() +
                            "-" +
                            (1 +
                              new Date(
                                itemv.field_sale_date.und[0].value
                              ).getMonth())
                        ],
                        total: [
                          parseInt(itemv.field_sale_total_kg_.und[0].value, 10)
                        ]
                      };
                      VentasTotalMes.push(TotalMes);
                    } else {
                      TotalMesConsumo = {
                        date: [
                          new Date(
                            itemv.field_sale_date.und[0].value
                          ).getFullYear() +
                            "-" +
                            (1 +
                              new Date(
                                itemv.field_sale_date.und[0].value
                              ).getMonth())
                        ],
                        total: [
                          parseInt(itemv.field_sale_total_kg_.und[0].value, 10)
                        ]
                      };
                      ConsumoTotalMes.push(TotalMesConsumo);
                    }
                  }

                  var groupBy = function(miarray, prop) {
                    return miarray.reduce(function(groups, item) {
                      var val = item[prop];
                      groups[val] = groups[val] || {
                        date: item.date,
                        total: 0
                      };
                      groups[val].total += item.total[0];
                      return groups;
                    }, {});
                  };

                  var XVentas = [];
                  var YVentas = [];
                  for (
                    let i = 0;
                    i < Object.values(groupBy(VentasTotalMes, "date")).length;
                    i++
                  ) {
                    XVentas.push(
                      Object.values(groupBy(VentasTotalMes, "date"))[i].date[0]
                    );
                    YVentas.push(
                      Object.values(groupBy(VentasTotalMes, "date"))[i].total
                    );
                  }
                  //DATOS DE LA GRAFICA VENTA VS CONSUMO
                  var DatoVentas = {
                    x: XVentas,
                    y: YVentas,
                    name: "Ventas",
                    type: "bar"
                  };

                  var XConsumo = [];
                  var YConsumo = [];
                  for (
                    let i = 0;
                    i < Object.values(groupBy(ConsumoTotalMes, "date")).length;
                    i++
                  ) {
                    XConsumo.push(
                      Object.values(groupBy(ConsumoTotalMes, "date"))[i].date[0]
                    );
                    YConsumo.push(
                      Object.values(groupBy(ConsumoTotalMes, "date"))[i].total
                    );
                  }
                  var DatoConsumo = {
                    x: XConsumo,
                    y: YConsumo,
                    name: "Consumo",
                    type: "bar"
                  };

                  var DataVentaVsConsumo = [DatoVentas, DatoConsumo];

                  var LayoutVentaVsConsumo = {
                    font: { size: 18 },
                    barmode: "group"
                  };

                  try {
                    Plotly.newPlot(
                      "GraficoVentasVsConsumo",
                      DataVentaVsConsumo,
                      LayoutVentaVsConsumo,
                      { responsive: true }
                    );
                  } catch (error) {}

                  //DATOS DE LA TABLA VENTAS VS CONSUMO
                  var values = [XVentas, YVentas, XConsumo, YConsumo];

                  var data = [
                    {
                      type: "table",
                      header: {
                        values: [
                          ["Periodo ventas"],
                          ["Ventas"],
                          ["Periodo consumo"],
                          ["Consumo"]
                        ],
                        align: "center",
                        line: { width: 1, color: "black" },
                        fill: { color: "grey" },
                        font: {
                          family: "Arial",
                          size: 12,
                          color: "white"
                        }
                      },
                      cells: {
                        values: values,
                        align: "center",
                        line: { color: "black", width: 1 },
                        font: {
                          family: "Arial",
                          size: 11,
                          color: ["black"]
                        }
                      }
                    }
                  ];

                  Plotly.newPlot("TablaVentasVsConsumos", data);
                }
*/
/**
                 * Este Evento crea el reporte de Actividad Pesquera por municipio


                $("#btnConvsVenUser").click(consultaConvsVenUser);
                function consultaConvsVenUser() {
                  if ($("#SelectedPescadores").val() == "Todos") {
                    consultaConvsVen();
                  } else {
                    let FiltroFecha = ResponseVentas.filter(
                      v =>
                        v.field_sale_date.und[0].value >= $("#dtpini").val() &&
                        v.field_sale_date.und[0].value <= $("#dtpfin").val()
                    );

                    FiltroFisherman = ResponsePescadores.filter(function(data) {
                      return (
                        data.field_document.und[0].value ==
                        $("#SelectedPescadores").val()
                      );
                    });

                    var ConsumoTotalMes = [];
                    var VentasTotalMes = [];
                    for (const itemv of FiltroFecha) {
                      if (
                        FiltroFisherman[0].nid ==
                        itemv.field_id_fisherman_sima.und[0].target_id
                      ) {
                        if (itemv.field_total.und[0].value != "0") {
                          TotalMes = {
                            date: [
                              new Date(
                                itemv.field_sale_date.und[0].value
                              ).getFullYear() +
                                "-" +
                                (1 +
                                  new Date(
                                    itemv.field_sale_date.und[0].value
                                  ).getMonth())
                            ],
                            total: [
                              parseInt(
                                itemv.field_sale_total_kg_.und[0].value,
                                10
                              )
                            ]
                          };
                          VentasTotalMes.push(TotalMes);
                        } else {
                          TotalMesConsumo = {
                            date: [
                              new Date(
                                itemv.field_sale_date.und[0].value
                              ).getFullYear() +
                                "-" +
                                (1 +
                                  new Date(
                                    itemv.field_sale_date.und[0].value
                                  ).getMonth())
                            ],
                            total: [
                              parseInt(
                                itemv.field_sale_total_kg_.und[0].value,
                                10
                              )
                            ]
                          };
                          ConsumoTotalMes.push(TotalMesConsumo);
                        }
                      }
                    }

                    var groupBy = function(miarray, prop) {
                      return miarray.reduce(function(groups, item) {
                        var val = item[prop];
                        groups[val] = groups[val] || {
                          date: item.date,
                          total: 0
                        };
                        groups[val].total += item.total[0];
                        return groups;
                      }, {});
                    };

                    var XVentas = [];
                    var YVentas = [];
                    for (
                      let i = 0;
                      i < Object.values(groupBy(VentasTotalMes, "date")).length;
                      i++
                    ) {
                      XVentas.push(
                        Object.values(groupBy(VentasTotalMes, "date"))[i]
                          .date[0]
                      );
                      YVentas.push(
                        Object.values(groupBy(VentasTotalMes, "date"))[i].total
                      );
                    }
                    //DATOS DE LA GRAFICA VENTA VS CONSUMO
                    var DatoVentas = {
                      x: XVentas,
                      y: YVentas,
                      name: "Ventas",
                      type: "bar"
                    };

                    var XConsumo = [];
                    var YConsumo = [];
                    for (
                      let i = 0;
                      i <
                      Object.values(groupBy(ConsumoTotalMes, "date")).length;
                      i++
                    ) {
                      XConsumo.push(
                        Object.values(groupBy(ConsumoTotalMes, "date"))[i]
                          .date[0]
                      );
                      YConsumo.push(
                        Object.values(groupBy(ConsumoTotalMes, "date"))[i].total
                      );
                    }
                    var DatoConsumo = {
                      x: XConsumo,
                      y: YConsumo,
                      name: "Consumo",
                      type: "bar"
                    };

                    var DataVentaVsConsumo = [DatoVentas, DatoConsumo];

                    var LayoutVentaVsConsumo = {
                      font: { size: 18 },
                      barmode: "group"
                    };

                    try {
                      Plotly.newPlot(
                        "GraficoVentasVsConsumo",
                        DataVentaVsConsumo,
                        LayoutVentaVsConsumo,
                        { responsive: true }
                      );
                    } catch (error) {}

                    //DATOS DE LA TABLA VENTAS VS CONSUMO
                    var values = [XVentas, YVentas, XConsumo, YConsumo];

                    var data = [
                      {
                        type: "table",
                        header: {
                          values: [
                            ["Periodo ventas"],
                            ["Ventas"],
                            ["Periodo consumo"],
                            ["Consumo"]
                          ],
                          align: "center",
                          line: { width: 1, color: "black" },
                          fill: { color: "grey" },
                          font: {
                            family: "Arial",
                            size: 12,
                            color: "white"
                          }
                        },
                        cells: {
                          values: values,
                          align: "center",
                          line: { color: "black", width: 1 },
                          font: {
                            family: "Arial",
                            size: 11,
                            color: ["black"]
                          }
                        }
                      }
                    ];

                    Plotly.newPlot("TablaVentasVsConsumos", data);
                  }
                }
 */
