jQuery(document).ready(function($) {
    //jQuery time
    var selectPrecipitation = JSON.parse(jsonPrecipitacion);
    var selectEvapotranspiracion = JSON.parse(jsonEvapotranspiracion);
    var selectTemperature = JSON.parse(jsonTemperatura);
    var selectstreamflow = JSON.parse(jsonCaudal);
    var selectSediments = JSON.parse(jsonSedimento);
    var selectNarrative = JSON.parse(jsonNarrativa);
    var selectCatalog = JSON.parse(jsonCatalogo);
    var selectProject = JSON.parse(jsonProject);
    var selectHuella = JSON.parse(jsonCapasHuella);
    var projecNarrative = new Array();
    var project = new Array();
    var targertIdproject = new Array();
    var targertIdproject = [];
    var targertIdprojectnume = 0;
    var token;
    var project = [];
    var paso = 1;
    var step = 1;
    var val;
    var numProject = 0;
    var numFootprint = 0;
    var arrayProject = 0;
    var dataJson = {};
    var name;
    var geoJson;
    var previousStep;
    var current_fs, next_fs, previous_fs; //fieldsets
    var left, opacity, scale; //fieldset properties which we will animate
    var myElement = document.getElementById("progressbar");
    var pasostoTales = 13;
    var porcentaje = (step / pasostoTales) * 100;
    var baseUrl = window.location.protocol + "//" + window.location.hostname;
    var dkanUrl = "/api/3/action/package_show?id=";
    var fullUrlDataset = baseUrl + dkanUrl + datasetUuid;
    var datasetRequest = $.ajax({
        url: fullUrlDataset,
        type: 'GET',
    });
    location.href = '#inicio';

    function map() {
        datasetRequest.then(function(response) {
            var geoJsonUrl = response.result[0].resources[0].url;
            var geoJsonRequest = $.ajax({
                url: geoJsonUrl,
                type: 'GET',
            });
            geoJsonRequest.then(function(response) {
                //console.log("Respuesta GeoJSON::" + response);
                geoJson = response;
                callMap(geoJson);
            });

        });

        var geJsonPointUrl = "/api/3/action/package_show?id=75351835-434e-4b39-b7f9-e752ee32ba5e";
        var fullUrlDataset = baseUrl + geJsonPointUrl;
        var datasetPointRequest = $.ajax({
            url: fullUrlDataset,
            type: 'GET',
        });
        datasetPointRequest.then(function(response) {
            geoJsonPointUrl = response.result[0].resources[0].url;
            var geoJsonPointUrlRequest = $.ajax({
                url: geoJsonPointUrl,
                type: 'GET'
            });
            geoJsonPointUrlRequest.then(function(response) {
                geoJsonPointResponse = response;
            });
        });

    }

    function callMap(geoJson) {

        var mapDiv = document.createElement("div");
        mapDiv.id = "map";
        var mainContainer = document.getElementById("mapa");
        mainContainer.append(mapDiv);

        // Web map code goes here
        map = L.map('map', { center: [5.547631, -74.945643], zoom: 9, scrollWheelZoom: false });
        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', { attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>' }).addTo(map);

        map.doubleClickZoom.disable();


        L.geoJson(geoJson, {
            onEachFeature: onEachFeature
        }).addTo(map);

        // map.on('click', onMapClick);
    }


    function onEachFeature(feature, layer) {
        layer.on({
            click: updateChart
        });
    }

    /*
     * Update chart Yaxis when user's
     * clic on Arc in the Map
     *
     * @param    Object  e   object selected
     */

    function updateChart(e) {

        var layer = e.target;
        $('#coordinateX').val(e.latlng.lat.toString());
        $('#coordinateY').val(e.latlng.lng.toString());
        $('#arcId').val(layer.feature.properties.ID);
    }

    $('#addProject').click(function() {
        var coordinateX = $('#coordinateX').val();
        if (coordinateX != "") {
            var titleProject = $('#titleProject').val();
            var potencia = $('#idPower').val();
            if (titleProject != "" && titleProject != " " && potencia != "" && potencia != " ") {
                var descriptionProject = $('#descriptionProject').val();
                var coordinateY = $('#coordinateY').val();
                var arcId = $('#arcId').val();
                var selectTypeProject = $('#selectTypeProject').val();
                var sedimentRetention = $('#sedimentRetention').val();
                var idHeight = $('#idHeight').val();
                var idVolume = $('#idVolume').val();
                var idPower = $('#idPower').val();
                var yearProject = $('#yearProject').val();

                $("#coordinateY").prop("disabled", true);
                $("#coordinateX").prop("disabled", false);
                $("#arcId").prop("disabled", false);
                var dataJson = {
                    'title': titleProject,
                    'type': 'tier1_project',
                    'field_description': {
                        'und': [{ 'value': descriptionProject }]
                    },
                    'field_project_type': {
                        'und': [{
                            'value': selectTypeProject
                        }]
                    },
                    'field_arc_id': {
                        'und': [{
                            'value': arcId
                        }]
                    },
                    'field_x_coord': {
                        'und': [{
                            'value': coordinateX
                        }]
                    },
                    'field_y_coord': {
                        'und': [{
                            'value': coordinateY
                        }]
                    },
                    'field_sediment_retention': {
                        'und': [{
                            'value': sedimentRetention
                        }]
                    },
                    'field_reservoir_height': {
                        'und': [{
                            'value': idHeight
                        }]
                    },
                    'field_reservoir_total_volume': {
                        'und': [{
                            'value': idVolume
                        }]
                    },
                    'field_installed_power': {
                        'und': [{
                            'value': idPower
                        }]
                    },
                    'field_operation_year': {
                        'und': [{
                            'value': yearProject
                        }]
                    },
                }


                var container = $('#project');
                var inputs = container.find('input');
                var id = inputs.length + 1;
                numProject++;
                //$('<input />', { type: 'checkbox', id: 'cb' + id, value: val, class: 'check', style: " width: 4%; height: auto; display:inline;  " }).appendTo(container).attr('checked', true);
                $('<label />', { 'for': 'cb' + id, text: numProject + '. ' + titleProject, class: 'check', style: "padding-right: 3%; padding-bottom: 2%;" }).appendTo(container);


                arrayProject = project.push(dataJson);
                resetform();
            } else {
                alert("Complete los campos obligatorios");
            }

        } else {
            location.href = '#';
        }
    })


    $("#addFootprint").click(function() {
        var valor = $("#selectFootprint").val();
        var Footprintname = $('#selectFootprint option:selected').text()
        var container = $('#checkAddFootprint');
        var inputs = container.find('input');
        var id = inputs.length + 1;
        numFootprint++;
        $('<input />', { type: 'checkbox', id: 'cb' + id, value: valor, name: 'check4', class: 'check4', style: " width: 4%; height: auto; display:inline;  " }).appendTo(container).attr('checked', true);
        $('<label />', { 'for': 'cb' + id, class: 'labelcheck4', text: numFootprint + '. ' + Footprintname, style: "padding-right: 3%; padding-bottom: 2%;" }).appendTo(container);
    });

    function resetform() {

        $("#titleProject").val('');
        $("#descriptionProject").val('');
        $("#coordinateX").val('');
        $("#coordinateY").val('');
        $("#arcId").val('');
        $("#selectTypeProject").val(0);
        $("#sedimentRetention").val('');
        $("#idHeight").val('');
        $("#idVolume").val('');
        $("#idPower").val('');
        $("#yearProject").val('');
        location.href = '#';
    }

    myElement.style.background = "linear-gradient(to right, #4eb8d1 " + porcentaje + "%, #1d7492 " + porcentaje + "%)";
    $("#nameStep").text("Paso de decisión: Cálculo del caudal (Q)");

    $("#streamflowStepNo1").css("display", "none");
    $("#precipitationStepNo2").css("display", "none");
    $("#precipitationStepYes2").css("display", "none");
    $("#medium").css("display", "none");
    $("#popup-link").css("display", "none");

    $("#executionStep").css("backgroundColor", "#104659");
    $("#executionStep").css("color", "#DFF1F2");



    $(document).ready(function() {
        $('#btnSave').click(function() {
            addCheckbox($('#selectproject').val());
        });
    });

    $('#selectproject').on('change', function() {
        val = $(this).val();
        name = $('#selectproject option:selected').text()
    });

    function addCheckbox() {
        var container = $('#cblist');
        var inputs = container.find('input');
        var id = inputs.length + 1;

        $('<input />', { type: 'checkbox', id: 'cb' + id, value: val, name: 'check', class: 'check', style: " width: 4%; height: auto; display:inline;  " }).appendTo(container).attr('checked', true);
        $('<label />', { 'for': 'cb' + id, class: 'labelcheck', text: name, style: "padding-right: 3%; padding-bottom: 2%;" }).appendTo(container);
    }


    $(document).ready(function() {
        $('#btnSaveCatalog').click(function() {
            var IdCatalog = $('#selectCatalog').val();
            apiUrlBase = "/api/fishermanApp/entity_node.json?";
            //datasetRequest
            apiParameters = "parameters[type]=project_catalog&parameters[nid]=" + IdCatalog + "&fields=field_projects";
            fullUrlApi = baseUrl + apiUrlBase + apiParameters;
            proyectCatalogId = $.ajax({
                url: fullUrlApi,
                type: 'GET'
            });
            proyectCatalogId.then(function(response) {
                CatalogId = response[0].field_projects.und;

                var arrayProjects = [];
                CatalogId.forEach(function(project) {
                    apiParameters = "parameters[type]=tier1_project&parameters[nid]=" + project.target_id + "&fields=title,nid";
                    fullUrlApi = baseUrl + apiUrlBase + apiParameters;
                    var nameProject = $.ajax({
                        url: fullUrlApi,
                        type: 'GET'

                    });
                    arrayProjects.push(nameProject);
                });
                for (const item of arrayProjects) {
                    item.then(function(response) {
                        name = response[0].title;
                        nidval = response[0].nid;
                        var container = $('#cblist1');
                        var inputs = container.find('input');
                        var id = inputs.length + 1;

                        $('<input />', { type: 'checkbox', id: 'cb' + id, value: nidval, name: 'check1', class: 'check1', style: " width: 4%; height: auto; display:inline;  " }).appendTo(container).attr('checked', true);
                        $('<label />', { 'for': 'cb' + id, class: 'labelcheck1', text: name, style: "padding-right: 3%; padding-bottom: 2%;" }).appendTo(container);
                    });
                }
            });

        });
    });

    $(document).ready(function() {
        $('#btnSaveNarrative ').click(function() {
            var nameNarrative = $('#selectNarrative option:selected').text();
            var valNarrative = $('#selectNarrative').val();
            var container = $('#listNarratives');
            var inputs = container.find('input');
            var id = inputs.length + 1;
            $('<input />', { type: 'checkbox', id: 'cb' + id, value: valNarrative, name: 'checkNarrative', class: 'checkNarrative', style: " width: 4%; height: auto; display:inline;  " }).appendTo(container).attr('checked', true);
            $('<label />', { 'for': 'cb' + id, class: 'lavelNarrative', text: id + '. ' + nameNarrative, style: "padding-right: 3%; padding-bottom: 2%;" }).appendTo(container);
        });
    });

    $(".next").click(function() {
        //if (animating) return false;
        animating = true;
        paso++;
        //  console.log(paso);

        nexValidation = true; //nextStep();

        //alert(nexValidation);

        if (nexValidation != false) {
            if (paso == 2) {
                previousStep = 1;
                // var question1 = $("#radio").val();
                var ele = document.getElementsByName('radio');
                for (i = 0; i < ele.length; i++) {
                    if (ele[i].checked)
                        var question1 = ele[i].value;
                }
                if (question1 == 1) {
                    step = 2;
                    $("#nameStep").text("Paso de decisión: Cálculo de la evapotranspiración Real (ETR)");
                    paso = 2;
                    current_fs = $(this).parent();
                    var idStep = $('#question');
                    confStep(idStep, current_fs);
                    //va a 3
                } else {
                    step = 3;
                    $("#streamflowStepNo1").css("display", "block");
                    $("#popup-link").css("display", "block");
                    $("#nameStep").text("Datos básicos de la ejecución.");
                    $("#executionStep").css("backgroundColor", "#104659");
                    $("#executionStep").css("color", "#DFF1F2");
                    paso = 3;
                    current_fs = $(this).parent();
                    var idStep = $('#basicData');
                    confStep(idStep, current_fs);
                    //va a 4
                }
            } else if (paso == 3) {
                previousStep = 2;
                // var question1 = $("#radio").val();
                var ele = document.getElementsByName('questionEv');
                for (i = 0; i < ele.length; i++) {
                    if (ele[i].checked)
                        var question2 = ele[i].value;
                }
                if (question2 == 1) {
                    step = 3;
                    $("#precipitationStepYes2").css("display", "block");
                    $("#popup-link").css("display", "block");
                    $("#nameStep").text(" Datos básicos de la ejecución.");
                    $("#executionStep2").css("backgroundColor", "#104659");
                    $("#executionStep2").css("color", "#DFF1F2");
                    paso = 5;
                    current_fs = $(this).parent();
                    var idStep = $('#basicData');
                    confStep(idStep, current_fs);
                    //va al 6
                } else {
                    step = 3;
                    $("#precipitationStepNo2").css("display", "block");
                    $("#popup-link").css("display", "block");
                    $("#nameStep").text("Datos básicos de la ejecución.");
                    $("#executionStep1").css("backgroundColor", "#104659");
                    $("#executionStep1").css("color", "#DFF1F2");
                    paso = 6;
                    current_fs = $(this).parent();
                    var idStep = $('#basicData');
                    confStep(idStep, current_fs);
                    //va al 7
                }
            } else if (paso == 4) {
                var titleExecution = $("#title").val();
                if (titleExecution != "" && titleExecution != " ") {
                    previousStep = 3;
                    //console.log(selectstreamflow);
                    var select = document.getElementById("selectstreamflow");
                    for (index in selectstreamflow) {
                        select.options[select.options.length] = new Option(selectstreamflow[index].title, selectstreamflow[index].nid);
                    }
                    $("#executionStep").css("backgroundColor", "#2ca13a");
                    $("#streamflowStep").css("backgroundColor", "#104659");
                    $("#streamflowStep").css("color", "#DFF1F2");
                    step = 7;
                    $("#nameStep").text("Caja de datos del caudal (Q)");
                    current_fs = $(this).parent();
                    var idStep = $('#streamflow');
                    confStep(idStep, current_fs);
                    //va a 5
                } else {
                    paso = 3;
                    alert("Completar Nombre de la Ejecución");
                }
            } else if (paso == 5) {
                previousStep = 4;
                //console.log(selectSediments);
                var select = document.getElementById("selectSediments");
                for (index in selectSediments) {
                    select.options[select.options.length] = new Option(selectSediments[index].title, selectSediments[index].nid);
                }
                $("#streamflowStep").css("backgroundColor", "#2ca13a"); +
                $("#sedimentsStep").css("backgroundColor", "#104659");
                $("#sedimentsStep").css("color", "#DFF1F2");
                step = 8;
                $("#nameStep").text("Caja de datos de sedimento (Qs)");
                paso = 11;
                current_fs = $(this).parent();
                var idStep = $('#Sediments');
                confStep(idStep, current_fs);
                //va a 12
            } else if (paso == 6) {
                var titleExecution = $("#title").val();
                if (titleExecution != "" && titleExecution != " ") {
                    previousStep = 5;
                    $("#executionStep2").css("backgroundColor", "#2ca13a");
                    $("#precipitationStep2").css("backgroundColor", "#104659");
                    $("#precipitationStep2").css("color", "#DFF1F2");
                    //console.log(selectPrecipitation);
                    var select = document.getElementById("selectPrecipitation");
                    for (index in selectPrecipitation) {
                        select.options[select.options.length] = new Option(selectPrecipitation[index].title, selectPrecipitation[index].nid);
                    }
                    step = 4;
                    $("#nameStep").text("Caja de datos de precipitación (P)");
                    paso = 7;
                    current_fs = $(this).parent();
                    var idStep = $('#precipitation');
                    confStep(idStep, current_fs);
                    //va a 8
                } else {
                    paso = 5;
                    alert("Completar Nombre de la Ejecución");
                }

            } else if (paso == 7) {
                var titleExecution = $("#title").val();
                if (titleExecution != "" && titleExecution != " ") {
                    previousStep = 6;
                    $("#executionStep1").css("backgroundColor", "#2ca13a");
                    $("#precipitationStep").css("backgroundColor", "#104659");
                    $("#precipitationStep").css("color", "#DFF1F2");
                    //console.log(selectPrecipitation);
                    var select = document.getElementById("selectPrecipitation");
                    for (index in selectPrecipitation) {
                        select.options[select.options.length] = new Option(selectPrecipitation[index].title, selectPrecipitation[index].nid);
                    }
                    step = 4;
                    $("#nameStep").text("Caja de datos de precipitación (P)");
                    paso = 8;
                    current_fs = $(this).parent();
                    var idStep = $('#precipitation');
                    confStep(idStep, current_fs);
                    //va a 9
                } else {
                    paso = 6;
                    alert("Completar Nombre de la Ejecución");
                }
            } else if (paso == 8) {
                previousStep = 7;
                $("#precipitationStep2").css("backgroundColor", "#2ca13a");
                $("#temperatureStep").css("backgroundColor", "#104659");
                $("#temperatureStep").css("color", "#DFF1F2");
                //console.log(selectTemperature);
                var select = document.getElementById("selectTemperature");
                for (index in selectTemperature) {
                    select.options[select.options.length] = new Option(selectTemperature[index].title, selectTemperature[index].nid);
                }
                step = 5;
                $("#nameStep").text("Caja de datos de temperatura (T)");
                paso = 10;
                current_fs = $(this).parent();
                var idStep = $('#temperature');
                confStep(idStep, current_fs);
                //va a 11
            } else if (paso == 9) {
                previousStep = 8;
                $("#precipitationStep").css("backgroundColor", "#2ca13a");
                $("#evapotranspiracionStep").css("backgroundColor", "#104659");
                $("#evapotranspiracionStep").css("color", "#DFF1F2");
                //console.log(selectEvapotranspiracion);
                var select = document.getElementById("selectEvapotranspiracion");
                for (index in selectEvapotranspiracion) {
                    select.options[select.options.length] = new Option(selectEvapotranspiracion[index].title, selectEvapotranspiracion[index].nid);
                }
                step = 6;
                $("#nameStep").text("Caja de datos de evapotranspiración Real (ETR)");
                current_fs = $(this).parent();
                var idStep = $('#Evapotranspiracion');
                confStep(idStep, current_fs);
                //va a 10
            } else if (paso == 10) {
                previousStep = 9;
                $("#evapotranspiracionStep").css("backgroundColor", "#2ca13a");
                $("#sedimentsStep1").css("backgroundColor", "#104659");
                $("#sedimentsStep1").css("color", "#DFF1F2");
                //console.log(selectSediments);
                var select = document.getElementById("selectSediments");
                for (index in selectSediments) {
                    select.options[select.options.length] = new Option(selectSediments[index].title, selectSediments[index].nid);
                }
                paso = 12;
                step = 8;
                $("#nameStep").text("Caja de datos de sedimento (Qs)");
                current_fs = $(this).parent();
                var idStep = $('#Sediments');
                confStep(idStep, current_fs);
                //va al 13
            } else if (paso == 11) {
                previousStep = 10;
                $("#temperatureStep").css("backgroundColor", "#2ca13a");
                $("#sedimentsStep2").css("backgroundColor", "#104659");
                $("#sedimentsStep2").css("color", "#DFF1F2");
                //console.log(selectSediments);
                var select = document.getElementById("selectSediments");
                for (index in selectSediments) {
                    select.options[select.options.length] = new Option(selectSediments[index].title, selectSediments[index].nid);
                }
                step = 8;
                paso = 13
                $("#nameStep").text("Caja de datos de sedimento (Qs)");
                current_fs = $(this).parent();
                var idStep = $('#Sediments');
                confStep(idStep, current_fs);
                //va al 14
            } else if (paso == 12) {
                var valueStreamflow = $("#selectstreamflow").val();
                var valueSediments = $("#selectSediments").val();
                if (valueStreamflow == 0 && valueSediments == 0) {
                    alert("Recuerde que no ingreso datos de Caudal ni de sedimento");
                }
                previousStep = 11;
                $("#sedimentsStep").css("backgroundColor", "#2ca13a");
                $("#narrativeStep").css("backgroundColor", "#104659");
                $("#narrativeStep").css("color", "#DFF1F2");
                step = 9;
                paso = 14;
                $("#nameStep").text("Narrativa");
                current_fs = $(this).parent();
                var idStep = $('#narrative');
                confStep(idStep, current_fs);
                //va a la funcion newNarrative 
                //va al 15
            } else if (paso == 13) {
                var valuePrecipitacion = $("#selectPrecipitation").val();
                var valueEvapotranspiracion = $("#selectEvapotranspiracion").val();
                var valueSediments = $("#selectSediments").val();
                if (valuePrecipitacion == 0 && valueEvapotranspiracion == 0 && valueSediments == 0) {
                    alert("Recuerde que no ingreso datos de Precipitación, Evapotranspiración y Sedimentos");
                };
                previousStep = 12;
                $("#sedimentsStep1").css("backgroundColor", "#2ca13a");
                $("#narrativeStep1").css("backgroundColor", "#104659");
                $("#narrativeStep1").css("color", "#DFF1F2");
                step = 9;
                paso = 15;
                $("#nameStep").text("Narrativa");
                current_fs = $(this).parent();
                var idStep = $('#narrative');
                confStep(idStep, current_fs);
                //va a la funcion newNarrative 
                //va al 16
            } else if (paso == 14) {
                var valuePrecipitacion = $("#selectPrecipitation").val();
                var valueTemperature = $("#selectTemperature").val();
                var valueSediments = $("#selectSediments").val();
                if (valuePrecipitacion == 0 && valueTemperature == 0 && valueSediments == 0) {
                    alert("Recuerde que no ingreso datos de Precipitación, Temperatura y Sedimentos");
                };
                previousStep = 13;
                $("#sedimentsStep2").css("backgroundColor", "#2ca13a");
                $("#narrativeStep2").css("backgroundColor", "#104659");
                $("#narrativeStep2").css("color", "#DFF1F2");
                step = 9;
                paso = 16;
                $("#nameStep").text("Narrativa");
                current_fs = $(this).parent();
                var idStep = $('#narrative');
                confStep(idStep, current_fs);
                //va a la funcion newNarrative 
                //va al 17
            } else if (paso == 15) {

                paso = 23;
                previousStep = 14;
                $("#sedimentsStep").css("backgroundColor", "#2ca13a");
                $("#narrativeStep").css("backgroundColor", "#104659");
                $("#narrativeStep").css("color", "#DFF1F2");
                step = 10;
                var select = document.getElementById("selectNarrative");
                for (index in selectNarrative) {
                    select.options[select.options.length] = new Option(selectNarrative[index].title, selectNarrative[index].nid);
                }
                $("#nameStep").text("Seleccione la narrativa deseada del siguiente listado:");
                current_fs = $(this).parent();
                var idStep = $('#narrativeSelect');
                confStep(idStep, current_fs);
                //va al paso 24
            } else if (paso == 16) {
                paso = 24;
                previousStep = 15;
                step = 10;
                var select = document.getElementById("selectNarrative");
                for (index in selectNarrative) {
                    select.options[select.options.length] = new Option(selectNarrative[index].title, selectNarrative[index].nid);
                }
                $("#nameStep").text("Narrativa");
                current_fs = $(this).parent();
                var idStep = $('#narrativeSelect');
                confStep(idStep, current_fs);
                //va al paso 25
            } else if (paso == 17) {
                paso = 25;
                previousStep = 16;
                step = 10;
                var select = document.getElementById("selectNarrative");
                for (index in selectNarrative) {
                    select.options[select.options.length] = new Option(selectNarrative[index].title, selectNarrative[index].nid);
                }
                $("#nameStep").text("Narrativa");
                current_fs = $(this).parent();
                var idStep = $('#narrativeSelect');
                confStep(idStep, current_fs);
                //va al paso 26
            } else if (paso == 18) {
                var titleNarrative = $("#titleNarrative").val();
                if (titleNarrative != "" && titleNarrative != " ") {
                    step = 11;
                    paso = 20;
                    previousStep = 20;
                    $("#nameStep").text("Aleatorización de narrativas");
                    current_fs = $(this).parent();
                    var idStep = $('#aleatorizaciones');
                    confStep(idStep, current_fs);
                    //viene de la funcion newNarrative paso 14
                    //va al paso 21
                } else {
                    paso = 17;
                    alert("Complete el nombre de narrativa");
                }
            } else if (paso == 19) {
                var titleNarrative = $("#titleNarrative").val();
                if (titleNarrative != "" && titleNarrative != " ") {
                    step = 11;
                    paso = 21;
                    previousStep = 21;
                    $("#nameStep").text("Aleatorización de narrativas");
                    current_fs = $(this).parent();
                    var idStep = $('#aleatorizaciones');
                    confStep(idStep, current_fs);
                    //viene de la funcion newNarrative del paso 15
                    //va a el paso 22
                } else {
                    paso = 18;
                    alert("Complete el nombre de narrativa");
                }
            } else if (paso == 20) {
                var titleNarrative = $("#titleNarrative").val();
                if (titleNarrative != "" && titleNarrative != " ") {
                    step = 11;
                    paso = 22;
                    previousStep = 22;
                    $("#nameStep").text("Aleatorización de narrativas");
                    current_fs = $(this).parent();
                    var idStep = $('#aleatorizaciones');
                    confStep(idStep, current_fs);
                    //viene de la funcion newNarrative del paso 15
                    //va a el paso 23
                } else {
                    paso = 19;
                    alert("Complete el nombre de narrativa");
                }
            } else if (paso == 21) {
                var aeatorizacion = $('#aleatorizacionesNunber').val();
                if (aeatorizacion != "") {
                    step = 12;
                    $("#narrativeStep").css("backgroundColor", "#2ca13a");
                    $("#catalogsStep").css("backgroundColor", "#104659");
                    $("#catalogsStep").css("color", "#DFF1F2");
                    paso = 21;
                    previousStep = 23;
                    $("#nameStep").text("Catalogs");
                    current_fs = $(this).parent();
                    var idStep = $('#catalogs');
                    confStep(idStep, current_fs);
                    //va al paso a la funcion existingCatalog paso 21
                } else {
                    paso = 20;
                    alert("ingrese un valor para aleatorizacion");
                }
            } else if (paso == 22) {
                var aeatorizacion = $('#aleatorizacionesNunber').val();
                if (aeatorizacion != "") {
                    step = 12;
                    $("#narrativeStep1").css("backgroundColor", "#2ca13a");
                    $("#catalogsStep1").css("backgroundColor", "#104659");
                    $("#catalogsStep1").css("color", "#DFF1F2");
                    paso = 22;
                    previousStep = 24;
                    $("#nameStep").text("Catalogs");
                    current_fs = $(this).parent();
                    var idStep = $('#catalogs');
                    confStep(idStep, current_fs);
                    //va a la funcion existingCatalog paso 22
                } else {
                    paso = 20;
                    alert("ingrese un valor para aleatorizacion");
                }
            } else if (paso == 23) {
                var aeatorizacion = $('#aleatorizacionesNunber').val();
                if (aeatorizacion != "") {
                    step = 12;
                    $("#narrativeStep2").css("backgroundColor", "#2ca13a");
                    $("#catalogsStep2").css("backgroundColor", "#104659");
                    $("#catalogsStep2").css("color", "#DFF1F2");
                    paso = 23;
                    previousStep = 25;
                    $("#nameStep").text("Catalogs");
                    current_fs = $(this).parent();
                    var idStep = $('#catalogs');
                    confStep(idStep, current_fs);
                    //va a la funcion existingCatalog paso 23
                    //va a la funcion existingCatalog paso 22
                } else {
                    paso = 20;
                    alert("ingrese un valor para aleatorizacion");
                }
            } else if (paso == 24) {
                var narrativaVal = 0;
                $("input:checkbox[name=checkNarrative]:checked").each(function() {
                    narrativaVal = 1;
                });
                if (narrativaVal == 0) {
                    paso = 23;
                    alert('ingrese una narrativa');
                } else {
                    $("#narrativeStep").css("backgroundColor", "#2ca13a");
                    $("#catalogsStep").css("backgroundColor", "#2ca13a");
                    $("#catalogsStep").css("color", "#DFF1F2");
                    $("#footprintStep").css("backgroundColor", "#104659");
                    $("#footprintStep").css("color", "#DFF1F2");
                    paso = 55;
                    step = 15;
                    previousStep = 26;
                    $("#nameStep").text("Modelo de huella");
                    current_fs = $(this).parent();
                    var idStep = $('#question4');
                    confStep(idStep, current_fs);
                    //viene de la funcion existingCatalog del paso 24
                }
            } else if (paso == 25) {
                var narrativaVal = 0;
                $("input:checkbox[name=checkNarrative]:checked").each(function() {
                    narrativaVal = 1;
                });
                if (narrativaVal == 0) {
                    paso = 24;
                    alert('ingrese una narrativa');
                } else {
                    $("#narrativeStep1").css("backgroundColor", "#2ca13a");
                    $("#catalogsStep1").css("backgroundColor", "#2ca13a");
                    $("#catalogsStep1").css("color", "#DFF1F2");
                    $("#footprintStep1").css("backgroundColor", "#104659");
                    $("#footprintStep1").css("color", "#DFF1F2");
                    paso = 63;
                    step = 15;
                    previousStep = 27;
                    $("#nameStep").text("Huella");
                    current_fs = $(this).parent();
                    var idStep = $('#question4');
                    confStep(idStep, current_fs);
                }
            } else if (paso == 26) {
                var narrativaVal = 0;
                $("input:checkbox[name=checkNarrative]:checked").each(function() {
                    narrativaVal = 1;
                });
                if (narrativaVal == 0) {
                    paso = 25;
                    alert('ingrese una narrativa');
                } else {
                    $("#narrativeStep2").css("backgroundColor", "#2ca13a");
                    $("#catalogsStep2").css("backgroundColor", "#2ca13a");
                    $("#catalogsStep2").css("color", "#DFF1F2");
                    $("#footprintStep2").css("backgroundColor", "#104659");
                    $("#footprintStep2").css("color", "#DFF1F2");
                    paso = 71;
                    step = 15;
                    previousStep = 28;
                    $("#nameStep").text("Modelo de huella");
                    current_fs = $(this).parent();
                    var idStep = $('#question4');
                    confStep(idStep, current_fs);
                }
            } else if (paso == 27) {
                var titleCatalog = $("#titleCatalog").val();
                if (titleCatalog != "" && titleCatalog != " ") {
                    Step = 13;
                    paso = 32;
                    previousStep = 35;
                    $("#nameStep").text("Selección de proyectos");
                    current_fs = $(this).parent();
                    var idStep = $('#question3');
                    confStep(idStep, current_fs);
                    //viene de la funcion newCatalogs del paso 21 del nueva narrativa del no de la primera pregunta y va a seleccion de proyectos
                    //va al paso 33
                } else {
                    paso = 26;
                    alert("Complete el nombre del catalogo");
                }
            } else if (paso == 28) {
                Step = 13;
                paso = 33;
                previousStep = 36;
                $("#nameStep").text("Selección de proyectos");
                current_fs = $(this).parent();
                var idStep = $('#question3');
                confStep(idStep, current_fs);
                //viene de la funcion newCatalogs del paso 24 de narrativa exiastente del no de la primera pregunta va a seleccion de proyectos
                //va al paso 34
            } else if (paso == 29) {
                var titleCatalog = $("#titleCatalog").val();
                if (titleCatalog != "" && titleCatalog != " ") {
                    Step = 13;
                    paso = 34;
                    previousStep = 37;
                    $("#nameStep").text("Selección de proyectos");
                    current_fs = $(this).parent();
                    var idStep = $('#question3');
                    confStep(idStep, current_fs);
                    //viene de la funcion newCatalogs del paso 22 del nueva narrativa del no de la segunda pregunta y va a seleccion de proyectos
                    //va al paso 35
                } else {
                    paso = 26;
                    alert("Complete el nombre del catalogo");
                }
            } else if (paso == 30) {
                Step = 13;
                paso = 35;
                previousStep = 38;
                $("#nameStep").text("Selección de proyectos");
                current_fs = $(this).parent();
                var idStep = $('#question3');
                confStep(idStep, current_fs);
                //viene de la funcion newCatalogs del paso 25 de narrativa exiastente del no de la segunda pregunta va a seleccion de proyectos
                //va al paso 36
            } else if (paso == 31) {
                var titleCatalog = $("#titleCatalog").val();
                if (titleCatalog != "" && titleCatalog != " ") {
                    Step = 13;
                    paso = 36;
                    previousStep = 39;
                    $("#nameStep").text("Selección de proyectos");
                    current_fs = $(this).parent();
                    var idStep = $('#question3');
                    confStep(idStep, current_fs);
                    //viene de la funcion newCatalogs del paso 23 del nueva narrativa del si de la segunda pregunta y va a seleccion de proyectos
                    //va al paso 37
                } else {
                    paso = 26;
                    alert("Complete el nombre del catalogo");
                }
            } else if (paso == 32) {
                Step = 13;
                paso = 37;
                previousStep = 40;
                $("#nameStep").text("Selección de proyectos");
                current_fs = $(this).parent();
                var idStep = $('#question3');
                confStep(idStep, current_fs);
                //viene de la funcion newCatalogs del paso 26 de narrativa exiastente del si de la segunda pregunta va a seleccion de proyectos
                //va al paso 38
            } else if (paso == 33) {
                // var question1 = $("#radio").val();
                var ele = document.getElementsByName('questionPro');
                for (i = 0; i < ele.length; i++) {
                    if (ele[i].checked)
                        var question2 = ele[i].value;
                }
                if (question2 == 1) {
                    step = 14;
                    paso = 38;
                    previousStep = 41;
                    $("#nameStep").text("Selección de proyectos");
                    var select = document.getElementById("selectproject");
                    for (index in selectProject) {
                        select.options[select.options.length] = new Option(selectProject[index].title, selectProject[index].nid);
                    }
                    current_fs = $(this).parent();
                    var idStep = $('#projectSelect');
                    confStep(idStep, current_fs);
                    var proyectos = $('#btnSave');
                    //Select de proyecto del nueva narrativa del no de la primera pregunta
                    //viene del nuevo catalogo de proyecto del si de agregar proyectos existente 
                    //va al 39
                } else if (question2 == 0) {
                    step = 14;
                    $(".contForm").css("height", "638px");
                    $(".conter").css("height", "716px");
                    paso = 39;
                    previousStep = 41;
                    $("#nameStep").text("Ubicación del proyecto");
                    current_fs = $(this).parent();
                    var idStep = $('#projectMap');
                    confStep(idStep, current_fs);
                    map();
                    //Select de proyecto en el mapa del nueva narrativa del no de la primera pregunta
                    //viene del nuevo catalogo de proyecto del no de añadir nuevo proyecto
                    //va al 40
                }
            } else if (paso == 34) {
                var ele = document.getElementsByName('questionPro');
                for (i = 0; i < ele.length; i++) {
                    if (ele[i].checked)
                        var question2 = ele[i].value;
                }
                if (question2 == 1) {
                    step = 14;
                    paso = 40;
                    previousStep = 42;
                    var select = document.getElementById("selectproject");
                    for (index in selectProject) {
                        select.options[select.options.length] = new Option(selectProject[index].title, selectProject[index].nid);
                    }
                    $("#nameStep").text("Selección de proyectos");
                    current_fs = $(this).parent();
                    var idStep = $('#projectSelect');
                    confStep(idStep, current_fs);
                    //Select de proyecto de narrativa existente del no de la primera pregunta
                    //viene del nuevo catalogo de proyecto del si de agregar proyectos existente 
                    //va al 41
                } else if (question2 == 0) {
                    step = 14;
                    $(".contForm").css("height", "638px");
                    $(".conter").css("height", "716px");
                    // paso = 41;
                    previousStep = 42;
                    $("#nameStep").text("Ubicación del proyecto");
                    current_fs = $(this).parent();
                    var idStep = $('#projectMap');
                    confStep(idStep, current_fs);
                    map();
                    //Select de proyecto en el mapa del narrativa existente del no de la primera pregunta
                    //viene del nuevo catalogo de proyecto del no de añadir nuevo proyecto
                }
            } else if (paso == 35) {
                // var question1 = $("#radio").val();
                var ele = document.getElementsByName('questionPro');
                for (i = 0; i < ele.length; i++) {
                    if (ele[i].checked)
                        var question2 = ele[i].value;
                }
                if (question2 == 1) {
                    step = 14;
                    paso = 42;
                    previousStep = 43;
                    var select = document.getElementById("selectproject");
                    for (index in selectProject) {
                        select.options[select.options.length] = new Option(selectProject[index].title, selectProject[index].nid);
                    }
                    $("#nameStep").text("Selección de proyectos");
                    current_fs = $(this).parent();
                    var idStep = $('#projectSelect');
                    confStep(idStep, current_fs);
                    //Select de proyecto del nueva narrativa del no de la segunda pregunta
                    //viene del nuevo catalogo de proyecto del si de agregar proyectos existente 
                    //va a 43
                } else if (question2 == 0) {
                    step = 14;
                    $(".contForm").css("height", "638px");
                    $(".conter").css("height", "716px");
                    paso = 43;
                    previousStep = 43;
                    $("#nameStep").text("Ubicación del proyecto");
                    current_fs = $(this).parent();
                    var idStep = $('#projectMap');
                    confStep(idStep, current_fs);
                    map();
                    //Select de proyecto en el mapa del nueva narrativa del no de la segunda pregunta
                    //viene del nuevo catalogo de proyecto del no de añadir nuevo proyecto
                    //va al 44
                }
            } else if (paso == 36) {
                var ele = document.getElementsByName('questionPro');
                for (i = 0; i < ele.length; i++) {
                    if (ele[i].checked)
                        var question2 = ele[i].value;
                }
                if (question2 == 1) {
                    step = 14;
                    paso = 44;
                    previousStep = 44;
                    var select = document.getElementById("selectproject");
                    for (index in selectProject) {
                        select.options[select.options.length] = new Option(selectProject[index].title, selectProject[index].nid);
                    }
                    $("#nameStep").text("Selección de proyectos");
                    current_fs = $(this).parent();
                    var idStep = $('#projectSelect');
                    confStep(idStep, current_fs);
                    //Select de proyecto de narrativa existente del no de la segunda pregunta
                    //viene del nuevo catalogo de proyecto del si de agregar proyectos existente 
                    //va al 45
                } else if (question2 == 0) {
                    step = 14;
                    $(".contForm").css("height", "638px");
                    $(".conter").css("height", "716px");
                    paso = 45;
                    previousStep = 44;
                    $("#nameStep").text("Ubicación del proyecto");
                    current_fs = $(this).parent();
                    var idStep = $('#projectMap');
                    confStep(idStep, current_fs);
                    map();
                    //Select de proyecto en el mapa del narrativa existente del no de la segunda pregunta
                    //viene del nuevo catalogo de proyecto del no de añadir nuevo proyecto
                    //va al 46
                }
            } else if (paso == 37) {
                // var question1 = $("#radio").val();
                var ele = document.getElementsByName('questionPro');
                for (i = 0; i < ele.length; i++) {
                    if (ele[i].checked)
                        var question2 = ele[i].value;
                }
                if (question2 == 1) {
                    step = 14;
                    paso = 46;
                    previousStep = 45;
                    var select = document.getElementById("selectproject");
                    for (index in selectProject) {
                        select.options[select.options.length] = new Option(selectProject[index].title, selectProject[index].nid);
                    }
                    $("#nameStep").text("Selección de proyectos");
                    current_fs = $(this).parent();
                    var idStep = $('#projectSelect');
                    confStep(idStep, current_fs);
                    //Select de proyecto del nueva narrativa del si de la segunda pregunta
                    //viene del nuevo catalogo de proyecto del si de agregar proyectos existente 
                    //va al 47
                } else if (question2 == 0) {
                    step = 14;
                    $(".contForm").css("height", "638px");
                    $(".conter").css("height", "716px");
                    paso = 47;
                    previousStep = 45;
                    $("#nameStep").text("Ubicación del proyecto");
                    current_fs = $(this).parent();
                    var idStep = $('#projectMap');
                    confStep(idStep, current_fs);
                    map();
                    //Select de proyecto en el mapa del nueva narrativa del si de la segunda pregunta
                    //viene del nuevo catalogo de proyecto del no de añadir nuevo proyecto
                    //val al 48
                }
            } else if (paso == 38) {
                var ele = document.getElementsByName('questionPro');
                for (i = 0; i < ele.length; i++) {
                    if (ele[i].checked)
                        var question2 = ele[i].value;
                }
                if (question2 == 1) {
                    step = 14;
                    paso = 48;
                    previousStep = 46;
                    var select = document.getElementById("selectproject");
                    for (index in selectProject) {
                        select.options[select.options.length] = new Option(selectProject[index].title, selectProject[index].nid);
                    }
                    $("#nameStep").text("Selección de proyectos");
                    current_fs = $(this).parent();
                    var idStep = $('#projectSelect');
                    confStep(idStep, current_fs);
                    //Select de proyecto de narrativa existente del si de la segunda pregunta
                    //viene del nuevo catalogo de proyecto del si de agregar proyectos existente 
                    //va al 49
                } else if (question2 == 0) {
                    step = 14;
                    $(".contForm").css("height", "638px");
                    $(".conter").css("height", "716px");
                    paso = 49;
                    previousStep = 46;
                    $("#nameStep").text("Ubicación del proyecto");
                    current_fs = $(this).parent();
                    var idStep = $('#projectMap');
                    confStep(idStep, current_fs);
                    map();
                    //Select de proyecto en el mapa del narrativa existente del si de la segunda pregunta
                    //viene del nuevo catalogo de proyecto del no de añadir nuevo proyecto
                    //va al 50
                }
            } else if (paso == 39) {

                $("#catalogsStep").css("backgroundColor", "#2ca13a");
                $("#footprintStep").css("backgroundColor", "#104659");
                $("#footprintStep").css("color", "#DFF1F2");
                paso = 61;
                step = 15;
                previousStep = 47;
                $("#nameStep").text("Modelo de huella");
                current_fs = $(this).parent();
                var idStep = $('#question4');
                confStep(idStep, current_fs);
                //viene del 33 del if == 1 de nueva narrativa del no de la primera pregunta

            } else if (paso == 40) {
                if ($('.check').length) {
                    $("#catalogsStep").css("backgroundColor", "#2ca13a");
                    $("#footprintStep").css("backgroundColor", "#104659");
                    $("#footprintStep").css("color", "#DFF1F2");
                    $(".contForm").css("height", "393px");
                    paso = 59;
                    step = 15;
                    previousStep = 48;
                    $("#nameStep").text("Modelo de huella");
                    current_fs = $(this).parent();
                    var idStep = $('#question4');
                    confStep(idStep, current_fs);
                    //viene del 33 del if == 0 de nueva narrativa del no de la primera pregunta
                } else {
                    paso = 39;
                    alert('ingese un proyecto');
                }

            } else if (paso == 41) {
                $("#catalogsStep").css("backgroundColor", "#2ca13a");
                $("#footprintStep").css("backgroundColor", "#104659");
                $("#footprintStep").css("color", "#DFF1F2");
                step = 15;
                previousStep = 49;
                $("#nameStep").text("Modelo de huella");
                current_fs = $(this).parent();
                var idStep = $('#question4');
                confStep(idStep, current_fs);
                //viene del 34 del if == 1 de narrativa existente del no de la primera pregunta
            } else if (paso == 42) {
                $("#catalogsStep").css("backgroundColor", "#2ca13a");
                $("#footprintStep").css("backgroundColor", "#104659");
                $("#footprintStep").css("color", "#DFF1F2");
                step = 15;
                previousStep = 50;
                $("#nameStep").text("Modelo de huella");
                current_fs = $(this).parent();
                var idStep = $('#question4');
                confStep(idStep, current_fs);
                //viene del 34 del if == 0 de narrativa existente del no de la primera pregunta 
            } else if (paso == 43) {
                $("#catalogsStep1").css("backgroundColor", "#2ca13a");
                $("#footprintStep1").css("backgroundColor", "#104659");
                $("#footprintStep1").css("color", "#DFF1F2");
                paso = 69;
                step = 15;
                previousStep = 51;
                $("#nameStep").text("Modelo de huella");
                current_fs = $(this).parent();
                var idStep = $('#question4');
                confStep(idStep, current_fs);
                //viene del 35 del if == 1 de nueva narrativa del no de la segunda pregunta
            } else if (paso == 44) {
                if ($('.check').length) {
                    $("#catalogsStep1").css("backgroundColor", "#2ca13a");
                    $("#footprintStep1").css("backgroundColor", "#104659");
                    $("#footprintStep1").css("color", "#DFF1F2");
                    $(".contForm").css("height", "393px");
                    paso = 67;
                    step = 15;
                    previousStep = 52;
                    $("#nameStep").text("Modelo de huella");
                    current_fs = $(this).parent();
                    var idStep = $('#question4');
                    confStep(idStep, current_fs);
                    //viene del 35 del if == 0 de nueva narrativa del no de la segunda pregunta
                } else {
                    paso = 43;
                    alert('ingrese proyectos');
                }
            } else if (paso == 45) {
                $("#catalogsStep1").css("backgroundColor", "#2ca13a");
                $("#footprintStep1").css("backgroundColor", "#104659");
                $("#footprintStep1").css("color", "#DFF1F2");
                step = 15;
                previousStep = 53;
                $("#nameStep").text("Modelo de huella");
                current_fs = $(this).parent();
                var idStep = $('#question4');
                confStep(idStep, current_fs);
                //viene del 36 del if == 1 de narrativa existente del no de la segunda pregunta
            } else if (paso == 46) {
                $("#catalogsStep1").css("backgroundColor", "#2ca13a");
                $("#footprintStep1").css("backgroundColor", "#104659");
                $("#footprintStep1").css("color", "#DFF1F2");
                $(".contForm").css("height", "393px");
                step = 15;
                previousStep = 54;
                $("#nameStep").text("Modelo de huella");
                current_fs = $(this).parent();
                var idStep = $('#question4');
                confStep(idStep, current_fs);
                //viene del 36 del if == 0 de narrativa existente del no de la segunda pregunta 
            } else if (paso == 47) {
                $("#catalogsStep2").css("backgroundColor", "#2ca13a");
                $("#footprintStep2").css("backgroundColor", "#104659");
                $("#footprintStep2").css("color", "#DFF1F2");
                paso = 77;
                step = 15;
                previousStep = 55;
                $("#nameStep").text("Modelo de huella");
                current_fs = $(this).parent();
                var idStep = $('#question4');
                confStep(idStep, current_fs);
                //viene del 37 del if == 1 de nueva narrativa del si de la segunda pregunta
            } else if (paso == 48) {
                if ($('.check').length) {
                    $("#catalogsStep2").css("backgroundColor", "#2ca13a");
                    $("#footprintStep2").css("backgroundColor", "#104659");
                    $("#footprintStep2").css("color", "#DFF1F2");
                    $(".contForm").css("height", "393px");
                    paso = 75;
                    step = 15;
                    previousStep = 56;
                    $("#nameStep").text("Modelo de huella");
                    current_fs = $(this).parent();
                    var idStep = $('#question4');
                    confStep(idStep, current_fs);
                    //viene del 37 del if == 0 de nueva narrativa del si de la segunda pregunta{}
                } else {
                    paso = 47;
                    alert('ingrese proyecto');
                }
            } else if (paso == 49) {
                $("#catalogsStep2").css("backgroundColor", "#2ca13a");
                $("#footprintStep2").css("backgroundColor", "#104659");
                $("#footprintStep2").css("color", "#DFF1F2");
                step = 15;
                previousStep = 57;
                $("#nameStep").text("Modelo de huella");
                current_fs = $(this).parent();
                var idStep = $('#question4');
                confStep(idStep, current_fs);
                //viene del 38 del if == 1 de narrativa existente del si de la segunda pregunta
            } else if (paso == 50) {
                $("#catalogsStep2").css("backgroundColor", "#2ca13a");
                $("#footprintStep2").css("backgroundColor", "#104659");
                $("#footprintStep2").css("color", "#DFF1F2");
                $(".contForm").css("height", "393px");
                step = 15;
                previousStep = 58;
                $("#nameStep").text("Modelo de huella");
                current_fs = $(this).parent();
                var idStep = $('#question4');
                confStep(idStep, current_fs);
                //viene del 38 del if == 0 de narrativa existente del si de la segunda pregunta 
            } else if (paso == 51) {
                $("#catalogsStep").css("backgroundColor", "#2ca13a");
                $("#footprintStep").css("backgroundColor", "#104659");
                $("#footprintStep").css("color", "#DFF1F2");
                paso = 57;
                step = 15;
                previousStep = 59;
                $("#nameStep").text("Modelo de huella");
                current_fs = $(this).parent();
                var idStep = $('#question4');
                confStepPrevious(idStep, current_fs);
                //viene de la funcion existingCatalog del paso 21
            } else if (paso == 53) {
                $("#catalogsStep1").css("backgroundColor", "#2ca13a");
                $("#footprintStep1").css("backgroundColor", "#104659");
                $("#footprintStep1").css("color", "#DFF1F2");
                paso = 65;
                step = 15;
                previousStep = 61;
                $("#nameStep").text("Modelo de huella");
                current_fs = $(this).parent();
                var idStep = $('#question4');
                confStep(idStep, current_fs);
                //viene de la funcion existingCatalog del paso 22
            } else if (paso == 55) {
                $("#catalogsStep2").css("backgroundColor", "#2ca13a");
                $("#footprintStep2").css("backgroundColor", "#104659");
                $("#footprintStep2").css("color", "#DFF1F2");
                paso = 73;
                step = 15;
                previousStep = 63;
                $("#nameStep").text("Modelo de huella");
                current_fs = $(this).parent();
                var idStep = $('#question4');
                confStep(idStep, current_fs);
                //viene de la funcion existingCatalog del paso 23
            } else if (paso == 56) {
                var ele = document.getElementsByName('questionHue');
                for (i = 0; i < ele.length; i++) {
                    if (ele[i].checked)
                        var question4 = ele[i].value;
                }
                if (question4 == 1) {
                    paso = 56;
                    step = 16;
                    previousStep = 64;
                    $("#nameStep").text("Seleccionar Huella");
                    var select = document.getElementById("selectFootprint");
                    for (index in selectHuella) {
                        select.options[select.options.length] = new Option(selectHuella[index].title, selectHuella[index].nid);
                    }
                    current_fs = $(this).parent();
                    var idStep = $('#footprint');
                    confStep(idStep, current_fs);
                    //viene de la funcion existingCatalog del paso 24
                } else {
                    var caudal = $("#selectstreamflow").val();
                    var sedimentos = $("#selectSediments").val();

                    if (sedimentos == 0) {
                        $("#sedimentos").attr('disabled', 'disabled');
                        $("#sedimentos").prop("checked", false);
                    } else {
                        $("#sedimentos").prop("checked", true);
                    }

                    if (caudal == 0) {
                        $("#dor").attr('disabled', 'disabled');
                        $("#dorw").attr('disabled', 'disabled');
                        $("#dor").prop("checked", false);
                        $("#dorw").prop("checked", false);
                        $("#fragmentacion").attr('disabled', 'disabled');
                        $("#fragmentacion").prop("checked", true);
                        //var container = $('#model');
                        //var inputs = container.find('input');
                        //var id = inputs.length + 1;
                        //$('<input />', { type: 'checkbox', id: 'dor' + id, value: 'dor', class: 'checkModel', style: " width: 4%; height: auto; display:inline;  " }).appendTo(container).attr('checked', true);
                        //$('<label />', { 'for': 'cb' + id, text: "DOR", style: "padding-right: 3%; padding-bottom: 2%;" }).appendTo(container);
                        /* idNarrative = new Array();
                         idprojectNarrativa = new Array();
                         var idNarratiProject = $(".checkNarrative:checked").length; //Creamos una Variable y Obtenemos el Numero de Checkbox que esten Seleccionados
                         $("input:checkbox:checked").each(function() {
                             idNarrative.push($(this).val()); //toca hacer un for para poder  recorrerlo  para traer los proyectos 
                         });

                         $.each(idNarrative, function(index, value) {
                             apiUrlBase = "/api/fishermanApp/entity_node.json?";
                             //datasetRequest
                             apiParameters = "http://test.sima/api/fishermanApp/entity_node.json?parameters[type]=narrative&parameters[nid]=" + value;
                             fullUrlApi = baseUrl + apiUrlBase + apiParameters;
                             proyectNarrativeId = $.ajax({
                                 url: fullUrlApi,
                                 type: 'GET'
                             });
                             idprojectNarrativa.push(proyectNarrativeId);

                         });
                         preojectNarrative(idprojectNarrativa);*/

                    } else {
                        $("#dor").prop("checked", true);
                        $("#dorw").prop("checked", true);
                        $("#fragmentacion").attr('disabled', 'disabled');
                        $("#fragmentacion").prop("checked", true);
                    }
                    $("#huella").css("display", "none");
                    $("#huellaLabel").css("display", "none");
                    $(".contForm").css("height", "460px");
                    $(".conter").css("height", "630px");
                    step = 16;
                    previousStep = 64;
                    $("#nameStep").text("Seleccionar Modelos");
                    current_fs = $(this).parent();
                    var idStep = $('#model');
                    confStep(idStep, current_fs);
                    //viene de la funcion existingCatalog del paso 24
                }
            } else if (paso == 57) {
                var caudal = $("#selectstreamflow").val();
                var sedimentos = $("#selectSediments").val();
                checkedFootprint = $(".check4:checked").length;
                if (checkedFootprint != 0) {
                    $("#huella").css("display", "inline-block");
                    $("#huellaLabel").css("display", "inline-block");
                    $("#huella").prop("checked", true);
                    if (sedimentos == 0) {
                        $("#sedimentos").attr('disabled', 'disabled');
                        $("#sedimentos").prop("checked", false);
                    } else {
                        $("#sedimentos").prop("checked", true);
                    }

                    if (caudal == 0) {
                        $("#dor").attr('disabled', 'disabled');
                        $("#dorw").attr('disabled', 'disabled');
                        $("#dor").prop("checked", false);
                        $("#dorw").prop("checked", false);
                        $("#fragmentacion").attr('disabled', 'disabled');
                        $("#fragmentacion").prop("checked", true);
                    } else {
                        $("#dor").prop("checked", true);
                        $("#dorw").prop("checked", true);
                        $("#fragmentacion").attr('disabled', 'disabled');
                        $("#fragmentacion").prop("checked", true);
                    }
                    $(".contForm").css("height", "460px");
                    $(".conter").css("height", "630px");
                    step = 16;
                    previousStep = 65;
                    $("#nameStep").text("Seleccionar Huella");
                    current_fs = $(this).parent();
                    var idStep = $('#model');
                    confStep(idStep, current_fs);
                    //viene de si del paso 56
                } else {
                    alert("Por favor ingrese una capa");
                    paso = 56;
                    step = 16;
                    previousStep = 64;
                }

            } else if (paso == 58) {
                var ele = document.getElementsByName('questionHue');
                for (i = 0; i < ele.length; i++) {
                    if (ele[i].checked)
                        var question4 = ele[i].value;
                }
                if (question4 == 1) {
                    paso = 58;
                    step = 16;
                    previousStep = 66;
                    $("#nameStep").text("Seleccionar Huella");
                    var select = document.getElementById("selectFootprint");
                    for (index in selectHuella) {
                        select.options[select.options.length] = new Option(selectHuella[index].title, selectHuella[index].nid);
                    }
                    current_fs = $(this).parent();
                    var idStep = $('#footprint');
                    confStep(idStep, current_fs);
                    //viene de la funcion existingCatalog del paso 51
                } else {
                    var caudal = $("#selectstreamflow").val();
                    var sedimentos = $("#selectSediments").val();

                    if (sedimentos == 0) {
                        $("#sedimentos").attr('disabled', 'disabled');
                        $("#sedimentos").prop("checked", false);
                    } else {
                        $("#sedimentos").prop("checked", true);
                    }

                    if (caudal == 0) {
                        $("#dor").attr('disabled', 'disabled');
                        $("#dorw").attr('disabled', 'disabled');
                        $("#dor").prop("checked", false);
                        $("#dorw").prop("checked", false);
                        $("#fragmentacion").attr('disabled', 'disabled');
                        $("#fragmentacion").prop("checked", true);
                    } else {
                        $("#dor").prop("checked", true);
                        $("#dorw").prop("checked", true);
                        $("#fragmentacion").attr('disabled', 'disabled');
                        $("#fragmentacion").prop("checked", true);
                    }
                    $("#huella").css("display", "none");
                    $("#huellaLabel").css("display", "none");
                    $(".contForm").css("height", "460px");
                    $(".conter").css("height", "630px");

                    step = 16;
                    previousStep = 66;
                    $("#nameStep").text("Seleccionar Modelos");
                    current_fs = $(this).parent();
                    var idStep = $('#model');
                    confStep(idStep, current_fs);
                    //viene de la funcion existingCatalog del paso 51
                }
            } else if (paso == 59) {
                var caudal = $("#selectstreamflow").val();
                var sedimentos = $("#selectSediments").val();
                checkedFootprint = $(".check4:checked").length;
                if (checkedFootprint != 0) {
                    $("#huella").css("display", "inline-block");
                    $("#huellaLabel").css("display", "inline-block");
                    $("#huella").prop("checked", true);
                    if (sedimentos == 0) {
                        $("#sedimentos").attr('disabled', 'disabled');
                        $("#sedimentos").prop("checked", false);
                    } else {
                        $("#sedimentos").prop("checked", true);
                    }

                    if (caudal == 0) {
                        $("#dor").attr('disabled', 'disabled');
                        $("#dorw").attr('disabled', 'disabled');
                        $("#dor").prop("checked", false);
                        $("#dorw").prop("checked", false);
                        $("#fragmentacion").attr('disabled', 'disabled');
                        $("#fragmentacion").prop("checked", true);
                    } else {
                        $("#dor").prop("checked", true);
                        $("#dorw").prop("checked", true);
                        $("#fragmentacion").attr('disabled', 'disabled');
                        $("#fragmentacion").prop("checked", true);
                    }
                    $(".contForm").css("height", "460px");
                    $(".conter").css("height", "630px");

                    step = 16;
                    previousStep = 67;
                    $("#nameStep").text("Seleccionar Huella");
                    current_fs = $(this).parent();
                    var idStep = $('#model');
                    confStep(idStep, current_fs);
                    //viene de si del paso 58
                } else {
                    alert("Por favor ingrese una capa");
                    paso = 58;
                    step = 16;
                    previousStep = 66;
                }
            } else if (paso == 60) {
                var ele = document.getElementsByName('questionHue');
                for (i = 0; i < ele.length; i++) {
                    if (ele[i].checked)
                        var question4 = ele[i].value;
                }
                if (question4 == 1) {
                    paso = 60;
                    step = 16;
                    previousStep = 68;
                    $("#nameStep").text("Seleccionar Huella");
                    var select = document.getElementById("selectFootprint");
                    for (index in selectHuella) {
                        select.options[select.options.length] = new Option(selectHuella[index].title, selectHuella[index].nid);
                    }
                    current_fs = $(this).parent();
                    var idStep = $('#footprint');
                    confStep(idStep, current_fs);
                    //viene de la funcion existingCatalog del paso 40
                } else {
                    var caudal = $("#selectstreamflow").val();
                    var sedimentos = $("#selectSediments").val();

                    if (sedimentos == 0) {
                        $("#sedimentos").attr('disabled', 'disabled');
                        $("#sedimentos").prop("checked", false);
                    } else {
                        $("#sedimentos").prop("checked", true);
                    }

                    if (caudal == 0) {
                        $("#dor").attr('disabled', 'disabled');
                        $("#dorw").attr('disabled', 'disabled');
                        $("#dor").prop("checked", false);
                        $("#dorw").prop("checked", false);
                        $("#fragmentacion").attr('disabled', 'disabled');
                        $("#fragmentacion").prop("checked", true);

                    } else {
                        $("#dor").prop("checked", true);
                        $("#dorw").prop("checked", true);
                        $("#fragmentacion").attr('disabled', 'disabled');
                        $("#fragmentacion").prop("checked", true);
                    }
                    $("#huella").css("display", "none");
                    $("#huellaLabel").css("display", "none");
                    $(".contForm").css("height", "460px");
                    $(".conter").css("height", "630px");
                    step = 16;
                    previousStep = 68;
                    $("#nameStep").text("Seleccionar Modelos");
                    current_fs = $(this).parent();
                    var idStep = $('#model');
                    confStep(idStep, current_fs);
                    //viene de la funcion existingCatalog del paso 40
                }
            } else if (paso == 61) {
                var caudal = $("#selectstreamflow").val();
                var sedimentos = $("#selectSediments").val();
                checkedFootprint = $(".check4:checked").length;
                if (checkedFootprint != 0) {
                    $("#huella").css("display", "inline-block");
                    $("#huellaLabel").css("display", "inline-block");
                    $("#huella").prop("checked", true);
                    if (sedimentos == 0) {
                        $("#sedimentos").attr('disabled', 'disabled');
                        $("#sedimentos").prop("checked", false);
                    } else {
                        $("#sedimentos").prop("checked", true);
                    }

                    if (caudal == 0) {
                        $("#dor").attr('disabled', 'disabled');
                        $("#dorw").attr('disabled', 'disabled');
                        $("#dor").prop("checked", false);
                        $("#dorw").prop("checked", false);
                        $("#fragmentacion").attr('disabled', 'disabled');
                        $("#fragmentacion").prop("checked", true);
                    } else {
                        $("#dor").prop("checked", true);
                        $("#dorw").prop("checked", true);
                        $("#fragmentacion").attr('disabled', 'disabled');
                        $("#fragmentacion").prop("checked", true);
                    }
                    $(".contForm").css("height", "460px");
                    $(".conter").css("height", "630px");
                    step = 16;
                    previousStep = 69;
                    $("#nameStep").text("Seleccionar Huella");
                    current_fs = $(this).parent();
                    var idStep = $('#model');
                    confStep(idStep, current_fs);
                    //viene de si del paso 60
                    //viene de si del paso 58
                } else {
                    alert("Por favor ingrese una capa");
                    paso = 60;
                    step = 16;
                    previousStep = 68;

                }
            } else if (paso == 62) {
                var ele = document.getElementsByName('questionHue');
                for (i = 0; i < ele.length; i++) {
                    if (ele[i].checked)
                        var question4 = ele[i].value;
                }
                if (question4 == 1) {
                    paso = 62;
                    step = 16;
                    previousStep = 70;
                    $("#nameStep").text("Seleccionar Huella");
                    var select = document.getElementById("selectFootprint");
                    for (index in selectHuella) {
                        select.options[select.options.length] = new Option(selectHuella[index].title, selectHuella[index].nid);
                    }
                    current_fs = $(this).parent();
                    var idStep = $('#footprint');
                    confStep(idStep, current_fs);
                    //viene de la funcion existingCatalog del paso 39
                } else {
                    var caudal = $("#selectstreamflow").val();
                    var sedimentos = $("#selectSediments").val();

                    if (sedimentos == 0) {
                        $("#sedimentos").attr('disabled', 'disabled');
                        $("#sedimentos").prop("checked", false);
                    } else {
                        $("#sedimentos").prop("checked", true);
                    }

                    if (caudal == 0) {
                        $("#dor").attr('disabled', 'disabled');
                        $("#dorw").attr('disabled', 'disabled');
                        $("#dor").prop("checked", false);
                        $("#dorw").prop("checked", false);
                        $("#fragmentacion").attr('disabled', 'disabled');
                        $("#fragmentacion").prop("checked", true);
                    } else {
                        $("#dor").prop("checked", true);
                        $("#dorw").prop("checked", true);
                        $("#fragmentacion").attr('disabled', 'disabled');
                        $("#fragmentacion").prop("checked", true);
                    }
                    $("#huella").css("display", "none");
                    $("#huellaLabel").css("display", "none");
                    $(".contForm").css("height", "460px");
                    $(".conter").css("height", "630px");

                    step = 16;
                    previousStep = 70;
                    $("#nameStep").text("Seleccionar Modelos");
                    current_fs = $(this).parent();
                    var idStep = $('#model');
                    confStep(idStep, current_fs);
                    //viene de la funcion existingCatalog del paso 39
                }
            } else if (paso == 63) {
                var caudal = $("#selectstreamflow").val();
                var sedimentos = $("#selectSediments").val();
                checkedFootprint = $(".check4:checked").length;
                if (checkedFootprint != 0) {
                    $("#huella").css("display", "inline-block");
                    $("#huellaLabel").css("display", "inline-block");
                    $("#huella").prop("checked", true);
                    if (sedimentos == 0) {
                        $("#sedimentos").attr('disabled', 'disabled');
                        $("#sedimentos").prop("checked", false);
                    } else {
                        $("#sedimentos").prop("checked", true);
                    }

                    if (caudal == 0) {
                        $("#dor").attr('disabled', 'disabled');
                        $("#dorw").attr('disabled', 'disabled');
                        $("#dor").prop("checked", false);
                        $("#dorw").prop("checked", false);
                        $("#fragmentacion").attr('disabled', 'disabled');
                        $("#fragmentacion").prop("checked", true);
                    } else {
                        $("#dor").prop("checked", true);
                        $("#dorw").prop("checked", true);
                        $("#fragmentacion").attr('disabled', 'disabled');
                        $("#fragmentacion").prop("checked", true);
                    }
                    $(".contForm").css("height", "460px");
                    $(".conter").css("height", "630px");
                    step = 16;
                    previousStep = 71;
                    $("#nameStep").text("Seleccionar Huella");
                    current_fs = $(this).parent();
                    var idStep = $('#model');
                    confStep(idStep, current_fs);
                    //viene de si del paso 62
                } else {
                    alert("Por favor ingrese una capa");
                    paso = 62;
                    step = 16;
                    previousStep = 70;
                }
            } else if (paso == 64) {
                var ele = document.getElementsByName('questionHue');
                for (i = 0; i < ele.length; i++) {
                    if (ele[i].checked)
                        var question4 = ele[i].value;
                }
                if (question4 == 1) {
                    paso = 64;
                    step = 16;
                    previousStep = 72;
                    $("#nameStep").text("Seleccionar Huella");
                    var select = document.getElementById("selectFootprint");
                    for (index in selectHuella) {
                        select.options[select.options.length] = new Option(selectHuella[index].title, selectHuella[index].nid);
                    }
                    current_fs = $(this).parent();
                    var idStep = $('#footprint');
                    confStep(idStep, current_fs);
                    //viene de la funcion existingCatalog del paso 25
                } else {
                    var valorPrecipitacion = $("#selectPrecipitation").val();
                    var valorEvapotranspiracion = $("#selectEvapotranspiracion").val();
                    var sedimentos = $("#selectSediments").val();

                    if (sedimentos == 0) {
                        $("#sedimentos").attr('disabled', 'disabled');
                        $("#sedimentos").prop("checked", false);
                    } else {
                        $("#sedimentos").prop("checked", true);
                    }

                    if (valorPrecipitacion == 0 || valorEvapotranspiracion == 0) {
                        $("#dor").attr('disabled', 'disabled');
                        $("#dorw").attr('disabled', 'disabled');
                        $("#dor").prop("checked", false);
                        $("#dorw").prop("checked", false);
                        $("#fragmentacion").attr('disabled', 'disabled');
                        $("#fragmentacion").prop("checked", true);

                    } else {
                        $("#dor").prop("checked", true);
                        $("#dorw").prop("checked", true);
                        $("#fragmentacion").attr('disabled', 'disabled');
                        $("#fragmentacion").prop("checked", true);
                    }
                    $("#huella").css("display", "none");
                    $("#huellaLabel").css("display", "none");
                    $(".contForm").css("height", "460px");
                    $(".conter").css("height", "630px");
                    step = 16;
                    previousStep = 72;
                    $("#nameStep").text("Seleccionar Modelos");
                    current_fs = $(this).parent();
                    var idStep = $('#model');
                    confStep(idStep, current_fs);
                    //viene de la funcion existingCatalog del paso 25
                }
            } else if (paso == 65) {
                var valorPrecipitacion = $("#selectPrecipitation").val();
                var valorEvapotranspiracion = $("#selectEvapotranspiracion").val();
                var sedimentos = $("#selectSediments").val();

                checkedFootprint = $(".check4:checked").length;
                if (checkedFootprint != 0) {
                    $("#huella").css("display", "inline-block");
                    $("#huellaLabel").css("display", "inline-block");
                    $("#huella").prop("checked", true);
                    if (sedimentos == 0) {
                        $("#sedimentos").attr('disabled', 'disabled');
                        $("#sedimentos").prop("checked", false);
                    } else {
                        $("#sedimentos").prop("checked", true);
                    }

                    if (valorPrecipitacion == 0 || valorEvapotranspiracion == 0) {
                        $("#dor").attr('disabled', 'disabled');
                        $("#dorw").attr('disabled', 'disabled');
                        $("#dor").prop("checked", false);
                        $("#dorw").prop("checked", false);
                        $("#fragmentacion").attr('disabled', 'disabled');
                        $("#fragmentacion").prop("checked", true);
                    } else {
                        $("#dor").prop("checked", true);
                        $("#dorw").prop("checked", true);
                        $("#fragmentacion").attr('disabled', 'disabled');
                        $("#fragmentacion").prop("checked", true);
                    }
                    $(".contForm").css("height", "460px");
                    $(".conter").css("height", "630px");
                    step = 16;
                    previousStep = 73;
                    $("#nameStep").text("Seleccionar Huella");
                    current_fs = $(this).parent();
                    var idStep = $('#model');
                    confStep(idStep, current_fs);
                    //viene de si del paso 64
                } else {
                    alert("Por favor ingrese una capa");
                    paso = 64;
                    step = 16;
                    previousStep = 72;
                }
            } else if (paso == 66) {
                var ele = document.getElementsByName('questionHue');
                for (i = 0; i < ele.length; i++) {
                    if (ele[i].checked)
                        var question4 = ele[i].value;
                }
                if (question4 == 1) {
                    paso = 66;
                    step = 16;
                    previousStep = 74;
                    $("#nameStep").text("Seleccionar Huella");
                    var select = document.getElementById("selectFootprint");
                    for (index in selectHuella) {
                        select.options[select.options.length] = new Option(selectHuella[index].title, selectHuella[index].nid);
                    }
                    current_fs = $(this).parent();
                    var idStep = $('#footprint');
                    confStep(idStep, current_fs);
                    //viene de la funcion existingCatalog del paso 53
                } else {
                    var valorPrecipitacion = $("#selectPrecipitation").val();
                    var valorEvapotranspiracion = $("#selectEvapotranspiracion").val();
                    var sedimentos = $("#selectSediments").val();

                    if (sedimentos == 0) {
                        $("#sedimentos").attr('disabled', 'disabled');
                        $("#sedimentos").prop("checked", false);
                    } else {
                        $("#sedimentos").prop("checked", true);
                    }

                    if (valorPrecipitacion == 0 || valorEvapotranspiracion == 0) {
                        $("#dor").attr('disabled', 'disabled');
                        $("#dorw").attr('disabled', 'disabled');
                        $("#dor").prop("checked", false);
                        $("#dorw").prop("checked", false);
                        $("#fragmentacion").attr('disabled', 'disabled');
                        $("#fragmentacion").prop("checked", true);

                    } else {
                        $("#dor").prop("checked", true);
                        $("#dorw").prop("checked", true);
                        $("#fragmentacion").attr('disabled', 'disabled');
                        $("#fragmentacion").prop("checked", true);
                    }
                    $("#huella").css("display", "none");
                    $("#huellaLabel").css("display", "none");
                    $(".contForm").css("height", "460px");
                    $(".conter").css("height", "630px");
                    step = 16;
                    previousStep = 74;
                    $("#nameStep").text("Seleccionar Modelo");
                    current_fs = $(this).parent();
                    var idStep = $('#model');
                    confStep(idStep, current_fs);
                    //viene de la funcion existingCatalog del paso 53
                }
            } else if (paso == 67) {
                var valorPrecipitacion = $("#selectPrecipitation").val();
                var valorEvapotranspiracion = $("#selectEvapotranspiracion").val();
                var sedimentos = $("#selectSediments").val();

                checkedFootprint = $(".check4:checked").length;
                if (checkedFootprint != 0) {
                    $("#huella").css("display", "inline-block");
                    $("#huellaLabel").css("display", "inline-block");
                    $("#huella").prop("checked", true);
                    if (sedimentos == 0) {
                        $("#sedimentos").attr('disabled', 'disabled');
                        $("#sedimentos").prop("checked", false);
                    } else {
                        $("#sedimentos").prop("checked", true);
                    }

                    if (valorPrecipitacion == 0 || valorEvapotranspiracion == 0) {
                        $("#dor").attr('disabled', 'disabled');
                        $("#dorw").attr('disabled', 'disabled');
                        $("#dor").prop("checked", false);
                        $("#dorw").prop("checked", false);
                        $("#fragmentacion").attr('disabled', 'disabled');
                        $("#fragmentacion").prop("checked", true);
                    } else {
                        $("#dor").prop("checked", true);
                        $("#dorw").prop("checked", true);
                        $("#fragmentacion").attr('disabled', 'disabled');
                        $("#fragmentacion").prop("checked", true);
                    }
                    $(".contForm").css("height", "460px");
                    $(".conter").css("height", "630px");
                    step = 16;
                    previousStep = 75;
                    $("#nameStep").text("Seleccionar Huella");
                    current_fs = $(this).parent();
                    var idStep = $('#model');
                    confStep(idStep, current_fs);
                    //viene de si del paso 66
                } else {
                    alert("Por favor ingrese una capa");
                    paso = 66;
                    step = 16;
                    previousStep = 74;
                }
            } else if (paso == 68) {
                var ele = document.getElementsByName('questionHue');
                for (i = 0; i < ele.length; i++) {
                    if (ele[i].checked)
                        var question4 = ele[i].value;
                }
                if (question4 == 1) {
                    paso = 68;
                    step = 16;
                    previousStep = 76;
                    $("#nameStep").text("Seleccionar Huella");
                    var select = document.getElementById("selectFootprint");
                    for (index in selectHuella) {
                        select.options[select.options.length] = new Option(selectHuella[index].title, selectHuella[index].nid);
                    }
                    current_fs = $(this).parent();
                    var idStep = $('#footprint');
                    confStep(idStep, current_fs);
                    //viene de la funcion existingCatalog del paso 44
                } else {
                    var valorPrecipitacion = $("#selectPrecipitation").val();
                    var valorEvapotranspiracion = $("#selectEvapotranspiracion").val();
                    var sedimentos = $("#selectSediments").val();

                    if (sedimentos == 0) {
                        $("#sedimentos").attr('disabled', 'disabled');
                        $("#sedimentos").prop("checked", false);
                    } else {
                        $("#sedimentos").prop("checked", true);
                    }

                    if (valorPrecipitacion == 0 || valorEvapotranspiracion == 0) {
                        $("#dor").attr('disabled', 'disabled');
                        $("#dorw").attr('disabled', 'disabled');
                        $("#dor").prop("checked", false);
                        $("#dorw").prop("checked", false);
                        $("#fragmentacion").attr('disabled', 'disabled');
                        $("#fragmentacion").prop("checked", true);

                    } else {
                        $("#dor").prop("checked", true);
                        $("#dorw").prop("checked", true);
                        $("#fragmentacion").attr('disabled', 'disabled');
                        $("#fragmentacion").prop("checked", true);
                    }
                    $("#huella").css("display", "none");
                    $("#huellaLabel").css("display", "none");
                    $(".contForm").css("height", "460px");
                    $(".conter").css("height", "630px");
                    step = 16;
                    previousStep = 76;
                    $("#nameStep").text("Seleccionar Modelos");
                    current_fs = $(this).parent();
                    var idStep = $('#model');
                    confStep(idStep, current_fs);
                    //viene de la funcion existingCatalog del paso 44
                }
            } else if (paso == 69) {
                var valorPrecipitacion = $("#selectPrecipitation").val();
                var valorEvapotranspiracion = $("#selectEvapotranspiracion").val();
                var sedimentos = $("#selectSediments").val();

                checkedFootprint = $(".check4:checked").length;
                if (checkedFootprint != 0) {
                    $("#huella").css("display", "inline-block");
                    $("#huellaLabel").css("display", "inline-block");
                    $("#huella").prop("checked", true);
                    if (sedimentos == 0) {
                        $("#sedimentos").attr('disabled', 'disabled');
                        $("#sedimentos").prop("checked", false);
                    } else {
                        $("#sedimentos").prop("checked", true);
                    }

                    if (valorPrecipitacion == 0 || valorEvapotranspiracion == 0) {
                        $("#dor").attr('disabled', 'disabled');
                        $("#dorw").attr('disabled', 'disabled');
                        $("#dor").prop("checked", false);
                        $("#dorw").prop("checked", false);
                        $("#fragmentacion").attr('disabled', 'disabled');
                        $("#fragmentacion").prop("checked", true);
                    } else {
                        $("#dor").prop("checked", true);
                        $("#dorw").prop("checked", true);
                        $("#fragmentacion").attr('disabled', 'disabled');
                        $("#fragmentacion").prop("checked", true);
                    }
                    $(".contForm").css("height", "460px");
                    $(".conter").css("height", "630px");
                    step = 16;
                    previousStep = 77;
                    $("#nameStep").text("Seleccionar Huella");
                    current_fs = $(this).parent();
                    var idStep = $('#model');
                    confStep(idStep, current_fs);
                    //viene de si del paso 68
                } else {
                    alert("Por favor ingrese una capa");
                    paso = 68;
                    step = 16;
                    previousStep = 76;
                }
            } else if (paso == 70) {
                var ele = document.getElementsByName('questionHue');
                for (i = 0; i < ele.length; i++) {
                    if (ele[i].checked)
                        var question4 = ele[i].value;
                }
                if (question4 == 1) {
                    paso = 70;
                    step = 16;
                    previousStep = 78;
                    $("#nameStep").text("Seleccionar Huella");
                    var select = document.getElementById("selectFootprint");
                    for (index in selectHuella) {
                        select.options[select.options.length] = new Option(selectHuella[index].title, selectHuella[index].nid);
                    }
                    current_fs = $(this).parent();
                    var idStep = $('#footprint');
                    confStep(idStep, current_fs);
                    //viene de la funcion existingCatalog del paso 43
                } else {
                    var valorPrecipitacion = $("#selectPrecipitation").val();
                    var valorEvapotranspiracion = $("#selectEvapotranspiracion").val();
                    var sedimentos = $("#selectSediments").val();

                    if (sedimentos == 0) {
                        $("#sedimentos").attr('disabled', 'disabled');
                        $("#sedimentos").prop("checked", false);
                    } else {
                        $("#sedimentos").prop("checked", true);
                    }

                    if (valorPrecipitacion == 0 || valorEvapotranspiracion == 0) {
                        $("#dor").attr('disabled', 'disabled');
                        $("#dorw").attr('disabled', 'disabled');
                        $("#dor").prop("checked", false);
                        $("#dorw").prop("checked", false);
                        $("#fragmentacion").attr('disabled', 'disabled');
                        $("#fragmentacion").prop("checked", true);

                    } else {
                        $("#dor").prop("checked", true);
                        $("#dorw").prop("checked", true);
                        $("#fragmentacion").attr('disabled', 'disabled');
                        $("#fragmentacion").prop("checked", true);
                    }
                    $("#huella").css("display", "none");
                    $("#huellaLabel").css("display", "none");
                    $(".contForm").css("height", "460px");
                    $(".conter").css("height", "630px");
                    step = 16;
                    previousStep = 76;
                    $("#nameStep").text("Seleccionar Modelos");
                    current_fs = $(this).parent();
                    var idStep = $('#model');
                    confStep(idStep, current_fs);
                    //viene de la funcion existingCatalog del paso 43
                }
            } else if (paso == 71) {
                var valorPrecipitacion = $("#selectPrecipitation").val();
                var valorEvapotranspiracion = $("#selectEvapotranspiracion").val();
                var sedimentos = $("#selectSediments").val();

                checkedFootprint = $(".check4:checked").length;
                if (checkedFootprint != 0) {
                    $("#huella").css("display", "inline-block");
                    $("#huellaLabel").css("display", "inline-block");
                    $("#huella").prop("checked", true);
                    if (sedimentos == 0) {
                        $("#sedimentos").attr('disabled', 'disabled');
                        $("#sedimentos").prop("checked", false);
                    } else {
                        $("#sedimentos").prop("checked", true);
                    }

                    if (valorPrecipitacion == 0 || valorEvapotranspiracion == 0) {
                        $("#dor").attr('disabled', 'disabled');
                        $("#dorw").attr('disabled', 'disabled');
                        $("#dor").prop("checked", false);
                        $("#dorw").prop("checked", false);
                        $("#fragmentacion").attr('disabled', 'disabled');
                        $("#fragmentacion").prop("checked", true);
                    } else {
                        $("#dor").prop("checked", true);
                        $("#dorw").prop("checked", true);
                        $("#fragmentacion").attr('disabled', 'disabled');
                        $("#fragmentacion").prop("checked", true);
                    }
                    $(".contForm").css("height", "460px");
                    $(".conter").css("height", "630px");
                    step = 16;
                    previousStep = 79;
                    $("#nameStep").text("Seleccionar Huella");
                    current_fs = $(this).parent();
                    var idStep = $('#model');
                    confStep(idStep, current_fs);
                    //viene de si del paso 70
                } else {
                    alert("Por favor ingrese una capa");
                    paso = 70;
                    step = 16;
                    previousStep = 78;
                }
            } else if (paso == 72) {
                var ele = document.getElementsByName('questionHue');
                for (i = 0; i < ele.length; i++) {
                    if (ele[i].checked)
                        var question4 = ele[i].value;
                }
                if (question4 == 1) {
                    paso = 72;
                    step = 16;
                    previousStep = 80;
                    $("#nameStep").text("Seleccionar Huella");
                    var select = document.getElementById("selectFootprint");
                    for (index in selectHuella) {
                        select.options[select.options.length] = new Option(selectHuella[index].title, selectHuella[index].nid);
                    }
                    current_fs = $(this).parent();
                    var idStep = $('#footprint');
                    confStep(idStep, current_fs);
                    //viene de la funcion existingCatalog del paso 26
                } else {
                    var valorPrecipitacion = $("#selectPrecipitation").val();
                    var valorTemperatura = $("#selectTemperature").val();
                    var sedimentos = $("#selectSediments").val();

                    if (sedimentos == 0) {
                        $("#sedimentos").attr('disabled', 'disabled');
                        $("#sedimentos").prop("checked", false);
                    } else {
                        $("#sedimentos").prop("checked", true);
                    }

                    if (valorPrecipitacion == 0 || valorTemperatura == 0) {
                        $("#dor").attr('disabled', 'disabled');
                        $("#dorw").attr('disabled', 'disabled');
                        $("#dor").prop("checked", false);
                        $("#dorw").prop("checked", false);
                        $("#fragmentacion").attr('disabled', 'disabled');
                        $("#fragmentacion").prop("checked", true);

                    } else {
                        $("#dor").prop("checked", true);
                        $("#dorw").prop("checked", true);
                        $("#fragmentacion").attr('disabled', 'disabled');
                        $("#fragmentacion").prop("checked", true);
                    }
                    $("#huella").css("display", "none");
                    $("#huellaLabel").css("display", "none");
                    $(".contForm").css("height", "460px");
                    $(".conter").css("height", "630px");
                    step = 16;
                    previousStep = 80;
                    $("#nameStep").text("Seleccionar Modelos");
                    current_fs = $(this).parent();
                    var idStep = $('#model');
                    confStep(idStep, current_fs);
                    //viene de la funcion existingCatalog del paso 26
                }
            } else if (paso == 73) {
                var valorPrecipitacion = $("#selectPrecipitation").val();
                var valorTemperatura = $("#selectTemperature").val();
                var sedimentos = $("#selectSediments").val();

                checkedFootprint = $(".check4:checked").length;
                if (checkedFootprint != 0) {
                    $("#huella").css("display", "inline-block");
                    $("#huellaLabel").css("display", "inline-block");
                    $("#huella").prop("checked", true);
                    if (sedimentos == 0) {
                        $("#sedimentos").attr('disabled', 'disabled');
                        $("#sedimentos").prop("checked", false);
                    } else {
                        $("#sedimentos").prop("checked", true);
                    }

                    if (valorPrecipitacion == 0 || valorTemperatura == 0) {
                        $("#dor").attr('disabled', 'disabled');
                        $("#dorw").attr('disabled', 'disabled');
                        $("#dor").prop("checked", false);
                        $("#dorw").prop("checked", false);
                        $("#fragmentacion").attr('disabled', 'disabled');
                        $("#fragmentacion").prop("checked", true);
                    } else {
                        $("#dor").prop("checked", true);
                        $("#dorw").prop("checked", true);
                        $("#fragmentacion").attr('disabled', 'disabled');
                        $("#fragmentacion").prop("checked", true);
                    }
                    $(".contForm").css("height", "460px");
                    $(".conter").css("height", "630px");
                    step = 16;
                    previousStep = 81;
                    $("#nameStep").text("Seleccionar Huella");
                    current_fs = $(this).parent();
                    var idStep = $('#model');
                    confStep(idStep, current_fs);
                    //viene de si del paso 72
                } else {
                    alert("Por favor ingrese una capa");
                    paso = 72;
                    step = 16;
                    previousStep = 80;
                }
            } else if (paso == 74) {
                var ele = document.getElementsByName('questionHue');
                for (i = 0; i < ele.length; i++) {
                    if (ele[i].checked)
                        var question4 = ele[i].value;
                }
                if (question4 == 1) {
                    paso = 74;
                    step = 16;
                    previousStep = 82;
                    $("#nameStep").text("Seleccionar Huella");
                    var select = document.getElementById("selectFootprint");
                    for (index in selectHuella) {
                        select.options[select.options.length] = new Option(selectHuella[index].title, selectHuella[index].nid);
                    }
                    current_fs = $(this).parent();
                    var idStep = $('#footprint');
                    confStep(idStep, current_fs);
                    //viene de la funcion existingCatalog del paso 55
                } else {
                    var valorPrecipitacion = $("#selectPrecipitation").val();
                    var valorTemperatura = $("#selectTemperature").val();
                    var sedimentos = $("#selectSediments").val();

                    if (sedimentos == 0) {
                        $("#sedimentos").attr('disabled', 'disabled');
                        $("#sedimentos").prop("checked", false);
                    } else {
                        $("#sedimentos").prop("checked", true);
                    }

                    if (valorPrecipitacion == 0 || valorTemperatura == 0) {
                        $("#dor").attr('disabled', 'disabled');
                        $("#dorw").attr('disabled', 'disabled');
                        $("#dor").prop("checked", false);
                        $("#dorw").prop("checked", false);
                        $("#fragmentacion").attr('disabled', 'disabled');
                        $("#fragmentacion").prop("checked", true);

                    } else {
                        $("#dor").prop("checked", true);
                        $("#dorw").prop("checked", true);
                        $("#fragmentacion").attr('disabled', 'disabled');
                        $("#fragmentacion").prop("checked", true);
                    }
                    $("#huella").css("display", "none");
                    $("#huellaLabel").css("display", "none");
                    $(".contForm").css("height", "460px");
                    $(".conter").css("height", "630px");
                    step = 16;
                    previousStep = 82;
                    $("#nameStep").text("Seleccionar Modelos");
                    current_fs = $(this).parent();
                    var idStep = $('#model');
                    confStep(idStep, current_fs);
                    //viene de la funcion existingCatalog del paso 55
                }
            } else if (paso == 75) {
                var valorPrecipitacion = $("#selectPrecipitation").val();
                var valorTemperatura = $("#selectTemperature").val();
                var sedimentos = $("#selectSediments").val();

                checkedFootprint = $(".check4:checked").length;
                if (checkedFootprint != 0) {
                    $("#huella").css("display", "inline-block");
                    $("#huellaLabel").css("display", "inline-block");
                    $("#huella").prop("checked", true);
                    if (sedimentos == 0) {
                        $("#sedimentos").attr('disabled', 'disabled');
                        $("#sedimentos").prop("checked", false);
                    } else {
                        $("#sedimentos").prop("checked", true);
                    }

                    if (valorPrecipitacion == 0 || valorTemperatura == 0) {
                        $("#dor").attr('disabled', 'disabled');
                        $("#dorw").attr('disabled', 'disabled');
                        $("#dor").prop("checked", false);
                        $("#dorw").prop("checked", false);
                        $("#fragmentacion").attr('disabled', 'disabled');
                        $("#fragmentacion").prop("checked", true);
                    } else {
                        $("#dor").prop("checked", true);
                        $("#dorw").prop("checked", true);
                        $("#fragmentacion").attr('disabled', 'disabled');
                        $("#fragmentacion").prop("checked", true);
                    }
                    $(".contForm").css("height", "460px");
                    $(".conter").css("height", "630px");
                    step = 16;
                    previousStep = 83;
                    $("#nameStep").text("Seleccionar Huella");
                    current_fs = $(this).parent();
                    var idStep = $('#model');
                    confStep(idStep, current_fs);
                    //viene de si del paso 74
                } else {
                    alert("Por favor ingrese una capa");
                    paso = 74;
                    step = 16;
                    previousStep = 82;
                }
            } else if (paso == 76) {
                var ele = document.getElementsByName('questionHue');
                for (i = 0; i < ele.length; i++) {
                    if (ele[i].checked)
                        var question4 = ele[i].value;
                }
                if (question4 == 1) {
                    paso = 76;
                    step = 16;
                    previousStep = 84;
                    $("#nameStep").text("Seleccionar Huella");
                    var select = document.getElementById("selectFootprint");
                    for (index in selectHuella) {
                        select.options[select.options.length] = new Option(selectHuella[index].title, selectHuella[index].nid);
                    }
                    current_fs = $(this).parent();
                    var idStep = $('#footprint');
                    confStep(idStep, current_fs);
                    //viene de la funcion existingCatalog del paso 48
                } else {
                    var valorPrecipitacion = $("#selectPrecipitation").val();
                    var valorTemperatura = $("#selectTemperature").val();
                    var sedimentos = $("#selectSediments").val();

                    if (sedimentos == 0) {
                        $("#sedimentos").attr('disabled', 'disabled');
                        $("#sedimentos").prop("checked", false);
                    } else {
                        $("#sedimentos").prop("checked", true);
                    }

                    if (valorPrecipitacion == 0 || valorTemperatura == 0) {
                        $("#dor").attr('disabled', 'disabled');
                        $("#dorw").attr('disabled', 'disabled');
                        $("#dor").prop("checked", false);
                        $("#dorw").prop("checked", false);
                        $("#fragmentacion").attr('disabled', 'disabled');
                        $("#fragmentacion").prop("checked", true);

                    } else {
                        $("#dor").prop("checked", true);
                        $("#dorw").prop("checked", true);
                        $("#fragmentacion").attr('disabled', 'disabled');
                        $("#fragmentacion").prop("checked", true);
                    }
                    $("#huella").css("display", "none");
                    $("#huellaLabel").css("display", "none");
                    $(".contForm").css("height", "460px");
                    $(".conter").css("height", "630px");
                    step = 16;
                    previousStep = 84;
                    $("#nameStep").text("Seleccionar Modelos");
                    current_fs = $(this).parent();
                    var idStep = $('#model');
                    confStep(idStep, current_fs);
                    //viene de la funcion existingCatalog del paso 48
                }
            } else if (paso == 77) {
                var valorPrecipitacion = $("#selectPrecipitation").val();
                var valorTemperatura = $("#selectTemperature").val();
                var sedimentos = $("#selectSediments").val();

                checkedFootprint = $(".check4:checked").length;
                if (checkedFootprint != 0) {
                    $("#huella").css("display", "inline-block");
                    $("#huellaLabel").css("display", "inline-block");
                    $("#huella").prop("checked", true);
                    if (sedimentos == 0) {
                        $("#sedimentos").attr('disabled', 'disabled');
                        $("#sedimentos").prop("checked", false);
                    } else {
                        $("#sedimentos").prop("checked", true);
                    }

                    if (valorPrecipitacion == 0 || valorTemperatura == 0) {
                        $("#dor").attr('disabled', 'disabled');
                        $("#dorw").attr('disabled', 'disabled');
                        $("#dor").prop("checked", false);
                        $("#dorw").prop("checked", false);
                        $("#fragmentacion").attr('disabled', 'disabled');
                        $("#fragmentacion").prop("checked", true);
                    } else {
                        $("#dor").prop("checked", true);
                        $("#dorw").prop("checked", true);
                        $("#fragmentacion").attr('disabled', 'disabled');
                        $("#fragmentacion").prop("checked", true);
                    }
                    $(".contForm").css("height", "460px");
                    $(".conter").css("height", "630px");
                    step = 16;
                    previousStep = 85;
                    $("#nameStep").text("Seleccionar Huella");
                    current_fs = $(this).parent();
                    var idStep = $('#model');
                    confStep(idStep, current_fs);
                    //viene de si del paso 76
                } else {
                    alert("Por favor ingrese una capa");
                    paso = 76;
                    step = 16;
                    previousStep = 84;
                }
            } else if (paso == 78) {
                var ele = document.getElementsByName('questionHue');
                for (i = 0; i < ele.length; i++) {
                    if (ele[i].checked)
                        var question4 = ele[i].value;
                }
                if (question4 == 1) {
                    paso = 78;
                    step = 16;
                    previousStep = 86;
                    $("#nameStep").text("Seleccionar Huella");
                    var select = document.getElementById("selectFootprint");
                    for (index in selectHuella) {
                        select.options[select.options.length] = new Option(selectHuella[index].title, selectHuella[index].nid);
                    }
                    current_fs = $(this).parent();
                    var idStep = $('#footprint');
                    confStep(idStep, current_fs);
                    //viene de la funcion existingCatalog del paso 47
                } else {
                    var valorPrecipitacion = $("#selectPrecipitation").val();
                    var valorTemperatura = $("#selectTemperature").val();
                    var sedimentos = $("#selectSediments").val();

                    $("#huella").css("display", "none");
                    $("#huellaLabel").css("display", "none");
                    $(".contForm").css("height", "460px");
                    $(".conter").css("height", "630px");
                    step = 16;
                    previousStep = 84;
                    $("#nameStep").text("Seleccionar Modelos");
                    if (sedimentos == 0) {
                        $("#sedimentos").attr('disabled', 'disabled');
                        $("#sedimentos").prop("checked", false);
                    } else {
                        $("#sedimentos").prop("checked", true);
                    }

                    if (valorPrecipitacion == 0 || valorTemperatura == 0) {
                        $("#dor").attr('disabled', 'disabled');
                        $("#dorw").attr('disabled', 'disabled');
                        $("#dor").prop("checked", false);
                        $("#dorw").prop("checked", false);
                        $("#fragmentacion").attr('disabled', 'disabled');
                        $("#fragmentacion").prop("checked", true);

                    } else {
                        $("#dor").prop("checked", true);
                        $("#dorw").prop("checked", true);
                        $("#fragmentacion").attr('disabled', 'disabled');
                        $("#fragmentacion").prop("checked", true);
                    }
                    $("#huella").css("display", "none");
                    $("#huellaLabel").css("display", "none");
                    $(".contForm").css("height", "460px");
                    $(".conter").css("height", "630px");
                    step = 16;
                    previousStep = 86;
                    $("#nameStep").text("Seleccionar Modelos");
                    current_fs = $(this).parent();
                    var idStep = $('#model');
                    confStep(idStep, current_fs);
                    //viene de la funcion existingCatalog del paso 47
                }
            } else if (paso == 79) {
                var valorPrecipitacion = $("#selectPrecipitation").val();
                var valorTemperatura = $("#selectTemperature").val();
                var sedimentos = $("#selectSediments").val();

                checkedFootprint = $(".check4:checked").length;
                if (checkedFootprint != 0) {
                    $("#huella").css("display", "inline-block");
                    $("#huellaLabel").css("display", "inline-block");
                    $("#huella").prop("checked", true);
                    if (sedimentos == 0) {
                        $("#sedimentos").attr('disabled', 'disabled');
                        $("#sedimentos").prop("checked", false);
                    } else {
                        $("#sedimentos").prop("checked", true);
                    }

                    if (valorPrecipitacion == 0 || valorTemperatura == 0) {
                        $("#dor").attr('disabled', 'disabled');
                        $("#dorw").attr('disabled', 'disabled');
                        $("#dor").prop("checked", false);
                        $("#dorw").prop("checked", false);
                        $("#fragmentacion").attr('disabled', 'disabled');
                        $("#fragmentacion").prop("checked", true);
                    } else {
                        $("#dor").prop("checked", true);
                        $("#dorw").prop("checked", true);
                        $("#fragmentacion").attr('disabled', 'disabled');
                        $("#fragmentacion").prop("checked", true);
                    }
                    $(".contForm").css("height", "460px");
                    $(".conter").css("height", "630px");
                    step = 16;
                    previousStep = 87;
                    $("#nameStep").text("Seleccionar Huella");
                    current_fs = $(this).parent();
                    var idStep = $('#model');
                    confStep(idStep, current_fs);
                    //viene de si del paso 78
                } else {
                    alert("Por favor ingrese una capa");
                    paso = 78;
                    step = 16;
                    previousStep = 86;
                }
            }
        } else {
            animating = false;
        }
    });

    $(".existingCatalog").click(function() {
        if (paso == 21) {
            paso = 50;
            step = 13;
            var select = document.getElementById("selectCatalog");
            for (index in selectCatalog) {
                select.options[select.options.length] = new Option(selectCatalog[index].title, selectCatalog[index].nid);
            }
            previousStep = 29;
            $("#nameStep").text("Selección de catálogo de proyectos");
            current_fs = $(this).parent();
            var idStep = $('#catalogsSelect');
            confStep(idStep, current_fs);
            //Select de proyecto del nueva narrativa del no de la primera pregunta
            //viene del 21
            //va al 51

        } else if (paso == 22) {
            paso = 52;
            step = 13;
            var select = document.getElementById("selectCatalog");
            for (index in selectCatalog) {
                select.options[select.options.length] = new Option(selectCatalog[index].title, selectCatalog[index].nid);
            }
            previousStep = 31;
            $("#nameStep").text("Selección de catálogo de proyectos");
            current_fs = $(this).parent();
            var idStep = $('#catalogsSelect');
            confStep(idStep, current_fs);
            //Select de proyecto de  del nueva narrativa del  no de la segunda pregunta
            //viene del 22
            //va al 53
        } else if (paso == 23) {
            paso = 54;
            step = 13;
            var select = document.getElementById("selectCatalog");
            for (index in selectCatalog) {
                select.options[select.options.length] = new Option(selectCatalog[index].title, selectCatalog[index].nid);
            }
            previousStep = 33;
            $("#nameStep").text("Selección de catálogo de proyectos");
            current_fs = $(this).parent();
            var idStep = $('#catalogsSelect');
            confStep(idStep, current_fs);
            //Select de proyecto de  del nueva narrativa del  si de la segunda pregunta
            //viene del 23
        }
    });

    $(".newCatalogs").click(function() {
        if (paso == 21) {
            paso = 26;
            step = 13;
            previousStep = 29;
            $("#nameStep").text("Creación del catálogo de proyectos");
            current_fs = $(this).parent();
            var idStep = $('#basicDataCatalog');
            confStep(idStep, current_fs);
            //Datos basicos de catalogo del nueva narrativa del no de la primera pregunta
            //viene del 21
        } else if (paso == 24) {
            paso = 27;
            step = 13;
            previousStep = 30;
            $("#nameStep").text("Creación del catálogo de proyectos");
            current_fs = $(this).parent();
            var idStep = $('#basicDataCatalog');
            confStep(idStep, current_fs);
            //Datos basicos de catalogo  de narrativa exiastente del no de la primera pregunta
            //viene del 24
        } else if (paso == 22) {
            paso = 28;
            step = 13;
            previousStep = 31;
            $("#nameStep").text("Creación del catálogo de proyectos");
            current_fs = $(this).parent();
            var idStep = $('#basicDataCatalog');
            confStep(idStep, current_fs);
            //Datos basicos de catalogo de  del nueva narrativa del  no de la segunda pregunta
            //viene del 22
        } else if (paso == 25) {
            paso = 29;
            step = 13;
            previousStep = 32;
            $("#nameStep").text("Creación del catálogo de proyectos");
            current_fs = $(this).parent();
            var idStep = $('#basicDataCatalog');
            confStep(idStep, current_fs);
            //Datos basicos de catalogo de narrativa exiastente del no de la segunda pregunta
            //viene del 25
        } else if (paso == 23) {
            paso = 30;
            step = 13;
            previousStep = 33;
            $("#nameStep").text("Creación del catálogo de proyectos");
            current_fs = $(this).parent();
            var idStep = $('#basicDataCatalog');
            confStep(idStep, current_fs);
            //Datos basicos de catalogo de  del nueva narrativa del  si de la segunda pregunta
            //viene del 23
        } else if (paso == 26) {
            paso = 31;
            step = 13;
            previousStep = 34;
            $("#nameStep").text("Creación del catálogo de proyectos");
            current_fs = $(this).parent();
            var idStep = $('#basicDataCatalog');
            confStep(idStep, current_fs);
            //Datos basicos de catalogo de narrativa exiastente del si de la segunda pregunta
            //viene del 24
        }
    });

    $(".previous").click(function() {
        // if (animating) return false;
        animating = true;
        if (previousStep == 1) {
            previousStep = 1;
            $("#popup-link").css("display", "none");
            $("#streamflowStepNo1").css("display", "none");
            step = 1;
            $("#nameStep").text("Pregunta");
            paso = 1;
            current_fs = $(this).parent();
            var idStep = $('#question0');
            $("#msform")[0].reset();
            confStepPrevious(idStep, current_fs);
            //va a la primera pregunata 
        } else if (previousStep == 2) {
            $("#popup-link").css("display", "none");
            $("#precipitationStepNo2").css("display", "none");
            $("#precipitationStepYes2").css("display", "none");
            $("#precipitationStep").css("backgroundColor", "#FFFFFF");
            $("#precipitationStep").css("color", "#104659");
            $("#executionStep").css("backgroundColor", "#104659");
            previousStep = 1;
            step = 2;
            $("#nameStep").text("Paso de decisión: Cálculo de la evapotranspiración Real (ETR)");
            paso = 2;
            current_fs = $(this).parent();
            var idStep = $('#question');
            $("#msform")[0].reset();
            confStepPrevious(idStep, current_fs);
            // va a segunda pregunta 5 6
        } else if (previousStep == 3) {
            previousStep = 1;
            $("#executionStep1").css("backgroundColor", "#104659");
            $("#streamflowStep").css("backgroundColor", "#FFFFFF");
            $("#streamflowStep").css("color", "#104659");
            step = 3;
            $("#nameStep").text("Datos básicos de la ejecución.");
            paso = 3;
            current_fs = $(this).parent();
            var idStep = $('#basicData');
            confStepPrevious(idStep, current_fs);
            //Esta en caudal y va a datos  basicos del no de la primera pregunta 1
        } else if (previousStep == 4) {
            previousStep = 3;
            $("#streamflowStep").css("backgroundColor", "#104659");
            $("#sedimentsStep").css("backgroundColor", "#FFFFFF");
            $("#sedimentsStep").css("color", "#104659");
            step = 7;
            paso = 4;
            $("#nameStep").text("Caja de datos del caudal (Q)");
            current_fs = $(this).parent();
            var idStep = $('#streamflow');
            confStepPrevious(idStep, current_fs);
            //esta en sedimentos y va a caudal 3
        } else if (previousStep == 5) {
            previousStep = 2;
            $("#executionStep2").css("backgroundColor", "#104659");
            $("#precipitationStep2").css("backgroundColor", "#FFFFFF");
            $("#precipitationStep2").css("color", "#104659");
            step = 3;
            $("#nameStep").text("Datos básicos de la ejecución.");
            paso = 5;
            current_fs = $(this).parent();
            var idStep = $('#basicData');
            confStepPrevious(idStep, current_fs);
            //esta en precipitacion del si de la segunda pregunta va a datos basicos del si de la segunda pregunta 2
        } else if (previousStep == 6) {
            previousStep = 2;
            $("#executionStep1").css("backgroundColor", "#104659");
            $("#precipitationStep").css("backgroundColor", "#FFFFFF");
            $("#precipitationStep").css("color", "#104659");
            step = 3;
            $("#nameStep").text("Datos básicos de la ejecución.");
            paso = 6;
            current_fs = $(this).parent();
            var idStep = $('#basicData');
            confStepPrevious(idStep, current_fs);
            //esta en precipitacion del no de la segunda pregunta va a datos basicos del no de la segunda pregunta 2
        } else if (previousStep == 7) {
            previousStep = 5;
            $("#precipitationStep2").css("backgroundColor", "#104659");
            $("#temperatureStep").css("backgroundColor", "#FFFFFF");
            $("#temperatureStep").css("color", "#104659");
            step = 4;
            $("#nameStep").text("Caja de datos de precipitación (P)");
            paso = 7;
            current_fs = $(this).parent();
            var idStep = $('#precipitation');
            confStepPrevious(idStep, current_fs);
            //esta en temperatura y va a precipitacion 5              

        } else if (previousStep == 8) {
            previousStep = 6;
            $("#precipitationStep").css("backgroundColor", "#104659");
            $("#evapotranspiracionStep").css("backgroundColor", "#FFFFFF");
            $("#evapotranspiracionStep").css("color", "#104659");
            step = 4;
            $("#nameStep").text("Precipitación");
            paso = 8;
            current_fs = $(this).parent();
            var idStep = $('#precipitation');
            confStepPrevious(idStep, current_fs);
            //esta en evapotranspiracion y va a precipitacion del no de la segunda pregunta 6

        } else if (previousStep == 9) {
            previousStep = 8;
            $("#evapotranspiracionStep").css("backgroundColor", "#104659");
            $("#sedimentsStep1").css("backgroundColor", "#FFFFFF");
            $("#sedimentsStep1").css("color", "#104659");
            step = 6;
            paso = 9;
            $("#nameStep").text("Caja de datos de evapotranspiración Real (ETR)");
            current_fs = $(this).parent();
            var idStep = $('#Evapotranspiracion');
            confStepPrevious(idStep, current_fs);
            //esta en sedimentos del no de la segunda pregunta y va a evapotranspiracion
        } else if (previousStep == 10) {
            previousStep = 7;
            $("#temperatureStep").css("backgroundColor", "#104659");
            $("#sedimentsStep2").css("backgroundColor", "#FFFFFF");
            $("#sedimentsStep2").css("color", "#104659");
            step = 5;
            $("#nameStep").text("Caja de datos de temperatura (T)");
            paso = 10;
            current_fs = $(this).parent();
            var idStep = $('#temperature');
            confStepPrevious(idStep, current_fs);
            //esta en sedimentos del si de la segunda pregunta y va a temperatura 7
        } else if (previousStep == 11) {
            previousStep = 4;
            $("#sedimentsStep").css("backgroundColor", "#104659");
            $("#narrativeStep").css("backgroundColor", "#FFFFFF");
            $("#narrativeStep").css("color", "#104659");
            step = 8;
            $("#nameStep").text("Caja de datos de sedimento (Qs)");
            paso = 11;
            current_fs = $(this).parent();
            var idStep = $('#Sediments');
            confStepPrevious(idStep, current_fs);
            //esta en narrativa y va a sedimentos 4

        } else if (previousStep == 12) {
            previousStep = 9;
            $("#sedimentsStep1").css("backgroundColor", "#104659");
            $("#narrativeStep1").css("backgroundColor", "#FFFFFF");
            $("#narrativeStep1").css("color", "#104659");
            paso = 12;
            step = 8;
            $("#nameStep").text("Caja de datos de sedimento (Qs)");
            current_fs = $(this).parent();
            var idStep = $('#Sediments');
            confStepPrevious(idStep, current_fs);
            //esta en narrativa y va a sedimentos del no de la segunda pregunta 

        } else if (previousStep == 13) {
            previousStep = 10;
            $("#sedimentsStep2").css("backgroundColor", "#104659");
            $("#narrativeStep2").css("backgroundColor", "#FFFFFF");
            $("#narrativeStep2").css("color", "#104659");
            step = 8;
            paso = 13
            $("#nameStep").text("Caja de datos de sedimento (Qs)");
            current_fs = $(this).parent();
            var idStep = $('#Sediments');
            confStepPrevious(idStep, current_fs);
            //esta en narrativa y va a sedimentos  de si de la segunda pregunta 10
        } else if (previousStep == 14) {
            $("#selectNarrative").val('0');
            $(".checkNarrative").remove();
            $(".lavelNarrative").remove();
            $('#titleNarrative').val('');
            $('#descriptionNarrative').val('');
            $('#aleatorizacionesNunber').val('');
            previousStep = 11;
            $("#sedimentsStep").css("backgroundColor", "#2ca13a");
            $("#narrativeStep").css("backgroundColor", "#104659");
            $("#narrativeStep").css("color", "#DFF1F2");
            step = 9;
            paso = 14;
            $("#nameStep").text("Narrativa");
            current_fs = $(this).parent();
            var idStep = $('#narrative');
            confStepPrevious(idStep, current_fs);
            //Esta en Seleccion de narrativa  del no de la primera pregunta y va añadir o crear narrativa
        } else if (previousStep == 15) {
            $("#selectNarrative").val('0');
            $(".checkNarrative").remove();
            $(".lavelNarrative").remove();
            $('#titleNarrative').val('');
            $('#descriptionNarrative').val('');
            $('#aleatorizacionesNunber').val('');
            previousStep = 12;
            step = 9;
            paso = 15;
            $("#nameStep").text("Narrativa");
            current_fs = $(this).parent();
            var idStep = $('#narrative');
            confStepPrevious(idStep, current_fs);
            //Esta en Seleccion de narrativa  del no de la segunda pregunta y va añadir o crear narrativa
        } else if (previousStep == 16) {
            $("#selectNarrative").val('0');
            $(".checkNarrative").remove();
            $(".lavelNarrative").remove();
            $('#titleNarrative').val('');
            $('#descriptionNarrative').val('');
            $('#aleatorizacionesNunber').val('');
            previousStep = 13;
            step = 9;
            paso = 16;
            $("#nameStep").text("Narrativa");
            current_fs = $(this).parent();
            var idStep = $('#narrative');
            confStepPrevious(idStep, current_fs);
            //Esta en Seleccion de narrativa  del si de la segunda pregunta y va añadir o crear narrativa

        } else if (previousStep == 17) {
            $("#selectNarrative").val('0');
            $(".checkNarrative").remove();
            $(".lavelNarrative").remove();
            $('#titleNarrative').val('');
            $('#descriptionNarrative').val('');
            $('#aleatorizacionesNunber').val('');
            previousStep = 11;
            $("#sedimentsStep").css("backgroundColor", "#2ca13a");
            $("#narrativeStep").css("backgroundColor", "#104659");
            $("#narrativeStep").css("color", "#DFF1F2");
            step = 9;
            paso = 14;
            $("#nameStep").text("Narrativa");
            current_fs = $(this).parent();
            var idStep = $('#narrative');
            confStepPrevious(idStep, current_fs);
            //esta en el paso 14 de la funcion newNarrative y va a  narrativa del no de la primera pregunta
        } else if (previousStep == 18) {
            $("#selectNarrative").val('0');
            $(".checkNarrative").remove();
            $(".lavelNarrative").remove();
            $('#titleNarrative').val('');
            $('#descriptionNarrative').val('');
            $('#aleatorizacionesNunber').val('');
            previousStep = 12;
            step = 9;
            paso = 15;
            $("#nameStep").text("Narrativa");
            current_fs = $(this).parent();
            var idStep = $('#narrative');
            confStepPrevious(idStep, current_fs);
            //esta en el paso 15 de la funcion newNarrative ya va a narrativa del no de la segunda pregunta
        } else if (previousStep == 19) {
            $("#selectNarrative").val('0');
            $(".checkNarrative").remove();
            $(".lavelNarrative").remove();
            $('#titleNarrative').val('');
            $('#descriptionNarrative').val('');
            $('#aleatorizacionesNunber').val('');
            previousStep = 13;
            step = 9;
            paso = 16;
            $("#nameStep").text("Narrativa");
            current_fs = $(this).parent();
            var idStep = $('#narrative');
            confStepPrevious(idStep, current_fs);
            //esta en el paso 16 de la funcion newNarrative ya va a narrativa del si de la segunda pregunta
        } else if (previousStep == 20) {
            paso = 17;
            previousStep = 17;
            $("#nameStep").text("Narrativa");
            current_fs = $(this).parent();
            var idStep = $('#basicDataNarrative');
            confStepPrevious(idStep, current_fs);
            //esta en aleatorizaciones y va a datos basicos del no de la primera pregunta 
        } else if (previousStep == 21) {
            paso = 18;
            previousStep = 18;
            $("#nameStep").text("Narrativa");
            current_fs = $(this).parent();
            var idStep = $('#basicDataNarrative');
            confStepPrevious(idStep, current_fs);
            //esta en aleatorizaciones y va a datos basicos del no de la segunda pregunta 
        } else if (previousStep == 22) {
            paso = 19;
            previousStep = 19;
            $("#nameStep").text("Narrativa");
            current_fs = $(this).parent();
            var idStep = $('#basicDataNarrative');
            confStepPrevious(idStep, current_fs);
            //esta en aleatorizaciones y va a datos basicos del si de la segunda pregunta 
        } else if (previousStep == 23) {
            step = 11;
            $("#narrativeStep").css("backgroundColor", "#104659");
            $("#catalogsStep").css("backgroundColor", "#FFFFFF");
            $("#catalogsStep").css("color", "#104659");
            paso = 20;
            previousStep = 20;
            $("#nameStep").text("Aleatorización de narrativas");
            current_fs = $(this).parent();
            var idStep = $('#aleatorizaciones');
            confStepPrevious(idStep, current_fs);
            //esta en opciones de catalogo y va a Aleatorización del no de la primera pregunta 
        } else if (previousStep == 24) {
            step = 11;
            $("#narrativeStep1").css("backgroundColor", "#104659");
            $("#catalogsStep1").css("backgroundColor", "#FFFFFF");
            $("#catalogsStep1").css("color", "#104659");
            paso = 21;
            previousStep = 21;
            $("#nameStep").text("Aleatorización de narrativas");
            current_fs = $(this).parent();
            var idStep = $('#aleatorizaciones');
            confStepPrevious(idStep, current_fs);
            //ersta en opciones de catalogo y va a Aleatorización del no de la segunda pregunta 
        } else if (previousStep == 25) {
            step = 11;
            $("#narrativeStep2").css("backgroundColor", "#104659");
            $("#catalogsStep2").css("backgroundColor", "#FFFFFF");
            $("#catalogsStep2").css("color", "#104659");
            paso = 22;
            previousStep = 22;
            $("#nameStep").text("Aleatorización de narrativas");
            current_fs = $(this).parent();
            var idStep = $('#aleatorizaciones');
            confStepPrevious(idStep, current_fs);
            //esta en opciones de catalogo y va a Aleatorización del si de la segunda pregunta 
        } else if (previousStep == 26) {
            paso = 23;
            previousStep = 14;
            $("#narrativeStep").css("backgroundColor", "#104659");
            $("#catalogsStep").css("backgroundColor", "#FFFFFF");
            $("#catalogsStep").css("color", "#104659");
            $("#footprintStep").css("backgroundColor", "#FFFFFF");
            $("#footprintStep").css("color", "#104659");
            step = 10;
            $("#nameStep").text("Narrativa");
            current_fs = $(this).parent();
            var idStep = $('#narrativeSelect');
            confStepPrevious(idStep, current_fs);
            //esta en opcion de catalogo y va a seleccionar narreativa del no de la primera pregunta
        } else if (previousStep == 27) {
            paso = 24;
            previousStep = 15;
            $("#narrativeStep1").css("backgroundColor", "#104659");
            $("#catalogsStep1").css("backgroundColor", "#FFFFFF");
            $("#catalogsStep1").css("color", "#104659");
            $("#footprintStep1").css("backgroundColor", "#FFFFFF");
            $("#footprintStep1").css("color", "#104659");
            step = 10;
            $("#nameStep").text("Narrativa");
            current_fs = $(this).parent();
            var idStep = $('#narrativeSelect');
            confStepPrevious(idStep, current_fs);
            //esta en opcion de catalogo y va a seleccionar narreativa del no de la segunda pregunta
        } else if (previousStep == 28) {
            paso = 25;
            previousStep = 16;
            $("#narrativeStep2").css("backgroundColor", "#104659");
            $("#catalogsStep2").css("backgroundColor", "#FFFFFF");
            $("#catalogsStep2").css("color", "#104659");
            $("#footprintStep2").css("backgroundColor", "#FFFFFF");
            $("#footprintStep2").css("color", "#104659");
            step = 10;
            $("#nameStep").text("Narrativa");
            current_fs = $(this).parent();
            var idStep = $('#narrativeSelect');
            confStepPrevious(idStep, current_fs);
            //esta en opcion de catalogo y va a seleccionar narreativa del si de la segunda pregunta 
        } else if (previousStep == 29) {
            $("#selectCatalog").val('0');
            $(".check1").remove();
            $(".labelcheck1").remove();
            $('#titleCatalog').val('');
            $('#descriptionCatalog').val('');
            step = 12;
            paso = 21;
            previousStep = 23;
            $("#nameStep").text("Catalogs");
            current_fs = $(this).parent();
            var idStep = $('#catalogs');
            confStepPrevious(idStep, current_fs);
            //esta en Select de proyecto del nueva narrativa del no de la primera preguntay va a opcion de catalogo
        } else if (previousStep == 31) {
            $("#selectCatalog").val('0');
            $(".check1").remove();
            $(".labelcheck1").remove();
            $('#titleCatalog').val('');
            $('#descriptionCatalog').val('');
            step = 12;
            paso = 22;
            previousStep = 24;
            $("#nameStep").text("Catalogs");
            current_fs = $(this).parent();
            var idStep = $('#catalogs');
            confStepPrevious(idStep, current_fs);
            //esta en Select de proyecto del nueva narrativa del no de la segunda preguntay va a opcion de catalogo
        } else if (previousStep == 33) {
            $("#selectCatalog").val('0');
            $(".check1").remove();
            $(".labelcheck1").remove();
            $('#titleCatalog').val('');
            $('#descriptionCatalog').val('');
            step = 12;
            paso = 23;
            previousStep = 25;
            $("#nameStep").text("Catalogs");
            current_fs = $(this).parent();
            var idStep = $('#catalogs');
            confStepPrevious(idStep, current_fs);
            //esta en Select de proyecto del nueva narrativa del si de la segunda preguntay va a opcion de catalogo
        } else if (previousStep == 35) {
            paso = 26;
            step = 13;
            previousStep = 29;
            $("#nameStep").text("Selección de proyectos");
            current_fs = $(this).parent();
            var idStep = $('#basicDataCatalog');
            confStepPrevious(idStep, current_fs);
            //esta en seleccion de proyectos y va a Datos basicos de catalogo del nueva narrativa del no de la primera pregunta
        } else if (previousStep == 36) {
            paso = 27;
            step = 13;
            previousStep = 30;
            $("#nameStep").text("Creación del catálogo de proyectos");
            current_fs = $(this).parent();
            var idStep = $('#basicDataCatalog');
            confStepPrevious(idStep, current_fs);
            //esta en seleccion de proyectos y va a Datos basicos de catalogo  de narrativa exiastente del no de la primera pregunta
        } else if (previousStep == 37) {
            paso = 29;
            step = 13;
            previousStep = 31;
            $("#nameStep").text("Creación del catálogo de proyectos");
            current_fs = $(this).parent();
            var idStep = $('#basicDataCatalog');
            confStepPrevious(idStep, current_fs);
            //esta en seleccion de proyectos y va a Datos basicos de catalogo del nueva narrativa del no de la segunda pregunta
        } else if (previousStep == 38) {
            paso = 29;
            step = 13;
            previousStep = 32;
            $("#nameStep").text("Creación del catálogo de proyectos");
            current_fs = $(this).parent();
            var idStep = $('#basicDataCatalog');
            confStepPrevious(idStep, current_fs);
            //esta en seleccion de proyectos y va a Datos basicos de catalogo  de narrativa exiastente del no de la segunda pregunta
        } else if (previousStep == 39) {
            paso = 31;
            step = 13;
            previousStep = 33;
            $("#nameStep").text("Creación del catálogo de proyectos");
            current_fs = $(this).parent();
            var idStep = $('#basicDataCatalog');
            confStepPrevious(idStep, current_fs);
            //esta en seleccion de proyectos y va a Datos basicos de catalogo del nueva narrativa del si de la segunda pregunta
        } else if (previousStep == 40) {
            paso = 32;
            step = 13;
            previousStep = 34;
            $("#nameStep").text("Creación del catálogo de proyectos");
            current_fs = $(this).parent();
            var idStep = $('#basicDataCatalog');
            confStepPrevious(idStep, current_fs);
            //esta en seleccion de proyectos y va a Datos basicos de catalogo  de narrativa exiastente del si de la segunda pregunta
        } else if (previousStep == 41) {
            $("#selectNarrative").val('0');
            $(".check").remove();
            $(".labelcheck").remove();

            Step = 13;
            $(".contForm").css("height", "393px");
            paso = 32;
            previousStep = 35;
            $("#nameStep").text("Selección de proyectos");
            current_fs = $(this).parent();
            var idStep = $('#question3');
            confStepPrevious(idStep, current_fs);
            //esta en el seleccionar  o en el seleccionar del mapa proyectos del añadir preoyecos existentes de añadir nueva narrativa del no de la primera pregunta 
        } else if (previousStep == 42) {
            Step = 13;
            $(".contForm").css("height", "393px");
            paso = 33;
            previousStep = 36;
            $("#nameStep").text("Selección de proyectos");
            current_fs = $(this).parent();
            var idStep = $('#question3');
            confStepPrevious(idStep, current_fs);
            //esta en el seleccionar  o en el seleccionar del mapa proyectos del añadir preoyecos existentes de añadir narrativa existente del no de la primera pregunta 
        } else if (previousStep == 43) {
            Step = 13;
            $(".contForm").css("height", "393px");
            paso = 34;
            previousStep = 37;
            $("#nameStep").text("Selección de proyectos");
            current_fs = $(this).parent();
            var idStep = $('#question3');
            confStepPrevious(idStep, current_fs);
            //esta en el seleccionar  o en el seleccionar del mapa proyectos del añadir preoyecos existentes de añadir nueva narrativa del no de la segunda pregunta 
        } else if (previousStep == 44) {
            Step = 13;
            $(".contForm").css("height", "393px");
            paso = 35;
            previousStep = 38;
            $("#nameStep").text("Selección de proyectos");
            current_fs = $(this).parent();
            var idStep = $('#question3');
            confStepPrevious(idStep, current_fs);
            //esta en el seleccionar  o en el seleccionar del mapa proyectos del añadir preoyecos existentes de añadir narrativa existente del no de la segunda pregunta 
        } else if (previousStep == 45) {
            Step = 13;
            $(".contForm").css("height", "393px");
            paso = 36;
            previousStep = 39;
            $("#nameStep").text("Selección de proyectos");
            current_fs = $(this).parent();
            var idStep = $('#question3');
            confStepPrevious(idStep, current_fs);
            //esta en el seleccionar  o en el seleccionar del mapa proyectos del añadir preoyecos existentes de añadir nueva narrativa del si de la segunda pregunta 
        } else if (previousStep == 46) {
            Step = 13;
            $(".contForm").css("height", "393px");
            paso = 37;
            previousStep = 40;
            $("#nameStep").text("Selección de proyectos");
            current_fs = $(this).parent();
            var idStep = $('#question3');
            confStepPrevious(idStep, current_fs);
            //esta en el seleccionar  o en el seleccionar del mapa proyectos del añadir preoyecos existentes de añadir narrativa existente del si de la segunda pregunta 
        } else if (previousStep == 47) {
            $("#catalogsStep").css("backgroundColor", "#104659");
            $("#footprintStep").css("backgroundColor", "#FFFFFF");
            $("#footprintStep").css("color", "#104659");
            step = 14;
            paso = 38;
            previousStep = 41;
            $("#nameStep").text("Selección de proyectos");
            current_fs = $(this).parent();
            var idStep = $('#projectSelect');
            confStepPrevious(idStep, current_fs);
            //esta en pregunta de huerlla y va a seleccion de proyectos del si de añadir proyectos existentes de añadir nueva narrativa del no de la primera pregunta 
        } else if (previousStep == 48) {
            $("#catalogsStep").css("backgroundColor", "#104659");
            $("#footprintStep").css("backgroundColor", "#FFFFFF");
            $("#footprintStep").css("color", "#104659");
            $(".contForm").css("height", "638px");
            $(".conter").css("height", "716px");
            step = 14;
            paso = 39;
            previousStep = 41;
            $("#nameStep").text("Ubicación del proyecto");
            current_fs = $(this).parent();
            var idStep = $('#projectMap');
            confStepPrevious(idStep, current_fs);
            //esta en pregunta de huella y va a seleccionar en el mapa del no de añadir proyectos existentes de añadir nueva narrativa del no de la primera pregunta 
        } else if (previousStep == 49) {
            $("#catalogsStep").css("backgroundColor", "#104659");
            $("#footprintStep").css("backgroundColor", "#FFFFFF");
            $("#footprintStep").css("color", "#104659");
            step = 14;
            paso = 40;
            previousStep = 42;
            $("#nameStep").text("Selección de proyectos");
            current_fs = $(this).parent();
            var idStep = $('#projectSelect');
            confStepPrevious(idStep, current_fs);
            //esta en pregunta de huella y va a seleccion de proyectos del si de añadir proyectos existentes de añadir narrativa existente del no de la primera pregunta 
        } else if (previousStep == 50) {
            $("#catalogsStep").css("backgroundColor", "#104659");
            $("#footprintStep").css("backgroundColor", "#FFFFFF");
            $("#footprintStep").css("color", "#104659");
            $(".contForm").css("height", "638px");
            $(".conter").css("height", "716px");
            step = 14;
            // paso = 41;
            previousStep = 42;
            $("#nameStep").text("Ubicación del proyecto");
            current_fs = $(this).parent();
            var idStep = $('#projectMap');
            confStepPrevious(idStep, current_fs);
            //esta en pregunta de huella y va a seleccionar en el mapa del no de añadir proyectos existentes de añadir narrativa existente  del no de la primera pregunta 
        } else if (previousStep == 51) {
            $("#catalogsStep1").css("backgroundColor", "#104659");
            $("#footprintStep1").css("backgroundColor", "#FFFFFF");
            $("#footprintStep1").css("color", "#104659");
            step = 14;
            paso = 42;
            previousStep = 43;
            $("#nameStep").text("Selección de proyectos");
            current_fs = $(this).parent();
            var idStep = $('#projectSelect');
            confStepPrevious(idStep, current_fs);
            //esta en pregunta de huerlla y va a seleccion de proyectos del si de añadir proyectos existentes de añadir nueva narrativa del no de la segunda pregunta 
        } else if (previousStep == 52) {
            $("#catalogsStep1").css("backgroundColor", "#104659");
            $("#footprintStep1").css("backgroundColor", "#FFFFFF");
            $("#footprintStep1").css("color", "#104659");
            $(".contForm").css("height", "638px");
            $(".conter").css("height", "716px");
            step = 14;
            paso = 39;
            previousStep = 41;
            $("#nameStep").text("Ubicación del proyecto");
            current_fs = $(this).parent();
            var idStep = $('#projectMap');
            confStepPrevious(idStep, current_fs);
            //esta en pregunta de huella y va a seleccionar en el mapa del no de añadir proyectos existentes de añadir nueva narrativa del no de la segunda pregunta 
        } else if (previousStep == 53) {
            $("#catalogsStep1").css("backgroundColor", "#104659");
            $("#footprintStep1").css("backgroundColor", "#FFFFFF");
            $("#footprintStep1").css("color", "#104659");
            step = 14;
            paso = 44;
            previousStep = 44;
            $("#nameStep").text("Selección de proyectos");
            current_fs = $(this).parent();
            var idStep = $('#projectSelect');
            confStepPrevious(idStep, current_fs);
            //esta en pregunta de huella y va a seleccion de proyectos del si de añadir proyectos existentes de añadir narrativa existente del no de la segunda pregunta 
        } else if (previousStep == 54) {
            $("#catalogsStep1").css("backgroundColor", "#104659");
            $("#footprintStep1").css("backgroundColor", "#FFFFFF");
            $("#footprintStep1").css("color", "#104659");
            $(".contForm").css("height", "638px");
            $(".conter").css("height", "716px");
            step = 14;
            paso = 45;
            previousStep = 44;
            $("#nameStep").text("Ubicación del proyecto");
            current_fs = $(this).parent();
            var idStep = $('#projectMap');
            confStepPrevious(idStep, current_fs);
            //esta en pregunta de huella y va a seleccionar en el mapa del no de añadir proyectos existentes de añadir narrativa existente  del no de la segunda pregunta 
        } else if (previousStep == 55) {
            $("#catalogsStep2").css("backgroundColor", "#104659");
            $("#footprintStep2").css("backgroundColor", "#FFFFFF");
            $("#footprintStep2").css("color", "#104659");
            step = 14;
            paso = 46;
            previousStep = 45;
            $("#nameStep").text("Selección de proyectos");
            current_fs = $(this).parent();
            var idStep = $('#projectSelect');
            confStepPrevious(idStep, current_fs);
            //esta en pregunta de huerlla y va a seleccion de proyectos del si de añadir proyectos existentes de añadir nueva narrativa del si de la segunda pregunta 
        } else if (previousStep == 56) {
            $("#catalogsStep2").css("backgroundColor", "#104659");
            $("#footprintStep2").css("backgroundColor", "#FFFFFF");
            $("#footprintStep2").css("color", "#104659");
            $(".contForm").css("height", "638px");
            $(".conter").css("height", "716px");
            step = 14;
            paso = 47;
            previousStep = 45;
            $("#nameStep").text("Ubicación del proyecto");
            current_fs = $(this).parent();
            var idStep = $('#projectMap');
            confStepPrevious(idStep, current_fs);
            //esta en pregunta de huella y va a seleccionar en el mapa del no de añadir proyectos existentes de añadir nueva narrativa del si de la segunda pregunta
        } else if (previousStep == 57) {
            $("#catalogsStep2").css("backgroundColor", "#104659");
            $("#footprintStep2").css("backgroundColor", "#FFFFFF");
            $("#footprintStep2").css("color", "#104659");
            step = 14;
            paso = 48;
            previousStep = 46;
            $("#nameStep").text("Selección de proyectos");
            current_fs = $(this).parent();
            var idStep = $('#projectSelect');
            confStepPrevious(idStep, current_fs);
            //esta en pregunta de huella y va a seleccion de proyectos del si de añadir proyectos existentes de añadir narrativa existente del si de la segunda pregunta         
        } else if (previousStep == 58) {
            $("#catalogsStep2").css("backgroundColor", "#104659");
            $("#footprintStep2").css("backgroundColor", "#FFFFFF");
            $("#footprintStep2").css("color", "#104659");
            $(".contForm").css("height", "638px");
            $(".conter").css("height", "716px");
            step = 14;
            paso = 49;
            previousStep = 46;
            $("#nameStep").text("Ubicación del proyecto");
            current_fs = $(this).parent();
            var idStep = $('#projectMap');
            confStepPrevious(idStep, current_fs);
            //esta en pregunta de huella y va a seleccionar en el mapa del no de añadir proyectos existentes de añadir narrativa existente  del si de la segunda pregunta
        } else if (previousStep == 59) {
            $("#catalogsStep").css("backgroundColor", "#104659");
            $("#footprintStep").css("backgroundColor", "#FFFFFF");
            $("#footprintStep").css("color", "#104659");
            paso = 50;
            step = 13;
            previousStep = 29;
            $("#nameStep").text("Selección de catálogo de proyectos");
            current_fs = $(this).parent();
            var idStep = $('#catalogsSelect');
            confStepPrevious(idStep, current_fs);
            //esta en huella y va al selecionar proyecto del añadirproyecto existente de añadir nueva narrativa  del no de la primera pregunta 
        } else if (previousStep == 61) {
            $("#catalogsStep1").css("backgroundColor", "#104659");
            $("#footprintStep1").css("backgroundColor", "#FFFFFF");
            $("#footprintStep1").css("color", "#104659");
            paso = 52;
            step = 13;
            previousStep = 31;
            $("#nameStep").text("Selección de catálogo de proyectos");
            current_fs = $(this).parent();
            var idStep = $('#catalogsSelect');
            confStepPrevious(idStep, current_fs);
            //esta en huella y va al selecionar proyecto del añadirproyecto existente de añadir nueva narrativa  del no de la segunda pregunta 
        } else if (previousStep == 63) {
            $("#catalogsStep2").css("backgroundColor", "#104659");
            $("#footprintStep2").css("backgroundColor", "#FFFFFF");
            $("#footprintStep2").css("color", "#104659");
            paso = 54;
            step = 13;
            previousStep = 33;
            $("#nameStep").text("Selección de catálogo de proyectos");
            current_fs = $(this).parent();
            var idStep = $('#catalogsSelect');
            confStepPrevious(idStep, current_fs);
            //esta en huella y va al selecionar proyecto del añadirproyecto existente de añadir nueva narrativa  del si de la segunda pregunta 
        } else if (previousStep == 64) {

            $("#selectFootprint").val('0');
            $(".check4").remove();
            $(".labelcheck4").remove();
            paso = 55;
            step = 15;
            previousStep = 26;
            $("#nameStep").text("Huella");
            current_fs = $(this).parent();
            var idStep = $('#question4');
            confStepPrevious(idStep, current_fs);
            //viene del paso 56 y va al 24
        } else if (previousStep == 65) {
            paso = 56;
            step = 16;
            previousStep = 64;
            $(".contForm").css("height", "393px");
            $(".conter").css("height", "638px");
            $("#nameStep").text("Seleccionar Huella");
            current_fs = $(this).parent();
            var idStep = $('#footprint');
            confStepPrevious(idStep, current_fs);
            //viene del paso 57 y va al 56
        } else if (previousStep == 66) {
            $("#selectFootprint").val('0');
            $(".check4").remove();
            $(".labelcheck4").remove();
            paso = 57;
            step = 15;
            previousStep = 59;
            $(".contForm").css("height", "393px");
            $("#nameStep").text("Huella");
            current_fs = $(this).parent();
            var idStep = $('#question4');
            confStepPrevious(idStep, current_fs);
            //vien del paso 58 y va al 51
        } else if (previousStep == 67) {
            paso = 58;
            step = 16;
            previousStep = 66;
            $(".contForm").css("height", "393px");
            $(".conter").css("height", "638px");
            $("#nameStep").text("Seleccionar Huella");
            current_fs = $(this).parent();
            var idStep = $('#footprint');
            confStepPrevious(idStep, current_fs);
        } else if (previousStep == 68) {
            $("#selectFootprint").val('0');
            $(".check4").remove();
            $(".labelcheck4").remove();
            paso = 59;
            step = 15;
            previousStep = 48;
            $(".contForm").css("height", "393px");
            $("#nameStep").text("Huella");
            current_fs = $(this).parent();
            var idStep = $('#question4');
            confStepPrevious(idStep, current_fs);
            //viene del 60 y va al 40 
        } else if (previousStep == 69) {
            paso = 60;
            step = 16;
            previousStep = 68;
            $(".contForm").css("height", "393px");
            $(".conter").css("height", "638px");
            $("#nameStep").text("Seleccionar Huella");
            current_fs = $(this).parent();
            var idStep = $('#footprint');
            confStepPrevious(idStep, current_fs);
            //viene del 61 y va al 60
        } else if (previousStep == 70) {
            $("#selectFootprint").val('0');
            $(".check4").remove();
            $(".labelcheck4").remove();
            paso = 61;
            step = 15;
            previousStep = 47;
            $(".contForm").css("height", "393px");
            $("#nameStep").text("Huella");
            current_fs = $(this).parent();
            var idStep = $('#question4');
            confStepPrevious(idStep, current_fs);
            //viend del 62 y va al 39
        } else if (previousStep == 71) {
            paso = 62;
            step = 16;
            previousStep = 70;
            $(".contForm").css("height", "393px");
            $(".conter").css("height", "638px");
            $("#nameStep").text("Seleccionar Huella");
            current_fs = $(this).parent();
            var idStep = $('#footprint');
            confStepPrevious(idStep, current_fs);
            //viene del 63 y va al 62 
        } else if (previousStep == 72) {
            $("#selectFootprint").val('0');
            $(".check4").remove();
            $(".labelcheck4").remove();
            paso = 63;
            step = 15;
            previousStep = 27;
            $(".contForm").css("height", "393px");
            $("#nameStep").text("Huella");
            current_fs = $(this).parent();
            var idStep = $('#question4');
            confStepPrevious(idStep, current_fs);
            //viene del 64 hy va al 25
        } else if (previousStep == 73) {
            paso = 64;
            step = 16;
            previousStep = 72;
            $(".contForm").css("height", "393px");
            $(".conter").css("height", "638px");
            $("#nameStep").text("Seleccionar Huella");
            current_fs = $(this).parent();
            var idStep = $('#footprint');
            confStepPrevious(idStep, current_fs);
            //viene del 65 y va al 64
        } else if (previousStep == 74) {
            $("#selectFootprint").val('0');
            $(".check4").remove();
            $(".labelcheck4").remove();
            paso = 65;
            step = 15;
            previousStep = 61;
            $(".contForm").css("height", "393px");
            $("#nameStep").text("Huella");
            current_fs = $(this).parent();
            var idStep = $('#question4');
            confStepPrevious(idStep, current_fs);
            //viene del 66 y va al 53
        } else if (previousStep == 75) {
            paso = 66;
            step = 16;
            previousStep = 74;
            $(".contForm").css("height", "393px");
            $(".conter").css("height", "638px");
            $("#nameStep").text("Seleccionar Huella");
            current_fs = $(this).parent();
            var idStep = $('#footprint');
            confStepPrevious(idStep, current_fs);
            //viene del 67 y va al 66
        } else if (previousStep == 76) {
            $("#selectFootprint").val('0');
            $(".check4").remove();
            $(".labelcheck4").remove();
            paso = 67;
            step = 15;
            previousStep = 52;
            $(".contForm").css("height", "393px");
            $("#nameStep").text("Huella");
            current_fs = $(this).parent();
            var idStep = $('#question4');
            confStepPrevious(idStep, current_fs);
            //viene del 68 y va al 44
        } else if (previousStep == 77) {
            paso = 68;
            step = 16;
            previousStep = 76;
            $(".contForm").css("height", "393px");
            $(".conter").css("height", "638px");
            $("#nameStep").text("Seleccionar Huella");
            current_fs = $(this).parent();
            var idStep = $('#footprint');
            confStepPrevious(idStep, current_fs);
            //viene del 69 y va al 68 
        } else if (previousStep == 78) {
            $("#selectFootprint").val('0');
            $(".check4").remove();
            $(".labelcheck4").remove();
            paso = 69;
            step = 15;
            previousStep = 51;
            $(".contForm").css("height", "393px");
            $("#nameStep").text("Huella");
            current_fs = $(this).parent();
            var idStep = $('#question4');
            confStepPrevious(idStep, current_fs);
            //viene del 70 y vas al 43
        } else if (previousStep == 79) {
            paso = 70;
            step = 16;
            previousStep = 78;
            $(".contForm").css("height", "393px");
            $(".conter").css("height", "638px");
            $("#nameStep").text("Seleccionar Huella");
            current_fs = $(this).parent();
            var idStep = $('#footprint');
            confStepPrevious(idStep, current_fs);
            //viene del 71 y va al 70
        } else if (previousStep == 80) {
            $("#selectFootprint").val('0');
            $(".check4").remove();
            $(".labelcheck4").remove();
            paso = 71;
            step = 15;
            previousStep = 28;
            $(".contForm").css("height", "393px");
            $("#nameStep").text("Huella");
            current_fs = $(this).parent();
            var idStep = $('#question4');
            confStepPrevious(idStep, current_fs);
            //viene del 72 y va al 26
        } else if (previousStep == 81) {
            paso = 72;
            step = 16;
            previousStep = 80;
            $(".contForm").css("height", "393px");
            $(".conter").css("height", "638px");
            $("#nameStep").text("Seleccionar Huella");
            current_fs = $(this).parent();
            var idStep = $('#footprint');
            confStepPrevious(idStep, current_fs);
            //viene del 72 y va al 72
        } else if (previousStep == 82) {
            $("#selectFootprint").val('0');
            $(".check4").remove();
            $(".labelcheck4").remove();
            paso = 73;
            step = 15;
            previousStep = 63;
            $(".contForm").css("height", "393px");
            $("#nameStep").text("Huella");
            current_fs = $(this).parent();
            var idStep = $('#question4');
            confStepPrevious(idStep, current_fs);
            //viene del 74 y va al 55
        } else if (previousStep == 83) {
            paso = 74;
            step = 16;
            previousStep = 82;
            $(".contForm").css("height", "393px");
            $(".conter").css("height", "63638px");
            $("#nameStep").text("Seleccionar Huella");
            current_fs = $(this).parent();
            var idStep = $('#footprint');
            confStepPrevious(idStep, current_fs);
            //viene del 75  y va al 74
        } else if (previousStep == 84) {
            $("#selectFootprint").val('0');
            $(".check4").remove();
            $(".labelcheck4").remove();
            paso = 75;
            step = 15;
            previousStep = 56;
            $(".contForm").css("height", "393px");
            $("#nameStep").text("Huella");
            current_fs = $(this).parent();
            var idStep = $('#question4');
            confStepPrevious(idStep, current_fs);
            //viene del 76 y va al 48
        } else if (previousStep == 85) {
            paso = 76;
            step = 16;
            previousStep = 84;
            $(".contForm").css("height", "393px");
            $(".conter").css("height", "638px");
            $("#nameStep").text("Seleccionar Huella");
            current_fs = $(this).parent();
            var idStep = $('#footprint');
            confStepPrevious(idStep, current_fs);
            //viene del 77 y va al 76
        } else if (previousStep == 86) {
            $("#selectFootprint").val('0');
            $(".check4").remove();
            $(".labelcheck4").remove();
            paso = 77;
            step = 15;
            previousStep = 55;
            $(".contForm").css("height", "393px");
            $("#nameStep").text("Huella");
            current_fs = $(this).parent();
            var idStep = $('#question4');
            confStepPrevious(idStep, current_fs);
            //viene del 78 y va al 47
        } else if (previousStep == 87) {
            paso = 76;
            step = 16;
            previousStep = 86;
            $(".contForm").css("height", "393px");
            $(".conter").css("height", "638px");
            $("#nameStep").text("Seleccionar Huella");
            current_fs = $(this).parent();
            var idStep = $('#footprint');
            confStepPrevious(idStep, current_fs);
            //viene del 79 y va al 78
        }
    });


    $(".newNarrative").click(function() {
        //console.log(paso);
        if (paso == 14) {
            paso = 17;
            previousStep = 17;
            $("#nameStep").text("Creación de narrativa");
            current_fs = $(this).parent();
            var idStep = $('#basicDataNarrative');
            confStep(idStep, current_fs);
            //Datos basicos de nueva narrativa del no de la primera pregunta 
        } else if (paso == 15) {
            paso = 18;
            previousStep = 18;
            $("#nameStep").text("Creación de narrativa");
            current_fs = $(this).parent();
            var idStep = $('#basicDataNarrative');
            confStep(idStep, current_fs);
            //Datos basicos de nueva narrativa del no de la segunda pregunta
        } else if (paso == 16) {
            paso = 19;
            previousStep = 19;
            $("#nameStep").text("Creación de narrativa");
            current_fs = $(this).parent();
            var idStep = $('#basicDataNarrative');
            confStep(idStep, current_fs);
            // Datos basicos de nueva narrativa del si de la segunda pregunta
        }
    });

    //toca que todo el proceso lo haga con cada seccion del if 

    function confStep(idStep, current_fs) {
        var porcentaje = (step / pasostoTales) * 100;
        myElement.style.background = "linear-gradient(to right, #4eb8d1 " + porcentaje + "%, #1d7492 " + porcentaje + "%)";

        next_fs = idStep;
        //next_fs = $(this).parent().next();

        //activate next step on progressbar using the index of next_fs
        $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

        //show the next fieldset
        next_fs.show();
        //hide the current fieldset with style
        current_fs.animate({ opacity: 0 }, {
            step: function(now, mx) {
                //as the opacity of current_fs reduces to 0 - stored in "now"
                //1. scale current_fs down to 80%
                scale = -(1 - now) * 0.2;
                //2. bring next_fs from the right(50%)
                left = (now * 50) + "%";
                //3. increase opacity of next_fs to 1 as it moves in
                opacity = 1 - now;
                current_fs.css({
                    'transform': 'scale(' + scale + ')',
                    'display': 'inline'
                });
                next_fs.css({ 'left': left, 'opacity': opacity });
            },
            duration: 10,
            complete: function() {
                current_fs.hide();
                animating = false;
            },
            //this comes from the custom easing plugin
            easing: 'easeInOutBack'
        });
        // console.log(paso);

    }

    function confStepPrevious(idStep, current_fs) {
        var porcentaje = (step / pasostoTales) * 100;
        myElement.style.background = "linear-gradient(to right, #4eb8d1 " + porcentaje + "%, #1d7492 " + porcentaje + "%)";

        //current_fs = $(this).parent();
        previous_fs = idStep;
        //previous_fs = $(this).parent().prev();

        //de-activate current step on progressbar
        $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

        //show the previous fieldset
        previous_fs.show();
        //hide the current fieldset with style
        current_fs.animate({ opacity: 0 }, {
            step: function(now, mx) {
                //as the opacity of current_fs reduces to 0 - stored in "now"
                //1. scale previous_fs from 80% to 100%
                scale = 1 + (1 - now) * 0.0;
                //2. take current_fs to the right(50%) - from 0%
                left = ((1 - now) * 50) + "%";
                //3. increase opacity of previous_fs to 1 as it moves in
                opacity = 1 - now;
                current_fs.css({ 'left': left });
                previous_fs.css({ 'transform': 'scale(' + scale + ')', 'opacity': opacity });
            },
            duration: 10,
            complete: function() {
                current_fs.hide();
                animating = false;
            },
            //this comes from the custom easing plugin
            easing: 'easeInOutBack'
        });
        // console.log(paso);
    }

    $(".submit").click(function() {
        return false;
    })


    $.ajax({
        type: 'GET',
        // make sure you respect the same origin policy with this url:
        // http://en.wikipedia.org/wiki/Same_origin_policy
        url: 'http://test.sima-magdalena.co/api/fishermanApp?q=services/session/token',
        headers: {
            'Content-Type': 'application/json',
        },
        error: function(err) {
            console.log('Error!', err)
        },
        success: function(msg) {
            //alert('sucess' + msg);
            token = msg;
        }
    });

    $(".guardar").click(function() {
        if (arrayProject != 0) {

            $.each(project, function(index, value) {
                var numberOfProjects = project.length - 1;
                $.ajax({
                    type: 'POST',
                    // make sure you respect the same origin policy with this url:
                    // http://en.wikipedia.org/wiki/Same_origin_policy
                    url: 'http://test.sima-magdalena.co/api/fishermanApp/entity_node/',
                    headers: {
                        'Content-Type': 'application/json',
                        'Accept': 'application/json',
                        'X-CSRF-Token': token
                    },
                    xhrFields: { withCredentials: true },
                    data: JSON.stringify(value),
                    error: function(err) {
                        console.log('Error!', err)
                    },
                    success: function(response) {
                        targertIdprojectnume = 1;
                        targertIdproject.push(response.nid);
                        if (index == numberOfProjects) {
                            saveCatalog(targertIdproject);
                        }
                    }
                });
            });
        } else {
            saveSelectCatalog()
        }
    });

    function saveSelectCatalog() {
        var targertIdCatalog = $("#selectCatalog").val();
        if (targertIdCatalog != 0) {

            var checkedProject = $(".check1:checked").length; //Creamos una Variable y Obtenemos el Numero de Checkbox que esten Seleccionados
            $("input:checkbox[name=check1]:checked").each(function() {
                $(this).val();
                objectUnd = {};
                objectUnd.target_id = $(this).val();
                projecNarrative.push(objectUnd);
            });
            saveNarrative(projecNarrative);
        } else {
            saveCatalog();
        }

    }

    function saveCatalog() {
        var titleCatalog = $("#titleCatalog").val();
        var descriptionCatalog = $("#descriptionCatalog").val();

        if (titleCatalog != "") {
            if (targertIdprojectnume != 0) {
                $.each(targertIdproject, function(index, value) {
                    objectUnd = {};
                    objectUnd.target_id = value;
                    projecNarrative.push(objectUnd); //ir con navegador
                });

                var dataJson = {
                    'title': titleCatalog,
                    'type': 'project_catalog',
                    'field_description': {
                        'und': [{ 'value': descriptionCatalog }]
                    },
                    'field_projects': {
                        'und': projecNarrative
                    },
                }

            } else {
                var checked = $(".check:checked").length; //Creamos una Variable y Obtenemos el Numero de Checkbox que esten Seleccionados
                $("input:checkbox[name=check]:checked").each(function() {
                    $(this).val();
                    objectUnd = {};
                    objectUnd.target_id = $(this).val();
                    projecNarrative.push(objectUnd);
                });

                var dataJson = {
                    'title': titleCatalog,
                    'type': 'project_catalog',
                    'field_description': {
                        'und': [{ 'value': descriptionCatalog }]
                    },
                    'field_projects': {
                        'und': projecNarrative
                    },
                }
            }
            $.ajax({
                type: 'POST',
                // make sure you respect the same origin policy with this url:
                // http://en.wikipedia.org/wiki/Same_origin_policy
                url: 'http://test.sima-magdalena.co/api/fishermanApp/entity_node/',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'X-CSRF-Token': token
                },
                xhrFields: { withCredentials: true },
                data: JSON.stringify(dataJson),
                error: function(err) {
                    console.log('Error!', err)
                },
                success: function(response) {
                    targertIdCatalog = response.nid;
                    saveNarrative(projecNarrative);
                }
            });

        } else {
            var targertIdNarrative = $("#selectNarrative").val();
            if (targertIdNarrative != 0) {
                saveExecution(targertIdNarrative);
            }
        }
    }

    function saveNarrative() {

        var titleNarrative = $("#titleNarrative").val();
        var descriptionNarrative = $("#descriptionNarrative").val();


        if (titleNarrative != "") {

            var dataJson = {
                'title': titleNarrative,
                'type': 'narrative',
                'field_narrative_description': {
                    'und': [{ 'value': descriptionNarrative }]
                },
                'field_narrative_projects': {
                    'und': projecNarrative
                },
            }
            $.ajax({
                type: 'POST',
                // make sure you respect the same origin policy with this url:
                // http://en.wikipedia.org/wiki/Same_origin_policy
                url: 'http://test.sima-magdalena.co/api/fishermanApp/entity_node/',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'X-CSRF-Token': token
                },
                xhrFields: { withCredentials: true },
                data: JSON.stringify(dataJson),
                error: function(err) {
                    console.log('Error!', err)
                },
                success: function(response) {
                    targertIdNarrative = response.nid;
                    saveExecution(targertIdNarrative);
                }
            });
        }
    }

    function saveExecution(targertIdNarrative) {
        var titles = $("#title").val();
        var description = $("#description").val();
        var targetIdPrecipitation = $("#selectPrecipitation").val();
        var targetIdTemperature = $("#selectTemperature").val();
        var targetIdEvaporation = $("#selectEvapotranspiracion").val();
        var targetIdStreamflow = $("#selectstreamflow").val();
        var targetIdSediments = $("#selectSediments").val();
        var models = $(".checkModuls:checked").length;
        var targertIdproject = $("#projectSelect").val();
        var ele = document.getElementsByName('questionHue');
        for (i = 0; i < ele.length; i++) {
            if (ele[i].checked)
                var targertIdFootprint = ele[i].value;
        }
        var dataJson = {};
        dataJson.title = titles;

        var und = {};
        var undArray = [];
        var undArrayObject = {};
        var undArrayObjectFootprint = {};
        undArrayObject.value = description;
        undArray.push(undArrayObject);
        und.und = undArray;
        dataJson.body = und;

        dataJson.type = 'execution';;

        if (targetIdPrecipitation != 0) {
            var und = {};
            var undArray = [];
            var undArrayObject = {};
            undArrayObject.target_id = targetIdPrecipitation;
            undArray.push(undArrayObject);
            und.und = undArray;
            dataJson.field_precipitation = und;
        }

        if (targetIdTemperature != 0) {
            var und = {};
            var undArray = [];
            var undArrayObject = {};
            undArrayObject.target_id = targetIdTemperature;
            undArray.push(undArrayObject);
            und.und = undArray;
            dataJson.field_temperature = und;
        }

        if (targetIdEvaporation != 0) {
            var und = {};
            var undArray = [];
            var undArrayObject = {};
            undArrayObject.target_id = targetIdEvaporation;
            undArray.push(undArrayObject);
            und.und = undArray;
            dataJson.field_evaporation = und;
        }
        if (targetIdSediments != 0) {
            var und = {};
            var undArray = [];
            var undArrayObject = {};
            undArrayObject.target_id = targetIdSediments;
            undArray.push(undArrayObject);
            und.und = undArray;
            dataJson.field_sediments = und;
        }

        if (targetIdStreamflow != 0) {
            var und = {};
            var undArray = [];
            var undArrayObject = {};
            undArrayObject.target_id = targetIdStreamflow;
            undArray.push(undArrayObject);
            und.und = undArray;
            dataJson.field_streamflow = und;
        }

        if (targertIdNarrative != 0) {
            idNarrative = new Array();
            var checked = $(".checkNarrative:checked").length; //Creamos una Variable y Obtenemos el Numero de Checkbox que esten Seleccionados
            if (checked != 0) {
                $("input:checkbox[name=checkNarrative]:checked").each(function() {
                    $(this).val();
                    objectUnd = {};
                    objectUnd.target_id = $(this).val();
                    idNarrative.push(objectUnd);
                });
                var und = {};
                und.und = idNarrative;
                dataJson.field_narratives = und;
            } else {
                var und = {};
                var undArray = [];
                var undArrayObject = {};
                undArrayObject.target_id = targertIdNarrative;
                undArray.push(undArrayObject);
                und.und = undArray;
                dataJson.field_narratives = und;
            }
        }

        if (targertIdFootprint != 0) {
            footprintAreas = new Array();
            var checked = $(".check4:checked").length; //Creamos una Variable y Obtenemos el Numero de Checkbox que esten Seleccionados
            $("input:checkbox[name=check4]:checked").each(function() {
                $(this).val();
                objectUnd = {};
                objectUnd.target_id = $(this).val();
                footprintAreas.push(objectUnd);
            });
            var und = {};
            und.und = footprintAreas;
            dataJson.field_footprint_areas = und
        }

        if (models != 0) {
            var footprintStatus = false;
            modulsarray = new Array();
            $("input:checkbox[name=checkModuls]:checked").each(function() {
                $(this).val();
                objectUnd = {};
                switch ($(this).val()) {
                    case 'DOR':
                        objectUnd.value = "1";
                        break;
                    case 'DORw':
                        objectUnd.value = "2";
                        break;
                    case 'Sedimentos':
                        objectUnd.value = "5";
                        break;
                    case 'Fragmentacion':
                        objectUnd.value = "7";
                        break;
                    case 'Huella':
                        footprintStatus = true;
                        break;
                    default:
                        objectUnd.value = "7";
                        break;
                }
                modulsarray.push(objectUnd);
            });
            var und = {};
            und.und = modulsarray;
            dataJson.field_models = und
            if (footprintStatus) {
                var und = {};
                var objectUnd = {};
                objectUnd.value = "1";
                var footprintArray = [];
                footprintArray.push(objectUnd);
                und.und = footprintArray;
                dataJson.field_footprint_model = und;
            }
        }

        if (targertIdproject != 0) {
            var und = {};
            var undArray = [];
            var undArrayObject = {};
            undArrayObject.target_id = targertIdproject;
            var und = {};
            var undArray = [];
            var undArrayObject = {};
            undArrayObject.target_id = targertIdproject;
            undArray.push(undArrayObject);
            und.und = undArray;
            dataJson.tier1_project = und;
        }



        /* var dataJson = {
             'title': title,
             'body': { 'und': [{ 'value': description }] },
             'type': 'execution',
             'field_precipitation': { 'und': [{ 'target_id': targetIdPrecipitation }] },
             'field_temperature': { 'und': [{ 'target_id': targetIdTemperature }] },
             'field_evaporation': { 'und': [{ 'target_id': targetIdEvaporation }] },
             'field_streamflow': { 'und': [{ 'target_id': targetIdStreamflow }] },
             'field_sediments': { 'und': [{ 'target_id': targetIdSediments }] }
         };*/

        //alert(dataJson);
        // console.log(dataJson);

        $.ajax({
            type: 'POST',
            // make sure you respect the same origin policy with this url:
            // http://en.wikipedia.org/wiki/Same_origin_policy
            url: 'http://test.sima-magdalena.co/api/fishermanApp/entity_node/',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'X-CSRF-Token': token
            },
            xhrFields: { withCredentials: true },
            data: JSON.stringify(dataJson),
            error: function(err) {
                console.log('Error!', err)
            },
            success: function(response) {
                var targertId = response.nid;
                location.href = 'execution-list';
            }
        });
    }
});