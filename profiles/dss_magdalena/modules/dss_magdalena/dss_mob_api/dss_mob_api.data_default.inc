<?php
/**
 * @file
 * dss_mob_api.data_default.inc
 */

/**
 * Implements hook_data_default().
 */
function dss_mob_api_data_default() {
  $export = array();

  $data_table = new stdClass();
  $data_table->disabled = FALSE; /* Edit this to true to make a default data_table disabled initially */
  $data_table->api_version = 1;
  $data_table->title = 'municipios';
  $data_table->name = 'feeds_datastore_municipios_0';
  $data_table->table_schema = array(
    'fields' => array(
      'id' => array(
        'type' => 'int',
        'not null' => FALSE,
      ),
      'name' => array(
        'type' => 'text',
        'not null' => FALSE,
        'size' => 'big',
      ),
      'id_departamento' => array(
        'type' => 'int',
        'not null' => FALSE,
      ),
      'title' => array(
        'type' => 'text',
        'not null' => FALSE,
        'size' => 'big',
      ),
      'feeds_flatstore_entry_id' => array(
        'type' => 'serial',
        'size' => 'normal',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'timestamp' => array(
        'description' => 'The Unix timestamp for the data.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => FALSE,
      ),
      'feeds_entity_id' => array(
        'description' => 'The feeds entity id for the data.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => FALSE,
      ),
    ),
    'indexes' => array(
      'feeds_flatstore_entry_id' => array(
        0 => 'feeds_flatstore_entry_id',
      ),
      'timestamp' => array(
        0 => 'timestamp',
      ),
    ),
    'primary key' => array(
      0 => 'feeds_flatstore_entry_id',
    ),
    'mysql_engine' => 'MYISAM',
  );
  $data_table->meta = array(
    'fields' => array(
      'id' => array(
        'label' => 'id',
      ),
      'name' => array(
        'label' => 'name',
      ),
      'id_departamento' => array(
        'label' => 'id_departamento',
      ),
      'title' => array(
        'label' => 'title',
      ),
    ),
  );
  $export['feeds_datastore_municipios_0'] = $data_table;

  return $export;
}
