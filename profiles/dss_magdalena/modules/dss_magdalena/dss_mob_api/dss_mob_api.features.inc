<?php
/**
 * @file
 * dss_mob_api.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function dss_mob_api_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "data" && $api == "data_default") {
    return array("version" => "1");
  }
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function dss_mob_api_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function dss_mob_api_node_info() {
  $items = array(
    'activity' => array(
      'name' => t('Actividad'),
      'base' => 'node_content',
      'description' => t('Actividades disponibles para el pescador App Movil'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'art' => array(
      'name' => t('Arte'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Nombre'),
      'help' => '',
    ),
    'association' => array(
      'name' => t('Asociación'),
      'base' => 'node_content',
      'description' => t('Asociaciones disponibles para App móvil pescadores'),
      'has_title' => '1',
      'title_label' => t('Nombre'),
      'help' => '',
    ),
    'boat' => array(
      'name' => t('Embarcación'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Nombre'),
      'help' => '',
    ),
    'department' => array(
      'name' => t('Departamento'),
      'base' => 'node_content',
      'description' => t('Departamentos de Colombia App Móvil'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'engine' => array(
      'name' => t('Motor'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Nombre'),
      'help' => '',
    ),
    'fisherman' => array(
      'name' => t('Pescador'),
      'base' => 'node_content',
      'description' => t('Pescadores para la aplicación móvil'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'labor' => array(
      'name' => t('Faena'),
      'base' => 'node_content',
      'description' => t('Fanea de la app móvil'),
      'has_title' => '1',
      'title_label' => t('Nombre'),
      'help' => '',
    ),
    'municipality' => array(
      'name' => t('Municipio'),
      'base' => 'node_content',
      'description' => t('Municipios de Colombia App Móvil Pescadores'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'schooling' => array(
      'name' => t('Escolaridad'),
      'base' => 'node_content',
      'description' => t('Escolaridad del pescador en la App Móvil pescadores'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_rdf_default_mappings().
 */
function dss_mob_api_rdf_default_mappings() {
  $rdf_mappings = array();

  // Exported RDF mapping: comment_node_labor
  $rdf_mappings['comment']['comment_node_labor'] = array(
    'rdftype' => array(
      0 => 'sioc:Post',
      1 => 'sioct:Comment',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'comment_body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'pid' => array(
      'predicates' => array(
        0 => 'sioc:reply_of',
      ),
      'type' => 'rel',
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
  );

  // Exported RDF mapping: labor
  $rdf_mappings['node']['labor'] = array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  );

  // Exported RDF mapping: boat
  $rdf_mappings['node']['boat'] = array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  );

  return $rdf_mappings;
}
