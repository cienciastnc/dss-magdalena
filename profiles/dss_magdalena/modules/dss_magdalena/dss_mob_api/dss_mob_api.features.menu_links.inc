<?php
/**
 * @file
 * dss_mob_api.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function dss_mob_api_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_artes:arts.
  $menu_links['main-menu_artes:arts'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'arts',
    'router_path' => 'arts',
    'link_title' => 'Artes',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_artes:arts',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'main-menu_app-mvil:http://localhost',
  );
  // Exported menu link: main-menu_faenas:labors.
  $menu_links['main-menu_faenas:labors'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'labors',
    'router_path' => 'labors',
    'link_title' => 'Faenas',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_faenas:labors',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
    'parent_identifier' => 'main-menu_app-mvil:http://localhost',
  );
  // Exported menu link: navigation_arte:node/add/art.
  $menu_links['navigation_arte:node/add/art'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'node/add/art',
    'router_path' => 'node/add/art',
    'link_title' => 'Arte',
    'options' => array(
      'identifier' => 'navigation_arte:node/add/art',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'navigation_aadir-contenido:node/add',
  );
  // Exported menu link: navigation_embarcacin:node/add/boat.
  $menu_links['navigation_embarcacin:node/add/boat'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'node/add/boat',
    'router_path' => 'node/add/boat',
    'link_title' => 'Embarcación',
    'options' => array(
      'identifier' => 'navigation_embarcacin:node/add/boat',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'navigation_aadir-contenido:node/add',
  );
  // Exported menu link: navigation_faena:node/add/labor.
  $menu_links['navigation_faena:node/add/labor'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'node/add/labor',
    'router_path' => 'node/add/labor',
    'link_title' => 'Faena',
    'options' => array(
      'attributes' => array(
        'title' => 'Fanea de la app móvil',
      ),
      'identifier' => 'navigation_faena:node/add/labor',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'navigation_aadir-contenido:node/add',
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Arte');
  t('Artes');
  t('Embarcación');
  t('Faena');
  t('Faenas');

  return $menu_links;
}
