<?php
/**
 * @file
 * dss_mob_api.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function dss_mob_api_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer panelizer node activity defaults'.
  $permissions['administer panelizer node activity defaults'] = array(
    'name' => 'administer panelizer node activity defaults',
    'roles' => array(),
    'module' => 'panelizer',
  );

  // Exported permission: 'administer panelizer node art defaults'.
  $permissions['administer panelizer node art defaults'] = array(
    'name' => 'administer panelizer node art defaults',
    'roles' => array(),
    'module' => 'panelizer',
  );

  // Exported permission: 'administer panelizer node association defaults'.
  $permissions['administer panelizer node association defaults'] = array(
    'name' => 'administer panelizer node association defaults',
    'roles' => array(),
    'module' => 'panelizer',
  );

  // Exported permission: 'administer panelizer node department defaults'.
  $permissions['administer panelizer node department defaults'] = array(
    'name' => 'administer panelizer node department defaults',
    'roles' => array(),
    'module' => 'panelizer',
  );

  // Exported permission: 'administer panelizer node fisherman defaults'.
  $permissions['administer panelizer node fisherman defaults'] = array(
    'name' => 'administer panelizer node fisherman defaults',
    'roles' => array(),
    'module' => 'panelizer',
  );

  // Exported permission: 'administer panelizer node labor defaults'.
  $permissions['administer panelizer node labor defaults'] = array(
    'name' => 'administer panelizer node labor defaults',
    'roles' => array(),
    'module' => 'panelizer',
  );

  // Exported permission: 'administer panelizer node municipality defaults'.
  $permissions['administer panelizer node municipality defaults'] = array(
    'name' => 'administer panelizer node municipality defaults',
    'roles' => array(),
    'module' => 'panelizer',
  );

  // Exported permission: 'administer panelizer node schooling defaults'.
  $permissions['administer panelizer node schooling defaults'] = array(
    'name' => 'administer panelizer node schooling defaults',
    'roles' => array(),
    'module' => 'panelizer',
  );

  // Exported permission: 'clear departamentos feeds'.
  $permissions['clear departamentos feeds'] = array(
    'name' => 'clear departamentos feeds',
    'roles' => array(),
    'module' => 'feeds',
  );

  // Exported permission: 'clear municipios feeds'.
  $permissions['clear municipios feeds'] = array(
    'name' => 'clear municipios feeds',
    'roles' => array(),
    'module' => 'feeds',
  );

  // Exported permission: 'create activity content'.
  $permissions['create activity content'] = array(
    'name' => 'create activity content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'create art content'.
  $permissions['create art content'] = array(
    'name' => 'create art content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'create association content'.
  $permissions['create association content'] = array(
    'name' => 'create association content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'create department content'.
  $permissions['create department content'] = array(
    'name' => 'create department content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'create fisherman content'.
  $permissions['create fisherman content'] = array(
    'name' => 'create fisherman content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'create labor content'.
  $permissions['create labor content'] = array(
    'name' => 'create labor content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'create municipality content'.
  $permissions['create municipality content'] = array(
    'name' => 'create municipality content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'create schooling content'.
  $permissions['create schooling content'] = array(
    'name' => 'create schooling content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any activity content'.
  $permissions['delete any activity content'] = array(
    'name' => 'delete any activity content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any art content'.
  $permissions['delete any art content'] = array(
    'name' => 'delete any art content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any association content'.
  $permissions['delete any association content'] = array(
    'name' => 'delete any association content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any department content'.
  $permissions['delete any department content'] = array(
    'name' => 'delete any department content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any fisherman content'.
  $permissions['delete any fisherman content'] = array(
    'name' => 'delete any fisherman content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any labor content'.
  $permissions['delete any labor content'] = array(
    'name' => 'delete any labor content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any municipality content'.
  $permissions['delete any municipality content'] = array(
    'name' => 'delete any municipality content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any schooling content'.
  $permissions['delete any schooling content'] = array(
    'name' => 'delete any schooling content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own activity content'.
  $permissions['delete own activity content'] = array(
    'name' => 'delete own activity content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own art content'.
  $permissions['delete own art content'] = array(
    'name' => 'delete own art content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own association content'.
  $permissions['delete own association content'] = array(
    'name' => 'delete own association content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own department content'.
  $permissions['delete own department content'] = array(
    'name' => 'delete own department content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own fisherman content'.
  $permissions['delete own fisherman content'] = array(
    'name' => 'delete own fisherman content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own labor content'.
  $permissions['delete own labor content'] = array(
    'name' => 'delete own labor content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own municipality content'.
  $permissions['delete own municipality content'] = array(
    'name' => 'delete own municipality content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own schooling content'.
  $permissions['delete own schooling content'] = array(
    'name' => 'delete own schooling content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit any activity content'.
  $permissions['edit any activity content'] = array(
    'name' => 'edit any activity content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit any art content'.
  $permissions['edit any art content'] = array(
    'name' => 'edit any art content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit any association content'.
  $permissions['edit any association content'] = array(
    'name' => 'edit any association content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit any department content'.
  $permissions['edit any department content'] = array(
    'name' => 'edit any department content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit any fisherman content'.
  $permissions['edit any fisherman content'] = array(
    'name' => 'edit any fisherman content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit any labor content'.
  $permissions['edit any labor content'] = array(
    'name' => 'edit any labor content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit any municipality content'.
  $permissions['edit any municipality content'] = array(
    'name' => 'edit any municipality content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit any schooling content'.
  $permissions['edit any schooling content'] = array(
    'name' => 'edit any schooling content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own activity content'.
  $permissions['edit own activity content'] = array(
    'name' => 'edit own activity content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own art content'.
  $permissions['edit own art content'] = array(
    'name' => 'edit own art content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own association content'.
  $permissions['edit own association content'] = array(
    'name' => 'edit own association content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own department content'.
  $permissions['edit own department content'] = array(
    'name' => 'edit own department content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own fisherman content'.
  $permissions['edit own fisherman content'] = array(
    'name' => 'edit own fisherman content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own labor content'.
  $permissions['edit own labor content'] = array(
    'name' => 'edit own labor content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own municipality content'.
  $permissions['edit own municipality content'] = array(
    'name' => 'edit own municipality content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own schooling content'.
  $permissions['edit own schooling content'] = array(
    'name' => 'edit own schooling content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'import departamentos feeds'.
  $permissions['import departamentos feeds'] = array(
    'name' => 'import departamentos feeds',
    'roles' => array(),
    'module' => 'feeds',
  );

  // Exported permission: 'import municipios feeds'.
  $permissions['import municipios feeds'] = array(
    'name' => 'import municipios feeds',
    'roles' => array(),
    'module' => 'feeds',
  );

  // Exported permission: 'unlock departamentos feeds'.
  $permissions['unlock departamentos feeds'] = array(
    'name' => 'unlock departamentos feeds',
    'roles' => array(),
    'module' => 'feeds',
  );

  // Exported permission: 'unlock municipios feeds'.
  $permissions['unlock municipios feeds'] = array(
    'name' => 'unlock municipios feeds',
    'roles' => array(),
    'module' => 'feeds',
  );

  return $permissions;
}
