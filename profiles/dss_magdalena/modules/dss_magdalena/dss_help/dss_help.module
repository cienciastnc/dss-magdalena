<?php
/**
 * @file
 * Pluggable system to provide dss help facilities for SIMA.
 */

/**
 * Implements hook_menu().
 *
 * Strings in hook_help() should not be run through t().
 */
function dss_help_menu() {
  $items['help/index'] = array(
    'type' => MENU_NORMAL_ITEM,
    'page callback' => 'advanced_help_index_page_v2',
    'page arguments' => array('dss_help'),
    'access arguments' => array('view advanced help index'),
  );

  // View help topic.
  $items['help/%/%'] = array(
    'page callback' => 'dss_help_topic_page',
    'page arguments' => array(1, 2),
    'access arguments' => array('view advanced help topic'),
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/**
 * Page callback to view the advanced help topic index.
 *
 * Hack for advanced help TODO: DELETE This Hack or make it better ;)
 *
 * @param string $module
 *   Name of the module.
 *
 * @return array
 *   Returns module index.
 */
function advanced_help_index_page_v2($module = '') {
  // Set Pdf link.
  $path = drupal_get_path('module', 'dss_help') . '/help/INTRODUCCION-USUARIO-SIMA.pdf';
  $link = l(t('Descargar Documento de Introducción a Usuario SIMA'), $path);
  $path_guia = drupal_get_path('module', 'dss_help') . '/help/Breve_GUIA_PRACTICA_SIMA_0.docx';
  $link_guia = l(t('Descargar Documento de de Guía Práctica para SIMA'), $path_guia);
  $header_html = '<p>El documento detallado a continuación provee una orientación general sobre las funcionalidades del sistema de apoyo a la toma de decisiones en Macrocuencas (denominado “SIMA”) con el propósito de brindar al usuario una guía lo más sencilla e informativa posible.</p>';
  $header_html .= "<p><ul><li>{$link}</li><li>{$link_guia}</li></ul></p>";

  // Handle advanced help index output.
  $output = advanced_help_index_page($module);
  if (isset($output['advanced_help_search'])) {
    // $output['advanced_help_search']['#markup'] .= $header_html;.
    $output['advanced_help_search']['#markup'] = $header_html;
  }
  else {
    $output['advanced_help_search']['#markup'] = $header_html;
  }

  return $output;
}

/**
 * Page callback to view a help topic.
 *
 * Code taken from .module file from advanced_help.
 */
function dss_help_topic_page($module, $topic) {
  $info = advanced_help_get_topic($module, $topic);
  if (!$info) {
    return drupal_not_found();
  }

  $popup = !empty($_GET['popup']) && user_access('view advanced help popup');

  drupal_set_title($info['title']);

  // Set up breadcrumb.
  $breadcrumb = array();

  $parent = $info;
  $pmodule = $module;

  // Loop checker.
  $checked = array();
  while (!empty($parent['parent'])) {
    if (strpos($parent['parent'], '%')) {
      list($pmodule, $ptopic) = explode('%', $parent['parent']);
    }
    else {
      $ptopic = $parent['parent'];
    }

    if (!empty($checked[$pmodule][$ptopic])) {
      break;
    }
    $checked[$pmodule][$ptopic] = TRUE;

    $parent = advanced_help_get_topic($pmodule, $ptopic);
    if (!$parent) {
      break;
    }

    $breadcrumb[] = advanced_help_l($parent['title'], "help/$pmodule/$ptopic");
  }

  $breadcrumb[] = advanced_help_l(advanced_help_get_module_name($pmodule), "help/index");
  $breadcrumb[] = advanced_help_l(t('Help'), "admin/help/ah");

  $output = dss_help_view_topic($module, $topic, $popup);
  if (empty($output)) {
    $output = t('Missing help topic.');
  }

  if ($popup) {
    // Prevent devel module from spewing.
    $GLOBALS['devel_shutdown'] = FALSE;
    // Suppress admin_menu.
    module_invoke('admin_menu', 'suppress');
    drupal_set_breadcrumb(array_reverse($breadcrumb));
    print theme('advanced_help_popup', array('content' => $output));
    return;
  }

  drupal_add_css(drupal_get_path('module', 'advanced_help') . '/help.css');
  drupal_add_css(drupal_get_path('module', 'dss_help') . '/css/styles.css');
  $breadcrumb[] = l(t('Home'), '');
  drupal_set_breadcrumb(array_reverse($breadcrumb));
  return $output;
}

/**
 * Load and render a help topic.
 *
 * Code taken from .module file from advanced_help.
 *
 * @param string $module
 *   Name of the module.
 * @param string $topic
 *   Name of the topic.
 * @param bool $popup
 *   Whether to show in popup or not.
 *
 * @return string
 *   Returns formatted topic.
 */
function dss_help_view_topic($module, $topic, $popup = FALSE) {
  $file_info = advanced_help_get_topic_file_info($module, $topic);
  if ($file_info) {
    $info = advanced_help_get_topic($module, $topic);
    $file = "./$file_info[path]/$file_info[file]";

    $output = file_get_contents($file);
    if (isset($info['readme file']) && $info['readme file']) {
      $ext = pathinfo($file, PATHINFO_EXTENSION);
      if ('md' == $ext && module_exists('markdown')) {
        $filters = module_invoke('markdown', 'filter_info');
        $md_info = $filters['filter_markdown'];

        if (function_exists($md_info['process callback'])) {
          $function = $md_info['process callback'];
          $output = '<div class="advanced-help-topic">' . filter_xss_admin($function($output, NULL)) . '</div>';
        }
        else {
          $output = '<div class="advanced-help-topic"><pre class="readme">' . check_plain($output) . '</pre></div>';
        }
      }
      else {
        $readme = '';
        if ('md' == $ext) {
          $readme .=
                        '<p>' .
                        t('If you install the !module module, the text below will be filtered by the module, producing rich text.',
                    array(
                      '!module' => l(t('Markdown filter'),
                        'https://www.drupal.org/project/markdown',
                        array('attributes' => array('title' => t('Link to project.')))),
                    )) . '</p>';
        }
        $readme .=
                    '<div class="advanced-help-topic"><pre class="readme">' . check_plain($output) . '</pre></div>';
        $output = $readme;
      }
      return $output;
    }

    // Make some exchanges. The strtr is because url() translates $ into %24
    // but we need to change it back for the regex replacement.
    //
    // Change 'topic:' to the URL for another help topic.
    if ($popup) {
      $output = preg_replace('/href="topic:([^"]+)"/', 'href="' . strtr(url('help/$1', array('query' => array('popup' => 'true'))), array('%24' => '$')) . '"', $output);
      $output = preg_replace('/src="topic:([^"]+)"/', 'src="' . strtr(url('help/$1', array('query' => array('popup' => 'true'))), array('%24' => '$')) . '"', $output);
      $output = preg_replace('/&topic:([^"]+)&/', strtr(url('help/$1', array('query' => array('popup' => 'true'))), array('%24' => '$')), $output);
    }
    else {
      $output = preg_replace('/href="topic:([^"]+)"/', 'href="' . strtr(url('help/$1'), array('%24' => '$')) . '"', $output);
      $output = preg_replace('/src="topic:([^"]+)"/', 'src="' . strtr(url('help/$1'), array('%24' => '$')) . '"', $output);
      $output = preg_replace('/&topic:([^"]+)&/', strtr(url('help/$1'), array('%24' => '$')), $output);
    }

    global $base_path;

    // Change 'path:' to the URL to the base help directory.
    $output = preg_replace('/href="path:([^"]+)"/', 'href="' . $base_path . $info['path'] . '/$1"', $output);
    $output = preg_replace('/src="path:([^"]+)"/', 'src="' . $base_path . $info['path'] . '/$1"', $output);
    $output = str_replace('&path&', $base_path . $info['path'] . '/', $output);

    // Change 'trans_path:' to the URL to the actual help directory.
    $output = preg_replace('/href="trans_path:([^"]+)"/', 'href="' . $base_path . $file_info['path'] . '/$1"', $output);
    $output = preg_replace('/src="trans_path:([^"]+)"/', 'src="' . $base_path . $file_info['path'] . '/$1"', $output);
    $output = str_replace('&trans_path&', $base_path . $file_info['path'] . '/', $output);

    // Change 'base_url:' to the URL to the site.
    $output = preg_replace('/href="base_url:([^"]+)"/', 'href="' . strtr(url('$1'), array('%24' => '$')) . '"', $output);
    $output = preg_replace('/src="base_url:([^"]+)"/', 'src="' . strtr(url('$1'), array('%24' => '$')) . '"', $output);
    $output = preg_replace('/&base_url&([^"]+)"/', strtr(url('$1'), array('%24' => '$')) . '"', $output);

    // Run the line break filter if requested.
    if (!empty($info['line break'])) {
      // Remove the header since it adds an extra <br /> to the filter.
      $output = preg_replace('/^<!--[^\n]*-->\n/', '', $output);

      $output = _filter_autop($output);
    }

    if (!empty($info['navigation'])) {
      $topics = advanced_help_get_topics();
      advanced_help_get_topic_hierarchy($topics);
      if (!empty($topics[$module][$topic]['children'])) {
        $items = advanced_help_get_tree($topics, $topics[$module][$topic]['children']);
        $output .= theme('item_list', array('items' => $items));
      }

      list($parent_module, $parent_topic) = $topics[$module][$topic]['_parent'];
      if ($parent_topic) {
        $parent = $topics[$module][$topic]['_parent'];
        $up = "help/$parent[0]/$parent[1]";
      }
      else {
        $up = "help/index";
      }

      $siblings = $topics[$parent_module][$parent_topic]['children'];
      uasort($siblings, 'advanced_help_uasort');
      $prev = $next = NULL;
      $found = FALSE;
      foreach ($siblings as $sibling) {
        list($sibling_module, $sibling_topic) = $sibling;
        if ($found) {
          $next = $sibling;
          break;
        }
        if ($sibling_module == $module && $sibling_topic == $topic) {
          $found = TRUE;
          continue;
        }
        $prev = $sibling;
      }

      if ($prev || $up || $next) {
        $navigation = '<div class="help-navigation clear-block">';

        if ($prev) {
          $navigation .= advanced_help_l('«« ' . $topics[$prev[0]][$prev[1]]['title'], "help/$prev[0]/$prev[1]", array('attributes' => array('class' => 'help-left')));
        }
        if ($up) {
          $navigation .= advanced_help_l(t('Up'), $up, array('attributes' => array('class' => $prev ? 'help-up' : 'help-up-noleft')));
        }
        if ($next) {
          $navigation .= advanced_help_l($topics[$next[0]][$next[1]]['title'] . ' »»', "help/$next[0]/$next[1]", array('attributes' => array('class' => 'help-right')));
        }

        $navigation .= '</div>';

        $output .= $navigation;
      }
    }

    if (!empty($info['css'])) {
      drupal_add_css($info['path'] . '/' . $info['css']);
    }

    $output = '<div class="advanced-help-topic">' . $output . '</div>';
    drupal_alter('advanced_help_topic', $output, $popup);

    return $output;
  }
}
