<?php
/**
 * @file
 * Implements common utilities.
 */

/**
 * Implements hook_form().
 */
function dss_visualizations_time_slider_form($form, &$form_state, $data_dimension) {
  // Creates default select and radio buttons.
  $options = array(
    0 => t('Unique'),
    1 => t('Range'),
  );

  $options_select = array(
    0 => t('Maximum'),
    1 => t('Minimum'),
    2 => t('Average'),
    3 => t('Diferencial'),
  );

  $form['time_slider'] = array(
    '#type' => 'radios',
    '#default_value' => 0,
    '#options' => $options,
  );

  $form['aggregation_select'] = array(
    '#type' => 'select',
    '#default_value' => 0,
    '#options' => $options_select,
  );

  // Multi dimension
  // Creates the weap level selects.
  if (is_array($data_dimension) && (count($data_dimension) > 0)) {
    $form['aggregation_select_dimension'] = array(
      '#type' => 'select',
      '#default_value' => 0,
      '#options' => $data_dimension,
    );
  }
  return $form;
}

/**
 * Implements hook_form().
 */
function dss_visualizations_map_form($form, &$form_state, $data_dimension, $variable_machine_name) {
  // Aggregation function.
  $options_select = array(
    0 => t('Valor Original'),
    1 => t('Max, Min, Averages'),
    2 => t('Summation'),
  );

  $form['aggregation_select'] = array(
    '#type' => 'select',
    '#default_value' => 0,
    '#options' => $options_select,
  );

  // Scale.
  $options_scale = array(
    0 => t('xMillon'),
    1 => t('x1000'),
    2 => t('x100'),
    3 => t('x10'),
    4 => t('x1'),
    5 => t('x/10'),
    6 => t('x/100'),
    7 => t('x/1000'),
    8 => t('x/Millon'),
  );

  $form['scale_select'] = array(
    '#type' => 'select',
    '#default_value' => 4,
    '#options' => $options_scale,
  );

  // Graph type.
  $options_view = array(
    0 => t('Serie de tiempo'),
    1 => t('Ciclo anual'),
    2 => t('Duracion'),
  );

  $form['view_graph_select'] = array(
    '#type' => 'select',
    '#default_value' => 0,
    '#options' => $options_view,
  );

  // Multi dimension
  // Creates the weap level selects.
  if (is_array($data_dimension) && (count($data_dimension) > 0)) {
    $form['aggregation_select_dimension'] = array(
      '#type' => 'select',
      '#default_value' => 0,
      '#options' => $data_dimension,
    );
  }

  return $form;
}

/**
 * Implements hook_form().
 */
function dss_visualizations_visualization_control_form($form, &$form_state, $weap_level_data) {
  $options_visualization_control_form = array(
    0 => t('Separated'),
    1 => t('Integrated'),
  );

  $form['visualization_control_select'] = array(
    '#type' => 'select',
    '#default_value' => 0,
    '#options' => $options_visualization_control_form,
  );

  return $form;
}

/**
 * Provides a way to store profile visualizations for Time & Space graph.
 */
function dss_visualizations_set_profile() {
  // Check if there's a profile post.
  if (isset($_POST['visualization_profile']) && !empty($_POST['visualization_profile'])) {
    $value_post = $_POST['visualization_profile'];

    // Get users's vocabulary settings.
    global $user;
    $account = user_load($user->uid);
    $profile = profile2_load_by_user($account, 'main');
    if (!$profile) {
      $profile = profile_create(array('type' => 'main', 'user' => $account));

      // Just in case that the profile cannot be created!
      if (!$profile) {
        return;
      }
    }
    $lang = LANGUAGE_NONE;
    $profile->field_visualization_vocabulary[$lang][0]['value'] = $value_post;
    profile2_save($profile);
  }
}

/**
 * Returns a Table with legend Information.
 *
 * @param array $data_array
 *   The Sima Resource (Caja de Datos).
 * @param string $table_id
 *   The Sima Dataset (DSS Variable).
 *
 * @return string
 *   The Table HTML.
 *
 * @throws \Exception
 *   If a field cannot be obtained, it will raise an exception.
 */
function dss_visualizations_table_legend($data_array, $table_id) {
  // Creating the Table.
  $rows = array();
  foreach ($data_array as $key => $value) {
    $key_string = (string) $key;
    if (($key_string == '0') || ($key_string == 'head')) {
      $header = $value;
    }
    else {
      array_push($rows, $value);
    }
  }

  return theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array('class' => array('dss-resource-leaflet-table'), 'id' => $table_id),
  ));
}
