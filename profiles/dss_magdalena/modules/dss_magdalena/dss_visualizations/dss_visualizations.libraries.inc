<?php
/**
 * @file
 * Hooks and functions to deal with loading libraries.
 */

use Drupal\dss_magdalena\DSS\Entity\SimaResource;
use Drupal\dss_magdalena\DSS\Entity\SimaDataset;

/**
 * Implements hook_libraries_info().
 */
function dss_visualizations_libraries_info() {
  // Declare return array.
  $libraries = array();

  // Add c3 library.
  $lib_attr = array(
    'name' => 'c3',
    'vendor url' => 'http://c3js.org/',
    'download url' => 'https://github.com/masayuki0812/c3/archive/0.4.11-rc4.zip',
    'lib prefix' => 'c3',
    'css prefix' => 'c3',
    'lib regex prefix' => 'v[0-9]',
    'add lib path' => TRUE,
  );
  $libraries += dss_visualizations_add_library($lib_attr);

  // Adds nvd3js library.
  $lib_attr = array(
    'name' => 'nvd3js',
    'vendor url' => 'http://nvd3.org/',
    'download url' => 'https://github.com/novus/nvd3/archive/v1.8.2.zip',
    'lib prefix' => 'nv.d3',
    'css prefix' => 'nv.d3',
    'lib regex prefix' => 'v[0-9]',
    'add lib path' => FALSE,
  );
  $libraries += dss_visualizations_add_library($lib_attr);

  // Adds dimple library.
  $lib_attr = array(
    'name' => 'dimple',
    'vendor url' => 'https://github.com/PMSI-AlignAlytics/dimple',
    'download url' => 'https://github.com/PMSI-AlignAlytics/dimple/releases/download/2.2.0/dimple.v2.2.0.min.js',
    'lib prefix' => 'dimple',
    'css prefix' => 'dimple',
    'lib regex prefix' => 'v[0-9].[0-9].[0-9]',
    'add lib path' => FALSE,
  );
  $libraries += dss_visualizations_add_library($lib_attr);

  // Adds d3plus library.
  $lib_attr = array(
    'name' => 'd3plus',
    'vendor url' => 'https://github.com/diego4serve/d3plus',
    'download url' => 'https://github.com/diego4serve/d3plus/archive/1.9.7-custom.zip',
    'lib prefix' => 'd3plus',
    'css prefix' => 'd3plus',
    'lib regex prefix' => 'v[0-9].[0-9].[0-9]',
    'add lib path' => FALSE,
  );
  $libraries += dss_visualizations_add_library($lib_attr);

  // Adds dc library.
  $lib_attr = array(
    'name' => 'dc',
    'vendor url' => 'https://github.com/dc-js/dc.js',
    'download url' => 'https://github.com/dc-js/dc.js/archive/2.0.1.zip',
    'lib prefix' => 'dc',
    'css prefix' => 'dc',
    'lib regex prefix' => 'v[0-9].[0-9].[0-9]',
    'add lib path' => FALSE,
  );
  $libraries += dss_visualizations_add_library($lib_attr);

  // Adds crossfilter library.
  $lib_attr = array(
    'name' => 'crossfilter',
    'vendor url' => 'https://github.com/square/crossfilter',
    'download url' => 'https://github.com/square/crossfilter/archive/v1.3.12.zip',
    'lib prefix' => 'crossfilter',
    'css prefix' => 'crossfilter',
    'lib regex prefix' => 'v[0-9].[0-9].[0-9]',
    'add lib path' => FALSE,
  );
  $libraries += dss_visualizations_add_library($lib_attr);

  // Adds dc_leaflet library.
  $lib_attr = array(
    'name' => 'dc_leaflet',
    'vendor url' => 'https://github.com/dc-js/dc.leaflet.js',
    'download url' => 'https://github.com/dc-js/dc.leaflet.js/archive/0.3.1.zip',
    'lib prefix' => 'dc.leaflet',
    'css prefix' => 'leaflet-legend',
    'lib regex prefix' => 'v[0-9]',
    'add lib path' => FALSE,
  );
  $libraries += dss_visualizations_add_library($lib_attr);

  // Adds bootstrap-slider library.
  $lib_attr = array(
    'name' => 'bootstrap-slider',
    'vendor url' => 'https://github.com/seiyria/bootstrap-slider',
    'download url' => 'https://github.com/seiyria/bootstrap-slider/archive/v9.7.2.zip',
    'lib prefix' => 'bootstrap-slider',
    'css prefix' => 'bootstrap-slider',
    'lib regex prefix' => 'v[0-9]',
    'add lib path' => FALSE,
  );
  $libraries += dss_visualizations_add_library($lib_attr);

  // Adds lz_string library.
  $lib_attr = array(
    'name' => 'lz_string',
    'vendor url' => 'https://github.com/pieroxy/lz-string',
    'download url' => 'https://github.com/pieroxy/lz-string/archive/1.4.4.zip',
    'lib prefix' => 'lz-string',
    'css prefix' => 'lz-string',
    'lib regex prefix' => 'v[0-9]',
    'add lib path' => FALSE,
  );
  $libraries += dss_visualizations_add_library($lib_attr);

  // Adds Spectrum library.
  $lib_attr = array(
    'name' => 'spectrum',
    'vendor url' => 'https://github.com/bgrins/spectrum',
    'download url' => 'https://github.com/bgrins/spectrum/archive/1.8.0.zip',
    'lib prefix' => 'spectrum',
    'css prefix' => 'spectrum',
    'lib regex prefix' => 'v[0-9]',
    'add lib path' => FALSE,
  );
  $libraries += dss_visualizations_add_library($lib_attr);

  // Adds leaflet fullscreen library.
  $lib_attr = array(
    'name' => 'leaflet_fullscreen',
    'vendor url' => 'https://github.com/brunob/leaflet.fullscreen',
    'download url' => 'https://github.com/brunob/leaflet.fullscreen/archive/1.1.4.zip',
    'lib prefix' => 'Control.FullScreen',
    'css prefix' => 'Control.FullScreen',
    'lib regex prefix' => 'v[0-9]',
    'add lib path' => FALSE,
  );
  $libraries += dss_visualizations_add_library($lib_attr);

  return $libraries;
}

/**
 * Helper function to register libraries.
 *
 * @param array $lib_attr
 *   Array with the library parameters.
 *
 * @return array
 *   Array with the library definition.
 */
function dss_visualizations_add_library($lib_attr) {
  // Define return array.
  $libraries = array();

  // Path to library, (if installed).
  $path_lib = libraries_get_path($lib_attr['name']);

  // Adding the nvd3 library.
  $files = array();

  // In the repository the files might me named nv.d3.js and nv.d3.min.js.
  $files += file_scan_directory($path_lib, '/' . $lib_attr['lib prefix'] . '.js|' . $lib_attr['lib prefix'] . '.min.js/', array('nomask' => '/(\.\.?|CVS|map)$/'));

  // They could also have the version # in the file name.
  $files += file_scan_directory($path_lib, '/' . $lib_attr['lib prefix'] . '.' . $lib_attr['lib regex prefix'] . '(.min)?.js/', array('nomask' => '/(\.\.?|CVS|map)$/'));

  // Get the CSS files.
  $css_files = array();
  $css_files += file_scan_directory($path_lib, '/' . $lib_attr['css prefix'] . '.css|' . $lib_attr['css prefix'] . '.min.css/');
  $css_files += file_scan_directory($path_lib, '/' . $lib_attr['css prefix'] . '.' . $lib_attr['lib regex prefix'] . '(.min)?.css/');

  // If we don't have any files we shouldn't add the library at all.
  // This will fire drupal error and direct the user to reports.
  if (count($files) == 0) {
    return $libraries;
  }

  // Select the compressed version of the ".js" file,
  // otherwise select the first one at $files.
  $matched_criteria = array_filter($files, function($var) {
    return preg_match("/min/i", $var->filename);
  });
  if (!empty($matched_criteria)) {
    // Sort matched criteria files from small path to large ones.
    usort($matched_criteria, function($a, $b) {
      return strlen($a->filename) - strlen($b->filename);
    });

    $files = $matched_criteria;
  }
  $file = array_shift($files);
  $css_file = (count($css_files) > 0) ? array_shift($css_files) : FALSE;
  $version = 0;

  // If this is a repository, there should be a package file. We can filter
  // out the version number.
  $package = file_exists($path_lib . '/package.json') ? file_get_contents($path_lib . '/package.json') : FALSE;
  if ($package) {
    preg_match('/\"version\"\:\ \"([0-9\.]*)\"/', $package, $version_matches);
    if (isset($version_matches[1])) {
      $version = $version_matches[1];
    }
  }

  // If this is from the zip file, we should be able to get the version
  // from the actual file itself.
  if (strpos($file->filename, '.v')) {
    preg_match('/\.v([0-9])/', $file->filename, $version_matches);
    if (isset($version_matches[1])) {
      $version = $version_matches[1];
    }
  }

  // Finding the location of the file.
  $library_path = pathinfo($file->uri)['dirname'];

  $css = array();
  if ($css_file) {
    $css_dir = pathinfo($css_file->uri)['dirname'];
    if ($css_dir == $library_path) {
      $css = array($css_file->filename);
    }
    else {
      $path_slice = str_replace($library_path . '/', '', $css_dir);
      $css = array($path_slice . '/' . $css_file->filename);
    }
  }

  $libraries[$lib_attr['name']] = array(
    'name' => $lib_attr['name'],
    'vendor url' => $lib_attr['vendor url'],
    'download url' => $lib_attr['download url'],
    // This will bypass the check for file_exists.
    'installed' => TRUE,
    'library path' => ($lib_attr['add lib path'] ? $path_lib : $library_path),
    'files' => array(
      'js' => array(
        $file->filename,
      ),
      'css' => $css,
    ),
    'version' => $version,
    'version arguments' => array(
      'file' => '' . $lib_attr['lib prefix'] . '.js',
      'pattern' => '/version\:\"([0-9\.]*)\"/',
      'lines' => 3,
      'cols' => 40000,
    ),
  );

  return $libraries;
}

/**
 * Implements hook_libraries_info_alter().
 */
function dss_visualizations_libraries_info_alter(&$libraries) {
  // Load the directory where the c3 example libraries are.
  $example_path = drupal_get_path('module', 'dss_visualizations') . '/libraries/';

  $all_libraries = array_merge(
    c3_default_libraries(),
    nvd3js_default_libraries(),
    dimple_default_libraries(),
    d3plus_default_libraries(),
    dc_leaflet_default_libraries()
  );
  foreach ($all_libraries as $library_name) {
    // Automatically add in the d3.drupal dependency so that each
    // d3.library doesn't have to.
    $libraries[$library_name]['installed'] = TRUE;
    $libraries[$library_name]['library path'] = $example_path . $library_name;
  }
}

/**
 * Implements hook_libraries_info_file_paths().
 */
function dss_visualizations_libraries_info_file_paths() {
  // Get all library directories.
  $libraries = libraries_get_libraries();

  $paths = array();
  // Output an array of paths to check for.
  foreach ($libraries as $path) {
    $paths[] = $path;
  }

  // Load the directory where the libraries are.
  $example_path = drupal_get_path('module', 'dss_visualizations') . '/libraries/';
  $all_libraries = array_merge(
    c3_default_libraries(),
    dimple_default_libraries(),
    d3plus_default_libraries(),
    nvd3js_default_libraries(),
    dc_leaflet_default_libraries()
  );

  // Add these to the search directories for libraries.
  foreach ($all_libraries as $example) {
    $paths[] = $example_path . $example;
  }

  return $paths;
}

/**
 * Helper callback to return all sample libraries located inside this module.
 */
function c3_default_libraries() {
  return array(
    'd3.c3.line_grids',
    'd3.c3.line_curve',
    'd3.c3.duration_curve',
    'd3.c3.flow_pattern',
    'd3.c3.pie',
    'd3.c3.histogram',
    'd3.c3.radar',
    'd3.c3.categories',
    'd3.c3.cause_effect_fixed',
    'd3.c3.stacked_balance',
    'd3.c3.area_stacked',
    'd3.c3.multi_objectives',
    'd3.c3.page_notice',
  );
}

/**
 * Helper callback to return all sample libraries located inside this module.
 */
function nvd3js_default_libraries() {
  return array(
    'd3.nvd3js.stacked_area',
  );
}

/**
 * Helper callback to return all sample libraries located inside this module.
 */
function dimple_default_libraries() {
  return array(
    'd3.dimple.multibars',
    'd3.dimple.balance',
    'd3.dimple.rectangular_pie',
  );
}

/**
 * Helper callback to return all sample libraries located inside this module.
 */
function d3plus_default_libraries() {
  return array(
    'd3.d3plus.cause_effect',
    'd3.d3plus.schematic',
    'd3.d3plus.boxplot',
  );
}

/**
 * Helper callback to return all sample libraries located inside this module.
 */
function dc_leaflet_default_libraries() {
  return array(
    'd3.dc_leaflet.map_aggregation',
    'd3.dc_leaflet.time_aggregation',
    'd3.dc_leaflet.choropleth_map',
    'd3.dc_leaflet.fragmentation',
    'd3.dc_leaflet.eloha_map',
  );
}

/**
 * Get the JSON visualization profile.
 *
 * @param object $account
 *   Drupla account object.
 *
 * @return string JSON
 *   JSON string with visualization profile
 */
function dss_get_visualization_profile($account) {
  $ret_val = "{}";

  $uid = user_load($account->uid);
  if ($uid) {
    $profile = profile2_load_by_user($uid, 'main');
    if ($profile) {
      $vocabulary_array = field_get_items('profile2', $profile, 'field_visualization_vocabulary');
      if ($vocabulary_array) {
        if (array_key_exists('value', reset($vocabulary_array))) {
          $vocabulary = reset($vocabulary_array)['value'];
          if (!empty($vocabulary)) {
            $ret_val = $vocabulary;
          }
        }
      }
    }
  }

  return $ret_val;
}

/**
 * Returns a Table with Resource Information.
 *
 * @param \Drupal\dss_magdalena\DSS\Entity\SimaResource $resource
 *   The Sima Resource (Caja de Datos).
 * @param \Drupal\dss_magdalena\DSS\Entity\SimaDataset $dataset
 *   The Sima Dataset (DSS Variable).
 *
 * @return string
 *   The Table HTML.
 *
 * @throws \Exception
 *   If a field cannot be obtained, it will raise an exception.
 */
function dss_visualizations_dynamic_table(SimaResource $resource, SimaDataset $dataset, $classname) {
  // Creating the Table for DataSet.
  $header_dataset = [t('Información de la Variable'), t('Value')];
  $rows_dataset = [
    [t('Units'), $dataset->getUnitOfMeasure()],
    [t('Range'), $dataset->getValueRange()],
    [t('Temporality'), $dataset->getTemporality()],
    [t('Modeling Variable'), $dataset->getModelingVariable()],
    [t('Variable Category'), $dataset->getVariableCategory()],
    [t('Type'), $dataset->getObjectType()],
    [t('Subsystem'), $dataset->getSubsystem()],
    [t('Node Category'), $dataset->getNodeCategory()],
    [t('Appropriate Spatial Scale'), $dataset->getSpatialScale()],
    [t('Numeric Scale'), $dataset->getNumericScale()],
    [t('Geometry'), $dataset->getGeometry()],
    [t('Number Of Dimensions'), $dataset->getDimensions()],
    [t('DPSIR'), $dataset->getDpsir()],
    [t('Associated Thematic Tree'), $dataset->getAssociatedThematicTree()],
  ];
  $dataset_table = theme('table', array(
    'header' => $header_dataset,
    'rows' => $rows_dataset,
    'attributes' => array('class' => array('dss-resource-leaflet-table'), 'id' => array($classname)),
  ));

  // Get layer information.
  if ($layer = $resource->getLayer()) {
    $spatial_layer = sprintf('%s (%s.geojson)', $layer->getTitle(), $layer->getMachineName());
  }
  else {
    $spatial_layer = t('Undefined');
  }

  // Creating the Table for Resource.
  $header_resource = [t('Resource Data'), t('Value')];
  $rows_resource = [
    [t("Nombre"), $resource->getTitle()],
    [t('Creation Date'), $resource->getCreationDate()],
    [t('Spatial Layer'), $spatial_layer],
    [t('Processing Type'), $resource->getProcessingType()],
    [t("ID del Caso de Estudio de Modelo"), $resource->getCaseStudy()->getTitle()],
  ];

  $resource_table = theme('table', array(
    'header' => $header_resource,
    'rows' => $rows_resource,
    'attributes' => array('class' => array('dss-resource-leaflet-table'), 'id' => array($classname)),
  ));

  return "<div id=\"" . $classname . "\">" . $resource_table . $dataset_table . "</div><br/>";
}
