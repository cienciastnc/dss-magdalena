/**
 * @file
 * Javascript for [library name].
 */

(function($) {

  /**
   * Adds library to the global d3 object.
   *
   * @param select
   * @param settings
   *   Array of values passed to d3_draw.
   *   id: required. This will be needed to attach your
   *       visualization to the DOM.
   */
  Drupal.d3.multibars = function (select, settings) {
    // Your custom JS.
    d3.select('#' + settings.id).append('div')
                                .attr('id', 'split1_chart');

    d3.select('#' + settings.id).append('div')
                                .attr('id', 'split2_chart');
    var data1 = settings.rows1;
    var data2 = settings.rows2;
    var order_chart1 = [];
    var order_chart2 = [];

    for (var i = 0; i < settings.rows1.length; i++) {
      order_chart1.push(settings.rows1[i].indice);
    }

    for (var i = 0; i < settings.rows2.length; i++) {
      order_chart2.push(settings.rows2[i].indice);
    }

    chart1(data1, "#split1_chart");
    chart2(data2, "#split2_chart");

    d3.select('#' + settings.id).append('div')
                                .attr('id', 'clear');

    function chart1(data, div) {
      var svg = dimple.newSvg(div, 600, 500);
      var myChart = new dimple.chart(svg, data);
      myChart.setBounds(60, 30, 450, 330);
      axisX = myChart.addCategoryAxis("x", ["indice"]);
      axisX.fontSize = "16px"
      axisX.addOrderRule(order_chart1);
      axisY = myChart.addPctAxis("y", "valor");
      axisY.fontSize = "16px"
      var mySeries = myChart.addSeries("propiedad", dimple.plot.bar);
      myLegend = myChart.addLegend(65, 10, 450, 20, "right");
      myLegend.fontSize = "14px;"
      myChart.draw();

      myChart.legends = [];
      var filterValues = dimple.getUniqueValues(data, "propiedad");
      myLegend.shapes.selectAll("rect")
        // Add a click event to each rectangle.
        .on("click", function (e) {
          // This indicates whether the item is already visible or not.
          var hide = false;
          var newFilters = [];
          // If the filters contain the clicked shape hide it.
          filterValues.forEach(function (f) {
            if (f === e.aggField.slice(-1)[0]) {
              hide = true;
            }
            else {
              newFilters.push(f);
            }
          });
          // Hide the shape or show it.
          if (hide) {
            d3.select(this).style("opacity", 0.2);
          }
          else {
            newFilters.push(e.aggField.slice(-1)[0]);
            d3.select(this).style("opacity", 0.8);
          }
          // Update the filters.
          filterValues = newFilters;
          // Filter the data.
          myChart.data = dimple.filterData(data, "propiedad", filterValues);
          // Passing a duration parameter makes the chart animate. Without
          // it there is no transition.
          myChart.draw();
        });
    }

    function chart2(data, div) {
      var svg = dimple.newSvg(div, 600, 500);
      var myChart = new dimple.chart(svg, data);
      myChart.setBounds(60, 30, 450, 330);
      axisX = myChart.addCategoryAxis("x", "indice");
      axisX.fontSize = "16px"
      axisX.addOrderRule(order_chart2);
      axisY = myChart.addMeasureAxis("y", "valor");
      axisY.fontSize = "16px"
      myChart.addSeries("indice", dimple.plot.bar);
      myLegend = myChart.addLegend(65, 10, 450, 20, "right");
      myLegend.fontSize = "14px;"
      myChart.draw();

      myChart.legends = [];
      var filterValues = dimple.getUniqueValues(data, "indice");
      myLegend.shapes.selectAll("rect")
        // Add a click event to each rectangle.
        .on("click", function (e) {
          // This indicates whether the item is already visible or not.
          var hide = false;
          var newFilters = [];
          // If the filters contain the clicked shape hide it.
          filterValues.forEach(function (f) {
            if (f === e.aggField.slice(-1)[0]) {
              hide = true;
            }
            else {
              newFilters.push(f);
            }
          });
          // Hide the shape or show it.
          if (hide) {
            d3.select(this).style("opacity", 0.2);
          }
          else {
            newFilters.push(e.aggField.slice(-1)[0]);
            d3.select(this).style("opacity", 0.8);
          }
          // Update the filters.
          filterValues = newFilters;
          // Filter the data.
          myChart.data = dimple.filterData(data, "indice", filterValues);
          // Passing a duration parameter makes the chart animate. Without
          // it there is no transition.
          myChart.draw();
        });
    }
  }

})(jQuery);
