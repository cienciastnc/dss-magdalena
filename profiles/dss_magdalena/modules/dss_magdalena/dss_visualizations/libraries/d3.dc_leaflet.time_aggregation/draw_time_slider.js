/**
 * Created by jclaros on 16/04/17.
 */

function drawTimeSlider(settings) {
    //Set jQuery variable for compatibility
    let $ = jQuery;

    //Set choropleth interface vars
    settings.nodes.forEach(function (e_node, e_index) {
        Drupal.settings.d3.inventory['leaflet_chart' + e_index.toString()].geomap = settings.geomap[parseInt(e_index)];
        Drupal.settings.d3.inventory['leaflet_chart' + e_index.toString()].data_ext = settings.data_ext[parseInt(e_index)];
    });

    //Sanity Check if there is data send
    if(settings.data.length === 0){
        alert("PRECAUCIÓN: NO HAY DATA A SER MOSTRADA.");
        setMessage(Drupal.t("PRECAUCIÓN: NO HAY DATA A SER MOSTRADA."));
    }else if(settings.data[0].length === 0) {
        alert("PRECAUCIÓN: NO HAY DATA A SER MOSTRADA.");
        setMessage(Drupal.t("PRECAUCIÓN: NO HAY DATA A SER MOSTRADA."));
    }

    //Get type of "value" data
    let hasMultipleDimension = false;
    let dimensionNames = [];
    if (typeof settings.data[0][0]['value'] !== "undefined") {
        let objValue = settings.data[0][0]['value'];
        if (isJson(objValue)) {
            //set dimension flag
            hasMultipleDimension = true;

            //Set dimension Names
            dimensionNames = Object.keys(JSON.parse(objValue));
        }
    }

    //Detecting temporality type of the graph
    //0 - normal var, 1 - not year, 2 - not timestep, 3 - not year and not
    // timestep, 4 - unable to detect
    let graph_temporality_type = 0;
    if ((typeof settings.data_ext !== "undefined") && (typeof settings.data_ext[0] !== "undefined")) {
        //Check year
        if ((settings.data_ext[0].max_year === null) || (settings.data_ext[0].min_year === null)) {
            graph_temporality_type = 1;
        }

        //Check timestep
        if ((settings.data_ext[0].max_timestep === null) || (settings.data_ext[0].min_timestep === null)) {
            //Ads the revious value to detect the third case
            graph_temporality_type = 2 + graph_temporality_type;
        }
    }
    else {
        graph_temporality_type = 4;
    }

    //Temporality - Sanity Check
    if (graph_temporality_type === 4) {
        setMessage(Drupal.t("WARNING: NO POSSIBLE TO DETECT THE VARIABLE TEMPORALITY."));
    }

    //Support for different types of temporality
    let none_year = "0";
    let none_timestep = "0";

    //Support for normal and diff vocabulary, control var and saved state vars
    let switch_legend_control = "0";    //Normal: 0, Diff: 1 vocabulary
    let legend_colors_saved = undefined;
    let legend_range_saved = undefined;
    let legend_label_saved = undefined;
    let legend_config_saved = undefined;
    let legend_length_saved = undefined;

    //Pre-processing data
    settings.data.forEach(function (e_data, e_index) {
        let element_key_array = [];
        let data_keyed = {};
        if (typeof e_data !== 'undefined') {
            if (e_data.length > 0) {
                for (let index = 0; index < e_data.length; ++index) {
                    //Set year data, if not define use default "none_year" value
                    let year_data = e_data[index]["year"];
                    if (year_data) {
                        //DO NOTHING
                    }
                    else {
                        year_data = none_year;
                    }

                    //Set timestep data, if not define use default
                    // "none_timestep" value
                    let timestep_data = e_data[index]["timestep"];
                    if (timestep_data) {
                        //DO NOTHING
                    }
                    else {
                        timestep_data = none_timestep;
                    }

                    // Set the composed key
                    let composed_year_key = year_data + '.' + timestep_data;

                    //Index Data
                    if (typeof data_keyed[getElementNameString(e_data[index]["geobranchid"].toLowerCase())] === "undefined") {
                        data_keyed[getElementNameString(e_data[index]["geobranchid"].toLowerCase())] = {};
                        element_key_array.push(getElementNameString(e_data[index]["geobranchid"].toLowerCase()));
                    }
                    data_keyed[getElementNameString(e_data[index]["geobranchid"].toLowerCase())][composed_year_key] = e_data[index]["value"];
                }
            }
            else {
                setMessage(Drupal.t("Empty data at node @node.", {
                    '@node': settings.nodes[parseInt(e_index)]
                }));
            }
        }
        else {
            setMessage(Drupal.t("Not data found at node @node.", {
                '@node': settings.nodes[parseInt(e_index)]
            }));
        }

        //Set choropleth interface vars
        Drupal.settings.d3.inventory['leaflet_chart' + e_index.toString()].data_keyed = data_keyed;
        Drupal.settings.d3.inventory['leaflet_chart' + e_index.toString()].element_key_array = element_key_array;

        //Set callback function
        Drupal.settings.d3.inventory['leaflet_chart' + e_index.toString()].updateStyleCallBack = callBackStyleUpdateMap;

        //Set Temporality type for each choropleth map
        Drupal.settings.d3.inventory['leaflet_chart' + e_index.toString()].graph_temporality_type = graph_temporality_type;
    });

    //Set Semaphore to true and let choroplet_map a go
    settings.semaphore = true;

    //Init element container for Max, Min, Average and Differential
    settings.nodes.forEach(function (e_node, e_index) {
        let data_aggregation = {};
        Drupal.settings.d3.inventory['leaflet_chart' + e_index.toString()].element_key_array.forEach(function (v_element) {
            data_aggregation[v_element] = {
                max: 0,
                min: 0,
                avg: 0,
                diff: 0,
            };
        });
        Drupal.settings.d3.inventory['leaflet_chart' + e_index.toString()].data_aggregation = data_aggregation;
    });

    //Calc number of steps on variable
    let num_steps_array = [];
    let num_slider_elements_array = [];
    let max_slider_array = [];
    settings.data_ext.forEach(function (e_data_ext, e_index) {
        if (typeof e_data_ext !== 'undefined') {
            //Calculate number of elements for timestep
            if (graph_temporality_type === 0 || graph_temporality_type === 1) {
                num_steps_array[e_index] = e_data_ext['max_timestep'] - e_data_ext['min_timestep'] + 1;
            }
            else {
                num_steps_array[e_index] = 1;
            }

            //Calculate number of elements for year
            if (graph_temporality_type === 0 || graph_temporality_type === 2) {
                num_slider_elements_array[e_index] = (e_data_ext['max_year'] - e_data_ext['min_year'] + 1) * num_steps_array[e_index];
            }
            else {
                num_slider_elements_array[e_index] = 1 * num_steps_array[e_index];
            }
        }
        else {
            num_slider_elements_array[e_index] = 1;
            setMessage(Drupal.t("Not extended data found @node.", {
                '@node': settings.nodes[parseInt(e_index)]
            }));
        }
        max_slider_array[e_index] = num_slider_elements_array[e_index] - 1;
    });
    //TODO: Hack, using the first one
    let num_steps = num_steps_array[0];
    let num_slider_elements = num_slider_elements_array[0];
    let max_slider = max_slider_array[0];

    //Set choropleth interface vars
    settings.nodes.forEach(function (e_node, e_index) {
        Drupal.settings.d3.inventory['leaflet_chart' + e_index.toString()].actual_time = set_data(0)['index'];
    });

    //Check the type of temporality from var and enable or disable slider bar
    // && radio buttons
    if (graph_temporality_type === 3 || graph_temporality_type === 4) {
        //Hide and disable radio buttons according temporality
        $('#edit-time-slider').hide().find('input, textarea').prop('disabled', true);
        $("#edit-aggregation-select").hide();
    }
    else {
        //Adds the slider to the html document first before working wit it
        if ($('input[name=time_slider]:checked').val() === "0") {
            document.getElementById("sima").innerHTML = '<div id="inputSlider" class="well"><input id="ex1" type="text" data-slider-id="exSlider" data-slider-min="0" data-slider-max="' + max_slider + '" data-slider-step="1" data-slider-value="0"/></div>';
            $("#edit-aggregation-select").hide();
        }
        else {
            document.getElementById("dblpointbar").innerHTML = '<div id="inputSlider2" class="well"><input id="ex2" type="text" data-slider-id="exSlider2" data-slider-min="0" data-slider-max="' + max_slider + '" data-slider-step="1" data-slider-value="[0,' + max_slider + ']"/></div>';
            $("#edit-aggregation-select").show();
        }
    }

    /** JQuery **/

        //Define jQuery vars
    let edit_aggregation_select_dimension = $("#edit-aggregation-select-dimension");
    let table_id = $("#" + settings.dina_table);
    let table_label_id = $("#" + settings.dina_label);

    //JQuery handles for Bootstrap Slider
    runJquerySlider();

    //Handle the radio input group on change using JQuery
    $('input[type="radio"]').on("change", function (e) {
        if ($('input[name=time_slider]:checked').val() === "0") {
            document.getElementById("dblpointbar").innerHTML = '';
            document.getElementById("sima").innerHTML = '<div id="inputSlider" class="well"><input id="ex1" type="text" data-slider-id="exSlider" data-slider-min="0" data-slider-max="' + max_slider + '" data-slider-step="1" data-slider-value="0"/></div>';
            $("#edit-aggregation-select").hide();

            //Restored switched vocabulary
            if (switch_legend_control === "1") {      //Differential vocabulary had been applied
                restore_vocabulary_legend();
            }

            //Exec Jquery slider function
            runJquerySlider();

            //Update Map with default value
            settings.nodes.forEach(function (e_node, e_index) {
                Drupal.settings.d3.inventory['leaflet_chart' + e_index.toString()].actual_time = set_data(0)['index'];
                Drupal.d3.choropleth_map['leaflet_chart' + e_index.toString()].geojson_setStyle();
            });
        }
        else {
            document.getElementById("sima").innerHTML = '';
            document.getElementById("dblpointbar").innerHTML = '<div id="inputSlider2" class="well"><input id="ex2" type="text" data-slider-id="exSlider2" data-slider-min="0" data-slider-max="' + max_slider + '" data-slider-step="1" data-slider-value="[0,' + max_slider + ']"/></div>';
            $("#edit-aggregation-select").show();

            //Sets diff vocabulary if differential view was selected before
            if (($("#edit-aggregation-select").find(':selected').val() === "3") && switch_legend_control === "0") { //Differential view selected
                switch_vocabulary_legend();
            }

            //Exec Jquery slider function
            runJquerySlider();

            // Update data and geojson style
            // note: update_aggregation_data uses slider data, so slider must
            // exit first, that's why runJquerySlider() is called first.
            settings.nodes.forEach(function (e_node, e_index) {
                update_aggregation_data('leaflet_chart' + e_index.toString());

                //Calling Choropleth map interface
                Drupal.d3.choropleth_map['leaflet_chart' + e_index.toString()].geojson_setStyle();
            });
        }
    });

    //Handle select change event using JQuery
    $("#edit-aggregation-select").on("change", function (e) {
        //Switch between normal and diff vocabulary
        if (($("#edit-aggregation-select").find(':selected').val() === "3") && switch_legend_control === "0") {      //Differential
            switch_vocabulary_legend();
        }
        else if (($("#edit-aggregation-select").find(':selected').val() !== "3") && switch_legend_control === "1") {
            restore_vocabulary_legend();
        }

        //Reset GeoJson layer style
        settings.nodes.forEach(function (e_node, e_index) {
            //Calling Choropleth map interface
            Drupal.d3.choropleth_map['leaflet_chart' + e_index.toString()].geojson_setStyle();
        });
    });

    //Handle multi-dimensions change event using JQuery
    edit_aggregation_select_dimension.on("change", function (e) {
        //Trigger an event to Update graph.
        if ($('input[name=time_slider]:checked').val() === "0") {
            if (graph_temporality_type === 3) {
                //Update choroplet style for none temporality
                settings.nodes.forEach(function (e_node, e_index) {
                    //Reset GeoJson layer style
                    Drupal.d3.choropleth_map['leaflet_chart' + e_index.toString()].geojson_setStyle();
                });
                //geojson_setStyle();
            }
            else {
                $("#ex1").trigger("change");
            }
        }
        else {
            $("#ex2").trigger("slideStop");
        }
    });

    //Set Hide and show of table of variable's properties
    table_id.hide();
    table_label_id.on("click", function (ev) {
        table_id.toggle();
    });

    //Update message
    Drupal.d3.page_notice();

    // Helper functions

    function runJquerySlider() {
        $("#ex1").on("change", function (slideEvt) {
            settings.nodes.forEach(function (e_node, e_index) {
                Drupal.settings.d3.inventory['leaflet_chart' + e_index.toString()].actual_time = set_data($('#ex1').slider('getValue'))['index'];

                //Reset GeoJson layer style
                Drupal.d3.choropleth_map['leaflet_chart' + e_index.toString()].geojson_setStyle();
            });
        });

        // Slider with JQuery
        $('#ex1').slider({
            tooltip: 'always',
            formatter: function (value) {
                let new_value = set_data(value);
                let ret_string = "";

                //Check if year should be displayed
                if (graph_temporality_type === 0 || graph_temporality_type === 2) {
                    ret_string += Drupal.t('Year: ') + new_value['year'];
                }

                //Check if month should be displayed
                if (num_steps > 1) {
                    ret_string += (ret_string === "" ? '' : ', ') + Drupal.t('Month') + ': ' + new_value['month_name'];
                }

                return ret_string;
            }
        });

        $("#ex2").on("slideStop", function (slideEvt) {
            settings.nodes.forEach(function (e_node, e_index) {
                //Update choropleth data
                update_aggregation_data('leaflet_chart' + e_index.toString());

                //Reset GeoJson layer style
                Drupal.d3.choropleth_map['leaflet_chart' + e_index.toString()].geojson_setStyle();
            });
        });

        $("#ex2").slider({
            tooltip: 'always',
            formatter: function (value) {
                //console.log(value);
                let new_value_left = set_data(value[0]);
                let new_value_right = set_data(value[1]);
                let ret_string_left = "";
                let ret_string_right = "";

                //Check if year should be displayed
                if (graph_temporality_type === 0 || graph_temporality_type === 2) {
                    ret_string_left = Drupal.t('Year: ') + new_value_left['year'];
                    ret_string_right = Drupal.t('Year: ') + new_value_right['year'];
                }

                //Check if month should be displayed
                if (num_steps > 1) {
                    ret_string_left += (ret_string_left === "" ? '' : ', ') + Drupal.t('Month') + ': ' + new_value_left['month_name'];
                    ret_string_right += (ret_string_right === "" ? '' : ', ') + Drupal.t('Month') + ': ' + new_value_right['month_name'];
                }

                return '[ ' + ret_string_left + ' ] : [ ' + ret_string_right + ' ]';
            }
        });
    }

    function set_data(value) {
        //Process slider data
        let v_year = 0;
        if (typeof settings.data_ext !== 'undefined') {
            //TODO: Hack, using settings.data_ext[0] assuming all have same
            // time period.
            if (graph_temporality_type === 0 || graph_temporality_type === 2) {
                v_year = Math.trunc(value / num_steps) + parseInt(settings.data_ext[0]['min_year']);
            }
            else {
                v_year = none_year;
            }

        }
        let v_month = Math.round(value % num_steps);
        let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        let v_month_name = months[v_month];
        let v_timestep = 0;
        if (graph_temporality_type === 0 || graph_temporality_type === 1) {
            v_timestep = v_month + 1;
        }
        else {
            v_timestep = none_timestep;
        }

        //Process multi-dimensions string
        let v_level_string = '';
        // for(var i=2; i < parseInt(settings.weap_level); i++) {
        //     v_level_string += '.' +
        // settings.weap_level_data[i-2][parseInt($('#edit-aggregation-select-level'
        // + (i-2).toString()).val())]; }

        return {
            year: v_year,
            timestep: v_timestep,
            month_name: v_month_name,
            level_string: v_level_string,
            index: (v_year + '.' + v_timestep + v_level_string)
        };
    }

    //Calculate Get Max, Min & Average
    function update_aggregation_data(v_choropleth_id) {
        //Get slider data
        let value = $('#ex2').slider('getValue');
        let start_time = set_data(value[0]);
        let end_time = set_data(value[1]);

        //Get data from select multi-dimensions
        let v_level_string = '';
        // for(var i=2; i < parseInt(settings.weap_level); i++) {
        //     v_level_string += '.' +
        // settings.weap_level_data[i-2][parseInt($('#edit-aggregation-select-level'
        // + (i-2).toString()).val())]; } Process multi-dimensions string
        let v_dimension_select_value = null;
        if (hasMultipleDimension) {
            v_dimension_select_value = edit_aggregation_select_dimension.find(':selected').val();
        }

        //Update aggregation data
        Drupal.settings.d3.inventory[v_choropleth_id].element_key_array.forEach(function (v_element) {
            //Iterate over start time to end time
            let v_values_array = [];
            let num_years = end_time["year"] - start_time["year"] + 1;
            let y = 1;
            let v_num_steps = 0;
            for (let i = 0; i < num_years; i++) {
                //Control initial timestep
                if (i === 0) {
                    y = start_time["timestep"];
                }
                else {
                    y = 1;
                }

                //Control final timestep
                if (i === (num_years - 1)) {
                    v_num_steps = end_time["timestep"];
                }
                else {
                    v_num_steps = num_steps;
                }

                for (; y < (v_num_steps + 1); y++) {
                    //Get data key
                    let year_step_string = (parseInt(start_time["year"]) + i).toString() + '.' + y.toString();

                    //Get data value
                    let v_scalar_value = hasMultipleDimension ? JSON.parse(Drupal.settings.d3.inventory[v_choropleth_id].data_keyed[v_element][year_step_string])[dimensionNames[v_dimension_select_value]] : (Drupal.settings.d3.inventory[v_choropleth_id].data_keyed[v_element][year_step_string]);

                    v_values_array.push(v_scalar_value);
                }
            }

            //Make aggregate calculations
            Drupal.settings.d3.inventory[v_choropleth_id].data_aggregation[v_element]["max"] = max_array(v_values_array);
            Drupal.settings.d3.inventory[v_choropleth_id].data_aggregation[v_element]["min"] = min_array(v_values_array);
            Drupal.settings.d3.inventory[v_choropleth_id].data_aggregation[v_element]["avg"] = avg_array(v_values_array);
            Drupal.settings.d3.inventory[v_choropleth_id].data_aggregation[v_element]["diff"] = diff_array(v_values_array);
        });

        /* Sub Helper functions */

        function max_array(v_array) {
            return parseFloat(Math.max.apply(null, v_array)).toFixed(2);
        }

        function min_array(v_array) {
            return parseFloat(Math.min.apply(null, v_array)).toFixed(2);
        }

        function avg_array(v_array) {
            let sum = 0;
            v_array.forEach(function (v_array_element) {
                sum += parseFloat(v_array_element);
            });

            return parseFloat(sum / v_array.length).toFixed(2);
        }

        function diff_array(v_array) {
            let array_length = v_array.length;

            //Sanity check
            if (array_length === 0) {
                return null;
            }

            let fist_element_index = 0;
            let last_element_index = array_length - 1;

            return parseFloat(Math.abs(parseFloat(v_array[last_element_index]) - parseFloat(v_array[fist_element_index]))).toFixed(2);
        }
    }

    function callBackStyleUpdateMap(v_map_name) {
        settings.nodes.forEach(function (e_node, e_index) {
            let map_name = 'leaflet_chart' + e_index.toString();
            if (map_name !== v_map_name) {     //Prevent calling function from calling itself
                // Update other map colors
                Drupal.settings.d3.inventory[map_name].legend_colors = Drupal.settings.d3.inventory[v_map_name].legend_colors;
                Drupal.settings.d3.inventory[map_name].legend_range = Drupal.settings.d3.inventory[v_map_name].legend_range;
                Drupal.settings.d3.inventory[map_name].legend_label = Drupal.settings.d3.inventory[v_map_name].legend_label;
                Drupal.settings.d3.inventory[map_name].legend_config = Drupal.settings.d3.inventory[v_map_name].legend_config;
                Drupal.settings.d3.inventory[map_name].legend_length = Drupal.settings.d3.inventory[v_map_name].legend_length;

                // Update color
                Drupal.d3.choropleth_map[map_name].geojson_setStyle();

                // Update legend
                Drupal.d3.choropleth_map[map_name].update_legend();
            }
        });
    }

    //Split branch names by "\"
    function getElementNameString(v_name) {
        if (v_name == null) {
            return '';
        }
        let splited_array = v_name.split("\\");
        return splited_array[splited_array.length - 1];
    }

    //Check if a the value passed is a json string object
    function isJson(item) {
        //Check if item is a string, if not change to json string
        item = typeof item !== "string" ? JSON.stringify(item) : item;

        //Check if it is an json object
        try {
            item = JSON.parse(item);
        }
        catch (e) {
            return false;
        }

        //Verify that the result is a javascript object
        if (typeof item === "object" && item !== null) {
            return true;
        }

        return false;
    }

    function switch_vocabulary_legend() {
        //Update control var
        switch_legend_control = "1";

        //Save choropleth interface vars, using node "0"
        let e_index_tmp = 0;
        legend_colors_saved = Drupal.settings.d3.inventory['leaflet_chart' + e_index_tmp.toString()].legend_colors;
        legend_range_saved = Drupal.settings.d3.inventory['leaflet_chart' + e_index_tmp.toString()].legend_range;
        legend_label_saved = Drupal.settings.d3.inventory['leaflet_chart' + e_index_tmp.toString()].legend_label;
        legend_config_saved = Drupal.settings.d3.inventory['leaflet_chart' + e_index_tmp.toString()].legend_config;
        legend_length_saved = Drupal.settings.d3.inventory['leaflet_chart' + e_index_tmp.toString()].legend_length;

        //Set new vocabulary
        settings.nodes.forEach(function (e_node, e_index) {
            //Set vocabulary to each choroplet
            Drupal.settings.d3.inventory['leaflet_chart' + e_index.toString()].legend_colors = Drupal.settings.d3.inventory['leaflet_chart' + e_index.toString()].legend_colors_diff;
            Drupal.settings.d3.inventory['leaflet_chart' + e_index.toString()].legend_range = Drupal.settings.d3.inventory['leaflet_chart' + e_index.toString()].legend_range_diff;
            Drupal.settings.d3.inventory['leaflet_chart' + e_index.toString()].legend_label = Drupal.settings.d3.inventory['leaflet_chart' + e_index.toString()].legend_label_diff;
            Drupal.settings.d3.inventory['leaflet_chart' + e_index.toString()].legend_config = Drupal.settings.d3.inventory['leaflet_chart' + e_index.toString()].legend_config_diff;
            Drupal.settings.d3.inventory['leaflet_chart' + e_index.toString()].legend_length = Drupal.settings.d3.inventory['leaflet_chart' + e_index.toString()].legend_length_diff;

            //Set control var
            Drupal.settings.d3.inventory['leaflet_chart' + e_index.toString()].switch_legend_control = switch_legend_control;

            // Update legend
            Drupal.d3.choropleth_map['leaflet_chart' + e_index.toString()].update_legend();
        });
    }

    function restore_vocabulary_legend() {
        //Update control var
        switch_legend_control = "0";

        //Set new vocabulary
        settings.nodes.forEach(function (e_node, e_index) {
            //Set vocabulary to each choroplet
            Drupal.settings.d3.inventory['leaflet_chart' + e_index.toString()].legend_colors = legend_colors_saved;
            Drupal.settings.d3.inventory['leaflet_chart' + e_index.toString()].legend_range = legend_range_saved;
            Drupal.settings.d3.inventory['leaflet_chart' + e_index.toString()].legend_label = legend_label_saved;
            Drupal.settings.d3.inventory['leaflet_chart' + e_index.toString()].legend_config = legend_config_saved;
            Drupal.settings.d3.inventory['leaflet_chart' + e_index.toString()].legend_length = legend_length_saved;

            //Set control var
            Drupal.settings.d3.inventory['leaflet_chart' + e_index.toString()].switch_legend_control = switch_legend_control;

            // Update legend
            Drupal.d3.choropleth_map['leaflet_chart' + e_index.toString()].update_legend();
        });
    }
}

function setMessage(v_str_msg) {
    //Create missing properties
    if (typeof Drupal.settings.d3.inventory.notice0 == "undefined") {
        Drupal.settings.d3.inventory.notice0 = {};
        Drupal.settings.d3.inventory.notice0.rows = [];
    }
    else if (typeof Drupal.settings.d3.inventory.notice0.rows == "undefined") {
        Drupal.settings.d3.inventory.notice0.rows = [];
    }

    //Add the msg string to message
    Drupal.settings.d3.inventory.notice0.rows.push(v_str_msg);

    /*let innerDivHtml = document.getElementById("msg").innerHTML;
    if(innerDivHtml !== ''){
        innerDivHtml = innerDivHtml + '<sub>, ' +v_str_msg +'</sub>';
    }else{
        innerDivHtml = '<sub>' + Drupal.t('Note') + ': '+ v_str_msg +'</sub>';
    }
    document.getElementById("msg").innerHTML = innerDivHtml;*/
}

function clearMessage() {
    document.getElementById("msg").innerHTML = '';
}