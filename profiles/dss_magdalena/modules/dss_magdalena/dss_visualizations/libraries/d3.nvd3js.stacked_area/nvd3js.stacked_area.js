/**
 * @file
 * Javascript for [library name].
 */

(function($) {

  /**
   * Adds library to the global d3 object.
   *
   * @param select
   * @param settings
   *   Array of values passed to d3_draw.
   *   id: required. This will be needed to attach your
   *       visualization to the DOM.
   */
  Drupal.d3.stacked_area = function(select, settings) {
    // Your custom JS.
    var colors = settings.colors;
    var data = [];

    for (var i = 0; i < settings.legend.length; i++) {
      var values = [];

      for (var j = 0; j < settings.rows[i].length; j++) {
        values.push([settings.rows[i][j][0], settings.rows[i][j][1]]);
      }

      data.push({
        "key": settings.legend[i],
        "values": values
      })
    }

    nv.addGraph(function() {
      var chart = nv.models.stackedAreaChart()
        .margin({
          right: 100
        })
          .x(function(d) {
            return d3.time.format("%Y-%m").parse(d[0]);
            // We can modify the data accessor functions...
          })
        .y(function(d) {
          return d[1]
          // ...in case your data is formatted differently.
        })
      // Tooltips which show all data points. Very nice!
        .useInteractiveGuideline(true)
      // Allow user to choose 'Stacked', 'Stream', 'Expanded' mode.
        .showControls(true)
        .clipEdge(true);

      // Format x-axis labels with custom function.
      chart.xAxis
        .axisLabel("Mes")
        .tickFormat(function(d) {
          return d3.time.format('%b')(new Date(d))
        });

      chart.yAxis
        .axisLabel("Flow (m3/s)")
        .tickFormat(d3.format(',.2f'));

      d3.select('#sima svg')
        .datum(data)
        .call(chart);

      nv.utils.windowResize(chart.update);

      return chart;
    });

    // Assign color to circle indicator on each td in table ELOHA.
    function assignIndColor(value) {
      if (value > 0 && value <= 0.09) {
        return colors.good
      }
      else if (value > 0.1 && value <= 0.19) {
        return colors.medium
      }
      else if (value > 0.20 && value <= 0.49) {
        return colors.bad
      }
      else {
        return colors.very_bad
      }
    }

    // Generate ELOHa table.
    var table = d3.select("#" + settings.id)
                  .append("table")
                  .attr("class", "eloha_table");
    var thead = table.append("thead").append("tr");
    thead.append("td").text("");
    thead.append("td").text("");

    var length = settings.rows[0].length;
    var monthNames = ["January", "February", "March", "April", "May", "June",
      "July", "August", "September", "October", "November", "December"
    ];
    for (var i = 0; i < length; i++) {
      var date = d3.time.format("%Y-%m").parse(settings.rows[0][i][0]);
      var text = settings.rows[0][i][0];
      thead.append("td").text(monthNames[date.getMonth()]);
    }

    var tbody = table.append("tbody");
    var array_length = settings.rows.length;

    for (var i = 0; i < array_length; i++) {
      // Generate EcoDef rows.
      var row1 = tbody.append("tr")
                      .attr("class", "row-" + i.toString());
      row1.append("td").attr("rowspan", 2)
                       .text(settings.legend[i]);
      row1.append("td").text("EcoDef");
      var row_length = settings.rows[i].length

      // Fill EcoDef rows.
      for (var j = 0; j < row_length; j++) {
        var td = row1.append("td").style("text-align", "right");
        var svg_ind = td.append("svg").attr("class", "ind")
                                    .attr("width", 20)
                                    .attr("height", 20);
        var ind_color = assignIndColor(settings.rows[i][j][1]);
        svg_ind.append("circle").attr("cx", 10)
                                .attr("cy", 15)
                                .attr("r", 5)
                                .style("stroke", ind_color)
                                .style("fill", ind_color);
        td.append("span").text(settings.rows[i][j][1])
                         .attr("class", "ind");
      }

      // Generate EcoSur rows.
      var row2 = tbody.append("tr")
                      .attr("class", "row-" + i.toString());
      row2.append("td").text("EcoSur");

      // Fill EcoSur rows.
      for (var j = 0; j < row_length; j++) {
        var num = Math.round((settings.rows[i][j][1] - 0.1) * 100) / 100;
        var td = row2.append("td").style("text-align", "right");
        var svg_ind = td.append("svg").attr("class", "ind")
                                    .attr("width", 20)
                                    .attr("height", 20);
        var ind_color = assignIndColor(num);
        svg_ind.append("circle").attr("cx", 10)
                                .attr("cy", 15)
                                .attr("r", 5)
                                .style("stroke", ind_color)
                                .style("fill", ind_color);
        td.append("span").text(num)
                         .attr("class", "ind");
      }

    }
    setTimeout(function(){
      // Do what you need here.
      for (var i = 0; i < array_length; i++) {
        var row_color = d3.select(".nv-area-" + i.toString())
                          .style("fill");
        var rows = d3.selectAll("tr.row-" + i.toString());
        rows.style("background-color", row_color);
      }

    // Remove text from x axis
    d3.selectAll('g.nv-x.nv-axis.nvd3-svg text').remove();
    }, 1000);
  }
})(jQuery);
