/**
 * @file
 * Javascript for [library name].
 */

(function ($) {

  /**
   * Adds library to the global d3 object.
   *
   * @param select
   * @param settings
   *   Array of values passed to d3_draw.
   *   id: required. This will be needed to attach your
   *       visualization to the DOM.
   */
  Drupal.d3.duration_curve = function (select, settings) {
    // Your custom JS.
    var margin = { top: 30, right: 20, bottom: 35, left: 50 },
      width = 600 - margin.left - margin.right,
      height = 270 - margin.top - margin.bottom;

    var x = d3.time.scale().range([0, width]);
    var y = d3.scale.linear().range([height, 0]);

    var xAxis = d3.svg.axis()
      .scale(x)
      .orient("bottom")
      .ticks(10);

    var yAxis = d3.svg.axis()
      .scale(y)
      .orient("left")
      .ticks(5);

    var area = d3.svg.area()
      .x(function (d) { return x(d.q);
      })
      .y0(function (d) { return y(d.min);
      })
      .y1(function (d) { return y(d.close);
      });

    var valueline = d3.svg.line()
      .x(function (d) { return x(d.q);
      })
      .y(function (d) { return y(d.close);
      });

    var svg = d3.select('#' + settings.id)
      .append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
      .append("g")
      .attr("transform",
      "translate(" + margin.left + "," + margin.top + ")");

    // Function for the x grid lines.
    function make_x_axis() {
      return d3.svg.axis()
        .scale(x)
        .orient("bottom")
        .ticks(5)
    }

    // Function for the y grid lines.
    function make_y_axis() {
      return d3.svg.axis()
        .scale(y)
        .orient("left")
        .ticks(5)
    }

    // Get the data.
    var data = settings.rows;

    data.forEach(function (d) {
      d.q = d.q;
      d.close = +d.close;
    });

    // Scale the range of the data.
    x.domain(d3.extent(data, function (d) { return d.q;
    }));
    y.domain([0, d3.max(data, function (d) { return d.close;
    })]);

    var bucket = [];
    var prev = undefined;
    var datanew = [];

    data.forEach(function(d){
      if (d.min >= 500) {
        if (prev && prev.min < 500) {
          datanew.push(bucket);
          bucket = [];
        }
        bucket.push(d);
      }
      else if (d.min >= 300) {
        if (prev && prev.min < 300) {
          datanew.push(bucket);
          bucket = [];
        }
        else if (prev && prev.min >= 500) {
          datanew.push(bucket);
          bucket = [];
        }
        bucket.push(d);
      }
      else if (d.min >= 100) {
        if (prev && prev.min < 100) {
          datanew.push(bucket);
          bucket = [];
        }
        else if (prev && prev.min >= 300) {
          datanew.push(bucket);
          bucket = [];
        }
        bucket.push(d);
      }
      else if (d.min >= 30) {
        if (prev && prev.min < 30) {
          datanew.push(bucket);
          bucket = [];
        }
        else if (prev && prev.min >= 100) {
          datanew.push(bucket);
          bucket = [];
        }
        bucket.push(d);
      }
      prev = d;
    });

    // Datanew will hold the chunk of data with close > 150.
    datanew.push(bucket);
    debugger
    // Iterate through the chunks.
    datanew.forEach(function(d){
      console.log(d)
      if (d[0].min >= 500) {
        // Less than 150 so make it red as this chunk is for less than 150.
        c = "yellow";
      }
      else if (d[0].min >= 300) {
        // Greater than 150 so make it blue as this chunk is for greater than 150.
        c = "blue";
      }
      else if (d[0].min >= 100) {
        c = "orange";
      }
      else if (d[0].min >= 30) {
        c = "red";
      }
      else {
        c = "gray";
      }
      svg.append("path")
        .datum(d)
        .attr("class", "area")
      // Color selected above.
        .attr("stroke", c)
        .style("fill", c)
        .attr("d", area);
    });

    // Add the filled area
    // svg.append("path")
    //   .datum(data)
    //   .attr("class", "area")
    //   .attr("d", area);.
    // Draw the x Grid lines.
    svg.append("g")
      .attr("class", "grid")
      .attr("transform", "translate(0," + height + ")")
      .call(make_x_axis()
        .tickSize(-height, 0, 0)
        .tickFormat("")
      )

    // Draw the y Grid lines.
    svg.append("g")
      .attr("class", "grid")
      .call(make_y_axis()
        .tickSize(-width, 0, 0)
        .tickFormat("")
      )

    // Add the valueline path.
    svg.append("path")
      .attr("d", valueline(data));

    // Add the X Axis.
    svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis);

    // Add the Y Axis.
    svg.append("g")
      .attr("class", "y axis")
      .call(yAxis);

    // Add the text label for the X axis.
    svg.append("text")
      .attr("transform",
      "translate(" + (width / 2) + " ," +
      (height + margin.bottom) + ")")
      .style("text-anchor", "middle")
      .text(settings.legends.x);

    // Add the white background to the y axis label for legibility
    // svg.append("text")
    //   .attr("transform", "rotate(-90)")
    //   .attr("y", 6)
    //   .attr("x", margin.top - (height / 2))
    //   .attr("dy", ".71em")
    //   .style("text-anchor", "end")
    //   .attr("class", "shadow")
    //   .text(settings.legends.y);.
    // Add the text label for the Y axis
    // svg.append("text")
    //   .attr("transform", "rotate(-90)")
    //   .attr("y", 6)
    //   .attr("x", margin.top - (height / 2))
    //   .attr("dy", ".71em")
    //   .style("text-anchor", "end")
    //   .text(settings.legends.y);.
  }

})(jQuery);
