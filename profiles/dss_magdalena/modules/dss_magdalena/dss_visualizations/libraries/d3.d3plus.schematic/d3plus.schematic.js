/**
 * @file
 * Javascript for [library name].
 */

(function($) {

  /**
   * Adds library to the global d3 object.
   *
   * @param select
   * @param settings
   *   Array of values passed to d3_draw.
   *   id: required. This will be needed to attach your
   *       visualization to the DOM.
   */
  Drupal.d3.schematic = function (select, settings) {
    // Your custom JS.
    var colors = settings.colors;
    var container = d3.select("#" + settings.id).append("div")
                                          .attr("id", "container");
    var container_width = parseInt(d3.select("div#container")
                                     .style("width"));
    var data_width = 0;
    var km_sum = 0;

    var array_length = settings.rows.length;
    for (var i = 0; i < array_length; i++) {
      km_sum = 0;
      var data_length = settings.rows[i].data.length;
      for (var j = 0; j < data_length; j++) {
        var km_sum = km_sum + settings.rows[i].data[j].km;
      }
      drawRow(i, array_length, data_length, km_sum);
    }

    function drawRow(i, array_length, data_length, km_sum) {
      var row = container.append("div").attr("class", "chart_row");
      data_width = container_width - 100;
      row.append("div").attr("class", "ind_label inline left")
                       .text(settings.rows[i].indice.substring(0, 20));

      var data = row.append("div").attr("class", "chart_data inline right")
                       .style("width", String(data_width) + "px");
      row.append("div").attr("class", "clear");

      var ul = data.append("ul").attr("class", "chart_ul");

      for (var j = 0; j < data_length; j++) {
        var pixels = ((data_width * settings.rows[i].data[j].km) / km_sum) - 1;
        var color = assignColor(settings.rows[i].data[j].value);
        var li = ul.append("li").attr("class", "chart_li inline")
                                 .style("width", String(pixels) + "px")
                                 .style("height", "70px")
                                 .style("background-color", color);

        var text = "Valor: " + settings.rows[i].data[j].value + "Tramo: " +
                    settings.rows[i].data[j].km + "\n km";
        li.append('span').attr('class', 'tooltiptext')
                         .text(text);
      }

      function assignColor(value) {
        if (value > 0 && value <= 0.2) {
          return colors.very_bad;
        }
        else if (value > 0.2 && value <= 0.4) {
          return colors.bad
        }
        else if (value > 0.4 && value <= 0.6) {
          return colors.medium
        }
        else if (value > 0.6 && value <= 0.8) {
          return colors.good
        }
        else if (value > 0.8 && value <= 1) {
          return colors.very_good
        }
        else {
          return colors.not_match
        }
      }
    }

    var svg = d3.select('div#container').append('svg')
                        .attr('width', data_width + 10)
                        .attr('height', 70)
                        .style("margin-left", 100);

    var x = d3.scale.linear()
              .domain([0, km_sum])
              .range([0, data_width]);
    var xAxis = d3.svg.axis()
                  .scale(x)
                  .tickValues(d3.range(0, km_sum, 10))
                  .orient("bottom");

    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + 10 + ")")
        .call(xAxis);

    svg.append("text")
      .attr("transform",
      "translate(" + (data_width / 2) + " ," + 50 + ")")
      .style("text-anchor", "middle")
      .text('Tramo (en KM)');

    var table = container.append("table")
                         .attr("class", "legend-table");

    var thead = table.append("thead").append("tr");
    thead.append("td").text("Estado");
    thead.append("td").text("Rango de valor");
    thead.append("td").text("Color");
    
    var tbody = table.append("tbody");
    var tr = tbody.append('tr');
    tr.append("td").text('Muy bueno');
    tr.append("td").text('1 - 0.8');
    tr.append("td").style('background-color', colors.very_good);
    
    var tr = tbody.append('tr');
    tr.append("td").text('Bueno');
    tr.append("td").text('0.79 - 0.6');
    tr.append("td").style('background-color', colors.good);

    var tr = tbody.append('tr');
    tr.append("td").text('Medio');
    tr.append("td").text('0.59 - 0.4');
    tr.append("td").style('background-color', colors.medium);

    var tr = tbody.append('tr');
    tr.append("td").text('Mal');
    tr.append("td").text('0.39 - 0.2');
    tr.append("td").style('background-color', colors.bad);
    
    var tr = tbody.append('tr');
    tr.append("td").text('Muy Mal');
    tr.append("td").text('0.19 - 0.0');
    tr.append("td").style('background-color', colors.very_bad);
  }

})(jQuery);
