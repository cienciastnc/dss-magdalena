/**
 * @file
 * Javascript for [library name].
 */

(function($) {

    /**
     * Adds library to the global d3 object.
     *
     * @param select
     * @param settings
     *   Array of values passed to d3_draw.
     *   id: required. This will be needed to attach your
     *       visualization to the DOM.
     */
    Drupal.d3.fragmentation = function (select, settings) {
        // Obtaining GeoJSON and DataStore URLs.
        var page_url = location.pathname.split("/");
        var rid = page_url[2];
        var data_url = Drupal.settings.basePath + 'resource-data/' + rid;

        //Check if data is cached
        //localStorage.clear();
        if(localStorage.getItem('resource-data-'+rid) != null ) {
            //Get decompressed object
            var obj_string = LZString.decompressFromUTF16(localStorage.getItem('resource-data-'+rid));
            var obj_data = JSON.parse(obj_string);
            settings.geomap = obj_data.geomap;
            settings.data_ext = obj_data.data_ext;
            settings.data = obj_data.data;

            //Call graph
            // if(settings.layer_machine_name =='magdalena_streamsline_topology'){
            //     drawFragmentation(settings);
            // }else{
            //     drawMapChoropleth(settings);
            // }
            drawMapChoropleth(settings);
        }else{
            //Getting data
            $.getJSON(data_url, function (json) {
                //Getting data
                geojson = JSON.parse(json.layer);
                datastore = JSON.parse(json.datastore);
                settings.geomap = geojson;
                settings.data = datastore.items;
                settings.data_ext = datastore.ext;

                //Creates the object to be stored
                var string_obj_to_storage = JSON.stringify({
                    geomap : settings.geomap,
                    data_ext : settings.data_ext,
                    data : settings.data
                });

                //Compress and Store object to cache
                try {
                    localStorage.setItem('resource-data-' + rid, LZString.compressToUTF16(string_obj_to_storage));
                } catch (e) {
                    localStorage.removeItem('resource-data-' + rid);
                    setMessage("Not enough cache storage on web browser.");
                }

                //Call graph
                // if(settings.layer_machine_name =='magdalena_streamsline_topology'){
                //     drawFragmentation(settings);
                // }
            });
        }

        function drawFragmentation(settings){
            $("#curve").hide();

            //Check if there is a GeoJson
            if(settings.geomap == null){
                setMessage("There is no GeoJson associated to this variable.");
            }

            //Preprocessing data
            var data_keyed = {};
            if(typeof settings.data !== 'undefined') {
                for (var index = 0; index < settings.data.length; ++index) {
                    //var composed_year_key = settings.data[index]["year"] + '.' + settings.data[index]["timestep"];
                    if (typeof data_keyed[parseInt(settings.data[index]["branchid"])] === "undefined") {
                        data_keyed[parseInt(settings.data[index]["branchid"])] = {};
                    }
                    data_keyed[parseInt(settings.data[index]["branchid"])] = parseInt(settings.data[index]['value']);
                }
            }else{
                setMessage("Not data found.");
            }
            settings.data_keyed = data_keyed;

            //Calc Legend range
            var legend_range = [0, 2, 5, 10, 15, 25, 50, 100];

            //Create leaflet layers
            var color_tile = L.tileLayer( 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
                subdomains: ['a','b','c']
            });

            var gray_tile = L.tileLayer.grayscale('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
                subdomains: ['a','b','c']
            });

            var empty_tile =  L.tileLayer('');

            //Create the leaflet map
            var mymap = L.map('sima', {
                layers : [color_tile]
            }).setView([6.5, -75], 6);

            //Layer Control
            var baseMaps = {
                "Color": color_tile,
                "Gris": gray_tile,
                "Blanco" : empty_tile
            };
            L.control.layers(baseMaps, null, {position: 'bottomleft'}).addTo(mymap);

            //Adds the GeoJson Layer
            var geojson = L.geoJSON(settings.geomap, {
                style: style
            }).addTo(mymap);

            //Creates the legend on the map
            var legend = L.control({position: 'bottomright'});
            legend.onAdd = function (map) {

                var div = L.DomUtil.create('div', 'info legend'),
                    grades = legend_range,
                    labels = [];

                // loop through our density intervals and generate a label with a colored square for each interval
                for (var i = 0; i < grades.length; i++) {
                    div.innerHTML +=
                        '<i style="background:' + getColor(grades[i] + 1) + '"></i> ' +
                        grades[i] + (grades[i + 1] ? '&ndash;' + grades[i + 1] + '<br>' : '+');
                }

                return div;
            };
            legend.addTo(mymap);

            function style(feature) {
                //Define Styles for no data
                var no_data_style = {
                    fillColor : 'gray',
                    weight : 0.8,
                    opacity : 0.7,
                    color : 'black',
                    fillOpacity : 0.8
                };

                //Define Styles
                var line_style = {
                    //fillColor : getColor(element_value),
                    weight : widthFlow(feature.properties),
                    opacity : 0.7,
                    color : getColor(parseInt(settings.data_keyed[parseInt(feature.properties.ARCID)])),
                    fillOpacity : 0.4
                };

                //Get the style to be applied
                var returned_style = line_style;

                return returned_style;

                /* Sub Helper function */

                function widthFlow(properties){
                    //Calc Q_med_m3s for line weight
                    //Default value
                    var width_flow = 0.5;

                    //Calc the width flow with and offset of 0.5, just to be always visible
                    if (properties.Q_med_m3s != null){
                        width_flow = Math.sqrt(parseFloat(feature.properties.Q_med_m3s))/14 +0.5;
                    }

                    return width_flow;
                }
            }

            //Set color for the graph
            //TODO: Update this function to use vocabulary
            function getColor(d) {
                return d > legend_range[7] ? '#D51619' :
                    d > legend_range[6]  ? '#EA693D' :
                        d > legend_range[5]  ? '#FBB86C' :
                            d > legend_range[4]  ? '#FCE5A2' :
                                d > legend_range[3]   ? '#E4F1C7' :
                                    d > legend_range[2]   ? '#B5DCE1' :
                                        d > legend_range[1]   ? '#72AED1' :
                                            '#2879B4';
            }
        }

        function setMessage(v_str_msg){
            var innerDivHtml = document.getElementById("msg").innerHTML;
            if(innerDivHtml != ''){
                innerDivHtml = innerDivHtml + '<sub>, ' +v_str_msg +'</sub>';
            }else{
                innerDivHtml = '<sub>Note: '+ v_str_msg +'</sub>';
            }
            document.getElementById("msg").innerHTML = innerDivHtml;
        }

        function clearMessage(){
            document.getElementById("msg").innerHTML = '';
        }
    }

})(jQuery);