/**
 * Created by jclaros on 16/04/17.
 */

Drupal.d3.drawMultiObjectives = function (settings) {
    //Set jQuery variable for compatibility
    let $ = jQuery;

    /** JQuery **/

    //Define jQuery vars
    let edit_chart_type_select = $("#edit-chart-type");
    let radar_graph = $('#c_radar');
    let bar_graph = $('#c_bar');
    let table_graph = $('#sima');

    //Sets type of graphs
    let type_graphs = [radar_graph, bar_graph, table_graph];

    //Get default select option
    hide_all_graphs();
    let select_value = parseInt(edit_chart_type_select.find(':selected').val());
    type_graphs[select_value].show();

    //Handle Selected scale new changes
    edit_chart_type_select.on("change", function(slideEvt) {
        //Update seected graph
        hide_all_graphs();
        let select_chart_type_value = parseInt(edit_chart_type_select.find(':selected').val());
        type_graphs[select_chart_type_value].show();
    });

    /** Helper functions **/

    function hide_all_graphs(){
        radar_graph.hide();
        bar_graph.hide();
        table_graph.hide();
    }

    // Generate HTML Table.
    let table = d3.select('#' + settings.id).append('table').attr('id', 'multiobjectives_table');
    let thead = table.append('thead');
    let tbody = table.append('tbody');

    // Fill table header.
    let thead_tr = thead.append('tr');
    thead_tr.append('th').text(Drupal.t('Objetivos'));
    for (let i = 0; i < settings.legend.length; i++) {
        thead_tr.append('th').text(settings.legend[i]);
    }

    // Fill table body.
    if(settings.rows.length > 0) {      //Sanity Check
        for (let m = 0; m < settings.rows[0].length; m++) {
            let tbody_tr = tbody.append('tr');
            tbody_tr.append('td').text(settings.rows[0][m][0]);
            for (let y = 0; y < settings.rows.length; y++) {
                tbody_tr.append('td').text(settings.rows[y][m][1]);
            }
        }
    }
}