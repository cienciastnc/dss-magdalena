/**
 * @file
 * Javascript for [library name].
 */

(function($) {

    /**
     * Adds library to the global d3 object.
     *
     * @param select
     * @param settings
     *   Array of values passed to d3_draw.
     *   id: required. This will be needed to attach your
     *       visualization to the DOM.
     */
    Drupal.d3.multi_objectives = function (select, settings) {
/*        // Obtaining GeoJSON and DataStore URLs.
        var page_url = location.pathname.split("/");
        var rid_array = settings.nodes_rid;
        settings.nodes = rid_array;

        //Load data from nodes on rid_array
        //localStorage.clear();
        var ajax_results = [];
        settings.geomap = [];
        settings.data_ext = [];
        settings.data = [];
        settings.data_keyed = [];
        settings.data_mean_keyed = [];
        rid_array.forEach(function(e_rid, e_index){
            //Check if data is cached
            if(localStorage.getItem('resource-data-'+e_rid) !== null ) {
                //Get decompressed object
                var obj_string = LZString.decompressFromUTF16(localStorage.getItem('resource-data-'+e_rid));
                var obj_data = JSON.parse(obj_string);
                settings.geomap[e_index] = obj_data.geomap;
                settings.data_ext[e_index] = obj_data.data_ext;
                settings.data[e_index] = obj_data.data;
            }else{
                //Getting data
                var data_url = Drupal.settings.basePath + 'resource-data/' + e_rid;
                ajax_results.push($.getJSON(data_url, function (json) {
                    //Getting data
                    var geojson = JSON.parse(json.layer);
                    var datastore = JSON.parse(json.datastore);
                    settings.geomap[e_index] = geojson;
                    settings.data[e_index] = datastore.items;
                    settings.data_ext[e_index] = datastore.ext;

                    //Creates the object to be stored
                    var string_obj_to_storage = JSON.stringify({
                        geomap : geojson,
                        data_ext : datastore.ext,
                        data : datastore.items
                    });

                    //Compress and Store object to cache
                    try {
                        localStorage.setItem('resource-data-' + e_rid, LZString.compressToUTF16(string_obj_to_storage));
                    } catch (e) {
                        localStorage.removeItem('resource-data-' + e_rid);
                        setMessage(Drupal.t("Not enough cache to storage on web browser."));
                    }
                }));
            }
        });
*/
        //Call graph
//        $.when.apply(this, ajax_results).done(function(){
            Drupal.d3.drawMultiObjectives(settings);
//        });

        //
        function setMessage(v_str_msg){
            var innerDivHtml = document.getElementById("msg").innerHTML;
            if(innerDivHtml != ''){
                innerDivHtml = innerDivHtml + '<sub>, ' +v_str_msg +'</sub>';
            }else{
                innerDivHtml = '<sub>' + Drupal.t('Note') + ': '+ v_str_msg +'</sub>';
            }
            document.getElementById("msg").innerHTML = innerDivHtml;
        }

        function clearMessage(){
            document.getElementById("msg").innerHTML = '';
        }
    }

})(jQuery);