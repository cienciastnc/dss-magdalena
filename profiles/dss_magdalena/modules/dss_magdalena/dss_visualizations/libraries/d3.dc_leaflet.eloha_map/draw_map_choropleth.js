/**
 * Created by jclaros on 16/04/17.
 */

Drupal.d3.drawMapChoropleth = function (settings) {
    //Set jQuery variable for compatibility
    let $ = jQuery;

    //Check if there is a GeoJson
    settings.geomap.forEach(function (e_geomap, e_index) {
        if (e_geomap === null) {
            setMessage(Drupal.t("There is no GeoJson associated to node @node.", {
                '@node': settings.nodes[parseInt(e_index)]
            }));
        }
    });

    //Sanity Check if there is data send
    if(settings.data.length === 0){
        alert("PRECAUCIÓN: NO HAY DATA A SER MOSTRADA.");
        setMessage(Drupal.t("PRECAUCIÓN: NO HAY DATA A SER MOSTRADA."));
    }else if(settings.data[0].length === 0) {
        alert("PRECAUCIÓN: NO HAY DATA A SER MOSTRADA.");
        setMessage(Drupal.t("PRECAUCIÓN: NO HAY DATA A SER MOSTRADA."));
    }

    //Pre-processing data
    //settings.data_keyed = [];
    settings.data.forEach(function (e_data, e_index) {
        let data_keyed = {};
        if (typeof e_data !== 'undefined') {
            if (e_data.length > 0) {
                for (let index = 0; index < e_data.length; ++index) {
                    // Set composed key
                    let composed_label_key = e_data[index]["timestep"];
                    if (composed_label_key === null || composed_label_key === '') {    //Detects Verdict data
                        composed_label_key = '0';
                    }

                    //Sanity Check
                    if (e_data[index]["geobranchid"] == null) {
                        continue;
                    }

                    //Index Data
                    if (typeof data_keyed[getElementNameString(e_data[index]["geobranchid"])] === "undefined") {
                        data_keyed[getElementNameString(e_data[index]["geobranchid"])] = {};
                    }
                    data_keyed[getElementNameString(e_data[index]["geobranchid"])][composed_label_key] = e_data[index]["value"];
                }
            }
            else {
                setMessage(Drupal.t("Empty data at node") + " @node.", {
                    '@node': settings.nodes[parseInt(e_index)]
                });
            }
        }
        else {
            setMessage(Drupal.t("Not data found at node") + " @node.", {
                '@node': settings.nodes[parseInt(e_index)]
            });
        }
        //console.log(data_keyed);
        settings.data_keyed[e_index] = data_keyed;
    });

    //Calc number of steps on variable
    let num_steps = [];
    settings.data_ext.forEach(function (e_data_ext, e_index) {
        if (typeof e_data_ext !== 'undefined') {
            num_steps[e_index] = e_data_ext['max_timestep'] - e_data_ext['min_timestep'] + 1;
        }
        else {
            setMessage(Drupal.t("Not extended data found") + " @node.", {
                '@node': settings.nodes[parseInt(e_index)]
            });
        }
    });

    //Read vocabulary object
    let vocabulary_obj = {};
    try {
        vocabulary_obj = JSON.parse(settings.vocabulary);
    }
    catch (e) {
        vocabulary_obj = {};
        setMessage(Drupal.t('Using default vocabulary.'));
    }

    //Set defaults legend colors and range for vocabulary's graph presentation
    let color_array_default = ['#00f', '#0f0', '#ff0', '#f00', '#00d55f', '#00cdda', '#0049e0', '#4200e6'];
    let legend_range_default = [0, 25, 50, 75];
    let legend_label_default = ['Bueno', 'Regular', 'Escaso', 'Malo'];
    let legend_scale = ['Y', 'Z', 'E', 'P', 'T', 'G', 'M', 'k', 'h', 'da', '', 'd', 'c', 'm', 'u', 'n', 'p', 'f', 'a', 'z', 'y'];
    let legend_scale_legend = ['Yotta', 'Zetta', 'Exa', 'Peta', 'Tera', 'Giga', 'Mega', 'kilo', 'hecto', 'deca', '(ninguno)', 'deci', 'centi', 'mili', 'micro', 'nano', 'pico', 'femto', 'atto', 'zepto', 'yocto'];
    let legend_scale_values = [10 ** 24, 10 ** 21, 10 * 18, 10 ** 15, 10 ** 12, 10 ** 9, 10 ** 6, 10 ** 3, 10 ** 2, 10, 1, 10 ** -1, 10 ** -2, 10 ** -3, 10 ** -6, 10 ** -9, 10 ** -12, 10 ** -15, 10 ** -18, 10 ** -21, 10 ** -24];
    let out_of_range_color = ['#848484'];
    let legend_config_default = {
        view: "range",    //Possible values: "fixed", "range"
        order: "asc",      //Possible values: "asc", "desc"
        scale: 10          //Possible values: 0 -> 'M',1 -> 'k',2 -> 'h',3 ->
                           // 'da',4 -> 'x1',5 -> 'd',6 -> 'c',7 => 'm',8 ->
                           // 'u'.
    };

    //Set legend colors and range for normal graph presentation
    if (typeof vocabulary_obj[settings.variable_machine_name] !== 'undefined') {
        settings.legend_colors = vocabulary_obj[settings.variable_machine_name].legend_colors;
        settings.legend_range = vocabulary_obj[settings.variable_machine_name].legend_range;
        settings.legend_label = (typeof vocabulary_obj[settings.variable_machine_name].legend_label !== 'undefined') ? vocabulary_obj[settings.variable_machine_name].legend_label : legend_label_default;
        settings.legend_config = (typeof vocabulary_obj[settings.variable_machine_name].legend_config !== 'undefined') ? vocabulary_obj[settings.variable_machine_name].legend_config : legend_config_default;
    }
    else {
        settings.legend_colors = color_array_default;
        settings.legend_label = legend_label_default;
        settings.legend_config = legend_config_default;
        settings.legend_range = legend_range_default;
        setMessage(Drupal.t('Using default vocabulary.'));
    }

    //Set legend length in case color legend and range legend were different
    let legend_length = Math.min(settings.legend_colors.length, settings.legend_range.length);
    settings.legend_length = legend_length;

    //Get data array index data for each resource
    let deficit_indicator_index = settings.machine_name_array.indexOf('dss_indicadores_eloha_deficit');
    let excess_indicator_index = settings.machine_name_array.indexOf('dss_indicadores_eloha_exceso');
    let deficit_index_index = settings.machine_name_array.indexOf('dss_indice_eloha_deficit');
    let excess_index_index = settings.machine_name_array.indexOf('dss_indice_eloha_exceso');
    let verdict_index = settings.machine_name_array.indexOf('dss_veredicto_eloha');
    let impact_index = settings.machine_name_array.indexOf('dss_impactos_eloha_verbales');
    if ((deficit_indicator_index === -1) || (excess_indicator_index === -1) || (deficit_index_index === -1) || (excess_index_index === -1) || (verdict_index === -1) || (impact_index === -1)) {
        setMessage(Drupal.t("Not all resources found on data."));
    }

    //Create leaflet layers
    let color_tile = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
        subdomains: ['a', 'b', 'c']
    });

    let gray_tile = L.tileLayer.grayscale('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
        subdomains: ['a', 'b', 'c']
    });

    let empty_tile = L.tileLayer('');

    //Create the leaflet map
    let mymap = L.map('sima', {
        layers: [color_tile],
        fullscreenControl: true,
        fullscreenControlOptions: {
            position: 'topleft'
        }
    }).setView([6.5, -75], 6);

    //Layer Control
    let baseMaps = {
        "Color": color_tile,
        "Grey": gray_tile,
        "Blank": empty_tile
    };
    L.control.layers(baseMaps, null, {position: 'bottomleft'}).addTo(mymap);

    //Adds the GeoJson Layer, using only the first geomap in array
    let geojson = L.geoJSON(settings.geomap[0], {
        style: style,
        onEachFeature: onEachFeature,
        pointToLayer: pointToLayer
    }).addTo(mymap);

    //Var used to store the selected geo elements
    let selected_elements = [];

    //Control box
    let info = L.control();

    info.onAdd = function (map) {
        this._div = L.DomUtil.create('div', 'info'); // create a div with a
                                                     // class "info"
        this.update();
        return this._div;
    };

    //Method that we will use to update the control based on feature properties
    // passed
    info.update = function (props) {
        //Get Value
        let element_value = 0;
        try {
            element_value = settings.data_keyed[parseInt(verdict_index)][getElementNameString(props.GeoBranchID)]['0']; //0-Means verdict
        }
        catch (err) {
            element_value = undefined;
        }

        //Check if data is defined
        if (typeof element_value === "undefined") {
            element_value = Drupal.t("Not available.");
        }

        //Update control info
        let scale_multiplication_factor = legend_scale_values[settings.legend_config.scale];
        this._div.innerHTML = (
            props ?
                Drupal.t('Element') + ': <b>' + (props.nom_visual ? props.nom_visual.toUpperCase() : getElementNameString(props.Branch.toUpperCase())) +
                '</b><br/>' + Drupal.t('Verdict') + ': <b>' + (parseFloat(element_value) / scale_multiplication_factor).toLocaleString() + '</b>'
                : '<h4>' + Drupal.t('Choose an element') + '</h4>');
    };
    info.addTo(mymap);

    //Creates the legend on the map
    let legend = L.control({position: 'bottomright'});
    legend.onAdd = function (map) {

        let div = L.DomUtil.create('div', 'info legend'),
            grades = settings.legend_range;

        //Setup for-loop
        let init_value = undefined;
        let func_inc_dec = undefined;
        let func_comp = undefined;
        if (settings.legend_config.order === 'asc') {
            init_value = 0;
            func_inc_dec = (i) => {
                return i + 1
            };
            func_comp = (i) => {
                return i < settings.legend_length
            };
        }
        else {
            init_value = settings.legend_length - 1;
            func_inc_dec = (i) => {
                return i - 1
            };
            func_comp = (i) => {
                return i >= 0
            };
        }

        //Loop through our density intervals and generate a label with a
        // colored square for each interval let unit_text =
        // legend_scale[settings.legend_config.scale];
        let labels = settings.legend_label;
        let table_string = '<table class="tableLegend">';
        for (let i = init_value; func_comp(i); i = func_inc_dec(i)) {
            //Get label to be displayed
            let label_i = typeof labels[i] !== 'undefined' ? labels[i] : '';

            //Display vocabulary
            if (typeof settings.legend_config.view !== "undefined" && settings.legend_config.view === "range") {
                table_string += '<tr><td>' +
                    '<i style="background:' + settings.legend_colors[i] + '"></i> ' +
                    grades[i] + (grades[i + 1] ? ' &ndash; ' + grades[i + 1] : ' +') + '</td><td>' + label_i + '</td></tr>';
            }
            else {
                table_string += '<tr><td>' +
                    '<i style="background:' + settings.legend_colors[i] + '"></i> ' +
                    grades[i] + '</td><td>' + label_i + '</td></tr>';
            }
        }
        table_string += '</table>';
        div.innerHTML += table_string;

        //Show scale
        if (legend_scale_legend[settings.legend_config.scale] !== '(ninguno)') {
            div.innerHTML += "&nbsp;" + Drupal.t("Escala") + ": <b>" + legend_scale_legend[settings.legend_config.scale] + "</b>";
        }

        return div;
    };
    legend.addTo(mymap);

    //Creates the choose legend on the map
    let chooseLegend = L.control({position: 'bottomright'});
    chooseLegend.onAdd = function (map) {

        let div = L.DomUtil.create('div', 'info legend'),
            grades = settings.legend_range,
            labels = [];

        // loop through our density intervals and generate a label with a
        // colored square for each interval
        for (let i = 0; i < settings.legend_length; i++) {
            //Get label to be displayed
            let label_l = typeof settings.legend_label[i] !== 'undefined' ? settings.legend_label[i] : '';

            //Set value for the i grade
            let grades_value = grades[i];
            if (typeof grades[i] === 'undefined') {
                grades_value = grades[grades.length - 1];
            }

            //Create the DIV legend content
            div.innerHTML +=
                '<input type="text" id="c' + i + '" class="colorPicker" value="' + settings.legend_colors[i] + '" /> &nbsp;' +
                '<input type="text" id="t' + i + '" class="textGUISet"  value="' + grades_value + '" />&nbsp;' +
                '<input type="text" id="l' + i + '" class="textLabelGUISet"  value="' + label_l + '" size="20"/></br>';
        }

        //Adds the size buttons
        div.innerHTML += Drupal.t("Size") + '&nbsp;&nbsp; <button type="button" id="addButton" class="legendButton">+</button><button type="button" id="delButton" class="legendButton">-</button></br>';

        //Adds the view radio buttons
        div.innerHTML += Drupal.t("Vista") + ': ' + '<label><input type="radio" id="view0" name="view_radio" value="range" class="" ' + (settings.legend_config.view === 'range' ? 'checked' : '') + '>&nbsp;' + Drupal.t("Rango") + '</label>' +
            '&nbsp;<label><input type="radio" id="view1" name="view_radio" value="fixed" class="" ' + (settings.legend_config.view === 'fixed' ? 'checked' : '') + '>&nbsp;' + Drupal.t("Fijo") + '</label></br>';

        //Adds the order radio buttons
        div.innerHTML += Drupal.t("Orden Visual") + ': ' + '<label><input type="radio" id="order0" name="order_radio" value="asc" class="" ' + (settings.legend_config.order === 'asc' ? 'checked' : '') + '>&nbsp;' + Drupal.t("Ascendente") + '</label>' +
            '&nbsp;<label><input type="radio" id="order1" name="order_radio" value="desc" class="" ' + (settings.legend_config.order === 'desc' ? 'checked' : '') + '>&nbsp;' + Drupal.t("Descendente") + '</label></br>';

        //Interchange labels
        div.innerHTML += Drupal.t("Orden") + '&nbsp;&nbsp; <button type="button" id="invertButton" class="invertLegend">' + Drupal.t("Cambiar") + '</button></br>';

        //Adds the scale select box
        let select_string = Drupal.t("Escala") + ': ' + '<select id="scale_select_id" name="scale_select">';
        for (let i = 0; i < legend_scale_legend.length; i++) {
            select_string += '<option value="' + i + '" label="' + legend_scale_legend[i] + '" ' + (settings.legend_config.scale === i ? 'selected' : '') + '></option>';
        }
        select_string += '</select>';
        div.innerHTML += select_string;

        return div;
    };
    //chooseLegend.addTo(mymap);

    //Set color picker element
    document.getElementById(settings.id[0] + "_cpicker").innerHTML = '<sub>' + Drupal.t('Change Legend') + '</sub>';

    //Update message
    Drupal.d3.page_notice();


    /** JQuery **/

        //Define jQuery vars
    let eloha_div = $('#' + settings.table_eloha_id);
    let cpicker_div = $('#' + settings.id[0] + '_cpicker');
    let cpicker_class = $(".colorPicker");

    //Handle Spectrum color picker
    let toggle_val = 0;
    cpicker_class.spectrum({
        change: function (color) {
            //console.log(color);
        }
    });

    //Handle Spectrum color picker selection
    if (cpicker_div.is(":visible")) {
        //New toggle way for jQuery 1.10
        cpicker_div.click(function(){
            if(toggle_val === 0){
                selectableLegend();
                toggle_val = 1;
            }else{
                fixedLegend();
                toggle_val = 0;
            }
        });
    }

    //Set Hide and show of table of Eloha's variables
    settings.machine_name_array.forEach(function (e_item, e_index) {
        let table_id = $("#" + settings.dina_table + e_index.toString());
        let table_label_id = $("#" + settings.dina_label + e_index.toString());

        table_id.hide();
        table_label_id.on("click", function (ev) {
            table_id.toggle("fold", 800);
        });
    });

    //Update display
    updateDisplayElohaGraph();

    /** Helper functions **/

    //Set color for the graph
    function getColor(d) {
        for (let i = (settings.legend_length - 1); i >= 0; i--) {
            if (d >= (settings.legend_range[i] * legend_scale_values[settings.legend_config.scale])) {
                return settings.legend_colors[i];
            }
        }

        //This in case there are values less than the minimum, Huh??
        return out_of_range_color[0];
    }

    function update_graphs_data() {
        //Clean all data arrays
        //Reset Eloha Graph Data
        Drupal.settings.d3.inventory[settings.table_eloha_id].def_indicator = [];
        Drupal.settings.d3.inventory[settings.table_eloha_id].sur_indicator = [];
        Drupal.settings.d3.inventory[settings.table_eloha_id].def_index = [];
        Drupal.settings.d3.inventory[settings.table_eloha_id].sur_index = [];
        Drupal.settings.d3.inventory[settings.table_eloha_id].overall = '';
        Drupal.settings.d3.inventory[settings.table_eloha_id].impact_strings = [];


        //Check if there are elements selected if not hide the all line_curve
        // graph and histogram graphs
        if (selected_elements.length === 0) {
            //Update display
            updateDisplayElohaGraph();

            return;
        }

        //Update Eloha Table Data
        updateElohaGraphData();

        //Update display
        updateDisplayElohaGraph();

        //Call line curve graph
        Drupal.d3.area_stacked(settings.table_eloha_id, Drupal.settings.d3.inventory[settings.table_eloha_id]);
    }

    function style(feature) {
        //Define Styles for no data
        var no_data_style = {
            fillColor: 'gray',
            weight: 0.8,
            opacity: 0.7,
            color: 'black',
            fillOpacity: 0.8
        };

        //Get actual value for element
        let element_value = get_element_value(feature.properties);
        if (typeof element_value === "undefined") {
            //return no_data_style;
            element_value = 0;
        }
        element_value = parseFloat(element_value);

        //Define Styles
        let polygon_style = {
            fillColor: getColor(element_value),
            weight: 0.8,
            opacity: 0.7,
            color: 'black',
            fillOpacity: 0.3
        };

        let line_style = {
            fillColor: getColor(element_value),
            weight: widthFlow(feature.properties),
            opacity: 0.7,
            color: getColor(element_value),
            fillOpacity: 0.4
        };

        let point_style = {
            fillColor: getColor(element_value),
            weight: 1.5,
            opacity: 0.7,
            color: 'black',
            fillOpacity: 0.4
        };

        //Get the style to be applied
        let returned_style = polygon_style;

        if (settings.geometry === 'Line') {
            returned_style = line_style;
        }

        if (settings.geometry === 'Point') {
            returned_style = point_style;
        }

        return returned_style;

        /* Sub Helper function */

        function widthFlow(properties) {
            //Calc Q_med_m3s for line weight
            //Default value
            let width_flow = 0.5;

            //Calc the width flow with and offset of 0.5, just to be always
            // visible
            if (properties.Q_med_m3s != null) {
                width_flow = Math.sqrt(parseFloat(feature.properties.Q_med_m3s)) / 14 + 0.5;
            }

            return width_flow;
        }
    }

    function onEachFeature(feature, layer) {
        layer.on({
            mouseover: highlightFeature,
            mouseout: resetHighlight,
            click: select_unselected,
        });
    }

    function pointToLayer(feature, latlng) {
        let point_style = {
            radius: 8
        };

        return L.circleMarker(latlng, point_style);
    }

    function highlightFeature(e) {
        let layer = e.target;

        let current_style = {
            weight: 3,
            color: '#050666',
            fillOpacity: 0.6
        };

        if (settings.geometry == 'Line') {
            current_style = {
                weight: 9,
                color: '#050666',
                fillOpacity: 0.6
            }
        }
        layer.setStyle(current_style);

        if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
            layer.bringToFront();
        }

        info.update(layer.feature.properties);
    }

    function resetHighlight(e) {
        let layer = e.target;
        let properties = layer.feature.properties;
        let idx_element = -1;
        try {
            idx_element = selected_elements.map(function (e) {
                return e.id
            }).indexOf(getElementNameString(properties.GeoBranchID));
        }
        catch (e) {
            idx_element = -1;
        }

        if (idx_element < 0) {
            geojson.resetStyle(layer);
        }
        info.update();
    }

    //Var used to store the selected geo elements
    function select_unselected(e) {
        let layer = e.target;
        let properties = layer.feature.properties;

        //Select a new element or unselected an already selected one
        let idx_element = -1;
        try {
            //Search for the clicked element, return -1 if not found
            idx_element = selected_elements.map(function (e) {
                return e.id
            }).indexOf(getElementNameString(properties.GeoBranchID));
        }
        catch (e) {
            idx_element = -1;
            return;
        }
        if (idx_element >= 0) {
            selected_elements.splice(idx_element, 1);
            geojson.resetStyle(layer);

            //Update Eloha graph data
            update_graphs_data();

            return;
        }

        //Add the new element
        let new_selected_element = {
            id: getElementNameString(properties.GeoBranchID),
            properties: properties,
            e: e
        };

        //Clear old element
        if (selected_elements.length > 0) {
            let layer_to_delete = selected_elements[0].e.target;
            geojson.resetStyle(layer_to_delete);
        }

        //Set new element
        selected_elements = [new_selected_element];

        let current_style = {
            fillColor: '#050666',
            fillOpacity: 0.6
        };
        layer.setStyle(current_style);

        if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
            layer.bringToFront();
        }

        //Update Eloha graph
        update_graphs_data();
    }

    function updateElohaGraphData() {
        //Get actual element
        if (selected_elements.length === 0) {
            return;
        }
        let element_obj = selected_elements[0];

        //Deficit indicator
        let deficit_indicator_array = [];
        let deficit_indicator_obj = settings.data_keyed[deficit_indicator_index][element_obj.id];
        for (let key in deficit_indicator_obj) {
            //Make sure the keys are not a properties from the prototype.
            if (deficit_indicator_obj.hasOwnProperty(key)) {
                let data_by_month_obj = JSON.parse(deficit_indicator_obj[key]);
                let data_by_month_key_array = Object.keys(data_by_month_obj);
                data_by_month_key_array.forEach(function (e_key, e_index) {
                    if (typeof deficit_indicator_array[e_key] === 'undefined') {
                        deficit_indicator_array[e_key] = ['def' + e_index.toString()];
                    }
                    deficit_indicator_array[e_key].push(parseFloat(data_by_month_obj[e_key]).toFixed(2).toString());
                });
            }
        }

        //Excess indicator
        let excess_indicator_array = [];
        let excess_indicator_obj = settings.data_keyed[excess_indicator_index][element_obj.id];
        for (let key in excess_indicator_obj) {
            //Make sure is not a property from the prototype
            if (excess_indicator_obj.hasOwnProperty(key)) {
                let data_by_month_obj = JSON.parse(excess_indicator_obj[key]);
                let data_by_month_key_array = Object.keys(data_by_month_obj);
                data_by_month_key_array.forEach(function (e_key, e_index) {
                    if (typeof excess_indicator_array[e_key] === 'undefined') {
                        excess_indicator_array[e_key] = ['sur' + e_index.toString()];
                    }
                    excess_indicator_array[e_key].push(parseFloat(data_by_month_obj[e_key]).toFixed(2).toString());
                });
            }
        }

        //Deficit index
        let deficit_index_array = [];
        let deficit_index_obj = settings.data_keyed[deficit_index_index][element_obj.id];
        for (let key in deficit_index_obj) {
            //Make sure is not a property from the prototype
            if (deficit_index_obj.hasOwnProperty(key)) {
                let data_by_month_obj = JSON.parse(deficit_index_obj[key]);
                let data_by_month_key_array = Object.keys(data_by_month_obj);
                data_by_month_key_array.forEach(function (e_key, e_index) {
                    if (typeof deficit_index_array[e_key] === 'undefined') {
                        deficit_index_array[e_key] = ['def_i' + e_index.toString()];
                    }
                    deficit_index_array[e_key].push(parseInt(data_by_month_obj[e_key]).toString());
                });
            }
        }

        //Excess index
        let excess_index_array = [];
        let excess_index_obj = settings.data_keyed[excess_index_index][element_obj.id];
        for (let key in excess_index_obj) {
            //Make sure is not a property from the prototype
            if (excess_index_obj.hasOwnProperty(key)) {
                let data_by_month_obj = JSON.parse(excess_index_obj[key]);
                let data_by_month_key_array = Object.keys(data_by_month_obj);
                data_by_month_key_array.forEach(function (e_key, e_index) {
                    if (typeof excess_index_array[e_key] === 'undefined') {
                        excess_index_array[e_key] = ['sur_i' + e_index.toString()];
                    }
                    excess_index_array[e_key].push(parseInt(data_by_month_obj[e_key]).toString());
                });
            }
        }

        //Verbal impact
        let impact_verbal_array = [];
        let impact_verbal_obj = settings.data_keyed[impact_index][element_obj.id];
        for (let key in impact_verbal_obj) {
            //Make sure is not a property from the prototype
            if (impact_verbal_obj.hasOwnProperty(key)) {
                //Get data by a whole month
                let data_by_month_obj = JSON.parse(impact_verbal_obj[key]);
                let data_by_month_key_array = Object.keys(data_by_month_obj);
                data_by_month_key_array.forEach(function (e_key, e_index) {
                    //Get the flow impact data by index
                    //let data_by_flow_obj =
                    // JSON.parse(data_by_month_obj[e_key]);
                    let data_by_flow_obj = data_by_month_obj[e_key];

                    //Sanity check
                    if (typeof impact_verbal_array[e_key] === 'undefined') {
                        impact_verbal_array[e_key] = ['verbal_v' + e_index.toString()];
                    }
                    if (typeof impact_verbal_array[e_key][key] === 'undefined') {
                        impact_verbal_array[e_key][key] = '';
                    }

                    //
                    for (let v_key in data_by_flow_obj) {
                        //Make sure is not a property from the prototype
                        if (data_by_flow_obj.hasOwnProperty(v_key)) {
                            if (impact_verbal_array[e_key][key] === '') {
                                impact_verbal_array[e_key][key] += '*' + data_by_flow_obj[v_key];
                            }
                            else {
                                impact_verbal_array[e_key][key] += '<br/>' + '*' + data_by_flow_obj[v_key];
                            }
                        }
                    }
                });
            }
        }

        //Verdict
        let verdict_value = Drupal.t('Not available.');
        if (typeof settings.data_keyed[verdict_index][element_obj.id] !== 'undefined') {
            verdict_value = parseFloat(settings.data_keyed[verdict_index][element_obj.id]['0']).toFixed(2).toString();
        }

        //Set data for Eloha Graphs
        Drupal.settings.d3.inventory[settings.table_eloha_id].def_indicator = deficit_indicator_array;
        Drupal.settings.d3.inventory[settings.table_eloha_id].sur_indicator = excess_indicator_array;
        Drupal.settings.d3.inventory[settings.table_eloha_id].def_index = deficit_index_array;
        Drupal.settings.d3.inventory[settings.table_eloha_id].sur_index = excess_index_array;
        Drupal.settings.d3.inventory[settings.table_eloha_id].impact_strings = impact_verbal_array;
        Drupal.settings.d3.inventory[settings.table_eloha_id].overall = verdict_value;
    }

    //Split branch names by "\"
    function getElementNameString(v_name) {
        if (v_name == null) {
            return '';
        }
        let splited_array = v_name.split("\\");
        return splited_array[splited_array.length - 1];
    }

    function updateDisplayElohaGraph() {
        //Show or Hide Eloha Graph
        eloha_div.empty();
        if (selected_elements.length > 0) {
            eloha_div.show();
        }
        else {
            eloha_div.hide();
        }
    }

    function fixedLegend() {
        //Update colors
        updateColorPickerJquery();

        //Set legend
        mymap.removeControl(chooseLegend);
        legend.addTo(mymap);

        //Set color picker element
        document.getElementById(settings.id[0] + '_cpicker').innerHTML = '<sub>' + Drupal.t("Change Legend") + '</sub>';

        //Update color visualization profile
        let string_vocabulary = JSON.stringify(vocabulary_obj);
        $.post('/visualization/set_profile', {visualization_profile: string_vocabulary});

        //Update map style
        geojson.setStyle(style);
    }

    function selectableLegend() {
        //Set legend
        mymap.removeControl(legend);
        chooseLegend.addTo(mymap);

        updateLegendControls();

        //Set color picker element
        document.getElementById(settings.id[0] + '_cpicker').innerHTML = '<sub>' + Drupal.t("Update Legend") + '</sub>';

        /* Sub Helper functions */

        function updateLegendControls() {
            //Activate Spectrum JQuery
            $(".colorPicker").spectrum({
                showInitial: true,
                showPalette: true,
                palette: [
                    ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)", "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)"],
                    ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)", "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
                    ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)", "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)"],
                    ["rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)", "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)"],
                    ["rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)", "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)"],
                    ["rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)", "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)"],
                    ["rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)", "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
                ],
            });

            //Activate Legend Size buttons
            $("#addButton").click(incrementLegend);
            $("#delButton").click(decrementLegend);

            //Activate Legend Size buttons
            $('#invertButton').click(invertLabelLegend);
        }

        function incrementLegend() {
            //Update legend length
            settings.legend_length += 1;

            //Update legend
            mymap.removeControl(chooseLegend);
            chooseLegend.addTo(mymap);

            updateLegendControls();
        }

        function decrementLegend() {
            if (settings.legend_length <= 1) {
                return;
            }

            //Update legend length
            settings.legend_length -= 1;

            //Update legend
            mymap.removeControl(chooseLegend);
            chooseLegend.addTo(mymap);

            updateLegendControls();
        }

        function invertLabelLegend() {
            //Update legend color and label directly from DOM
            for (let i = 0; i < Math.floor(settings.legend_length / 2); i++) {
                //Get elements from DOM
                let actualColorElement = $("#c" + i.toString());
                let invertColorElement = $("#c" + (settings.legend_length - 1 - i).toString());
                let actualLabelElement = $("#l" + i.toString());
                let invertLabelElement = $("#l" + (settings.legend_length - 1 - i).toString());

                //Get actual values
                let actualColor = '#' + actualColorElement.spectrum("get").toHex();
                let invertColor = '#' + invertColorElement.spectrum("get").toHex();
                let actualLabel = actualLabelElement.val();
                let invertLabel = invertLabelElement.val();

                //Set new inverted values
                actualColorElement.spectrum("set", invertColor);
                invertColorElement.spectrum("set", actualColor);
                actualLabelElement.val(invertLabel);
                invertLabelElement.val(actualLabel);
            }
        }
    }

    function updateColorPickerJquery() {
        //Update legend color array
        for (var i = 0; i < settings.legend_length; i++) {
            settings.legend_colors[i] = '#' + $("#c" + i.toString()).spectrum("get").toHex();

            //Set value from legend input text
            let text_value = $("#t" + i.toString()).val();
            let numValue = text_value.indexOf(".") === -1 ? parseInt(text_value) : parseFloat(text_value);
            settings.legend_range[i] = isNaN(numValue) ? settings.legend_range[i] : numValue;

            //Set label from legend input text
            settings.legend_label[i] = $("#l" + i.toString()).val();
        }

        //Update real size of array
        settings.legend_colors.length = settings.legend_length;
        settings.legend_range.length = settings.legend_length;
        settings.legend_label.length = settings.legend_length;

        //Update vocabulary config
        settings.legend_config = {};
        settings.legend_config.view = $('input[name=view_radio]:checked').val();
        settings.legend_config.order = $('input[name=order_radio]:checked').val();
        settings.legend_config.scale = parseInt($("#scale_select_id").find(':selected').val());

        //Update color profile object
        if (!vocabulary_obj.hasOwnProperty(settings.variable_machine_name)) {
            vocabulary_obj[settings.variable_machine_name] = {};
        }
        vocabulary_obj[settings.variable_machine_name].legend_colors = settings.legend_colors;
        vocabulary_obj[settings.variable_machine_name].legend_range = settings.legend_range;
        vocabulary_obj[settings.variable_machine_name].legend_label = settings.legend_label;
        vocabulary_obj[settings.variable_machine_name].legend_config = settings.legend_config;
    }

    function get_element_value(properties) {
        let element_value = undefined;
        try {
            element_value = settings.data_keyed[parseInt(verdict_index)][getElementNameString(properties.GeoBranchID)]['0'];
        }
        catch (err) {
            element_value = undefined;
        }

        return element_value;
    }
}

function setMessage(v_str_msg) {
    //Create missing properties
    if (typeof Drupal.settings.d3.inventory.notice0 == "undefined") {
        Drupal.settings.d3.inventory.notice0 = {};
        Drupal.settings.d3.inventory.notice0.rows = [];
    }
    else if (typeof Drupal.settings.d3.inventory.notice0.rows == "undefined") {
        Drupal.settings.d3.inventory.notice0.rows = [];
    }

    //Add the msg string to message
    Drupal.settings.d3.inventory.notice0.rows.push(v_str_msg);

    // let innerDivHtml = document.getElementById("msg").innerHTML;
    // if(innerDivHtml != ''){
    //     innerDivHtml = innerDivHtml + '<sub>, ' +v_str_msg +'</sub>';
    // }else{
    //     innerDivHtml = '<sub>' + Drupal.t('Note') + ': '+ v_str_msg
    // +'</sub>'; } document.getElementById("msg").innerHTML = innerDivHtml;
}

function clearMessage() {
    document.getElementById("msg").innerHTML = '';
}
