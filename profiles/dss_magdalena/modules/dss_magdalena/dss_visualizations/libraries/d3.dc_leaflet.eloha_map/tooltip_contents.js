/**
 * Created by jclaros on 16/04/17.
 */

Drupal.d3.tooltip_contents = function (d, defaultTitleFormat, defaultValueFormat, color, that, settings) {
    //Set jQuery variable for compatibility
    var $ = jQuery;

    //var $$ = this;
    var $$ = that, config = $$.config,
        titleFormat = config.tooltip_format_title || defaultTitleFormat,
        nameFormat = config.tooltip_format_name || function (name) {
                return name;
            },
        valueFormat = config.tooltip_format_value || defaultValueFormat,
        text, i, title, value, name, bgcolor;
    for (i = 0; i < d.length; i++) {
        if (!(d[i] && (d[i].value || d[i].value === 0))) {
            continue;
        }

        if (!text) {
            title = titleFormat ? titleFormat(d[i].x) : d[i].x;
            text = "<table class='" + $$.CLASS.tooltip + "'>" + (title || title === 0 ? "<tr><th colspan='2'>" + title + "</th></tr>" : "");
        }

        name = nameFormat(d[i].name);
        value = valueFormat(d[i].value, d[i].ratio, d[i].id, d[i].index);
        bgcolor = $$.levelColor ? $$.levelColor(d[i].value) : color(d[i].id);

        text += "<tr class='" + $$.CLASS.tooltipName + "-" + d[i].id + "'>";
        text += "<td class='name'><span style='background-color:" + bgcolor + "'></span>" + name + "</td>";
        text += "<td class='value'>" + value + "</td>";
        text += "</tr>";

        // Multidimensional tooltip
        // if(parseInt(Drupal.settings.d3.inventory.sima.weap_level) > 2) {
        //     if(parseInt($('#edit-aggregation-select option:selected').val()) == 0 ) {
        //         //Process multi-dimensions string
        //         var v_level_string = '';
        //         for (var y = 2; y < parseInt(Drupal.settings.d3.inventory.sima.weap_level); y++) {
        //             v_level_string += '.' + Drupal.settings.d3.inventory.sima.weap_level[y - 2][parseInt($('#edit-aggregation-select-level' + (y - 2).toString()).val())];
        //         }
        //
        //         //Add a separator
        //         text += "<tr>";
        //         text += "<th colspan='2'></th>"
        //         text += "</tr>";
        //
        //         //Create the extra dimension tooltip rows
        //         Drupal.settings.d3.inventory.sima.weap_level_data[parseInt(Drupal.settings.d3.inventory.sima.weap_level) - 3].forEach(function (e_dim, e_index) {
        //             var dim_value = Drupal.settings.d3.inventory.sima.data_keyed[settings.num_id][d[i].id][settings.x_key_array[d[i].index] + '.' + e_dim];
        //             if (typeof dim_value !== "undefined") {
        //                 text += "<tr>";
        //                 text += "<td class='name'>" + e_dim + "</td>";
        //                 text += "<td class='value'>" + dim_value + "</td>";
        //                 text += "</tr>";
        //             }
        //         });
        //     }
        // }
    }
    return text + "</table>";
}