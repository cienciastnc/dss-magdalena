/**
 * @file
 * Javascript for [library name].
 */

(function ($) {

    /**
     * Adds library to the global d3 object.
     *
     * @param select
     * @param settings
     *   Array of values passed to d3_draw.
     *   id: required. This will be needed to attach your
     *       visualization to the DOM.
     */
    Drupal.d3.stacked_balance = function (select, settings) {
        //Pre-process data on chart1
        var chart1_key_array = [];
        var chart1_val_array = [];
        settings.input_rows.forEach(function(elem,index){
            chart1_key_array.push(elem[0]);
            chart1_val_array.push(elem[1]);
        });
        var chart1_val_sum = chart1_val_array.reduce(function(a, b) {
            return parseFloat(a) + parseFloat(b);
            }, 0);

        //Pre-process data on chart2
        var chart2_key_array = [];
        var chart2_val_array = [];
        settings.output_rows.forEach(function(elem,index){
            chart2_key_array.push(elem[0]);
            chart2_val_array.push(elem[1]);
        });
        var chart2_val_sum = chart2_val_array.reduce(function(a, b) {
            return parseFloat(a) + parseFloat(b);
            }, 0);

        //Sets max y axis value
        var max_y_axis = parseInt(Math.max(chart1_val_sum, chart2_val_sum)*11/10);

        //Creates main div's to main div container
        d3.select('#' + settings.id)
            .append('div')
            .attr('id', 'wb_chart');

        d3.select('#' + settings.id)
            .append('div')
            .attr('id', 'wb_legend');

        //Creates chart's div
        d3.select('#wb_chart')
            .append('div')
            .attr('id', 'wb_chart_1');

        d3.select('#wb_chart')
            .append('div')
            .attr('id', 'wb_chart_2');

        //Creates chart legend's div
        d3.select('#wb_legend')
            .append('div')
            .attr('id', 'wb_legend_1');

        d3.select('#wb_legend')
            .append('div')
            .attr('id', 'wb_legend_2');

        //INPUT Graph
        var chart = c3.generate({
            bindto: '#wb_chart_1',
            data: {
                columns: settings.input_rows,
                type: 'bar',
                groups: [chart1_key_array],
                order: null
            },
            axis: {
                x: {
                    type: 'category',
                    categories: [Drupal.t('INPUT')]
                },
                y: {
                    label: {
                        text: 'Unidades',
                        position: 'outer-left'
                    },
                    max: max_y_axis,
                    min: 0,
                    padding: {top:0, bottom:0}
                }
            },
            grid: {
                x: {
                    show: true
                },
                y: {
                    show: true
                }
            },
            tooltip: {
                grouped: false // Default true
            },
            bar: {
                width: {
                    ratio: 0.5 // this makes bar width 50% of length between ticks
                }
            },
            legend: {
                show: false
            }
        });

        //OUTPUT Graph
        var chart2 = c3.generate({
            bindto: '#wb_chart_2',
            data: {
                columns: settings.output_rows,
                type: 'bar',
                groups: [chart2_key_array],
                order: null
            },
            axis: {
                x: {
                    type: 'category',
                    categories: [Drupal.t('OUTPUT')]
                },
                y: {
                    show: false,
                    max: max_y_axis,
                    min: 0,
                    padding: {top:0, bottom:0}
                }
            },
            grid: {
                x: {
                    show: true
                },
                y: {
                    show: true
                }
            },
            tooltip: {
                grouped: false // Default true
            },
            bar: {
                width: {
                    ratio: 0.5 // this makes bar width 50% of length between ticks
                }
            },
            legend: {
                show: false
            }
        });

        //Creates the legend on separate div for chart 1
        d3.select('#wb_legend_1').insert('div', '.chart').attr('class', 'legend_custom').selectAll('span')
            .data(chart1_key_array)
            .enter().append('span')
            .attr('data-id', function (id) { return id; })
            .html(function (id) { return '<span></span>'+id; })
            .each(function (id) {
                d3.select(this).select('span').style('background-color', chart.color(id));
            })
            .on('mouseover', function (id) {
                chart.focus(id);
            })
            .on('mouseout', function (id) {
                chart.revert();
            })
            .on('click', function (id) {
            });

        //Creates the legend on separate div for chart 2
        d3.select('#wb_legend_2').insert('div', '.chart').attr('class', 'legend_custom').selectAll('span')
            .data(chart2_key_array)
            .enter().append('span')
            .attr('data-id', function (id) { return id; })
            .html(function (id) { return '<span></span>'+id; })
            .each(function (id) {
                d3.select(this).select('span').style('background-color', chart.color(id));
            })
            .on('mouseover', function (id) {
                chart2.focus(id);
            })
            .on('mouseout', function (id) {
                chart2.revert();
            })
            .on('click', function (id) {
            });
    }
})(jQuery);
