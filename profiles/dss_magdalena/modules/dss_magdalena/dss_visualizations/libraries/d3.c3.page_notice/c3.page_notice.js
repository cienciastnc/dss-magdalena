/**
 * @file
 * Javascript for [library name].
 */

(function($) {

    /**
     * Adds library to the global d3 object.
     *
     * @param select
     * @param settings
     *   Array of values passed to d3_draw.
     *   id: required. This will be needed to attach your
     *       visualization to the DOM.
     */
    Drupal.d3.page_notice = function (select, settings) {
        if (Drupal.settings.d3.inventory.notice0.rows.length === 0){
            return
        }

        //console.log("AL fin");
        //Define jQuery vars
        let msg_div = $("#msg0");

        //
        let output = "<span>"+ Drupal.t("Note") +":</span><ul>";

        //settings.rows.forEach(function (e_msg, e_index) {
        Drupal.settings.d3.inventory.notice0.rows.forEach(function (e_msg, e_index) {
            output += "<li>" + e_msg + "</li>";
        });

        output +=  "</ul>";
        msg_div.html(output);
    }
})(jQuery);
