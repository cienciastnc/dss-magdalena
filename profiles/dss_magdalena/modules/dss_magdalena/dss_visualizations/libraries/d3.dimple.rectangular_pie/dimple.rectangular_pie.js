/**
 * @file
 * Javascript for [library name].
 */

(function($) {

  /**
   * Adds library to the global d3 object.
   *
   * @param select
   * @param settings
   *   Array of values passed to d3_draw.
   *   id: required. This will be needed to attach your
   *       visualization to the DOM.
   */
  Drupal.d3.rectangular_pie = function (select, settings) {
    // Your custom JS.
    d3.select('#' + settings.id).append('div')
                                .attr('id', 'split1_chart');

    d3.select('#' + settings.id).append('div')
                                .attr('id', 'split2_chart');
    var data = settings.rows;
    var order_chart = [];

    for (var i = 0; i < settings.rows.length; i++) {
      order_chart.push(settings.rows[i].indice);
    }

    var svg = dimple.newSvg('#split1_chart', 250, 500);
    var myChart = new dimple.chart(svg, data);
    myChart.setBounds(80, 40, "30%", "70%");
    axisX = myChart.addCategoryAxis("x", ["indice"]);
    axisX.fontSize = "14px"
    axisX.addOrderRule(order_chart);
    axisY = myChart.addPctAxis("y", "valor");
    axisY.fontSize = "14px"
    var mySeries = myChart.addSeries("propiedad", dimple.plot.bar);
    myLegend = myChart.addLegend(65, 10, 450, 20, "right");
    myLegend.fontSize = "14px;"
    myChart.draw();

    myChart.legends = [];
    var filterValues = dimple.getUniqueValues(data, "propiedad");
    myLegend.shapes.selectAll("rect")
      // Add a click event to each rectangle.
      .on("click", function (e) {
        // This indicates whether the item is already visible or not.
        var hide = false;
        var newFilters = [];
        // If the filters contain the clicked shape hide it.
        filterValues.forEach(function (f) {
          if (f === e.aggField.slice(-1)[0]) {
            hide = true;
          }
          else {
            newFilters.push(f);
          }
        });
        // Hide the shape or show it.
        if (hide) {
          d3.select(this).style("opacity", 0.2);
        }
        else {
          newFilters.push(e.aggField.slice(-1)[0]);
          d3.select(this).style("opacity", 0.8);
        }
        // Update the filters.
        filterValues = newFilters;
        // Filter the data.
        myChart.data = dimple.filterData(data, "propiedad", filterValues);
        // Passing a duration parameter makes the chart animate. Without
        // it there is no transition.
        myChart.draw();
      });

    setTimeout(function () {
      for (var i = 0; i < settings.rows.length; i++) {
        var property_name = settings.rows[i].propiedad;
        var value = settings.rows[i].valor;

        for (var j = 0; j < settings.vocabulary_colors.length; j++) {
          var min_val = settings.vocabulary_colors[j].min_val;
          var max_val = settings.vocabulary_colors[j].max_val;
          var leyenda = settings.vocabulary_colors[i].leyenda;
          var color = settings.vocabulary_colors[i].color;

          if (value > min_val && value <= max_val) {
            var class_name = 'dimple-' + property_name;

            d3.select('rect.' + class_name)
              .style('fill', color)
              .style('stroke', color);
          }
        }
      }
    }, 100);

    
    var table = d3.select("#split2_chart")
                  .append("table")
                  .attr("class", "legend-table");
    var thead = table.append("thead").append("tr");
    thead.append("td").text("Estado");
    thead.append("td").text("Rango de valor");
    thead.append("td").text("Color");

    var tbody = table.append("tbody");
    var array_length = settings.vocabulary_colors.length;

    for (var index = 0; index < array_length; index++) {
      //Generate EcoDef rows
      var leyenda = settings.vocabulary_colors[index].leyenda;
      var texto_rango = settings.vocabulary_colors[index].min_val.toString() + " - " + settings.vocabulary_colors[index].max_val.toString();
      var row = tbody.append("tr")
                      .attr("class", "row-" + index.toString());

      row.append("td").text(leyenda);
      row.append("td").text(texto_rango);
      row.append("td").style("background-color", settings.vocabulary_colors[index].color);
    }
  }

})(jQuery);
