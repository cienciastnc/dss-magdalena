/**
 * @file
 * Javascript for [library name].
 */

(function($) {

    /**
     * Adds library to the global d3 object.
     *
     * @param select
     * @param settings
     *   Array of values passed to d3_draw.
     *   id: required. This will be needed to attach your
     *       visualization to the DOM.
     */
    Drupal.d3.cause_effect_fixed = function (select, settings) {
        // Your custom JS.
        var current_model = settings.models[0].models[0];
        var current_description = settings.models[0].descriptions;

        var data_all = [
            {'connection_name': 'from_1_to_6', 'rel': 'soft'},
            {'connection_name': 'from_3_to_8', 'rel': 'soft'},
            {'connection_name': 'from_13_to_2', 'rel': 'soft'},
            {'connection_name': 'from_2_to_9', 'rel': 'soft'},
            {'connection_name': 'from_2_to_10', 'rel': 'hard'},
            {'connection_name': 'from_2_to_17', 'rel': 'soft'},
            {'connection_name': 'from_2_to_36', 'rel': 'soft'},
            {'connection_name': 'from_3_to_7', 'rel': 'hard'},
            {'connection_name': 'from_3_to_8', 'rel': 'hard'},
            {'connection_name': 'from_4_to_10', 'rel': 'hard'},
            {'connection_name': 'from_4_to_16', 'rel': 'hard'},
            {'connection_name': 'from_4_to_25', 'rel': 'soft'},
            {'connection_name': 'from_4_to_24', 'rel': 'hard'},
            {'connection_name': 'from_5_to_16', 'rel': 'hard'},
            {'connection_name': 'from_5_to_19', 'rel': 'hard'},
            {'connection_name': 'from_5_to_14', 'rel': 'hard'},
            {'connection_name': 'from_5_to_15', 'rel': 'hard'},
            {'connection_name': 'from_5_to_24', 'rel': 'soft'},
            {'connection_name': 'from_6_to_12', 'rel': 'soft'},
            {'connection_name': 'from_6_to_21', 'rel': 'soft'},
            {'connection_name': 'from_6_to_13', 'rel': 'soft'},
            {'connection_name': 'from_7_to_6', 'rel': 'soft'},
            {'connection_name': 'from_7_to_11', 'rel': 'soft'},
            {'connection_name': 'from_7_to_10', 'rel': 'hard'},
            {'connection_name': 'from_8_to_10', 'rel': 'hard'},
            {'connection_name': 'from_8_to_11', 'rel': 'soft'},
            {'connection_name': 'from_8_to_21', 'rel': 'hard'},
            {'connection_name': 'from_10_to_16', 'rel': 'hard'},
            {'connection_name': 'from_10_to_19', 'rel': 'hard'},
            {'connection_name': 'from_10_to_17', 'rel': 'soft'},
            {'connection_name': 'from_11_to_36', 'rel': 'soft'},
            {'connection_name': 'from_12_to_18', 'rel': 'soft'},
            {'connection_name': 'from_13_to_9', 'rel': 'soft'},
            {'connection_name': 'from_13_to_25', 'rel': 'soft'},
            {'connection_name': 'from_14_to_29', 'rel': 'soft'},
            {'connection_name': 'from_14_to_23', 'rel': 'soft'},
            {'connection_name': 'from_15_to_20', 'rel': 'hard'},
            {'connection_name': 'from_15_to_22', 'rel': 'soft'},
            {'connection_name': 'from_15_to_23', 'rel': 'soft'},
            {'connection_name': 'from_16_to_19', 'rel': 'hard'},
            {'connection_name': 'from_16_to_26', 'rel': 'hard'},
            {'connection_name': 'from_16_to_28', 'rel': 'hard'},
            {'connection_name': 'from_16_to_33', 'rel': 'soft'},
            {'connection_name': 'from_16_to_25', 'rel': 'soft'},
            {'connection_name': 'from_16_to_28', 'rel': 'hard'},
            {'connection_name': 'from_16_to_29', 'rel': 'hard'},
            {'connection_name': 'from_16_to_23', 'rel': 'soft'},
            {'connection_name': 'from_17_to_23', 'rel': 'soft'},
            {'connection_name': 'from_18_to_19', 'rel': 'hard'},
            {'connection_name': 'from_20_to_29', 'rel': 'hard'},
            {'connection_name': 'from_21_to_16', 'rel': 'hard'},
            {'connection_name': 'from_21_to_25', 'rel': 'soft'},
            {'connection_name': 'from_21_to_26', 'rel': 'hard'},
            {'connection_name': 'from_22_to_29', 'rel': 'soft'},
            {'connection_name': 'from_23_to_29', 'rel': 'soft'},
            {'connection_name': 'from_23_to_33', 'rel': 'soft'},
            {'connection_name': 'from_23_to_30', 'rel': 'soft'},
            {'connection_name': 'from_24_to_30', 'rel': 'soft'},
            {'connection_name': 'from_24_to_36', 'rel': 'hard'},
            {'connection_name': 'from_24_to_37', 'rel': 'hard'},
            {'connection_name': 'from_25_to_29', 'rel': 'soft'},
            {'connection_name': 'from_27_to_21', 'rel': 'hard'},
            {'connection_name': 'from_27_to_24', 'rel': 'hard'},
            {'connection_name': 'from_28_to_34', 'rel': 'soft'},
            {'connection_name': 'from_28_to_32', 'rel': 'soft'},
            {'connection_name': 'from_29_to_34', 'rel': 'soft'},
            {'connection_name': 'from_29_to_32', 'rel': 'soft'},
            {'connection_name': 'from_30_to_29', 'rel': 'soft'},
            {'connection_name': 'from_30_to_23', 'rel': 'soft'},
            {'connection_name': 'from_31_to_28', 'rel': 'hard'},
            {'connection_name': 'from_31_to_23', 'rel': 'soft'},
            {'connection_name': 'from_31_to_33', 'rel': 'soft'},
            {'connection_name': 'from_35_to_23', 'rel': 'soft'},
            {'connection_name': 'from_36_to_32', 'rel': 'soft'},
            {'connection_name': 'from_38_to_10', 'rel': 'hard'},
            {'connection_name': 'from_38_to_24', 'rel': 'hard'},
            {'connection_name': 'from_39_to_16', 'rel': 'hard'},
            {'connection_name': 'from_39_to_26', 'rel': 'hard'},
        ]

        var data_weap = [
            {'connection_name': 'from_2_to_10', 'rel': 'hard'},
            {'connection_name': 'from_3_to_7', 'rel': 'hard'},
            {'connection_name': 'from_3_to_8', 'rel': 'hard'},
            {'connection_name': 'from_4_to_10', 'rel': 'hard'},
            {'connection_name': 'from_4_to_16', 'rel': 'hard'},
            {'connection_name': 'from_5_to_16', 'rel': 'hard'},
            {'connection_name': 'from_5_to_19', 'rel': 'hard'},
            {'connection_name': 'from_7_to_10', 'rel': 'hard'},
            {'connection_name': 'from_7_to_21', 'rel': 'hard'},
            {'connection_name': 'from_8_to_10', 'rel': 'hard'},
            {'connection_name': 'from_8_to_21', 'rel': 'hard'},
            {'connection_name': 'from_10_to_16', 'rel': 'hard'},
            {'connection_name': 'from_10_to_19', 'rel': 'hard'},
            {'connection_name': 'from_16_to_19', 'rel': 'hard'},
            {'connection_name': 'from_16_to_26', 'rel': 'hard'},
            {'connection_name': 'from_16_to_28', 'rel': 'hard'},
            {'connection_name': 'from_16_to_29', 'rel': 'hard'},
            {'connection_name': 'from_18_to_19', 'rel': 'hard'},
            {'connection_name': 'from_21_to_16', 'rel': 'hard'},
            {'connection_name': 'from_21_to_26', 'rel': 'hard'},
            {'connection_name': 'from_27_to_21', 'rel': 'hard'},
            {'connection_name': 'from_31_to_28', 'rel': 'hard'},
            {'connection_name': 'from_38_to_10', 'rel': 'hard'},
            {'connection_name': 'from_39_to_16', 'rel': 'hard'},
            {'connection_name': 'from_39_to_26', 'rel': 'hard'},
        ]

        var data_eloha = [
            {'connection_name': 'from_16_to_29', 'rel': 'hard'},
        ]

        var data_huella = [
            {'connection_name': 'from_4_to_24', 'rel': 'hard'},
            {'connection_name': 'from_5_to_24', 'rel': 'hard'},
            {'connection_name': 'from_24_to_36', 'rel': 'hard'},
            {'connection_name': 'from_24_to_37', 'rel': 'hard'},
            {'connection_name': 'from_27_to_24', 'rel': 'hard'},
            {'connection_name': 'from_38_to_24', 'rel': 'hard'},
        ]

        var data_fragmentacion = [
            {'connection_name': 'from_5_to_15', 'rel': 'hard'},
            {'connection_name': 'from_15_to_20', 'rel': 'hard'},
            {'connection_name': 'from_20_to_29', 'rel': 'hard'},
        ]

        var data_dor_h = [
            {'connection_name': 'from_5_to_16', 'rel': 'hard'},
            {'connection_name': 'from_10_to_16', 'rel': 'hard'},
            {'connection_name': 'from_16_to_29', 'rel': 'hard'},
        ]

        var data_dor_w = [
            {'connection_name': 'from_5_to_16', 'rel': 'hard'},
            {'connection_name': 'from_10_to_16', 'rel': 'hard'},
            {'connection_name': 'from_16_to_29', 'rel': 'hard'},
        ]

        var data_sediments = [
            {'connection_name': 'from_5_to_15', 'rel': 'hard'},
            {'connection_name': 'from_15_to_20', 'rel': 'hard'},
            {'connection_name': 'from_20_to_29', 'rel': 'hard'},
        ]

        var nodes = [
            { 'name': '1', 'content': ['Hábitos:', 'dieta..' ], 'type': 'lightgray', 'x': 10, 'y': 20, 'rel': 'soft', 'present': true },
            { 'name': '2', 'content': ['USO DEL SUELO', '(deforestación)'], 'type': 'blue', 'x': 110, 'y': 30, 'rel': 'hard', 'present': true },
            { 'name': '3', 'content': ['CAMBIO', 'CLIMÁTICO'], 'type': 'orange', 'x': 400, 'y': 30, 'rel': 'soft', 'present': true },
            { 'name': '4', 'content': ['MINERIA', '- donde', '- demanda'], 'type': 'blue', 'x': 600, 'y': 30, 'rel': 'hard', 'present': true },
            { 'name': '5', 'content': ['REPRESAS & PLANTAS', '- localización', '- tamaño/diseño', '- manejo'], 'type': 'blue', 'x': 680, 'y': 30, 'rel': 'hard', 'present': true },
            { 'name': '6', 'content': ['Comportamiento', 'personas'], 'type': 'lightgray', 'x': 20, 'y': 90, 'rel': 'soft', 'present': true },
            { 'name': '7', 'content': ['T, V, U'], 'type': 'smallorange', 'x': 300, 'y': 90, 'rel': 'soft', 'present': true },
            { 'name': '8', 'content': ['PRECIPITACIONES'], 'type': 'smallorange', 'x': 370, 'y': 90, 'rel': 'soft', 'present': true },
            { 'name': '9', 'content': ['CO2'], 'type': 'lightgray', 'x': 210, 'y': 130, 'rel': 'soft', 'present': true },
            { 'name': '10', 'content': ['Hidrologia', 'sistema'], 'type': 'smallorange', 'x': 390, 'y': 150, 'rel': 'soft', 'present': true },
            { 'name': '11', 'content': ['Distribucion', 'areal', 'especies'], 'type': 'lightgray', 'x': 190, 'y': 190, 'rel': 'soft', 'present': true },
            { 'name': '12', 'content': ['Aire', 'acondicionado'], 'type': 'lightgray', 'x': 5, 'y': 200, 'rel': 'soft', 'present': true },
            { 'name': '13', 'content': ['Ganaderia'], 'type': 'lightgray', 'x': 110, 'y': 210, 'rel': 'soft', 'present': true },
            { 'name': '14', 'content': ['Hydro', 'peaking'], 'type': 'lightgray', 'x': 580, 'y': 230, 'rel': 'soft', 'present': true },
            { 'name': '15', 'content': ['Efecto barrera', ' y captura sed'], 'type': 'orange', 'x': 680, 'y': 230, 'rel': 'soft', 'present': true },
            { 'name': '16', 'content': ['Régimen hidrico', 'rios, humedales', '(acuiferos)'], 'type': 'orange', 'x': 280, 'y': 250, 'rel': 'soft', 'present': true },
            { 'name': '17', 'content': ['Aporte', 'solido'], 'type': 'lightgray', 'x': 480, 'y': 250, 'rel': 'soft', 'present': true },
            { 'name': '18', 'content': ['Demanda', 'eléctrica'], 'type': 'orange', 'x': 5, 'y': 310, 'rel': 'soft', 'present': true },
            { 'name': '19', 'content': ['Producción', 'hidro-electrica', '(seguridad)'], 'type': 'red', 'x': 400, 'y': 350, 'rel': 'soft', 'present': true },
            { 'name': '20', 'content': ['Continuidad', 'longitudinal'], 'type': 'orange', 'x': 510, 'y': 380, 'rel': 'soft', 'present': true },
            { 'name': '21', 'content': ['Demanda', 'agrícola'], 'type': 'orange', 'x': 25, 'y': 400, 'rel': 'soft', 'present': true },
            { 'name': '22', 'content': ['Migración', 'ictio-fauna'], 'type': 'lightgray', 'x': 550, 'y': 420, 'rel': 'soft', 'present': true },
            { 'name': '23', 'content': ['Transporte', 'sólido y ', "morfología"], 'type': 'lightgray', 'x': 690, 'y': 440, 'rel': 'soft', 'present': true },
            { 'name': '24', 'content': ['Huella', 'real'], 'type': 'orange', 'x': 850, 'y': 440, 'rel': 'soft', 'present': true },
            { 'name': '25', 'content': ['Calidad', 'del', 'agua'], 'type': 'lightgray', 'x': 320, 'y': 440, 'rel': 'soft', 'present': true },
            { 'name': '26', 'content': ['Abastecimiento', 'Hídrico', '(deficit)'], 'type': 'red', 'x': 130, 'y': 470, 'rel': 'soft', 'present': true },
            { 'name': '27', 'content': ['DESARROLLO', 'AGRICOLA', '(riego)', '- donde', '- cuando'], 'type': 'blue', 'x': 5, 'y': 490, 'rel': 'soft', 'present': true },
            { 'name': '28', 'content': ['Alteración', 'ecosistemas', '(humedales)'], 'type': 'red', 'x': 190, 'y': 510, 'rel': 'soft', 'present': true },
            { 'name': '29', 'content': ['Alteración', 'ecosistemas', '(rios)'], 'type': 'red', 'x': 360, 'y': 510, 'rel': 'soft', 'present': true },
            { 'name': '30', 'content': ['Vegetación', 'riparial', '(rios)'], 'type': 'lightgray', 'x': 620, 'y': 530, 'rel': 'soft', 'present': true },
            { 'name': '31', 'content': ['CONTROL', 'INUNDACIONES', '(diques,', 'conectividad)'], 'type': 'blue', 'x': 5, 'y': 590, 'rel': 'soft', 'present': true },
            { 'name': '32', 'content': ['Bio-', 'diversidad'], 'type': 'lightgray', 'x': 480, 'y': 590, 'rel': 'soft', 'present': true },
            { 'name': '33', 'content': ['Riesgo', 'inundaciones'], 'type': 'lightgray', 'x': 180, 'y': 610, 'rel': 'soft', 'present': true },
            { 'name': '34', 'content': ['Pesca'], 'type': 'lightgray', 'x': 280, 'y': 620, 'rel': 'soft', 'present': true },
            { 'name': '35', 'content': ['NAVEGACION', '(dragado:', 'adecuacion cauce)'], 'type': 'lightgray', 'x': 340, 'y': 610, 'rel': 'soft', 'present': true },
            { 'name': '36', 'content': ['Conservación', 'zonas', '"portafolio"'], 'type': 'red', 'x': 590, 'y': 610, 'rel': 'soft', 'present': true },
            { 'name': '37', 'content': ['Impacto', 'resguardos', '"uso de suelo"'], 'type': 'red', 'x': 740, 'y': 610, 'rel': 'soft', 'present': true },
            { 'name': '38', 'content': ['EXPLOTACIÓN', 'FORESTAL'], 'type': 'blue', 'x': 800, 'y': 75, 'rel': 'hard', 'present': true },
            { 'name': '39', 'content': ['Crecimiento', 'Poblacional'], 'type': 'orange', 'x': 45, 'y': 260, 'rel': 'hard', 'present': true },
        ];


        var connections = [
            { 'connection_name': 'from_1_to_6', 'from': '1', 'to': '6', 'x1': 30, 'y1': 40, 'x2': 40, 'y2': 75 },
            { 'connection_name': 'from_13_to_2', 'from': '13', 'to': '2', 'x1': 140, 'y1': 195, 'x2': 140, 'y2': 50 },
            { 'connection_name': 'from_2_to_9', 'from': '2', 'to': '9', 'x1': 220, 'y1': 40, 'x2': 220, 'y2': 115 },
            { 'connection_name': 'from_2_to_10', 'from': '2', 'to': '10', 'x1': 150, 'y1': 50, 'x2': 380, 'y2': 150 },
            { 'connection_name': 'from_2_to_17', 'from': '2', 'to': '17', 'x1': 220, 'y1': 40, 'x2': 470, 'y2': 260 },
            { 'connection_name': 'from_2_to_36', 'from': '2', 'to': '36', 'x1': 220, 'y1': 40, 'x2': 620, 'y2': 600 },
            { 'connection_name': 'from_3_to_7', 'from': '3', 'to': '7', 'x1': 430, 'y1': 50, 'x2': 320, 'y2': 75 },
            { 'connection_name': 'from_3_to_8', 'from': '3', 'to': '8', 'x1': 430, 'y1': 50, 'x2': 420, 'y2': 75 },
            { 'connection_name': 'from_4_to_10', 'from': '4', 'to': '10', 'x1': 640, 'y1': 65, 'x2': 420, 'y2': 135 },
            { 'connection_name': 'from_4_to_16', 'from': '4', 'to': '16', 'x1': 640, 'y1': 65, 'x2': 400, 'y2': 255 },
            { 'connection_name': 'from_4_to_25', 'from': '4', 'to': '25', 'x1': 640, 'y1': 65, 'x2': 330, 'y2': 420 },
            { 'connection_name': 'from_4_to_24', 'from': '4', 'to': '24', 'x1': 640, 'y1': 65, 'x2': 855, 'y2': 425 },
            { 'connection_name': 'from_5_to_16', 'from': '5', 'to': '16', 'x1': 740, 'y1': 75, 'x2': 400, 'y2': 260 },
            { 'connection_name': 'from_5_to_19', 'from': '5', 'to': '19', 'x1': 740, 'y1': 75, 'x2': 440, 'y2': 335 },
            { 'connection_name': 'from_5_to_14', 'from': '5', 'to': '14', 'x1': 740, 'y1': 75, 'x2': 600, 'y2': 220 },
            { 'connection_name': 'from_5_to_15', 'from': '5', 'to': '15', 'x1': 740, 'y1': 75, 'x2': 750, 'y2': 215 },
            { 'connection_name': 'from_5_to_24', 'from': '5', 'to': '24', 'x1': 740, 'y1': 75, 'x2': 860, 'y2': 425 },
            { 'connection_name': 'from_6_to_12', 'from': '6', 'to': '12', 'x1': 50, 'y1': 110, 'x2': 25, 'y2': 185 },
            { 'connection_name': 'from_6_to_21', 'from': '6', 'to': '21', 'x1': 50, 'y1': 110, 'x2': 80, 'y2': 385 },
            { 'connection_name': 'from_6_to_13', 'from': '6', 'to': '13', 'x1': 50, 'y1': 110, 'x2': 140, 'y2': 195 },
            { 'connection_name': 'from_7_to_6', 'from': '7', 'to': '6', 'x1': 320, 'y1': 95, 'x2': 100, 'y2': 100 },
            { 'connection_name': 'from_7_to_11', 'from': '7', 'to': '11', 'x1': 320, 'y1': 95, 'x2': 220, 'y2': 180 },
            { 'connection_name': 'from_7_to_10', 'from': '7', 'to': '10', 'x1': 320, 'y1': 95, 'x2': 410, 'y2': 135 },
            { 'connection_name': 'from_7_to_21', 'from': '7', 'to': '21', 'x1': 320, 'y1': 95, 'x2': 85, 'y2': 385 },
            { 'connection_name': 'from_8_to_11', 'from': '8', 'to': '11', 'x1': 400, 'y1': 95, 'x2': 220, 'y2': 180 },
            { 'connection_name': 'from_8_to_21', 'from': '8', 'to': '21', 'x1': 400, 'y1': 95, 'x2': 90, 'y2': 385 },
            { 'connection_name': 'from_8_to_10', 'from': '8', 'to': '10', 'x1': 400, 'y1': 95, 'x2': 415, 'y2': 135 },
            { 'connection_name': 'from_10_to_16', 'from': '10', 'to': '16', 'x1': 420, 'y1': 165, 'x2': 340, 'y2': 235 },
            { 'connection_name': 'from_10_to_17', 'from': '10', 'to': '17', 'x1': 420, 'y1': 165, 'x2': 470, 'y2': 260 },
            { 'connection_name': 'from_11_to_36', 'from': '11', 'to': '36', 'x1': 220, 'y1': 225, 'x2': 620, 'y2': 600 },
            { 'connection_name': 'from_12_to_18', 'from': '12', 'to': '18', 'x1': 20, 'y1': 215, 'x2': 20, 'y2': 290 },
            { 'connection_name': 'from_13_to_9', 'from': '13', 'to': '9', 'x1': 140, 'y1': 195, 'x2': 215, 'y2': 135 },
            { 'connection_name': 'from_13_to_25', 'from': '13', 'to': '25', 'x1': 130, 'y1': 220, 'x2': 330, 'y2': 420 },
            { 'connection_name': 'from_14_to_29', 'from': '14', 'to': '29', 'x1': 600, 'y1': 250, 'x2': 390, 'y2': 500 },
            { 'connection_name': 'from_14_to_23', 'from': '14', 'to': '23', 'x1': 600, 'y1': 250, 'x2': 710, 'y2': 430 },
            { 'connection_name': 'from_15_to_20', 'from': '15', 'to': '20', 'x1': 720, 'y1': 250, 'x2': 585, 'y2': 365 },
            { 'connection_name': 'from_15_to_22', 'from': '15', 'to': '22', 'x1': 720, 'y1': 250, 'x2': 575, 'y2': 410 },
            { 'connection_name': 'from_15_to_23', 'from': '15', 'to': '23', 'x1': 720, 'y1': 250, 'x2': 710, 'y2': 430 },
            { 'connection_name': 'from_16_to_19', 'from': '16', 'to': '19', 'x1': 320, 'y1': 285, 'x2': 390, 'y2': 360 },
            { 'connection_name': 'from_16_to_26', 'from': '16', 'to': '26', 'x1': 320, 'y1': 285, 'x2': 175, 'y2': 455 },
            { 'connection_name': 'from_16_to_28', 'from': '16', 'to': '28', 'x1': 320, 'y1': 285, 'x2': 240, 'y2': 495 },
            { 'connection_name': 'from_16_to_33', 'from': '16', 'to': '33', 'x1': 320, 'y1': 285, 'x2': 230, 'y2': 600 },
            { 'connection_name': 'from_16_to_25', 'from': '16', 'to': '25', 'x1': 320, 'y1': 285, 'x2': 330, 'y2': 420 },
            { 'connection_name': 'from_16_to_29', 'from': '16', 'to': '29', 'x1': 320, 'y1': 285, 'x2': 390, 'y2': 495 },
            { 'connection_name': 'from_16_to_23', 'from': '16', 'to': '23', 'x1': 320, 'y1': 285, 'x2': 710, 'y2': 430 },
            { 'connection_name': 'from_17_to_23', 'from': '17', 'to': '23', 'x1': 530, 'y1': 260, 'x2': 710, 'y2': 430 },
            { 'connection_name': 'from_18_to_19', 'from': '18', 'to': '19', 'x1': 70, 'y1': 320, 'x2': 390, 'y2': 365 },
            { 'connection_name': 'from_20_to_29', 'from': '20', 'to': '29', 'x1': 540, 'y1': 400, 'x2': 410, 'y2': 495 },
            { 'connection_name': 'from_21_to_16', 'from': '21', 'to': '16', 'x1': 95, 'y1': 400, 'x2': 270, 'y2': 260 },
            { 'connection_name': 'from_21_to_25', 'from': '21', 'to': '25', 'x1': 95, 'y1': 400, 'x2': 330, 'y2': 420 },
            { 'connection_name': 'from_21_to_26', 'from': '21', 'to': '26', 'x1': 95, 'y1': 400, 'x2': 165, 'y2': 455 },
            { 'connection_name': 'from_22_to_29', 'from': '22', 'to': '29', 'x1': 570, 'y1': 440, 'x2': 445, 'y2': 520 },
            { 'connection_name': 'from_23_to_29', 'from': '23', 'to': '29', 'x1': 710, 'y1': 470, 'x2': 445, 'y2': 520 },
            { 'connection_name': 'from_23_to_33', 'from': '23', 'to': '33', 'x1': 710, 'y1': 470, 'x2': 230, 'y2': 600 },
            { 'connection_name': 'from_23_to_30', 'from': '23', 'to': '30', 'x1': 710, 'y1': 470, 'x2': 680, 'y2': 540 },
            { 'connection_name': 'from_24_to_30', 'from': '24', 'to': '30', 'x1': 860, 'y1': 460, 'x2': 680, 'y2': 540 },
            { 'connection_name': 'from_24_to_36', 'from': '24', 'to': '36', 'x1': 860, 'y1': 460, 'x2': 630, 'y2': 595 },
            { 'connection_name': 'from_24_to_37', 'from': '24', 'to': '37', 'x1': 860, 'y1': 460, 'x2': 760, 'y2': 595 },
            { 'connection_name': 'from_25_to_29', 'from': '25', 'to': '29', 'x1': 330, 'y1': 470, 'x2': 350, 'y2': 520 },
            { 'connection_name': 'from_27_to_21', 'from': '27', 'to': '21', 'x1': 45, 'y1': 475, 'x2': 60, 'y2': 420 },
            { 'connection_name': 'from_27_to_24', 'from': '27', 'to': '24', 'x1': 90, 'y1': 500, 'x2': 850, 'y2': 430 },
            { 'connection_name': 'from_28_to_34', 'from': '28', 'to': '34', 'x1': 220, 'y1': 550, 'x2': 300, 'y2': 610 },
            { 'connection_name': 'from_28_to_32', 'from': '28', 'to': '32', 'x1': 220, 'y1': 550, 'x2': 500, 'y2': 575 },
            { 'connection_name': 'from_29_to_34', 'from': '29', 'to': '34', 'x1': 390, 'y1': 530, 'x2': 300, 'y2': 610 },
            { 'connection_name': 'from_29_to_32', 'from': '29', 'to': '32', 'x1': 390, 'y1': 530, 'x2': 500, 'y2': 575 },
            { 'connection_name': 'from_30_to_29', 'from': '30', 'to': '29', 'x1': 610, 'y1': 540, 'x2': 445, 'y2': 520 },
            { 'connection_name': 'from_30_to_23', 'from': '30', 'to': '23', 'x1': 680, 'y1': 540, 'x2': 710, 'y2': 470 },
            { 'connection_name': 'from_31_to_28', 'from': '31', 'to': '28', 'x1': 110, 'y1': 610, 'x2': 185, 'y2': 530 },
            { 'connection_name': 'from_31_to_23', 'from': '31', 'to': '23', 'x1': 95, 'y1': 580, 'x2': 710, 'y2': 430 },
            { 'connection_name': 'from_31_to_33', 'from': '31', 'to': '33', 'x1': 95, 'y1': 580, 'x2': 230, 'y2': 600 },
            { 'connection_name': 'from_35_to_23', 'from': '35', 'to': '23', 'x1': 420, 'y1': 600, 'x2': 710, 'y2': 430 },
            { 'connection_name': 'from_36_to_32', 'from': '36', 'to': '32', 'x1': 580, 'y1': 620, 'x2': 500, 'y2': 610 },
            { 'connection_name': 'from_38_to_10', 'from': '38', 'to': '10', 'x1': 840, 'y1': 95, 'x2': 465, 'y2': 155 },
            { 'connection_name': 'from_38_to_24', 'from': '38', 'to': '24', 'x1': 840, 'y1': 95, 'x2': 865, 'y2': 425 },
            { 'connection_name': 'from_39_to_16', 'from': '39', 'to': '16', 'x1': 130, 'y1': 265, 'x2': 270, 'y2': 255 },
            { 'connection_name': 'from_39_to_26', 'from': '39', 'to': '26', 'x1': 130, 'y1': 265, 'x2': 170, 'y2': 455 },
        ];

        // ==================== Drawing funcions =============================.
        function drawSvg(select_index) {
                svg1 = d3.select('#' + settings.id)
              .append('div')
              .attr('id', 'description-graph')
              .attr('width', 900)
              .attr('height', 100);

            svg3 = d3.select('#' + settings.id)
            .append('div')
            .attr('id', 'hover-btn');


            var svg = d3.select('#' + settings.id)
                .append('svg')
                .attr('id', 'main-graph')
                .attr('width', 900)
                .attr('height', 650);
            // Define marker for arrows.
            svg.append("defs").selectAll("marker")
                .data(["soft", "hard"])
                .enter().append("marker")
                .attr("id", function (d) { return d;
                })
                .attr("viewBox", "0 -5 10 10")
                .attr("refX", 5)
                .attr("refY", 0)
                .attr("markerWidth", 4)
                .attr("markerHeight", 4)
                .attr("orient", "auto")
                .append("path")
                .attr("class","arrowHead")
                .attr("d", "M0,-5L10,0L0,5");



            $('selected_graph').attr('color','');

            $('#' + settings.id).append(
                "<select id='sel_graph'></select>"
            );

            var options = settings.models;
            var option_selected = '<option selected>';
            var option_unselected = '<option>';
            var html_option = "";

            options.forEach(function(item, index){
                if (index == select_index) {
                    html_option = option_selected;
                }
                else {
                    html_option = option_unselected;
                }

                $('#sel_graph').append($(html_option).text(item.title).val(item.models));
            });

            if (select_index == options.length) {
                html_option = option_selected;
            }
            else {
                html_option = option_unselected;
            }
            $('#sel_graph').append($(html_option).text("Cuadro de Referencia").val(current_model + "," + "todos"));

        }

        function drawNode(d, t, x, y) {
            d.forEach(function (element, index) {
                d3.select("svg").append("text")
                    .attr("class", t)
                    .attr("x", x)
                    .attr("y", y + (index * 14))
                    .text(element);
            });
        }

        function drawConnection(x1, x2, y1, y2, rel) {
            var svg = d3.select('svg');
            svg.append('line')
                .attr('x1', x1)
                .attr('x2', x2)
                .attr('y1', y1)
                .attr('y2', y2)
                .attr('y2', y2)
                .attr('class', rel)
                .attr('marker-end', 'url(#' + rel + ')');
        }

        function writeDescription(text) {
            d3.select('#description-graph').append("text")
              .attr("class", "smallorange")
              .attr("x", 10)
              .attr("y", 20)
              .html(text);
        }

        function drawGraph(model) {
            var nodes_present = [];
            var active_connections = [];
            var active_model = model;
            //$('#sima').prepend("<div id='imagen_modelo'><h1>imagen :"+model+"</div>");

            switch (active_model) {
                case 'weap':
                    active_connections = data_weap;
                    //imgpath="/profiles/dss_magdalena/modules/dss_magdalena/dss_interface/images/elementos_index/19-boton.jpg"
                    break;

                case 'eloha':
                    active_connections = data_eloha;
                    //imgpath="eloha.png";
                    break;

                case 'territorial_footprint':
                    active_connections = data_huella;
                    //imgpath="huella.png";
                    break;

                case 'fragmentation':
                    active_connections = data_fragmentacion;
                    //imgpath="fragmentacion.png";
                    break;

                case 'dor_h':
                    active_connections = data_dor_h;
                    //imgpath="dor_h.png";
                    break;

                case 'dor_w':
                    active_connections = data_dor_w;
                    //imgpath='dor_w.png';
                    break;

                case 'sediments':
                    active_connections = data_sediments;
                    //imgpath='sedimentos.png'
                    break;

                case 'todos':
                    active_connections = data_all;
                    break;

                default:
                    break;
            }
            
            //$("#imagen_modelo").append("<img src="+imgpath+"></img>");

            /*Borrar este for*/
            for (var i = 0; i < active_connections.length; i++) {
                for (var j = 0; j < connections.length; j++) {
                    if (active_connections[i].connection_name == connections[j].connection_name) {

                        if (jQuery.inArray(connections[j].from, nodes_present) == -1) {
                            for (var k = 0; k < nodes.length; k++) {
                                if (connections[j].from == nodes[k].name) {
                                    var content = nodes[k].content;
                                    var x = nodes[k].x;
                                    var y = nodes[k].y;
                                    var type = nodes[k].type;
                                    drawNode(nodes[k].content, nodes[k].type, x, y)
                                }
                            }
                        }

                        if (jQuery.inArray(connections[j].to, nodes_present) == -1) {
                            for (var k = 0; k < nodes.length; k++) {
                                if (connections[j].to == nodes[k].name) {
                                    var content = nodes[k].content;
                                    var x = nodes[k].x;
                                    var y = nodes[k].y;
                                    var type = nodes[k].type;

                                    drawNode(nodes[k].content, nodes[k].type, x, y)
                                }
                            }
                        }

                        var x1 = connections[j].x1;
                        var x2 = connections[j].x2;
                        var x2 = connections[j].x2;
                        var y1 = connections[j].y1;
                        var y2 = connections[j].y2;
                        var rel = active_connections[i].rel;

                        drawConnection(x1, x2, y1, y2, rel);
                    }
                }
            }/*finaliza aca*/
        }

        drawSvg(0);
        drawGraph(current_model);
        writeDescription(current_description);


        $("#hover-btn").hover(function(){
            
        });




        $(document).on('change', $('#sel_grraph'), function(){
            var values = $('#sel_graph').val();
            var index = $('#sel_graph option:selected').index();
            values = values.split(",");

            $('svg').remove();
            $('#sel_graph').remove();
            $('#description-graph').remove();
            drawSvg(index);

            values.forEach(function (item, index) {
                drawGraph(item);
            });
            var description = settings.models[index].descriptions;
            writeDescription(description);

        });

    }

})(jQuery);
