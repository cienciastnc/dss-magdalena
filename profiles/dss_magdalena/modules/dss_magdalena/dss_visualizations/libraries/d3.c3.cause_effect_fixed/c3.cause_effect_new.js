

function magnify(imgID, zoom) {
    var img, glass, w, h, bw;
    img = document.getElementById(imgID);
  
    /* Create magnifier glass: */
    glass = document.createElement("DIV");
    glass.setAttribute("class", "img-magnifier-glass");
  
    /* Insert magnifier glass: */
    img.parentElement.insertBefore(glass, img);
  
    /* Set background properties for the magnifier glass: */
    glass.style.backgroundImage = "url('" + img.src + "')";
    glass.style.backgroundRepeat = "no-repeat";
    glass.style.backgroundSize = (img.width * zoom) + "px " + (img.height * zoom) + "px";
    bw = 3;
    w = glass.offsetWidth / 2;
    h = glass.offsetHeight / 2;
  
    /* Execute a function when someone moves the magnifier glass over the image: */
    glass.addEventListener("mousemove", moveMagnifier);
    img.addEventListener("mousemove", moveMagnifier);
  
    /*and also for touch screens:*/
    glass.addEventListener("touchmove", moveMagnifier);
    img.addEventListener("touchmove", moveMagnifier);
    function moveMagnifier(e) {
      var pos, x, y;
      /* Prevent any other actions that may occur when moving over the image */
      e.preventDefault();
      /* Get the cursor's x and y positions: */
      pos = getCursorPos(e);
      x = pos.x;
      y = pos.y;
      /* Prevent the magnifier glass from being positioned outside the image: */
      if (x > img.width - (w / zoom)) {x = img.width - (w / zoom);}
      if (x < w / zoom) {x = w / zoom;}
      if (y > img.height - (h / zoom)) {y = img.height - (h / zoom);}
      if (y < h / zoom) {y = h / zoom;}
      /* Set the position of the magnifier glass: */
      glass.style.left = (x - w) + "px";
      glass.style.top = (y - h) + "px";
      /* Display what the magnifier glass "sees": */
      glass.style.backgroundPosition = "-" + ((x * zoom) - w + bw) + "px -" + ((y * zoom) - h + bw) + "px";

    }
  
    function getCursorPos(e) {
      var a, x = 0, y = 0;
      e = e || window.event;
      /* Get the x and y positions of the image: */
      a = img.getBoundingClientRect();
      /* Calculate the cursor's x and y coordinates, relative to the image: */
      x = e.pageX - a.left;
      y = e.pageY - a.top;
      /* Consider any page scrolling: */
      x = x - window.pageXOffset;
      y = y - window.pageYOffset;
      return {x : x, y : y};
    }
  }

/**
 * @file
 * Javascript for [library name].
 */

(function($) {

    /**
     * Adds library to the global d3 object.
     *
     * @param select
     * @param settings
     *   Array of values passed to d3_draw.
     *   id: required. This will be needed to attach your
     *       visualization to the DOM.
     */
    Drupal.d3.cause_effect_fixed = function (select, settings) {
        // Your custom JS.
        var isfullactive=false;
        var current_model = settings.models[0].models[0];
        var current_description = settings.models[0].descriptions;
        basePath='/profiles/dss_magdalena/modules/dss_magdalena/dss_visualizations/libraries/d3.c3.cause_effect_fixed/img/'

        svg1 = d3.select('#' + settings.id)
        .append('div')
        .attr('id', 'description-graph')
        .attr('width', 900)
        .attr('height', 100);
        svg3 = d3.select('#' + settings.id)
        .append('div')
        .attr('id', 'hover-btn').attr('title','dar clic para intercambiar imagen entre el cuadro de referencia y ')
        .append("img").attr('src',basePath+'preview.png').attr("width",'60');
        d3.select('#hover-btn').append("p").text("Cuadro de referencia").attr('font-size','small');
        svg1 = d3.select('#' + settings.id)
        .append('div')
        .attr('id', 'img-c').style("position","relative")
        .append('img').attr('id','img-model-graph');

        $("#img-model-graph").text(basePath+current_model+".jpg");
        $("#img-model-graph").attr('src',basePath+current_model+".jpg");
/*
        $("#hover-btn *").hover(function(){
            $("#img-model-graph").text(basePath+"full.jpg");
            $("#img-model-graph").attr('src',basePath+"full.jpg");
        });
        $("#hover-btn *").mouseout(function(){
            $("#img-model-graph").text(basePath+current_model+".jpg");
            $("#img-model-graph").attr('src',basePath+current_model+".jpg");
        });
*/
        $("#hover-btn *").click(function(){
          if(isfullactive){
            $("#img-model-graph").text(basePath+current_model+".jpg");
            $("#img-model-graph").attr('src',basePath+current_model+".jpg");
            $(".img-magnifier-glass").remove();
            magnify("img-model-graph", 1.5);
            isfullactive=false;
          }else{
            $("#img-model-graph").text(basePath+"full.jpg");
            $("#img-model-graph").attr('src',basePath+"full.jpg");
            $(".img-magnifier-glass").remove();
            magnify("img-model-graph", 1.5);
            isfullactive=true;
          }

      });



        writeDescription(current_description);
        function writeDescription(text) {
            d3.select('#description-graph').append("text")
              .attr("class", "smallorange")
              .attr("x", 10)
              .attr("y", 20)
              .html(text);
        }

    }
    $( window ).on( "load", function() { magnify("img-model-graph", 1.5);})
    
})(jQuery);

