/**
 * @file
 * Javascript for [library name].
 */

(function($) {

  /**
   * Adds library to the global d3 object.
   *
   * @param select
   * @param settings
   *   Array of values passed to d3_draw.
   *   id: required. This will be needed to attach your
   *       visualization to the DOM.
   */
  Drupal.d3.categories = function(select, settings) {
    // Your custom JS.
    var columns = [];
    var categories = [];

    // Build columns array for chart1.
    for (var i = 0; i < settings.legend.length; i++) {
      var current_column = [];
      current_column.push(settings.legend[i]);
      for (var j = 0; j < settings.rows[i].length; j++) {
        current_column.push(settings.rows[i][j][1]);
      }
      columns.push(current_column);
    }

    // Build categories array for chart1.
    for (var i = 0; i < settings.rows[0].length; i++) {
      categories.push(settings.rows[0][i][0]);
    }

    var chart = c3.generate({
      bindto: '#' + settings.id,
      data: {
          columns: columns,
          type: "bar"
      },
      axis: {
          x: {
              type: 'category',
              categories: categories
          },
          y: {
            label:  settings.units
          }
      }
    });
  }

})(jQuery);
