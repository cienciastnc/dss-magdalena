/**
 * @file
 * Javascript for [library name].
 */

(function ($) {

  /**
   * Adds library to the global d3 object.
   *
   * @param select
   * @param settings
   *   Array of values passed to d3_draw.
   *   id: required. This will be needed to attach your
   *       visualization to the DOM.
   */
  Drupal.d3.cause_effect = function (select, settings) {
    // Your custom JS.
    var links = settings.rows;

    var nodes = {};

    // Compute the distinct nodes from the links.
    links.forEach(function (link) {
      link.source = nodes[link.source] || (nodes[link.source] = { name: link.source, source_type: link.source_type });
      link.target = nodes[link.target] || (nodes[link.target] = { name: link.target, target_type: link.target_type });
    });

    var width = 1160,
      height = 500;

    var force = d3.layout.force()
      .nodes(d3.values(nodes))
      .links(links)
      .size([width, height])
      .linkDistance(200)
      .charge(-300)
      .on("tick", tick)
      .start();

    var svg = d3.select("#" + settings.id).append("svg")
      .attr("width", width)
      .attr("height", height);

    // Per-type markers, as they don't inherit styles.
    svg.append("defs").selectAll("marker")
      .data(["suit", "licensing", "resolved"])
      .enter().append("marker")
      .attr("id", function (d) { return d;
      })
      .attr("viewBox", "0 -5 10 10")
      .attr("refX", 15)
      .attr("refY", -1.5)
      .attr("markerWidth", 6)
      .attr("markerHeight", 6)
      .attr("orient", "auto")
      .append("path")
      .attr("d", "M0,-5L10,0L0,5");

    var path = svg.append("g").selectAll("path")
      .data(force.links())
      .enter().append("path")
      .attr("class", function (d) { return "link " + d.type;
      })
      .attr("marker-end", function (d) { return "url(#" + d.type + ")";
      });

    var circle = svg.append("g").selectAll("circle")
      .data(force.nodes())
      .enter().append("circle")
      .attr("class", function (d) {
        if (d.source_type) {
          return "circle " + d.source_type
        }
        else if (d.target_type) {
          return "circle " + d.target_type
        }
      })
      .attr("r", 6)
      .call(force.drag);

    var text = svg.append("g").selectAll("text")
      .data(force.nodes())
      .enter().append("text")
      .attr("x", 8)
      .attr("y", ".31em")
      .text(function (d) { return d.name;
      });

    var container = d3.select('#' + settings.id);

    var legend1_flag = 1;
    var legend2_flag = 1;
    var legend3_flag = 1;
    container.append("div")
              .attr("class", "legend_control")
              .style("color", "black")
              .style("font-size", "24px")
              .on("click", function(){
                var newOpacity = legend1_flag == 1 ? 0 : 1;
                if (newOpacity == 0) {
                  d3.select(this).style("color", "lightgray")
                }
                else {
                  d3.select(this).style("color", "black")
                }
                d3.selectAll("path.link.suit").style("opacity", newOpacity);
                legend1_flag = newOpacity;
              })
              .text("Relaciones ⤷");

    container.append("div")
              .attr("class", "legend_control")
              .style("color", "black")
              .style("font-size", "24px")
              .on("click", function(){
                var newOpacity = legend2_flag == 1 ? 0 : 1;
                if (newOpacity == 0) {
                  d3.select(this).style("color", "lightgray")
                }
                else {
                  d3.select(this).style("color", "black")
                }
                d3.selectAll("path.link.resolved").style("opacity", newOpacity);
                legend2_flag = newOpacity;
              })
              .text("Relaciones ⇣");

    container.append("div")
              .attr("class", "legend_control")
              .style("color", "black")
              .style("font-size", "24px")
              .on("click", function(){
                var newOpacity = legend3_flag == 1 ? 0 : 1;
                if (newOpacity == 0) {
                  d3.select(this).style("color", "lightgray")
                }
                else {
                  d3.select(this).style("color", "black")
                }
                d3.selectAll("path.link.other").style("opacity", newOpacity);
                legend3_flag = newOpacity;
              })
              .text("Relaciones 𝄄");

    // Use elliptical arc path segments to doubly-encode directionality.
    function tick() {
      path.attr("d", linkArc);
      circle.attr("transform", transform);
      text.attr("transform", transform);
    }

    function linkArc(d) {
      var dx = d.target.x - d.source.x,
        dy = d.target.y - d.source.y,
        dr = Math.sqrt(dx * dx + dy * dy);
      return "M" + d.source.x + "," + d.source.y + "A" + dr + "," + dr + " 0 0,1 " + d.target.x + "," + d.target.y;
    }

    function transform(d) {
      return "translate(" + d.x + "," + d.y + ")";
    }

  }

})(jQuery);
