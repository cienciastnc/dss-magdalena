/**
 * @file
 * Javascript for [library name].
 */

(function($) {

  /**
   * Adds library to the global d3 object.
   *
   * @param select
   * @param settings
   *   Array of values passed to d3_draw.
   *   id: required. This will be needed to attach your
   *       visualization to the DOM.
   */
  Drupal.d3.boxplot = function (select, settings) {
    // Your custom JS.
    var data = settings.rows;

    var visualization = d3plus.viz()
      .container("#sima")
      .height(700)
      .data(data)
      .type("box")
      .id("name")
      .x(settings.x_value)
      .y(settings.y_value)
      .time("year")
      .format("es_ES")
      .font({"size": 18})
      .legend({"size": 38})
      .draw();

    setTimeout(function () {
      var yLabel = d3.select('#d3plus_graph_ylabel').text();
      yLabel = yLabel + " en (" + settings.units + ")";
      d3.select('g#timeline').remove();
      d3.select('#d3plus_graph_ylabel').text(yLabel);
    }, 1500);
    
    

  }

})(jQuery);
