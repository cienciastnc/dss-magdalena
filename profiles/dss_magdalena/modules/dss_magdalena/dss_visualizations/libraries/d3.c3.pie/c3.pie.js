/**
 * @file
 * Javascript for [library name].
 */

(function($) {

  /**
   * Adds library to the global d3 object.
   *
   * @param select
   * @param settings
   *   Array of values passed to d3_draw.
   *   id: required. This will be needed to attach your
   *       visualization to the DOM.
   */
  Drupal.d3.pie = function (select, settings) {
    // Your custom JS.
    d3.select('#' + settings.id).append('div')
                                .attr('id', 'split1_chart');

    d3.select('#' + settings.id).append('div')
                                .attr('id', 'split2_chart');

    d3.selection.prototype.moveToBack = function() {
        return this.each(function() {
            var firstChild = this.parentNode.firstChild;
          if (firstChild) {
              this.parentNode.insertBefore(this, firstChild);
          }
        });
    };

    var sum = 0.0;

    for (var i = 0; i < settings.rows.length; i++) {
      var sum = sum + settings.rows[i][3];
    }

    var chart = c3.generate({
      bindto: '#split2_chart',
      size : {
        width: 400,
        height: 400
      },
      padding: {
        top: 100,
        left: 100,
        right: 100,
        bottom: 100
      },
      data: {
        // Iris data from R.
        columns: settings.rows,
        type : 'pie',
        onclick: function (d, i) { console.log("onclick", d, i);
        },
        onmouseover: function (d, i) { console.log("onmouseover", d, i);
        },
        onmouseout: function (d, i) { console.log("onmouseout", d, i);
        }
      }
    });

    var circle_r = (1.0 * 83) / sum;

    var svg = d3.select('svg');
    var circle = svg.append("circle").attr("cx", 200.5)
                    .attr("cy", 187.5)
                    .attr("r", circle_r)
                    .style("stroke", "black")
                    .style("fill-opacity", 0)
                    .style("stroke-dasharray", 5,5);

    circle.moveToBack();

    // Generate HTML Table.
    var table = d3.select('#split1_chart').append('table').attr('id', 'pie_table');
    var thead = table.append('thead');
    var tbody = table.append('tbody');
    var tfoot = table.append('tfoot');

    // Fill table header.
    var thead_tr = thead.append('tr');
    thead_tr.append('th').text('Indice');
    thead_tr.append('th').text('V.E');
    thead_tr.append('th').text('Peso');
    thead_tr.append('th').text('Valor');

    // Fill table body.
    var total_peso = 0;
    var total_valor = 0;
    for (var i = 0; i < settings.rows.length; i++) {
      var tbody_tr = tbody.append('tr');
      tbody_tr.append('td').text(settings.rows[i][0]);
      tbody_tr.append('td').text(settings.rows[i][1]);
      tbody_tr.append('td').text(settings.rows[i][2]);
      tbody_tr.append('td').text(settings.rows[i][3]);
      total_peso += settings.rows[i][2];
      total_valor += settings.rows[i][3];
    }

    // Fill table footer.
    var tfoot_tr = tfoot.append('tr');
    tfoot_tr.append('td').text('Total').attr('colspan', 2);
    tfoot_tr.append('td').text(total_peso);
    tfoot_tr.append('td').text(total_valor);

    var p = d3.select('#split1_chart')
              .append('p')
              .text("* Cuanto mas cerca esté el pie central al circulo punteado mas cerca es su valor a 1");
  }

})(jQuery);
