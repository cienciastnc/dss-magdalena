/**
 * @file
 * Javascript for [library name].
 */

(function ($) {

    /**
     * Adds library to the global d3 object.
     *
     * @param select
     * @param settings
     *   Array of values passed to d3_draw.
     *   id: required. This will be needed to attach your
     *       visualization to the DOM.
     */
    Drupal.d3.map_aggregation = function (select, settings) {
        // Obtaining GeoJSON and DataStore URLs.
        let page_url = location.pathname.split("/");

        // Check what kind of separator is used "%2C" or ","
        let separator = "%2C";
        if (page_url[2].includes(",")) {
            separator = ",";
        }

        //Get list of nodes
        let rid_array = page_url[2].split(separator);
        settings.nodes = rid_array;

        //Set expiration time
        let exp_hours = 12;
        let expiration_time_ms = exp_hours * 3600 * 1000;

        //Load data from nodes on rid_array
        //localStorage.clear();
        let ajax_results = [];
        settings.geomap = [];
        settings.data_ext = [];
        settings.data = [];
        settings.data_keyed = [];
        settings.data_mean_keyed = [];
        settings.geomap2 = [];
        rid_array.forEach(function (e_rid, e_index) {
            //Check if data is cached
            if (localStorage.getItem('resource-data-' + e_rid) !== null) {
                //Get decompressed object
                let obj_string = LZString.decompressFromUTF16(localStorage.getItem('resource-data-' + e_rid));
                let obj_data = JSON.parse(obj_string);
                settings.geomap[e_index] = obj_data.geomap;
                settings.data_ext[e_index] = obj_data.data_ext;
                settings.data[e_index] = obj_data.data;

                //Get timestamp
                let obj_timestamp = obj_data.time_stamp;
                let now_timestamp = new Date().getTime();
                let diff_timestamp = now_timestamp - obj_timestamp;

                //Check expiration time
                if (diff_timestamp > expiration_time_ms) {
                    localStorage.removeItem('resource-data-' + e_rid);
                    getDataSources(e_rid, e_index);
                }
            }
            else {
                //Getting data from Data sources and Geojson
                getDataSources(e_rid, e_index);
            }
        });

        //Call graph
        $.when.apply(this, ajax_results).done(function () {
            Drupal.d3.drawMapChoropleth(settings);
        });

        function getDataSources(e_rid, e_index) {
            //Getting data from Data sources and Geojson
            let data_url = Drupal.settings.basePath + 'resource-data/' + e_rid;
            ajax_results.push($.getJSON(data_url, function (json) {
                //Getting data
                let geojson = JSON.parse(json.layer);
                let datastore = JSON.parse(json.datastore);
                settings.geomap[e_index] = geojson;
                settings.data[e_index] = datastore.items;
                settings.data_ext[e_index] = datastore.ext;

                //Creates the object to be stored
                let string_obj_to_storage = JSON.stringify({
                    geomap: geojson,
                    data_ext: datastore.ext,
                    data: datastore.items,
                    time_stamp: new Date().getTime()
                });

                //Compress and Store object to cache
                try {
                    localStorage.setItem('resource-data-' + e_rid, LZString.compressToUTF16(string_obj_to_storage));
                }
                catch (e) {
                    localStorage.removeItem('resource-data-' + e_rid);
                    setMessage(Drupal.t("Not enough cache to storage on web browser."));
                }
            }));
        }

        //
        function setMessage(v_str_msg) {
            //Create missing properties
            if (typeof Drupal.settings.d3.inventory.notice0 == "undefined") {
                Drupal.settings.d3.inventory.notice0 = {};
                Drupal.settings.d3.inventory.notice0.rows = [];
            }
            else if (typeof Drupal.settings.d3.inventory.notice0.rows == "undefined") {
                Drupal.settings.d3.inventory.notice0.rows = [];
            }

            //Add the msg string to message
            Drupal.settings.d3.inventory.notice0.rows.push(v_str_msg);

            /*            let innerDivHtml = document.getElementById("msg").innerHTML;
                        if(innerDivHtml !== ''){
                            innerDivHtml = innerDivHtml + '<sub>, ' +v_str_msg +'</sub>';
                        }else{
                            innerDivHtml = '<sub>' + Drupal.t('Note') + ': '+ v_str_msg +'</sub>';
                        }
                        document.getElementById("msg").innerHTML = innerDivHtml;*/
        }

        function clearMessage() {
            document.getElementById("msg").innerHTML = '';
        }
    }

})(jQuery);