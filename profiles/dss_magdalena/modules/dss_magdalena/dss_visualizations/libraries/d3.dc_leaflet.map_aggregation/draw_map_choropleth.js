/**
 * Created by jclaros on 16/04/17.
 */

Drupal.d3.drawMapChoropleth = function (settings) {
    //Set jQuery variable for compatibility
    let $ = jQuery;

    //Sanity Check if there is a GeoJson
    settings.geomap.forEach(function (e_geomap, e_index) {
        if (e_geomap === null) {
            setMessage(Drupal.t("There is no GeoJson associated to node @node.", {
                '@node': settings.nodes[parseInt(e_index)]
            }));
        }
    });

    //Sanity Check if there is data send
    if(settings.data.length === 0){
        alert("PRECAUCIÓN: NO HAY DATA A SER MOSTRADA.");
        setMessage(Drupal.t("PRECAUCIÓN: NO HAY DATA A SER MOSTRADA."));
    }else if(settings.data[0].length === 0) {
        alert("PRECAUCIÓN: NO HAY DATA A SER MOSTRADA.");
        setMessage(Drupal.t("PRECAUCIÓN: NO HAY DATA A SER MOSTRADA."));
    }

    //Get type of "value" data to set dimensions
    let hasMultipleDimension = false;
    let dimensionNames = [];
    if (typeof settings.data[0][0]['value'] !== "undefined") {
        let objValue = settings.data[0][0]['value'];
        if (isJson(objValue)) {
            //set dimension flag
            hasMultipleDimension = true;

            //Set dimension Names
            dimensionNames = Object.keys(JSON.parse(objValue));
        }
    }

    //Detecting temporality type of the graph
    //0 - normal var, 1 - not year, 2 - not timestep, 3 - not year and not
    // timestep, 4 - unable to detect
    let graph_temporality_type = 0;
    if ((typeof settings.data_ext !== "undefined") && (typeof settings.data_ext[0] !== "undefined")) {
        //Check year
        if ((settings.data_ext[0].max_year === null) || (settings.data_ext[0].min_year === null)) {
            graph_temporality_type = 1;
        }

        //Check timestep
        if ((settings.data_ext[0].max_timestep === null) || (settings.data_ext[0].min_timestep === null)) {
            //Ads the revious value to detect the third case
            graph_temporality_type = 2 + graph_temporality_type;
        }
    } else {
        graph_temporality_type = 4;
    }

    //Temporality - Sanity Check
    if (graph_temporality_type === 4) {
        setMessage(Drupal.t("WARNING: NO POSSIBLE TO DETECT THE VARIABLE TEMPORALITY."));
    }
    if (graph_temporality_type === 2 || graph_temporality_type === 3) {
        //Only for time graph
        setMessage(Drupal.t("NO POSSIBLE TO DISPLAY."));
    }

    //Support for different types of temporality
    let none_year = "0";
    let none_timestep = "0";

    //Pre-processing data
    //settings.data_keyed = [];
    settings.data.forEach(function (e_data, e_index) {
        let data_keyed = {};
        if (typeof e_data !== 'undefined') {
            if (e_data.length > 0) {
                for (let index = 0; index < e_data.length; ++index) {
                    //Set year data, if not define use default "none_year" value
                    let year_data = e_data[index]["year"];
                    if (year_data) {
                        //DO NOTHING
                    } else {
                        year_data = none_year;
                    }

                    //Set timestep data, if not define use default
                    // "none_timestep" value
                    let timestep_data = e_data[index]["timestep"];
                    if (timestep_data) {
                        //DO NOTHING
                    } else {
                        timestep_data = none_timestep;
                    }

                    // Set the composed key
                    let composed_year_key = year_data + '.' + timestep_data;

                    //Index Data
                    if (typeof data_keyed[getElementNameString(e_data[index]["geobranchid"])] === "undefined") {
                        data_keyed[getElementNameString(e_data[index]["geobranchid"])] = {};
                    }
                    data_keyed[getElementNameString(e_data[index]["geobranchid"])][composed_year_key] = e_data[index]["value"];
                }
            } else {
                setMessage(Drupal.t("Empty data at node") + " @node.", {
                    '@node': settings.nodes[parseInt(e_index)]
                });
            }
        } else {
            setMessage(Drupal.t("Not data found at node") + " @node.", {
                '@node': settings.nodes[parseInt(e_index)]
            });
        }
        //console.log(data_keyed);
        settings.data_keyed[e_index] = data_keyed;
    });

    //Set aggregation function select options
    let select_options_line_curve = [Drupal.t('Valor Original'), Drupal.t('Max, Min, Averages'), Drupal.t('Summation')];
    let select_options_histogram = [Drupal.t('Maximum'), Drupal.t('Minimum'), Drupal.t('Average'), Drupal.t('W. Average')];
    let select_options_duration = [Drupal.t('Default'), Drupal.t('January'), Drupal.t('February'), Drupal.t('March'), Drupal.t('April'), Drupal.t('May'), Drupal.t('June'), Drupal.t('July'), Drupal.t('August'), Drupal.t('September'), Drupal.t('October'), Drupal.t('November'), Drupal.t('December')]
    let select_options_array = [select_options_line_curve, select_options_histogram, select_options_duration];

    //Set type of view graphs for temporality
    let select_options_view_graph_temporality_0 = {
        "0": Drupal.t('Serie de tiempo'),
        "1": Drupal.t('Ciclo anual'),
        "2": Drupal.t('Duracion')
    };
    let select_options_view_graph_temporality_1 = {
        "1": Drupal.t('Ciclo anual'),
    };
    let select_options_view_graph_temporality_234 = {};

    //Set view graph options and set the selected one
    let selected_graph_option = null;
    let select_options_view_graph = null;
    switch (graph_temporality_type) {
        case 0:
            selected_graph_option = "0";
            select_options_view_graph = select_options_view_graph_temporality_0;
            break;
        case 1:
            selected_graph_option = "1";
            select_options_view_graph = select_options_view_graph_temporality_1;
            break;
        case 2:
        case 3:
            select_options_view_graph = select_options_view_graph_temporality_234;
            break;
        default:
            selected_graph_option = "0";
            select_options_view_graph = select_options_view_graph_temporality_0;
    }

    //Calc number of steps on variable
    let num_steps = [];
    settings.data_ext.forEach(function (e_data_ext, e_index) {
        if (typeof e_data_ext !== 'undefined') {
            num_steps[e_index] = e_data_ext['max_timestep'] - e_data_ext['min_timestep'] + 1;
        } else {
            setMessage(Drupal.t("Not extended data found") + " @node.", {
                '@node': settings.nodes[parseInt(e_index)]
            });
        }
    });

    // Set default scale values
    let scale_default = [1000000, 1000, 100, 10, 1, 1 / 10, 1 / 100, 1 / 1000, 1 / 1000000];
    let scale_label_default = ['*Millon', '*1k', '*100', '*10', '', '/10', '/100', '/1k', '/Millon'];

    //Create leaflet layers
    let color_tile = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
        subdomains: ['a', 'b', 'c']
    });

    let gray_tile = L.tileLayer.grayscale('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
        subdomains: ['a', 'b', 'c']
    });

    let empty_tile = L.tileLayer('');

    //Map Layer Control
    let baseMaps = {
        "Color": color_tile,
        "Grey": gray_tile,
        "Blank": empty_tile
    };

    //Create the leaflet map
    let mymap = L.map('sima', {
        layers: [color_tile],
        fullscreenControl: true,
        fullscreenControlOptions: {
            position: 'topleft'
        }
    }).setView([6.5, -75], 6);

    //Sets default leaflet icon Path
    L.Icon.Default.imagePath = '/profiles/dss_magdalena/libraries/leaflet/dist/images/';

    //Create default pane
    let defaultPane = mymap.createPane('default');
    defaultPane.style.zIndex = 410;

    //Adds the GeoJson Layer, using only the first geomap in array
    let geojson = L.geoJSON(settings.geomap[0], {
        pane: 'default',
        style: style,
        onEachFeature: onEachFeature,
        pointToLayer: pointToLayer
    }).addTo(mymap);

    //Adds default overlay to overlay control object
    let default_map_name = Drupal.t('Default');
    let overlays = {};
    overlays[default_map_name] = geojson;

    //Add Context layer
    let context_layer_names = settings.context_layer_machine_names;
    Object.keys(context_layer_names).forEach(function (e_key, e_index) {
        //Create a leflet pane for current processing context overlay
        let context_machine_name = context_layer_names[e_key];
        let temp_pane_name = context_machine_name + '-' + e_index.toString()
        let temp_Pane = mymap.createPane(temp_pane_name);
        temp_Pane.style.zIndex = 450;

        //Sets leaflet overlay layers and add them to overlay control object
        overlays[e_key] = L.geoJSON(null, {
            pane: temp_pane_name,
            style: style_context,
            onEachFeature: onEachFeature_context
        });
    });

    //mymap.getPane("streamflow_gauges-0").style.zIndex = 610;

    //Set the leaflet layer control
    L.control.layers(baseMaps, overlays, {position: 'bottomleft'}).addTo(mymap);

    //Var used to store the selected geo elements
    let selected_elements = [];

    //Saves y-label titles
    let y_label_curve_titles = [];
    let y_label_histo_titles = [];
    settings.nodes.forEach(function (e_node, e_index) {
        y_label_curve_titles.push(Drupal.settings.d3.inventory['curve' + e_index.toString()].y_label);
        y_label_histo_titles.push(Drupal.settings.d3.inventory['histo' + e_index.toString()].y_label);
    });
    let y_label_curve_comparison = null;
    let y_label_histo_comparison = null;
    if (settings.nodes.length > 1) {
        y_label_curve_comparison = Drupal.settings.d3.inventory.curve_integrated.y_label;
        y_label_histo_comparison = Drupal.settings.d3.inventory.histo_integrated.y_label;
    }

    //Control box
    let info = L.control();

    //Creates the control label
    info.onAdd = function (map) {
        this._div = L.DomUtil.create('div', 'info'); // create a div with a
                                                     // class "info"
        this.update();
        return this._div;
    };

    //Method that we will use to update the control based on feature properties
    // passed
    info.update = function (props) {
        //Update control info
        this._div.innerHTML = (
            props ?
                Drupal.t('Element') + ': <b>' + (props.nom_visual ? props.nom_visual.toUpperCase() : getElementNameString(props.Branch.toUpperCase())) + '</b>'
                : '<h4>' + Drupal.t('Choose an element') + '</h4>');
    };
    info.addTo(mymap);


    /** JQuery **/

    //Define jQuery vars
    let edit_aggregation_select = $("#edit-aggregation-select");
    let edit_scale_select = $("#edit-scale-select");
    let edit_view_graph_select = $("#edit-view-graph-select");
    let edit_visualization_control_select = $("#edit-visualization-control-select");
    let edit_aggregation_select_dimension = $("#edit-aggregation-select-dimension");
    let table_id = $("#" + settings.dina_table);
    let table_label_id = $("#" + settings.dina_label);

    //Update view graph options
    edit_view_graph_select.empty();
    Object.keys(select_options_view_graph).forEach(function (e_key_option, e_index) {
        edit_view_graph_select.append('<option value="' + e_key_option + '">' + select_options_view_graph[e_key_option] + '</option>');
    });

    //If edit_aggregation_select is empty set an "no option" option
    if (edit_view_graph_select.find('option').length === 0) {
        edit_view_graph_select.append('<option disabled selected value>' + Drupal.t('- No options -') + '</option>');
    }

    //Handle Select new changes
    edit_aggregation_select.on("change", function (slideEvt) {
        //Control visualization of graphs
        update_graphs_visualization();

        //Graph line curve or histogram graphs
        if (parseInt(edit_view_graph_select.find(':selected').val()) === 0) {
            //Update line curve graph data
            update_line_curve_data();

            //Draw dynamic line_curve
            for (let i = 0; i < settings.nodes.length; i++) {
                //Update tool grouping for multidimensional graphs
                // if(parseInt(settings.weap_level) > 2){
                //     Drupal.settings.d3.inventory['curve' +
                // i.toString()].tooltip_grouped =
                // parseInt(edit_aggregation_select.find(':selected').val())
                // !== 0; }

                //Call line curve graph
                Drupal.d3.line_curve('curve' + i.toString(), Drupal.settings.d3.inventory['curve' + i.toString()]);
            }

            //Draw comparison line_curve
            if (settings.nodes.length > 1) {
                Drupal.d3.line_curve('curve_integrated', Drupal.settings.d3.inventory.curve_integrated);
            }
        } else if (parseInt(edit_view_graph_select.find(':selected').val()) === 1) {
            //Update histogram graph data
            update_histogram_data();

            //Draw dynamic line_curve
            for (let i = 0; i < settings.nodes.length; i++) {
                //Update tool grouping for multidimensional graphs
                // if(parseInt(settings.weap_level) > 2){
                //     Drupal.settings.d3.inventory['histo' +
                // i.toString()].tooltip_grouped =
                // parseInt(edit_aggregation_select.find(':selected').val())
                // !== 0; }

                //Call line curve graph
                Drupal.d3.histogram('histo' + i.toString(), Drupal.settings.d3.inventory['histo' + i.toString()]);
            }

            //Draw comparison line_curve
            if (settings.nodes.length > 1) {
                Drupal.d3.histogram('histo_integrated', Drupal.settings.d3.inventory.histo_integrated);
            }
        } else {
            //Update line curve graph data for duration
            update_line_curve_duration_data();

            //set x-axis scale

            //Draw dynamic line_curve
            for (let i = 0; i < settings.nodes.length; i++) {
                //Call line curve graph
                Drupal.d3.line_curve('curve' + i.toString(), Drupal.settings.d3.inventory['curve' + i.toString()]);
            }

            //Draw comparison line_curve
            if (settings.nodes.length > 1) {
                Drupal.d3.line_curve('curve_integrated', Drupal.settings.d3.inventory.curve_integrated);
            }
        }
    });

    //Handle Selected scale new changes
    edit_scale_select.on("change", function (slideEvt) {
        // Trigger an event to the other select.
        edit_aggregation_select.trigger("change");
    });

    //Handle Selected view control changes
    edit_view_graph_select.on("change", function (slideEvt) {
        //Update aggregation function combo accordingly to type of graph
        // selected
        let select_view_graph_value = parseInt(edit_view_graph_select.find(':selected').val());
        edit_aggregation_select.empty();
        if (!isNaN(select_view_graph_value)) {  //Check if the selected value is not empty
            select_options_array[select_view_graph_value].forEach(function (e_new_option, e_index) {
                //Exclude from duration graph month elements for variables with
                // timestep of 1
                if ((select_view_graph_value === 2) && (e_index > 0) && (parseInt(settings.data_ext[0]['max_timestep']) === 1)) {
                    return;
                }

                edit_aggregation_select.append('<option value="' + e_index.toString() + '">' + e_new_option + '</option>');
            });
        }

        //If edit_aggregation_select is empty set an "no option" option
        if (edit_aggregation_select.find('option').length === 0) {
            edit_aggregation_select.append('<option disabled selected value>' + Drupal.t('- No options -') + '</option>');
        }

        // Trigger an event to the other select.
        edit_aggregation_select.trigger("change");
    });

    //Handle multi-dimensions change event using JQuery
    edit_aggregation_select_dimension.on("change", function (e) {
        // Trigger an event to the other select.
        edit_aggregation_select.trigger("change");
    });

    //Handle the way to be shown the comparison graphs
    if (settings.nodes.length > 1) {
        edit_visualization_control_select.on("change", function (slideEvt) {
            update_graphs_visualization();
        });
    }

    //Set Hide and show of table of variable's properties
    table_id.hide();
    table_label_id.on("click", function (ev) {
        table_id.toggle("fold", 800);
    });

    //Hide dc Graph on load
    hideLineCurveGraph();
    hideHistogramGraph();

    //Set actual view graph option
    edit_view_graph_select.val(selected_graph_option);
    edit_view_graph_select.trigger("change");

    /** Load Context GeoJson **/

        //Getting Context geojson layer
    let context_layer_obj = {};
    let ajax_results = [];
    Object.keys(context_layer_names).forEach(function (e_key, e_index) {
        //Check if data is cached
        let context_machine_name = context_layer_names[e_key];
        if (localStorage.getItem('context-layer-' + context_machine_name) !== null) {
            //Get decompressed object
            let obj_string = LZString.decompressFromUTF16(localStorage.getItem('context-layer-' + context_machine_name));
            let obj_data = JSON.parse(obj_string);
            context_layer_obj[e_key] = JSON.parse(obj_data.layer);
        } else {
            let data_url2 = Drupal.settings.basePath + 'layers/' + context_machine_name + ".geojson";
            ajax_results.push($.getJSON(data_url2, function (json) {
                //Getting data
                context_layer_obj[e_key] = JSON.parse(json.layer);

                //Convert o string the object to be stored
                let string_obj_to_storage = JSON.stringify(json);

                //Compress and Store object to cache
                try {
                    localStorage.setItem('context-layer-' + context_machine_name, LZString.compressToUTF16(string_obj_to_storage));
                } catch (e) {
                    localStorage.removeItem('context-layer-' + context_machine_name);
                    setMessage(Drupal.t("Not enough cache to storage on web browser for context layer."));
                }
            }));
        }
    });

    //Update message
    Drupal.d3.page_notice();

    //Runs after ajax loaded the context layers
    $.when.apply(this, ajax_results).done(function () {
        //Adds leaflet layer data for each context
        Object.keys(overlays).forEach(function (e_key, e_index) {
            //Sanity check
            if (typeof overlays[e_key] === 'undefined') {
                return;
            }

            //Exclude main overlay
            if (e_key === default_map_name) {
                return;
            }

            //Adds layer data
            overlays[e_key].addData(context_layer_obj[e_key]);
        });
    });


    /** Helper functions **/

    function update_graphs_visualization() {
        //Check if there are elements selected if not hide the all line_curve
        // graph and histogram graphs
        if (selected_elements.length === 0) {
            //Reset all line curve data
            for (let i = 0; i < settings.nodes.length; i++) {
                Drupal.settings.d3.inventory['curve' + i.toString()].rows = [];
            }
            if (typeof Drupal.settings.d3.inventory.curve_integrated !== 'undefined') {
                Drupal.settings.d3.inventory.curve_integrated.rows = [];
            }

            //Reset all histogram data
            for (let i = 0; i < settings.nodes.length; i++) {
                Drupal.settings.d3.inventory['histo' + i.toString()].rows = [];
            }
            if (typeof Drupal.settings.d3.inventory.histo_integrated !== 'undefined') {
                Drupal.settings.d3.inventory.histo_integrated.rows = [];
            }

            //Hide line curve graphs
            hideLineCurveGraph();

            //Hide histogram graphs
            hideHistogramGraph();

            return;
        }

        //Set visible line_curve graph if it's hidden
        if (settings.nodes.length > 1) {
            hideLineCurveGraph();
            hideHistogramGraph();

            edit_visualization_control_select.show();

            if (parseInt(edit_view_graph_select.find(':selected').val()) === 1) {     //Histogram
                if (parseInt(edit_visualization_control_select.find(':selected').val()) > 0) {
                    $('#histo_integrated').show();
                } else {
                    for (let i = 0; i < settings.nodes.length; i++) {
                        let actual_curve = $('#histo' + i.toString());
                        if (!actual_curve.is(":visible")) {
                            actual_curve.show();
                        }
                    }
                }

                //Hide br tags from histogram graphs
                $('.histo_br').show();
            } else {
                if (parseInt(edit_visualization_control_select.find(':selected').val()) > 0) {
                    $('#curve_integrated').show();
                    $('#curve_integrated_title').show();
                } else {
                    for (let i = 0; i < settings.nodes.length; i++) {
                        let actual_curve = $('#curve' + i.toString());
                        if (!actual_curve.is(":visible")) {
                            actual_curve.show();
                            $('#curve' + i.toString() + '_title').show();
                        }
                    }
                }

                //Hide br tags from curve graphs
                $('.curve_br').show();
            }

            //Show legend table
            $('#legend_table').show();
        } else {                          //Line Curve graph types: time and duration
            if (parseInt(edit_view_graph_select.find(':selected').val()) === 1) {     //Histogram
                hideLineCurveGraph();

                for (let i = 0; i < settings.nodes.length; i++) {
                    let actual_curve = $('#histo' + i.toString());
                    if (!actual_curve.is(":visible")) {
                        actual_curve.show();
                    }
                }

                //Hide br tags from histogram graphs
                $('.histo_br').show();
            } else {                          //Line Curve graph types: time and duration
                //Hide histogram Graph
                hideHistogramGraph();

                for (let i = 0; i < settings.nodes.length; i++) {
                    let actual_curve = $('#curve' + i.toString());
                    if (!actual_curve.is(":visible")) {
                        actual_curve.show();
                        $('#curve' + i.toString() + '_title').show();
                    }
                }

                //Hide br tags from curve graphs
                $('.curve_br').show();
            }
        }
    }

    function hideLineCurveGraph() {
        //Hide Dynamic line curve graphs
        for (let i = 0; i < settings.nodes.length; i++) {
            $('#curve' + i.toString()).hide();
            $('#curve' + i.toString() + '_title').hide();
        }

        //Hide comparison graph
        if (settings.nodes.length > 1) {
            $('#curve_integrated').hide();
            $('#curve_integrated_title').hide();
            edit_visualization_control_select.hide();

            //Hide legend table
            $('#legend_table').hide();
        }

        //Hide br tags from curve graphs
        $('.curve_br').hide();
    }

    function hideHistogramGraph() {
        //Hide Dynamic histograms
        for (let i = 0; i < settings.nodes.length; i++) {
            $('#histo' + i.toString()).hide();
        }

        //Hide comparison graph
        if (settings.nodes.length > 1) {
            $('#histo_integrated').hide();
            edit_visualization_control_select.hide();

            //Hide legend table
            $('#legend_table').hide();
        }

        //Hide br tags from curve graphs
        $('.histo_br').hide();
    }

    function style(feature) {
        //Define Styles
        let polygon_style = {
            fillColor: '#a9c7ff',
            weight: 0.8,
            opacity: 0.7,
            color: 'black',
            fillOpacity: 0.3
        };

        let line_style = {
            fillColor: '#a9c7ff',
            weight: widthFlow(feature.properties),
            opacity: 0.7,
            color: 'black',
            fillOpacity: 0.4
        };

        let point_style = {
            fillColor: '#a9c7ff',
            weight: 1.5,
            opacity: 0.7,
            color: 'black',
            fillOpacity: 0.4
        };

        //Get the style to be applied
        let returned_style = polygon_style;

        if (settings.geometry === 'Line') {
            returned_style = line_style;
        }

        if (settings.geometry === 'Point') {
            returned_style = point_style;
        }

        return returned_style;

        /* Sub Helper function */

        function widthFlow(properties) {
            //Calc Q_med_m3s for line weight
            //Default value
            let width_flow = 0.5;

            //Calc the width flow with and offset of 0.5, just to be always
            // visible
            if (properties.Q_med_m3s != null) {
                width_flow = Math.sqrt(parseFloat(feature.properties.Q_med_m3s)) / 14 + 0.5;
            }

            return width_flow;
        }
    }

    function style_context(feature) {
        //Define Styles
        let polygon_style = {
            fillColor: '#a9c7ff',
            weight: 0.8,
            opacity: 0.7,
            color: 'black',
            fillOpacity: 0.3
        };

        let line_style = {
            fillColor: '#a9c7ff',
            weight: widthFlow(feature.properties),
            opacity: 0.7,
            color: 'black',
            fillOpacity: 0.4
        };

        let point_style = {
            fillColor: '#a9c7ff',
            weight: 1.5,
            opacity: 0.7,
            color: 'black',
            fillOpacity: 0.4
        };

        //Get the style to be applied
        let returned_style = null;

        if (feature.geometry.type === 'LineString') {
            returned_style = line_style;
        } else if (feature.geometry.type === 'Point') {
            returned_style = point_style;
        } else { //MultiPolygon
            returned_style = polygon_style;
        }

        return returned_style;

        /* Sub Helper function */

        function widthFlow(properties) {
            //Calc Q_med_m3s for line weight
            //Default value
            let width_flow = 0.5;

            //Calc the width flow with and offset of 0.5, just to be always
            // visible
            if (properties.Q_med_m3s != null) {
                width_flow = Math.sqrt(parseFloat(feature.properties.Q_med_m3s)) / 14 + 0.5;
            }

            return width_flow;
        }
    }

    function onEachFeature(feature, layer) {
        layer.on({
            mouseover: highlightFeature,
            mouseout: resetHighlight,
            click: select_unselected,
        });
    }

    function onEachFeature_context(feature, layer) {
        //Sets Popups
        layer.bindPopup(feature.properties.nom_visual ? feature.properties.nom_visual : getElementNameString(feature.properties.Branch));


        //Sets highlight
        layer.on({
            mouseover: highlightFeature_context,
            mouseout: resetHighlight_context,
        });
    }

    function pointToLayer(feature, latlng) {
        let point_style = {
            radius: 8
        };

        return L.circleMarker(latlng, point_style);
    }

    function highlightFeature(e) {
        let layer = e.target;

        let current_style = {
            weight: 3,
            color: '#050666',
            fillOpacity: 0.6
        };

        if (settings.geometry === 'Line') {
            current_style = {
                weight: 9,
                color: '#050666',
                fillOpacity: 0.6
            }
        }
        layer.setStyle(current_style);

        if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
            layer.bringToFront();
        }

        info.update(layer.feature.properties);
    }

    function highlightFeature_context(e) {
        let layer = e.target;

        let current_style = {
            weight: 3,
            color: '#050666',
            fillOpacity: 0.6
        };

        if (layer.feature.geometry.type === 'LineString') {
            current_style = {
                weight: 9,
                color: '#050666',
                fillOpacity: 0.6
            }
        }

        if (layer.feature.geometry.type !== 'Point') {
            layer.setStyle(current_style);

            //In case of use of other browser
            if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
                layer.bringToFront();
            }
        }
    }

    function resetHighlight(e) {
        let layer = e.target;
        let properties = layer.feature.properties;
        let idx_element = -1;
        try {
            idx_element = selected_elements.map(function (e) {
                return e.id
            }).indexOf(getElementNameString(properties.GeoBranchID));
        } catch (e) {
            idx_element = -1;
        }

        if (idx_element < 0) {
            geojson.resetStyle(layer);
        }
        info.update();
    }

    function resetHighlight_context(e) {
        let layer = e.target;
        if (layer.feature.geometry.type !== 'Point') {
            layer.setStyle(style_context(layer.feature));
        }
    }

    //Var used to store the selected geo elements
    function select_unselected(e) {
        let layer = e.target;
        let properties = layer.feature.properties;

        //Select a new element or unselected an already selected one
        let idx_element = -1;
        try {
            //Search for the clicked element, return -1 if not found
            idx_element = selected_elements.map(function (e) {
                return e.id
            }).indexOf(getElementNameString(properties.GeoBranchID));
        } catch (e) {
            idx_element = -1;
            return;
        }
        if (idx_element >= 0) {
            selected_elements.splice(idx_element, 1);
            geojson.resetStyle(layer);

            //Trigger slider event to update line_curve graph
            edit_aggregation_select.trigger("change");

            return;
        }

        //Add the new element
        let new_selected_element = {
            id: getElementNameString(properties.GeoBranchID),
            properties: properties
        };
        selected_elements.push(new_selected_element);

        let current_style = {
            fillColor: '#050666',
            fillOpacity: 0.6
        };
        layer.setStyle(current_style);

        if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
            layer.bringToFront();
        }

        //Trigger slider event to update line_curve graph
        edit_aggregation_select.trigger("change");
    }

    //Split branch names by "\"
    //TODO: delete this function is no longer necessary
    function getElementNameString(v_name) {
        if (v_name == null) {
            return '';
        }
        let splited_array = v_name.split("\\");
        return splited_array[splited_array.length - 1];
    }

    //Update data from line_curve based on selected elements
    function update_line_curve_data() {
        //Get Scale selection
        let select_scale_value = edit_scale_select.find(':selected').val();

        //Generate the X array labels for "Integrated" graph
        let num_steps_comparison = undefined;
        let x_axis_object_comparison = undefined;
        if (settings.nodes.length > 1) {
            let max_year_array = [];
            let min_year_array = [];
            let max_timestep = [];
            let min_timestep = [];
            settings.data_ext.forEach(function (e_data_ext, e_index) {
                max_year_array.push(e_data_ext['max_year']);
                min_year_array.push(e_data_ext['min_year']);
                max_timestep.push(e_data_ext['max_timestep']);
                min_timestep.push(e_data_ext['min_timestep']);
            });
            let comparison_max_year = Math.max.apply(null, max_year_array);
            let comparison_min_year = Math.min.apply(null, min_year_array);
            let comparison_max_timestep = Math.max.apply(null, max_timestep);
            let comparison_min_timestep = Math.min.apply(null, min_timestep);

            num_steps_comparison = comparison_max_timestep - comparison_min_timestep + 1;
            x_axis_object_comparison = generateXAxisLabel(comparison_max_year, comparison_min_year, num_steps_comparison);

            //Set new comparison graph
            Drupal.settings.d3.inventory.curve_integrated.rows = [];
            Drupal.settings.d3.inventory.curve_integrated.rows.push(x_axis_object_comparison.x_array);
            Drupal.settings.d3.inventory.curve_integrated.y_label = y_label_curve_comparison + '  ' + scale_label_default[select_scale_value];
        }

        //Generate the X array labels and data
        settings.data_ext.forEach(function (e_data_ext, e_index) {
            let num_steps_graph = 0;
            let x_axis_object = {};
            if (settings.nodes.length > 1) {
                //Generate the X array labels for "Integrated" graph
                num_steps_graph = num_steps_comparison;
                x_axis_object = x_axis_object_comparison;
            } else {
                //Generate the X array labels for "Separated" graph
                num_steps_graph = num_steps[e_index];
                x_axis_object = generateXAxisLabel(e_data_ext['max_year'], e_data_ext['min_year'], num_steps_graph);
            }

            //Select function of aggregation
            let function_curve_aggregation = [func_default, func_maxMinAverages, func_summation,];
            let select_actual_value = edit_aggregation_select.find(':selected').val();

            //Update Graph Format details
            Drupal.settings.d3.inventory['curve' + e_index.toString()].x_label = Drupal.settings.d3.inventory['curve' + e_index.toString()].x_time_label;
            Drupal.settings.d3.inventory['curve' + e_index.toString()].x_type = Drupal.settings.d3.inventory['curve' + e_index.toString()].x_time_type;
            Drupal.settings.d3.inventory['curve' + e_index.toString()].x_count = Drupal.settings.d3.inventory['curve' + e_index.toString()].x_time_count;
            Drupal.settings.d3.inventory['curve' + e_index.toString()].x_max = undefined;
            Drupal.settings.d3.inventory['curve' + e_index.toString()].x_min = undefined;
            Drupal.settings.d3.inventory['curve' + e_index.toString()].x_values = undefined;


            //Update number of steps on dc graph and apply the correct display
            // format
            if (num_steps_graph <= 1) {
                Drupal.settings.d3.inventory['curve' + e_index.toString()].x_display_format = "%Y";
            } else {
                Drupal.settings.d3.inventory['curve' + e_index.toString()].x_display_format = Drupal.settings.d3.inventory['curve' + e_index.toString()].x_time_display_format;
            }

            //Create the Graph Data
            let v_rows = function_curve_aggregation[select_actual_value](x_axis_object.x_key_array, settings.data_keyed[e_index], e_index);

            //Set results to Drupal vars
            Drupal.settings.d3.inventory['curve' + e_index.toString()].rows = [];
            Drupal.settings.d3.inventory['curve' + e_index.toString()].rows.push(x_axis_object.x_array);
            Drupal.settings.d3.inventory['curve' + e_index.toString()].rows = v_rows.reduce(function (coll, item) {
                coll.push(item);
                return coll;
            }, Drupal.settings.d3.inventory['curve' + e_index.toString()].rows);
            Drupal.settings.d3.inventory['curve' + e_index.toString()].x_key_array = x_axis_object.x_key_array;
            Drupal.settings.d3.inventory['curve' + e_index.toString()].y_label = y_label_curve_titles[e_index] + '  ' + scale_label_default[select_scale_value];

            //Add the graph to comparison graph
            if (settings.nodes.length > 1) {
                Drupal.settings.d3.inventory.curve_integrated.rows = v_rows.reduce(function (coll, item) {
                    coll.push(item);
                    return coll;
                }, Drupal.settings.d3.inventory.curve_integrated.rows);

                //Update Graph Format details
                Drupal.settings.d3.inventory.curve_integrated.x_label = Drupal.settings.d3.inventory['curve' + e_index.toString()].x_time_label;
                Drupal.settings.d3.inventory.curve_integrated.x_type = Drupal.settings.d3.inventory['curve' + e_index.toString()].x_time_type;
                Drupal.settings.d3.inventory.curve_integrated.x_count = Drupal.settings.d3.inventory['curve' + e_index.toString()].x_time_count;
                Drupal.settings.d3.inventory.curve_integrated.x_max = undefined;
                Drupal.settings.d3.inventory.curve_integrated.x_min = undefined;
                Drupal.settings.d3.inventory.curve_integrated.x_values = undefined;

                //Update number of steps on dc graph and apply the correct
                // display format
                if (num_steps_graph <= 1) {
                    Drupal.settings.d3.inventory['curve' + e_index.toString()].x_display_format = "%Y";
                } else {
                    Drupal.settings.d3.inventory['curve' + e_index.toString()].x_display_format = Drupal.settings.d3.inventory['curve' + e_index.toString()].x_time_display_format;
                }
            }
        });
    }

    //Update data from histogram graphs based on selected elements
    function update_histogram_data() {
        //Get Scale selection
        let select_scale_value = edit_scale_select.find(':selected').val();

        //Define the X array labels for "Integrated" and normal graph, is the
        // same for separated and integrated graphs.
        let x_axis_data = ['x', 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];


        //Generate the X array labels for "Integrated" and "separated" graph
        if (settings.nodes.length > 1) {
            //Set new comparison graph
            Drupal.settings.d3.inventory.histo_integrated.rows = [];
            Drupal.settings.d3.inventory.histo_integrated.rows.push(x_axis_data);
            Drupal.settings.d3.inventory.histo_integrated.y_label = y_label_histo_comparison + '  ' + scale_label_default[select_scale_value];
        }

        //Select function of aggregation
        let function_histo_aggregation = [func_histogram_max, func_histogram_min, func_histogram_average, func_histogram_w_average];
        let select_actual_value = parseInt(edit_aggregation_select.find(':selected').val());

        //Generate the data array for each node
        settings.data_ext.forEach(function (e_data_ext, e_index) {
            //Create the Graph Data
            let v_rows = function_histo_aggregation[select_actual_value](settings.data_keyed[e_index], e_index);

            //Set results to Drupal vars
            Drupal.settings.d3.inventory['histo' + e_index.toString()].rows = [];
            Drupal.settings.d3.inventory['histo' + e_index.toString()].rows.push(x_axis_data);
            Drupal.settings.d3.inventory['histo' + e_index.toString()].rows = v_rows.reduce(function (coll, item) {
                coll.push(item);
                return coll;
            }, Drupal.settings.d3.inventory['histo' + e_index.toString()].rows);
            Drupal.settings.d3.inventory['histo' + e_index.toString()].y_label = y_label_histo_titles[e_index] + '  ' + scale_label_default[select_scale_value];

            //Add the graph to comparison graph
            if (settings.nodes.length > 1) {
                Drupal.settings.d3.inventory.histo_integrated.rows = v_rows.reduce(function (coll, item) {
                    coll.push(item);
                    return coll;
                }, Drupal.settings.d3.inventory.histo_integrated.rows);
            }
        });
    }

    //Update data from line_curve based on selected elements for duration
    function update_line_curve_duration_data() {
        //Get Scale selection
        let select_scale_value = edit_scale_select.find(':selected').val();

        //Select function of aggregation
        let function_duration_aggregation = [func_duration_default];
        let select_actual_func_value = 0;

        //Get select option
        let select_type_value = edit_aggregation_select.find(':selected').val();

        //Generate the X array labels for "Integrated" graph
        let num_steps_comparison_duration = undefined;
        let x_axis_object_comparison_duration = undefined;
        if (settings.nodes.length > 1) {
            let max_year_array = [];
            let min_year_array = [];
            let max_timestep = [];
            let min_timestep = [];
            settings.data_ext.forEach(function (e_data_ext, e_index) {
                max_year_array.push(e_data_ext['max_year']);
                min_year_array.push(e_data_ext['min_year']);
                max_timestep.push(e_data_ext['max_timestep']);
                min_timestep.push(e_data_ext['min_timestep']);
            });
            let comparison_max_year = Math.max.apply(null, max_year_array);
            let comparison_min_year = Math.min.apply(null, min_year_array);
            let comparison_max_timestep = Math.max.apply(null, max_timestep);
            let comparison_min_timestep = Math.min.apply(null, min_timestep);

            num_steps_comparison_duration = comparison_max_timestep - comparison_min_timestep + 1;
            x_axis_object_comparison_duration = generateXAxisLabel_duration(comparison_max_year, comparison_min_year, num_steps_comparison_duration, select_type_value);

            //Set new comparison graph
            Drupal.settings.d3.inventory.curve_integrated.rows = [];
            Drupal.settings.d3.inventory.curve_integrated.rows.push(x_axis_object_comparison_duration.x_array);
            Drupal.settings.d3.inventory.curve_integrated.y_label = y_label_curve_comparison + '  ' + scale_label_default[select_scale_value];
        }

        //Generate the data array for each node
        settings.data_ext.forEach(function (e_data_ext, e_index) {
            let num_steps_graph = 0;
            let x_axis_object = {};
            if (settings.nodes.length > 1) {
                //Generate the X array labels for "Integrated" graph
                num_steps_graph = num_steps_comparison_duration;
                x_axis_object = x_axis_object_comparison_duration;
            } else {
                //Generate the X array labels for "Separated" graph
                num_steps_graph = num_steps[e_index];
                x_axis_object = generateXAxisLabel_duration(e_data_ext['max_year'], e_data_ext['min_year'], num_steps_graph, select_type_value);
            }

            //Update Graph Format details
            Drupal.settings.d3.inventory['curve' + e_index.toString()].x_label = Drupal.settings.d3.inventory['curve' + e_index.toString()].x_duration_label;
            Drupal.settings.d3.inventory['curve' + e_index.toString()].x_type = Drupal.settings.d3.inventory['curve' + e_index.toString()].x_duration_type;
            Drupal.settings.d3.inventory['curve' + e_index.toString()].x_count = Drupal.settings.d3.inventory['curve' + e_index.toString()].x_duration_count;
            Drupal.settings.d3.inventory['curve' + e_index.toString()].x_display_format = Drupal.settings.d3.inventory['curve' + e_index.toString()].x_duration_display_format;
            Drupal.settings.d3.inventory['curve' + e_index.toString()].x_max = Drupal.settings.d3.inventory['curve' + e_index.toString()].x_duration_max;
            Drupal.settings.d3.inventory['curve' + e_index.toString()].x_min = Drupal.settings.d3.inventory['curve' + e_index.toString()].x_duration_min;
            Drupal.settings.d3.inventory['curve' + e_index.toString()].x_values = Drupal.settings.d3.inventory['curve' + e_index.toString()].x_duration_values;

            //Create the Graph Data
            let v_rows = function_duration_aggregation[select_actual_func_value](x_axis_object.x_key_array, settings.data_keyed[e_index], e_index, select_type_value);

            //Set results to Drupal vars
            Drupal.settings.d3.inventory['curve' + e_index.toString()].rows = [];
            Drupal.settings.d3.inventory['curve' + e_index.toString()].rows.push(x_axis_object.x_array);
            Drupal.settings.d3.inventory['curve' + e_index.toString()].rows = v_rows.reduce(function (coll, item) {
                coll.push(item);
                return coll;
            }, Drupal.settings.d3.inventory['curve' + e_index.toString()].rows);
            Drupal.settings.d3.inventory['curve' + e_index.toString()].x_key_array = x_axis_object.x_key_array;
            Drupal.settings.d3.inventory['curve' + e_index.toString()].y_label = y_label_curve_titles[e_index] + '  ' + scale_label_default[select_scale_value];

            //Add the graph to comparison graph
            if (settings.nodes.length > 1) {
                Drupal.settings.d3.inventory.curve_integrated.rows = v_rows.reduce(function (coll, item) {
                    coll.push(item);
                    return coll;
                }, Drupal.settings.d3.inventory.curve_integrated.rows);

                //Update Graph Format details
                Drupal.settings.d3.inventory.curve_integrated.x_label = Drupal.settings.d3.inventory['curve' + e_index.toString()].x_duration_label;
                Drupal.settings.d3.inventory.curve_integrated.x_type = Drupal.settings.d3.inventory['curve' + e_index.toString()].x_duration_type;
                Drupal.settings.d3.inventory.curve_integrated.x_count = Drupal.settings.d3.inventory['curve' + e_index.toString()].x_duration_count;
                Drupal.settings.d3.inventory.curve_integrated.x_display_format = Drupal.settings.d3.inventory['curve' + e_index.toString()].x_duration_display_format;
                Drupal.settings.d3.inventory.curve_integrated.x_max = Drupal.settings.d3.inventory['curve' + e_index.toString()].x_duration_max;
                Drupal.settings.d3.inventory.curve_integrated.x_min = Drupal.settings.d3.inventory['curve' + e_index.toString()].x_duration_min;
                Drupal.settings.d3.inventory.curve_integrated.x_values = Drupal.settings.d3.inventory['curve' + e_index.toString()].x_duration_values;

            }
        });
    }

    function func_duration_default(v_x_key_array, v_data_keyed, v_index, v_type) {
        //Return object
        let v_rows = [];

        // Get the actual selected scale factor
        let select_scale_value = edit_scale_select.find(':selected').val();
        if (!select_scale_value) {
            select_scale_value = 4;  //default value - x1
        }

        // Set the additional string to be added to each geo-element name if
        // comparison graph is used
        let element_added_string = settings.nodes.length > 1 ? ' - e' + v_index.toString() : '';

        //Process multi-dimensions string
        let v_dimension_select_value = null;
        if (hasMultipleDimension) {
            v_dimension_select_value = edit_aggregation_select_dimension.find(':selected').val();
        }

        //Getting "value" data
        selected_elements.forEach(function (v_element) {
            let y_values_array = [];
            v_x_key_array.forEach(function (e_value, e_index) {
                //Get month for actual item on loop
                let actual_month = parseInt(e_value.split(".")[1]);

                //Adding values
                if ((parseInt(v_type) === 0) || (actual_month === parseInt(v_type))) {
                    //Get actual value
                    let v_scalar_value = hasMultipleDimension ? JSON.parse(v_data_keyed[v_element.id][e_value])[dimensionNames[v_dimension_select_value]] : (v_data_keyed[v_element.id][e_value]);

                    //Add to values array
                    y_values_array.push(parseFloat(v_scalar_value) / scale_default[select_scale_value]);
                }
            });

            //Sort array in descending order from max to min.
            y_values_array.sort((a, b) => b - a);

            //Adding data indicator at the beginning the array
            y_values_array.unshift(getElementNameString(v_element.properties.Branch.toUpperCase()) + element_added_string);

            //Adding to return property
            v_rows.push(y_values_array);
        });

        //Return result
        return v_rows;
    }

    function func_histogram_minMaxAvg(v_data_keyed, v_index, v_type) {
        //Return container
        let v_rows = [];

        //Define math functions
        let math_function_string = ["Max", "Min", "Avg", "W. Avg"];

        // Get the actual selected scale factor
        let select_scale_value = edit_scale_select.find(':selected').val();
        if (!select_scale_value) {
            select_scale_value = 0;
        }

        // Set the additional string to be added to each geo-element name if
        // comparison graph is used
        let element_added_string = settings.nodes.length > 1 ? ' - e' + v_index.toString() : '';

        //The aggregation array
        let aggregate_value_array = [math_function_string[v_type] + element_added_string,];

        //Process multi-dimensions string
        let v_dimension_select_value = null;
        if (hasMultipleDimension) {
            v_dimension_select_value = edit_aggregation_select_dimension.find(':selected').val();
        }

        //Adds elements
        let v_values_object = {};
        let v_weighted_total = {};
        selected_elements.forEach(function (v_element) {
            //Get aggregate data
            Object.keys(v_data_keyed[v_element.id]).forEach(function (e_key) {
                //Get month for actual item on loop
                let actual_month = parseInt(e_key.split(".")[1]);

                //Define month array if not defined
                if (typeof v_values_object[actual_month] === 'undefined') {
                    v_values_object[actual_month] = [];
                }

                //Define weighted array if not defined
                if (typeof v_weighted_total[actual_month] === 'undefined') {
                    v_weighted_total[actual_month] = 0;
                }

                //Get and store value to corresponding array
                try {
                    //Get actual value
                    let v_scalar_value = hasMultipleDimension ? JSON.parse(v_data_keyed[v_element.id][e_key])[dimensionNames[v_dimension_select_value]] : (v_data_keyed[v_element.id][e_key]);

                    if (v_type === 3) { //For weighted average
                        if (settings.geometry === 'Line') {
                            //TODO: weighted average for Line and points
                        } else {
                            v_values_object[actual_month].push(parseFloat(v_scalar_value) * parseFloat(v_element.properties["Area_km2"]));
                            v_weighted_total[actual_month] += parseFloat(v_element.properties["Area_km2"]);
                        }
                    } else {
                        v_values_object[actual_month].push(v_scalar_value);
                    }
                } catch (err) {
                    //Just no consider it for calculations
                }
            });
        });

        //Make aggregate calculations and add result to returned rows
        Object.keys(v_values_object).forEach(function (e_month_key) {
            let aggregation_month_value = null;
            switch (v_type) {
                case 0:
                    aggregation_month_value = Math.max.apply(null, v_values_object[e_month_key]);
                    break;
                case 1:
                    aggregation_month_value = Math.min.apply(null, v_values_object[e_month_key]);
                    break;
                case 2:
                    aggregation_month_value = avg_array(v_values_object[e_month_key]);
                    break;
                case 3:
                    aggregation_month_value = weighted_avg_array(v_values_object[e_month_key], v_weighted_total[e_month_key]);
                    break;
                default:
                    aggregation_month_value = []
            }
            aggregate_value_array.push(aggregation_month_value / scale_default[select_scale_value]);
        });

        //Add the results to return container
        v_rows.push(aggregate_value_array);

        return v_rows;

        /* Sub Helper function */

        function avg_array(v_array) {
            let sum = 0;
            v_array.forEach(function (v_array_element) {
                sum += parseFloat(v_array_element);
            });

            return parseFloat(sum / v_array.length).toFixed(2);
        }

        function weighted_avg_array(v_array, v_sum_total) {
            let sum = 0;
            v_array.forEach(function (v_array_element) {
                sum += parseFloat(v_array_element);
            });
            return parseFloat(sum / v_sum_total).toFixed(2);
        }
    }

    function func_histogram_max(v_data_keyed, v_index) {
        //Set aggregation type 0 - max
        return func_histogram_minMaxAvg(v_data_keyed, v_index, 0);
    }

    function func_histogram_min(v_data_keyed, v_index) {
        //Set aggregation type 1 - min
        return func_histogram_minMaxAvg(v_data_keyed, v_index, 1);
    }

    function func_histogram_average(v_data_keyed, v_index) {
        //Set aggregation type 2 - avg
        return func_histogram_minMaxAvg(v_data_keyed, v_index, 2);
    }

    function func_histogram_w_average(v_data_keyed, v_index) {
        //Set aggregation type 3 - avg
        return func_histogram_minMaxAvg(v_data_keyed, v_index, 3);
    }

    //Default aggregation function, which display the value of the variable
    // over time
    function func_default(v_x_key_array, v_data_keyed, v_index) {
        //Return container
        let v_rows = [];

        // Get the actual selected scale factor
        let select_scale_value = edit_scale_select.find(':selected').val();
        if (!select_scale_value) {
            select_scale_value = 4;
        }

        //Process multi-dimensions string
        let v_dimension_select_value = null;
        if (hasMultipleDimension) {
            v_dimension_select_value = edit_aggregation_select_dimension.find(':selected').val();
        }

        // Set the additional string to be added to each geo-element name if
        // comparison graph is used
        let element_added_string = settings.nodes.length > 1 ? ' - e' + v_index.toString() : '';

        //Add selected elements to data array used by line_curve
        selected_elements.forEach(function (v_element) {
            let v_element_values = [getElementNameString(v_element.properties.Branch.toUpperCase()) + element_added_string,];
            v_x_key_array.forEach(function (v_key_time) {
                try {
                    let v_scalar_value = hasMultipleDimension ? JSON.parse(v_data_keyed[v_element.id][v_key_time])[dimensionNames[v_dimension_select_value]] : (v_data_keyed[v_element.id][v_key_time]);
                    v_element_values.push(v_scalar_value / scale_default[select_scale_value]);
                } catch (err) {
                    //If there no data is found null is set as value
                    v_element_values.push(null);
                }
            });
            v_rows.push(v_element_values);
        });

        //Return result
        return v_rows;
    }

    //Maximum Minimum, Average and Weighted average
    function func_maxMinAverages(v_x_key_array, v_data_keyed, v_index) {
        //Return container
        let v_rows = [];

        // Get the actual selected scale factor
        let select_scale_value = edit_scale_select.find(':selected').val();
        if (!select_scale_value) {
            select_scale_value = 0;
        }

        // Set the additional string to be added to each geo-element name if
        // comparison graph is used
        let element_added_string = settings.nodes.length > 1 ? ' - e' + v_index.toString() : '';

        //The four aggregation arrays
        let max_value_array = ["Max" + element_added_string,];
        let min_value_array = ["Min" + element_added_string,];
        let avg_value_array = ["Avg" + element_added_string,];
        let weighted_avg_value_array = ["W. Avg" + element_added_string,];

        //Process multi-dimensions string
        let v_dimension_select_value = null;
        if (hasMultipleDimension) {
            v_dimension_select_value = edit_aggregation_select_dimension.find(':selected').val();
        }

        //Adds elements
        v_x_key_array.forEach(function (v_key_time) {
            let v_values_array = [];
            let v_weighted_array = [];
            let v_weighted_total = 0;
            selected_elements.forEach(function (v_element) {
                try {
                    let v_scalar_value = hasMultipleDimension ? JSON.parse(v_data_keyed[v_element.id][v_key_time])[dimensionNames[v_dimension_select_value]] : (v_data_keyed[v_element.id][v_key_time]);
                    v_values_array.push(v_scalar_value);
                    if (settings.geometry === 'Line') {
                        //TODO: for Line and points
                    } else {
                        v_weighted_array.push(parseFloat(v_scalar_value) * parseFloat(v_element.properties["Area_km2"]));
                        v_weighted_total += parseFloat(v_element.properties["Area_km2"]);
                    }
                } catch (err) {
                    //Just no consider it for calculations
                }
            });

            //Make aggregate calculations
            let max_value = Math.max.apply(null, v_values_array);
            let min_value = Math.min.apply(null, v_values_array);
            let avg_value = avg_array(v_values_array);
            let weighted_avg_value = weighted_avg_array(v_weighted_array, v_weighted_total);

            //Add
            max_value_array.push((max_value) / scale_default[select_scale_value]);
            min_value_array.push((min_value) / scale_default[select_scale_value]);
            avg_value_array.push((avg_value) / scale_default[select_scale_value]);
            weighted_avg_value_array.push((weighted_avg_value) / scale_default[select_scale_value]);
        });

        //Add results to rows
        v_rows.push(max_value_array);
        v_rows.push(min_value_array);
        v_rows.push(avg_value_array);
        v_rows.push(weighted_avg_value_array);

        return v_rows;

        /* Sub Helper function */

        function avg_array(v_array) {
            let sum = 0;
            v_array.forEach(function (v_array_element) {
                sum += parseFloat(v_array_element);
            });

            return parseFloat(sum / v_array.length).toFixed(2);
        }

        function weighted_avg_array(v_array, v_sum_total) {
            let sum = 0;
            v_array.forEach(function (v_array_element) {
                sum += parseFloat(v_array_element);
            });
            return parseFloat(sum / v_sum_total).toFixed(2);
        }
    }

    function func_summation(v_x_key_array, v_data_keyed, v_index) {
        //Return container
        let v_rows = [];

        // Get the actual selected scale factor
        let select_scale_value = edit_scale_select.find(':selected').val();
        if (!select_scale_value) {
            select_scale_value = 0;
        }

        // Set the additional string to be added to each geo-element name if
        // comparison graph is used
        let element_added_string = settings.nodes.length > 1 ? ' - e' + v_index.toString() : '';

        //Define the aggregation array
        let summation_array = ["Summation" + element_added_string,];

        //Process multi-dimensions string
        let v_dimension_select_value = null;
        if (hasMultipleDimension) {
            v_dimension_select_value = edit_aggregation_select_dimension.find(':selected').val();
        }

        //Adds elements
        v_x_key_array.forEach(function (v_key_time) {
            let v_values_array = [];
            selected_elements.forEach(function (v_element) {
                try {
                    let v_scalar_value = hasMultipleDimension ? JSON.parse(v_data_keyed[v_element.id][v_key_time])[dimensionNames[v_dimension_select_value]] : (v_data_keyed[v_element.id][v_key_time]);
                    v_values_array.push(v_scalar_value);
                } catch (err) {
                    //Just no consider it for calculations
                }
            });

            //Make calculations
            let summation_value = sum_array(v_values_array);

            //Add
            summation_array.push((summation_value) / scale_default[select_scale_value]);
        });

        //Add results to rows
        v_rows.push(summation_array);

        return v_rows;

        /* Sub Helper function */

        function sum_array(v_array) {
            let sum = 0;
            v_array.forEach(function (v_array_element) {
                sum += parseFloat(v_array_element);
            });

            return parseFloat(sum).toFixed(2);
        }
    }

    //Generate the X array labels for a line curve graph
    function generateXAxisLabel(v_max_year, v_min_year, v_num_steps) {
        let x_array = ["x"];
        let x_key_array = [];
        let num_years = v_max_year - v_min_year + 1;
        let y = 1;
        //var v_num_steps = 0;
        for (let i = 0; i < num_years; i++) {
            //Control initial timestep
            y = 1;

            //Generate the X axis
            for (; y < (v_num_steps + 1); y++) {
                x_array.push((parseInt(v_min_year) + i).toString() + "-" + ("00" + y).substr(-2, 2) + "-01");
                x_key_array.push((parseInt(v_min_year) + i).toString() + '.' + y.toString());
            }
        }

        return {
            x_array: x_array,
            x_key_array: x_key_array
        }
    }

    //Generate the X array labels for a line curve graph
    function generateXAxisLabel_duration(v_max_year, v_min_year, v_num_steps, v_type) {
        let x_key_array = [];
        let num_years = v_max_year - v_min_year + 1;
        let y = 1;
        //var v_num_steps = 0;
        for (let i = 0; i < num_years; i++) {
            //Control initial timestep
            y = 1;

            //Generate the X axis
            for (; y < (v_num_steps + 1); y++) {
                let actual_month = parseInt(y);
                if ((parseInt(v_type) === 0) || (actual_month === parseInt(v_type))) {
                    x_key_array.push((parseInt(v_min_year) + i).toString() + '.' + y.toString());
                }
            }
        }

        //Generation the new "x-axis" Data
        let x_array = ["x"];
        let x_array_length = x_key_array.length;
        x_key_array.forEach(function (e_value, e_index) {
            //Calculate x-axis value for each y value element
            let x_element_value = (e_index + 1) * 100 / (x_array_length + 1);

            //Add it to the x_value array
            x_array.push(x_element_value.toFixed(2));
        });

        return {
            x_array: x_array,
            x_key_array: x_key_array
        }
    }

    //Check if a the value passed is a json string object
    function isJson(item) {
        //Check if item is a string, if not change to json string
        item = typeof item !== "string" ? JSON.stringify(item) : item;

        //Check if it is an json object
        try {
            item = JSON.parse(item);
        } catch (e) {
            return false;
        }

        //Verify that the result is a javascript object
        if (typeof item === "object" && item !== null) {
            return true;
        }

        return false;
    }
}

function setMessage(v_str_msg) {
    //Create missing properties
    if (typeof Drupal.settings.d3.inventory.notice0 == "undefined") {
        Drupal.settings.d3.inventory.notice0 = {};
        Drupal.settings.d3.inventory.notice0.rows = [];
    } else if (typeof Drupal.settings.d3.inventory.notice0.rows == "undefined") {
        Drupal.settings.d3.inventory.notice0.rows = [];
    }

    //Add the msg string to message
    Drupal.settings.d3.inventory.notice0.rows.push(v_str_msg);

    /*    let innerDivHtml = document.getElementById("msg").innerHTML;
        if(innerDivHtml !== ''){
            innerDivHtml = innerDivHtml + '<sub>, ' +v_str_msg +'</sub>';
        }else{
            innerDivHtml = '<sub>' + Drupal.t('Note') + ': '+ v_str_msg +'</sub>';
        }
        document.getElementById("msg").innerHTML = innerDivHtml;*/
}

function clearMessage() {
    document.getElementById("msg").innerHTML = '';
}
