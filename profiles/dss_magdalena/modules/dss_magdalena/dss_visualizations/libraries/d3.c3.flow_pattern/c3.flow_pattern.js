/**
 * @file
 * Javascript for [library name].
 */

(function($) {

  /**
   * Adds library to the global d3 object.
   *
   * @param select
   * @param settings
   *   Array of values passed to d3_draw.
   *   id: required. This will be needed to attach your
   *       visualization to the DOM.
   */
  Drupal.d3.flow_pattern = function (select, settings) {
    // Your custom JS.
    var chart = c3.generate({
      bindto: '#' + settings.id,
      data: {
        x: 'x',
        columns: settings.rows
      },
      grid: {
        y: {
            show:settings.ygrid_show
        }
      },
      axis: {
        y: {
            label: {
              text: settings.y_label,
              position: 'outer-middle'
            }
        },
        x: {
            label:{
              text: settings.x_label,
              position: 'outer-right'
            }
        }
      }
    });

    // SUbchart code.
    // Please set the colors of the arrows in subchart.
    var header_flow_arrow_color = settings.colors.header_flow_arrow_color;
    var hidrological_gain_arrow_color = settings.colors.hidrological_gain_arrow_color;
    var loss_gain_riverbed_arrows_color = settings.colors.loss_gain_riverbed_arrows_color;
    var derivation_arrow_color = settings.colors.derivation_arrow_color;

    var svg_width = jQuery('svg').width();
    var x_axis_width = parseInt(jQuery('rect.c3-zoom-rect')[0].attributes.width.value);

    var x_data = settings.rows[0];
    var y_data = settings.rows[1];
    var max_value = x_data[x_data.length - 1];
    var step_length = x_axis_width / max_value;
    var subchart_spacing = svg_width - x_axis_width;

    for (var i = 1; i < x_data.length; i++) {
      var x_cursor = 0;

      if (y_data[i] > y_data[i - 1] && x_data[i] == x_data[i - 1]) {
        x_cursor = (x_data[i - 1] * step_length) + subchart_spacing;
        draw_arrow_down_hydrological_gain(x_cursor - 20);
      }

      if (y_data[i] < y_data[i - 1] && x_data[i] > x_data[i - 1]) {
        repeats = ((x_data[i] - x_data[i - 1]) * step_length) / 16;
        repeats = Math.round(repeats);
        x_cursor = (x_data[i - 1] * step_length) + subchart_spacing;
        draw_loss_arrows(repeats, x_cursor);
      }

      if (y_data[i] > y_data[i - 1] && x_data[i] > x_data[i - 1]) {
        repeats = ((x_data[i] - x_data[i - 1]) * step_length) / 16;
        repeats = Math.round(repeats);
        x_cursor = (x_data[i - 1] * step_length) + subchart_spacing;
        draw_input_arrows(repeats, x_cursor);
      }

      if (y_data[i] < y_data[i - 1] && x_data[i] == x_data[i - 1]) {
        x_cursor = (x_data[i - 1] * step_length) + subchart_spacing;
        draw_arrow_down_derivation(x_cursor - 20);
      }
    }

    d3.select("#" + settings.id);
    d3.select("svg").attr("height", 420);
    d3.select("svg").append("line")
                    .attr("x1", subchart_spacing)
                    .attr("y1", 370)
                    .attr("x2", svg_width)
                    .attr("y2", 370);

    d3.select("svg").append("text")
                    .attr("class", "symbol_big")
                    .attr("x", 10)
                    .attr("y", 380)
                    .text("➡︎")
                    .style("fill", header_flow_arrow_color);

    function draw_arrow_down_hydrological_gain(x_address, color) {
      d3.select("svg").append("text")
                      .attr("class", "symbol_big")
                      .attr("x", x_address)
                      .attr("y", 365)
                      .text("⬇︎")
                      .style("fill", hidrological_gain_arrow_color);
    }

    function draw_arrow_down_derivation(x_address, color) {
      d3.select("svg").append("text")
                      .attr("class", "symbol_big")
                      .attr("x", x_address)
                      .attr("y", 400)
                      .text("⬇︎")
                      .style("fill", derivation_arrow_color);
    }

    function draw_input_arrows(repeats, x_address, color) {
      d3.select("svg").append("text")
                      .attr("class", "symbol_small")
                      .attr("x", x_address)
                      .attr("y", 365)
                      .text("⇣".repeat(repeats))
                      .style("fill", loss_gain_riverbed_arrows_color);
    }

    function draw_loss_arrows(repeats, x_address, color) {
      d3.select("svg").append("text")
                      .attr("class", "symbol_small")
                      .attr("x", x_address)
                      .attr("y", 400)
                      .text("⇣".repeat(repeats))
                      .style("fill", loss_gain_riverbed_arrows_color);
    }

    // Adds table legend.
    var table = d3.select("#" + settings.id).append("table")
                                .attr("class", "symbol_table");

    var row1 = table.append("tr");
    row1.append("th").attr("class", "symbol_big").text("➡︎")
                     .style("color", header_flow_arrow_color);
    row1.append("td").text("Caudal cabecera tramo");

    var row2 = table.append("tr");
    row2.append("th").attr("class", "symbol_big").text("⬇︎").style("color", hidrological_gain_arrow_color);
    row2.append("td").text("Aporte hidrico (superficial) concentrado (subcuenca)");

    var row3 = table.append("tr");
    var gain_loss_symbols = row3.append("th").attr("class", "symbol_small")
    gain_loss_symbols.append("sub").text("⇣⇣").style("color", loss_gain_riverbed_arrows_color);
    gain_loss_symbols.append("sup").text("⇣⇣").style("color", loss_gain_riverbed_arrows_color);
    row3.append("td").text("Perdida del cauce o aporte subcecuente");

    var row4 = table.append("tr");
    row4.append("th").attr("class", "symbol_big").text("⬇︎").style("color", derivation_arrow_color);
    row4.append("td").text("Derivacion o restitucion antropica");

  };
})(jQuery);
