/**
 * @file
 * Javascript for [library name].
 */

(function ($) {

    /**
     * Adds library to the global d3 object.
     *
     * @param select
     * @param settings
     *   Array of values passed to d3_draw.
     *   id: required. This will be needed to attach your
     *       visualization to the DOM.
     */
    Drupal.d3.line_curve = function (select, settings) {
        // Your custom JS.
        var color_patterns = ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b',
            '#e377c2', '#7f7f7f', '#bcbd22', '#17becf'];

        var chart = c3.generate({
            bindto: '#' + settings.id,
            data: {
                x: 'x',
                columns: settings.rows,
                type: 'spline'
            },
            legend: {
                position: 'center'
            },
            axis: {
                y: {
                    label: {
                        text: settings.y_label,
                        position: 'outer-middle'
                    }
                },
                x: {
                    label: {
                        text: settings.x_label,
                        position: 'outer-right'
                    },
                    type: settings.x_type,
                    tick: {
                        count: (settings.x_count !== null) ? settings.x_count : undefined,
                        format: (settings.x_display_format !== null) ? settings.x_display_format : undefined,
                        max: (settings.x_max !== null) ? settings.x_max : undefined,
                        min: (settings.x_min !== null) ? settings.x_min : undefined,
                        values: (settings.x_values !== null) ? settings.x_values : undefined
                    }
                }
            },
            color: {
                pattern: color_patterns
            },
            zoom: {
                enabled: (typeof settings.zoom !== 'undefined') ? settings.zoom : false
            },
            tooltip: {
                grouped: (typeof settings.tooltip_grouped !== 'undefined') ? settings.tooltip_grouped : true,
                contents: (typeof settings.tooltip_contents !== 'undefined') ? tooltip_func : undefined
            }
        });

        //chart.hide(['First'], {withLegend: true});

        if (settings.displayMaxMinAvg) {
            $('.c3-legend-item').click(function () {
                var item_class = $(this).text();
                var visibility = d3.select('.' + item_class + '_max').style('visibility');

                if (visibility == "visible") {
                    d3.select('.' + item_class + '_max').style('visibility', 'hidden');
                    d3.select('.' + item_class + '_avg').style('visibility', 'hidden');
                    d3.select('.' + item_class + '_min').style('visibility', 'hidden');
                } else {
                    d3.select('.' + item_class + '_max').style('visibility', 'visible');
                    d3.select('.' + item_class + '_avg').style('visibility', 'visible');
                    d3.select('.' + item_class + '_min').style('visibility', 'visible');
                }
            });

            for (i = 1; i < settings.rows.length; i++) {
                var nom = settings.rows[i][0];
                var max = d3.max(settings.rows[i].map(function (d) {
                    return +d;
                }));
                var min = d3.min(settings.rows[i].map(function (d) {
                    return +d;
                }));
                var Avg = d3.mean(settings.rows[i].map(function (d) {
                    return +d;
                }));
                chart.ygrids.add([
                    {value: max, text: 'Max. value(' + max + ')', class: nom + '_max'},
                    {value: Avg, text: 'Avg. value(' + Avg + ')', class: nom + '_avg'},
                    {value: min, text: 'Min. value(' + min + ')', class: nom + '_min'}]
                );


                d3.select('.' + nom + '_max').select('line')
                    .style('stroke', color_patterns[i - 1])
                    .style('stroke-dasharray', '1, 1')
                    .style('stroke-width', '1.5')
                    .style('stroke-linecap', 'round');
                d3.select('.' + nom + '_avg').select('line')
                    .style('stroke', color_patterns[i - 1])
                    .style('stroke-dasharray', '1, 10')
                    .style('stroke-width', '5')
                    .style('stroke-linecap', 'round');
                d3.select('.' + nom + '_min').select('line')
                    .style('stroke', color_patterns[i - 1])
                    .style('stroke-dasharray', '1, 10')
                    .style('stroke-width', '5');


                d3.select('.' + nom + '_max').style('stroke', color_patterns[i - 1]);
                d3.select('.' + nom + '_max text').style('fill', color_patterns[i - 1]);
                d3.select('.' + nom + '_avg').style('stroke', color_patterns[i - 1]);
                d3.select('.' + nom + '_avg text').style('fill', color_patterns[i - 1]);
                d3.select('.' + nom + '_min').style('stroke', color_patterns[i - 1]);
                d3.select('.' + nom + '_min text').style('fill', color_patterns[i - 1]);

            }
            ;
        }

        //JQuery

        $('#' + settings.id).append('<span><strong>Tipo de Linea: </strong></span>');
        $('#' + settings.id).append('<span id=type_line_curve class=type_line_active>Curva</span>');
        $('#' + settings.id).append('<span> | </span>');
        $('#' + settings.id).append('<span id=type_line_rect>Recta</span>');

        $('#type_line_rect').click(function () {
            chart.transform('line');
            $('.type_line_active').removeClass('type_line_active');
            $(this).addClass('type_line_active');
        });

        $('#type_line_curve').click(function () {
            chart.transform('spline');
            $('.type_line_active').removeClass('type_line_active');
            $(this).addClass('type_line_active');
        });

        //Helper function

        function tooltip_func(d, defaultTitleFormat, defaultValueFormat, color) {
            //Store calling chart reference
            var that = this;

            //Get external function - windows main javascript object from a browser
            var tooltip_external_func = getPropertyFromString(settings.tooltip_contents,window);

            //Check if calling function is defined.
            if (typeof tooltip_external_func === 'undefined') {
                return undefined;
            }

            return tooltip_external_func(d, defaultTitleFormat, defaultValueFormat, color, that, settings);

            //---------------
            //Helper function
            //---------------
            function getPropertyFromString( propertyName, object ) {
                var parts = propertyName.split( "." ),
                    length = parts.length,
                    i,
                    property = object || this;

                for ( i = 0; i < length; i++ ) {
                    property = property[parts[i]];
                }

                return property;
            }
        }

    };
})(jQuery);
