/**
 * @file
 * Javascript for [library name].
 */

(function($) {

  /**
   * Adds library to the global d3 object.
   *
   * @param select
   * @param settings
   *   Array of values passed to d3_draw.
   *   id: required. This will be needed to attach your
   *       visualization to the DOM.
   */
  Drupal.d3.balance = function (select, settings) {
    // Your custom JS.
    d3.select('#' + settings.id).append('div')
                                .attr('id', 'split1_chart');

    d3.select('#' + settings.id).append('div')
                                .attr('id', 'split2_chart');
    var data = settings.rows;
    var data2 = settings.rows2;

    chart1('#split1_chart', data);
    chart2('#split2_chart', data2);

    function chart1(div, data) {
      var order_chart = [];

      for (var i = 0; i < settings.rows.length; i++) {
        order_chart.push(settings.rows[i].INPUT);
      }

      var svg = dimple.newSvg(div, 600, 500);
      var myChart = new dimple.chart(svg, data);
      myChart.setBounds(80, 80, "70%", "70%");
      axisX = myChart.addCategoryAxis("x", ["INPUT"]);
      axisX.fontSize = "14px"
      axisX.addOrderRule(order_chart);
      axisY = myChart.addPctAxis("y", "valor");
      axisY.fontSize = "14px"
      var mySeries = myChart.addSeries("propiedad", dimple.plot.bar);
      myLegend = myChart.addLegend(65, 10, 450, 20, "right");
      myLegend.fontSize = "14px;"
      myChart.draw();

      myChart.legends = [];
      var filterValues = dimple.getUniqueValues(data, "propiedad");
      myLegend.shapes.selectAll("rect")
        // Add a click event to each rectangle.
        .on("click", function (e) {
          // This indicates whether the item is already visible or not.
          var hide = false;
          var newFilters = [];
          // If the filters contain the clicked shape hide it.
          filterValues.forEach(function (f) {
            if (f === e.aggField.slice(-1)[0]) {
              hide = true;
            }
            else {
              newFilters.push(f);
            }
          });
          // Hide the shape or show it.
          if (hide) {
            d3.select(this).style("opacity", 0.2);
          }
          else {
            newFilters.push(e.aggField.slice(-1)[0]);
            d3.select(this).style("opacity", 0.8);
          }
          // Update the filters.
          filterValues = newFilters;
          // Filter the data.
          myChart.data = dimple.filterData(data, "propiedad", filterValues);
          // Passing a duration parameter makes the chart animate. Without
          // it there is no transition.
          myChart.draw();
        });
    }

    function chart2(div, data) {
      var order_chart = [];

      for (var i = 0; i < settings.rows2.length; i++) {
        order_chart.push(settings.rows2[i].OUTPUT);
      }

      var svg = dimple.newSvg(div, 600, 500);
      var myChart = new dimple.chart(svg, data);
      myChart.setBounds(80, 80, "70%", "70%");
      axisX = myChart.addCategoryAxis("x", ["OUTPUT"]);
      axisX.fontSize = "14px"
      axisX.addOrderRule(order_chart);
      axisY = myChart.addPctAxis("y", "valor");
      axisY.fontSize = "14px"
      var mySeries = myChart.addSeries("propiedad", dimple.plot.bar);
      myLegend = myChart.addLegend(20, 10, 550, 20, "right");
      myLegend.fontSize = "14px;"
      myChart.draw();

      myChart.legends = [];
      var filterValues = dimple.getUniqueValues(data, "propiedad");
      myLegend.shapes.selectAll("rect")
        // Add a click event to each rectangle.
        .on("click", function (e) {
          // This indicates whether the item is already visible or not.
          var hide = false;
          var newFilters = [];
          // If the filters contain the clicked shape hide it.
          filterValues.forEach(function (f) {
            if (f === e.aggField.slice(-1)[0]) {
              hide = true;
            }
            else {
              newFilters.push(f);
            }
          });
          // Hide the shape or show it.
          if (hide) {
            d3.select(this).style("opacity", 0.2);
          }
          else {
            newFilters.push(e.aggField.slice(-1)[0]);
            d3.select(this).style("opacity", 0.8);
          }
          // Update the filters.
          filterValues = newFilters;
          // Filter the data.
          myChart.data = dimple.filterData(data, "propiedad", filterValues);
          // Passing a duration parameter makes the chart animate. Without
          // it there is no transition.
          myChart.draw();
        });
    }

  }

})(jQuery);
