/**
 * @file
 * Javascript for [library name].
 */

(function ($) {

    /**
     * Adds library to the global d3 object.
     *
     * @param select
     * @param settings
     *   Array of values passed to d3_draw.
     *   id: required. This will be needed to attach your
     *       visualization to the DOM.
     */
    Drupal.d3.radar = function (select, settings) {
        // Your custom JS.
        var data = [];

        for (var i = 0; i < settings.legend.length; i++) {
            var values = [];

            for (var j = 0; j < settings.rows[i].length; j++) {
                values.push({
                    axis: settings.rows[i][j][0],
                    value: settings.rows[i][j][1]
                });
            }

            data.push(
                values
                // {
                // className: settings.legend[i],
                // axes: values
                // }
            );
        }

        //Radar Chart config
        // RadarChart.defaultConfig.radius = 8;
        // RadarChart.defaultConfig.maxValue = 1;
        // RadarChart.defaultConfig.minValue = 0;
        // RadarChart.defaultConfig.levels = 4;

        //Radar Chart config
        var radar_cfg = {
            maxValue: 1,
            levels: 4,
            ExtraWidthX: 100
        };

        //Draw
        //RadarChart.draw("#" + settings.id, data);
        RadarChart.draw("#" + settings.id, data, radar_cfg);
        var container = d3.select('#' + settings.id);
        container.append('div');

        setTimeout(function () {
            for (var i = 0; i < settings.legend.length; i++) {
                var legend_color = d3.select('polygon.radar-chart-serie' + i)
                    .style('fill');
                container.append('div')
                    .attr('class', 'legend_controls')
                    .attr('data-serie', i)
                    .style('color', legend_color)
                    .text(settings.legend[i])
                    .on('click', function () {
                        var serie = d3.select(this).attr('data-serie');
                        var legend_color = d3.select('polygon.radar-chart-serie' + serie)
                            .style('fill');
                        var visibility = d3.select('polygon.radar-chart-serie' + serie)
                            .style('visibility');
                        if (visibility == "visible") {
                            d3.selectAll('.radar-chart-serie' + serie).style('visibility', 'hidden');
                            d3.select(this).style('color', 'lightgray');
                        }
                        else {
                            d3.selectAll('.radar-chart-serie' + serie).style('visibility', 'visible');
                            d3.select(this).style('color', legend_color);
                        }
                    });
            }
        }, 500)

    }

})(jQuery);
