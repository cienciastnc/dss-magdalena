/**
 * @file
 * Javascript for [library name].
 */

(function ($) {

    /**
     * Adds library to the global d3 object.
     *
     * @param select
     * @param settings
     *   Array of values passed to d3_draw.
     *   id: required. This will be needed to attach your
     *       visualization to the DOM.
     */
    Drupal.d3.choropleth_map = async function (select, settings) {

        //TODO: Hack Wait for slider, works fine but find a better way
        //Check the existence of the semaphore variable, and if it exits wait
        // for it.
        let check = true;
        if (typeof Drupal.settings.d3.inventory.sima.semaphore !== 'undefined') {
            while (check) {
                await sleep(500);
                if (Drupal.settings.d3.inventory.sima.semaphore === true) {
                    check = false;
                }
            }
        }

        function sleep(ms) {
            return new Promise(resolve => setTimeout(resolve, ms));
        }

        //Get type of "value" data to set dimensions
        let hasMultipleDimension = false;
        let dimensionNames = [];
        let f_element = Object.keys(settings.data_keyed)[0];
        let s_element = Object.keys(settings.data_keyed[f_element])[0];
        if (typeof settings.data_keyed[f_element][s_element] !== "undefined") {
            let objValue = settings.data_keyed[f_element][s_element];
            if (isJson(objValue)) {
                //set dimension flag
                hasMultipleDimension = true;

                //Set dimension Names
                dimensionNames = Object.keys(JSON.parse(objValue));
            }
        }

        //Set external Interface, TODO: Solve the id[0] to id
        this.choropleth_map[settings.id[0]] = {};
        this.choropleth_map[settings.id[0]].geojson_setStyle = updateGeojsonStyle;
        this.choropleth_map[settings.id[0]].update_legend = updateLegend;

        //Sanity check
        //Check if there is a GeoJson and validate its content.
        if (settings.geomap === null) {
            setMessage(Drupal.t('There is no GeoJson associated to this variable.'));
        }
        if (typeof settings.geomap === 'string') {
            //Convert string to a valid geojson object
            settings.geomap = JSON.parse(settings.geomap);
        }

        //Read vocabulary object
        let vocabulary_obj = {};
        try {
            vocabulary_obj = JSON.parse(settings.vocabulary);
        }
        catch (e) {
            vocabulary_obj = {};
            setMessage(Drupal.t('Using default vocabulary.'));
        }

        //Set defaults legend colors and range for vocabulary's graph
        // presentation
        let color_array_default = ['#00f', '#0f0', '#ff0', '#f00', '#00d55f', '#00cdda', '#0049e0', '#4200e6'];
        let legend_range_default = [0, 25, 50, 75];
        let legend_label_default = ['Bueno', 'Regular', 'Escaso', 'Malo'];
        let legend_scale = ['Y', 'Z', 'E', 'P', 'T', 'G', 'M', 'k', 'h', 'da', '', 'd', 'c', 'm', 'u', 'n', 'p', 'f', 'a', 'z', 'y'];
        let legend_scale_legend = ['Yotta', 'Zetta', 'Exa', 'Peta', 'Tera', 'Giga', 'Mega', 'kilo', 'hecto', 'deca', '(ninguno)', 'deci', 'centi', 'mili', 'micro', 'nano', 'pico', 'femto', 'atto', 'zepto', 'yocto'];
        let legend_scale_values = [10 ** 24, 10 ** 21, 10 * 18, 10 ** 15, 10 ** 12, 10 ** 9, 10 ** 6, 10 ** 3, 10 ** 2, 10, 1, 10 ** -1, 10 ** -2, 10 ** -3, 10 ** -6, 10 ** -9, 10 ** -12, 10 ** -15, 10 ** -18, 10 ** -21, 10 ** -24];
        let out_of_range_color = ['#848484'];
        let legend_config_default = {
            view: "range",    //Possible values: "fixed", "range"
            order: "asc",      //Possible values: "asc", "desc"
            scale: 10          //Possible values: 0 -> 'M',1 -> 'k',2 -> 'h',3
                               // -> 'da',4 -> 'x1',5 -> 'd',6 -> 'c',7 =>
                               // 'm',8 -> 'u'.
        };

        //Set legend colors and range for normal graph presentation
        if (typeof vocabulary_obj[settings.variable_machine_name] !== 'undefined') {
            settings.legend_colors = vocabulary_obj[settings.variable_machine_name].legend_colors;
            settings.legend_range = vocabulary_obj[settings.variable_machine_name].legend_range;
            settings.legend_label = (typeof vocabulary_obj[settings.variable_machine_name].legend_label !== 'undefined') ? vocabulary_obj[settings.variable_machine_name].legend_label : legend_label_default.slice();
            settings.legend_config = (typeof vocabulary_obj[settings.variable_machine_name].legend_config !== 'undefined') ? vocabulary_obj[settings.variable_machine_name].legend_config : legend_config_default.slice();
        }
        else {
            settings.legend_colors = color_array_default.slice();
            settings.legend_label = legend_label_default.slice();
            settings.legend_config = JSON.parse(JSON.stringify(legend_config_default));

            //Set legend range vocabulary automatically
            if (typeof settings.data_ext !== 'undefined') {
                //Check if it is a multidimensional var
                if (hasMultipleDimension) {
                    settings.legend_range = setLegendRange();
                }
                else {
                    settings.legend_range = setLegendRange(settings.data_ext['max_value']);
                }
                setMessage(Drupal.t('Using default autogenerated range vocabulary.'));
            }
            else {
                settings.legend_range = legend_range_default;
                setMessage(Drupal.t('Using default vocabulary.'));
            }
        }

        //Set legend colors and range for differential graph presentation
        if (typeof vocabulary_obj[settings.variable_machine_name + '_diff'] !== 'undefined') {
            settings.legend_colors_diff = vocabulary_obj[settings.variable_machine_name + '_diff'].legend_colors;
            settings.legend_range_diff = vocabulary_obj[settings.variable_machine_name + '_diff'].legend_range;
            settings.legend_label_diff = (typeof vocabulary_obj[settings.variable_machine_name + '_diff'].legend_label !== 'undefined') ? vocabulary_obj[settings.variable_machine_name + '_diff'].legend_label : legend_label_default.slice();
            settings.legend_config_diff = (typeof vocabulary_obj[settings.variable_machine_name + '_diff'].legend_config !== 'undefined') ? vocabulary_obj[settings.variable_machine_name + '_diff'].legend_config : legend_config_default.slice();
        }
        else {
            settings.legend_colors_diff = color_array_default.slice();
            settings.legend_range_diff = legend_range_default.slice();
            settings.legend_label_diff = legend_label_default.slice();
            settings.legend_config_diff = JSON.parse(JSON.stringify(legend_config_default));
        }

        //Set legend length in case color legend and range legend were different
        let legend_length = Math.min(settings.legend_colors.length, settings.legend_range.length);
        settings.legend_length = legend_length;

        //Set legend diff length in case color legend and range legend were
        // different
        let legend_length_diff = Math.min(settings.legend_colors_diff.length, settings.legend_range_diff.length);
        settings.legend_length_diff = legend_length_diff;

        //Create leaflet layers
        let color_tile = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
            subdomains: ['a', 'b', 'c']
        });

        let gray_tile = L.tileLayer.grayscale('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
            subdomains: ['a', 'b', 'c']
        });

        let empty_tile = L.tileLayer('');

        //Map Layer Control
        let baseMaps = {
            "Color": color_tile,
            "Grey": gray_tile,
            "Blank": empty_tile
        };

        //Create the leaflet map (Do not touch id[0], needs to be fixed!
        let mymap = L.map(settings.id[0], {
            layers: [color_tile],
            fullscreenControl: true,
            fullscreenControlOptions: {
                position: 'topleft'
            }
        }).setView([6.5, -75], 6);

        //Sets default leaflet icon Path
        L.Icon.Default.imagePath = '/profiles/dss_magdalena/libraries/leaflet/dist/images/';

        //Create default pane
        let defaultPane = mymap.createPane('default');
        defaultPane.style.zIndex = 410;

        //Adds the GeoJson Layer, using only the first geomap in array
        let geojson = L.geoJSON(settings.geomap, {
            pane: 'default',
            style: style,
            onEachFeature: onEachFeature,
            pointToLayer: pointToLayer
        }).addTo(mymap);

        //Adds default overlay to overlay control object
        let default_map_name = Drupal.t('Default');
        let overlays = {};
        overlays[default_map_name] = geojson;

        //Add Context layer
        let context_layer_names = settings.context_layer_machine_names;
        Object.keys(context_layer_names).forEach(function (e_key, e_index) {
            //Create a leflet pane for current processing context overlay
            let context_machine_name = context_layer_names[e_key];
            let temp_pane_name = context_machine_name + '-' + e_index.toString()
            let temp_Pane = mymap.createPane(temp_pane_name);
            temp_Pane.style.zIndex = 450;

            //Sets leaflet overlay layers and add them to overlay control object
            overlays[e_key] = L.geoJSON(null, {
                pane: temp_pane_name,
                style: style_context,
                onEachFeature: onEachFeature_context
            });
        });

        //Set the leaflet layer control
        L.control.layers(baseMaps, overlays, {position: 'bottomleft'}).addTo(mymap);

        //Control box
        let info = L.control();

        //Creates the control label
        info.onAdd = function (map) {
            this._div = L.DomUtil.create('div', 'info'); // create a div with a
                                                         // class "info"
            this.update();
            return this._div;
        };

        //Method that we will use to update the control based on feature
        // properties passed
        info.update = function (props) {
            //Get actual value for element
            let element_value = null;
            if ($('input[name=time_slider]:checked').val() === "0") {
                element_value = get_element_value(props);

                //Check if data is defined
                if (typeof element_value === "undefined") {
                    element_value = Drupal.t("Not available.");
                }
            }
            else {
                element_value = get_aggregation_object(props);

                //Check if data is defined
                if (typeof element_value === "undefined") {
                    element_value = {
                        max: Drupal.t("Not available."),
                        min: Drupal.t("Not available."),
                        avg: Drupal.t("Not available."),
                        diff: Drupal.t("Not available.")
                    };
                }
            }

            //Update control info
            let scale_multiplication_factor = legend_scale_values[settings.legend_config.scale];
            let html_string = "";
            if (props) {
                if ($('input[name=time_slider]:checked').val() === "0") {
                    html_string = Drupal.t('Element: <b> @ele_name </b><br/>Value: <b> @value</b>', {
                        '@ele_name': (props.nom_visual ? props.nom_visual.toUpperCase() : getElementNameString(props.Branch.toUpperCase())),
                        '@value': (parseFloat(element_value) / scale_multiplication_factor).toLocaleString()
                    });
                }
                else {
                    html_string = Drupal.t('Element: <b>@ele_name</b><br/>Max: <b> @max </b><br/>Min: <b> @min </b><br/>Avg: <b> @avg </b><br/>Diff: <b> @diff </b>', {
                        '@ele_name': (props.nom_visual ? props.nom_visual.toUpperCase() : getElementNameString(props.Branch.toUpperCase())),
                        '@max': (parseFloat(element_value.max) / scale_multiplication_factor).toLocaleString(),
                        '@min': (parseFloat(element_value.min) / scale_multiplication_factor).toLocaleString(),
                        '@avg': (parseFloat(element_value.avg) / scale_multiplication_factor).toLocaleString(),
                        '@diff': (parseFloat(element_value.diff) / scale_multiplication_factor).toLocaleString()
                    });
                }
            }
            else {
                html_string = '<h4>' + Drupal.t('Choose an Element') + '</h4>';
            }
            this._div.innerHTML = html_string;
        };
        info.addTo(mymap);

        //Creates the legend on the map
        let legend = L.control({position: 'bottomright'});
        legend.onAdd = function (map) {

            let div = L.DomUtil.create('div', 'info legend'),
                grades = settings.legend_range;

            //Setup for-loop
            let init_value = undefined;
            let func_inc_dec = undefined;
            let func_comp = undefined;
            if (settings.legend_config.order === 'asc') {
                init_value = 0;
                func_inc_dec = (i) => {
                    return i + 1
                };
                func_comp = (i) => {
                    return i < settings.legend_length
                };
            }
            else {
                init_value = settings.legend_length - 1;
                func_inc_dec = (i) => {
                    return i - 1
                };
                func_comp = (i) => {
                    return i >= 0
                };
            }

            //Shoe title only for differential legend
            if (settings.switch_legend_control === "1") {
                div.innerHTML += "&nbsp;<b>" + Drupal.t("Leyenda") + " " + Drupal.t("diferencial") + "</b>";
            }

            //Loop through our density intervals and generate a label with a
            // colored square for each interval let unit_text =
            // legend_scale[settings.legend_config.scale];
            let labels = settings.legend_label;
            let table_color = settings.switch_legend_control === "1" ? 'style="background-color: aliceblue;"' : '';
            let table_string = '<table class="tableLegend"' + table_color + '>';
            for (let i = init_value; func_comp(i); i = func_inc_dec(i)) {
                //Get label to be displayed
                let label_i = typeof labels[i] !== 'undefined' ? labels[i] : '';

                //Display vocabulary
                if (typeof settings.legend_config.view !== "undefined" && settings.legend_config.view === "range") {
                    table_string += '<tr><td>' +
                        '<i style="background:' + settings.legend_colors[i] + '"></i> ' +
                        grades[i] + (grades[i + 1] ? ' &ndash; ' + grades[i + 1] : ' +') + '</td><td>' + label_i + '</td></tr>';
                }
                else {
                    table_string += '<tr><td>' +
                        '<i style="background:' + settings.legend_colors[i] + '"></i> ' +
                        grades[i] + '</td><td>' + label_i + '</td></tr>';
                }
            }
            table_string += '</table>';
            div.innerHTML += table_string;

            //Show scale
            if (legend_scale_legend[settings.legend_config.scale] !== '(ninguno)') {
                div.innerHTML += "&ensp;" + Drupal.t("Escala") + ": <b>" + legend_scale_legend[settings.legend_config.scale] + "</b>";
            }

            return div;
        };
        legend.addTo(mymap);

        //Creates the choose legend on the map
        let chooseLegend = L.control({position: 'bottomright'});
        chooseLegend.onAdd = function (map) {

            let div = L.DomUtil.create('div', 'info legend'),
                grades = settings.legend_range,
                labels = [];

            // Loop through our density intervals and generate a label with a
            // colored square for each interval
            for (let i = 0; i < settings.legend_length; i++) {
                //Get label to be displayed
                let label_l = typeof settings.legend_label[i] !== 'undefined' ? settings.legend_label[i] : '';

                //Set value for the i grade
                let grades_value = grades[i];
                if (typeof grades[i] === 'undefined') {
                    grades_value = grades[grades.length - 1];
                }

                //Create the DIV legend content
                div.innerHTML +=
                    '<input type="text" id="c' + i + '" class="colorPicker" value="' + settings.legend_colors[i] + '" /> &nbsp;' +
                    '<input type="text" id="t' + i + '" class="textGUISet"  value="' + grades_value + '" />&nbsp;' +
                    '<input type="text" id="l' + i + '" class="textLabelGUISet"  value="' + label_l + '" size="20"/></br>';
            }

            //Adds the size buttons
            div.innerHTML += Drupal.t("Size") + '&nbsp;&nbsp; <button type="button" id="addButton" class="legendButton">+</button><button type="button" id="delButton" class="legendButton">-</button></br>';

            //Adds the view radio buttons
            div.innerHTML += Drupal.t("Vista") + ': ' + '<label><input type="radio" id="view0" name="view_radio" value="range" class="" ' + (settings.legend_config.view === 'range' ? 'checked' : '') + '>&nbsp;' + Drupal.t("Rango") + '</label>' +
                '&nbsp;<label><input type="radio" id="view1" name="view_radio" value="fixed" class="" ' + (settings.legend_config.view === 'fixed' ? 'checked' : '') + '>&nbsp;' + Drupal.t("Fijo") + '</label></br>';

            //Adds the order radio buttons
            div.innerHTML += Drupal.t("Orden Visual") + ': ' + '<label><input type="radio" id="order0" name="order_radio" value="asc" class="" ' + (settings.legend_config.order === 'asc' ? 'checked' : '') + '>&nbsp;' + Drupal.t("Ascendente") + '</label>' +
                '&nbsp;<label><input type="radio" id="order1" name="order_radio" value="desc" class="" ' + (settings.legend_config.order === 'desc' ? 'checked' : '') + '>&nbsp;' + Drupal.t("Descendente") + '</label></br>';

            //Interchange labels
            div.innerHTML += Drupal.t("Orden") + '&nbsp;&nbsp; <button type="button" id="invertButton" class="invertLegend">' + Drupal.t("Cambiar") + '</button></br>';

            //Adds the scale select box
            let select_string = Drupal.t("Escala") + ': ' + '<select id="scale_select_id" name="scale_select">';
            for (let i = 0; i < legend_scale_legend.length; i++) {
                select_string += '<option value="' + i + '" label="' + legend_scale_legend[i] + '" ' + (settings.legend_config.scale === i ? 'selected' : '') + '></option>';
            }
            select_string += '</select>';
            div.innerHTML += select_string;

            return div;
        };
        //chooseLegend.addTo(mymap);

        //Set colot picker element
        document.getElementById(settings.id[0] + "_cpicker").innerHTML = '<sub>' + Drupal.t('Change Legend') + '</sub>';


        /** JQuery **/

        //Define jQuery vars
        let edit_aggregation_select_dimension = $("#edit-aggregation-select-dimension");
        let cpicker_div = $('#' + settings.id[0] + '_cpicker');
        let cpicker_class = $(".colorPicker");

        //Handle Spectrum color picker
        cpicker_class.spectrum({
            change: function (color) {
                //console.log(color);
            }
        });

        //Handle Spectrum color picker selection
        let toggle_val = 0;
        if (cpicker_div.is(":visible")) {
            //New toggle way for jQuery 1.10
            cpicker_div.click(function(){
                if(toggle_val === 0){
                    selectableLegend();
                    toggle_val = 1;
                }else{
                    fixedLegend();
                    toggle_val = 0;
                }
            });
        }

        /** Load Context GeoJson **/

        //Getting Context geojson layer
        let context_layer_obj = {};
        let ajax_results = [];
        Object.keys(context_layer_names).forEach(function (e_key, e_index) {
            //Check if data is cached
            let context_machine_name = context_layer_names[e_key];
            if (localStorage.getItem('context-layer-' + context_machine_name) !== null) {
                //Get decompressed object
                let obj_string = LZString.decompressFromUTF16(localStorage.getItem('context-layer-' + context_machine_name));
                let obj_data = JSON.parse(obj_string);
                context_layer_obj[e_key] = JSON.parse(obj_data.layer);
            }
            else {
                let data_url2 = Drupal.settings.basePath + 'layers/' + context_machine_name + ".geojson";
                ajax_results.push($.getJSON(data_url2, function (json) {
                    //Getting data
                    context_layer_obj[e_key] = JSON.parse(json.layer);

                    //Convert o string the object to be stored
                    let string_obj_to_storage = JSON.stringify(json);

                    //Compress and Store object to cache
                    try {
                        localStorage.setItem('context-layer-' + context_machine_name, LZString.compressToUTF16(string_obj_to_storage));
                    }
                    catch (e) {
                        localStorage.removeItem('context-layer-' + context_machine_name);
                        setMessage(Drupal.t("Not enough cache to storage on web browser for context layer."));
                    }
                }));
            }
        });

        //Update message
        Drupal.d3.page_notice();

        //Runs after ajax loaded the context layers
        $.when.apply(this, ajax_results).done(function () {
            //Adds leaflet layer data for each context
            Object.keys(overlays).forEach(function (e_key, e_index) {
                //Sanity check
                if (typeof overlays[e_key] === 'undefined') {
                    return;
                }

                //Exclude main overlay
                if (e_key === default_map_name) {
                    return;
                }

                //Adds layer data
                overlays[e_key].addData(context_layer_obj[e_key]);
            });
        });


        /** Helper functions **/

        //Set color for the graph
        function getColor(d) {
            for (let i = (settings.legend_length - 1); i >= 0; i--) {
                if (d >= (settings.legend_range[i] * legend_scale_values[settings.legend_config.scale])) {
                    return settings.legend_colors[i];
                }
            }

            //This in case there are values less than the minimum, Huh??
            return out_of_range_color[0];
        }

        function style(feature) {
            //Define Styles for no data
            let no_data_style = {
                fillColor: 'gray',
                weight: 0.8,
                opacity: 0.7,
                color: 'black',
                fillOpacity: 0.8
            };

            //Get actual value for element
            let element_value = get_element_value(feature.properties);
            if (typeof element_value === "undefined") {
                //return no_data_style;
                element_value = 0;
            }

            //Define Styles
            let polygon_style = {
                fillColor: getColor(element_value),
                weight: 0.8,
                opacity: 0.7,
                color: 'black',
                fillOpacity: 0.4
            };

            let line_style = {
                fillColor: getColor(element_value),
                weight: widthFlow(feature.properties),
                opacity: 0.7,
                color: getColor(element_value),
                fillOpacity: 0.4
            };

            let point_style = {
                fillColor: getColor(element_value),
                weight: 1.5,
                opacity: 0.7,
                color: 'black',
                fillOpacity: 0.4
            };

            //Get the style to be applied
            let returned_style = polygon_style;

            if (settings.geometry === 'Line') {
                returned_style = line_style;
            }

            if (settings.geometry === 'Point') {
                returned_style = point_style;
            }

            return returned_style;

            /* Sub Helper function */

            function widthFlow(properties) {
                //Calc Q_med_m3s for line weight
                //Default value
                let width_flow = 0.5;

                //Calc the width flow with and offset of 0.5, just to be always
                // visible
                if (properties.Q_med_m3s != null) {
                    width_flow = Math.sqrt(parseFloat(feature.properties.Q_med_m3s)) / 14 + 0.5;
                }

                return width_flow;
            }
        }

        function style_context(feature) {
            //Define Styles
            let polygon_style = {
                fillColor: '#a9c7ff',
                weight: 0.8,
                opacity: 0.7,
                color: 'black',
                fillOpacity: 0.3
            };

            let line_style = {
                fillColor: '#a9c7ff',
                weight: widthFlow(feature.properties),
                opacity: 0.7,
                color: 'black',
                fillOpacity: 0.4
            };

            let point_style = {
                fillColor: '#a9c7ff',
                weight: 1.5,
                opacity: 0.7,
                color: 'black',
                fillOpacity: 0.4
            };

            //Get the style to be applied
            let returned_style = null;

            if (feature.geometry.type === 'LineString') {
                returned_style = line_style;
            }
            else if (feature.geometry.type === 'Point') {
                returned_style = point_style;
            }
            else { //MultiPolygon
                returned_style = polygon_style;
            }

            return returned_style;

            /* Sub Helper function */

            function widthFlow(properties) {
                //Calc Q_med_m3s for line weight
                //Default value
                let width_flow = 0.5;

                //Calc the width flow with and offset of 0.5, just to be always
                // visible
                if (properties.Q_med_m3s != null) {
                    width_flow = Math.sqrt(parseFloat(feature.properties.Q_med_m3s)) / 14 + 0.5;
                }

                return width_flow;
            }
        }

        function onEachFeature(feature, layer) {
            layer.on({
                mouseover: highlightFeature,
                mouseout: resetHighlight
            });
        }

        function onEachFeature_context(feature, layer) {
            //Sets Popups
            layer.bindPopup(feature.properties.nom_visual ? feature.properties.nom_visual : getElementNameString(feature.properties.Branch));

            //Sets highlight
            layer.on({
                mouseover: highlightFeature_context,
                mouseout: resetHighlight_context,
            });
        }

        function pointToLayer(feature, latlng) {
            let point_style = {
                radius: 8
            };

            return L.circleMarker(latlng, point_style);
        }

        function highlightFeature(e) {
            let layer = e.target;

            let current_style = {
                weight: 3,
                color: '#050666',
                fillOpacity: 0.5
            };

            if (settings.geometry === 'Line') {
                current_style = {
                    weight: 9,
                    color: '#050666',
                    fillOpacity: 0.5
                }
            }
            layer.setStyle(current_style);

            if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
                layer.bringToFront();
            }

            info.update(layer.feature.properties);
        }

        function highlightFeature_context(e) {
            let layer = e.target;

            let current_style = {
                weight: 3,
                color: '#050666',
                fillOpacity: 0.6
            };

            if (layer.feature.geometry.type === 'LineString') {
                current_style = {
                    weight: 9,
                    color: '#050666',
                    fillOpacity: 0.6
                }
            }

            if (layer.feature.geometry.type !== 'Point') {
                layer.setStyle(current_style);

                //In case of use of other browser
                if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
                    layer.bringToFront();
                }
            }
        }

        function resetHighlight(e) {
            let layer = e.target;
            geojson.resetStyle(layer);
            info.update();
        }

        function resetHighlight_context(e) {
            let layer = e.target;
            if (layer.feature.geometry.type !== 'Point') {
                layer.setStyle(style_context(layer.feature));
            }
        }

        //Split branch names by "\"
        function getElementNameString(v_name) {
            if (v_name == null) {
                return '';
            }
            let splited_array = v_name.split("\\");
            return splited_array[splited_array.length - 1];
        }

        function get_element_value(properties) {
            //Process multi-dimensions string
            let v_dimension_select_value = null;
            if (hasMultipleDimension) {
                v_dimension_select_value = $("#edit-aggregation-select-dimension").find(':selected').val();
            }

            let element_value = 0;
            if ($('input[name=time_slider]:checked').val() === "0") {
                try {
                    element_value = hasMultipleDimension ? JSON.parse(settings.data_keyed[getElementNameString(properties.GeoBranchID)][settings.actual_time])[dimensionNames[v_dimension_select_value]] : (settings.data_keyed[getElementNameString(properties.GeoBranchID)][settings.actual_time]);
                }
                catch (err) {
                    element_value = undefined;
                }
            }
            else {
                let select_elements = ["max", "min", "avg", "diff"];
                let selected_aggregation = select_elements[parseInt($("#edit-aggregation-select").val())]
                try {
                    element_value = settings.data_aggregation[getElementNameString(properties.GeoBranchID)][selected_aggregation];
                }
                catch (err) {
                    element_value = undefined;
                }
            }

            return element_value;
        }

        function get_aggregation_object(properties) {
            let element_value = 0;
            try {
                element_value = settings.data_aggregation[getElementNameString(properties.GeoBranchID)];
            }
            catch (err) {
                element_value = undefined;
            }

            return element_value;
        }

        function setLegendRange(v_max, v_min, v_step) {
            //Set default parameter, step is seven as default
            if (typeof v_min === 'undefined') {
                v_min = 0;
            }
            if (typeof v_step === 'undefined') {
                v_step = 3;
            }

            //If v_max is not defined, calc max value instead
            if (typeof v_max === 'undefined') {
                let default_selected_dimension = "0";
                let string_key = (hasMultipleDimension) ? dimensionNames[default_selected_dimension] : "";
                v_max = getMax(settings.data_keyed, string_key);
            }

            //Calc step
            let step = (parseFloat(v_max) - parseFloat(v_min)) / parseFloat(v_step);
            let step_exp = Math.pow(10, Math.trunc(Math.log10(step)));
            let real_step = Math.trunc(step / step_exp) * step_exp;
            let long_range = range(v_min, v_max, real_step);
            let final_range = [0];
            let max_num = long_range.length - 1;
            if (long_range.length === (v_step + 1)) {
                max_num += 1;
            }
            let init_loop = max_num - v_step;
            //Check sanity
            if (init_loop < 0) {
                init_loop = 0;
            }
            for (let i = init_loop; i < max_num; i++) {
                final_range.push(long_range[i]);
            }

            return final_range;

            /* Sub Helper function */

            function range(start, stop, step) {
                if (typeof stop === 'undefined') {
                    // one param defined
                    stop = start;
                    start = 0;
                }

                if (typeof step === 'undefined') {
                    step = 1;
                }

                if ((step > 0 && start >= stop) || (step < 0 && start <= stop)) {
                    return [];
                }

                let result = [];
                for (let i = start; step > 0 ? i < stop : i > stop; i += step) {
                    result.push(i);
                }

                return result;
            }

            function getMax(data_keyed_array, string_key) {
                let max_result = 0;

                //Process all data to get the Max value
                Object.keys(data_keyed_array).forEach(function (e_key, e_index) {
                    Object.keys(data_keyed_array[e_key]).forEach(function (e_key_i, e_index_i) {
                        // Get value from data
                        let actual_data = data_keyed_array[e_key][e_key_i];
                        let actual_data_value_string = (hasMultipleDimension) ? JSON.parse(actual_data)[string_key] : actual_data;
                        let actual_data_value = parseFloat(actual_data_value_string);

                        // Get max value
                        if (actual_data_value > max_result) {
                            max_result = actual_data_value;
                        }
                    });
                });

                return max_result;
            }
        }

        function fixedLegend() {
            //Update colors
            updateColorPickerJquery();

            //Set legend
            mymap.removeControl(chooseLegend);
            legend.addTo(mymap);

            //Update graph
            if ($('input[name=time_slider]:checked').val() === "0") {
                if (settings.graph_temporality_type === 3) {
                    //Update choroplet style for none temporality
                    updateGeojsonStyle();
                }
                else {
                    $("#ex1").trigger("change");
                }
            }
            else {
                $("#ex2").trigger("slideStop");
            }

            //Set color picker element
            document.getElementById(settings.id[0] + '_cpicker').innerHTML = '<sub>' + Drupal.t("Change Legend") + '</sub>';

            //Update color visualization profile
            let $string_vocabulary = JSON.stringify(vocabulary_obj);
            $.post('/visualization/set_profile', {visualization_profile: $string_vocabulary});

            //Call the callback function, if defined
            if (settings.updateStyleCallBack !== null) {
                settings.updateStyleCallBack(settings.id[0]);
            }
        }

        function selectableLegend() {
            //Set legend
            mymap.removeControl(legend);
            chooseLegend.addTo(mymap);

            updateLegendControls();

            //Set color picker element
            document.getElementById(settings.id[0] + '_cpicker').innerHTML = '<sub>' + Drupal.t("Update Legend") + '</sub>';

            /* Sub Helper functions */

            function updateLegendControls() {
                //Activate Spectrum JQuery
                $(".colorPicker").spectrum({
                    showInitial: true,
                    showPalette: true,
                    palette: [
                        ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)", "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)"],
                        ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)", "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
                        ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)", "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)"],
                        ["rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)", "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)"],
                        ["rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)", "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)"],
                        ["rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)", "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)"],
                        ["rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)", "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
                    ],
                });

                //Activate Legend Size buttons
                $("#addButton").click(incrementLegend);
                $("#delButton").click(decrementLegend);

                //Activate Legend Size buttons
                $('#invertButton').click(invertLabelLegend);
            }

            function incrementLegend() {
                //Update legend length
                settings.legend_length += 1;

                //Update legend
                mymap.removeControl(chooseLegend);
                chooseLegend.addTo(mymap);

                updateLegendControls();
            }

            function decrementLegend() {
                if (settings.legend_length <= 1) {
                    return;
                }

                //Update legend length
                settings.legend_length -= 1;

                //Update legend
                mymap.removeControl(chooseLegend);
                chooseLegend.addTo(mymap);

                updateLegendControls();
            }

            function invertLabelLegend() {
                //Update legend color and label directly from DOM
                for (let i = 0; i < Math.floor(settings.legend_length / 2); i++) {
                    //Get elements from DOM
                    let actualColorElement = $("#c" + i.toString());
                    let invertColorElement = $("#c" + (settings.legend_length - 1 - i).toString());
                    let actualLabelElement = $("#l" + i.toString());
                    let invertLabelElement = $("#l" + (settings.legend_length - 1 - i).toString());

                    //Get actual values
                    let actualColor = '#' + actualColorElement.spectrum("get").toHex();
                    let invertColor = '#' + invertColorElement.spectrum("get").toHex();
                    let actualLabel = actualLabelElement.val();
                    let invertLabel = invertLabelElement.val();

                    //Set new inverted values
                    actualColorElement.spectrum("set", invertColor);
                    invertColorElement.spectrum("set", actualColor);
                    actualLabelElement.val(invertLabel);
                    invertLabelElement.val(actualLabel);
                }
            }
        }

        function updateColorPickerJquery() {
            //Update legend color array
            for (let i = 0; i < settings.legend_length; i++) {
                settings.legend_colors[i] = '#' + $("#c" + i.toString()).spectrum("get").toHex();

                //Set value from legend input text
                let text_value = $("#t" + i.toString()).val();
                let numValue = text_value.indexOf(".") === -1 ? parseInt(text_value) : parseFloat(text_value);
                settings.legend_range[i] = isNaN(numValue) ? settings.legend_range[i] : numValue;

                //Set label from legend input text
                settings.legend_label[i] = $("#l" + i.toString()).val();
            }

            //Update real size of array
            settings.legend_colors.length = settings.legend_length;
            settings.legend_range.length = settings.legend_length;
            settings.legend_label.length = settings.legend_length;

            //Update vocabulary config
            settings.legend_config = {};
            settings.legend_config.view = $('input[name=view_radio]:checked').val();
            settings.legend_config.order = $('input[name=order_radio]:checked').val();
            settings.legend_config.scale = parseInt($("#scale_select_id").find(':selected').val());

            //Set Diff vocabulary
            let machine_name_modification = '';
            if (settings.switch_legend_control === "1") {
                machine_name_modification = '_diff';

                //Update diff vocabulary
                settings.legend_colors_diff = settings.legend_colors;
                settings.legend_range_diff = settings.legend_range;
                settings.legend_label_diff = settings.legend_label;
                settings.legend_config_diff = settings.legend_config;
            }

            //Update color profile object
            if (!vocabulary_obj.hasOwnProperty(settings.variable_machine_name + machine_name_modification)) {
                vocabulary_obj[settings.variable_machine_name + machine_name_modification] = {};
            }
            vocabulary_obj[settings.variable_machine_name + machine_name_modification].legend_colors = settings.legend_colors;
            vocabulary_obj[settings.variable_machine_name + machine_name_modification].legend_range = settings.legend_range;
            vocabulary_obj[settings.variable_machine_name + machine_name_modification].legend_label = settings.legend_label;
            vocabulary_obj[settings.variable_machine_name + machine_name_modification].legend_config = settings.legend_config;
        }

        function updateGeojsonStyle() {
            geojson.setStyle(style);
        }

        function updateLegend() {
            //Set legend
            mymap.removeControl(legend);
            legend.addTo(mymap);
        }

        //Check if a the value passed is a json string object
        function isJson(item) {
            //Check if item is a string, if not change to json string
            item = typeof item !== "string" ? JSON.stringify(item) : item;

            //Check if it is an json object
            try {
                item = JSON.parse(item);
            }
            catch (e) {
                return false;
            }

            //Verify that the result is a javascript object
            if (typeof item === "object" && item !== null) {
                return true;
            }

            return false;
        }

        //To check
        function setMessage(v_str_msg) {
            //Create missing properties
            if (typeof Drupal.settings.d3.inventory.notice0 == "undefined") {
                Drupal.settings.d3.inventory.notice0 = {};
                Drupal.settings.d3.inventory.notice0.rows = [];
            }
            else if (typeof Drupal.settings.d3.inventory.notice0.rows == "undefined") {
                Drupal.settings.d3.inventory.notice0.rows = [];
            }

            //Add the msg string to message
            Drupal.settings.d3.inventory.notice0.rows.push(v_str_msg);

            /*//Check existence
            let innerDivHtml_element = document.getElementById("msg");
            if(innerDivHtml_element != null){
                let innerDivHtml = document.getElementById("msg").innerHTML;
                if(innerDivHtml !== ''){
                    innerDivHtml = innerDivHtml + '<sub>, ' +v_str_msg +'</sub>';
                }else{
                    innerDivHtml = '<sub>' + Drupal.t('Note') + ': '+ v_str_msg +'</sub>';
                }
                document.getElementById("msg").innerHTML = innerDivHtml;
            }*/
        }

        function clearMessage() {
            document.getElementById("msg").innerHTML = '';
        }
    }
})(jQuery);
