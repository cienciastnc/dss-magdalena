/**
 * @file
 * Javascript for [library name].
 */

(function ($) {

    /**
     * Adds library to the global d3 object.
     *
     * @param select
     * @param settings
     *   Array of values passed to d3_draw.
     *   id: required. This will be needed to attach your
     *       visualization to the DOM.
     */
    Drupal.d3.area_stacked = function (select, settings) {
        //Set Color patterns
        let color_patterns = ['#7c9db4', '#ffcfa4', '#71a671', '#db6a6a', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf'];
        let month_names = [Drupal.t('January'), Drupal.t('February'), Drupal.t('March'), Drupal.t('April'), Drupal.t('May'), Drupal.t('June'), Drupal.t('July'), Drupal.t('August'), Drupal.t('September'), Drupal.t('October'), Drupal.t('November'), Drupal.t('December')];
        let head_names = [Drupal.t('Duration'), Drupal.t('Type')].concat(month_names);

        //Check data passed to the graph
        if((typeof settings.def_indicator === 'undefined') || (settings.def_indicator.length === 0)){
            //Adds Table error table
            let error_table = d3.select("#" + settings.id)
                .append("table")
                .attr("class", "eloha_table");
            let error_row = error_table.append("thead").append("tr")
                .style("background-color", color_patterns[0])
                .style("opacity", 1);
            error_row.append("td").attr("width", "20%").style("text-align", "center").text(Drupal.t("Notice"));
            error_row.append("td").attr("width", "80%").style("text-align", "center").text(Drupal.t("No data avaliable."));

            return;
        }

        //Create Stacked Graph
        if(settings.show_stacked){
            //Pre-process data on chart
            let def_key_array = [];
            let def_type_array = {};
            settings.def_indicator.forEach(function(elem,index){
                def_key_array.push(elem[0]);
                def_type_array[elem[0]] = 'area';
            });

            //Graph Stacked Area
            let chart = c3.generate({
                bindto: '#' + settings.id,
                data: {
                    columns: settings.def_indicator,
                    types: def_type_array,
                    order: null,
                    groups: [def_key_array],
                },
                color: {
                    pattern: color_patterns
                },
                axis: {
                    x: {
                        type: 'category',
                        categories: month_names,
                    },
                    y: {
                        label: {
                            text: 'm3/s',
                            position: 'outer-left'
                        }
                    }
                },
                grid: {
                    y: {
                        show: true
                    }
                },
            });
        }

        // Assign color to circle indicator on each td in table ELOHA.
        function assignIndColor(value) {
            if (value >= 0 && value < 1) {
                return settings.colors.good
            }
            else if (value >= 1 && value < 2) {
                return settings.colors.medium
            }
            else if (value >= 2) {
                return settings.colors.bad
            }
            else {
                return settings.colors.nodata
            }
        }

        //Eloha Indicator and Index header
        d3.select("#" + settings.id)
            .append("h3")
            .style("text-align", "left")
            .text(Drupal.t("Indicators and Indexes"));

        // Generate ELOHa table.
        let table = d3.select("#" + settings.id)
            .append("table")
            .attr("class", "eloha_table");

        //Adds table head
        let thead = table.append("thead").append("tr");
        head_names.forEach(function(elem,index){
            thead.append("td").style("text-align", "center").text(elem);
        });

        //Adds Table body
        let tbody = table.append("tbody");
        settings.def_indicator.forEach(function(def_elem,def_index){
            let row1 = tbody.append("tr")
                .attr("class", "row-" + def_index.toString())
                .style("background-color", color_patterns[def_index])
                .style("opacity", 1);

            //Flow
            row1.append("td").attr("rowspan", "2").style("text-align", "center").text(settings.flow_name[def_index]);

            //def
            row1.append("td").style("text-align", "right").text(Drupal.t('Deficit'));
            month_names.forEach(function(elem,index){
                let td = row1.append("td").style("text-align", "right");
                let svg_ind = td.append("svg").attr("class", "ind")
                    .attr("width", 20)
                    .attr("height", 20)
                    .attr("title", "Index: " + settings.def_index[def_index][index+1]);
                let ind_color = assignIndColor(settings.def_index[def_index][index+1]);
                svg_ind.append("circle").attr("cx", 10)
                    .attr("cy", 15)
                    .attr("r", 5)
                    .style("stroke", ind_color)
                    .style("fill", ind_color);
                td.append("span").text(def_elem[index+1]);
            });

            //sur
            let row2 = tbody.append("tr")
                .attr("class", "row-" + def_index.toString())
                .style("background-color", color_patterns[def_index])
                .style("opacity", 1);
            row2.append("td").style("text-align", "right").text(Drupal.t('Excess'));
            month_names.forEach(function(elem,index){
                let td = row2.append("td").style("text-align", "right");
                let svg_ind = td.append("svg").attr("class", "ind")
                    .attr("width", 20)
                    .attr("height", 20)
                    .attr("title", "Index: " + settings.sur_index[def_index][index+1]);
                let ind_color = assignIndColor(settings.sur_index[def_index][index+1]);
                svg_ind.append("circle").attr("cx", 10)
                    .attr("cy", 15)
                    .attr("r", 5)
                    .style("stroke", ind_color)
                    .style("fill", ind_color);
                td.append("span").text(settings.sur_indicator[def_index][index+1]);
            });
        });

        //Verbal Impact header title
        d3.select("#" + settings.id)
            .append("h3")
            .style("text-align", "left")
            .text(Drupal.t("Verbal Impacts"));

        //Adds Table body for verbal impacts
        let verbal_table = d3.select("#" + settings.id)
            .append("table")
            .attr("class", "eloha_tooltip");

        //Adds table head
        let verbal_thead = verbal_table.append("thead").append("tr")
            .style("background-color", color_patterns[0])
            .style("opacity", 1);
        verbal_thead.append("td").text('');
        month_names.forEach(function(elem,index){
            verbal_thead.append("td").style("text-align", "center").text(elem);
        });

        //tbody
        let verbal_tbody = verbal_table.append("tbody");
        settings.impact_strings.forEach(function(b_elem,b_index){
            let row1 = verbal_tbody.append("tr")
                .style("opacity", 1);

            row1.append("td").text(settings.impact_name[b_index]);

            month_names.forEach(function(elem,index){
                let test = row1.append("td");
                let test_2 = test.append("label").attr('title', settings.impact_strings[b_index][index+1]);
                if((typeof settings.impact_strings[b_index][index+1] !== 'undefined') && (settings.impact_strings[b_index][index+1] !== '')){
                    //Sets color por high and medium alteration
                    if(b_index == 0){
                        test.style("background-color", settings.impact_color.bad);
                    }else{
                        test.style("background-color", settings.impact_color.medium);
                    }

                    //Set text label to be displayed
                    test_2.text(Drupal.t('Impact'));
                }else{
                    test_2.text('');
                }
            });
        });

        //Adds Table body for overall verdict
        let verdict_table = d3.select("#" + settings.id)
            .append("table")
            .attr("class", "eloha_table");
        let thead_overall = verdict_table.append("thead").append("tr")
            .style("background-color", color_patterns[0])
            .style("opacity", 1);
        thead_overall.append("td").attr("width", "20%").style("text-align", "center").text(Drupal.t("Overall alteration"));
        thead_overall.append("td").attr("width", "80%").style("text-align", "center").text(settings.overall);

        /** JQuery **/

        //Enable tooltips for indicator an index table
        $(".eloha_table").tooltip();

        //Enable tooltips for verdict table, enable line breaks.
        $(".eloha_tooltip").tooltip({
            content: function() {
                return $(this).attr('title');
                }
        });
    }
})(jQuery);
