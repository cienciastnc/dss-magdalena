/**
 * @file
 * Javascript for [library name].
 */

(function($) {

  /**
   * Adds library to the global d3 object.
   *
   * @param select
   * @param settings
   *   Array of values passed to d3_draw.
   *   id: required. This will be needed to attach your
   *       visualization to the DOM.
   */
  Drupal.d3.histogram = function (select, settings) {
    // Your custom JS.
      var chart = c3.generate({
        bindto: '#' + settings.id,
        data: {
          x: 'x',
          columns: settings.rows,
          type: 'bar',
        },
        grid: {
          y: {
            show: settings.ygrid_show
          }
        },
        legend: {
          position: 'center'
        },
        axis: {
          y: {
            label: {
              text: settings.y_label,
              position: 'outer-middle'
              }
          },
          x: {
            label:{
              text: settings.x_label,
              position: 'outer-right'
            },
            tick: {
                fit: (typeof settings.x_fit !== 'undefined') ? settings.x_fit : true,
            }
          }
        },
        bar: {
           width: {
               ratio: 0.5
           }
        }
      });

  $('#' + settings.id).prepend('<h1>' + settings.title + '</h1>').addClass('histogram_title');
  }
})(jQuery);
