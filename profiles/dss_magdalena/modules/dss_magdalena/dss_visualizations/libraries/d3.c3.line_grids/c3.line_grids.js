/**
 * @file
 * Javascript for [library name].
 */

(function($) {

  /**
   * Adds library to the global d3 object.
   *
   * @param select
   * @param settings
   *   Array of values passed to d3_draw.
   *   id: required. This will be needed to attach your
   *       visualization to the DOM.
   */
  Drupal.d3.line_grids = function (select, settings) {
    // Your custom JS.
    var chart = c3.generate({
      bindto: '#' + settings.id,
      data: {
        columns: settings.rows
      }
    });
  }

})(jQuery);
