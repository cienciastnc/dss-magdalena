<?php
/**
 * @file
 * Provides theme functions.
 */

/**
 * Implements hook_theme().
 */
function dss_visualizations_theme() {
  // Register Themes.
  $themes = array(
    'nvd3js' => array(
      'variables' => array(),
      'template' => 'nvd3js',
      'path' => drupal_get_path('module', 'dss_visualizations') . '/templates',
    ),
    'dc_leaflet' => array(
      'variables' => array(),
      'template' => 'dc_leaflet',
      'path' => drupal_get_path('module', 'dss_visualizations') . '/templates',
    ),
    'dc_leaflet_choropleth' => array(
      'variables' => array(),
      'template' => 'dc_leaflet_choropleth',
      'path' => drupal_get_path('module', 'dss_visualizations') . '/templates',
    ),
    'c3_line_curve' => array(
      'variables' => array(),
      'template' => 'c3_line_curve',
      'path' => drupal_get_path('module', 'dss_visualizations') . '/templates',
    ),
    'c3_multi_objectives' => array(
      'variables' => array(),
      'template' => 'c3_multi_objectives',
      'path' => drupal_get_path('module', 'dss_visualizations') . '/templates',
    ),
    'visualization_control' => array(
      'variables' => array(),
      'template' => 'visualization_control',
      'path' => drupal_get_path('module', 'dss_visualizations') . '/templates',
    ),
    'page_notice' => array(
      'variables' => array(),
      'template' => 'page_notice',
      'path' => drupal_get_path('module', 'dss_visualizations') . '/templates',
    ),
  );
  return $themes;
}

/**
 * Implements hook_preprocess_nvd3js() for nvd3js.tpl.php.
 *
 * Implements hook_preprocess_HOOK()
 */
function dss_visualizations_preprocess_nvd3js(&$variables) {
  // Let drupal_attributes render this.
  $variables['classes_array'][] = $variables['type'];
  $variables['attributes_array']['id'] = array($variables['vis_id']);

  $library = isset($variables['library']) ? $variables['library'] : FALSE;

  // Let's look for a template file in the library definition.
  if (isset($library['template'])) {
    $variables['theme_hook_suggestion'] = isset($library['library name']) ? $library['library name'] : str_replace('.', '_', $library['machine name']);
  }
}

/**
 * Implements hook_preprocess_dc_leaflet() for dc_leaflet.tpl.php.
 *
 * Implements hook_preprocess_HOOK()
 */
function dss_visualizations_preprocess_dc_leaflet(&$variables) {
  // Let drupal_attributes render this.
  $variables['classes_array'][] = $variables['type'];
  $variables['attributes_array']['id'] = $variables['vis_id'];
  $variables['title_attributes_array'][] = $variables['title'];

  $library = isset($variables['library']) ? $variables['library'] : FALSE;

  // Let's look for a template file in the library definition.
  if (isset($library['template'])) {
    $variables['theme_hook_suggestion'] = isset($library['library name']) ? $library['library name'] : str_replace('.', '_', $library['machine name']);
  }
}

/**
 * Implements hook_preprocess_dc_leaflet() for dc_leaflet.tpl.php.
 *
 * Implements hook_preprocess_HOOK()
 */
function dss_visualizations_preprocess_dc_leaflet_choropleth(&$variables) {
  // Let drupal_attributes render this.
  $variables['classes_array'][] = $variables['type'];
  $variables['attributes_array']['id'] = $variables['vis_id'];

  $library = isset($variables['library']) ? $variables['library'] : FALSE;

  // Let's look for a template file in the library definition.
  if (isset($library['template'])) {
    $variables['theme_hook_suggestion'] = isset($library['library name']) ? $library['library name'] : str_replace('.', '_', $library['machine name']);
  }
}

/**
 * Implements hook_preprocess_nvd3js() for nvd3js.tpl.php.
 *
 * Implements hook_preprocess_HOOK()
 */
function dss_visualizations_preprocess_c3_line_curve(&$variables) {
  // Let drupal_attributes render this.
  $variables['classes_array'][] = $variables['type'];
  $variables['attributes_array']['id'] = array($variables['vis_id']);

  $library = isset($variables['library']) ? $variables['library'] : FALSE;

  // Let's look for a template file in the library definition.
  if (isset($library['template'])) {
    $variables['theme_hook_suggestion'] = isset($library['library name']) ? $library['library name'] : str_replace('.', '_', $library['machine name']);
  }
}

/**
 * Implements hook_preprocess_visualizations_controls().
 *
 * For visualizations_controls.tpl.php.
 *
 * Implements hook_preprocess_HOOK()
 */
function dss_visualizations_preprocess_visualization_control(&$variables) {
  // Let drupal_attributes render this.
  $variables['classes_array'][] = $variables['type'];

  $library = isset($variables['library']) ? $variables['library'] : FALSE;

  // Let's look for a template file in the library definition.
  if (isset($library['template'])) {
    $variables['theme_hook_suggestion'] = isset($library['library name']) ? $library['library name'] : str_replace('.', '_', $library['machine name']);
  }
}
