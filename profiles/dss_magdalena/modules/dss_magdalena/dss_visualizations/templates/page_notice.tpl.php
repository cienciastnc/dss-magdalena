<?php
/**
 * @file
 * Default theme file for message control.
 */
?>

<div id="msg0" class="message">
  <?php
  if (is_array($rows) && count($rows) > 0) {
    echo "<span>" . t("Note") . ": </span>";
    echo "<ul>";
    foreach ($rows as $line) {
      echo "<li>" . $line . "</li>";
    }
    echo "</ul>";
  }
  ?>
</div>
