<?php
/**
 * @file
 * Default theme file for dc_leaflet js library visualizations.
 */
?>
<div class="multiobjectives-headers">
  <?php print isset($chart_form) ? $chart_form : ''; ?>
</div>
<div id="<?php print $vis_id; ?>"></div>
<div id="msg" class="message"></div>
