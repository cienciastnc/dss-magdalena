<?php
/**
 * @file
 * Default theme file for dc_leaflet js library visualizations.
 */
?>
<?php foreach ($attributes_array['id'] as $v_id){ ?>
    <div id="<?php print $v_id . '_title'; ?>" class="dc_leaflet_title"><?php print $title ?></div>
    <div id="<?php print $v_id ?>" class="<?php print $classes ?>"></div>
    <div id="<?php print $v_id . '_cpicker'; ?>" class="cpicker"></div>
    <div id="<?php print $v_id . '_msg'; ?>" class="message"></div>
<?php } ?>
