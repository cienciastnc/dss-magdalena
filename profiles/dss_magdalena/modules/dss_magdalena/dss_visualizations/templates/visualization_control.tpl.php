<?php
/**
 * @file
 * Default theme file for visualizations control.
 */
?>
<div <?php print isset($class_name) ? "class=\"{$class_name}\"" : ''; ?>>
  <?php print isset($form) ? $form : ''; ?>
</div>
