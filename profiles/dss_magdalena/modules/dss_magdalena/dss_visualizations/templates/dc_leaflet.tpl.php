<?php
/**
 * @file
 * Default theme file for dc_leaflet js library visualizations.
 */
 ?>
<div class="leaflet-title">
    <h3><?php print $title ?></h3>
</div>
<div class="leaflet-headers">
  <?php print isset($leaflet_form) ? $leaflet_form : ''; ?>
</div>
<?php foreach ($attributes_array['id'] as $v_id){ ?>
        <div id="<?php print $v_id ?>" class="<?php print $classes ?>"></div>
<?php } ?>
<div id="<?php print $v_id . '_cpicker'; ?>" class="cpicker"></div>
<div id="msg" class="message"></div>
