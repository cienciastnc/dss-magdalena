<?php
/**
 * @file
 * Default theme file for nvd3 visualizations.
 */
 ?>
<div <?php print $attributes ?> class="<?php print implode(' ', $classes_array); ?>">
 <svg style="height: 400px;"></svg>
</div>
