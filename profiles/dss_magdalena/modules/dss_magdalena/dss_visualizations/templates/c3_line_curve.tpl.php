<?php
/**
 * @file
 * Default theme file for c3 line curve js library visualizations.
 */
?>
<div id="<?php print $vis_id . '_title'; ?>" class="c3_line_curve_title"><?php print $title ?></div>
<div <?php print $attributes ?> class="<?php print implode(' ', $classes_array); ?>"></div>
