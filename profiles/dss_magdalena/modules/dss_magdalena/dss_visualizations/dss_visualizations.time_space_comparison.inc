<?php
/**
 * @file
 * Implements Visualizations for Time-Space Comparison.
 */

use Drupal\dss_magdalena\DSS\Entity\SimaResource;

module_load_include('inc', 'dss_visualizations', 'dss_visualizations.time_space_forms_utilities');


/**
 * Provides resource visualizations for Time/Space aggregations comparison.
 *
 * @param string $nodes
 *   The Resource node object.
 * @param string $visual_type
 *   The visualization graph type: time-aggregation-comparison,
 *   space-aggregation-comparison.
 *
 * @return string
 *   The output visualization.
 *
 * @throws \Exception
 *   If a field cannot be obtained, it will raise an exception.
 */
function dss_visualizations_aggregation_comparison($nodes, $visual_type) {
  // Get arrays of nodes to be compared.
  $node_array = array_map('intval', explode(',', $nodes));

  // Load the Resources.
  $resource_array = array();
  foreach ($node_array as $e_node) {
    $resource = SimaResource::load($e_node);

    // Check if the resources exist.
    if (!$resource) {
      drupal_set_message(t('You are trying to access a Resource that does not exist from node (%nid).', array('%nid' => $e_node)), 'error');
      drupal_goto('dataset');
      return '';
    }

    // Add it to resource array.
    array_push($resource_array, $resource);
  }

  // Check that the resources has been imported into the DataStore.
  foreach ($resource_array as $e_resource) {
    if (!$e_resource->isDataStoreImported()) {
      $nid = $e_resource->getId();
      drupal_set_message(t('The Resource with nid=%nid has not been imported to the DataStore yet. Please import it.', array('%nid' => $nid)), 'warning');
      drupal_goto("node/{$nid}/datastore");
      return '';
    }
  }

  // Dataset must exist.
  $dataset_array = array();
  foreach ($resource_array as $e_resource) {
    $dataset = $e_resource->getDataset();
    if (!$dataset) {
      $nid = $e_resource->getId();
      drupal_set_message(t('You are trying to access a Resource (%nid) that is not linked to a Dataset (Variable).', array('%rid' => $nid)), 'error');
      drupal_goto('dataset');
      return '';
    }

    // Add it to dataset array.
    array_push($dataset_array, $dataset);
  }

  // Call visual aggregation comparison.
  switch ($visual_type) {
    case "space-comparison":
      return dss_visualization_space_aggregation_comparison($resource_array, $dataset_array);

    case "time-comparison":
      return dss_visualization_time_aggregation_comparison($resource_array, $dataset_array);

    default:
      drupal_set_message(t('You are trying to access a visualization type that does not exists.'), 'error');
      drupal_goto('dataset');
  }
}

/**
 * Page callback for space aggregation comparison.
 *
 * @param array $resource_array
 *   The Entity Resource array.
 * @param array $dataset_array
 *   The Entity Dataset array.
 *
 * @return string
 *   The page output.
 *
 * @throws \Exception
 *   If a field cannot be obtained, it will raise an exception.
 */
function dss_visualization_time_aggregation_comparison($resource_array, $dataset_array) {
  // Get first resource and dataset.
  $resource = $resource_array[0];
  $dataset = $dataset_array[0];

  // Collecting data.
  $title = $resource->getTitle();
  $layer = $resource->getLayer();
  $data_dimension = $resource->getDataDimension();
  $layer_machine_name = $layer ? $layer->getMachineName() : NULL;
  $variable_units = $dataset->getUnitOfMeasure();
  $variable_range = $dataset->getValueRange();
  $variable_name = $dataset->getTitle();
  $variable_temporality = $dataset->getTemporality();
  $variable_geometry = $dataset->getGeometry();
  $variable_weap_level = $dataset->getWeapLevel();
  $variable_machine_name = $dataset->getMachineName();
  $context_layer_machine_name = $dataset->getContextMachineNames();
  $data_dimension_labels = $dataset->getDimensionLabels();

  // Set form.
  $map_form = drupal_get_form('dss_visualizations_map_form', $data_dimension_labels, $variable_machine_name);
  $leaflet_form = drupal_render($map_form);

  // Get users's vocabulary settings.
  global $user;
  $vocabulary = dss_get_visualization_profile($user);

  // Set visualization control form.
  $vis_form = drupal_get_form('dss_visualizations_visualization_control_form', $data_dimension);
  $vis_ctrl_form = drupal_render($vis_form);

  // Set id names for dynamic tables defined below.
  $dina_label = 'dina_label_0';
  $dina_table = 'dina_table_0';

  // Data Array.
  $chart = array(
    // First id must be called "sima".
    'id' => array('sima'),
    'type' => 'map_aggregation',
    'title' => $variable_units ? "{$title} [{$variable_units}][$variable_range]" : "{$title}",
    'variable' => $variable_name,
    'unit' => $variable_units,
    'temporality' => $variable_temporality,
    'geometry' => $variable_geometry,
    'leaflet_form' => $leaflet_form,
    'layer_machine_name' => $layer_machine_name,
    'variable_machine_name' => $variable_machine_name,
    'vocabulary' => $vocabulary,
    'weap_level' => $variable_weap_level,
    'data_dimension' => $data_dimension,
    'context_layer_machine_names' => $context_layer_machine_name,
    'dina_label' => $dina_label,
    'dina_table' => $dina_table,
  );

  // Curves.
  $chart_curve = array(
    'id' => 'curve',
    'type' => 'line_curve',
    'title' => '',
    'x_label' => 'Time',
    'x_type' => 'timeseries',
    'x_count' => 0,
    'x_format' => '%Y-%m-%d',
    'x_display_format' => '%Y-%m',
    'y_label' => $variable_units ? "{$variable_name} [{$variable_units}]" : "{$variable_name}",
    'displayMaxMinAvg' => FALSE,
    'num_id' => 0,
    'rows' => array(),
    'zoom' => TRUE,
    'x_key_array' => array(),
    // 'tooltip_grouped' => intval($variable_weap_level) > 2 ? FALSE : TRUE,
    // 'tooltip_contents' => 'Drupal.d3.tooltip_contents',.
    /* Time Graph Settings */
    'x_time_label' => 'Time',
    'x_time_type' => 'timeseries',
    'x_time_count' => 0,
    'x_time_display_format' => '%Y-%m',
    /* Duration Graph Settings */
    'x_duration_label' => '%',
    'x_duration_type' => 'indexed',
    'x_duration_count' => 0,
    'x_duration_display_format' => NULL,
    'x_duration_max' => 100,
    'x_duration_min' => 0,
    'x_duration_values' => [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100],
  );

  // Histogram.
  $chart_histogram = array(
    'id' => 'histo',
    'type' => 'histogram',
    'title' => $resource->getCaseStudy() ? $resource->getCaseStudy()->getTitle() : t('No Case Study'),
    'x_label' => 'Month',
    'x_fit' => FALSE,
    'ygrid_show' => 'true',
    'y_label' => $variable_units ? "{$variable_name} [{$variable_units}]" : "{$variable_name}",
    'num_id' => 0,
    'rows' => array(),
  );

  // Add visualization control.
  $vis_control_data = array(
    'id' => 'vis_control',
    'type' => 'visualization_control',
    'class_name' => 'visual-control',
    'form' => $vis_ctrl_form,
  );

  // Notices.
  $chart_notices = array(
    'id' => 'notice0',
    'type' => 'page_notice',
    'rows' => array(),
  );

  // Adds the Header table, the map graph and the control block form.
  $output = '<label id="' . $dina_label . '">&bull;&nbsp;' . $variable_name . "<small>&ensp;<font color=\"blue\">(view/hide)</font></small></label>";
  $output .= dss_visualizations_dynamic_table($resource, $dataset, $dina_table);
  $output .= dss_visualizations_draw($chart);
  $output .= theme('visualization_control', $vis_control_data);

  // Add the integrated line_curve.
  $chart_curve['id'] = 'curve_integrated';
  $chart_curve['title'] = 'Comparison Graph';
  $output .= dss_visualizations_draw($chart_curve);
  $output .= "<br/>";

  // Add the integrated line_curve.
  $chart_histogram['id'] = 'histo_integrated';
  $chart_histogram['title'] = 'Histogram Graph';
  $output .= dss_visualizations_draw($chart_histogram);

  // Define legend header.
  $legend_data = [[t('Legend'), '']];

  // Adding line curve the dynamic output.
  for ($i = 0; $i < count($resource_array); $i++) {
    // Adding line curve graphs.
    $chart_curve['id'] = 'curve' . $i;
    $chart_curve['num_id'] = $i;
    $chart_curve['title'] = ($resource_array[$i]->getCaseStudy() ? $resource_array[$i]->getCaseStudy()->getTitle() : t('No Case Study')) . ' - ' . $resource_array[$i]->getTitle();

    // Render dynamic output.
    if ($i != 0) {
      $output .= "<br class='curve_br'/><br class='curve_br'/>";
    }
    $output .= dss_visualizations_draw($chart_curve);
  }

  // Adding histogram the dynamic output.
  for ($i = 0; $i < count($resource_array); $i++) {
    // Adding line curve graphs.
    $chart_histogram['id'] = 'histo' . $i;
    $chart_histogram['num_id'] = $i;
    $chart_histogram['title'] = ($resource_array[$i]->getCaseStudy() ? $resource_array[$i]->getCaseStudy()->getTitle() : t('No Case Study')) . ' - ' . $resource_array[$i]->getTitle();

    // Render dynamic output.
    if ($i != 0) {
      $output .= "<br class='histo_br'/><br class='histo_br'/>";
    }
    $output .= dss_visualizations_draw($chart_histogram);
  }

  // Adding Legend output.
  for ($i = 0; $i < count($resource_array); $i++) {
    // Add legend data.
    array_push($legend_data, ['e' . $i, ($resource_array[$i]->getCaseStudy() ? $resource_array[$i]->getCaseStudy()->getTitle() : t('No Case Study')) . ' - ' . $resource_array[$i]->getTitle()]);
  }

  $output .= "<br/><br/>";
  $output .= dss_visualizations_table_legend($legend_data, 'legend_table');
  $output .= dss_visualizations_draw($chart_notices);

  return $output;
}

/**
 * Page callback for space aggregation comparison.
 *
 * @param array $resource_array
 *   The Entity Resource array.
 * @param array $dataset_array
 *   The Entity Dataset array.
 *
 * @return string
 *   The page output.
 *
 * @throws \Exception
 *   If a field cannot be obtained, it will raise an exception.
 */
function dss_visualization_space_aggregation_comparison($resource_array, $dataset_array) {
  // Get first resource and dataset.
  $resource = $resource_array[0];
  $dataset = $dataset_array[0];

  // Collecting data.
  $title = $resource->getTitle();
  $layer = $resource->getLayer();
  $data_dimension = $resource->getDataDimension();
  $layer_machine_name = $layer ? $layer->getMachineName() : NULL;
  $variable_units = $dataset->getUnitOfMeasure();
  $variable_range = $dataset->getValueRange();
  $variable_name = $dataset->getTitle();
  $variable_temporality = $dataset->getTemporality();
  $variable_geometry = $dataset->getGeometry();
  $variable_dimensions = $dataset->getDimensions();
  $variable_weap_level = $dataset->getWeapLevel();
  $variable_machine_name = $dataset->getMachineName();
  $context_layer_machine_name = $dataset->getContextMachineNames();
  $data_dimension_labels = $dataset->getDimensionLabels();

  // Set form.
  $time_slider_form = drupal_get_form('dss_visualizations_time_slider_form', $data_dimension_labels);
  $leaflet_form = drupal_render($time_slider_form);

  // Get users's vocabulary settings.
  global $user;
  $vocabulary = dss_get_visualization_profile($user);

  // Set id names for dynamic tables defined below.
  $dina_label = 'dina_label_0';
  $dina_table = 'dina_table_0';

  // Data Array.
  $chart = array(
    // First id must be called "sima".
    'id' => array('sima', 'dblpointbar'),
    'type' => 'time_aggregation',
    'title' => $variable_units ? "{$title} [{$variable_units}][$variable_range]" : "{$title}",
    'variable' => $variable_name,
    'unit' => $variable_units,
    'temporality' => $variable_temporality,
    'geometry' => $variable_geometry,
    'leaflet_form' => $leaflet_form,
    'layer_machine_name' => $layer_machine_name,
    'variable_machine_name' => $variable_machine_name,
    'dimensions' => $variable_dimensions,
    'weap_level' => $variable_weap_level,
    'data_dimension' => $data_dimension,
    'dina_label' => $dina_label,
    'dina_table' => $dina_table,
  );

  // Choropleth map.
  $chart_choropleth = array(
    'id' => array('leaflet_chart0'),
    'type' => 'choropleth_map',
    'title' => '',
    'geometry' => $variable_geometry,
    'vocabulary' => $vocabulary,
    'variable_machine_name' => $variable_machine_name,
    'weap_level' => $variable_weap_level,
    // 'geomap' => NULL,
    // 'data_keyed' => NULL,
    // 'data_ext' => NULL,
    // 'element_key_array' => NULL,.
    'context_layer_machine_names' => $context_layer_machine_name,
  // Normal: 0, Diff: 1 vocabulary.
    'switch_legend_control' => "0",
  );

  // Notices.
  $chart_notices = array(
    'id' => 'notice0',
    'type' => 'page_notice',
    'rows' => array(),
  );

  $output = '<label id="' . $dina_label . '">&bull;&nbsp;' . $variable_name . "<small>&ensp;<font color=\"blue\">(view/hide)</font></small></label>";
  $output .= dss_visualizations_dynamic_table($resource, $dataset, $dina_table);
  $output .= dss_visualizations_draw($chart);
  for ($i = 0; $i < count($resource_array); $i++) {
    $chart_choropleth['id'] = array('leaflet_chart' . $i);
    $chart_choropleth['title'] = ($resource_array[$i]->getCaseStudy() ? $resource_array[$i]->getCaseStudy()->getTitle() : t('No Case Study')) . ' - ' . $resource_array[$i]->getTitle();
    // $output .= "</br>";.
    $output .= dss_visualizations_draw($chart_choropleth);
  }
  $output .= dss_visualizations_draw($chart_notices);

  return $output;
}
