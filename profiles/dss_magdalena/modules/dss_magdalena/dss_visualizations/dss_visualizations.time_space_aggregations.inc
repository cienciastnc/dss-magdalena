<?php
/**
 * @file
 * Implements Visualizations for Time-Space Aggregations.
 */

use Drupal\dss_magdalena\DSS\Entity\SimaResource;
use Drupal\dss_magdalena\DSS\Entity\SimaDataset;

module_load_include('inc', 'dss_visualizations', 'dss_visualizations.time_space_forms_utilities');


/**
 * Provides resource visualizations for Time/Space aggregations.
 *
 * @param int $node
 *   The Resource node object.
 * @param string $visual_type
 *   The visualization graph type: time-aggregation, space-aggregation.
 *
 * @return string
 *   The output visualization.
 *
 * @throws \Exception
 *   If a field cannot be obtained, it will raise an exception.
 */
function dss_visualizations_aggregation($node, $visual_type) {
  // Load the Resource.
  $resource = SimaResource::load($node);
  $nid = $resource->getId();
  if (!$resource) {
    drupal_set_message(t('You are trying to access a Resource (%rid) that does not exist.', array(
      '%rid' => $nid,
    )), 'error');
    drupal_goto('dataset');
  }

  // Check that the resource has been imported into the DataStore.
  if (!$resource->isDataStoreImported()) {
    drupal_set_message(t('The Resource with nid=%rid has not been imported to the DataStore yet. Please import it.', array(
      '%rid' => $nid,
    )), 'warning');
    // drupal_goto("node/{$nid}/datastore");.
  }

  // Dataset must exist.
  $dataset = $resource->getDataset();
  if (!$dataset) {
    drupal_set_message(t('You are trying to access a Resource (%rid) that is not linked to a Dataset (Variable).', array(
      '%rid' => $nid,
    )), 'error');
    drupal_goto('dataset');
  }

  // Dataset must not have temporality = NONE.
  $temporality = $dataset->getTemporalityEntity();
  if ($visual_type == 'time-aggregation' && $temporality->isTemporalityNone()) {
    drupal_set_message(t('This Resource (%rid) has Temporality = "None", thus cannot have a Time Graph.', array(
      '%rid' => $nid,
    )), 'error');
    drupal_goto("node/{$nid}/space-aggregation");
  }

  // Call visual aggregation.
  switch ($visual_type) {
    case "space-aggregation":
      return dss_visualization_space_aggregation($resource, $dataset);

    case "time-aggregation":
      return dss_visualization_time_aggregation($resource, $dataset);

    default:
      drupal_set_message(t('You are trying to access a visualization type that does not exists.'), 'error');
      drupal_goto('dataset');
  }
}

/**
 * Page callback for time aggregation.
 *
 * @param \Drupal\dss_magdalena\DSS\Entity\SimaResource $resource
 *   The Entity Resource.
 * @param \Drupal\dss_magdalena\DSS\Entity\SimaDataset $dataset
 *   The Entity Dataset.
 *
 * @return string
 *   The page output.
 *
 * @throws \Exception
 *   If a field cannot be obtained, it will raise an exception.
 */
function dss_visualization_time_aggregation(SimaResource $resource, SimaDataset $dataset) {
  // Collecting data.
  $title = $resource->getTitle();
  $layer = $resource->getLayer();
  $data_dimension = $resource->getDataDimension();
  $layer_machine_name = $layer ? $layer->getMachineName() : NULL;
  $variable_units = $dataset->getUnitOfMeasure();
  $variable_range = $dataset->getValueRange();
  $variable_name = $dataset->getTitle();
  $variable_temporality = $dataset->getTemporality();
  $variable_geometry = $dataset->getGeometry();
  $variable_weap_level = $dataset->getWeapLevel();
  $variable_machine_name = $dataset->getMachineName();
  $context_layer_machine_name = $dataset->getContextMachineNames();
  $data_dimension_labels = $dataset->getDimensionLabels();

  // Set form.
  $map_form = drupal_get_form('dss_visualizations_map_form', $data_dimension_labels, $variable_machine_name);
  $leaflet_form = drupal_render($map_form);

  // Get users's vocabulary settings.
  global $user;
  $vocabulary = dss_get_visualization_profile($user);

  // Set id names for dynamic tables defined below.
  $dina_label = 'dina_label_0';
  $dina_table = 'dina_table_0';

  // Map, Data Array.
  $chart = array(
    // First id must be called "sima".
    'id' => array('sima'),
    'type' => 'map_aggregation',
    'title' => $variable_units ? "{$title} [{$variable_units}][$variable_range]" : "{$title}",
    'variable' => $variable_name,
    'unit' => $variable_units,
    'range' => $variable_range,
    'temporality' => $variable_temporality,
    'geometry' => $variable_geometry,
    'leaflet_form' => $leaflet_form,
    'layer_machine_name' => $layer_machine_name,
    'variable_machine_name' => $variable_machine_name,
    'vocabulary' => $vocabulary,
    'weap_level' => $variable_weap_level,
    'data_dimension' => $data_dimension,
    'context_layer_machine_names' => $context_layer_machine_name,
    'dina_label' => $dina_label,
    'dina_table' => $dina_table,
  );

  // Curves.
  $chart_curve = array(
    'id' => 'curve0',
    'type' => 'line_curve',
    'title' => $resource->getCaseStudy() ? $resource->getCaseStudy()->getTitle() : t('No Case Study'),
    'x_label' => 'Time',
    'x_type' => 'timeseries',
    'x_count' => 0,
    'x_format' => '%Y-%m-%d',
    'x_display_format' => '%Y-%m',
    'y_label' => $variable_units ? "{$variable_name} [{$variable_units}]" : "{$variable_name}",
    'displayMaxMinAvg' => FALSE,
    'num_id' => 0,
    'rows' => array(),
    'zoom' => TRUE,
    // 'tooltip_grouped' => intval($variable_weap_level) > 2 ? FALSE : TRUE,
    // 'tooltip_contents' => 'Drupal.d3.tooltip_contents',.
    'x_key_array' => array(),
    /* Time Graph Settings */
    'x_time_label' => 'Time',
    'x_time_type' => 'timeseries',
    'x_time_count' => 0,
    'x_time_display_format' => '%Y-%m',
    /* Duration Graph Settings */
    'x_duration_label' => '%',
    'x_duration_type' => 'indexed',
    'x_duration_count' => 0,
    'x_duration_display_format' => NULL,
    'x_duration_max' => 100,
    'x_duration_min' => 0,
    'x_duration_values' => [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100],
  );

  // Histogram.
  $chart_histogram = array(
    'id' => 'histo0',
    'type' => 'histogram',
    'title' => $resource->getCaseStudy() ? $resource->getCaseStudy()->getTitle() : t('No Case Study'),
    'x_label' => 'Month',
    'x_fit' => FALSE,
    'ygrid_show' => 'true',
    'y_label' => $variable_units ? "{$variable_name} [{$variable_units}]" : "{$variable_name}",
    'num_id' => 0,
    'rows' => array(),
  );

  // Notices.
  $chart_notices = array(
    'id' => 'notice0',
    'type' => 'page_notice',
    'rows' => array(),
  );

  $output = "</br>";
  $output .= '<label id="' . $dina_label . '">&bull;&nbsp;' . $variable_name . "<small>&ensp;<font color=\"blue\">(view/hide)</font></small></label>";
  $output .= dss_visualizations_dynamic_table($resource, $dataset, $dina_table);
  $output .= dss_visualizations_draw($chart);
  $output .= "</br>";
  $output .= dss_visualizations_draw($chart_curve);
  $output .= dss_visualizations_draw($chart_histogram);
  $output .= dss_visualizations_draw($chart_notices);
  return $output;
}

/**
 * Page callback for space aggregation.
 *
 * @param \Drupal\dss_magdalena\DSS\Entity\SimaResource $resource
 *   The Entity Resource.
 * @param \Drupal\dss_magdalena\DSS\Entity\SimaDataset $dataset
 *   The Entity Dataset.
 *
 * @return string
 *   The page output.
 *
 * @throws \Exception
 *   If a field cannot be obtained, it will raise an exception.
 */
function dss_visualization_space_aggregation(SimaResource $resource, SimaDataset $dataset) {
  // Collecting data.
  $title = $resource->getTitle();
  $layer = $resource->getLayer();
  $data_dimension = $resource->getDataDimension();
  $layer_machine_name = $layer ? $layer->getMachineName() : NULL;
  $variable_units = $dataset->getUnitOfMeasure();
  $variable_range = $dataset->getValueRange();
  $variable_name = $dataset->getTitle();
  $variable_temporality = $dataset->getTemporality();
  $variable_geometry = $dataset->getGeometry();
  $variable_dimensions = $dataset->getDimensions();
  $variable_weap_level = $dataset->getWeapLevel();
  $variable_machine_name = $dataset->getMachineName();
  $context_layer_machine_name = $dataset->getContextMachineNames();
  $data_dimension_labels = $dataset->getDimensionLabels();

  // Set form.
  $time_slider_form = drupal_get_form('dss_visualizations_time_slider_form', $data_dimension_labels);
  $leaflet_form = drupal_render($time_slider_form);

  // Get users's vocabulary settings.
  global $user;
  $vocabulary = dss_get_visualization_profile($user);

  // Set id names for dynamic tables defined below.
  $dina_label = 'dina_label_0';
  $dina_table = 'dina_table_0';

  // Data Array.
  $chart = array(
    // First id must be called "sima".
    'id' => array('sima', 'dblpointbar'),
    'type' => 'time_aggregation',
    'title' => $variable_units ? "{$title} [{$variable_units}][$variable_range]" : "{$title}",
    'variable' => $variable_name,
    'unit' => $variable_units,
    'range' => $variable_range,
    'temporality' => $variable_temporality,
    'geometry' => $variable_geometry,
    'leaflet_form' => $leaflet_form,
    'layer_machine_name' => $layer_machine_name,
    'variable_machine_name' => $variable_machine_name,
    'dimensions' => $variable_dimensions,
    'weap_level' => $variable_weap_level,
    'data_dimension' => $data_dimension,
    'dina_label' => $dina_label,
    'dina_table' => $dina_table,
  );

  // Choropleth map.
  $chart_choropleth = array(
    'id' => array('leaflet_chart0'),
    'type' => 'choropleth_map',
    'title' => $resource->getCaseStudy() ? $resource->getCaseStudy()->getTitle() : t('No Case Study'),
    'geometry' => $variable_geometry,
    'vocabulary' => $vocabulary,
    'variable_machine_name' => $variable_machine_name,
    'weap_level' => $variable_weap_level,
    // 'geomap' => NULL,
    // 'data_keyed' => NULL,
    // 'data_ext' => NULL,
    // 'element_key_array' => NULL,.
    'context_layer_machine_names' => $context_layer_machine_name,
  // Normal: 0, Diff: 1 vocabulary.
    'switch_legend_control' => "0",
  );

  // Notices.
  $chart_notices = array(
    'id' => 'notice0',
    'type' => 'page_notice',
    'rows' => array(),
  );

  $output = "<br>";
  $output .= '<label id="' . $dina_label . '">&bull;&nbsp;' . $variable_name . "<small>&ensp;<font color=\"blue\">(view/hide)</font></small></label>";
  $output .= dss_visualizations_dynamic_table($resource, $dataset, $dina_table);
  $output .= dss_visualizations_draw($chart);
  $output .= dss_visualizations_draw($chart_choropleth);
  $output .= dss_visualizations_draw($chart_notices);

  return $output;
}
