<?php

/**
 * @file
 * Implements Visualizations for multi objectives Model.
 */

/**
 * Provides resource visualizations for Time/Space aggregations.
 *
 * @param array $data
 *   The array data to be displayed.
 *
 * @return string
 *   The output visualization.
 *
 * @throws \Exception
 *   If a field cannot be obtained, it will raise an exception.
 */
function dss_visualizations_multi_objectives($data) {
  // Sanity Check.
  if (is_array($data) && count($data) == 0) {
    return "";
  }

  $test_data = array(
    'dss_riesgo' => array(
      'title' => 'Riesgo',
      '308' => array(
        'title' => 'Caso Extremo',
        'value' => 0.72,
      ),
      '310' => array(
        'title' => 'Caso Linea Base',
        'value' => 0.95,
      ),
    ),
    'dss_molestia' => array(
      'title' => 'Molestia',
      '308' => array(
        'title' => 'Caso Extremo',
        'value' => 0.14,
      ),
      '310' => array(
        'title' => 'Caso Linea Base',
        'value' => 0.73,
      ),
    ),
    'dss_energia' => array(
      'title' => 'Energia',
      '308' => array(
        'title' => 'Caso Extremo',
        'value' => 0.83,
      ),
      '310' => array(
        'title' => 'Caso Linea Base',
        'value' => 0.22,
      ),
    ),
    'dss_ecosistema' => array(
      'title' => 'Ecosistema',
      '308' => array(
        'title' => 'Caso Extremo',
        'value' => 0.84,
      ),
      '310' => array(
        'title' => 'Caso Linea Base',
        'value' => 0.78,
      ),
    ),
    'dss_costos' => array(
      'title' => 'Costos',
      '308' => array(
        'title' => 'Caso Extremo',
        'value' => 0.72,
      ),
      '310' => array(
        'title' => 'Caso Linea Base',
        'value' => 0.3,
      ),
    ),
  );

  // Interpret received data.
  $row_label = array();
  $row_data = array();
  foreach ($data as $machine_names => $data_array) {
    $title_node = '';
    foreach ($data_array as $node_key => $node_data) {
      if ($node_key == 'title') {
        $title_node = $node_data;
      }
      else {
        // Create a node to acumulate data.
        if (!isset($row_data[$node_key])) {
          $row_data[$node_key] = array();
        }
        $temp = array($title_node, $node_data['value']);
        array_push($row_data[$node_key], $temp);

        // Names of the cases.
        if (!in_array($node_data['title'], $row_label)) {
          array_push($row_label, $node_data['title']);
        }
      }
    }
  }
  $row_data = array_values($row_data);

  // Load Demo Data.
  $row_label_t = array("Desarrollista AMB3x", "Actual Modificado ");
  $row_data_t = array(
    array(
      ["costos", 0.2],
      ["molestia", 0.4],
      ["energia", 0.6],
      ["riesgo", 0.9],
      ["ecosistema", 0.8],
    ),
    array(
      ["costos", 0.1],
      ["molestia", 0.3],
      ["energia", 0.6],
      ["riesgo", 0.4],
      ["ecosistema", 0.2],
    ),
  );

  /* Create graph */

  // Set form.
  $chart_select_form = drupal_get_form('dss_visualizations_chart_type_form');
  $chart_form = drupal_render($chart_select_form);

  // Multi Objetives Chart.
  $chart_table = array(
    'id' => 'sima',
    'type' => 'multi_objectives',
    'chart_form' => $chart_form,
    'legend' => $row_label,
    'rows' => $row_data,
  );

  // Radar Chart.
  $chart_radar = array(
    'id' => 'c_radar',
    'type' => 'radar',
    'legend' => $row_label,
    'rows' => $row_data,
  );

  // Bar Chart.
  $chart_bar = array(
    'id' => 'c_bar',
    'type' => 'categories',
    'legend' => $row_label,
    'units' => '',
    'rows' => $row_data,
  );

  // Table
  // Add Choroplet and Eloha Graphs.
  $output = dss_visualizations_draw($chart_table);
  $output .= dss_visualizations_draw($chart_radar);
  $output .= dss_visualizations_draw($chart_bar);

  return $output;
}

/**
 * Implements hook_form().
 */
function dss_visualizations_chart_type_form($form, &$form_state) {
  // Creates default select and radio buttons.
  $options = array(
    0 => t('Radar'),
    1 => t('Bars'),
    2 => t('Table'),
  );

  $form['chart_type'] = array(
    '#type' => 'select',
    '#default_value' => 0,
    '#options' => $options,
  );

  return $form;
}
