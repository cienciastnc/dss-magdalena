<?php

/**
 * @file
 * Implements Visualizations for Eloha Model.
 */

use Drupal\dss_magdalena\DSS\Entity\SimaCaseStudy;
use Drupal\dss_magdalena\DSS\Entity\SimaModel;

/**
 * Provides resource visualizations for Time/Space aggregations.
 *
 * @param int $cid
 *   The case id.
 *
 * @return string
 *   The output visualization.
 *
 * @throws \Exception
 *   If a field cannot be obtained, it will raise an exception.
 */
function dss_visualizations_eloha($cid) {
  // Load case, get case eloha's outputs and load them.
  $case = SimaCaseStudy::load($cid);
  $case_outputs = $case->getCaseOutputs();
  $case_resources_array = $case_outputs->getOutputResources(SimaModel::MODEL_ELOHA);

  // Check all eloha resources are part of the case of study.
  $error_control = FALSE;
  if (count($case_resources_array) < 6) {
    drupal_set_message(t('Not all the Eloha Outputs have been assigned to the Case of study (%cid)', array(
      '%cid' => $cid,
    )), 'error');
    $error_control = TRUE;
  }

  // Check if the resources had been imported and has a dataset assigned to it.
  foreach ($case_resources_array as $output_resource) {
    if (!$output_resource->isDataStoreImported()) {
      $resource_id = $output_resource->getId();

      // Check Dataset existence.
      $dataset = $output_resource->getDataset();
      if (!$dataset) {
        drupal_set_message(t('You are trying to access a Resource (%rid) that is not linked to a Dataset (Variable).', array(
          '%rid' => $resource_id,
        )), 'error');
        continue;
      }

      // Check variable has been imported.
      $dataset_name = $dataset->getTitle();
      $link_resource = l(t("here"), "node/{$resource_id}/datastore");
      drupal_set_message(t('The variable "%var" has not been imported to the DataStore yet. Please import it !link.', array(
        '%var' => $dataset_name,
        '!link' => $link_resource,
      )), 'warning');

      // Set error control vat to true.
      $error_control = TRUE;
    }
  }
  if ($error_control) {
    drupal_goto("case/{$cid}/model/eloha");
  }

  /* Create graph */

  // Set graph variable to verdict for map visualization.
  $resource = NULL;
  $dataset = NULL;
  $resource_machine_name_array = array();
  $rid_array = array();
  foreach ($case_resources_array as $key => $resource_item) {
    // Set resource machine names and id's.
    $resource_name = $resource_item->getDataset()->getMachineName();
    array_push($resource_machine_name_array, $resource_name);
    $resource_id = $resource_item->getId();
    array_push($rid_array, $resource_id);

    // Select verdict var for map visualization.
    if ($resource_name == 'dss_veredicto_eloha') {
      $resource = $resource_item;
      $dataset = $resource->getDataset();
    }
  }

  // Collecting data.
  $title = $resource->getTitle();
  $layer = $resource->getLayer();
  $layer_machine_name = $layer ? $layer->getMachineName() : NULL;
  $variable_units = $dataset->getUnitOfMeasure();
  $variable_name = $dataset->getTitle();
  $variable_temporality = $dataset->getTemporality();
  $variable_geometry = $dataset->getGeometry();
  $variable_machine_name = $dataset->getMachineName();

  // Get users's vocabulary settings.
  global $user;
  $vocabulary = dss_get_visualization_profile($user);

  // Set eloha graph id.
  $eloha_graph_id = 'eloha_table_vars';

  // Set id names for dynamic tables defined below.
  $dina_label = 'dina_label_';
  $dina_table = 'dina_table_';

  // Choropleth map.
  $chart_choropleth = array(
    'id' => array('sima'),
    'type' => 'eloha_map',
    'title' => $variable_units ? "{$title} [{$variable_units}]" : "{$title}",
    'variable' => $variable_name,
    'unit' => $variable_units,
    'temporality' => $variable_temporality,
    'geometry' => $variable_geometry,
    'leaflet_form' => NULL,
    'layer_machine_name' => $layer_machine_name,
    'variable_machine_name' => $variable_machine_name,
    'vocabulary' => $vocabulary,
    'nodes_machine_name' => $resource_name,
    'nodes_rid' => $rid_array,
    'table_eloha_id' => $eloha_graph_id,
    'machine_name_array' => $resource_machine_name_array,
    // 'geomap' => $resource->getLayer()->getGeoJson(),
    // 'data_keyed' => NULL,
    // 'data_ext' => NULL,
    // 'element_key_array' => NULL,.
    // Added but not part of eloha_map (dina_label, dina_table).
    'dina_label' => $dina_label,
    'dina_table' => $dina_table,
  );

  $chart_eloha = array(
    'id' => $eloha_graph_id,
    'type' => 'area_stacked',
    'colors' => array(
      'good' => 'green',
      'medium' => 'yellow',
      'bad' => 'red',
      'nodata' => 'grey',
    ),
    'show_stacked' => FALSE,
    'flow_name' => array(
      'Qmax-Q10',
      'Q10-Q75',
      'Q75-Q95',
      'Q95-Qmin',
    ),
    'impact_name' => array(
      t('Critical Alteration'),
      t('Alert Alteration'),
    ),
    'impact_color' => array(
      "medium" => "orange",
      "bad" => "red",
    ),
    'def_indicator' => array(),
    'sur_indicator' => array(),
    'def_index' => array(),
    'sur_index' => array(),
    'overall' => '',
    'impact_strings' => array(),
  );

  // Notices.
  $chart_notices = array(
    'id' => 'notice0',
    'type' => 'page_notice',
    'rows' => array(),
  );

  // Add Choroplet and Eloha Graphs.
  $output = dss_visualizations_draw($chart_choropleth);
  $output .= dss_visualizations_draw($chart_eloha);

  // Add variable details.
  $output .= "<br>";
  $output .= "<label><b>" . t("ELOHA OUTPUT VARS DETAILS") . ":</b></label><br>";
  foreach ($case_resources_array as $key => $resource_item) {
    $resource = $resource_item;
    $dataset = $resource->getDataset();
    $resource_name = $dataset->getTitle();
    $output .= '<label id="' . $dina_label . $key . '">&bull;&nbsp;' . $resource_name . "<small>&ensp;<font color=\"blue\">(view/hide)</font></small></label>";
    $table_id = $dina_table . $key;
    $output .= dss_visualizations_dynamic_table($resource, $dataset, $table_id);
  }

  $output .= dss_visualizations_draw($chart_notices);

  return $output;
}
