<?php
/**
 * @file
 * dss_user_profile.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function dss_user_profile_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_area_of_study'.
  $field_bases['field_area_of_study'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_area_of_study',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'Ciencias naturales' => 'Ciencias naturales',
        'Ingeniería' => 'Ingeniería',
        'Ciencias sociales' => 'Ciencias sociales',
        'Ciencias Administrativas' => 'Ciencias Administrativas',
        'Otras' => 'Otras',
      ),
      'allowed_values_function' => '',
      'profile2_private' => 0,
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_motivation_to_use_the_dss'.
  $field_bases['field_motivation_to_use_the_dss'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_motivation_to_use_the_dss',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'Curiosidad acerca del sistema' => 'Curiosidad acerca del sistema',
        'Interés en conocer el estado ambiental de las cuencas' => 'Interés en conocer el estado ambiental de las cuencas',
        'Asesoría profesional' => 'Asesoría profesional',
        'Soporte para inversiones' => 'Soporte para inversiones',
        'Investigación' => 'Investigación',
        'Docencia' => 'Docencia',
        'Aplicación en la labor de Administración publica' => 'Aplicación en la labor de Administración publica',
        'Otro' => 'Otro',
      ),
      'allowed_values_function' => '',
      'profile2_private' => 0,
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_user_country'.
  $field_bases['field_user_country'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_user_country',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'country_field',
    'settings' => array(
      'allowed_values' => array(),
      'allowed_values_function' => 'country_field_countries',
      'profile2_private' => 0,
    ),
    'translatable' => 0,
    'type' => 'country',
  );

  // Exported field_base: 'field_user_education_level'.
  $field_bases['field_user_education_level'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_user_education_level',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'Escuela Basica' => 'Escuela Basica',
        'Liceo' => 'Liceo',
        'Escuela Técnica' => 'Escuela Técnica',
        'Universidad' => 'Universidad',
        'PhD' => 'PhD',
        'Post-grado' => 'Post-grado',
        'Doctorado' => 'Doctorado',
      ),
      'allowed_values_function' => '',
      'profile2_private' => 0,
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_user_last_name'.
  $field_bases['field_user_last_name'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_user_last_name',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 255,
      'profile2_private' => 0,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  // Exported field_base: 'field_user_name'.
  $field_bases['field_user_name'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_user_name',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 255,
      'profile2_private' => 0,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  // Exported field_base: 'field_user_ocupation'.
  $field_bases['field_user_ocupation'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_user_ocupation',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'Asesor/Consultor Profesional' => 'Asesor/Consultor Profesional',
        'Investigador/Docente Universitario' => 'Investigador/Docente Universitario',
        'Otro' => 'Otro',
      ),
      'allowed_values_function' => '',
      'profile2_private' => 0,
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_user_organization'.
  $field_bases['field_user_organization'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_user_organization',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => 0,
    'module' => 'entityreference',
    'settings' => array(
      'handler' => 'base',
      'handler_settings' => array(
        'behaviors' => array(
          'views-select-list' => array(
            'status' => 0,
          ),
        ),
        'sort' => array(
          'type' => 'none',
        ),
        'target_bundles' => array(
          'organization' => 'organization',
        ),
      ),
      'profile2_private' => 0,
      'sync' => '',
      'target_type' => 'node',
    ),
    'translatable' => 0,
    'type' => 'entityreference',
  );

  // Exported field_base: 'field_user_organization_type'.
  $field_bases['field_user_organization_type'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_user_organization_type',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'Universidad' => 'Universidad',
        'Administración pública' => 'Administración pública',
        'Empresa' => 'Empresa',
        'Fundación/ONG' => 'Fundación/ONG',
        'Organización ambiental' => 'Organización ambiental',
        'Organización humanitaria' => 'Organización humanitaria',
        'Persona Individual' => 'Persona Individual',
      ),
      'allowed_values_function' => '',
      'profile2_private' => 0,
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_user_receive_news'.
  $field_bases['field_user_receive_news'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_user_receive_news',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        0 => 'No',
        1 => 'Yes',
      ),
      'allowed_values_function' => '',
      'profile2_private' => 1,
    ),
    'translatable' => 0,
    'type' => 'list_boolean',
  );

  // Exported field_base: 'field_visualization_vocabulary'.
  $field_bases['field_visualization_vocabulary'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_visualization_vocabulary',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'profile2_private' => 1,
    ),
    'translatable' => 0,
    'type' => 'text_long',
  );

  return $field_bases;
}
