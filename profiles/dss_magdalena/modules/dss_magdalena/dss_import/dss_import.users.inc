<?php
/**
 * @file
 * Migration for Users in DSS Magdalena.
 */

/**
 * Class DSSActiveUsers.
 */
class DSSActiveUsers extends ImportBaseUsers {

  /**
   * DSSActiveUsers constructor.
   *
   * @param array $arguments
   *   Constructor arguments.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);

    // Create a MigrateSource object.
    $this->description = t('Import DSS Users.');

    if (isset($arguments['path'])) {
      $import_path = $arguments['path'];
    }
    else {
      $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    }
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.users.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationUser();

    $this->addFieldMapping('created', 'created');
    $this->addFieldMapping('access', 'access');
    $this->addFieldMapping('login', 'login');
    $this->addFieldMapping('timezone', 'timezone');
    $this->addFieldMapping('signature', 'signature');
    $this->addFieldMapping('signature_format', 'signature_format');

  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Username');
    $columns[1] = array('mail', 'Email');
    $columns[2] = array('pass', 'Password');
    $columns[3] = array('created', 'Created');
    $columns[4] = array('access', 'Access');
    $columns[5] = array('login', 'Login');
    $columns[6] = array('status', 'Status');
    $columns[7] = array('timezone', 'Timezone');
    $columns[8] = array('roles', 'Roles');
    $columns[9] = array('signature', 'Signature');
    $columns[10] = array('signature_format', 'SignatureFormat');
    return $columns;
  }

}
