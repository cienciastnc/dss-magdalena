<?php

/**
 * @file
 * Migration for DPSIR Taxonomy Terms in DSS Magdalena.
 */

/**
 * Class DSSDPSIRTerms.
 */
class DSSDPSIRTerms extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.dpsir.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('dpsir');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}

/**
 * Migration for SWOT Taxonomy Terms in DSS Magdalena.
 */
class DSSSWOTTerms extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.swot.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('swot');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}

/**
 * Migration for Appropriate Spatial Scale Taxonomy Terms in DSS Magdalena.
 */
class DSSASSTerms extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.appropriate_spatial_scale.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('appropriate_spatial_scale');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}

/**
 * Migration for Geometry Taxonomy Terms in DSS Magdalena.
 */
class DSSGeomTerms extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.geometry.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('geometry');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}

/**
 * Migration for Variable Type Taxonomy Terms in DSS Magdalena.
 */
class DSSVarTypeTerms extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.variable_type.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('variable_type');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}

/**
 * Migration for Temporality Taxonomy Terms in DSS Magdalena.
 */
class DSSTemporalityTerms extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.temporality.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('temporality');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}

/**
 * Migration for PO Taxonomy Terms in DSS Magdalena.
 */
class DSSPOTerms extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.po.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('po');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}

/**
 * Migration for Numeric Scale Taxonomy Terms in DSS Magdalena.
 */
class DSSNumScaleTerms extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.numeric_scale.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('numeric_scale');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}

/**
 * Migration for Variable Category Taxonomy Terms in DSS Magdalena.
 */
class DSSVarCategoryTerms extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.variable_category.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('variable_category');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}

/**
 * Migration for Thematic Tree Taxonomy Terms in DSS Magdalena.
 */
class DSSThematicTreeTerms extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.thematic_tree.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('thematic_tree');

    // Adding field mappings.
    $this->addFieldMapping('parent_name', 'parent_name');
    $this->addFieldMapping('description', 'description');
    $this->addFieldMapping('weight', 'weight');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    $columns[1] = array('parent_name', 'ParentName');
    $columns[2] = array('description', 'Description');
    $columns[3] = array('weight', 'Weight');
    return $columns;
  }

}

/**
 * Migration for Orientation Taxonomy Terms in DSS Magdalena.
 */
class DSSOrientation extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.orientation.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('orientation');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}

/**
 * Migration for Publication Status Taxonomy Terms in DSS Magdalena.
 */
class DSSPublicationStatus extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.publication_status.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('publication_status');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}
/**
 * Migration for DSSPublicationType Taxonomy Term.
 */
class DSSPublicationType extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.publication_type.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('publication_type');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}

/**
 * Migration for Geographical Area Taxonomy Terms in DSS Magdalena.
 */
class DSSGeographicalArea extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.geographical_area.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('geographical_area');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}

/**
 * Migration for Digital Format Taxonomy Terms in DSS Magdalena.
 */
class DSSDigitalFormat extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.digital_format.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('digital_format');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}

/**
 * Migration for Content Style Taxonomy Terms in DSS Magdalena.
 */
class DSSContentStyle extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.content_style.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('content_style');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}

/**
 * Migration for Access Level Taxonomy Terms in DSS Magdalena.
 */
class DSSAccessLevel extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.access_level.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('access_level');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}

/**
 * Migration for Instruments Provided Taxonomy Terms in DSS Magdalena.
 */
class DSSInstrumentsProvided extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.instruments_provided.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('instruments_provided');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}

/**
 * Migration for Subsystem Taxonomy Terms in DSS Magdalena.
 */
class DSSSubsystem extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.subsystem.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('subsystem');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}

/**
 * Migration for Node Category Taxonomy Terms in DSS Magdalena.
 */
class DSSNodeCategory extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.node_category.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('node_category');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}

/**
 * Migration for Project Purpose Taxonomy Terms in DSS Magdalena.
 */
class DSSProjectPurpose extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.project_purpose.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('project_purpose');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}

/**
 * Migration for Project Primary Purpose Taxonomy Terms in DSS Magdalena.
 */
class DSSPrimaryPurpose extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.primary_purpose.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('primary_purpose');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}

/**
 * Migration for Project Execution Status Taxonomy Terms in DSS Magdalena.
 */
class DSSExecutionStatus extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.execution_status.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('execution_status');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}

/**
 * Migration for Project Operating Status Taxonomy Terms in DSS Magdalena.
 */
class DSSOperatingStatus extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.operating_status.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('operating_status');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}

/**
 * Migration for Project Maintenance Status Taxonomy Terms in DSS Magdalena.
 */
class DSSMaintenanceStatus extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.maintenance_status.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('maintenance_status');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}

/**
 * Migration for Constructive Typology Taxonomy Terms in DSS Magdalena.
 */
class DSSConstructiveTypology extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.constructive_typology.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('constructive_typology');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}

/**
 * Migration for Legal Personality Taxonomy Terms in DSS Magdalena.
 */
class DSSLegalPersonality extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.legal_personality.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('legal_personality');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}

/**
 * Migration for Organization Type Taxonomy Terms in DSS Magdalena.
 */
class DSSOrganizationType extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.organization_type.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('organization_type');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}

/**
 * Migration for Measurement methods Taxonomy Terms in DSS Magdalena.
 */
class DSSMeasurementMethods extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.measurement_methods.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('measurement_methods');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}

/**
 * Migration for Campaign measurement Taxonomy Terms in DSS Magdalena.
 */
class DSSCampaignMeasurement extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.campaign_measurement.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('campaign_measurement');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}

/**
 * Migration for Measurement stations Taxonomy Terms in DSS Magdalena.
 */
class DSSMeasurementStations extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.measurement_stations.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('measurement_stations');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}

/**
 * Migration for Manual measurement Taxonomy Terms in DSS Magdalena.
 */
class DSSManualMeasurement extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.manual_measurement.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('manual_measurement');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}

/**
 * Migration for Possible Descriptors Taxonomy Terms in DSS Magdalena.
 */
class DSSDescriptors extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.possible_descriptors.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('possible_descriptors');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}

/**
 * Migration for Variable Status Taxonomy Terms in DSS Magdalena.
 */
class DSSVariableStatus extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.variable_status.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('variable_status');

    // Adding field mappings.
    $this->addFieldMapping('parent_name', 'parent_name');
    $this->addFieldMapping('weight', 'weight');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    $columns[1] = array('parent_name', 'ParentName');
    $columns[2] = array('weight', 'Weight');
    return $columns;
  }

}

/**
 * Migration for Water Balance Status Taxonomy Terms in DSS Magdalena.
 */
class DSSWaterBalanceStatus extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.water_balance_status.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('water_balance_status');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}

/**
 * Migration for Water Balance Category Taxonomy Terms in DSS Magdalena.
 */
class DSSWaterBalanceCategory extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.water_balance_category.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('water_balance_category');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}

/**
 * Migration for WEAP Object Type Taxonomy Terms in DSS Magdalena.
 */
class DSSWEAPObjectType extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.weap_object_type.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('weap_object_type');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}

/**
 * Migration for Model Family Taxonomy Terms in DSS Magdalena.
 */
class DSSModelFamily extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.model_family.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('model_family');

    // Adding field mappings.
    $this->addFieldMapping('parent_name', 'parent_name');
    $this->addFieldMapping('weight', 'weight');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    $columns[1] = array('parent_name', 'ParentName');
    $columns[2] = array('weight', 'Weight');
    return $columns;
  }

}

/**
 * Migration for Value Range Taxonomy Terms in DSS Magdalena.
 */
class DSSValueRange extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.value_range.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('value_range');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}

/**
 * Migration for Unit of Measure Taxonomy Terms in DSS Magdalena.
 */
class DSSUnitMeasure extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.unit_of_measure.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('unit_of_measure');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}

/**
 * Migration for Relationship Type Taxonomy Terms in DSS Magdalena.
 */
class DSSRelationshipType extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.relationship_type.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('relationship_type');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}

/**
 * Migration for Dimensions Taxonomy Terms in DSS Magdalena.
 */
class DSSDimensions extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.dimensions.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('dimensions');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}

/**
 * Migration for Decision-making Group Taxonomy Terms in DSS Magdalena.
 */
class DSSDecisionGroup extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.decision_group.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('decision_group');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}

/**
 * Migration for Key to Alternatives Taxonomy Terms in DSS Magdalena.
 */
class DSSKeyAlternatives extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.key_alternatives.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('key_alternatives');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}

/**
 * Migration for Decision Type Taxonomy Terms in DSS Magdalena.
 */
class DSSDecisionType extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.decision_type.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('decision_type');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}

/**
 * Migration for Weap Level Taxonomy Terms in DSS Magdalena.
 */
class DSSWeapLevel extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.weap_level.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('weap_level');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}

/**
 * Migration for Irrigation District Taxonomy Terms in DSS Magdalena.
 */
class DSSIrrigationDistrict extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.irrigation_district.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('irrigation_district');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}

/**
 * Migration for Project Type of Work Taxonomy Terms in DSS Magdalena.
 */
class DSSTypeofWork extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.type_of_work.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('type_of_work');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}

/**
 * Migration for Project Specific Purpose Taxonomy Terms in DSS Magdalena.
 */
class DSSSpecificPurpose extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.specific_purpose.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('specific_purpose');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}

/**
 * Migration for Weap Level 1 Taxonomy Terms in DSS Magdalena.
 */
class DSSSLevel1 extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.level_1.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('level_1');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}

/**
 * Migration for Land Use Types Taxonomy Terms in DSS Magdalena.
 */
class DSSLandUseTypes extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.land_use_types.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('land_use_types');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}

/**
 * Migration for Baseline Status Types Taxonomy Terms in DSS Magdalena.
 */
class DSSBaselineStatus extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.baseline_status.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('baseline_status');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}

/**
 * Migration for Baseline Status Types Taxonomy Terms in DSS Magdalena.
 */
class DSSAggregationOperatorType extends ImportBaseTerms {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.terms.aggregation_operator_type.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('aggregation_operator_type');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('name', 'Name');
    return $columns;
  }

}
