<?php
/**
 * @file
 * DSS import module drush integration.
 */

use League\Csv\Writer;
use Drupal\dss_magdalena\DSS\Entity\SimaResource;

/**
 * Implements hook_drush_command().
 *
 * @See drush_parse_command() for a list of recognized keys.
 */
function dss_import_drush_command() {
  $items = array();

  $items['dss-import-content-import'] = array(
    'description' => 'Imports default content for SIMA.',
    'callback' => 'drush_dss_import_content_import',
    'examples' => array(
      'drush dss-import-content-import' => 'Import DSS menus',
    ),
    'aliases' => array('dss-imc', 'dss-content-import'),
  );

  $items['dss-import-post-content-import'] = array(
    'description' => 'Performs post content import tasks for SIMA.',
    'callback' => 'drush_dss_import_post_content_import',
    'options' => array(
      'process_datastore' => array(
        'description' => 'Process Datastore for all imported Resources.',
        'example_value' => 1,
      ),
    ),
    'examples' => array(
      'drush dss-import-post-content-import' => 'Performs post content import tasks',
      'drush dss-import-post-content-import --process_datastore=1' => 'Performs post content import tasks, including processing of datastore for resources.',
    ),
    'aliases' => array('dss-impc', 'dss-post-content-import'),
  );

  $items['dss-import-weapmaster-import'] = array(
    'description' => 'Imports a WEAP Masterfile.',
    'callback' => 'drush_dss_import_weapmaster_import',
    "arguments" => array(
      "url" => "Public URL where the file has to be downloaded from",
      "default" => "Force upload of default WEAP Masterfile",
    ),
    "required-arguments" => 0,
    'examples' => array(
      'drush dss-import-weapmaster-import' => 'Imports a WEAP Masterfile',
    ),
    'aliases' => array('dss-wmfi', 'dss-weap-import'),
  );

  $items['dss-export-current-users'] = array(
    'description' => 'Exports the list of current Users registered in SIMA.',
    'callback' => 'drush_dss_export_current_users',
    'examples' => array(
      'drush dss-export-current-users' => 'Exports Current Users in SIMA.',
    ),
    'aliases' => array('dss-exp-users', 'dss-export-users'),
  );

  $items['dss-process-datastore-single'] = array(
    'description' => 'Processes the datastore for a DSS Resource Node.',
    'callback' => 'drush_dss_import_process_datastore_single',
    "arguments" => array(
      "nid" => "Resource Node NID",
    ),
    "required-arguments" => 1,
    'examples' => array(
      'drush dss-process-datastore-single 23' => 'Processes the datastore for Node Resource with nid = 23.',
    ),
    'aliases' => array('dss-proc-single', 'dss-pdss'),
  );

  $items['dss-process-datastore-multiple'] = array(
    'description' => 'Processes the datastore for multiple DSS Resource Nodes.',
    'callback' => 'drush_dss_import_process_datastore_multiple',
    'examples' => array(
      'drush dss-process-datastore-multiple' => 'Processes the datastore for all Resource Nodes.',
    ),
    'aliases' => array('dss-proc-mul', 'dss-pdsm'),
  );

  return $items;
}

/**
 * Implements hook_drush_help().
 */
function dss_import_drush_help($section) {
  switch ($section) {
    case 'drush:dss-import-content-import':
      return dt('Imports default DSS Content.');

    case 'drush:dss-import-post-content-import':
      return dt('Post-processing content import tasks.');

    case 'drush:dss-import-weapmaster-import':
      return dt('Imports a WEAP Masterfile in Excel format.');

    case 'drush:dss-export-current-users':
      return dt('Exports the list of current Users registered in SIMA.');
  }
}

/**
 * Callback function for drush command dss-import-content-import.
 */
function drush_dss_import_content_import() {
  // Import DSS Content.
  dss_import_content_import();
}

/**
 * Callback function for drush command dss-import-content-import.
 */
function drush_dss_export_current_users() {
  // Exports current SIMA Users.
  $results = db_query("SELECT * FROM {users}", array());
  $rows[] = [
    'Username',
    'Email',
    'Password',
    'Created',
    'Access',
    'Login',
    'Status',
    'Timezone',
    'Roles',
    'Signature',
    'SignatureFormat',

  ];
  foreach ($results as $row) {
    if ($row->uid <= 1) {
      // Eliminating Anonymous and Administrator user.
      continue;
    }
    $user = user_load($row->uid);
    $row = array();
    $row[] = $user->name;
    $row[] = $user->mail;
    $row[] = $user->pass;
    $row[] = $user->created;
    $row[] = $user->access;
    $row[] = $user->login;
    $row[] = $user->status;
    $row[] = $user->timezone;
    $row[] = implode(',', $user->roles);
    $row[] = $user->signature;
    $row[] = $user->signature_format;
    $rows[] = $row;
  }

  // Saving CSV File.
  $destination = drupal_get_path('module', 'dss_import') . '/import/dss_import.users.csv';
  $csv = Writer::createFromFileObject(new SplTempFileObject());
  $csv->insertAll($rows);
  if (file_unmanaged_save_data($csv, $destination, FILE_EXISTS_REPLACE) === FALSE) {
    drush_log(dt('Cannot created/update Users file: !file', array(
      '!file' => $destination,
    )), 'error');
  }
  else {
    drush_log(dt('Created/Updated Users file successfully: !file', array(
      '!file' => $destination,
    )), 'ok');
  }
}


/**
 * Callback function for drush command dss-import-post-content-import.
 */
function drush_dss_import_post_content_import() {
  // Obtaining the limit.
  $process_datastore = drush_get_option("process_datastore");
  $process_datastore = isset($process_datastore) ? (bool) $process_datastore : FALSE;

  // Populates WEAP Index Table.
  dss_import_populate_weap_index_table();

  // Upload WEAP Master File.
  drush_dss_import_weapmaster_import();

  // Also import the WEAP MAster File in CSV Format.
  // @TODO: This should be converted from the previous uploaded file.
  drush_dss_import_weapmaster_csv_import();

  // Import Layers Metadata from the Masterfile (River Reaches and Catchments).
  dss_import_layers_metadata_import();

  // Post Content-Import Tasks.
  dss_import_post_content_import_tasks($process_datastore);

  // Clear all Drupal Caches.
  cache_clear_all();
}

/**
 * Callback function for drush command dss-import-weapmaster-import.
 *
 * @param string $uri
 *   The WEAP Masterfile URL.
 * @param string $default
 *   'yes' to use default masterfile. If so, the url parameter is ignored.
 *
 * @TODO: Fix this function.
 *
 * @return bool
 *   TRUE if it succeeds, FALSE otherwise.
 */
function drush_dss_import_weapmaster_import($uri = NULL, $default = NULL) {
  if (empty($uri)) {
    if (empty($default)) {
      $default = 'yes';
    }
  }

  $model_version = '';
  if ($default == 'yes') {
    // By default this is the file.
    $uri = drupal_get_path('module', 'dss_import') . '/weap_master_file/master-2.1.6.1.xlsx';
    $model_version = 'MAGDALENA_V2.1.6.1';
    variable_set('weap_master_model_version', $model_version);
  }
  elseif (empty($uri)) {
    return drush_set_error(dt("Could not upload WEAP Master File. Missing required parameter: 'url' (the public URL for the WEAP Masterfile)"));
  }

  // Now that the file is properly saved. Notify the class to save it.
  $file = file_save((object) array(
    'filename' => 'master-2.1.6.1.xlsx',
    'uri' => $uri,
    'status' => FILE_STATUS_PERMANENT,
    'filemime' => file_get_mimetype($uri),
  ));
  if (isset($file->fid)) {
    variable_set('dss_engine_weap_masterfile_excel_fid', $file->fid);
    $success = 'ok';
  }
  else {
    $success = 'failed';
  }
  drush_log(dt('WEAP Master File Uploaded: "!url". !version', array(
    '!url' => $file->uri,
    '!version' => '[' . $model_version . ']',
  )), $success);
}

/**
 * Import Masterfile in CSV Format.
 *
 * This should be removed and converted from the XLS file uploaded previously.
 *
 * @deprecated
 */
function drush_dss_import_weapmaster_csv_import() {
  // This is the link to the Master File in CSV Format.
  $model_version = 'MAGDALENA_V2.1.6.1';
  $uri = drupal_get_path('module', 'dss_import') . '/weap_master_file/master-2.1.6.1.csv';

  // Now that the file is properly saved. Notify the class to save it.
  $file = file_save((object) array(
    'filename' => 'master-2.1.6.1.csv',
    'uri' => $uri,
    'status' => FILE_STATUS_PERMANENT,
    'filemime' => file_get_mimetype($uri),
  ));

  if (isset($file->fid)) {
    variable_set('dss_engine_weap_masterfile_csv_fid', $file->fid);
    $success = 'ok';
  }
  else {
    $success = 'failed';
  }

  drush_log(dt('WEAP Master File in CSV Format Uploaded: "!url". !version', array(
    '!url' => $file->uri,
    '!version' => '[' . $model_version . ']',
  )), $success);
}

/**
 * Processes the Datastore for a single Resource Node.
 *
 * @param int $nid
 *   The Resource Node NID.
 */
function drush_dss_import_process_datastore_single($nid) {
  if ($resource = SimaResource::load($nid)) {
    $node = $resource->getDrupalEntity()->value();
    dss_import_process_datastore_single_resource($node);
  }
  else {
    drush_log(dt('The argument provided does not correspond to a Resource Node (nid = !nid)', array(
      '!nid' => $nid,
    )), 'failed');
  }
}

/**
 * Processes the Datastore for ALL the Resource Nodes.
 */
function drush_dss_import_process_datastore_multiple() {
  dss_import_process_datastore_multiple_resources();
}
