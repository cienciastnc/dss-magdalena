<?php
/**
 * @file
 * Migration for Nodes in DSS Magdalena.
 */

module_load_include('inc', 'geophp', 'geoPHP/geoPHP');

/**
 * Class DSSDataset.
 */
class DSSDataset extends ImportBaseNodes {

  /**
   * DSSDataset constructor.
   *
   * @param array $arguments
   *   Constructor arguments.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Import DSS Datasets.');
    $this->dependencies = array('DSSLayers');
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';

    // Create a MigrateSource object.
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.nodes.dataset.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationNode('dataset');

    // Taxonomy Terms.
    $this->addFieldMapping('field_variable_category', 'variable_category');
    $this->addFieldMapping('field_type', 'type');
    $this->addFieldMapping('field_appropriate_spatial_scale', 'appropriate_spatial_scale');
    $this->addFieldMapping('field_numeric_scale', 'numeric_scale');
    $this->addFieldMapping('field_geometry', 'geometry');
    $this->addFieldMapping('field_temporality', 'temporality');

    // Text fields.
    $this->addFieldMapping('field_weap_name', 'weap_name');
    $this->addFieldMapping('field_full_name', 'full_name');
    $this->addFieldMapping('field_machine_name', 'machine_name');
    $this->addFieldMapping('field_value_range', 'range');
    $this->addFieldMapping('field_unit_of_measure', 'unit_of_measure');
    $this->addFieldMapping('field_dimensions', 'dimensions');
    $this->addFieldMapping('field_dimensions_labels', 'dimensionslabels');

    // Added Fields.
    $this->addFieldMapping('field_subsystem', 'subsystem');
    $this->addFieldMapping('field_model_family', 'model_family');
    $this->addFieldMapping('field_node_category', 'node_category');
    $this->addFieldMapping('field_aggregation_tree', 'aggregation_tree');
    $this->addFieldMapping('field_child_variable', 'child_variable');
    $this->addFieldMapping('field_aggregation_algorithm', 'aggregation_algorithm');
    $this->addFieldMapping('field_reference_state', 'reference_state');
    $this->addFieldMapping('field_variable_status', 'variable_status');
    $this->addFieldMapping('field_decision_group', 'decision_group');
    $this->addFieldMapping('field_key_alternatives', 'key_alternatives');
    $this->addFieldMapping('field_decision_type', 'decision_type');
    $this->addFieldMapping('field_weap_object_type', 'weap_object_type');
    $this->addFieldMapping('field_weap_level', 'weap_level');
    $this->addFieldMapping('field_default_value‎', 'default_value');
    $this->addFieldMapping('field_level_1', 'level_1');
    $this->addFieldMapping('field_level_2', 'level_2');
    $this->addFieldMapping('field_level_3', 'level_3');
    $this->addFieldMapping('field_level_4', 'level_4');
    $this->addFieldMapping('field_relevance_analysis', 'relevance_analysis');

    // Thematic Tree Field Group Added Fields.
    $this->addFieldMapping('field_associated_thematic_tree', 'associated_thematic_tree');
    $this->addFieldMapping('field_dpsir', 'dpsir');
    $this->addFieldMapping('field_swot', 'swot');
    $this->addFieldMapping('field_po', 'po');

    // Entity Reference Fields.
    $this->addFieldMapping('field_context', 'context')->sourceMigration('DSSLayers');

  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('machine_name', 'MachineName');
    $columns[1] = array('variable_status', 'VariableStatus');
    $columns[2] = array('variable_category', 'VariableCategory');
    $columns[3] = array('weap_name', 'WEAPName');
    $columns[4] = array('weap_object_type', 'WEAPObjectType');
    $columns[5] = array('weap_level', 'WEAPLevel');
    $columns[6] = array('level_1', 'Level1');
    $columns[7] = array('level_2', 'Level2');
    $columns[8] = array('level_3', 'Level3');
    $columns[9] = array('level_4', 'Level4');
    $columns[10] = array('associated_thematic_tree', 'AssociatedThematicTree');
    $columns[11] = array('dpsir', 'DPSIR');
    $columns[12] = array('swot', 'SWOT');
    $columns[13] = array('po', 'PO');
    $columns[14] = array('decision_group', 'DecisionGroup');
    $columns[15] = array('key_alternatives', 'KeyAlternatives');
    $columns[16] = array('decision_type', 'DecisionType');
    $columns[17] = array('full_name', 'FullName');
    $columns[18] = array('title', 'Name');
    $columns[19] = array('body', 'Description');
    $columns[20] = array('thematic_tree', 'ThematicTree');
    $columns[21] = array('type', 'Type');
    $columns[22] = array('subsystem', 'Subsystem');
    $columns[23] = array('model_family', 'ModelFamily');
    $columns[24] = array('node_category', 'NodeCategory');
    $columns[25] = array('aggregation_tree', 'AggregationTree');
    $columns[26] = array('child_variable', 'ChildVariable');
    $columns[27] = array('aggregation_algorithm', 'AggregationAlgorithm');
    $columns[28] = array('reference_state', 'ReferenceState');
    $columns[29] = array('appropriate_spatial_scale', 'Spatialscale');
    $columns[30] = array('numeric_scale', 'NumericScale');
    $columns[31] = array('range', 'Range');
    $columns[32] = array('unit_of_measure', 'Unit');
    $columns[33] = array('geometry', 'Geometry');
    $columns[34] = array('dimensions', 'Dimensions');
    $columns[35] = array('dimensionslabels', 'DimensionsLabels');
    $columns[36] = array('temporality', 'Temporality');
    $columns[37] = array('default_value', 'DefaultValue');
    $columns[38] = array('context', 'Context');
    $columns[39] = array('relevance_analysis', 'RelevanceOfAnalysis');
    return $columns;
  }

  /**
   * Prepares Row for importing.
   */
  public function prepareRow($row) {
    $row->context = explode(",", $row->context);
    $row->dimensionslabels = explode(",", $row->dimensionslabels);
    return TRUE;
  }

}

/**
 * Class DSSResource.
 */
class DSSResource extends Migration {

  /**
   * DSSResource constructor.
   *
   * @param array $arguments
   *   Constructor arguments.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Import DSS Resources.');

    // Create a map object for tracking the relationships between source rows.
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'title' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
        ),
      ),
      MigrateDestinationNode::getKeySchema()
    );

    $this->dependencies = array(
      'DSSDataset',
      'DSSResourceFilesMigration',
      'DSSLayers',
    );
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';

    // Create a MigrateSource object.
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.nodes.resources.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationNode('resource');

    // Default fields.
    $this->addFieldMapping('uid', 'uid')->defaultValue(1);
    $this->addFieldMapping('status', 'status')->defaultValue(1);
    $this->addFieldMapping('language', 'language')->defaultValue('en');
    $this->addFieldMapping('title', 'title');
    $this->addFieldMapping('body', 'body');
    $this->addFieldMapping('body:format')->defaultValue('filtered_html');

    // Taxonomy Terms.
    $this->addFieldMapping('field_format', 'field_format');

    // Entity Reference Fields.
    $this->addFieldMapping('field_dataset_ref', 'dataset')->sourceMigration('DSSDataset');
    $this->addFieldMapping('field_spatial_layer', 'spatial_layer')->sourceMigration('DSSLayers');
    // @codingStandardsIgnoreStart
    // $this->addFieldMapping('field_upload', 'file_upload')->sourceMigration('DSSResourceFilesMigration');
    // @codingStandardsIgnoreEnd

    // Text fields.
    $this->addFieldMapping('field_full_name', 'full_name');

    // @TODO: Fix mapping the "recline.js visualization" field "field_upload".
    // For now it does not work, so we are using the body to transfer the data.
    // and be able to do a postprocessing later.
    // File Upload.
    $this->addFieldMapping('field_upload', 'file_upload');

    // @codingStandardsIgnoreStart
    // $this->addFieldMapping('field_upload:file_replace')->defaultValue(FILE_EXISTS_REPLACE);
    // $this->addFieldMapping('field_upload:source_dir')->defaultValue($import_path . 'resources');
    // $this->addFieldMapping('field_upload:destination_file', 'filename');.
    // @codingStandardsIgnoreEnd

    $this->addFieldMapping('field_upload:service_id')->defaultValue('');
    $this->addFieldMapping('field_upload:delimiter')->defaultValue(',');
    $this->addFieldMapping('field_upload:grid')->defaultValue(1);
    $this->addFieldMapping('field_upload:graph')->defaultValue(1);
    $this->addFieldMapping('field_upload:map')->defaultValue(0);
    $this->addFieldMapping('field_upload:embed')->defaultValue(0);

    // Creation Date.
    $creation_date = new DateTime('NOW');
    $creation_date_value = date_format($creation_date, 'Y-m-d');
    $timezone = drupal_get_user_timezone();
    $this->addFieldMapping('field_creation_date')
      ->defaultValue($creation_date_value);
    $this->addFieldMapping('field_creation_date:timezone')
      ->defaultValue($timezone);

    // Other fields.
    $this->addFieldMapping('field_processing_type', 'processing_type');
    $this->addFieldMapping('field_model_family', 'model_family');
    $this->addFieldMapping('field_model_declaration')
      ->defaultValue('MAGDALENA_V2.1.6.1');

  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('title', 'Name');
    $columns[1] = array('full_name', 'FullName');
    $columns[2] = array('dataset', 'Dataset');
    $columns[3] = array('field_format', 'Format');
    $columns[4] = array('description', 'Description');
    $columns[5] = array('file_upload', 'FileUpload');
    $columns[6] = array('processing_type', 'ProcessingType');
    $columns[7] = array('model_family', 'ModelFamily');
    $columns[8] = array('spatial_layer', 'Layer');
    return $columns;
  }

  /**
   * Prepares Row for importing.
   */
  public function prepareRow($row) {
    $row->body = $row->description . '. File:' . $row->file_upload;
  }

}

/**
 * Class DSSClimateScenario.
 */
class DSSClimateScenario extends ImportBaseNodes {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Import DSS Climate Scenarios.');
    /*$this->dependencies = array('DSSDataset');*/
    $this->dependencies = array('DSSResource');
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';

    // Create a MigrateSource object.
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.nodes.climate_scenario.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationNode('climate_scenario');
    $this->addFieldMapping('field_creation_date', 'creation_date');
    $this->addFieldMapping('field_full_name', 'full_name');
    $this->addFieldMapping('field_climate_id', 'climate_id');

    // Entity Reference Fields.
    $this->addFieldMapping('field_variable_databoxes_cli', 'variable_databoxes_cli')->sourceMigration('DSSResource');

  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('climate_id', 'ID');
    $columns[1] = array('title', 'Name');
    $columns[2] = array('full_name', 'FullName');
    $columns[3] = array('creation_date', 'CreationDate');
    $columns[4] = array('body', 'Description');
    $columns[5] = array('variable_databoxes_cli', 'ClimateScenarioResources');
    return $columns;
  }

  /**
   * Prepares Row for importing.
   */
  public function prepareRow($row) {
    $row->variable_databoxes_cli = explode(",", $row->variable_databoxes_cli);
    return TRUE;
  }

}

/**
 * Class DSSPopulationScenario.
 */
class DSSPopulationScenario extends ImportBaseNodes {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Import DSS Population Scenarios.');
    /*$this->dependencies = array('DSSDataset');*/
    $this->dependencies = array('DSSResource');
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';

    // Create a MigrateSource object.
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.nodes.population_scenario.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationNode('population_scenario');
    $this->addFieldMapping('field_creation_date', 'creation_date');
    $this->addFieldMapping('field_full_name', 'full_name');
    $this->addFieldMapping('field_population_id', 'population_id‎');

    // Entity Reference Fields.
    $this->addFieldMapping('field_variable_databoxes_pop', 'variable_databoxes_pop')->sourceMigration('DSSResource');

  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('population_id‎', 'ID');
    $columns[1] = array('title', 'Name');
    $columns[2] = array('full_name', 'FullName');
    $columns[3] = array('creation_date', 'CreationDate');
    $columns[4] = array('body', 'Description');
    $columns[5] = array('variable_databoxes_pop', 'PopulationScenarioResources');
    return $columns;
  }

  /**
   * Prepares Row for importing.
   */
  public function prepareRow($row) {
    $row->variable_databoxes_pop = explode(",", $row->variable_databoxes_pop);
    return TRUE;
  }

}

/**
 * Class DSSEnergyScenario.
 */
class DSSEnergyScenario extends ImportBaseNodes {

  /**
   * CSV Columns mapping.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Import DSS Energy Scenarios.');
    /*$this->dependencies = array('DSSDataset');*/
    $this->dependencies = array('DSSResource');
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';

    // Create a MigrateSource object.
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.nodes.energy_scenario.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationNode('energy_scenario');
    $this->addFieldMapping('field_creation_date', 'creation_date');
    $this->addFieldMapping('field_full_name', 'full_name');
    $this->addFieldMapping('field_energy_id‎', 'energy_id‎');

    // Entity Reference Fields.
    $this->addFieldMapping('field_variable_databoxes_ene', 'variable_databoxes_ene')->sourceMigration('DSSResource');

  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('energy_id', 'ID');
    $columns[1] = array('title', 'Name');
    $columns[2] = array('full_name', 'FullName');
    $columns[3] = array('creation_date', 'CreationDate');
    $columns[4] = array('body', 'Description');
    $columns[5] = array('variable_databoxes_ene', 'EnergyScenarioResources');
    return $columns;
  }

  /**
   * Prepares Row for importing.
   */
  public function prepareRow($row) {
    $row->variable_databoxes_ene = explode(",", $row->variable_databoxes_ene);
    return TRUE;
  }

}

/**
 * Class DSSProductiveLandUseAlternatives.
 */
class DSSProductiveLandUseAlternatives extends ImportBaseNodes {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Import DSS Productive Land Use Alternatives.');
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';

    // Create a MigrateSource object.
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.nodes.productive_landuse_alternatives.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationNode('productive_land_use_alternatives');
    $this->addFieldMapping('field_creation_date', 'creation_date');
    $this->addFieldMapping('field_full_name', 'full_name');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('title', 'Name');
    $columns[1] = array('full_name', 'FullName');
    $columns[2] = array('creation_date', 'CreationDate');
    $columns[3] = array('body', 'Description');
    return $columns;
  }

}

/**
 * Class DSSHydropowerAlternatives.
 */
class DSSHydropowerAlternatives extends ImportBaseNodes {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Import DSS Hydropower Alternatives.');
    /*$this->dependencies = array('DSSDataset');*/
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';

    // Create a MigrateSource object.
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.nodes.hydropower_alternatives.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationNode('hydropower_alternatives');
    $this->addFieldMapping('field_creation_date', 'creation_date');
    $this->addFieldMapping('field_full_name', 'full_name');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('title', 'Name');
    $columns[1] = array('full_name', 'FullName');
    $columns[2] = array('creation_date', 'CreationDate');
    $columns[3] = array('body', 'Description');
    return $columns;
  }

}

/**
 * Class DSSConnectivityAlternatives.
 */
class DSSConnectivityAlternatives extends ImportBaseNodes {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Import DSS River-Floodplain Connectivity Alternatives.');
    /*$this->dependencies = array('DSSDataset');*/
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';

    // Create a MigrateSource object.
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.nodes.connectivity_alternatives.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationNode('river_floodplain_connectivity');
    $this->addFieldMapping('field_creation_date', 'creation_date');
    $this->addFieldMapping('field_full_name', 'full_name');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('title', 'Name');
    $columns[1] = array('full_name', 'FullName');
    $columns[2] = array('creation_date', 'CreationDate');
    $columns[3] = array('body', 'Description');
    return $columns;
  }

}

/**
 * Class DSSScenario.
 */
class DSSScenario extends ImportBaseNodes {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Import DSS Scenarios.');
    $this->dependencies = array('DSSClimateScenario', 'DSSPopulationScenario',
      'DSSEnergyScenario',
    );
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';

    // Create a MigrateSource object.
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.nodes.scenario.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationNode('scenario');

    // Entity Reference Fields.
    $this->addFieldMapping('field_climate_scenario', 'climate_scenario')->sourceMigration('DSSClimateScenario');
    $this->addFieldMapping('field_population_scenario', 'population_scenario')->sourceMigration('DSSPopulationScenario');
    $this->addFieldMapping('field_energy_scenario', 'energy_scenario')->sourceMigration('DSSEnergyScenario');

  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('title', 'Name');
    $columns[1] = array('body', 'Description');
    $columns[2] = array('climate_scenario', 'ClimateScenario');
    $columns[3] = array('population_scenario', 'PopulationScenario');
    $columns[4] = array('energy_scenario', 'EnergyScenario');
    return $columns;
  }

}

/**
 * Class DSSIntegralPlan.
 */
class DSSIntegralPlan extends ImportBaseNodes {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Import DSS Integral Plans.');
    $this->dependencies = array(
      'DSSProductiveLandUseAlternatives',
      'DSSHydropowerAlternatives',
      'DSSConnectivityAlternatives',
      'DSSResource',
    );
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';

    // Create a MigrateSource object.
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.nodes.integral_plan.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationNode('integral_plan');

    // Entity Reference Fields.
    $this->addFieldMapping('field_prod_landuse_alternatives', 'productive_landuse_alternatives')->sourceMigration('DSSProductiveLandUseAlternatives');
    $this->addFieldMapping('field_hydropower_alternatives', 'hydropower_alternatives')->sourceMigration('DSSHydropowerAlternatives');
    $this->addFieldMapping('field_connectivity_alternatives', 'wetland_management_alternatives')->sourceMigration('DSSConnectivityAlternatives');

  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('title', 'Name');
    $columns[1] = array('body', 'Description');
    $columns[2] = array('productive_landuse_alternatives', 'ProductiveLandUseAlternatives');
    $columns[3] = array('hydropower_alternatives', 'HydropowerAlternatives');
    $columns[4] = array('wetland_management_alternatives', 'WetlandManagementAlternatives');
    return $columns;
  }

}

/**
 * Class DSSCaseStudy.
 */
class DSSCaseStudy extends ImportBaseNodes {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Import DSS Integral Plans.');
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';

    $this->dependencies = array('DSSIntegralPlan', 'DSSScenario');

    // Create a MigrateSource object.
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.nodes.case_study.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationNode('case_study');

    // Entity Reference Fields.
    $this->addFieldMapping('field_scenario', 'scenario')->sourceMigration('DSSScenario');
    $this->addFieldMapping('field_integral_plan', 'integral_plan')->sourceMigration('DSSIntegralPlan');

  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('title', 'Name');
    $columns[1] = array('body', 'Description');
    $columns[2] = array('scenario', 'Scenario');
    $columns[3] = array('integral_plan', 'IntegralPlan');
    return $columns;
  }

}

/**
 * Class DSSModel.
 */
class DSSModel extends ImportBaseNodes {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Import DSS Models.');
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';

    $this->dependencies = array('DSSDataset');

    // Create a MigrateSource object.
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.nodes.model.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationNode('model');

    // Added Fields.
    $this->addFieldMapping('field_code', 'code');

    // Entity Reference Fields.
    $this->addFieldMapping('field_required_variables', 'weap_required_variables')->sourceMigration('DSSDataset');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('title', 'Name');
    $columns[1] = array('code', 'ModelCode');
    $columns[2] = array('body', 'Description');
    $columns[3] = array('weap_required_variables', 'WEAPRequiredVariables');
    return $columns;
  }

  /**
   * Prepares rows for importing.
   */
  public function prepareRow($row) {
    $row->weap_required_variables = explode(",", $row->weap_required_variables);
    return TRUE;
  }

}

/**
 * Class DSSVariableVocabulary.
 */
class DSSVariableVocabulary extends ImportBaseNodes {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Import DSS Variable Vocabularies.');
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';

    /*$this->dependencies = array('DSSDataset');*/

    // Create a MigrateSource object.
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.nodes.variable_vocabulary.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationNode('variable_vocabulary');

    // Added Fields.
    $this->addFieldMapping('field_color_assigned', 'color');
    $this->addFieldMapping('field_value_range', 'values');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('title', 'Name');
    $columns[1] = array('color', 'Color');
    $columns[2] = array('values', 'Values');

    return $columns;
  }

}

/**
 * Class DSSLayers.
 */
class DSSLayers extends ImportBaseNodes {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Import DSS Layers.');
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';

    // Create a MigrateSource object.
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.nodes.layers.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationNode('layers');

    // Added Fields.
    $this->addFieldMapping('field_machine_name', 'machine_name');
    $this->addFieldMapping('field_spatialization', 'geo_type');
    $this->addFieldMapping('field_spatialization:geo_type', 'geo_type');
    $this->addFieldMapping('field_spatialization:wkt', 'wkt');
    $this->addFieldMapping('field_spatialization:lat', 'lat');
    $this->addFieldMapping('field_spatialization:lon', 'lon');
    $this->addFieldMapping('field_spatialization:left', 'left');
    $this->addFieldMapping('field_spatialization:top', 'top');
    $this->addFieldMapping('field_spatialization:right', 'right');
    $this->addFieldMapping('field_spatialization:bottom', 'bottom');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('title', 'Name');
    $columns[1] = array('body', 'Description');
    $columns[2] = array('machine_name', 'MachineName');
    $columns[3] = array('spatialization', 'GeoJson');

    return $columns;
  }

  /**
   * Prepares rows for importing.
   */
  public function prepareRow($row) {
    if (!empty($row->spatialization)) {
      $import_path = drupal_get_path('module', 'dss_import') . '/import/layers/';
      $geojson_file = $import_path . $row->spatialization;
      $geojson = file_get_contents($geojson_file);
      $geom = \geoPHP::load($geojson, 'json');
      $geofield_wkt = $geom->out('wkt');
      $wkt = array(
        'wkt' => $geofield_wkt,
      );
      $field_value = geofield_compute_values($wkt, 'wkt');
      $row->wkt = $field_value['wkt'];
      $row->geo_type = $field_value['geo_type'];
      $row->lat = $field_value['lat'];
      $row->lon = $field_value['lon'];
      $row->top = $field_value['top'];
      $row->bottom = $field_value['bottom'];
      $row->right = $field_value['right'];
      $row->left = $field_value['left'];
    }
    return TRUE;
  }

}

/**
 * Class DSSWaterBalanceTemplate.
 */
class DSSWaterBalanceTemplate extends ImportBaseNodes {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Import DSS Water Balance Template');
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';

    $this->dependencies = array('DSSDataset', 'DSSModel');

    // Create a MigrateSource object.
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.nodes.water.balance.template.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationNode('water_balance_template');

    // Added Fields.
    $this->addFieldMapping('field_water_balance_type', 'water_balance_type');
    $this->addFieldMapping('field_input:field_inputfac', 'input_factors');
    $this->addFieldMapping('field_output:field_outputfac', 'output_factors');
    $this->addFieldMapping('field_storage:field_storefac', 'storage_factors');
    $this->addFieldMapping('field_consumption:field_consfac', 'consumption_factors');

    // Entity Reference Fields.
    $this->addFieldMapping('field_model', 'model')->sourceMigration('DSSModel');
    $this->addFieldMapping('field_input:field_inputvar', 'input_variables')->sourceMigration('DSSDataset');
    $this->addFieldMapping('field_output:field_outputvar', 'output_variables')->sourceMigration('DSSDataset');
    $this->addFieldMapping('field_storage:field_storevar', 'storage_variables')->sourceMigration('DSSDataset');
    $this->addFieldMapping('field_consumption:field_consvar', 'consumption_variables')->sourceMigration('DSSDataset');

  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('title', 'Name');
    $columns[1] = array('body', 'Description');
    $columns[2] = array('water_balance_type', 'WaterBalanceType');
    $columns[3] = array('model', 'Model');
    $columns[4] = array('input_variables', 'InputVariables');
    $columns[5] = array('input_factors', 'InputFactors');
    $columns[7] = array('output_variables', 'OutputVariables');
    $columns[8] = array('output_factors', 'OutputFactors');
    $columns[10] = array('storage_variables', 'StorageVariables');
    $columns[11] = array('storage_factors', 'StorageFactors');
    $columns[13] = array('consumption_variables', 'ConsumptionVariables');
    $columns[14] = array('consumption_factors', 'ConsumptionFactors');
    $columns[16] = array('status', 'Status');
    return $columns;
  }

  /**
   * Prepares rows for importing.
   */
  public function prepareRow($row) {
    $row->model = explode(",", $row->model);
    $row->input_variables = explode(",", $row->input_variables);
    $row->input_factors = explode(",", $row->input_factors);
    $row->output_variables = explode(",", $row->output_variables);
    $row->output_factors = explode(",", $row->output_factors);
    $row->storage_variables = explode(",", $row->storage_variables);
    $row->storage_factors = explode(",", $row->storage_factors);
    $row->consumption_variables = explode(",", $row->consumption_variables);
    $row->consumption_factors = explode(",", $row->consumption_factors);
    return TRUE;
  }

}

/**
 * Class DSSRelation.
 */
class DSSRelation extends ImportBaseNodes {

  /**
   * DSSRelation constructor.
   *
   * @param array $arguments
   *   The constructor arguments.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Import DSS Relations.');
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';

    // Create a MigrateSource object.
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.nodes.relation.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationNode('relation');

    // Taxonomy Terms.
    $this->addFieldMapping('field_variable_category', 'variable_category');
    $this->addFieldMapping('field_persona', 'persona');
    $this->addFieldMapping('field_thematic_tree', 'thematic_tree');
    $this->addFieldMapping('field_type', 'type');
    $this->addFieldMapping('field_appropriate_spatial_scale', 'appropriate_spatial_scale');
    $this->addFieldMapping('field_numeric_scale', 'numeric_scale');
    $this->addFieldMapping('field_geometry', 'geometry');
    $this->addFieldMapping('field_temporality', 'temporality');

    // Text fields.
    $this->addFieldMapping('field_weap_code', 'weap_code');
    $this->addFieldMapping('field_weap_name', 'weap_name');
    $this->addFieldMapping('field_full_name', 'full_name');
    $this->addFieldMapping('field_range', 'range');
    $this->addFieldMapping('field_unit_of_measure', 'unit_of_measure');
    $this->addFieldMapping('field_dimensions', 'dimensions');

    // Added Fields.
    $this->addFieldMapping('field_subsystem', 'subsystem');
    $this->addFieldMapping('field_model_family', 'model_family');
    $this->addFieldMapping('field_node_category', 'node_category');
    $this->addFieldMapping('field_value_range_x', 'range_x');
    $this->addFieldMapping('field_value_range_y', 'range_y');
    $this->addFieldMapping('field_unit_of_measure_x', 'unit_of_measure_x');
    $this->addFieldMapping('field_unit_of_measure_y', 'unit_of_measure_y');
    $this->addFieldMapping('field_relationship_type', 'relationship_type');
    $this->addFieldMapping('field_relationship_parameters', 'relationship_parameters');

  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('variable_status', 'VariableStatus');
    $columns[1] = array('variable_category', 'VariableCategory');
    $columns[2] = array('weap_code', 'WEAPCode');
    $columns[3] = array('weap_name', 'WEAPName');
    $columns[4] = array('full_name', 'FullName');
    $columns[5] = array('title', 'Name');
    $columns[6] = array('body', 'Description');
    $columns[7] = array('thematic_tree', 'ThematicTree');
    $columns[8] = array('type', 'Type');
    $columns[9] = array('subsystem', 'Subsystem');
    $columns[10] = array('model_family', 'ModelFamily');
    $columns[11] = array('node_category', 'NodeCategory');
    $columns[12] = array('appropriate_spatial_scale', 'Spatialscale');
    $columns[13] = array('numeric_scale', 'NumericScale');
    $columns[14] = array('range_x', 'RangeX');
    $columns[15] = array('range_y', 'RangeY');
    $columns[16] = array('unit_of_measure_y', 'UnitX');
    $columns[17] = array('unit_of_measure_y', 'UnitY');
    $columns[18] = array('relationship_type', 'RelationType');
    $columns[19] = array('relationship_parameters', 'Parameters');
    $columns[20] = array('geometry', 'Geometry');
    $columns[21] = array('temporality', 'Temporality');
    /*$columns[22] = array('persona', 'Context');*/
    return $columns;
  }

  /*  public function prepareRow($row) {
  //    $row->interests = explode(", ", $row->interests);
  //    return TRUE;
  //  }*/
}

/**
 * Class DSSBasicPages.
 */
class DSSBasicPages extends ImportBaseNodes {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Import DSS Basic Pages.');
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';

    // Create a MigrateSource object.
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.nodes.page.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationNode('page');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('title', 'Name');
    $columns[1] = array('body', 'Description');

    return $columns;
  }

}

/**
 * Class DSSFAQs.
 */
class DSSFAQs extends ImportBaseNodes {

  /**
   * Public Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Import FAQ Pages.');
    $import_path = drupal_get_path('module', 'dss_import') . '/import/';

    // Create a MigrateSource object.
    $this->source = new MigrateSourceCSV($import_path . 'dss_import.nodes.faq.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationNode('faq');

    // Text fields.
    $this->addFieldMapping('field_detailed_question', 'detailed_question');
  }

  /**
   * CSV Columns mapping.
   */
  public function csvcolumns() {
    $columns[0] = array('title', 'Name');
    $columns[1] = array('detailed_question', 'DetailedQuestion');
    $columns[2] = array('body', 'Description');

    return $columns;
  }

}
