<?php
/**
 * @file
 * Declares our migrations.
 */

/**
 * Implements hook_migrate_api().
 */
function dss_import_migrate_api() {
  $api = array(
    'api' => 2,
    'groups' => array(
      'dss_import_files' => array(
        'title' => t('Default Files for DSS Magdalena'),
      ),
      'dss_import_taxonomy_terms' => array(
        'title' => t('Default Taxonomy Terms for DSS Magdalena'),
      ),
      'dss_import_nodes' => array(
        'title' => t('Default Nodes for DSS Magdalena'),
      ),
      'dss_import_users' => array(
        'title' => t('Default Users for DSS Magdalena'),
      ),
    ),
    'migrations' => array(
      // Files.
      'DSSResourceFilesMigration' => array(
        'class_name' => 'DSSResourceFilesMigration',
        'group_name' => 'dss_import_files',
      ),
      // Users.
      'DSSActiveUsers' => array(
        'class_name' => 'DSSActiveUsers',
        'group_name' => 'dss_import_users',
      ),
      // Taxonomy Terms.
      'DSSDPSIRTerms' => array(
        'class_name' => 'DSSDPSIRTerms',
        'group_name' => 'dss_import_taxonomy_terms',
      ),
      'DSSSWOTTerms' => array(
        'class_name' => 'DSSSWOTTerms',
        'group_name' => 'dss_import_taxonomy_terms',
      ),
      'DSSASSTerms' => array(
        'class_name' => 'DSSASSTerms',
        'group_name' => 'dss_import_taxonomy_terms',
      ),
      'DSSGeomTerms' => array(
        'class_name' => 'DSSGeomTerms',
        'group_name' => 'dss_import_taxonomy_terms',
      ),
      'DSSVarTypeTerms' => array(
        'class_name' => 'DSSVarTypeTerms',
        'group_name' => 'dss_import_taxonomy_terms',
      ),
      'DSSTemporalityTerms' => array(
        'class_name' => 'DSSTemporalityTerms',
        'group_name' => 'dss_import_taxonomy_terms',
      ),
      'DSSPOTerms' => array(
        'class_name' => 'DSSPOTerms',
        'group_name' => 'dss_import_taxonomy_terms',
      ),
      'DSSNumScaleTerms' => array(
        'class_name' => 'DSSNumScaleTerms',
        'group_name' => 'dss_import_taxonomy_terms',
      ),
      'DSSVarCategoryTerms' => array(
        'class_name' => 'DSSVarCategoryTerms',
        'group_name' => 'dss_import_taxonomy_terms',
      ),
      'DSSThematicTreeTerms' => array(
        'class_name' => 'DSSThematicTreeTerms',
        'group_name' => 'dss_import_taxonomy_terms',
      ),
      'DSSOrientation' => array(
        'class_name' => 'DSSOrientation',
        'group_name' => 'dss_import_taxonomy_terms',
      ),
      'DSSPublicationStatus' => array(
        'class_name' => 'DSSPublicationStatus',
        'group_name' => 'dss_import_taxonomy_terms',
      ),
      'DSSPublicationType' => array(
        'class_name' => 'DSSPublicationType',
        'group_name' => 'dss_import_taxonomy_terms',
      ),
      'DSSGeographicalArea' => array(
        'class_name' => 'DSSGeographicalArea',
        'group_name' => 'dss_import_taxonomy_terms',
      ),
      'DSSDigitalFormat' => array(
        'class_name' => 'DSSDigitalFormat',
        'group_name' => 'dss_import_taxonomy_terms',
      ),
      'DSSContentStyle' => array(
        'class_name' => 'DSSContentStyle',
        'group_name' => 'dss_import_taxonomy_terms',
      ),
      'DSSAccessLevel' => array(
        'class_name' => 'DSSAccessLevel',
        'group_name' => 'dss_import_taxonomy_terms',
      ),
      'DSSInstrumentsProvided' => array(
        'class_name' => 'DSSInstrumentsProvided',
      ),
      'DSSSubsystem' => array(
        'class_name' => 'DSSSubsystem',
        'group_name' => 'dss_import_taxonomy_terms',
      ),
      'DSSModelFamily' => array(
        'class_name' => 'DSSModelFamily',
        'group_name' => 'dss_import_taxonomy_terms',
      ),
      'DSSNodeCategory' => array(
        'class_name' => 'DSSNodeCategory',
        'group_name' => 'dss_import_taxonomy_terms',
      ),
      'DSSProjectPurpose' => array(
        'class_name' => 'DSSProjectPurpose',
        'group_name' => 'dss_import_taxonomy_terms',
      ),
      'DSSPrimaryPurpose' => array(
        'class_name' => 'DSSPrimaryPurpose',
        'group_name' => 'dss_import_taxonomy_terms',
      ),
      'DSSExecutionStatus' => array(
        'class_name' => 'DSSExecutionStatus',
        'group_name' => 'dss_import_taxonomy_terms',
      ),
      'DSSOperatingStatus' => array(
        'class_name' => 'DSSOperatingStatus',
      ),
      'DSSMaintenanceStatus' => array(
        'class_name' => 'DSSMaintenanceStatus',
        'group_name' => 'dss_import_taxonomy_terms',
      ),
      'DSSConstructiveTypology' => array(
        'class_name' => 'DSSConstructiveTypology',
        'group_name' => 'dss_import_taxonomy_terms',
      ),
      'DSSLegalPersonality' => array(
        'class_name' => 'DSSLegalPersonality',
        'group_name' => 'dss_import_taxonomy_terms',
      ),
      'DSSOrganizationType' => array(
        'class_name' => 'DSSOrganizationType',
        'group_name' => 'dss_import_taxonomy_terms',
      ),
      'DSSMeasurementMethods' => array(
        'class_name' => 'DSSMeasurementMethods',
        'group_name' => 'dss_import_taxonomy_terms',
      ),
      'DSSCampaignMeasurement' => array(
        'class_name' => 'DSSCampaignMeasurement',
        'group_name' => 'dss_import_taxonomy_terms',
      ),
      'DSSMeasurementStations' => array(
        'class_name' => 'DSSMeasurementStations',
        'group_name' => 'dss_import_taxonomy_terms',
      ),
      'DSSManualMeasurement' => array(
        'class_name' => 'DSSManualMeasurement',
        'group_name' => 'dss_import_taxonomy_terms',
      ),
      'DSSDescriptors' => array(
        'class_name' => 'DSSDescriptors',
        'group_name' => 'dss_import_taxonomy_terms',
      ),
      'DSSVariableStatus' => array(
        'class_name' => 'DSSVariableStatus',
        'group_name' => 'dss_import_taxonomy_terms',
      ),
      'DSSWEAPObjectType' => array(
        'class_name' => 'DSSWEAPObjectType',
      ),
      'DSSWaterBalanceCategory' => array(
        'class_name' => 'DSSWaterBalanceCategory',
        'group_name' => 'dss_import_taxonomy_terms',
      ),
      'DSSWaterBalanceStatus' => array(
        'class_name' => 'DSSWaterBalanceStatus',
        'group_name' => 'dss_import_taxonomy_terms',
      ),
      'DSSValueRange' => array(
        'class_name' => 'DSSValueRange',
        'group_name' => 'dss_import_taxonomy_terms',
      ),
      'DSSUnitMeasure' => array(
        'class_name' => 'DSSUnitMeasure',
        'group_name' => 'dss_import_taxonomy_terms',
      ),
      'DSSRelationshipType' => array(
        'class_name' => 'DSSRelationshipType',
        'group_name' => 'dss_import_taxonomy_terms',
      ),
      'DSSDimensions' => array(
        'class_name' => 'DSSDimensions',
        'group_name' => 'dss_import_taxonomy_terms',
      ),
      'DSSDecisionGroup' => array(
        'class_name' => 'DSSDecisionGroup',
        'group_name' => 'dss_import_taxonomy_terms',
      ),
      'DSSKeyAlternatives' => array(
        'class_name' => 'DSSKeyAlternatives',
        'group_name' => 'dss_import_taxonomy_terms',
      ),
      'DSSDecisionType' => array(
        'class_name' => 'DSSDecisionType',
        'group_name' => 'dss_import_taxonomy_terms',
      ),
      'DSSWeapLevel' => array(
        'class_name' => 'DSSWeapLevel',
        'group_name' => 'dss_import_taxonomy_terms',
      ),
      'DSSSLevel1' => array(
        'class_name' => 'DSSSLevel1',
        'group_name' => 'dss_import_taxonomy_terms',
      ),
      'DSSLandUseTypes' => array(
        'class_name' => 'DSSLandUseTypes',
        'group_name' => 'dss_import_taxonomy_terms',
      ),
      'DSSBaselineStatus' => array(
        'class_name' => 'DSSBaselineStatus',
        'group_name' => 'dss_import_taxonomy_terms',
      ),
      'DSSAggregationOperatorType' => array(
        'class_name' => 'DSSAggregationOperatorType',
        'group_name' => 'dss_import_taxonomy_terms',
      ),
      // Nodes.
      'DSSDataset' => array(
        'class_name' => 'DSSDataset',
        'group_name' => 'dss_import_nodes',
      ),
      'DSSResource' => array(
        'class_name' => 'DSSResource',
        'group_name' => 'dss_import_nodes',
      ),
      'DSSClimateScenario' => array(
        'class_name' => 'DSSClimateScenario',
        'group_name' => 'dss_import_nodes',
      ),
      'DSSPopulationScenario' => array(
        'class_name' => 'DSSPopulationScenario',
        'group_name' => 'dss_import_nodes',
      ),
      'DSSEnergyScenario' => array(
        'class_name' => 'DSSEnergyScenario',
        'group_name' => 'dss_import_nodes',
      ),
      'DSSProductiveLandUseAlternatives' => array(
        'class_name' => 'DSSProductiveLandUseAlternatives',
        'group_name' => 'dss_import_nodes',
      ),
      'DSSHydropowerAlternatives' => array(
        'class_name' => 'DSSHydropowerAlternatives',
        'group_name' => 'dss_import_nodes',
      ),
      'DSSConnectivityAlternatives' => array(
        'class_name' => 'DSSConnectivityAlternatives',
        'group_name' => 'dss_import_nodes',
      ),
      'DSSScenario' => array(
        'class_name' => 'DSSScenario',
        'group_name' => 'dss_import_nodes',
      ),
      'DSSIntegralPlan' => array(
        'class_name' => 'DSSIntegralPlan',
        'group_name' => 'dss_import_nodes',
      ),
      'DSSCaseStudy' => array(
        'class_name' => 'DSSCaseStudy',
        'group_name' => 'dss_import_nodes',
      ),
      'DSSModel' => array(
        'class_name' => 'DSSModel',
        'group_name' => 'dss_import_nodes',
      ),
      'DSSVariableVocabulary' => array(
        'class_name' => 'DSSVariableVocabulary',
        'group_name' => 'dss_import_nodes',
      ),
      'DSSWaterBalanceTemplate' => array(
        'class_name' => 'DSSWaterBalanceTemplate',
        'group_name' => 'dss_import_nodes',
      ),
      'DSSRelation' => array(
        'class_name' => 'DSSRelation',
        'group_name' => 'dss_import_nodes',
      ),
      'DSSIrrigationDistrict' => array(
        'class_name' => 'DSSIrrigationDistrict',
        'group_name' => 'dss_import_nodes',
      ),
      'DSSTypeofWork' => array(
        'class_name' => 'DSSTypeofWork',
        'group_name' => 'dss_import_nodes',
      ),
      'DSSSpecificPurpose' => array(
        'class_name' => 'DSSSpecificPurpose',
        'group_name' => 'dss_import_nodes',
      ),
      'DSSLayers' => array(
        'class_name' => 'DSSLayers',
        'group_name' => 'dss_import_nodes',
      ),
      'DSSBasicPages' => array(
        'class_name' => 'DSSBasicPages',
        'group_name' => 'dss_import_nodes',
      ),
      'DSSFAQs' => array(
        'class_name' => 'DSSFAQs',
        'group_name' => 'dss_import_nodes',
      ),
    ),
  );
  return $api;
}
