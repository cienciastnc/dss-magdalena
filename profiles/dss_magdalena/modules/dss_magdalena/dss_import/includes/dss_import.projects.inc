<?php
/**
 * @file
 * Imports Projects data from the Resources (Cajas de Datos).
 */

use Drupal\dss_magdalena\DSS\Entity\Ficha\SimaFichaHydropowerPlantDam;
use Drupal\dss_magdalena\DSS\Entity\Ficha\SimaFichaRorHydropowerPlant;
use Drupal\dss_magdalena\DSS\Entity\SimaController;
use Drupal\dss_magdalena\DSS\Entity\SimaDefaultCaseStudy;
use Drupal\dss_magdalena\DSS\Utils\SimaImportProjects;
use Drupal\dss_magdalena\DSS\Utils\SimaImportController;
use Drupal\dss_magdalena\DSS\Entity\SimaCaseStudy;
use Drupal\dss_magdalena\DSS\Entity\Ficha\SimaFichaWetlandsManagement;
use Drupal\dss_magdalena\DSS\Entity\SimaWetlandsManagementVariation;
use Drupal\dss_magdalena\DSS\Entity\SimaAlternative;
use Drupal\dss_magdalena\DSS\Entity\SimaProductiveLandUseProjectVariant;
use Drupal\dss_magdalena\DSS\Entity\SimaCatchments;
use League\Csv\Reader;

/**
 * Generates the list of projects from the resources.
 */
function dss_import_generate_projects_data_from_resources() {
  // Sets the default Case Studies.
  SimaDefaultCaseStudy::setDefinition();

  $projects_type = [
    SimaImportProjects::TYPE_HYDROPOWER_PLANT_OR_DAM,
    SimaImportProjects::TYPE_ROR_HYDROPOWER_PLANT,
    SimaImportProjects::TYPE_WETLAND_MANAGEMENT_PROJ,
    // SimaImportProjects::TYPE_PRODUCTIVE_LAND_USE_PROJ,.
  ];

  // Define the Case Study where these projects will be imported.
  $case_study = SimaDefaultCaseStudy::loadExtreme();
  $hydropower_alternative = $case_study->getHydropowerAlternatives();
  $wetland_alternative = $case_study->getWetlandManagementAlternatives();
  // $productive_landuse_alternative =
  // $case_study->getProductiveLandUseAlternatives();
  // Iterating over all the existent projects for each type.
  foreach ($projects_type as $type) {
    $import = SimaImportController::load($type);
    $target_bundle = $import->getTargetBundle();

    // First generate the list of Hydropower Plants or Dams.
    $projects = $import->getListOfItems();

    // Generate the joined File.
    $import->generateProjectCsvFile();

    foreach ($projects as $project) {
      /** @var \Drupal\dss_magdalena\DSS\Entity\Ficha\SimaFichaInterface $ficha */
      if ($ficha = $import->importFichaProject($project)) {
        $success = 'ok';

        // Now that the project is imported, we have to assign it to the
        // appropriate alternative in the case study.
        switch ($type) {
          case SimaImportProjects::TYPE_HYDROPOWER_PLANT_OR_DAM:
          case SimaImportProjects::TYPE_ROR_HYDROPOWER_PLANT:
            $hydropower_alternative->addFichaProject($ficha);
            break;

          case SimaImportProjects::TYPE_WETLAND_MANAGEMENT_PROJ:
            $wetland_alternative->addFichaProject($ficha);
            break;

          case SimaImportProjects::TYPE_PRODUCTIVE_LAND_USE_PROJ:
            // $productive_landuse_alternative->addFichaProject($ficha);
            break;

        }

        // Print message.
        drush_log(dt('Import Ficha/Project (type = !type, nid = !nid): !title.', array(
          '!type' => $target_bundle,
          '!title' => !empty($ficha) ? $ficha->getDrupalEntity()->label() : t('Unknown'),
          '!nid' => !empty($ficha) ? $ficha->getId() : t('None'),
        )), $success);
      }
      else {
        $success = 'failed';
      }

    }
  }

  // By this time all projects have been created, now save the development
  // alternatives for the default case study by linking all projects to them.
  $hydropower_alternative->save();
  // $productive_landuse_alternative->save();
  $wetland_alternative->save();

}

/**
 * Generate List of Plants for Hydropower Alternatives.
 */
function dss_import_default_hydropower_alternatives() {
  $path = DRUPAL_ROOT . base_path() . drupal_get_path('module', 'dss_import') . '/import/default_data/alternatives/hydropower/';
  $alternatives = new SimaController();
  /** @var \Drupal\dss_magdalena\DSS\Entity\Alternatives\SimaHydropowerAlternative[] $hydropower_alternatives */
  $hydropower_alternatives = $alternatives->listEntities('hydropower_alternatives');
  foreach ($hydropower_alternatives as $alternative) {
    // Fill out reservoirs.
    $file_reservoirs_uri = $path . $alternative->getTitle() . '-reservoirs.txt';
    $file_reservoirs = file($file_reservoirs_uri);
    if (!empty($file_reservoirs)) {
      array_walk($file_reservoirs, function(&$n) {
        $n = trim($n);
      });
      $reservoir_ids = [];
      $bundle = SimaFichaHydropowerPlantDam::BUNDLE;
      $projects = SimaFichaHydropowerPlantDam::loadListByTitles($file_reservoirs, $bundle);
      foreach ($projects as $reservoir) {
        $reservoir_ids[] = $reservoir->getId();
      }
      $alternative->set(SimaAlternative::HYDROPOWER_PROJECTS, array_unique($reservoir_ids));
    }

    $file_ror_uri = $path . $alternative->getTitle() . '-ror.txt';
    $file_ror = file($file_ror_uri);
    if (!empty($file_ror)) {
      array_walk($file_ror, function(&$n) {
        $n = trim($n);
      });
      $ror_ids = [];
      $bundle = SimaFichaRorHydropowerPlant::BUNDLE;
      $projects = SimaFichaRorHydropowerPlant::loadListByTitles($file_ror, $bundle);
      foreach ($projects as $ror) {
        $ror_ids[] = $ror->getId();
      }
      $alternative->set(SimaAlternative::ROR_HYDROPOWER_PROJECTS, array_unique($ror_ids));
    }
    $alternative->save();
    drush_log(dt('Added projects into Hydropower Alternative (nid = !nid): !title.', array(
      '!title' => $alternative->getTitle(),
      '!nid' => $alternative->getId(),
    )), 'ok');
  }
}

/**
 * Creates default variations for Wetlands Management Projects.
 */
function dss_import_default_wetlands_management_project_variations() {
  // Create variations of Productive Land Use Projects.
  $csv_file = DRUPAL_ROOT . base_path() . drupal_get_path('module', 'dss_import') . '/import/default_data/alternatives/wetlands/dss_import_wetlands_management.csv';
  $csv_file = Reader::createFromPath($csv_file);
  $headers = $csv_file->fetchOne();
  // If filter is not defined, then do not filter results.
  module_load_include('inc', 'dss_engine', 'src/DSS/inc/sima_import_weap_databox_csv-filters');
  $filter = isset($filter) ? $filter : 'filterByNoFilter';
  $results = $csv_file
    ->stripBom(FALSE)
    ->addFilter($filter)
    ->fetchAssoc($headers);

  $data = [];
  foreach ($results as $key => $result) {
    if ($key == 0) {
      continue;
    }
    $alternative = $result['Alternative'];
    $data[$alternative][] = (array) $result;
  }
  // We have the Data array, now fill the variations.
  foreach ($data as $alternative => $projects) {
    if ($wetland_alternative = SimaAlternative::loadByTitle($alternative, SimaAlternative::WETLANDS_MANAGEMENT_ALTERNATIVE)) {
      foreach ($projects as $project) {
        if ($wetland_management_proj = SimaFichaWetlandsManagement::loadByTitle($project['Project'], SimaFichaWetlandsManagement::BUNDLE)) {
          $title = $project['Project'] . ' - ' . $project['Variant'];
          $variation = SimaWetlandsManagementVariation::newVariation();
          $variation->setWetlandsManagementProject($wetland_management_proj);
          $variation->setTitle($title);
          $variation->setModifiedValue($project['Value']);
          $variation->setBody($project['Description']);
          $variation->save();
          $wetland_alternative->add(SimaAlternative::WETLAND_MANAGEMENT_PROJECTS, $variation->getDrupalEntity()->value());
          // Print message.
          drush_log(dt('Created Project Variation (nid = !nid): !title.', array(
            '!title' => $variation->getTitle(),
            '!nid' => $variation->getId(),
          )), 'ok');
        }
      }
      $wetland_alternative->save();
    }
  }
}

/**
 * Generates Project Variations.
 */
function dss_import_generate_project_variations() {
  // Loading Wetlands Management Alternative for "Caso Linea Base".
  $wetland_alternative = FALSE;
  if ($case = SimaDefaultCaseStudy::loadBaseline()) {
    $wetland_alternative = $case->getWetlandManagementAlternatives();
    $prod_land_use_base = $case->getProductiveLandUseAlternatives();

    // Populate bseline power plants.
    $case->populateBaselinePowerPlants();
  }

  // Create variations of Wetlands Management Projects.
  $wetlands_management_projects = SimaFichaWetlandsManagement::listEntities();
  foreach ($wetlands_management_projects as $wetland_management_proj) {

    // Setting up "Variante NULA".
    $title = $wetland_management_proj->getTitle() . ' - Variante NULA';
    $value = 0.00;
    $body = 'Es la situacion Linea Base sin aun drenaje; por tanto no hay conexion que pueda inundar la zona';
    $variation_exagera = SimaWetlandsManagementVariation::newVariation();
    $variation_exagera->setWetlandsManagementProject($wetland_management_proj);
    $variation_exagera->setTitle($title);
    $variation_exagera->setModifiedValue($value);
    $variation_exagera->setBody($body);
    $variation_exagera->save();
    // Print message.
    drush_log(dt('Created Project Variation (nid = !nid): !title.', array(
      '!title' => $variation_exagera->getTitle(),
      '!nid' => $variation_exagera->getId(),
    )), 'ok');

    // Adding variation to Wetland Management Alternative.
    if ($wetland_alternative) {
    // @codingStandardsIgnoreStart
    //  $wetland_alternative->add(SimaAlternative::WETLAND_MANAGEMENT_PROJECTS, $variation_exagera->getDrupalEntity()->value());
    // @codingStandardsIgnoreEnd
    }
  }
  // Add variation NULA to wetlands management project.
  if ($wetland_alternative) {
    // @codingStandardsIgnoreStart
    // $wetland_alternative->save();
    // @codingStandardsIgnoreEnd
  }

  // Create variations of Productive Land Use Projects.
  $prod_landuse_variations = [
    'C 2123704' => [
      'title' => 'Variante NULA',
      'body' => 'Ninguna modificacion al estado actual.',
      'type' => 'restoration',
      'land_use' => [],
    ],
  ];

  // Loading Productive Land Use for 'Caso Linea Base'.
  if ($case = SimaCaseStudy::loadCaseByTitle('Caso Extremo')) {
    $prod_land_use_extreme = $case->getProductiveLandUseAlternatives();
  }

  foreach ($prod_landuse_variations as $name => $pl_variation) {
    if ($catchment = SimaCatchments::loadByName($name)) {
      $variation = SimaProductiveLandUseProjectVariant::newProductiveLandUseProjectVariant();
      $variation->setTitle($catchment->getTitle() . ' - ' . $pl_variation['title']);
      $variation->setBody($pl_variation['body']);
      $variation->setLandUseProjectType($pl_variation['type']);
      $variation->setCatchment($catchment);
      $variation->setCatchmentArea($catchment->getCatchmentArea());
      $landuses = $catchment->getSimpleLandUsePercentages();
      foreach ($landuses as $key => $landuse) {
        $land_type = $landuse->getLandUseType()->getName();
        if (in_array($land_type, array_keys($pl_variation['land_use']))) {
          $percentage = $pl_variation['land_use'][$land_type];
          $landuses[$key]->setPercentage($percentage);
        }
      }
      $variation->setLandUsePercentage($landuses);
      $variation->save();
      drush_log(dt('Created Project Variation (nid = !nid): !title.', array(
        '!title' => $variation->getTitle(),
        '!nid' => $variation->getId(),
      )), 'ok');

      // Saving variation into the productive land use alternative.
      if ($prod_land_use_extreme) {
        // @codingStandardsIgnoreStart
        // $prod_land_use_extreme->add(SimaAlternative::PRODUCTIVE_LANDUSE_PROJECTS, $variation->getDrupalEntity()->value());
        // @codingStandardsIgnoreEnd
      }
      if ($prod_land_use_base && $name == 'C 2123704') {
        // @codingStandardsIgnoreStart
        // $prod_land_use_base->add(SimaAlternative::PRODUCTIVE_LANDUSE_PROJECTS, $variation->getDrupalEntity()->value());
        // @codingStandardsIgnoreEnd
      }
    }
  }
  // @codingStandardsIgnoreStart
  if ($prod_land_use_extreme) {
    // $prod_land_use_extreme->save();
  }
  if ($prod_land_use_base) {
    // $prod_land_use_base->save();
  }
  // @codingStandardsIgnoreEnd
}

/**
 * Import all project variations and assign them to the respective alternative.
 */
function dss_import_productive_land_use_project_variations() {
  // Create variations of Productive Land Use Projects.
  $csv_file = drupal_get_path('module', 'dss_import') . '/import/default_data/alternatives/productive_landuse/dss_import_productive_landuse_variations.csv';
  $csv_file = Reader::createFromPath($csv_file);
  $headers = $csv_file->fetchOne();
  // If filter is not defined, then do not filter results.
  module_load_include('inc', 'dss_engine', 'src/DSS/inc/sima_import_weap_databox_csv-filters');
  $filter = isset($filter) ? $filter : 'filterByNoFilter';
  $results = $csv_file
    ->stripBom(FALSE)
    ->addFilter($filter)
    ->fetchAssoc($headers);

  $data = [];
  foreach ($results as $key => $result) {
    if ($key == 0) {
      continue;
    }
    $alternative = $result['Alternative'];
    $data[$alternative][] = (array) $result;
  }

  foreach ($data as $alternative_key => $variations) {
    if ($alternative = SimaAlternative::loadByTitle($alternative_key, SimaAlternative::PRODUCTIVE_LANDUSE_ALTERNATIVE)) {
      foreach ($variations as $pl_variation) {
        if ($catchment = SimaCatchments::loadByName($pl_variation['Catchment'])) {
          $variation = SimaProductiveLandUseProjectVariant::newProductiveLandUseProjectVariant();
          $variation->setTitle($catchment->getTitle() . ' - ' . $pl_variation['Title']);
          $variation->setBody($pl_variation['Description']);
          $variation->setLandUseProjectType($pl_variation['Type']);
          $variation->setCatchment($catchment);
          $variation->setCatchmentArea($catchment->getCatchmentArea());
          $landuses = $catchment->getSimpleLandUsePercentages();
          foreach ($landuses as $key => $landuse) {
            $land_type = $landuse->getLandUseType()->getName();
            if (isset($pl_variation[$land_type])) {
              $percentage = $pl_variation[$land_type];
              $landuses[$key]->setPercentage($percentage);
            }
          }
          $variation->setLandUsePercentage($landuses);
          $variation->save();
          drush_log(dt('Created Project Variation (nid = !nid): !title.', array(
            '!title' => $variation->getTitle(),
            '!nid' => $variation->getId(),
          )), 'ok');
          $alternative->add(SimaAlternative::PRODUCTIVE_LANDUSE_PROJECTS, $variation->getDrupalEntity()
            ->value());
        }
      }
      $alternative->save();
      drush_log(dt('Saved Productive Land Use Alternative (nid = !nid): !title.', array(
        '!title' => $alternative->getTitle(),
        '!nid' => $alternative->getId(),
      )), 'ok');
    }
  }
}

/**
 * Imports Key Assumptions.
 */
function dss_import_key_assumptions_from_csv_master_file() {
  $import = new SimaImportProjects();
  if ($fid = $import->extractKeyAssumptions()) {
    drush_log(dt('Import Key Assumptions to file (fid = !file).', array(
      '!file' => $fid,
    )), 'ok');
  }
}
