<?php
/**
 * @file
 * Sets Model Configurations and Execution.
 */

use Drupal\dss_magdalena\DSS\Entity\SimaCaseStudy;
use Drupal\dss_magdalena\DSS\Entity\File\SimaElohaQEcoTable;
use Drupal\dss_magdalena\DSS\Entity\File\SimaStreamslineTopologyFile;
use Drupal\dss_magdalena\DSS\Entity\File\SimaTerritorialFootprintFile;
use Drupal\dss_magdalena\DSS\Entity\SimaDefaultCaseStudy;
use Drupal\dss_magdalena\DSS\Queue\SimaImportQueue;
use Drupal\dss_magdalena\DSS\Entity\SimaModel;

/**
 * Sets the variables and configurations needed for WEAP Model.
 */
function dss_import_model_configuration_weap() {
  variable_set('webdav_server', '181.143.190.60');
  variable_set('webdav_username', 'webdav');
  variable_set('webdav_password', 'dssTNCc0lombia');
  variable_set('webdav_ssl_verifypeer', 0);
  variable_set('webdav_enable_2fa', 'no');
  variable_set('webdav_ssl_certificate', '');
  variable_set('webdav_ssl_certificate_password', '');
  variable_set('webdav_impex_path', '/WebDAV');
  drush_log(dt('Successfully configured WEAP model.', array()), 'ok');
}

/**
 * Sets the variables and configurations needed for ELOHA Model.
 */
function dss_import_model_configuration_eloha() {
  // Select "Caso Linea Base" as the base case for ELOHA.
  if ($base_case = SimaDefaultCaseStudy::loadBaseline()) {
    variable_set('dss_engine_eloha_ref_case_study_cid', $base_case->getId());
  }

  $uri = drupal_get_path('module', 'dss_import') . '/import/models_configuration/Q_ECO_TABLE_JCP_test.csv';
  $file = file_save((object) array(
    'filename' => 'Q_ECO_TABLE_JCP_test.csv',
    'uri' => $uri,
    'status' => FILE_STATUS_PERMANENT,
    'filemime' => file_get_mimetype($uri),
  ));
  variable_set(SimaElohaQEcoTable::VAR_QECO_TABLE, $file->fid);

  $ws = 0.5;
  variable_set('dss_engine_eloha_parameter_ws', $ws);
  $wg = 0.5;
  variable_set('dss_engine_eloha_parameter_wg', $wg);
  drush_log(dt('Successfully configured ELOHA model.', array()), 'ok');
}

/**
 * Sets the variables and configurations needed for Fragmentation Model.
 */
function dss_import_model_configuration_fragmentation() {
  $uri = drupal_get_path('module', 'dss_import') . '/import/models_configuration/magdalena_streamsline_topology.csv';
  $file = file_save((object) array(
    'filename' => 'magdalena_streamsline_topology.csv',
    'uri' => $uri,
    'status' => FILE_STATUS_PERMANENT,
    'filemime' => file_get_mimetype($uri),
  ));
  variable_set(SimaStreamslineTopologyFile::VAR_STREAMSLINE_TOPOLOGY_FILE, $file->fid);
  drush_log(dt('Successfully configured Fragmentation model.', array()), 'ok');
}

/**
 * Sets the variables and configurations needed for Territorial Footprint Model.
 */
function dss_import_model_configuration_territorial_footprint() {
  $uri = drupal_get_path('module', 'dss_import') . '/import/models_configuration/Analisis_Huella_Embalses_Proyectados.csv';
  $file = file_save((object) array(
    'filename' => 'Analisis_Huella_Embalses_Proyectados.csv',
    'uri' => $uri,
    'status' => FILE_STATUS_PERMANENT,
    'filemime' => file_get_mimetype($uri),
  ));
  variable_set(SimaTerritorialFootprintFile::VAR_TERRITORIAL_FOOTPRINT_FILE, $file->fid);
  drush_log(dt('Successfully configured Territorial Footprint model.', array()), 'ok');
}

/**
 * Sets the variables and configurations needed for DOR-H Model.
 */
function dss_import_model_configuration_dor_h() {
  // Same as Fragmentation.
  drush_log(dt('Successfully configured DOR-H model.', array()), 'ok');
}

/**
 * Executes and queues WEAP Outputs for a particular case study.
 *
 * @param \Drupal\dss_magdalena\DSS\Entity\SimaCaseStudy $case
 *   The Case Study.
 */
function dss_import_weap_import_resources(SimaCaseStudy $case) {
  $source_dir = drupal_get_path('module', 'dss_import') . '/import/weap_outputs/';
  if ($case->getDefaultCaseStudy()->isExtreme()) {
    $source_dir .= 'extreme';
  }
  elseif ($case->getDefaultCaseStudy()->isBaseline()) {
    $source_dir .= 'baseline';
  }

  $case->importWeapOutputsFromDirectory($source_dir);
  $case->setModelingStatus(SimaModel::MODEL_WEAP, SimaModel::STATUS_PROCESSING);

  // Print results.
  drush_log(dt('Queuing case ouputs from "!model" model for  !case', array(
    '!model' => SimaModel::MODEL_WEAP,
    '!case' => $case->getTitle(),
  )), 'ok');
}

/**
 * Saves WEAP outputs for a particular case study.
 *
 * @param \Drupal\dss_magdalena\DSS\Entity\SimaCaseStudy $case
 *   The Case Study.
 *
 * @return bool
 *   TRUE if executed the import, FALSE otherwise.
 */
function dss_import_saving_imported_ouputs_weap(SimaCaseStudy $case) {
  if ($case->isProcessingWeapModelingStatus()) {
    // Implementing Batch API.
    $import_queue = SimaImportQueue::QUEUE_NAME;
    $queue = DrupalQueue::get($import_queue);
    $defined_queues = queue_ui_defined_queues();
    $batch = $defined_queues[$import_queue]['batch'];
    $batch['operations'][0][1] = [$queue, FALSE];
    $batch['file'] = drupal_get_path('module', 'dss_engine') . '/dss_engine.queue.inc';
    $batch['progressive'] = FALSE;
    batch_set($batch);
    // Start processing the batch operations.
    drush_backend_batch_process();
    return TRUE;
  }
  return FALSE;
}
