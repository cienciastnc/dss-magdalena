<?php
/**
 * @file
 * Defines the functions to import layers metadata.
 *
 * Among them are: River Reaches, Demand Sites and Catchments, etc.
 */

use Drupal\dss_magdalena\DSS\Utils\SimaImportRiverReaches;
use Drupal\dss_magdalena\DSS\Entity\SimaRiverReaches;
use Drupal\dss_magdalena\DSS\Utils\SimaImportCatchments;
use Drupal\dss_magdalena\DSS\Entity\SimaCatchments;

/**
 * Imports River Reaches.
 */
function dss_import_river_reaches() {
  $river_reaches = new SimaImportRiverReaches();
  $objects = $river_reaches->extractObjects();
  $geojson_features = $river_reaches->getGeoJsonFeatures();
  foreach ($objects as $key => $object) {
    if ($entity = $river_reaches->import($objects, $geojson_features, $key)) {
      $success = 'ok';
    }
    else {
      $success = 'failed';
    }
    // Print message.
    drush_log(dt('Import Branch ID = !branch_id (type = !type): !title.', array(
      '!branch_id' => $entity->get(SimaRiverReaches::FIELD_BRANCH_ID),
      '!type' => SimaRiverReaches::BUNDLE,
      '!title' => !empty($entity) ? $entity->getDrupalEntity()->label() : t('Unknown'),
    )), $success);
  }
}

/**
 * Imports Demand Sites and Catchments.
 */
function dss_import_catchments() {
  $catchments = new SimaImportCatchments();
  $objects = $catchments->extractObjects();
  $geojson_features = $catchments->getGeoJsonFeatures();
  foreach ($objects as $key => $object) {
    if ($entity = $catchments->import($objects, $geojson_features, $key)) {
      $success = 'ok';
    }
    else {
      $success = 'failed';
    }
    // Print message.
    drush_log(dt('Import (type = !type): !title.', array(
      '!type' => SimaCatchments::BUNDLE,
      '!title' => !empty($entity) ? $entity->getDrupalEntity()->label() : t('Unknown'),
    )), $success);
  }
}
