<?php
/**
 * @file
 * Functions to deal with interface options.
 */

use Drupal\dss_magdalena\DSS\Entity\SimaResource;

/**
 * Helper function to get button visualization access for graphs.
 *
 * @param int $node_list
 *   Id of the node from which the type of access will be obtained.
 *
 * @return int
 *   int value that describe the graph access type.
 *   0-none, 1-time, 2-space, 3-both buttons
 */
function dss_visualizations_button_access($node_list) {
  // Set default return value.
  $return_val = 0;

  // Get machine name of the first element passed, others should be the same.
  $node  = explode(",", $node_list)[0];
  $resource = SimaResource::load($node);

  // Sanity check - Return no buttons if the resource doesn't exist.
  if (!$resource) {
    return $return_val;
  }

  // Get the machine of the variable.
  $dataset = $resource->getDataset();
  if (!$dataset) {
    $return_val = 0;
  }
  else {
    $machine_name = $dataset->getMachineName();
    $temporality = strtolower($dataset->getTemporality());
    $geometry = strtolower($dataset->getGeometry());
    $numeric_scale = strtolower($dataset->getNumericScale());

    // Check geometry property.
    if ((strcmp($geometry, "none") == 0) || (strcmp($numeric_scale, "none") == 0)) {
      $return_val = 0;
    }
    else {
      // Check Numeric Scale property.
      if ((strcmp($numeric_scale, "cualitativa nominal") == 0) || (strcmp($numeric_scale, "cualitativa ordinal") == 0)) {
        $return_val = 0;
      }
      else {
        // Check Temporality property.
        if (strcmp($temporality, "none") == 0) {
          $return_val = 2;
        }
        else {
          $return_val = 3;
        }
      }
    }
  }
  return $return_val;
}

/**
 * Function updates dkan_datastore_download_access for visualizacion graphs.
 *
 * @param string $action
 *   Action to be taken.
 * @param string | int $resource
 *   Resource from which the access will be obtained.
 * @param string $visualization_type
 *   Visualization graph type.
 *
 * @return bool
 *   Boolean value.
 */
function dss_datastore_download_access($action, $resource, $visualization_type = NULL) {
  $node = is_object($resource) ? $resource : node_load($resource);

  // Check if the visualization menu button should be displayed.
  if ($visualization_type !== NULL) {
    $access_button_value = dss_visualizations_button_access($node->nid);
    if (strcmp($visualization_type, "time-aggregation") === 0) {
      if (!in_array($access_button_value, array(1, 3))) {
        return FALSE;
      }
    }
    elseif (strcmp($visualization_type, "space-aggregation") === 0) {
      if (!in_array($access_button_value, array(2, 3))) {
        return FALSE;
      }
    }
  }

  // Call normal DKAN access.
  return dkan_datastore_download_access($action, $resource);
}
