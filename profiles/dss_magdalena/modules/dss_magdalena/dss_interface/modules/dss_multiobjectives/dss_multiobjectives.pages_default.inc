<?php
/**
 * @file
 * dss_multiobjectives.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function dss_multiobjectives_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'multi_objetivos';
  $page->task = 'page';
  $page->admin_title = 'Multi Objetivos';
  $page->admin_description = 'Multi Objetivos';
  $page->path = 'multi-objectives';
  $page->access = array();
  $page->menu = array(
    'type' => 'none',
    'title' => '',
    'name' => 'navigation',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_multi_objetivos__panel';
  $handler->task = 'page';
  $handler->subtask = 'multi_objetivos';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Multi-Objetives Panel',
    'no_blocks' => 0,
    'pipeline' => 'ipe',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
  );
  $display = new panels_display();
  $display->layout = 'radix_brenham';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'left' => NULL,
      'right' => NULL,
      'bottom' => NULL,
      'header' => NULL,
      'sidebar' => NULL,
      'contentmain' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '096df111-feb2-4c7d-911c-758584861d24';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'page_multi_objetivos__panel';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-36d9c177-544e-41ca-8e3e-87647d24af6f';
  $pane->panel = 'contentmain';
  $pane->type = 'block';
  $pane->subtype = 'dss_interface-sima_evaluation';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '36d9c177-544e-41ca-8e3e-87647d24af6f';
  $display->content['new-36d9c177-544e-41ca-8e3e-87647d24af6f'] = $pane;
  $display->panels['contentmain'][0] = 'new-36d9c177-544e-41ca-8e3e-87647d24af6f';
  $pane = new stdClass();
  $pane->pid = 'new-2ee8e4d7-4f5f-42ba-95f1-4d8fea8d7fb3';
  $pane->panel = 'contentmain';
  $pane->type = 'block';
  $pane->subtype = 'dss_interface-sima_objectives';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '2ee8e4d7-4f5f-42ba-95f1-4d8fea8d7fb3';
  $display->content['new-2ee8e4d7-4f5f-42ba-95f1-4d8fea8d7fb3'] = $pane;
  $display->panels['contentmain'][1] = 'new-2ee8e4d7-4f5f-42ba-95f1-4d8fea8d7fb3';
  $pane = new stdClass();
  $pane->pid = 'new-9eca2c64-1f5d-4b4b-8048-580515192919';
  $pane->panel = 'contentmain';
  $pane->type = 'views';
  $pane->subtype = 'dkan_datasets';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_pager_settings' => 0,
    'use_pager' => 1,
    'nodes_per_page' => '10',
    'pager_id' => '1',
    'offset' => '0',
    'more_link' => 0,
    'feed_icons' => 0,
    'panel_args' => 0,
    'link_to_view' => 0,
    'args' => '',
    'url' => '',
    'display' => 'default',
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'sima-hidden',
  );
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '9eca2c64-1f5d-4b4b-8048-580515192919';
  $display->content['new-9eca2c64-1f5d-4b4b-8048-580515192919'] = $pane;
  $display->panels['contentmain'][2] = 'new-9eca2c64-1f5d-4b4b-8048-580515192919';
  $pane = new stdClass();
  $pane->pid = 'new-a1c11436-2a3a-43b9-bba6-dab458b982c3';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-rNap6LsW0S2bWWxIsB1OD8d1u0UEMrZQ';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Case Study',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'a1c11436-2a3a-43b9-bba6-dab458b982c3';
  $display->content['new-a1c11436-2a3a-43b9-bba6-dab458b982c3'] = $pane;
  $display->panels['sidebar'][0] = 'new-a1c11436-2a3a-43b9-bba6-dab458b982c3';
  $pane = new stdClass();
  $pane->pid = 'new-277d285f-1809-4bb2-8913-2daea2949287';
  $pane->panel = 'sidebar';
  $pane->type = 'views';
  $pane->subtype = 'list_of_objectives';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_pager_settings' => 0,
    'use_pager' => 0,
    'nodes_per_page' => '5',
    'pager_id' => '0',
    'offset' => '0',
    'more_link' => 0,
    'feed_icons' => 0,
    'panel_args' => 0,
    'link_to_view' => 0,
    'args' => '',
    'url' => '',
    'display' => 'block',
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '277d285f-1809-4bb2-8913-2daea2949287';
  $display->content['new-277d285f-1809-4bb2-8913-2daea2949287'] = $pane;
  $display->panels['sidebar'][1] = 'new-277d285f-1809-4bb2-8913-2daea2949287';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-277d285f-1809-4bb2-8913-2daea2949287';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['multi_objetivos'] = $page;

  return $pages;

}
