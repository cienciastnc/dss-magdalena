<?php
/**
 * @file
 * dss_frontpage.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function dss_frontpage_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'front_page';
  $page->task = 'page';
  $page->admin_title = 'Front Page';
  $page->admin_description = 'Front Page';
  $page->path = 'frontpage';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_front_page__panel';
  $handler->task = 'page';
  $handler->subtask = 'front_page';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => t('Front Page'),
    'no_blocks' => 0,
    'pipeline' => 'ipe',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
  );
  $display = new panels_display();
  $display->layout = 'full_width';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top-first' => NULL,
      'top-second' => NULL,
      'middle' => NULL,
      'bottom-first' => NULL,
      'bottom-second' => NULL,
      'bottom-third' => NULL,
      'footer' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '7617fc73-8c2a-4300-98ab-e4168c91290e';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'page_front_page__panel';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-bf4fc2d3-8123-4d7c-97b6-ce96c7b2dfbc';
  $pane->panel = 'bottom-first';
  $pane->type = 'block';
  $pane->subtype = 'dss_interface-sima_front_box1';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'bf4fc2d3-8123-4d7c-97b6-ce96c7b2dfbc';
  $display->content['new-bf4fc2d3-8123-4d7c-97b6-ce96c7b2dfbc'] = $pane;
  $display->panels['bottom-first'][0] = 'new-bf4fc2d3-8123-4d7c-97b6-ce96c7b2dfbc';
  $pane = new stdClass();
  $pane->pid = 'new-6a0fd056-964b-44cc-8061-a17251c41ebf';
  $pane->panel = 'bottom-second';
  $pane->type = 'block';
  $pane->subtype = 'dss_interface-sima_front_box2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '6a0fd056-964b-44cc-8061-a17251c41ebf';
  $display->content['new-6a0fd056-964b-44cc-8061-a17251c41ebf'] = $pane;
  $display->panels['bottom-second'][0] = 'new-6a0fd056-964b-44cc-8061-a17251c41ebf';
  $pane = new stdClass();
  $pane->pid = 'new-e7837ccf-f329-44d4-a26c-f3416c3113e9';
  $pane->panel = 'bottom-third';
  $pane->type = 'block';
  $pane->subtype = 'dss_interface-sima_front_box3';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'e7837ccf-f329-44d4-a26c-f3416c3113e9';
  $display->content['new-e7837ccf-f329-44d4-a26c-f3416c3113e9'] = $pane;
  $display->panels['bottom-third'][0] = 'new-e7837ccf-f329-44d4-a26c-f3416c3113e9';
  $pane = new stdClass();
  $pane->pid = 'new-bddb2158-160c-4065-9441-bb88ca56a825';
  $pane->panel = 'footer';
  $pane->type = 'block';
  $pane->subtype = 'dss_interface-sima_games';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'bddb2158-160c-4065-9441-bb88ca56a825';
  $display->content['new-bddb2158-160c-4065-9441-bb88ca56a825'] = $pane;
  $display->panels['footer'][0] = 'new-bddb2158-160c-4065-9441-bb88ca56a825';
  $pane = new stdClass();
  $pane->pid = 'new-74d2f132-d516-4121-bd7c-239d85378738';
  $pane->panel = 'footer';
  $pane->type = 'block';
  $pane->subtype = 'dss_interface-sima_front_bottom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '74d2f132-d516-4121-bd7c-239d85378738';
  $display->content['new-74d2f132-d516-4121-bd7c-239d85378738'] = $pane;
  $display->panels['footer'][1] = 'new-74d2f132-d516-4121-bd7c-239d85378738';
  $pane = new stdClass();
  $pane->pid = 'new-d1cfaf29-a136-47e7-9b59-8531c0dd6959';
  $pane->panel = 'footer';
  $pane->type = 'block';
  $pane->subtype = 'dss_interface-sima_logo_credits';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = 'd1cfaf29-a136-47e7-9b59-8531c0dd6959';
  $display->content['new-d1cfaf29-a136-47e7-9b59-8531c0dd6959'] = $pane;
  $display->panels['footer'][2] = 'new-d1cfaf29-a136-47e7-9b59-8531c0dd6959';
  $pane = new stdClass();
  $pane->pid = 'new-6d269fa5-8c21-4605-bbec-57b1d3da0b91';
  $pane->panel = 'middle';
  $pane->type = 'block';
  $pane->subtype = 'dss_interface-sima_add_front';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '6d269fa5-8c21-4605-bbec-57b1d3da0b91';
  $display->content['new-6d269fa5-8c21-4605-bbec-57b1d3da0b91'] = $pane;
  $display->panels['middle'][0] = 'new-6d269fa5-8c21-4605-bbec-57b1d3da0b91';
  $pane = new stdClass();
  $pane->pid = 'new-ccfee2c2-fb52-40c7-ba1c-7e07a4c23c72';
  $pane->panel = 'middle';
  $pane->type = 'block';
  $pane->subtype = 'dss_interface-sima_demo_front';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'ccfee2c2-fb52-40c7-ba1c-7e07a4c23c72';
  $display->content['new-ccfee2c2-fb52-40c7-ba1c-7e07a4c23c72'] = $pane;
  $display->panels['middle'][1] = 'new-ccfee2c2-fb52-40c7-ba1c-7e07a4c23c72';
  $pane = new stdClass();
  $pane->pid = 'new-0415a546-58b6-4bfc-8a9e-a27a808b3c15';
  $pane->panel = 'top-second';
  $pane->type = 'block';
  $pane->subtype = 'dss_interface-sima_search_front';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '0415a546-58b6-4bfc-8a9e-a27a808b3c15';
  $display->content['new-0415a546-58b6-4bfc-8a9e-a27a808b3c15'] = $pane;
  $display->panels['top-second'][0] = 'new-0415a546-58b6-4bfc-8a9e-a27a808b3c15';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['front_page'] = $page;

  return $pages;

}
