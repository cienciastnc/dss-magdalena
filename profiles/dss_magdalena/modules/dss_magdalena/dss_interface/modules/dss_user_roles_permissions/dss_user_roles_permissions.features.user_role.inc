<?php
/**
 * @file
 * dss_user_roles_permissions.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function dss_user_roles_permissions_user_default_roles() {
  $roles = array();

  // Exported role: Sima User.
  $roles['Sima User'] = array(
    'name' => 'Sima User',
    'weight' => 3,
  );

  return $roles;
}
