<?php
/**
 * @file
 * dss_site_menu.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function dss_site_menu_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_case-studies:case-study.
  $menu_links['main-menu_case-studies:case-study'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'case-study',
    'router_path' => 'case-study',
    'link_title' => 'Case Studies',
    'options' => array(
      'attributes' => array(
        'title' => 'Es un paquete de informacion completo y bastante articulado que caracteriza un Plan Integrado y las variables que condicionan su desempeno (Escenario y Contexto).
Un CASO se construye como un juego de "munecas rusas" empezando por el nivel mas bajo (detallado), o sea el de los proyectos.',
      ),
      'identifier' => 'main-menu_case-studies:case-study',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'main-menu_future:dss/future',
  );
  // Exported menu link: main-menu_change-of-land-use-projects:productive-land-use-projects.
  $menu_links['main-menu_change-of-land-use-projects:productive-land-use-projects'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'productive-land-use-projects',
    'router_path' => 'productive-land-use-projects',
    'link_title' => 'Change of Land Use Projects',
    'options' => array(
      'attributes' => array(
        'title' => 'Seleccion de subcuencas donde se daria el incremento de actividad AGRICOLA y, para cada una, caracterizacion de las variables clave: area total que se pretende lograr; fraccion que sera\' bajo regadio; umbrales de inicio y fin regadio.',
      ),
      'identifier' => 'main-menu_change-of-land-use-projects:productive-land-use-projects',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'main-menu_change-of-land-use:productive-land-use-alternatives-list',
  );
  // Exported menu link: main-menu_change-of-land-use:productive-land-use-alternatives-list.
  $menu_links['main-menu_change-of-land-use:productive-land-use-alternatives-list'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'productive-land-use-alternatives-list',
    'router_path' => 'productive-land-use-alternatives-list',
    'link_title' => 'Change of Land Use',
    'options' => array(
      'attributes' => array(
        'title' => 'Representa la decision de incrementar el area agricola en una o mas subcuencas de interes.',
      ),
      'identifier' => 'main-menu_change-of-land-use:productive-land-use-alternatives-list',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'main-menu_integral-plan-alternatives:integral-plan-list',
  );
  // Exported menu link: main-menu_climate:climate-scenario-list.
  $menu_links['main-menu_climate:climate-scenario-list'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'climate-scenario-list',
    'router_path' => 'climate-scenario-list',
    'link_title' => 'Climate',
    'options' => array(
      'attributes' => array(
        'title' => 'Paquete de series de tiempo coordinadas de las variables climaticas utilizadas por WEAP: precipitacion, temperatura, nubosidad, viento, brillo solar.',
      ),
      'identifier' => 'main-menu_climate:climate-scenario-list',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'parent_identifier' => 'main-menu_scenarios:scenario-list',
  );
  // Exported menu link: main-menu_context-information:node.
  $menu_links['main-menu_context-information:node'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node',
    'router_path' => 'node',
    'link_title' => 'Context Information',
    'options' => array(
      'attributes' => array(
        'title' => 'Contexto es la informacion que caracteriza el sistema fisico y supuestamente no cambia entre las alternativas a comparar.',
      ),
      'identifier' => 'main-menu_context-information:node',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
    'parent_identifier' => 'main-menu_future:dss/future',
  );
  // Exported menu link: main-menu_create-variable:node/add/dataset.
  $menu_links['main-menu_create-variable:node/add/dataset'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/add/dataset',
    'router_path' => 'node/add/dataset',
    'link_title' => 'Create Variable',
    'options' => array(
      'attributes' => array(
        'title' => 'Creates a new Variable',
      ),
      'identifier' => 'main-menu_create-variable:node/add/dataset',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'main-menu_data-management:dss/data-management',
  );
  // Exported menu link: main-menu_data-management:dss/data-management.
  $menu_links['main-menu_data-management:dss/data-management'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'dss/data-management',
    'router_path' => 'dss/data-management',
    'link_title' => 'Data Management',
    'options' => array(
      'attributes' => array(
        'title' => 'Data Management',
      ),
      'identifier' => 'main-menu_data-management:dss/data-management',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => 3,
    'customized' => 1,
  );
  // Exported menu link: main-menu_eloha-configuration:admin/config/dss-magdalena/settings/eloha.
  $menu_links['main-menu_eloha-configuration:admin/config/dss-magdalena/settings/eloha'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'admin/config/dss-magdalena/settings/eloha',
    'router_path' => 'admin/config/dss-magdalena/settings/eloha',
    'link_title' => 'ELOHA Configuration',
    'options' => array(
      'attributes' => array(
        'title' => 'ELOHA Configuration Settings',
      ),
      'identifier' => 'main-menu_eloha-configuration:admin/config/dss-magdalena/settings/eloha',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'main-menu_models-configuration:admin/config/dss-magdalena/settings',
  );
  // Exported menu link: main-menu_energy-demand:energy-scenario-list.
  $menu_links['main-menu_energy-demand:energy-scenario-list'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'energy-scenario-list',
    'router_path' => 'energy-scenario-list',
    'link_title' => 'Energy Demand',
    'options' => array(
      'attributes' => array(
        'title' => 'Demanda de energia que Colombia dirige a la macroocuenca en espera que sus recursos e infraestructuras puedan satisfacerla.',
      ),
      'identifier' => 'main-menu_energy-demand:energy-scenario-list',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 2,
    'customized' => 1,
    'parent_identifier' => 'main-menu_scenarios:scenario-list',
  );
  // Exported menu link: main-menu_faq:faq-page.
  $menu_links['main-menu_faq:faq-page'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'faq-page',
    'router_path' => 'faq-page',
    'link_title' => 'FAQ',
    'options' => array(
      'attributes' => array(
        'title' => 'Frequently Asked Questions',
      ),
      'identifier' => 'main-menu_faq:faq-page',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'main-menu_help:help/index',
  );
  // Exported menu link: main-menu_fragmentationdor-h-configuration:admin/config/dss-magdalena/settings/fragmentation_dorh.
  $menu_links['main-menu_fragmentationdor-h-configuration:admin/config/dss-magdalena/settings/fragmentation_dorh'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'admin/config/dss-magdalena/settings/fragmentation_dorh',
    'router_path' => 'admin/config/dss-magdalena/settings/fragmentation_dorh',
    'link_title' => 'Fragmentation/DOR-H Configuration',
    'options' => array(
      'attributes' => array(
        'title' => 'Fragmentation/DOR-H Configuration Settings',
      ),
      'identifier' => 'main-menu_fragmentationdor-h-configuration:admin/config/dss-magdalena/settings/fragmentation_dorh',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'parent_identifier' => 'main-menu_models-configuration:admin/config/dss-magdalena/settings',
  );
  // Exported menu link: main-menu_future:dss/future.
  $menu_links['main-menu_future:dss/future'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'dss/future',
    'router_path' => 'dss/future',
    'link_title' => 'Future',
    'options' => array(
      'attributes' => array(
        'title' => 'Que pasaria en el largo plazo, considerando la cuenca entera y los efectos acumulativos (y sus sinergias y antagonismos), al implementar una ALTernativa de Plan Integrado.',
      ),
      'identifier' => 'main-menu_future:dss/future',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => 2,
    'customized' => 1,
  );
  // Exported menu link: main-menu_help:help/index.
  $menu_links['main-menu_help:help/index'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'help/index',
    'router_path' => 'help/index',
    'link_title' => 'Help',
    'options' => array(
      'attributes' => array(
        'title' => 'Sima Help',
      ),
      'identifier' => 'main-menu_help:help/index',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => 4,
    'customized' => 1,
  );
  // Exported menu link: main-menu_historic:search/field_relevance_analysis/historic/type/dataset.
  $menu_links['main-menu_historic:search/field_relevance_analysis/historic/type/dataset'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'search/field_relevance_analysis/historic/type/dataset',
    'router_path' => 'search',
    'link_title' => 'Historic',
    'options' => array(
      'attributes' => array(
        'title' => 'Es el MODO que permite analizar el estado corriente del sistema y su evolucion en el pasado. Utiliza datos del sistema fisico-socio-economico-ambiental real en la macrocuenca considerada; no utiliza modelos de simulacion. Aun no esta\' implementado en Etapa I.',
      ),
      'identifier' => 'main-menu_historic:search/field_relevance_analysis/historic/type/dataset',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 1,
    'weight' => 1,
    'customized' => 1,
  );
  // Exported menu link: main-menu_hydroelectric-development:hydropower-alternatives-list.
  $menu_links['main-menu_hydroelectric-development:hydropower-alternatives-list'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'hydropower-alternatives-list',
    'router_path' => 'hydropower-alternatives-list',
    'link_title' => 'Hydroelectric Development',
    'options' => array(
      'attributes' => array(
        'title' => 'Alternativa sectorial coleccion de plantas hidroelectricas de embalse y de pasada.',
      ),
      'identifier' => 'main-menu_hydroelectric-development:hydropower-alternatives-list',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'main-menu_integral-plan-alternatives:integral-plan-list',
  );
  // Exported menu link: main-menu_import-weap-outputs:dss/data-management/import-weap-outputs.
  $menu_links['main-menu_import-weap-outputs:dss/data-management/import-weap-outputs'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'dss/data-management/import-weap-outputs',
    'router_path' => 'dss/data-management/import-weap-outputs',
    'link_title' => 'Import WEAP Outputs',
    'options' => array(
      'attributes' => array(
        'title' => 'Imports Outputs produced by WEAP into the system.',
      ),
      'identifier' => 'main-menu_import-weap-outputs:dss/data-management/import-weap-outputs',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'parent_identifier' => 'main-menu_data-management:dss/data-management',
  );
  // Exported menu link: main-menu_info-base:dss/context/infobase.
  $menu_links['main-menu_info-base:dss/context/infobase'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'dss/context/infobase',
    'router_path' => 'dss/context/%',
    'link_title' => 'Info base',
    'options' => array(
      'attributes' => array(
        'title' => 'Informacion de tipo fisico-socio-economico-ambiental que no cambiar entre Alternativas.',
      ),
      'identifier' => 'main-menu_info-base:dss/context/infobase',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'main-menu_context-information:node',
  );
  // Exported menu link: main-menu_initial-state:dss/context/initial_state.
  $menu_links['main-menu_initial-state:dss/context/initial_state'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'dss/context/initial_state',
    'router_path' => 'dss/context/%',
    'link_title' => 'Initial State',
    'options' => array(
      'attributes' => array(
        'title' => 'Volumen embalsado al tiempo 0 en cada embalse y cada "tanque" (lacuiferos o capas del suelo en el modelo WEAP).',
      ),
      'identifier' => 'main-menu_initial-state:dss/context/initial_state',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'main-menu_context-information:node',
  );
  // Exported menu link: main-menu_integral-plan-alternatives:integral-plan-list.
  $menu_links['main-menu_integral-plan-alternatives:integral-plan-list'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'integral-plan-list',
    'router_path' => 'integral-plan-list',
    'link_title' => 'Integral Plan Alternatives',
    'options' => array(
      'attributes' => array(
        'title' => 'Paquete de informacion que especifica todas las decisiones (dentro de las que la actual version del SIMA permite maniovrar) de planificacion de largo plazo que cambiaran "la cara y el comportamiento" de la macrocuenca.',
      ),
      'identifier' => 'main-menu_integral-plan-alternatives:integral-plan-list',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -48,
    'customized' => 1,
    'parent_identifier' => 'main-menu_future:dss/future',
  );
  // Exported menu link: main-menu_interventions:wetlands-management-projects.
  $menu_links['main-menu_interventions:wetlands-management-projects'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'wetlands-management-projects',
    'router_path' => 'wetlands-management-projects',
    'link_title' => 'Interventions',
    'options' => array(
      'attributes' => array(
        'title' => 'Conjunto de intervenciones, posiblemente en varios sitios/zonas, que apunta a modificar la interaccion rio-humedal.',
      ),
      'identifier' => 'main-menu_interventions:wetlands-management-projects',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'main-menu_wetlands-management:wetland-management-alternatives-list',
  );
  // Exported menu link: main-menu_model-list:model-list.
  $menu_links['main-menu_model-list:model-list'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'model-list',
    'router_path' => 'model-list',
    'link_title' => 'Model List',
    'options' => array(
      'attributes' => array(
        'title' => 'Model List',
      ),
      'identifier' => 'main-menu_model-list:model-list',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
    'parent_identifier' => 'main-menu_data-management:dss/data-management',
  );
  // Exported menu link: main-menu_model-parameters:dss/context/parameter.
  $menu_links['main-menu_model-parameters:dss/context/parameter'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'dss/context/parameter',
    'router_path' => 'dss/context/%',
    'link_title' => 'Model parameters',
    'options' => array(
      'attributes' => array(
        'title' => 'Coeficientes numericos utilizados por los modelos matematicos adoptados.',
      ),
      'identifier' => 'main-menu_model-parameters:dss/context/parameter',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
    'parent_identifier' => 'main-menu_context-information:node',
  );
  // Exported menu link: main-menu_models-configuration:admin/config/dss-magdalena/settings.
  $menu_links['main-menu_models-configuration:admin/config/dss-magdalena/settings'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'admin/config/dss-magdalena/settings',
    'router_path' => 'admin/config/dss-magdalena/settings',
    'link_title' => 'Models Configuration',
    'options' => array(
      'attributes' => array(
        'title' => 'Configure Models Settings',
      ),
      'identifier' => 'main-menu_models-configuration:admin/config/dss-magdalena/settings',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
    'parent_identifier' => 'main-menu_data-management:dss/data-management',
  );
  // Exported menu link: main-menu_population:population-scenario-list.
  $menu_links['main-menu_population:population-scenario-list'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'population-scenario-list',
    'router_path' => 'population-scenario-list',
    'link_title' => 'Population',
    'options' => array(
      'attributes' => array(
        'title' => 'Patron de evolucion de la poblacion de la macrocuenca.',
      ),
      'identifier' => 'main-menu_population:population-scenario-list',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 1,
    'customized' => 1,
    'parent_identifier' => 'main-menu_scenarios:scenario-list',
  );
  // Exported menu link: main-menu_reservoir-plants:hydropower-plants-and-dams.
  $menu_links['main-menu_reservoir-plants:hydropower-plants-and-dams'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'hydropower-plants-and-dams',
    'router_path' => 'hydropower-plants-and-dams',
    'link_title' => 'Reservoir plants',
    'options' => array(
      'attributes' => array(
        'title' => 'Proyecto de planta hidroelectrica de embalse. Se pueden crear variantes de cada proyecto en el mismo sitio variando cualquiera de sus caracteristicas.',
      ),
      'identifier' => 'main-menu_reservoir-plants:hydropower-plants-and-dams',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 1,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'main-menu_hydroelectric-development:hydropower-alternatives-list',
  );
  // Exported menu link: main-menu_run-of-river-plants:run-of-river-hydropower-plant.
  $menu_links['main-menu_run-of-river-plants:run-of-river-hydropower-plant'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'run-of-river-hydropower-plant',
    'router_path' => 'run-of-river-hydropower-plant',
    'link_title' => 'Run of River Plants',
    'options' => array(
      'attributes' => array(
        'title' => 'Plantas de generacion hidroelectrica de pasada: del todo analogo al item anterior de embalse, pero...sin embalse.',
      ),
      'identifier' => 'main-menu_run-of-river-plants:run-of-river-hydropower-plant',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 1,
    'weight' => -48,
    'customized' => 1,
    'parent_identifier' => 'main-menu_hydroelectric-development:hydropower-alternatives-list',
  );
  // Exported menu link: main-menu_scenarios:scenario-list.
  $menu_links['main-menu_scenarios:scenario-list'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'scenario-list',
    'router_path' => 'scenario-list',
    'link_title' => 'Scenarios',
    'options' => array(
      'attributes' => array(
        'title' => 'Conjunto de variables que afectan el comportamiento y la evolucion del sistema fisico-socio-economico-ambiental, pero que no estan bajo el control del tomador de decisiones.',
      ),
      'identifier' => 'main-menu_scenarios:scenario-list',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -47,
    'customized' => 1,
    'parent_identifier' => 'main-menu_future:dss/future',
  );
  // Exported menu link: main-menu_territorial-footprint-configuration:admin/config/dss-magdalena/settings/territorial_footprint.
  $menu_links['main-menu_territorial-footprint-configuration:admin/config/dss-magdalena/settings/territorial_footprint'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'admin/config/dss-magdalena/settings/territorial_footprint',
    'router_path' => 'admin/config/dss-magdalena/settings/territorial_footprint',
    'link_title' => 'Territorial Footprint Configuration',
    'options' => array(
      'attributes' => array(
        'title' => 'Territorial Footprint Configuration Settings',
      ),
      'identifier' => 'main-menu_territorial-footprint-configuration:admin/config/dss-magdalena/settings/territorial_footprint',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
    'parent_identifier' => 'main-menu_models-configuration:admin/config/dss-magdalena/settings',
  );
  // Exported menu link: main-menu_thematic-classification-of-variables:admin/structure/taxonomy/thematic_tree.
  $menu_links['main-menu_thematic-classification-of-variables:admin/structure/taxonomy/thematic_tree'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'admin/structure/taxonomy/thematic_tree',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Thematic Classification of Variables',
    'options' => array(
      'attributes' => array(
        'title' => 'Thematic Classification of Variables',
      ),
      'identifier' => 'main-menu_thematic-classification-of-variables:admin/structure/taxonomy/thematic_tree',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -45,
    'customized' => 1,
    'parent_identifier' => 'main-menu_data-management:dss/data-management',
  );
  // Exported menu link: main-menu_variables:shadow_dataset.
  $menu_links['main-menu_variables:shadow_dataset'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'shadow_dataset',
    'router_path' => 'shadow_dataset',
    'link_title' => 'Variables',
    'options' => array(
      'attributes' => array(
        'title' => 'Las variables DSS son entidades descritas por una serie de atributos estandar a las cuales estan asociadas varias cajas de datos (o simplemente los "datos").',
      ),
      'identifier' => 'main-menu_variables:shadow_dataset',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'main-menu_data-management:dss/data-management',
  );
  // Exported menu link: main-menu_weap-configuration:admin/config/dss-magdalena/settings.
  $menu_links['main-menu_weap-configuration:admin/config/dss-magdalena/settings'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'admin/config/dss-magdalena/settings',
    'router_path' => 'admin/config/dss-magdalena/settings',
    'link_title' => 'WEAP Configuration',
    'options' => array(
      'attributes' => array(
        'title' => 'WEAP Configuration Settings',
      ),
      'identifier' => 'main-menu_weap-configuration:admin/config/dss-magdalena/settings',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'main-menu_models-configuration:admin/config/dss-magdalena/settings',
  );
  // Exported menu link: main-menu_wetlands-management:wetland-management-alternatives-list.
  $menu_links['main-menu_wetlands-management:wetland-management-alternatives-list'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'wetland-management-alternatives-list',
    'router_path' => 'wetland-management-alternatives-list',
    'link_title' => 'Wetlands management',
    'options' => array(
      'attributes' => array(
        'title' => 'Manejo de humedales: paquete de decisiones para modificar el intercambio hidrico entre rios y humedales/cienagas/lagunas particularmente de la zona de la Mojana pensando a finalidades ambientales y de explotacion pesquera.',
      ),
      'identifier' => 'main-menu_wetlands-management:wetland-management-alternatives-list',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -47,
    'customized' => 1,
    'parent_identifier' => 'main-menu_integral-plan-alternatives:integral-plan-list',
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Case Studies');
  t('Change of Land Use');
  t('Change of Land Use Projects');
  t('Climate');
  t('Context Information');
  t('Create Variable');
  t('Data Management');
  t('ELOHA Configuration');
  t('Energy Demand');
  t('FAQ');
  t('Fragmentation/DOR-H Configuration');
  t('Future');
  t('Help');
  t('Historic');
  t('Hydroelectric Development');
  t('Import WEAP Outputs');
  t('Info base');
  t('Initial State');
  t('Integral Plan Alternatives');
  t('Interventions');
  t('Model List');
  t('Model parameters');
  t('Models Configuration');
  t('Population');
  t('Reservoir plants');
  t('Run of River Plants');
  t('Scenarios');
  t('Territorial Footprint Configuration');
  t('Thematic Classification of Variables');
  t('Variables');
  t('WEAP Configuration');
  t('Wetlands management');

  return $menu_links;
}
