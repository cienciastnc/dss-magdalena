<?php
/**
 * @file
 * Blocks for the core version of SIMA.
 */

use Drupal\dss_magdalena\DSS\Entity\SimaObjective;
use Drupal\dss_magdalena\DSS\Entity\SimaResource;

/**
 * Implements hook_block_info().
 */
function dss_interface_block_info() {
  $blocks['sima_demo_front'] = array(
    'info' => t('Welcome to the SIMA'),
    'cache' => DRUPAL_NO_CACHE,
  );
  $blocks['sima_mapa'] = array(
    'info' => t('Welcome to the SIMA Mapa'),
    'cache' => DRUPAL_NO_CACHE,
  );
  $blocks['sima_search_front'] = array(
    'info' => t('Search Your Data'),
    'cache' => DRUPAL_NO_CACHE,
  );
  $blocks['sima_add_front'] = array(
    'info' => t('Desafios de macrocuencas'),
    'cache' => DRUPAL_NO_CACHE,
  );
  $blocks['sima_logo_credits'] = array(
    'info' => t('Credits'),
    'cache' => DRUPAL_NO_CACHE,
  );
  $blocks['sima_front_box1'] = array(
    'info' => t('Que hace?'),
    'cache' => DRUPAL_NO_CACHE,
  );
  $blocks['sima_front_box2'] = array(
    'info' => t('Alcance'),
    'cache' => DRUPAL_NO_CACHE,
  );
  $blocks['sima_front_box3'] = array(
    'info' => t('Interacción'),
    'cache' => DRUPAL_NO_CACHE,
  );
  $blocks['sima_front_bottom'] = array(
    'info' => t('Sobre los Autores'),
    'cache' => DRUPAL_NO_CACHE,
  );
  $blocks['sima_games'] = array(
    'info' => t('Juegos SIMA'),
    'cache' => DRUPAL_NO_CACHE,
  );
  $blocks['sima_objectives'] = array(
    'info' => t('Objetivos'),
    'cache' => DRUPAL_NO_CACHE,
  );
  $blocks['sima_evaluation'] = array(
    'info' => t('Evaluación'),
    'cache' => DRUPAL_NO_CACHE,
  );

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function dss_interface_block_view($delta = '') {
  $block = array();

  switch ($delta) {
    case 'sima_demo_front':
      $block['subject'] = t('Welcome to SIMA');
      $default = dss_interface_sima_demo_front_block();
      $block['content'] = check_markup(variable_get('dss_interface_sima_demo_front_text', $default), variable_get('dss_interface_sima_demo_front_format', 'full_html'));
      break;

    case 'sima_search_front':
      $block['subject'] = t('Search Your Data');
      $block['content'] = dss_interface_sima_search_front_block();
      break;

    case 'sima_add_front':
      $block['subject'] = t('Desafios de macrocuencas');
      $default = dss_interface_sima_add_front_block();
      $block['content'] = check_markup(variable_get('dss_interface_sima_add_front_text', $default), variable_get('dss_interface_sima_add_front_format', 'full_html'));
      break;

    case 'sima_logo_credits':
      $block['subject'] = t('Credits');
      $default = dss_interface_sima_logo_credits_block();
      $block['content'] = check_markup(variable_get('dss_interface_sima_logo_credits_text', $default), variable_get('dss_interface_sima_logo_credits_format', 'full_html'));
      break;

    case 'sima_front_box1':
      $block['subject'] = t('Que hace?');
      $default = dss_interface_sima_front_box1_block();
      $block['content'] = check_markup(variable_get('dss_interface_sima_front_box1_text', $default), variable_get('dss_interface_sima_front_box1_format', 'full_html'));
      break;

    case 'sima_front_box2':
      $block['subject'] = t('Alcance');
      $default = dss_interface_sima_front_box2_block();
      $block['content'] = check_markup(variable_get('dss_interface_sima_front_box2_text', $default), variable_get('dss_interface_sima_front_box2_format', 'full_html'));
      break;

    case 'sima_front_box3':
      $block['subject'] = t('Interacción');
      $default = dss_interface_sima_front_box3_block();
      $block['content'] = check_markup(variable_get('dss_interface_sima_front_box3_text', $default), variable_get('dss_interface_sima_front_box3_format', 'full_html'));
      break;

    case 'sima_front_bottom':
      $block['subject'] = t('Sobre los Autores');
      $default = dss_interface_sima_front_bottom_block();
      $block['content'] = check_markup(variable_get('dss_interface_sima_front_bottom_text', $default), variable_get('dss_interface_sima_front_bottom_format', 'full_html'));
      break;
    case 'sima_mapa':
          $default = dss_interface_sima_mapa_block();
          $block['content'] = check_markup(variable_get('dss_interface_sima_games_text', $default), variable_get('dss_interface_sima_games_format', 'full_html'));
          break;

    case 'sima_games':
      $block['subject'] = t('Juegos SIMA');
      $default = dss_interface_sima_games_block();
      $block['content'] = check_markup(variable_get('dss_interface_sima_games_text', $default), variable_get('dss_interface_sima_games_format', 'full_html'));
      break;

    case 'sima_objectives':
      $block['subject'] = t('Perspectiva Multi-objetivo');
      $block['content'] = dss_interface_sima_objectives_block();
      break;

    case 'sima_evaluation':
      $block['subject'] = t('Tipo de Evaluación');
      $block['content'] = dss_interface_sima_evaluation_block();
      break;
  }
  return $block;
}

/**
 * Implements hook_block_configure().
 */
function dss_interface_block_configure($delta = '') {
  $form = array();
  if ($delta == 'sima_demo_front') {
    $form['dss_interface_sima_demo_front_text'] = array(
      '#type' => 'text_format',
      '#title' => t('Default'),
      '#description' => t('Add text here to override the default block content. Leave blank to continue using default.'),
      '#default_value' => variable_get('dss_interface_sima_demo_front_text', ''),
      '#format' => variable_get('dss_interface_sima_demo_front_format', 'full_html'),
    );
  }
  if ($delta == 'sima_add_front') {
    $form['dss_interface_sima_add_front_text'] = array(
      '#type' => 'text_format',
      '#title' => t('Desafios de macrocuencas'),
      '#description' => t('Add text here to override the default block content. Leave blank to continue using default.'),
      '#default_value' => variable_get('dss_interface_sima_add_front_text', ''),
      '#format' => variable_get('dss_interface_sima_add_front_format', 'full_html'),
    );
  }

  return $form;
}

/**
 * Implements hook_block_save().
 */
function dss_interface_block_save($delta = '', $edit = array()) {
  if ($delta == 'sima_demo_front') {
    if (empty($edit['dss_interface_sima_demo_front_text']['value'])) {
      variable_del('dss_interface_sima_demo_front_text');
      drupal_set_message(t('Reset block content to default.'));
    }
    else {
      variable_set('dss_interface_sima_demo_front_text', $edit['dss_interface_sima_demo_front_text']['value']);
      variable_set('dss_interface_sima_demo_front_format', $edit['dss_interface_sima_demo_front_text']['format']);
    }
  }
  if ($delta == 'sima_add_front') {
    if (empty($edit['dss_interface_sima_add_front_text']['value'])) {
      variable_del('dss_interface_sima_add_front_text');
      drupal_set_message(t('Reset block content to default.'));
    }
    else {
      variable_set('dss_interface_sima_add_front_text', $edit['dss_interface_sima_add_front_text']['value']);
      variable_set('dss_interface_sima_add_front_format', $edit['dss_interface_sima_add_front_text']['format']);
    }
  }
}

/**
 * Creates sima front box1 block.
 */
function dss_interface_sima_front_box1_block() {
  $box_graphic = url(drupal_get_path('module', 'dss_interface') . '/images/box1.jpg');
  $box_url = url('case-study');
  $content = '<div class = "sima-front-box"><a class="image" href=" ' . $box_url . ' "><img src="' . $box_graphic . '" style="width: 100%" alt="Que hace?"></a>';
  $content .= '<div class="sima-box-title">Que hace?</div>';
  $content .= t('<p class="front-box">Además de averiguar las consecuencias de planes hipotéticos (modo FUTURO), ofrece una base informativa y de análisis de la realidad y su evolución (modo HISTÓRICO) organizada en un directorio integrado dotado de filtros de búsqueda y herramientas de visualización.</p></div>');
  return $content;
}

/**
 * Creates sima front box1 block.
 */
function dss_interface_sima_front_box2_block() {
  $box_graphic = url(drupal_get_path('module', 'dss_interface') . '/images/box2.jpg');
  $box_url = url('case-study');
  $content = '<div class = "sima-front-box"><a class="image" href=" ' . $box_url . ' "><img src="' . $box_graphic . '" style="width: 100%" alt="Alcance"></a>';
  $content .= '<div class="sima-box-title">Alcance</div>';
  $content .= t('<p class="front-box"><b>SIMA</b> no resuelve por si solo el complejo problema de planificar una macrocuenca, pero contribuye de diferentes maneras: provee información integrada a diferentes actores, promueve el pensamiento a escala de cuenca, y provee una mesa "virtual", donde diferentes puntos de vista y objetivos pueden ser comparados.</p></div>');
  return $content;
}

/**
 * Creates sima front box1 block.
 */
function dss_interface_sima_front_box3_block() {
  $box_graphic = url(drupal_get_path('module', 'dss_interface') . '/images/box3.jpg');
  $box_url = url('case-study');
  $content = '<div class = "sima-front-box"><a class="image" href=" ' . $box_url . ' "><img src="' . $box_graphic . '" style="width: 100%" alt="Interacción"></a>';
  $content .= '<div class="sima-box-title">Interacción</div>';
  $content .= t('<p class="front-box">Puedes involucrarte de distintas formas: construyendo casos de estudio, proveyendo información, contribuyendo al desarrollo de nuevas aplicaciones y funcionalidades (siendo open source), o utilizando como herramienta didáctica y educativa.</p></div>');
  return $content;
}

/**
 * Creates the sima front bottom block.
 */
function dss_interface_sima_front_bottom_block() {
  $content = t('<p class="front-teaser"><b>SIMA</b> es un producto de un esfuerzo conjunto de TNC y CREACUA, apoyados por varias organizaciones y actores.</p>');
  return $content;
}

/**
 * Creates Credits block.
 */
function dss_interface_sima_logo_credits_block() {
  $homepage_graphic = url(drupal_get_path('module', 'dss_interface') . '/images/logo_credits.jpg');
  $content = '<p><img src="' . $homepage_graphic . '" style="width: 100%" alt="Image of SIMA case workflow"></p>';
  return $content;
}

/**
 * Creates search block for front page.
 */
function dss_interface_sima_search_front_block() {
  return dkan_sitewide_search_bar();
}

/**
 * Creates placeholder front block.
 */
function dss_interface_sima_demo_front_block() {
  $css = drupal_get_path('module', 'dss_interface') . '/css/front.css';
  drupal_add_css($css);
  $content = t('<p class="front-teaser"><b>SIMA</b> permite analizar los efectos hipoteticos de planes integrales y evaluarlos con un enfoque multicriterio a nivel estrategico de macro-cuenca <b>Magdalena</b>. Utiliza herramientas de prediccion como el modelo de simulacion de <b>Sistemas de Recursos Hidricos </span> <a href="http://www.weap21.org/">WEAP</a></b>.</p>');
  return $content;
}

/**
 * Creates "Casos de Estudio" block for front page.
 */
function dss_interface_sima_add_front_block() {
  $text = t('ESTADO DE IMPLEMENTACIÓN EN ETAPA I DEL SISTEMA DE APOYO A LA TOMA DE DECISIONES PARA LA MACROCUENCA MAGDALENA-CAUCA (SIMA-MAGDALENA)');
  $href = l($text, 'profiles/dss_magdalena/modules/dss_magdalena/dss_help/help/StatusDesarrolloSIMAEtapaII.docx', array('attributes' => array('target' => '_blank')));
  $output = '<div class="add content"><p>';
  $output .= t('Información, largo plazo & amplio alcance espacial, acciones multi-sectoriales, impactos acumulativos, incertidumbre, conflictos, sinergías y gobernanza');
  $output .= '</p><p style="text-align: center"><b>' . $href;
  $output .= '</b></p></div>';
  return $output;
}

/**
 * Creates the sima games block.
 */
function dss_interface_sima_games_block() {
  $simbasin_graphic = url(drupal_get_path('module', 'dss_interface') . '/images/simbasin-logo.png');
  $simbasin_url = url('http://www.simbasin.org/es/index.html', array('absolute' => TRUE, 'external' => TRUE));
  $simompox_url = url('http://www.simbasin.org/es/download2.html', array('absolute' => TRUE, 'external' => TRUE));
  $simbasin_href = l(t('Simbasin'), $simbasin_url, array('attributes' => array('target' => '_blank')));
  $simompox_href = l(t('Simompox'), $simompox_url, array('attributes' => array('target' => '_blank')));
  $output = '<div class="add content">';
  $output .= '<p align="center"><img src="' . $simbasin_graphic . '" alt="Simbasin"></p>';
  $output .= t('<p>Prueba los siguientes juegos SIMA:</p>');
  $output .= '</ul>';
  $output .= t('<li><b>!simbasin: </b>¿Quieres simular la gestión de una cuenca hidrográfica? SimBasin es un juego diseñado para conectar a los actores con los científicos mediante un juego que integra aspectos de modelación de recursos hídricos.</li>', array(
    '!simbasin' => $simbasin_href,
  ));
  $output .= t('<li><b>!simompox:</b> En este juego, podrás planificar a largo plazo diferentes medidas de manejo de la Depresión Momposina, para cumplir multiples objetivos: económicos, de protección de la población y de conservación de su gran riqueza natural. El objetivo principal de Si-Mompox es educar a las partes interesadas y aclarar como las intervenciones humanas y otros cambios tienen consecuencias positivas y negativas en la zona y que los efectos tienen consecuencias en el tiempo y en el espacio.</li>', array(
    '!simompox' => $simompox_href,
  ));
  $output .= '</ul></div>';
  return $output;
}

/**
 * Creates the Sima Objectives Block.
 */
function dss_interface_sima_objectives_block() {
  // Loan an objective selection for this particular user.
  if ($objective = SimaObjective::loadByCurrentUser()) {
    // Read current path and extract the selected case studies.
    $path = current_path();
    preg_match_all('/field_model_case_study_id\/(?P<digit>\d+)/', $path, $match);
    $cids = $match[1];
    if (is_array($cids) && count($cids) > 0) {
      $data = [];
      // Find all the resources for the selected case studies and objectives.
      $datasets = $objective->getSelectedObjectives();
      foreach ($datasets as $dataset) {
        foreach ($cids as $cid) {
          $dataset_machine_name = $dataset->getMachineName();
          $data[$dataset_machine_name]['title'] = $dataset->getTitle();
          if ($resource = SimaResource::loadByCaseStudyAndDataset($cid, $dataset_machine_name)) {
            $results = $resource->getDataStoreResultsNew();
            if (is_array($results)) {
              $case_title = $resource->getCaseStudy()->getTitle();
              $value['title'] = isset($case_title) ? $case_title : t('No title');
              $value['value'] = isset($results['items'][0]['value']) ? $results['items'][0]['value'] : 0;
            }
            else {
              $value['value'] = 0;
            }
            $data[$dataset_machine_name][$cid] = $value;
          }
        }
      }
      return dss_visualizations_multi_objectives($data);
    }
  }
  return t('Please select at least one Case Study and one Objective.');
}

/**
 * Sima Evaluation Block.
 */
function dss_interface_sima_evaluation_block() {
  // Only show Evaluation buttons if we are coming from the Evaluation workflow.
  $show_sima_evaluation_blocks = session_get('dss_engine_show_sima_evaluation_blocks', FALSE);
  if (!$show_sima_evaluation_blocks) {
    return '';
  }

  $strategic_evaluation_url = '/multi-objectives';
  $analytical_evaluation_url = '/search/field_relevance_analysis/simulation/type/dataset';
  $current_path = current_path();
  $active_strategic = '';
  $active_analytic = '';

  if (strpos($current_path, 'multi-objectives') !== FALSE) {
    // We are in Multi-objectives (Strategic Evaluation).
    $strategic_evaluation_url = '/' . $current_path;
    $url_suffix = str_replace('multi-objectives', '', $current_path);
    $analytical_evaluation_url .= $url_suffix;
    $active_strategic = 'active';
  }
  elseif (strpos($current_path, 'search') !== FALSE) {
    // We are in Dataset (Analytical Evaluation).
    $analytical_evaluation_url = '/' . $current_path;
    $url_suffix = str_replace('search', '', $current_path);
    $strategic_evaluation_url .= $url_suffix;
    $active_analytic = 'active';
  }
  $output = '<div id="sima-evaluation-block">';
  $output .= '<input type="submit" id="sima-strategic-evaluation" name="sima_str_eva" value="Evaluación Estratégica" class="btn btn-default sima-evaluation ' . $active_strategic . '"  onclick="location.href=\'' . $strategic_evaluation_url . '\';">';
  $output .= '<input type="submit" id="sima-analytical-evaluation" name="sima_ana_eva" value="Evaluación Analítica" class="btn btn-default sima-evaluation ' . $active_analytic . '" onclick="location.href=\'' . $analytical_evaluation_url . '\';">';
  $output .= '</div>';
  return $output;
}
/**
 * Creates the sima map block.
 */
function dss_interface_sima_mapa_block() {
  $topoNetworkNid = SimaResource::listByDataset(5161);
 /* print_r($topoNetworkNid);*/
  foreach ($topoNetworkNid as $key => $resource) { 
    $resourceLoaded = SimaResource::load($key);
    $resourceDataset = $resourceLoaded->getReferencedVariables();
    $topoNetworkUuid = $resourceDataset[0]->uuid;
  }
  drupal_add_js(
    "var datasetUuid='$topoNetworkUuid';",
    'inline'
);

  $css = drupal_get_path('module', 'dss_interface') . '/css/map.css';
  drupal_add_css($css);
  drupal_add_css('https://unpkg.com/leaflet@1.5.1/dist/leaflet.css', array( 'type' => 'external' ));
  $output = '<div style="background-color:#104659;padding:1px;"class="rtecenter"><h1 style="color:#fff">VISUALIZADOR RÁPIDO PARA CASOS DE ESTUDIO</h1></div>'; 
  $output .= '<div class="container barra_busqueda" >';

  $output .= '<div class="col-md-12 headermenu">';
  $output .= '<div class="col-md-4 headermapa">';
  $output .= '<p>Selecione un caso de estudio</p>';
  $output .= '</div>';
  $output .= '<div class="col-md-3">';
  $output .= '<select class="form-control" id="selectMapa"> <option value="0">-Ninguno-</option>';
  $output .= '</select>';
  $output .= '</div>';
  $output .= '<div class="col-md-3">';
  $output .= '<input type="button" value="Localizar" id="searchCaseStudy" class="btn btn-lg btn-primary">';
  $output .= '</div></div></div>';
  $output .= '<div id="mapa" class="mapaInicio">';
  $output .= '<input id="vermasMapa" type="button" value="ver más de este caso de estudio" id="verMas" class="btn btn-lg btn-primary botonFlotante">';
  $output .= '<div id="cover-spin">';
  $output .= '</div></div>';
   drupal_add_js(drupal_get_path('module', 'dss_engine') . '/js/map.js');
   drupal_add_js("https://unpkg.com/leaflet@1.5.1/dist/leaflet.js");
  return $output;
}

