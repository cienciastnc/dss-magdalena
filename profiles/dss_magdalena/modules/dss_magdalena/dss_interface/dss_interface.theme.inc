<?php
/**
 * @file
 * Overrides the default theme for viewing dataset search-results.
 */

use Drupal\dss_magdalena\DSS\Entity\SimaResource;

/**
 * Implements hook_theme().
 */
function dss_interface_theme() {
  $theme = array(
    'dss_interface_resource_view' => array(
      'variables' => array('node' => NULL),
    ),
    'dss_interface_search_results_preview_link' => array(
      'variables' => array('node' => NULL),
    ),
    'dss_interface_search_results_comparison_link' => array(
      'variables' => array('nodes' => NULL),
    ),
    'dss_interface_comparison_link' => array(
      'variables' => array(
        'title' => NULL,
        'desc' => NULL,
        'link' => NULL,
        'format' => NULL,
      ),
    ),
  );
  return $theme;
}

/**
 * Output the resource into the node content.
 */
function theme_dss_interface_resource_view($vars) {
  $node = entity_metadata_wrapper('node', $vars['node']);
  $nodes = $node->field_resources->value();
  $links = array();
  $div_id = 'dataset-resources-' . $node->getIdentifier();
  $output = '<h4><a data-toggle="collapse" href="#' . $div_id . '"><span class="icon-dkan-resource">&nbsp;</span>' . t('Data and Resources') . '</a></h4>';
  $output .= '<div class="collapse" id="' . $div_id . '" property="dcat:distribution">';

  // Get selected case study id's.
  $match_count = preg_match_all('/field_model_case_study_id+\/([0-9]{1,})/', current_path(), $matches);

  // Filter only selected nodes by case id.
  $node_links = NULL;
  if ($match_count > 0) {
    $filtered_nodes = array();
    foreach ($nodes as $e_node) {
      // Node could be empty if it has been deleted.
      if (!$e_node || !node_access('view', $e_node)) {
        continue;
      }

      // Get CaseStudy Id number from node.
      $resource = SimaResource::load($e_node);
      $case_nid = $resource->getCaseStudy()->getId();

      // Check if the Id is in the matched array.
      if (in_array($case_nid, $matches[1], TRUE)) {
        // Added to the filtered node array.
        array_push($filtered_nodes, $e_node);

        // Nodes for link graph comparison.
        $node_links .= $node_links ? ',' . $resource->getId() : '' . $resource->getId();
      }
    }
  }
  else {
    // None nodes were selected, so display all of them.
    $filtered_nodes = $nodes;
  }

  if (isset($filtered_nodes)) {
    $file_count = 0;
    foreach ($filtered_nodes as $node) {
      // Node could be empty if it has been deleted.
      if (!$node || !node_access('view', $node)) {
        continue;
      }
      $node_wrapper = entity_metadata_wrapper('node', $node);
      $body = $node_wrapper->body->value();

      if (isset($body['safe_value'])) {
        $desc = '<p class="description">' . dkan_dataset_text_trim($body['safe_value'], 80) . '</p>';
      }
      else {
        $desc = '<p class="description"></p>';
      }
      $format = t("data");
      if (isset($node->field_format) && $node->field_format && $node_wrapper->field_format->value()) {
        $format = $node_wrapper->field_format->value()->name;
      }
      if (isset($node->field_upload[LANGUAGE_NONE])) {
        $type = 'file_upload';
        $file_count++;
      }
      elseif (isset($node->field_link_remote_file[LANGUAGE_NONE])) {
        $type = 'link_remote_file';
      }
      else {
        $type = 'link_api';
      }
      $teaser_link = theme('dss_interface_search_results_preview_link', array('node' => $node, 'type' => $type));
      $links[] = theme('dkan_dataset_teaser_link', array(
        'link' => $teaser_link,
        'title' => $node->title,
        'nid' => $node->nid,
        'desc' => $desc,
        'format' => $format,
        'type' => $type,
        'node' => $node,
      ));
    }

    // Add the compare analysis links.
    if ($node_links && (count($filtered_nodes) > 1)) {
      $comparison_link = theme('dss_interface_search_results_comparison_link', array('nodes' => $node_links));
      $links[] = theme('dss_interface_comparison_link', array(
        'title' => t("Comparison Analysis"),
        'desc' => '<p class="description"><p>' . t("Compares the listed Case Studies.") . '</p></p>',
        'link' => $comparison_link,
        'format' => '',
      ));
    }

    $output .= theme('item_list', array('items' => $links, 'attributes' => array('class' => array('resource-list'))));
    // Close first dcat declaration.
    $output .= '</div>';
  }
  return $output;
}

/**
 * Theme function to create "open with" version of teaser links on Datasets.
 */
function theme_dss_interface_search_results_preview_link($vars) {
  $preview_link = $direct_link = '';
  $resource = $vars['node'];
  $geo_url = 'node/' . $resource->nid . '/space-aggregation';
  $time_url = 'node/' . $resource->nid . '/time-aggregation';
  $geo_link = l(
    '<i class="fa fa-map"></i> ' . t('Space'),
    $geo_url,
    array(
      'html' => TRUE,
      'attributes' => array(
        'class' => array('btn', 'btn-primary', 'data-link'),
      ),
    )
  );
  $time_link = l(
    '<i class="fa fa-clock-o"></i> ' . t('Time'),
    $time_url,
    array(
      'html' => TRUE,
      'attributes' => array(
        'class' => array('btn', 'btn-primary', 'data-link'),
      ),
    )
  );
  $node = $vars['node'];
  $previews = dkan_dataset_teaser_external_previews_for_resource($node);
  if (count($previews)) {
    // If multiple previews, use the "open with" format.
    if (count($previews) > 1) {
      $preview_link = '<div class="btn-group">
        <button class="btn btn-primary" data-toggle="dropdown">' . t('Open With') . '</button>
          <button class="btn btn-primary" data-toggle="dropdown">
            <span class="caret"></span><span class="sr-only">Toggle dropdown</span>
          </button>
        <ul class="dropdown-menu">
      ';
      foreach ($previews as $provider => $preview) {
        $preview_link .= '<li class="' . $provider . ' ">';
        $preview_link .= l('<span> ' . $preview['name'] . '</span>', $preview['url'], array('html' => TRUE));
        $preview_link .= '</li>';
      }
      $preview_link .= '</ul></div>';
    }
    // Otherwise use single button.
    else {
      $machine_name = current(array_keys($previews));
      $preview = $previews[$machine_name];
      if ($machine_name == 'dkan') {
        $button_text = t('Insight');
      }
      else {
        $button_text = $preview['name'];
      }
      $preview_link = l(
        '<i class="fa fa-bar-chart"></i> ' . $button_text,
        $preview['url'],
        array(
          'html' => TRUE,
          'attributes' => array(
            'class' => array('btn', 'btn-primary'),
          ),
        )
      );
    }
  }

  // Check buttons access.
  $blank_button = '&emsp;&emsp;&emsp;&emsp;&emsp;&ensp;&nbsp;';
  switch (dss_visualizations_button_access($resource->nid)) {
    case 0:
      // No buttons are displayed.
      $time_link = $blank_button;
      $geo_link = $blank_button;
      break;

    case 1:
      $geo_link = $blank_button;
      break;

    case 2:
      $time_link = $blank_button;
  }

  // Set results.
  $link = $preview_link . '&nbsp;' . $time_link . '&nbsp;' . $geo_link;
  return $link;

}

/**
 * Theme function to create comparison teaser links on Datasets.
 */
function theme_dss_interface_search_results_comparison_link($vars) {
  $node_list = $vars['nodes'];
  $geo_url = 'comparison-analysis/' . $node_list . '/space-comparison';
  $time_url = 'comparison-analysis/' . $node_list . '/time-comparison';
  $geo_link = l(
    '<i class="fa fa-map"></i> ' . t('Space'),
    $geo_url,
    array(
      'html' => TRUE,
      'attributes' => array(
        'class' => array('btn', 'btn-primary', 'data-link'),
      ),
    )
  );
  $time_link = l(
    '<i class="fa fa-clock-o"></i> ' . t('Time'),
    $time_url,
    array(
      'html' => TRUE,
      'attributes' => array(
        'class' => array('btn', 'btn-primary', 'data-link'),
      ),
    )
  );

  // Check buttons access.
  $blank_button = '&emsp;&emsp;&emsp;&emsp;&emsp;&ensp;&nbsp;';
  switch (dss_visualizations_button_access($node_list)) {
    case 0:
      // No buttons are displayed.
      $time_link = $blank_button;
      $geo_link = $blank_button;
      break;

    case 1:
      $geo_link = $blank_button;
      break;

    case 2:
      $time_link = $blank_button;
  }

  // Set results.
  $link = '&nbsp;&nbsp;' . $time_link . '&nbsp;' . $geo_link;
  return $link;
}

/**
 * Theme function for comparative analysis link.
 */
function theme_dss_interface_comparison_link($vars) {

  $desc = $vars['desc'];
  $output = '<div property="dcat:Distribution">';
  $format = '<span data-toggle="tooltip" data-placement="top" data-original-title="' . $vars['format'] . '" class="format-label" property="dc:format" data-format="' . $vars['format'] . '">' . $vars['format'] . '</span>';
  $title = '<div class="heading">' . $vars['title'] . '</div>';
  $output .= $title . $format . $desc . '<span class="links">' . $vars['link'] . '</span></div>';

  return $output;
}
