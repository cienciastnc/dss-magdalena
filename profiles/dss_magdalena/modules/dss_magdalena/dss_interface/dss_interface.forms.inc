<?php
/**
 * @file
 * Implements changes on interface forms.
 */

use Drupal\dss_magdalena\DSS\Entity\Ficha\SimaFichaWetlandsManagement;
use Drupal\dss_magdalena\DSS\Entity\SimaProductiveLandUseProjectVariant;
use Drupal\dss_magdalena\DSS\Entity\Multifield\SimaSimpleLandUsePercentages;
use Drupal\dss_magdalena\DSS\Entity\SimaWaterBalance;
use Drupal\dss_magdalena\DSS\Entity\Term\SimaLandUseType;
use Drupal\dss_magdalena\DSS\Entity\SimaCatchments;

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Disables field elements in the Wetlands Management Project Variations form
 * depending on what project typology we are creating a variation from.
 */
function dss_interface_form_wetlands_management_variations_node_form_alter(&$form, &$form_state, $form_id) {
  $wetlands_proj_id = isset($form['field_wet_mgmt_project_ref'][LANGUAGE_NONE]['#default_value'][0]) ? $form['field_wet_mgmt_project_ref'][LANGUAGE_NONE]['#default_value'][0] : FALSE;
  if (is_numeric($wetlands_proj_id)) {
    if ($wetlands_project = SimaFichaWetlandsManagement::load(intval($wetlands_proj_id))) {
      $typology = $wetlands_project->getTypology();
      switch ($typology) {
        case SimaFichaWetlandsManagement::TYPE_RIVER_FLOODING_THRESHOLD:
          $form['field_flood_return_fraction_v']['#disabled'] = TRUE;
          $form['field_river_flooding_fraction_v']['#disabled'] = TRUE;
          break;

        case SimaFichaWetlandsManagement::TYPE_RIVER_FLOODING_FRACTION:
          $form['field_river_flooding_threshold_v']['#disabled'] = TRUE;
          $form['field_flood_return_fraction_v']['#disabled'] = TRUE;
          break;

        case SimaFichaWetlandsManagement::TYPE_FLOOD_RETURN_FRACTION:
          $form['field_river_flooding_threshold_v']['#disabled'] = TRUE;
          $form['field_river_flooding_fraction_v']['#disabled'] = TRUE;
          break;
      }
    }
  }
  // Making sure it has a form token.
  if (empty($form_state['input']['form_token'])) {
    $form_state['input']['form_token'] = drupal_get_token($form['#token']);
  }
}

/**
 * Implements hook_node_view_alter().
 */
function dss_interface_node_view_alter(&$build) {
  if ($build['#view_mode'] == 'search_result' && $build['#bundle'] == 'dataset') {
    $build['resources']['#theme'] = 'dss_interface_resource_view';
    $build['#attached']['css'][] = drupal_get_path('module', 'dss_interface') . '/css/dataset-search_results.css';
  }
  if ($build['#bundle'] == 'water_balance') {
    $case_study_id = $build['field_case_study']['#items'][0]['target_id'];
    $model_id = $build['field_model']['#items'][0]['target_id'];
    $water_balance_type_id = $build['field_water_balance_type']['#items'][0]['tid'];
    $html = dss_interface_get_water_balance_template_output($water_balance_type_id, $model_id, $case_study_id);
    $build['field_water_balance_type']['#suffix'] = "<div id='water_balance_variables'>" . $html . "</div>";
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function dss_interface_form_water_balance_node_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'water_balance_node_form') {
    $form['field_water_balance_type'][LANGUAGE_NONE]['#ajax'] = array(
      'callback' => 'dss_interface_show_water_balance_resources',
      'method'  => 'replace',
      'wrapper' => 'water_balance_variables',
    );
    $form['field_case_study'][LANGUAGE_NONE]['#ajax'] = array(
      'callback' => 'dss_interface_show_water_balance_resources',
      'method'   => 'replace',
      'wrapper'  => 'water_balance_variables',
    );

    $form['#validate'][] = 'dss_interface_form_water_balance_node_form_validate';

    // Modify options for model select(Solo Hidrología).
    $options = $form['field_model'][LANGUAGE_NONE]['#options'];
    $new_options = [];
    foreach ($options as $key => $option_value) {
      if ($option_value == 'Hidrologia') {
        $new_options[$key] = $option_value;
      }
    }
    $form['field_model'][LANGUAGE_NONE]['#options'] = $new_options;

    $water_balance_type_id = isset($form['field_water_balance_type'][LANGUAGE_NONE]['#default_value'][0]) ? $form['field_water_balance_type'][LANGUAGE_NONE]['#default_value'][0] : NULL;
    $case_study_id = isset($form['field_case_study'][LANGUAGE_NONE]['#default_value'][0]) ? $form['field_case_study'][LANGUAGE_NONE]['#default_value'][0] : NULL;
    $model_id = isset($form['field_model'][LANGUAGE_NONE]['#default_value'][0]) ? $form['field_model'][LANGUAGE_NONE]['#default_value'][0] : NULL;
    $html = dss_interface_get_water_balance_template_output($water_balance_type_id, $model_id, $case_study_id);
    $form['field_case_study']['#suffix'] = "<div id='water_balance_variables'>" . $html . "</div>";
  }
}

/**
 * Implements hook_form_FORM_ID_validate().
 */
function dss_interface_form_water_balance_node_form_validate(&$form, &$form_state) {

  $water_balance_type_id = isset($form_state['values']['field_water_balance_type'][LANGUAGE_NONE]['0']['tid']) ? $form_state['values']['field_water_balance_type'][LANGUAGE_NONE]['0']['tid'] : NULL;
  $case_study_id = isset($form_state['values']['field_case_study'][LANGUAGE_NONE]['0']['target_id']) ? $form_state['values']['field_case_study'][LANGUAGE_NONE]['0']['target_id'] : NULL;
  $model_id = isset($form_state['values']['field_model'][LANGUAGE_NONE]['0']['target_id']) ? $form_state['values']['field_model'][LANGUAGE_NONE]['0']['target_id'] : NULL;

  $html = dss_interface_get_water_balance_template_output($water_balance_type_id, $model_id, $case_study_id);
  $form['field_case_study']['#suffix'] = "<div id='water_balance_variables'>" . $html . "</div>";

  $resources_list = SimaWaterBalance::listWaterBalanceResources($water_balance_type_id, $model_id, $case_study_id);

  if (empty($resources_list['input']) && empty($resources_list['output']) && empty($resources_list['storage']) && empty($resources_list['consumtion'])) {
    form_set_error(t('Empty'), t('Undefined Variables.'));
  }
  else {
    $is_ok = TRUE;
    foreach ($resources_list as $type => $resources) {
      foreach ($resources as $resource) {
        if ($resource) {
          if (!$resource->isDataStoreImported()) {
            $is_ok = FALSE;
            break;
          }
        }
      }
      if (!$is_ok) {
        break;
      }
    }
    if (!$is_ok) {
      $html = dss_interface_get_water_balance_template_validate_output($water_balance_type_id, $model_id, $case_study_id);
      form_set_error(t('List of Resources that have not been imported to the Datastore.'), $html);
    }
  }
}

/**
 * Generate html for Water Balance variables.
 */
function dss_interface_show_water_balance_resources($form, $form_state) {
  $output = '';
  if ((!is_null($form))&&(!is_null($form_state))) {
    $water_balance_type_id = isset($form_state['values']['field_water_balance_type'][LANGUAGE_NONE]['0']['tid']) ? $form_state['values']['field_water_balance_type'][LANGUAGE_NONE]['0']['tid'] : NULL;
    $case_study_id = isset($form_state['values']['field_case_study'][LANGUAGE_NONE]['0']['target_id']) ? $form_state['values']['field_case_study'][LANGUAGE_NONE]['0']['target_id'] : NULL;
    $model_id = isset($form_state['values']['field_model'][LANGUAGE_NONE]['0']['target_id']) ? $form_state['values']['field_model'][LANGUAGE_NONE]['0']['target_id'] : NULL;
    $html = dss_interface_get_water_balance_template_output($water_balance_type_id, $model_id, $case_study_id);
    $output = "<div id='water_balance_variables'>" . $html . "</div>";
  }
  return $output;
}

/**
 * Generate html for water balance variables.
 *
 * @param int $water_balance_type_id
 *   The Water Balance Type Id.
 * @param int $model_id
 *   The Model Id.
 * @param int $case_study_id
 *   The Case Study Id.
 *
 * @return string
 *   The HTML.
 */
function dss_interface_get_water_balance_template_output($water_balance_type_id, $model_id, $case_study_id) {
  if (empty($water_balance_type_id) || empty($model_id) || empty($case_study_id)) {
    return '';
  }
  $resources_list = SimaWaterBalance::listWaterBalanceResources($water_balance_type_id, $model_id, $case_study_id);
  $html = theme('dss_engine_water_balance_variables',
    array(
      'title' => t('Variables'),
      'data_variables' => $resources_list,
    ));
  return $html;
}

/**
 * Generate html for water balance variables not imported.
 *
 * @param int $water_balance_type_id
 *   The Water Balance Type Id.
 * @param int $model_id
 *   The Model Id.
 * @param int $case_study_id
 *   The Case Study Id.
 *
 * @return string
 *   The HTML.
 */
function dss_interface_get_water_balance_template_validate_output($water_balance_type_id, $model_id, $case_study_id) {
  if (empty($water_balance_type_id) || empty($model_id) || empty($case_study_id)) {
    return '';
  }
  $resources_list = SimaWaterBalance::listWaterBalanceResources($water_balance_type_id, $model_id, $case_study_id);
  $html = theme('dss_engine_water_balance_not_imported',
    array(
      'title' => t('List of Resources that have not been imported to the Datastore.'),
      'data_variables' => $resources_list,
    ));
  return $html;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function dss_interface_form_productive_land_use_variations_node_form_alter(&$form, &$form_state, $form_id) {
  $node = $form_state['node'];
  if ($prod_land_use_variant = SimaProductiveLandUseProjectVariant::load($node)) {

    // Obtaining disabled land types.
    $disabled_landtypes = array_filter([
      SimaLandUseType::loadByTaxonomyTermName(SimaLandUseType::NAME_URBANO),
      SimaLandUseType::loadByTaxonomyTermName(SimaLandUseType::NAME_CUERPO_AGUA),
      SimaLandUseType::loadByTaxonomyTermName(SimaLandUseType::NAME_CUERPOAGUA),
    ]);
    $disabled_tids = [];
    foreach ($disabled_landtypes as $land_type) {
      $disabled_tids[] = $land_type->getId();
    }

    // Setting up initial form values for Catchment Area.
    $form[SimaProductiveLandUseProjectVariant::FIELD_CATCHMENT_AREA][LANGUAGE_NONE][0]['value']['#default_value'] = number_format($prod_land_use_variant->getCatchmentArea(), 2, '.', '');

    // Disabling input text for Catchment Areas and breakdown areas.
    $form[SimaProductiveLandUseProjectVariant::FIELD_CATCHMENT_AREA][LANGUAGE_NONE][0]['value']['#attributes']['readonly'] = 'readonly';
    $form[SimaProductiveLandUseProjectVariant::FIELD_AGRICULTURAL_AREA][LANGUAGE_NONE][0]['value']['#attributes']['readonly'] = 'readonly';
    $form[SimaProductiveLandUseProjectVariant::FIELD_FOREST_INDUSTRIAL_AREA][LANGUAGE_NONE][0]['value']['#attributes']['readonly'] = 'readonly';
    $form[SimaProductiveLandUseProjectVariant::FIELD_MINING_AREA][LANGUAGE_NONE][0]['value']['#attributes']['readonly'] = 'readonly';
    $form[SimaProductiveLandUseProjectVariant::FIELD_PERCENTAGE_OF_IRRIGATED_AREA][LANGUAGE_NONE][0]['value']['#attributes']['readonly'] = 'readonly';

    // Disabling certain Land Types.
    foreach ($form[SimaProductiveLandUseProjectVariant::FIELD_LAND_USE][LANGUAGE_NONE] as $key => $item) {
      if (is_numeric($key) && in_array($item[SimaSimpleLandUsePercentages::FIELD_LAND_USE_TYPE][LANGUAGE_NONE]['#default_value'][0], $disabled_tids)) {
        $form[SimaProductiveLandUseProjectVariant::FIELD_LAND_USE][LANGUAGE_NONE][$key][SimaSimpleLandUsePercentages::FIELD_PERCENTAGE_SHARE][LANGUAGE_NONE][0]['value']['#attributes']['readonly'] = 'readonly';
      }
    }

    // Disabling % Irrigated Area.
    if ($catchment = SimaCatchments::load($node->__extra['catchment'])) {
      if (!$catchment->getConnectionToWaterSource()) {
        // If there is no connection to water source then there is no irrigated
        // area.
        foreach ($form[SimaProductiveLandUseProjectVariant::FIELD_LAND_USE][LANGUAGE_NONE] as $key => $item) {
          if (is_numeric($key)) {
            $form[SimaProductiveLandUseProjectVariant::FIELD_LAND_USE][LANGUAGE_NONE][$key]['field_irr_perc_area'][LANGUAGE_NONE][0]['value']['#attributes']['readonly'] = 'readonly';
          }
        }
      }
    }

    // Making sure it has a form token.
    if (empty($form_state['input']['form_token'])) {
      $form_state['input']['form_token'] = drupal_get_token($form['#token']);
    }
  }
}
