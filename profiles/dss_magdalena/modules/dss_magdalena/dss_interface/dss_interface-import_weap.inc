<?php
/**
 * @file
 * Defines the functions to import WEAP outputs.
 */

use Drupal\dss_magdalena\DSS\Queue\SimaImportQueue;
use Drupal\dss_magdalena\WeapController;
use Drupal\dss_magdalena\DSS\Entity\SimaController;
use Drupal\dss_magdalena\DSS\Entity\SimaCaseStudy;
use Drupal\dss_magdalena\DSS\Entity\SimaModel;

/**
 * Implements hook_form().
 */
function dss_interface_import_weap_outputs_form($form, &$form_state) {
  $form = array();
  $form['weap_import'] = array(
    '#title' => t('Case Studies'),
    '#type' => 'fieldset',
    'settings' => array('#tree' => TRUE),
  );

  $form['weap_import']['cases_available'] = array(
    '#title' => t('Cases Available for Import'),
    '#type' => 'fieldset',
    'settings' => array('#tree' => TRUE),
  );

  // Defining table for listing Sima Import case items.
  $header_cases = array(
    'id' => array('data' => t('id')),
    'name' => array('data' => t('Title')),
    'status' => array('data' => t('Case Status')),
    'import_status' => array('data' => t('Import Status')),
  );

  // Verifying that we have properly configured WebDAV Connection Settings.
  $weap_connector = new WeapController();
  if (!$weap_connector->checkConnectionSettings()) {
    $cases = FALSE;
  }
  else {
    $cases = dss_interface_get_case_studies_available_for_import();
    foreach ($cases as $cid) {

      $case_study = SimaCaseStudy::load($cid);
      $title = $case_study->get('title');
      $status = $case_study->getModelingStatus(SimaModel::MODEL_WEAP, TRUE);
      $import_status = $case_study->getImportStatus(SimaModel::MODEL_WEAP);

      $options[$cid] = array(
        'id' => array('data' => $cid),
        'name' => array('data' => $title),
        'status' => array('data' => $status),
        'import_status' => array('data' => $import_status),
      );
    }
  }

  if (!empty($cases)) {
    $form['weap_import']['cases_available']['message'] = array(
      '#markup' => t('Select all cases you want to import. If you select a case previously imported, only the missing resource outputs will be added. Note that only the cases with "PROCESSING" status will be listed here.'),
    );
    $form['weap_import']['cases_available']['cases'] = array(
      '#type' => 'tableselect',
      '#header' => $header_cases,
      // '#options' => drupal_map_assoc($cases),
      '#options' => $options,
      '#description' => t('Select all the case outputs you want to import.'),
    );
  }
  else {
    if ($cases === FALSE) {
      $message = t('SIMA cannot connect to WEAP Server. Please review your !connection_settings', array(
        '!connection_settings' => l(t('Connection Settings'), 'admin/config/dss-magdalena/settings'),
      ));
    }
    else {
      $message = t('There are no cases available for import. If you already !exported, you might need to wait until it is ready before you can import the results.', array(
        '!exported' => l(t('Exported a Case Study'), 'case-study'),
      ));
    }
    $form['weap_import']['cases_available']['message'] = array(
      '#markup' => $message,
    );
  }

  $form['weap_import']['process_datastore'] = array(
    '#type' => 'radios',
    '#title' => t('Process Datastore'),
    '#description' => t('Do you want to process the Datastore in WEAP Resources (Cajas de Datos) after importing them? This can take a lot of time. Processing datastore allows to parse the resource (output variable file) and store its contents in the database. If it is not selected, it can be processed later.'),
    '#default_value' => variable_get('dss_interface_sima_import_queue_process_datastore', 0),
    '#options' => array(
      t('No'),
      t('Yes'),
    ),
  );

  // Defining table for listing Sima Import Queue items.
  $header = array(
    'name' => array('data' => t('Name')),
    'title' => array('data' => t('Title')),
    'items' => array('data' => t('Number of items')),
    'class' => array('data' => t('Class')),
    'inspect' => array('data' => t('Inspect')),
    // 'operations' => array('data' => t('Operations')),.
  );

  // Piece taken from queue_ui module.
  // Get queues defined via queue_ui hook.
  $defined_queues = queue_ui_defined_queues();
  // Get queues names.
  $queues = dss_interface_get_queues();

  foreach ($queues as $class => $classed_queues) {
    $options = array();

    $class_info = QueueUI::get('QueueUI' . $class);

    // Output information for each queue of the current class.
    foreach ($classed_queues as $name => $queue) {
      $title = '';
      $inspect = FALSE;

      if (isset($defined_queues[$name])) {
        $title = $defined_queues[$name]['title'];
      }
      if (is_object($class_info) && $class_info->inspect) {
        $inspect = TRUE;
      }

      $options[$name] = array(
        'name' => array('data' => $name),
        'title' => array('data' => $title),
        'items' => array('data' => $queue['items']),
        'class' => array('data' => $class),
      );

      // If queue inspection is enabled for this class, add to options array.
      if ($inspect && $queue['items'] > 0) {
        $options[$name]['inspect'] = array('data' => l(t('Inspect'), QUEUE_UI_BASE . "/inspect/$name"));
      }
      else {
        $options[$name]['inspect'] = '';
      }
    }

    $form[$class] = array(
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#title' => t('WEAP Import Queue'),
      '#description' => t('When importing WEAP Outputs, they are not directly imported in the system but put in an importing queue. You can decide to process the queue by selecting the checkbox below.'),
    );

    $form[$class]['queues_' . $class] = array(
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options,
      '#empty' => t('No queues exist for @class.', array('@class' => $class)),
    );
  }

  // @todo deactivate options
  // Option to run batch.
  $form['batch'] = array(
    '#type' => 'submit',
    '#value' => t('Batch process'),
  );

  return $form;
}

/**
 * Implements hook_form_submit().
 */
function dss_interface_import_weap_outputs_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $queues = [SimaImportQueue::QUEUE_NAME];
  // Switch off submitted action.
  switch ($values['op']) {
    case $values['batch']:
      // Process queue(s) with batch.
      // We can only run batch on queues using our hook_queue_info() that
      // define batch.
      $defined_queues = queue_ui_defined_queues();
      $intersect = array_intersect(array_keys($defined_queues), $queues);
      $operations = array();

      // Including operations for the importing of selected cases.
      if (isset($values['cases']) && count($values['cases']) > 0) {
        $cases = array_filter(array_values($form_state['values']['cases']));
        $operations[] = array('dss_engine_sima_import_case_studies', array($cases));
      }

      // Do we want to process the datastore?
      $process_datastore = boolval($values['process_datastore']);

      foreach ($intersect as $name) {
        // Only if queue_info implementation defined batch can we set it here.
        if (isset($defined_queues[$name]['batch'])) {
          $batch = $defined_queues[$name]['batch'];
          // Add queue as argument to operations by resetting the operations
          // array.
          $queue_list = $values['queues_SystemQueue'];

          // Only process queue worker if the queue has been selected.
          if (!boolval($queue_list['dss_engine_sima_import_queue'])) {
            unset($batch['operations'][0]);
            unset($batch['finished']);
          }

          $queue = DrupalQueue::get($name);
          foreach ($batch['operations'] as $operation) {
            // First element is the batch process callback, second is the
            // argument.
            $operations[] = array(
              $operation[0],
              array_merge(array($queue, $process_datastore), $operation[1]),
            );
          }
          $batch['operations'] = $operations;
          $batch['file'] = drupal_get_path('module', 'dss_engine') . '/dss_engine.queue.inc';

          // Set.
          batch_set($batch);
        }
      }
      break;
  }
}

/**
 * Get queues. Taken from queue_ui.
 *
 * @return array
 *   Array of queues indexed by name and containing queue object and number
 *   of items.
 */
function dss_interface_get_queues() {
  $queues = array();
  $queue_names = [(object) [
    'name' => SimaImportQueue::QUEUE_NAME,
  ],
  ];
  // $queue_names = queue_ui_queue_names();
  if (!empty($queue_names)) {
    // Build array of queues indexed by name with number of items.
    foreach ($queue_names as $name) {
      $queue = DrupalQueue::get($name->name);
      $class = get_class($queue);
      $queues[$class][$name->name] = array(
        'queue' => $queue,
        'items' => $queue->numberOfItems(),
      );
    }
  }
  return $queues;
}

/**
 * Lists Cases Available for Import.
 *
 * @return array
 *   An array of Cases.
 */
function dss_interface_get_case_studies_available_for_import() {
  $sima_controller = new SimaController();
  $available_cases = $sima_controller->listEntities(SimaCaseStudy::BUNDLE);

  // Unset all cases whose WEAP modeling status is different than 'PROCESSING'.
  /** @var \Drupal\dss_magdalena\DSS\Entity\SimaCaseStudy $case */
  foreach ($available_cases as $cid => $case) {
    if (!$case->isProcessingWeapModelingStatus() && !$case->isOkWeapModelingStatus()) {
      unset($available_cases[$cid]);
    }
  }

  $weap_connector = new WeapController();
  $webdav_cases = $weap_connector->listCaseStudiesAvailableForImport();
  return array_keys(array_intersect_key($available_cases, $webdav_cases));
}
