<?php
/**
 * @file
 * Integral Plans views.
 */

$view = new view();
$view->name = 'integral_plan_list';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Integral Plan List';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Integral Plan List';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '20';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['pager']['options']['id'] = '0';
$handler->display->display_options['pager']['options']['quantity'] = '9';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'title' => 'title',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'title' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
/* Field: Entity Reference View Widget Checkbox: Content */
$handler->display->display_options['fields']['entityreference_view_widget']['id'] = 'entityreference_view_widget';
$handler->display->display_options['fields']['entityreference_view_widget']['table'] = 'node';
$handler->display->display_options['fields']['entityreference_view_widget']['field'] = 'entityreference_view_widget';
$handler->display->display_options['fields']['entityreference_view_widget']['label'] = '';
$handler->display->display_options['fields']['entityreference_view_widget']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['entityreference_view_widget']['ervw']['force_single'] = 1;
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
/* Field: Content: Hydropower Alternatives */
$handler->display->display_options['fields']['field_hydropower_alternatives']['id'] = 'field_hydropower_alternatives';
$handler->display->display_options['fields']['field_hydropower_alternatives']['table'] = 'field_data_field_hydropower_alternatives';
$handler->display->display_options['fields']['field_hydropower_alternatives']['field'] = 'field_hydropower_alternatives';
$handler->display->display_options['fields']['field_hydropower_alternatives']['label'] = 'Hydropower';
$handler->display->display_options['fields']['field_hydropower_alternatives']['type'] = 'entityreference_entity_view';
$handler->display->display_options['fields']['field_hydropower_alternatives']['settings'] = array(
  'view_mode' => 'default',
  'links' => 1,
);
$handler->display->display_options['fields']['field_hydropower_alternatives']['delta_offset'] = '0';
/* Field: Broken/missing handler */
$handler->display->display_options['fields']['field_agricultural_alternatives']['id'] = 'field_agricultural_alternatives';
$handler->display->display_options['fields']['field_agricultural_alternatives']['table'] = 'field_data_field_agricultural_alternatives';
$handler->display->display_options['fields']['field_agricultural_alternatives']['field'] = 'field_agricultural_alternatives';
$handler->display->display_options['fields']['field_agricultural_alternatives']['label'] = 'Agricultural';
/* Field: Content: Forest Exploitation Alternatives */
$handler->display->display_options['fields']['field_forest_alternatives']['id'] = 'field_forest_alternatives';
$handler->display->display_options['fields']['field_forest_alternatives']['table'] = 'field_data_field_forest_alternatives';
$handler->display->display_options['fields']['field_forest_alternatives']['field'] = 'field_forest_alternatives';
$handler->display->display_options['fields']['field_forest_alternatives']['label'] = 'Forest Exploitation';
$handler->display->display_options['fields']['field_forest_alternatives']['type'] = 'entityreference_entity_view';
$handler->display->display_options['fields']['field_forest_alternatives']['settings'] = array(
  'view_mode' => 'default',
  'links' => 1,
);
$handler->display->display_options['fields']['field_forest_alternatives']['delta_offset'] = '0';
/* Field: Content: Mining Exploitation Alternatives */
$handler->display->display_options['fields']['field_mining_alternatives']['id'] = 'field_mining_alternatives';
$handler->display->display_options['fields']['field_mining_alternatives']['table'] = 'field_data_field_mining_alternatives';
$handler->display->display_options['fields']['field_mining_alternatives']['field'] = 'field_mining_alternatives';
$handler->display->display_options['fields']['field_mining_alternatives']['label'] = 'Mining Exploitation';
$handler->display->display_options['fields']['field_mining_alternatives']['type'] = 'entityreference_entity_view';
$handler->display->display_options['fields']['field_mining_alternatives']['settings'] = array(
  'view_mode' => 'default',
  'links' => 1,
);
$handler->display->display_options['fields']['field_mining_alternatives']['delta_offset'] = '0';
/* Field: Content: Wetland Management Alternatives */
$handler->display->display_options['fields']['field_connectivity_alternatives']['id'] = 'field_connectivity_alternatives';
$handler->display->display_options['fields']['field_connectivity_alternatives']['table'] = 'field_data_field_connectivity_alternatives';
$handler->display->display_options['fields']['field_connectivity_alternatives']['field'] = 'field_connectivity_alternatives';
$handler->display->display_options['fields']['field_connectivity_alternatives']['label'] = 'River-Floodplain Connectivity';
$handler->display->display_options['fields']['field_connectivity_alternatives']['type'] = 'entityreference_entity_view';
$handler->display->display_options['fields']['field_connectivity_alternatives']['settings'] = array(
  'view_mode' => 'default',
  'links' => 1,
);
$handler->display->display_options['fields']['field_connectivity_alternatives']['delta_offset'] = '0';
/* Sort criterion: Content: Post date */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'node';
$handler->display->display_options['sorts']['created']['field'] = 'created';
$handler->display->display_options['sorts']['created']['order'] = 'DESC';
/* Contextual filter: Content: Nid */
$handler->display->display_options['arguments']['nid']['id'] = 'nid';
$handler->display->display_options['arguments']['nid']['table'] = 'node';
$handler->display->display_options['arguments']['nid']['field'] = 'nid';
$handler->display->display_options['arguments']['nid']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'integral_plan' => 'integral_plan',
);

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['defaults']['header'] = FALSE;
/* Header: Global: Text area */
$handler->display->display_options['header']['area']['id'] = 'area';
$handler->display->display_options['header']['area']['table'] = 'views';
$handler->display->display_options['header']['area']['field'] = 'area';
$handler->display->display_options['header']['area']['content'] = '<h3><a href="node/add/integral-plan">New Integral Plan</a></h3>';
$handler->display->display_options['header']['area']['format'] = 'filtered_html';
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
/* Field: Content: Hydropower Alternatives */
$handler->display->display_options['fields']['field_hydropower_alternatives']['id'] = 'field_hydropower_alternatives';
$handler->display->display_options['fields']['field_hydropower_alternatives']['table'] = 'field_data_field_hydropower_alternatives';
$handler->display->display_options['fields']['field_hydropower_alternatives']['field'] = 'field_hydropower_alternatives';
$handler->display->display_options['fields']['field_hydropower_alternatives']['label'] = 'Hydropower';
$handler->display->display_options['fields']['field_hydropower_alternatives']['type'] = 'entityreference_entity_view';
$handler->display->display_options['fields']['field_hydropower_alternatives']['settings'] = array(
  'view_mode' => 'sima',
  'links' => 1,
  'custom_link_to_entity' => 0,
  'custom_prefix' => '',
  'custom_suffix' => '',
  'custom_reverse' => 0,
  'custom_trim' => 0,
  'custom_strtolower' => 0,
  'custom_strtoupper' => 0,
  'custom_ucfirst' => 0,
  'custom_ucwords' => 0,
  'custom_strip_tags' => 0,
  'custom_strip_preserve' => '',
);
$handler->display->display_options['fields']['field_hydropower_alternatives']['delta_offset'] = '0';
/* Field: Content: Productive Land Use Alternatives */
$handler->display->display_options['fields']['field_prod_landuse_alternatives']['id'] = 'field_prod_landuse_alternatives';
$handler->display->display_options['fields']['field_prod_landuse_alternatives']['table'] = 'field_data_field_prod_landuse_alternatives';
$handler->display->display_options['fields']['field_prod_landuse_alternatives']['field'] = 'field_prod_landuse_alternatives';
$handler->display->display_options['fields']['field_prod_landuse_alternatives']['type'] = 'entityreference_entity_view';
$handler->display->display_options['fields']['field_prod_landuse_alternatives']['settings'] = array(
  'view_mode' => 'sima',
  'links' => 1,
  'custom_link_to_entity' => 0,
  'custom_prefix' => '',
  'custom_suffix' => '',
  'custom_reverse' => 0,
  'custom_trim' => 0,
  'custom_strtolower' => 0,
  'custom_strtoupper' => 0,
  'custom_ucfirst' => 0,
  'custom_ucwords' => 0,
  'custom_strip_tags' => 0,
  'custom_strip_preserve' => '',
);
$handler->display->display_options['fields']['field_prod_landuse_alternatives']['delta_offset'] = '0';
/* Field: Content: Wetland Management Alternatives */
$handler->display->display_options['fields']['field_connectivity_alternatives']['id'] = 'field_connectivity_alternatives';
$handler->display->display_options['fields']['field_connectivity_alternatives']['table'] = 'field_data_field_connectivity_alternatives';
$handler->display->display_options['fields']['field_connectivity_alternatives']['field'] = 'field_connectivity_alternatives';
$handler->display->display_options['fields']['field_connectivity_alternatives']['label'] = 'Wetlands Management';
$handler->display->display_options['fields']['field_connectivity_alternatives']['type'] = 'entityreference_entity_view';
$handler->display->display_options['fields']['field_connectivity_alternatives']['settings'] = array(
  'view_mode' => 'sima',
  'links' => 1,
  'custom_link_to_entity' => 0,
  'custom_prefix' => '',
  'custom_suffix' => '',
  'custom_reverse' => 0,
  'custom_trim' => 0,
  'custom_strtolower' => 0,
  'custom_strtoupper' => 0,
  'custom_ucfirst' => 0,
  'custom_ucwords' => 0,
  'custom_strip_tags' => 0,
  'custom_strip_preserve' => '',
);
$handler->display->display_options['fields']['field_connectivity_alternatives']['delta_offset'] = '0';
$handler->display->display_options['defaults']['arguments'] = FALSE;
$handler->display->display_options['path'] = 'integral-plan-list';

/* Display: Entity Reference View Widget */
$handler = $view->new_display('entityreference_view_widget', 'Entity Reference View Widget', 'entityreference_view_widget_integral_plan');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['defaults']['use_ajax'] = FALSE;
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'entityreference_view_widget' => 'entityreference_view_widget',
  'title' => 'title',
  'field_hydropower_alternatives' => 'field_hydropower_alternatives',
  'field_agricultural_alternatives' => 'field_agricultural_alternatives',
  'field_forest_alternatives' => 'field_forest_alternatives',
  'field_mining_alternatives' => 'field_mining_alternatives',
  'field_connectivity_alternatives' => 'field_connectivity_alternatives',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'entityreference_view_widget' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'title' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_hydropower_alternatives' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_agricultural_alternatives' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_forest_alternatives' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_mining_alternatives' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_connectivity_alternatives' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['defaults']['row_options'] = FALSE;
$translatables['integral_plan_list'] = array(
  t('Master'),
  t('Integral Plan List'),
  t('more'),
  t('Apply'),
  t('Reset'),
  t('Sort by'),
  t('Asc'),
  t('Desc'),
  t('Items per page'),
  t('- All -'),
  t('Offset'),
  t('« first'),
  t('‹ previous'),
  t('next ›'),
  t('last »'),
  t('Hydropower'),
  t('Agricultural'),
  t('Forest Exploitation'),
  t('Mining Exploitation'),
  t('River-Floodplain Connectivity'),
  t('All'),
  t('Page'),
  t('<h3><a href="node/add/integral-plan">New Integral Plan</a></h3>'),
  t('Productive Land Use Alternatives'),
  t('Wetlands Management'),
  t('Entity Reference View Widget'),
);
