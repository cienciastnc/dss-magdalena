<?php
/**
 * @file
 * Views for scenarios.
 */

$view = new view();
$view->name = 'scenario_list';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Scenario List';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Scenario List';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '10';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['pager']['options']['id'] = '0';
$handler->display->display_options['pager']['options']['quantity'] = '9';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['grouping'] = array(
  0 => array(
    'field' => 'field_scenario',
    'rendered' => 1,
    'rendered_strip' => 1,
  ),
);
$handler->display->display_options['style_options']['columns'] = array(
  'title' => 'title',
  'field_climate_scenario' => 'field_climate_scenario',
  'field_energy_scenario' => 'field_energy_scenario',
  'field_population_scenario' => 'field_population_scenario',
  'field_scenario' => 'field_scenario',
);
$handler->display->display_options['style_options']['default'] = 'field_scenario';
$handler->display->display_options['style_options']['info'] = array(
  'title' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_climate_scenario' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_energy_scenario' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_population_scenario' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_scenario' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 1,
  ),
);
/* Header: Global: Text area */
$handler->display->display_options['header']['area']['id'] = 'area';
$handler->display->display_options['header']['area']['table'] = 'views';
$handler->display->display_options['header']['area']['field'] = 'area';
/* Field: Entity Reference View Widget Checkbox: Content */
$handler->display->display_options['fields']['entityreference_view_widget']['id'] = 'entityreference_view_widget';
$handler->display->display_options['fields']['entityreference_view_widget']['table'] = 'node';
$handler->display->display_options['fields']['entityreference_view_widget']['field'] = 'entityreference_view_widget';
$handler->display->display_options['fields']['entityreference_view_widget']['label'] = '';
$handler->display->display_options['fields']['entityreference_view_widget']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['entityreference_view_widget']['ervw']['force_single'] = 1;
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = 'Scenario';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
/* Field: Content: Climate Scenario */
$handler->display->display_options['fields']['field_climate_scenario']['id'] = 'field_climate_scenario';
$handler->display->display_options['fields']['field_climate_scenario']['table'] = 'field_data_field_climate_scenario';
$handler->display->display_options['fields']['field_climate_scenario']['field'] = 'field_climate_scenario';
$handler->display->display_options['fields']['field_climate_scenario']['label'] = 'Climate';
$handler->display->display_options['fields']['field_climate_scenario']['type'] = 'entityreference_entity_view';
$handler->display->display_options['fields']['field_climate_scenario']['settings'] = array(
  'view_mode' => 'default',
  'links' => 1,
);
$handler->display->display_options['fields']['field_climate_scenario']['delta_offset'] = '0';
/* Field: Content: Energy Scenario */
$handler->display->display_options['fields']['field_energy_scenario']['id'] = 'field_energy_scenario';
$handler->display->display_options['fields']['field_energy_scenario']['table'] = 'field_data_field_energy_scenario';
$handler->display->display_options['fields']['field_energy_scenario']['field'] = 'field_energy_scenario';
$handler->display->display_options['fields']['field_energy_scenario']['label'] = 'Energy';
$handler->display->display_options['fields']['field_energy_scenario']['type'] = 'entityreference_entity_view';
$handler->display->display_options['fields']['field_energy_scenario']['settings'] = array(
  'view_mode' => 'default',
  'links' => 1,
);
$handler->display->display_options['fields']['field_energy_scenario']['delta_offset'] = '0';
/* Field: Content: Population Scenario */
$handler->display->display_options['fields']['field_population_scenario']['id'] = 'field_population_scenario';
$handler->display->display_options['fields']['field_population_scenario']['table'] = 'field_data_field_population_scenario';
$handler->display->display_options['fields']['field_population_scenario']['field'] = 'field_population_scenario';
$handler->display->display_options['fields']['field_population_scenario']['label'] = 'Population';
$handler->display->display_options['fields']['field_population_scenario']['type'] = 'entityreference_entity_view';
$handler->display->display_options['fields']['field_population_scenario']['settings'] = array(
  'view_mode' => 'default',
  'links' => 1,
);
$handler->display->display_options['fields']['field_population_scenario']['delta_offset'] = '0';
/* Sort criterion: Content: Post date */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'node';
$handler->display->display_options['sorts']['created']['field'] = 'created';
$handler->display->display_options['sorts']['created']['order'] = 'DESC';
/* Contextual filter: Content: Nid */
$handler->display->display_options['arguments']['nid']['id'] = 'nid';
$handler->display->display_options['arguments']['nid']['table'] = 'node';
$handler->display->display_options['arguments']['nid']['field'] = 'nid';
$handler->display->display_options['arguments']['nid']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'scenario' => 'scenario',
);

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['defaults']['header'] = FALSE;
/* Header: Global: Text area */
$handler->display->display_options['header']['area']['id'] = 'area';
$handler->display->display_options['header']['area']['table'] = 'views';
$handler->display->display_options['header']['area']['field'] = 'area';
$handler->display->display_options['header']['area']['content'] = '<h3><a href="node/add/scenario">New Scenario</a></h3>';
$handler->display->display_options['header']['area']['format'] = 'filtered_html';
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = 'Scenario';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
/* Field: Content: Climate Scenario */
$handler->display->display_options['fields']['field_climate_scenario']['id'] = 'field_climate_scenario';
$handler->display->display_options['fields']['field_climate_scenario']['table'] = 'field_data_field_climate_scenario';
$handler->display->display_options['fields']['field_climate_scenario']['field'] = 'field_climate_scenario';
$handler->display->display_options['fields']['field_climate_scenario']['label'] = 'Climate';
$handler->display->display_options['fields']['field_climate_scenario']['type'] = 'entityreference_entity_view';
$handler->display->display_options['fields']['field_climate_scenario']['settings'] = array(
  'view_mode' => 'sima',
  'links' => 1,
  'custom_link_to_entity' => 0,
  'custom_prefix' => '',
  'custom_suffix' => '',
  'custom_reverse' => 0,
  'custom_trim' => 0,
  'custom_strtolower' => 0,
  'custom_strtoupper' => 0,
  'custom_ucfirst' => 0,
  'custom_ucwords' => 0,
  'custom_strip_tags' => 0,
  'custom_strip_preserve' => '',
);
$handler->display->display_options['fields']['field_climate_scenario']['delta_offset'] = '0';
/* Field: Content: Energy Scenario */
$handler->display->display_options['fields']['field_energy_scenario']['id'] = 'field_energy_scenario';
$handler->display->display_options['fields']['field_energy_scenario']['table'] = 'field_data_field_energy_scenario';
$handler->display->display_options['fields']['field_energy_scenario']['field'] = 'field_energy_scenario';
$handler->display->display_options['fields']['field_energy_scenario']['label'] = 'Energy';
$handler->display->display_options['fields']['field_energy_scenario']['type'] = 'entityreference_entity_view';
$handler->display->display_options['fields']['field_energy_scenario']['settings'] = array(
  'view_mode' => 'sima',
  'links' => 1,
  'custom_link_to_entity' => 0,
  'custom_prefix' => '',
  'custom_suffix' => '',
  'custom_reverse' => 0,
  'custom_trim' => 0,
  'custom_strtolower' => 0,
  'custom_strtoupper' => 0,
  'custom_ucfirst' => 0,
  'custom_ucwords' => 0,
  'custom_strip_tags' => 0,
  'custom_strip_preserve' => '',
);
$handler->display->display_options['fields']['field_energy_scenario']['delta_offset'] = '0';
/* Field: Content: Population Scenario */
$handler->display->display_options['fields']['field_population_scenario']['id'] = 'field_population_scenario';
$handler->display->display_options['fields']['field_population_scenario']['table'] = 'field_data_field_population_scenario';
$handler->display->display_options['fields']['field_population_scenario']['field'] = 'field_population_scenario';
$handler->display->display_options['fields']['field_population_scenario']['label'] = 'Population';
$handler->display->display_options['fields']['field_population_scenario']['type'] = 'entityreference_entity_view';
$handler->display->display_options['fields']['field_population_scenario']['settings'] = array(
  'view_mode' => 'sima',
  'links' => 1,
  'custom_link_to_entity' => 0,
  'custom_prefix' => '',
  'custom_suffix' => '',
  'custom_reverse' => 0,
  'custom_trim' => 0,
  'custom_strtolower' => 0,
  'custom_strtoupper' => 0,
  'custom_ucfirst' => 0,
  'custom_ucwords' => 0,
  'custom_strip_tags' => 0,
  'custom_strip_preserve' => '',
);
$handler->display->display_options['fields']['field_population_scenario']['delta_offset'] = '0';
$handler->display->display_options['defaults']['arguments'] = FALSE;
$handler->display->display_options['path'] = 'scenario-list';

/* Display: Entity Reference View Widget */
$handler = $view->new_display('entityreference_view_widget', 'Entity Reference View Widget', 'entityreference_view_widget_scenario_list');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['defaults']['use_ajax'] = FALSE;
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'entityreference_view_widget' => 'entityreference_view_widget',
  'title' => 'title',
  'field_climate_scenario' => 'field_climate_scenario',
  'field_energy_scenario' => 'field_energy_scenario',
  'field_population_scenario' => 'field_population_scenario',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'entityreference_view_widget' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'title' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_climate_scenario' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_energy_scenario' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_population_scenario' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['defaults']['row_options'] = FALSE;
$translatables['scenario_list'] = array(
  t('Master'),
  t('Scenario List'),
  t('more'),
  t('Apply'),
  t('Reset'),
  t('Sort by'),
  t('Asc'),
  t('Desc'),
  t('Items per page'),
  t('- All -'),
  t('Offset'),
  t('« first'),
  t('‹ previous'),
  t('next ›'),
  t('last »'),
  t('Scenario'),
  t('Climate'),
  t('Energy'),
  t('Population'),
  t('All'),
  t('Page'),
  t('<h3><a href="node/add/scenario">New Scenario</a></h3>'),
  t('Entity Reference View Widget'),
);
