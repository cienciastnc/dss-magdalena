<?php
/**
 * @file
 * Default views.
 */

/**
 * Implements hook_views_default_views().
 */
function dss_interface_views_default_views() {
  $views_path = drupal_get_path('module', 'dss_interface') . '/views';
  $files = file_scan_directory($views_path, '/.*\.view.inc$/');
  foreach ($files as $filepath => $file) {
    require $filepath;
    if (isset($view)) {
      $views[$view->name] = $view;
    }
  }
  return $views;
}
