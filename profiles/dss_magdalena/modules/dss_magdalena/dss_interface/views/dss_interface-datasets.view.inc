<?php
/**
 * @file
 * Default View for Variables List.
 */

$view = new view();
$view->name = 'variables_list';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Variables List';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Variables List';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '40';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['pager']['options']['id'] = '0';
$handler->display->display_options['pager']['options']['quantity'] = '9';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['grouping'] = array(
  0 => array(
    'field' => 'field_variable_status',
    'rendered' => 1,
    'rendered_strip' => 0,
  ),
  1 => array(
    'field' => 'field_variable_category',
    'rendered' => 1,
    'rendered_strip' => 0,
  ),
);
$handler->display->display_options['style_options']['columns'] = array(
  'nid' => 'nid',
  'title' => 'title',
  'field_variable_category' => 'field_variable_category',
  'field_type' => 'field_type',
  'field_subsystem' => 'field_subsystem',
  'field_model_family' => 'field_model_family',
  'field_node_category' => 'field_node_category',
  'field_variable_status' => 'field_variable_status',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'nid' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'title' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_variable_category' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_type' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_subsystem' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_model_family' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_node_category' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_variable_status' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
/* Field: Content: Nid */
$handler->display->display_options['fields']['nid']['id'] = 'nid';
$handler->display->display_options['fields']['nid']['table'] = 'node';
$handler->display->display_options['fields']['nid']['field'] = 'nid';
$handler->display->display_options['fields']['nid']['label'] = 'ID';
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
/* Field: Content: Variable Category */
$handler->display->display_options['fields']['field_variable_category']['id'] = 'field_variable_category';
$handler->display->display_options['fields']['field_variable_category']['table'] = 'field_data_field_variable_category';
$handler->display->display_options['fields']['field_variable_category']['field'] = 'field_variable_category';
/* Field: Content: Type */
$handler->display->display_options['fields']['field_type']['id'] = 'field_type';
$handler->display->display_options['fields']['field_type']['table'] = 'field_data_field_type';
$handler->display->display_options['fields']['field_type']['field'] = 'field_type';
/* Field: Content: Subsystem */
$handler->display->display_options['fields']['field_subsystem']['id'] = 'field_subsystem';
$handler->display->display_options['fields']['field_subsystem']['table'] = 'field_data_field_subsystem';
$handler->display->display_options['fields']['field_subsystem']['field'] = 'field_subsystem';
/* Field: Content: Model Family */
$handler->display->display_options['fields']['field_model_family']['id'] = 'field_model_family';
$handler->display->display_options['fields']['field_model_family']['table'] = 'field_data_field_model_family';
$handler->display->display_options['fields']['field_model_family']['field'] = 'field_model_family';
/* Field: Content: WEAP Name */
$handler->display->display_options['fields']['field_weap_name']['id'] = 'field_weap_name';
$handler->display->display_options['fields']['field_weap_name']['table'] = 'field_data_field_weap_name';
$handler->display->display_options['fields']['field_weap_name']['field'] = 'field_weap_name';
$handler->display->display_options['fields']['field_weap_name']['label'] = 'WEAP Variable';
/* Field: Content: Variable Status */
$handler->display->display_options['fields']['field_variable_status']['id'] = 'field_variable_status';
$handler->display->display_options['fields']['field_variable_status']['table'] = 'field_data_field_variable_status';
$handler->display->display_options['fields']['field_variable_status']['field'] = 'field_variable_status';
$handler->display->display_options['fields']['field_variable_status']['label'] = 'Variable Type';
$handler->display->display_options['fields']['field_variable_status']['exclude'] = TRUE;
/* Sort criterion: Content: Variable Category (field_variable_category) */
$handler->display->display_options['sorts']['field_variable_category_tid']['id'] = 'field_variable_category_tid';
$handler->display->display_options['sorts']['field_variable_category_tid']['table'] = 'field_data_field_variable_category';
$handler->display->display_options['sorts']['field_variable_category_tid']['field'] = 'field_variable_category_tid';
/* Sort criterion: Content: Nid */
$handler->display->display_options['sorts']['nid']['id'] = 'nid';
$handler->display->display_options['sorts']['nid']['table'] = 'node';
$handler->display->display_options['sorts']['nid']['field'] = 'nid';
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'dataset' => 'dataset',
);
/* Filter criterion: Content: Variable Status (field_variable_status) */
$handler->display->display_options['filters']['field_variable_status_tid']['id'] = 'field_variable_status_tid';
$handler->display->display_options['filters']['field_variable_status_tid']['table'] = 'field_data_field_variable_status';
$handler->display->display_options['filters']['field_variable_status_tid']['field'] = 'field_variable_status_tid';
$handler->display->display_options['filters']['field_variable_status_tid']['value'] = '';
$handler->display->display_options['filters']['field_variable_status_tid']['exposed'] = TRUE;
$handler->display->display_options['filters']['field_variable_status_tid']['expose']['operator_id'] = 'field_variable_status_tid_op';
$handler->display->display_options['filters']['field_variable_status_tid']['expose']['label'] = 'Variable Type';
$handler->display->display_options['filters']['field_variable_status_tid']['expose']['operator'] = 'field_variable_status_tid_op';
$handler->display->display_options['filters']['field_variable_status_tid']['expose']['identifier'] = 'field_variable_status_tid';
$handler->display->display_options['filters']['field_variable_status_tid']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
);
$handler->display->display_options['filters']['field_variable_status_tid']['type'] = 'select';
$handler->display->display_options['filters']['field_variable_status_tid']['vocabulary'] = 'variable_status';
$handler->display->display_options['filters']['field_variable_status_tid']['hierarchy'] = 1;

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['path'] = 'variables-list';
$translatables['variables_list'] = array(
  t('Master'),
  t('Variables List'),
  t('more'),
  t('Apply'),
  t('Reset'),
  t('Sort by'),
  t('Asc'),
  t('Desc'),
  t('Items per page'),
  t('- All -'),
  t('Offset'),
  t('« first'),
  t('‹ previous'),
  t('next ›'),
  t('last »'),
  t('ID'),
  t('Title'),
  t('Variable Category'),
  t('Type'),
  t('Subsystem'),
  t('Model Family'),
  t('WEAP Variable'),
  t('Variable Type'),
  t('Page'),
);
