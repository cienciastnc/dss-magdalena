<?php
/**
 * @file
 * capas_huella.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function capas_huella_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function capas_huella_node_info() {
  $items = array(
    'footprint_layers' => array(
      'name' => t('Capas Modelo Huella'),
      'base' => 'node_content',
      'description' => t('Capas requeridas para ejecutar el modelo de huella en el módulo exploratorio'),
      'has_title' => '1',
      'title_label' => t('Nombre '),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
