<?php
/**
 * @file
 * capas_huella.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function capas_huella_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer panelizer node footprint_layers defaults'.
  $permissions['administer panelizer node footprint_layers defaults'] = array(
    'name' => 'administer panelizer node footprint_layers defaults',
    'roles' => array(),
    'module' => 'panelizer',
  );

  // Exported permission: 'create footprint_layers content'.
  $permissions['create footprint_layers content'] = array(
    'name' => 'create footprint_layers content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'create layers content'.
  $permissions['create layers content'] = array(
    'name' => 'create layers content',
    'roles' => array(
      'Sima User' => 'Sima User',
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any footprint_layers content'.
  $permissions['delete any footprint_layers content'] = array(
    'name' => 'delete any footprint_layers content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any layers content'.
  $permissions['delete any layers content'] = array(
    'name' => 'delete any layers content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own footprint_layers content'.
  $permissions['delete own footprint_layers content'] = array(
    'name' => 'delete own footprint_layers content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own layers content'.
  $permissions['delete own layers content'] = array(
    'name' => 'delete own layers content',
    'roles' => array(
      'Sima User' => 'Sima User',
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any footprint_layers content'.
  $permissions['edit any footprint_layers content'] = array(
    'name' => 'edit any footprint_layers content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit any layers content'.
  $permissions['edit any layers content'] = array(
    'name' => 'edit any layers content',
    'roles' => array(
      'Sima User' => 'Sima User',
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own footprint_layers content'.
  $permissions['edit own footprint_layers content'] = array(
    'name' => 'edit own footprint_layers content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own layers content'.
  $permissions['edit own layers content'] = array(
    'name' => 'edit own layers content',
    'roles' => array(
      'Sima User' => 'Sima User',
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  return $permissions;
}
