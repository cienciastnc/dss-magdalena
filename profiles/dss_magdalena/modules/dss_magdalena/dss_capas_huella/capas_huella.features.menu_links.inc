<?php
/**
 * @file
 * capas_huella.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function capas_huella_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: navigation_capas-modelo-huella:node/add/footprint-layers.
  $menu_links['navigation_capas-modelo-huella:node/add/footprint-layers'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'node/add/footprint-layers',
    'router_path' => 'node/add/footprint-layers',
    'link_title' => 'Capas Modelo Huella',
    'options' => array(
      'attributes' => array(
        'title' => 'Capas requeridas para ejecutar el modelo de huella en el módulo exploratorio',
      ),
      'identifier' => 'navigation_capas-modelo-huella:node/add/footprint-layers',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'navigation_aadir-contenido:node/add',
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Capas Modelo Huella');

  return $menu_links;
}
