<?php
/**
 * @file
 * Template for Case Readiness.
 */
?>
<label> <?php print $title; ?>: </label>
<?php
  $resources_list = $variables['data_variables'];
  foreach ($resources_list as $type => $resources) {
    if(!empty($resources)){
      ?>
      <div>
        <table class="table table-striped table-bordered sticky-enabled tableheader-processed sticky-table">
          <thead>
            <tr><th><?php print (strtoupper($type)) ?>
            </th></tr>
          </thead>
          <?php
            foreach ($resources as $resource) {
              if ($resource) {
                if(!$resource->isDataStoreImported()) {
                  $title = $resource->getTitle();
                  $resource_items = l($title, "node/{$resource->getId()}");
                ?>
                <tr>
                  <td><?php print ($resource_items) ?></td>
                </tr>
              <?php }
              }
            }
          ?>
        </table>
      </div>
      <?php
    }
  }
?>
