<?php
/**
 * @file
 * Outputs the modeling status in a wrapped html snipped.
 */
$status_es="";
switch ($status) {
    case 'PROCESSING':
        # code...
        $status_es="Procesando";
        break;
    case 'OK':
        $status_es="Ejecutado";
        break;
    case '0%':
        $status_es="Sin datos";
        break;
    case '100%':
        $status_es="No ejecutado";
        break;
    default:
        $status_es=$status;
        break;
}


?>
<span class="<?php print "$css_status $css_specific" ?>"><?php print $status_es ?></span>
