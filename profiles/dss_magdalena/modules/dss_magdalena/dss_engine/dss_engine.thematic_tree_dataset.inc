<?php
/**
 * @file
 * Handles the relationship between Thematic Tree and Datasets (Variables).
 */

use Drupal\dss_magdalena\DSS\SimaThematicTreeVariables as SimaThematicTreeVariables;

/**
 * Implements hook_form_BASE_FORM_ID_alter().
 */
function dss_engine_form_taxonomy_form_term_alter(&$form, &$form_state, $form_id) {

  // Validating that this belongs to the taxonomy vocabulary for Thematic Tree.
  $thematic_tree_vocabulary = taxonomy_vocabulary_machine_name_load("thematic_tree");
  if (!isset($form_state['term']->vid) || ($thematic_tree_vocabulary->vid !== $form_state['term']->vid)) {
    return;
  }

  // Obtain the Thematic Tree / Variable Relationships.
  $thematic_tree_tid = $form_state['term']->tid;
  $thematic_tree_datasets = SimaThematicTreeVariables::load($thematic_tree_tid);
  $datasets = $thematic_tree_datasets ? $thematic_tree_datasets->getThematicTreeDatasets() : array();

  // Saving stored datasets.
  $form_state['old_datasets'] = $datasets;

  // Set all options.
  $options = array();

  // Get all PO Terms.
  $v = taxonomy_vocabulary_machine_name_load("po");
  $terms = taxonomy_get_tree($v->vid);
  $option = array(
    'name' => $v->machine_name,
    'title' => $v->name,
    'weight' => 75,
    'delta' => 3,
    'options' => array(
      0 => t('Not Set'),
    ),
  );
  foreach ($terms as $item) {
    $option['options'][$item->tid] = $item->name;
  }
  $options[] = $option;

  // Get all DPSIR Terms.
  $v = taxonomy_vocabulary_machine_name_load("dpsir");
  $terms = taxonomy_get_tree($v->vid);
  $option = array(
    'name' => $v->machine_name,
    'title' => $v->name,
    'weight' => 80,
    'delta' => 4,
    'options' => array(
      0 => t('Not Set'),
    ),
  );
  foreach ($terms as $item) {
    $option['options'][$item->tid] = $item->name;
  }
  $options[] = $option;

  // Get all SWOT Terms.
  $v = taxonomy_vocabulary_machine_name_load("swot");
  $terms = taxonomy_get_tree($v->vid);
  $option = array(
    'name' => $v->machine_name,
    'title' => $v->name,
    'weight' => 85,
    'delta' => 5,
    'options' => array(
      0 => t('Not Set'),
    ),
  );
  foreach ($terms as $item) {
    $option['options'][$item->tid] = $item->name;
  }
  $options[] = $option;

  foreach ($form['field_variables'][LANGUAGE_NONE] as $key => $field) {
    if (is_numeric($key)) {
      // Obtaining the dataset.
      $variables = isset($form['#entity']->field_variables[LANGUAGE_NONE]) ? $form['#entity']->field_variables[LANGUAGE_NONE] : FALSE;
      $dataset_id = isset($variables[$key]['target_id']) ? $variables[$key]['target_id'] : FALSE;
      $dataset = $dataset_id ? $datasets[$dataset_id] : FALSE;

      // Filling up data for this dataset.
      foreach ($options as $option) {
        $form['field_variables'][LANGUAGE_NONE][$key][$option['name']] = array(
          '#type' => 'select',
          '#title' => $option['title'],
          '#options' => $option['options'],
          '#default_value' => $dataset ? _dss_engine_get_thematic_tree_options_default_value($dataset, $option) : array(),
          '#weight' => $option['weight'],
          '#delta' => $option['delta'],
        );
      }
      $form['field_variables'][LANGUAGE_NONE]['#after_build'] = array(
        'field_form_element_after_build',
      );
    }
  }

  // Also adding a new submit function.
  $form['#submit'][] = 'dss_engine_thematic_tree_variables_submit';

}

/**
 * Submit function for thematic tree/datasets (variable) relationships.
 *
 * @param array $form
 *   The form array.
 * @param array $form_state
 *   The form state array.
 */
function dss_engine_thematic_tree_variables_submit($form, &$form_state) {
  $values = $form_state['values'];
  $old_datasets = $form_state['old_datasets'];
  // Creating the Relationships Thematic Tree <-> Datasets.
  $thematic_tree_datasets = new SimaThematicTreeVariables($old_datasets);
  $thematic_tree_datasets->setThematicTreeDatasets($values['tid'], $values['field_variables']);
  if (!$thematic_tree_datasets->save()) {
    drupal_set_message(t('We could not save all Thematic Tree / Variable relationships.'), 'error');
  }

}

/**
 * Helper function to calculate the default value for PO,DPSIR and SWOT.
 *
 * @param object $dataset
 *   The Dataset Object.
 * @param array $option
 *   The options array.
 *
 * @return array
 *   The default value array.
 */
function _dss_engine_get_thematic_tree_options_default_value($dataset, $option) {
  $default_value = array();
  switch ($option['name']) {
    case SimaThematicTreeVariables::PO:
      $default_value = array(
        $dataset->{SimaThematicTreeVariables::PO},
      );
      break;

    case SimaThematicTreeVariables::DPSIR:
      $default_value = array(
        $dataset->{SimaThematicTreeVariables::DPSIR},
      );
      break;

    case SimaThematicTreeVariables::SWOT:
      $default_value = array(
        $dataset->{SimaThematicTreeVariables::SWOT},
      );
      break;
  }
  return $default_value;
}
