<?php

/**
 * @file
 * Default view for Water Balance Resource Entity References.
 */

$view = new view();
$view->name = 'water_balance_variable_list';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Water Balance Variable List';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'time';
$handler->display->display_options['cache']['results_lifespan'] = '3600';
$handler->display->display_options['cache']['results_lifespan_custom'] = '0';
$handler->display->display_options['cache']['output_lifespan'] = '3600';
$handler->display->display_options['cache']['output_lifespan_custom'] = '0';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '10';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['pager']['options']['id'] = '0';
$handler->display->display_options['pager']['options']['quantity'] = '9';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'entityreference_view_widget' => 'entityreference_view_widget',
  'title' => 'title',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'entityreference_view_widget' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'title' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
/* Field: Entity Reference View Widget Checkbox: Content */
$handler->display->display_options['fields']['entityreference_view_widget']['id'] = 'entityreference_view_widget';
$handler->display->display_options['fields']['entityreference_view_widget']['table'] = 'node';
$handler->display->display_options['fields']['entityreference_view_widget']['field'] = 'entityreference_view_widget';
$handler->display->display_options['fields']['entityreference_view_widget']['label'] = '';
$handler->display->display_options['fields']['entityreference_view_widget']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['entityreference_view_widget']['ervw']['force_single'] = 1;
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = 'Variable';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
/* Field: Content: Variable Status */
$handler->display->display_options['fields']['field_variable_status']['id'] = 'field_variable_status';
$handler->display->display_options['fields']['field_variable_status']['table'] = 'field_data_field_variable_status';
$handler->display->display_options['fields']['field_variable_status']['field'] = 'field_variable_status';
/* Field: Content: Variable Category */
$handler->display->display_options['fields']['field_variable_category']['id'] = 'field_variable_category';
$handler->display->display_options['fields']['field_variable_category']['table'] = 'field_data_field_variable_category';
$handler->display->display_options['fields']['field_variable_category']['field'] = 'field_variable_category';
/* Sort criterion: Content: Title */
$handler->display->display_options['sorts']['title']['id'] = 'title';
$handler->display->display_options['sorts']['title']['table'] = 'node';
$handler->display->display_options['sorts']['title']['field'] = 'title';
/* Contextual filter: Content: Nid */
$handler->display->display_options['arguments']['nid']['id'] = 'nid';
$handler->display->display_options['arguments']['nid']['table'] = 'node';
$handler->display->display_options['arguments']['nid']['field'] = 'nid';
$handler->display->display_options['arguments']['nid']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['nid']['not'] = TRUE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'dataset' => 'dataset',
);
$handler->display->display_options['filters']['type']['group'] = 1;
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Title */
$handler->display->display_options['filters']['title']['id'] = 'title';
$handler->display->display_options['filters']['title']['table'] = 'node';
$handler->display->display_options['filters']['title']['field'] = 'title';
$handler->display->display_options['filters']['title']['operator'] = 'contains';
$handler->display->display_options['filters']['title']['group'] = 1;
$handler->display->display_options['filters']['title']['exposed'] = TRUE;
$handler->display->display_options['filters']['title']['expose']['operator_id'] = 'title_op';
$handler->display->display_options['filters']['title']['expose']['label'] = 'Search by Name';
$handler->display->display_options['filters']['title']['expose']['operator'] = 'title_op';
$handler->display->display_options['filters']['title']['expose']['identifier'] = 'title';
$handler->display->display_options['filters']['title']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
);
/* Filter criterion: Content: Variable Status (field_variable_status) */
$handler->display->display_options['filters']['field_variable_status_tid']['id'] = 'field_variable_status_tid';
$handler->display->display_options['filters']['field_variable_status_tid']['table'] = 'field_data_field_variable_status';
$handler->display->display_options['filters']['field_variable_status_tid']['field'] = 'field_variable_status_tid';
$handler->display->display_options['filters']['field_variable_status_tid']['value'] = array(
  355 => '355',
);
$handler->display->display_options['filters']['field_variable_status_tid']['group'] = 1;
$handler->display->display_options['filters']['field_variable_status_tid']['exposed'] = TRUE;
$handler->display->display_options['filters']['field_variable_status_tid']['expose']['operator_id'] = 'field_variable_status_tid_op';
$handler->display->display_options['filters']['field_variable_status_tid']['expose']['label'] = 'Search by Variable Status';
$handler->display->display_options['filters']['field_variable_status_tid']['expose']['operator'] = 'field_variable_status_tid_op';
$handler->display->display_options['filters']['field_variable_status_tid']['expose']['identifier'] = 'field_variable_status_tid';
$handler->display->display_options['filters']['field_variable_status_tid']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
);
$handler->display->display_options['filters']['field_variable_status_tid']['type'] = 'select';
$handler->display->display_options['filters']['field_variable_status_tid']['vocabulary'] = 'variable_status';
$handler->display->display_options['filters']['field_variable_status_tid']['hierarchy'] = 1;

/* Display: Entity Reference View Widget */
$handler = $view->new_display('entityreference_view_widget', 'Entity Reference View Widget', 'variable_list_view_widget');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['defaults']['use_ajax'] = FALSE;
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'entityreference_view_widget' => 'entityreference_view_widget',
  'title' => 'title',
  'field_variable_status' => 'field_variable_status',
  'field_variable_category' => 'field_variable_category',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'entityreference_view_widget' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'title' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_variable_status' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_variable_category' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['defaults']['row_options'] = FALSE;
$translatables['water_balance_variable_list'] = array(
  t('Master'),
  t('more'),
  t('Apply'),
  t('Reset'),
  t('Sort by'),
  t('Asc'),
  t('Desc'),
  t('Items per page'),
  t('- All -'),
  t('Offset'),
  t('« first'),
  t('‹ previous'),
  t('next ›'),
  t('last »'),
  t('Variable'),
  t('Variable Status'),
  t('Variable Category'),
  t('All'),
  t('Search by Name'),
  t('Search by Variable Status'),
  t('Entity Reference View Widget'),
);
