<?php
/**
 * @file
 * Wetlands Management Projects List View.
 */

$view = new view();
$view->name = 'wetlands_management_project_variations';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Wetlands Management Project Variants';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Project Variants';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['style_plugin'] = 'table_megarows';
$handler->display->display_options['style_options']['columns'] = array(
  'title' => 'title',
  'field_river_flooding_threshold_v' => 'field_river_flooding_threshold_v',
  'field_river_flooding_fraction_v' => 'field_river_flooding_fraction_v',
  'field_flood_return_fraction_v' => 'field_flood_return_fraction_v',
  'nothing' => 'nothing',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'title' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_river_flooding_threshold_v' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_river_flooding_fraction_v' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_flood_return_fraction_v' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'nothing' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
$handler->display->display_options['style_options']['scroll_padding'] = '120';
$handler->display->display_options['row_plugin'] = 'fields';
/* Header: Global: Text area */
$handler->display->display_options['header']['area']['id'] = 'area';
$handler->display->display_options['header']['area']['table'] = 'views';
$handler->display->display_options['header']['area']['field'] = 'area';
$handler->display->display_options['header']['area']['empty'] = TRUE;
$handler->display->display_options['header']['area']['content'] = '<h2>Project Variants</h2>

<a href=\'node/add?field_wet_mgmt_project_ref=[node:nid]\'>New Project Variant</a>';
$handler->display->display_options['header']['area']['format'] = 'filtered_html';
$handler->display->display_options['header']['area']['tokenize'] = TRUE;
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = 'Variant Title';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
/* Field: Content: River Flooding Threshold */
$handler->display->display_options['fields']['field_river_flooding_threshold_v']['id'] = 'field_river_flooding_threshold_v';
$handler->display->display_options['fields']['field_river_flooding_threshold_v']['table'] = 'field_data_field_river_flooding_threshold_v';
$handler->display->display_options['fields']['field_river_flooding_threshold_v']['field'] = 'field_river_flooding_threshold_v';
$handler->display->display_options['fields']['field_river_flooding_threshold_v']['settings'] = array(
  'thousand_separator' => '',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 1,
  'custom_link_to_entity' => 0,
  'custom_prefix' => '',
  'custom_suffix' => '',
  'custom_reverse' => 0,
  'custom_trim' => 0,
  'custom_strtolower' => 0,
  'custom_strtoupper' => 0,
  'custom_ucfirst' => 0,
  'custom_ucwords' => 0,
  'custom_strip_tags' => 0,
  'custom_strip_preserve' => '',
);
/* Field: Content: River Flooding Fraction */
$handler->display->display_options['fields']['field_river_flooding_fraction_v']['id'] = 'field_river_flooding_fraction_v';
$handler->display->display_options['fields']['field_river_flooding_fraction_v']['table'] = 'field_data_field_river_flooding_fraction_v';
$handler->display->display_options['fields']['field_river_flooding_fraction_v']['field'] = 'field_river_flooding_fraction_v';
$handler->display->display_options['fields']['field_river_flooding_fraction_v']['settings'] = array(
  'thousand_separator' => '',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 1,
  'custom_link_to_entity' => 0,
  'custom_prefix' => '',
  'custom_suffix' => '',
  'custom_reverse' => 0,
  'custom_trim' => 0,
  'custom_strtolower' => 0,
  'custom_strtoupper' => 0,
  'custom_ucfirst' => 0,
  'custom_ucwords' => 0,
  'custom_strip_tags' => 0,
  'custom_strip_preserve' => '',
);
/* Field: Content: Flood Return Fraction */
$handler->display->display_options['fields']['field_flood_return_fraction_v']['id'] = 'field_flood_return_fraction_v';
$handler->display->display_options['fields']['field_flood_return_fraction_v']['table'] = 'field_data_field_flood_return_fraction_v';
$handler->display->display_options['fields']['field_flood_return_fraction_v']['field'] = 'field_flood_return_fraction_v';
$handler->display->display_options['fields']['field_flood_return_fraction_v']['settings'] = array(
  'thousand_separator' => '',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 1,
  'custom_link_to_entity' => 0,
  'custom_prefix' => '',
  'custom_suffix' => '',
  'custom_reverse' => 0,
  'custom_trim' => 0,
  'custom_strtolower' => 0,
  'custom_strtoupper' => 0,
  'custom_ucfirst' => 0,
  'custom_ucwords' => 0,
  'custom_strip_tags' => 0,
  'custom_strip_preserve' => '',
);
/* Field: Content: Megarow links */
$handler->display->display_options['fields']['megarow_links']['id'] = 'megarow_links';
$handler->display->display_options['fields']['megarow_links']['table'] = 'views_entity_node';
$handler->display->display_options['fields']['megarow_links']['field'] = 'megarow_links';
$handler->display->display_options['fields']['megarow_links']['label'] = 'More Details';
$handler->display->display_options['fields']['megarow_links']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['megarow_links']['megarow'] = array(
  'links' => 'View|node/[node:nid]
Edit|node/[node:nid]/edit',
);
/* Sort criterion: Content: Post date */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'node';
$handler->display->display_options['sorts']['created']['field'] = 'created';
$handler->display->display_options['sorts']['created']['order'] = 'DESC';
/* Contextual filter: Content: Wetlands Management Project (field_wet_mgmt_project_ref) */
$handler->display->display_options['arguments']['field_wet_mgmt_project_ref_target_id']['id'] = 'field_wet_mgmt_project_ref_target_id';
$handler->display->display_options['arguments']['field_wet_mgmt_project_ref_target_id']['table'] = 'field_data_field_wet_mgmt_project_ref';
$handler->display->display_options['arguments']['field_wet_mgmt_project_ref_target_id']['field'] = 'field_wet_mgmt_project_ref_target_id';
$handler->display->display_options['arguments']['field_wet_mgmt_project_ref_target_id']['default_action'] = 'default';
$handler->display->display_options['arguments']['field_wet_mgmt_project_ref_target_id']['default_argument_type'] = 'raw';
$handler->display->display_options['arguments']['field_wet_mgmt_project_ref_target_id']['default_argument_options']['index'] = '0';
$handler->display->display_options['arguments']['field_wet_mgmt_project_ref_target_id']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['field_wet_mgmt_project_ref_target_id']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['field_wet_mgmt_project_ref_target_id']['summary_options']['items_per_page'] = '25';
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'wetlands_management_variations' => 'wetlands_management_variations',
);

/* Display: EVA Field */
$handler = $view->new_display('entity_view', 'EVA Field', 'entity_view_1');
$handler->display->display_options['entity_type'] = 'node';
$handler->display->display_options['bundles'] = array(
  0 => 'wetlands_management',
);

/* Display: Wetlands Management Project Variants */
$handler = $view->new_display('page', 'Wetlands Management Project Variants', 'page_1');
$handler->display->display_options['defaults']['header'] = FALSE;
$handler->display->display_options['path'] = 'wetlands-management-projects-variations';
$translatables['wetlands_management_project_variations'] = array(
  t('Master'),
  t('Project Variants'),
  t('more'),
  t('Apply'),
  t('Reset'),
  t('Sort by'),
  t('Asc'),
  t('Desc'),
  t('Items per page'),
  t('- All -'),
  t('Offset'),
  t('« first'),
  t('‹ previous'),
  t('next ›'),
  t('last »'),
  t('<h2>Project Variants</h2>

<a href=\'node/add?field_wet_mgmt_project_ref=[node:nid]\'>New Project Variant</a>'),
  t('Variant Title'),
  t('River Flooding Threshold'),
  t('River Flooding Fraction'),
  t('Flood Return Fraction'),
  t('More Details'),
  t('All'),
  t('EVA Field'),
  t('Wetlands Management Project Variants'),
);
