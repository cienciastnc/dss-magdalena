<?php
/**
 * @file
 * Hydropower Plants or Dams.
 */

$view = new view();
$view->name = 'hydropower_plants_and_dams';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Hydropower Plants and Dams';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Hydropower Plants and Dams';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '30';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['pager']['options']['id'] = '0';
$handler->display->display_options['pager']['options']['quantity'] = '9';
$handler->display->display_options['style_plugin'] = 'table';
/* Field: Entity Reference View Widget Checkbox: Content */
$handler->display->display_options['fields']['entityreference_view_widget']['id'] = 'entityreference_view_widget';
$handler->display->display_options['fields']['entityreference_view_widget']['table'] = 'node';
$handler->display->display_options['fields']['entityreference_view_widget']['field'] = 'entityreference_view_widget';
$handler->display->display_options['fields']['entityreference_view_widget']['label'] = '';
$handler->display->display_options['fields']['entityreference_view_widget']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['entityreference_view_widget']['ervw']['force_single'] = 0;
/* Field: Content: River */
$handler->display->display_options['fields']['field_river']['id'] = 'field_river';
$handler->display->display_options['fields']['field_river']['table'] = 'field_data_field_river';
$handler->display->display_options['fields']['field_river']['field'] = 'field_river';
$handler->display->display_options['fields']['field_river']['settings'] = array(
  'custom_link_to_entity' => 0,
  'custom_prefix' => '',
  'custom_suffix' => '',
  'custom_reverse' => 0,
  'custom_trim' => 0,
  'custom_strtolower' => 0,
  'custom_strtoupper' => 0,
  'custom_ucfirst' => 0,
  'custom_ucwords' => 0,
  'custom_strip_tags' => 0,
  'custom_strip_preserve' => '',
);
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = 'Planta Hidroeléctrica / Represa';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
/* Field: Content: Reservoir Capacidad de Almacén */
$handler->display->display_options['fields']['field_storage_capacity']['id'] = 'field_storage_capacity';
$handler->display->display_options['fields']['field_storage_capacity']['table'] = 'field_data_field_storage_capacity';
$handler->display->display_options['fields']['field_storage_capacity']['field'] = 'field_storage_capacity';
$handler->display->display_options['fields']['field_storage_capacity']['label'] = 'Capacidad de Almacén';
$handler->display->display_options['fields']['field_storage_capacity']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['field_storage_capacity']['alter']['text'] = '(Capacidad de Almacén: [field_storage_capacity])';
$handler->display->display_options['fields']['field_storage_capacity']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_storage_capacity']['settings'] = array(
  'thousand_separator' => ',',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 0,
  'custom_link_to_entity' => 0,
  'custom_prefix' => '',
  'custom_suffix' => '',
  'custom_reverse' => 0,
  'custom_trim' => 0,
  'custom_strtolower' => 0,
  'custom_strtoupper' => 0,
  'custom_ucfirst' => 0,
  'custom_ucwords' => 0,
  'custom_strip_tags' => 0,
  'custom_strip_preserve' => '',
);
/* Field: Content: Generating Efficiency */
$handler->display->display_options['fields']['field_generating_efficiency']['id'] = 'field_generating_efficiency';
$handler->display->display_options['fields']['field_generating_efficiency']['table'] = 'field_data_field_generating_efficiency';
$handler->display->display_options['fields']['field_generating_efficiency']['field'] = 'field_generating_efficiency';
$handler->display->display_options['fields']['field_generating_efficiency']['settings'] = array(
  'thousand_separator' => '',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 1,
  'custom_link_to_entity' => 0,
  'custom_prefix' => '',
  'custom_suffix' => '',
  'custom_reverse' => 0,
  'custom_trim' => 0,
  'custom_strtolower' => 0,
  'custom_strtoupper' => 0,
  'custom_ucfirst' => 0,
  'custom_ucwords' => 0,
  'custom_strip_tags' => 0,
  'custom_strip_preserve' => '',
);
/* Field: Content: Maximum Turbine Flow */
$handler->display->display_options['fields']['field_max_turbine_flow']['id'] = 'field_max_turbine_flow';
$handler->display->display_options['fields']['field_max_turbine_flow']['table'] = 'field_data_field_max_turbine_flow';
$handler->display->display_options['fields']['field_max_turbine_flow']['field'] = 'field_max_turbine_flow';
$handler->display->display_options['fields']['field_max_turbine_flow']['settings'] = array(
  'thousand_separator' => '',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 1,
  'custom_link_to_entity' => 0,
  'custom_prefix' => '',
  'custom_suffix' => '',
  'custom_reverse' => 0,
  'custom_trim' => 0,
  'custom_strtolower' => 0,
  'custom_strtoupper' => 0,
  'custom_ucfirst' => 0,
  'custom_ucwords' => 0,
  'custom_strip_tags' => 0,
  'custom_strip_preserve' => '',
);
/* Field: Content: Storage @ MAX Normal Elevation */
$handler->display->display_options['fields']['field_top_conservation']['id'] = 'field_top_conservation';
$handler->display->display_options['fields']['field_top_conservation']['table'] = 'field_data_field_top_conservation';
$handler->display->display_options['fields']['field_top_conservation']['field'] = 'field_top_conservation';
$handler->display->display_options['fields']['field_top_conservation']['settings'] = array(
  'thousand_separator' => '',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 1,
  'custom_link_to_entity' => 0,
  'custom_prefix' => '',
  'custom_suffix' => '',
  'custom_reverse' => 0,
  'custom_trim' => 0,
  'custom_strtolower' => 0,
  'custom_strtoupper' => 0,
  'custom_ucfirst' => 0,
  'custom_ucwords' => 0,
  'custom_strip_tags' => 0,
  'custom_strip_preserve' => '',
);
/* Field: Content: Sima Case Status */
$handler->display->display_options['fields']['field_sima_case_status']['id'] = 'field_sima_case_status';
$handler->display->display_options['fields']['field_sima_case_status']['table'] = 'field_data_field_sima_case_status';
$handler->display->display_options['fields']['field_sima_case_status']['field'] = 'field_sima_case_status';
$handler->display->display_options['fields']['field_sima_case_status']['label'] = 'Base Line';
$handler->display->display_options['fields']['field_sima_case_status']['settings'] = array(
  'custom_link_to_entity' => 0,
  'custom_prefix' => '',
  'custom_suffix' => '',
  'custom_reverse' => 0,
  'custom_trim' => 0,
  'custom_strtolower' => 0,
  'custom_strtoupper' => 0,
  'custom_ucfirst' => 0,
  'custom_ucwords' => 0,
  'custom_strip_tags' => 0,
  'custom_strip_preserve' => '',
);
/* Sort criterion: Content: Post date */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'node';
$handler->display->display_options['sorts']['created']['field'] = 'created';
$handler->display->display_options['sorts']['created']['order'] = 'DESC';
/* Contextual filter: Content: Nid */
$handler->display->display_options['arguments']['nid']['id'] = 'nid';
$handler->display->display_options['arguments']['nid']['table'] = 'node';
$handler->display->display_options['arguments']['nid']['field'] = 'nid';
$handler->display->display_options['arguments']['nid']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['nid']['break_phrase'] = TRUE;
$handler->display->display_options['arguments']['nid']['not'] = TRUE;
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'hydropower' => 'hydropower',
);

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Content: River */
$handler->display->display_options['fields']['field_river']['id'] = 'field_river';
$handler->display->display_options['fields']['field_river']['table'] = 'field_data_field_river';
$handler->display->display_options['fields']['field_river']['field'] = 'field_river';
$handler->display->display_options['fields']['field_river']['settings'] = array(
  'custom_link_to_entity' => 0,
  'custom_prefix' => '',
  'custom_suffix' => '',
  'custom_reverse' => 0,
  'custom_trim' => 0,
  'custom_strtolower' => 0,
  'custom_strtoupper' => 0,
  'custom_ucfirst' => 0,
  'custom_ucwords' => 0,
  'custom_strip_tags' => 0,
  'custom_strip_preserve' => '',
);
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = 'Planta Hidroeléctrica / Represa ';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
/* Field: Content: Reservoir Capacidad de Almacén */
$handler->display->display_options['fields']['field_storage_capacity']['id'] = 'field_storage_capacity';
$handler->display->display_options['fields']['field_storage_capacity']['table'] = 'field_data_field_storage_capacity';
$handler->display->display_options['fields']['field_storage_capacity']['field'] = 'field_storage_capacity';
$handler->display->display_options['fields']['field_storage_capacity']['label'] = 'Capacidad de Almacén ';
$handler->display->display_options['fields']['field_storage_capacity']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['field_storage_capacity']['alter']['text'] = '(Capacidad de Almacén: [field_storage_capacity])';
$handler->display->display_options['fields']['field_storage_capacity']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_storage_capacity']['settings'] = array(
  'thousand_separator' => ',',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 0,
  'custom_link_to_entity' => 0,
  'custom_prefix' => '',
  'custom_suffix' => '',
  'custom_reverse' => 0,
  'custom_trim' => 0,
  'custom_strtolower' => 0,
  'custom_strtoupper' => 0,
  'custom_ucfirst' => 0,
  'custom_ucwords' => 0,
  'custom_strip_tags' => 0,
  'custom_strip_preserve' => '',
);
/* Field: Content: Generating Efficiency */
$handler->display->display_options['fields']['field_generating_efficiency']['id'] = 'field_generating_efficiency';
$handler->display->display_options['fields']['field_generating_efficiency']['table'] = 'field_data_field_generating_efficiency';
$handler->display->display_options['fields']['field_generating_efficiency']['field'] = 'field_generating_efficiency';
$handler->display->display_options['fields']['field_generating_efficiency']['settings'] = array(
  'thousand_separator' => '',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 1,
  'custom_link_to_entity' => 0,
  'custom_prefix' => '',
  'custom_suffix' => '',
  'custom_reverse' => 0,
  'custom_trim' => 0,
  'custom_strtolower' => 0,
  'custom_strtoupper' => 0,
  'custom_ucfirst' => 0,
  'custom_ucwords' => 0,
  'custom_strip_tags' => 0,
  'custom_strip_preserve' => '',
);
/* Field: Content: Maximum Turbine Flow */
$handler->display->display_options['fields']['field_max_turbine_flow']['id'] = 'field_max_turbine_flow';
$handler->display->display_options['fields']['field_max_turbine_flow']['table'] = 'field_data_field_max_turbine_flow';
$handler->display->display_options['fields']['field_max_turbine_flow']['field'] = 'field_max_turbine_flow';
$handler->display->display_options['fields']['field_max_turbine_flow']['settings'] = array(
  'thousand_separator' => '',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 1,
  'custom_link_to_entity' => 0,
  'custom_prefix' => '',
  'custom_suffix' => '',
  'custom_reverse' => 0,
  'custom_trim' => 0,
  'custom_strtolower' => 0,
  'custom_strtoupper' => 0,
  'custom_ucfirst' => 0,
  'custom_ucwords' => 0,
  'custom_strip_tags' => 0,
  'custom_strip_preserve' => '',
);
/* Field: Content: Storage @ MAX Normal Elevation */
$handler->display->display_options['fields']['field_top_conservation']['id'] = 'field_top_conservation';
$handler->display->display_options['fields']['field_top_conservation']['table'] = 'field_data_field_top_conservation';
$handler->display->display_options['fields']['field_top_conservation']['field'] = 'field_top_conservation';
$handler->display->display_options['fields']['field_top_conservation']['settings'] = array(
  'thousand_separator' => '',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 1,
  'custom_link_to_entity' => 0,
  'custom_prefix' => '',
  'custom_suffix' => '',
  'custom_reverse' => 0,
  'custom_trim' => 0,
  'custom_strtolower' => 0,
  'custom_strtoupper' => 0,
  'custom_ucfirst' => 0,
  'custom_ucwords' => 0,
  'custom_strip_tags' => 0,
  'custom_strip_preserve' => '',
);
/* Field: Content: Sima Case Status */
$handler->display->display_options['fields']['field_sima_case_status']['id'] = 'field_sima_case_status';
$handler->display->display_options['fields']['field_sima_case_status']['table'] = 'field_data_field_sima_case_status';
$handler->display->display_options['fields']['field_sima_case_status']['field'] = 'field_sima_case_status';
$handler->display->display_options['fields']['field_sima_case_status']['label'] = 'Base Line';
$handler->display->display_options['fields']['field_sima_case_status']['alter']['max_length'] = '1';
$handler->display->display_options['fields']['field_sima_case_status']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_sima_case_status']['settings'] = array(
  'custom_link_to_entity' => 0,
  'custom_prefix' => '',
  'custom_suffix' => '',
  'custom_reverse' => 0,
  'custom_trim' => 0,
  'custom_strtolower' => 0,
  'custom_strtoupper' => 0,
  'custom_ucfirst' => 0,
  'custom_ucwords' => 0,
  'custom_strip_tags' => 0,
  'custom_strip_preserve' => '',
);
$handler->display->display_options['path'] = 'hydropower-plants-and-dams';

/* Display: Entity Reference */
$handler = $view->new_display('entityreference', 'Entity Reference', 'entityreference_1');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'entityreference_style';
$handler->display->display_options['style_options']['search_fields'] = array(
  'title' => 'title',
);
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['row_plugin'] = 'entityreference_fields';
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['defaults']['header'] = FALSE;
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = 'Planta Hidroeléctrica / Represa ';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
/* Field: Content: Reservoir Capacidad de Almacén */
$handler->display->display_options['fields']['field_storage_capacity']['id'] = 'field_storage_capacity';
$handler->display->display_options['fields']['field_storage_capacity']['table'] = 'field_data_field_storage_capacity';
$handler->display->display_options['fields']['field_storage_capacity']['field'] = 'field_storage_capacity';
$handler->display->display_options['fields']['field_storage_capacity']['label'] = 'Capacidad de Almacén ';
$handler->display->display_options['fields']['field_storage_capacity']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['field_storage_capacity']['alter']['text'] = '(Capacidad de Almacén: [field_storage_capacity])';
$handler->display->display_options['fields']['field_storage_capacity']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_storage_capacity']['settings'] = array(
  'thousand_separator' => ',',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 0,
  'custom_link_to_entity' => 0,
  'custom_prefix' => '',
  'custom_suffix' => '',
  'custom_reverse' => 0,
  'custom_trim' => 0,
  'custom_strtolower' => 0,
  'custom_strtoupper' => 0,
  'custom_ucfirst' => 0,
  'custom_ucwords' => 0,
  'custom_strip_tags' => 0,
  'custom_strip_preserve' => '',
);

/* Display: Entity Reference View Widget */
$handler = $view->new_display('entityreference_view_widget', 'Entity Reference View Widget', 'entityreference_view_widget_hydropower_plants');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['defaults']['use_ajax'] = FALSE;
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'entityreference_view_widget' => 'entityreference_view_widget',
  'field_river' => 'field_river',
  'title' => 'title',
  'field_storage_capacity' => 'field_storage_capacity',
  'field_generating_efficiency' => 'field_generating_efficiency',
  'field_max_turbine_flow' => 'field_max_turbine_flow',
  'field_top_conservation' => 'field_top_conservation',
  'field_sima_case_status' => 'field_sima_case_status',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'entityreference_view_widget' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_river' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'title' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_storage_capacity' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_generating_efficiency' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_max_turbine_flow' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_top_conservation' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_sima_case_status' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['defaults']['header'] = FALSE;
$translatables['hydropower_plants_and_dams'] = array(
  t('Master'),
  t('Hydropower Plants and Dams'),
  t('more'),
  t('Apply'),
  t('Reset'),
  t('Sort by'),
  t('Asc'),
  t('Desc'),
  t('Items per page'),
  t('- All -'),
  t('Offset'),
  t('« first'),
  t('‹ previous'),
  t('next ›'),
  t('last »'),
  t('River'),
  t('Planta Hidroeléctrica / Represa'),
  t('Capacidad de Almacén'),
  t('(Capacidad de Almacén: [field_storage_capacity])'),
  t('Generating Efficiency'),
  t('Maximum Turbine Flow'),
  t('Storage @ MAX Normal Elevation'),
  t('Base Line'),
  t('All'),
  t('Page'),
  t('Planta Hidroeléctrica / Represa'),
  t('Capacidad de Almacén'),
  t('Entity Reference'),
  t('Entity Reference View Widget'),
);
