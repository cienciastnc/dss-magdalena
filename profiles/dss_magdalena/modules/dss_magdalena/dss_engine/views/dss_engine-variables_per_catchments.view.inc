<?php
/**
 * @file
 * View: Variables per Catchments.
 */

$view = new view();
$view->name = 'variables_per_catchments';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Variables per Catchments';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Variables per Catchments';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['pager']['options']['items_per_page'] = '302';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['style_plugin'] = 'leaflet';
$handler->display->display_options['style_options']['entity_type'] = 'node';
$handler->display->display_options['style_options']['data_source'] = 'field_spatialization';
$handler->display->display_options['style_options']['name_field'] = 'title';
$handler->display->display_options['style_options']['description_field'] = '#rendered_entity';
$handler->display->display_options['style_options']['view_mode'] = 'teaser';
$handler->display->display_options['style_options']['map'] = 'bing';
$handler->display->display_options['style_options']['height'] = '700';
$handler->display->display_options['style_options']['hide_empty'] = 0;
$handler->display->display_options['style_options']['zoom']['initialZoom'] = '-1';
$handler->display->display_options['style_options']['zoom']['minZoom'] = '0';
$handler->display->display_options['style_options']['zoom']['maxZoom'] = '18';
$handler->display->display_options['style_options']['vector_display']['stroke'] = 0;
$handler->display->display_options['style_options']['vector_display']['fill'] = 0;
$handler->display->display_options['style_options']['vector_display']['clickable'] = 0;
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
/* Field: Content: Spatialization */
$handler->display->display_options['fields']['field_spatialization']['id'] = 'field_spatialization';
$handler->display->display_options['fields']['field_spatialization']['table'] = 'field_data_field_spatialization';
$handler->display->display_options['fields']['field_spatialization']['field'] = 'field_spatialization';
$handler->display->display_options['fields']['field_spatialization']['label'] = '';
$handler->display->display_options['fields']['field_spatialization']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_spatialization']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_spatialization']['click_sort_column'] = 'wkt';
$handler->display->display_options['fields']['field_spatialization']['settings'] = array(
  'data' => 'full',
  'custom_link_to_entity' => 0,
  'custom_prefix' => '',
  'custom_suffix' => '',
  'custom_reverse' => 0,
  'custom_trim' => 0,
  'custom_strtolower' => 0,
  'custom_strtoupper' => 0,
  'custom_ucfirst' => 0,
  'custom_ucwords' => 0,
  'custom_strip_tags' => 0,
  'custom_strip_preserve' => '',
);
/* Sort criterion: Content: Post date */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'node';
$handler->display->display_options['sorts']['created']['field'] = 'created';
$handler->display->display_options['sorts']['created']['order'] = 'DESC';
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'demand_sites_and_catchments' => 'demand_sites_and_catchments',
);

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['path'] = 'variables-per-catchments';
$translatables['variables_per_catchments'] = array(
  t('Master'),
  t('Variables per Catchments'),
  t('more'),
  t('Apply'),
  t('Reset'),
  t('Sort by'),
  t('Asc'),
  t('Desc'),
  t('Page'),
);
