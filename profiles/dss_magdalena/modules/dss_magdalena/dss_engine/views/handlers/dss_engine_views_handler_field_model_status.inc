<?php
/**
 * @file
 * Handler for Views Field: 'Modeling Status': 'modeling_status'.
 */

use Drupal\dss_magdalena\DSS\Entity\SimaCaseStudy;
use Drupal\dss_magdalena\DSS\Entity\SimaModel;

/**
 * Class definition.
 */
// @codingStandardsIgnoreStart
class dss_engine_views_handler_field_model_status extends views_handler_field {
// @codingStandardsIgnoreEnd

  /**
   * Render function for views field "modeling_status".
   */
  public function render($values) {
    $output = '';
    if (class_exists('\Drupal\dss_magdalena\DSS\Entity\SimaCaseStudy')) {
      if ($case_study = SimaCaseStudy::load($values->nid)) {
        $items = [];
        $case_output = $case_study->getCaseOutputs();
        $modeling_status_list = $case_output->getModelingStatusList('model_name');
        foreach ($modeling_status_list as $model_machine_name => $model) {
          $link = l($model['name'], 'case/' . $case_study->getId() . '/model/' . $model_machine_name, array(
            'attributes' => array(
              'title' => $model['name'],
              'html' => TRUE,
            ),
          ));
          $status = SimaModel::getStatusLabel($model['status'], TRUE);
          // $status = is_numeric($status) ? "{$status}%" : $status;.
          $items[] = $link . ': ' . $status;
        }
        $output = theme('item_list', array(
          'items' => $items,
          'type' => 'ul',
        ));
      }
    }

    return $output;

  }
  /**
   * Views query method.
   */
  public function query() {
    // Do nothing, leave query blank, let views render the contents.
  }

}
