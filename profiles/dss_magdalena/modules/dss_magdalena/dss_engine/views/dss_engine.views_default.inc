<?php
/**
 * @file
 * Default views for dss_engine module.
 */

/**
 * Implements hook_views_default_views().
 */
function dss_engine_views_default_views() {
  $views = [];
  $views_path = drupal_get_path('module', 'dss_engine') . '/views';
  $files = file_scan_directory($views_path, '/.*\.view.inc$/');
  foreach ($files as $filepath => $file) {
    require $filepath;
    if (isset($view)) {
      $views[$view->name] = $view;
    }
  }
  return $views;
}
