<?php
/**
 * @file
 * Run of River Hydropower Plants.
 */

$view = new view();
$view->name = 'run_of_river_hydropower_plant';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Run of River Hydropower Plant';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Run of River Hydropower Plant';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '20';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'entityreference_view_widget' => 'entityreference_view_widget',
  'field_river' => 'field_river',
  'title' => 'title',
  'field_ror_generating_efficiency' => 'field_ror_generating_efficiency',
  'field_ror_max_turbine_flow' => 'field_ror_max_turbine_flow',
  'field_ror_plant_factor' => 'field_ror_plant_factor',
  'field_fixed_head' => 'field_fixed_head',
  'field_sima_case_status' => 'field_sima_case_status',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'entityreference_view_widget' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_river' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'title' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_ror_generating_efficiency' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_ror_max_turbine_flow' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_ror_plant_factor' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_fixed_head' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_sima_case_status' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
/* Field: Entity Reference View Widget Checkbox: Content */
$handler->display->display_options['fields']['entityreference_view_widget']['id'] = 'entityreference_view_widget';
$handler->display->display_options['fields']['entityreference_view_widget']['table'] = 'node';
$handler->display->display_options['fields']['entityreference_view_widget']['field'] = 'entityreference_view_widget';
$handler->display->display_options['fields']['entityreference_view_widget']['label'] = '';
$handler->display->display_options['fields']['entityreference_view_widget']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['entityreference_view_widget']['ervw']['force_single'] = 0;
/* Field: Content: River */
$handler->display->display_options['fields']['field_river']['id'] = 'field_river';
$handler->display->display_options['fields']['field_river']['table'] = 'field_data_field_river';
$handler->display->display_options['fields']['field_river']['field'] = 'field_river';
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
/* Field: Content: Generating Efficiency */
$handler->display->display_options['fields']['field_ror_generating_efficiency']['id'] = 'field_ror_generating_efficiency';
$handler->display->display_options['fields']['field_ror_generating_efficiency']['table'] = 'field_data_field_ror_generating_efficiency';
$handler->display->display_options['fields']['field_ror_generating_efficiency']['field'] = 'field_ror_generating_efficiency';
$handler->display->display_options['fields']['field_ror_generating_efficiency']['settings'] = array(
  'thousand_separator' => ',',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 1,
);
/* Field: Content: Maximum Turbine Flow */
$handler->display->display_options['fields']['field_ror_max_turbine_flow']['id'] = 'field_ror_max_turbine_flow';
$handler->display->display_options['fields']['field_ror_max_turbine_flow']['table'] = 'field_data_field_ror_max_turbine_flow';
$handler->display->display_options['fields']['field_ror_max_turbine_flow']['field'] = 'field_ror_max_turbine_flow';
$handler->display->display_options['fields']['field_ror_max_turbine_flow']['settings'] = array(
  'thousand_separator' => ',',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 1,
);
/* Field: Content: Plant Factor */
$handler->display->display_options['fields']['field_ror_plant_factor']['id'] = 'field_ror_plant_factor';
$handler->display->display_options['fields']['field_ror_plant_factor']['table'] = 'field_data_field_ror_plant_factor';
$handler->display->display_options['fields']['field_ror_plant_factor']['field'] = 'field_ror_plant_factor';
$handler->display->display_options['fields']['field_ror_plant_factor']['settings'] = array(
  'thousand_separator' => ',',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 1,
);
/* Field: Content: Fixed Head */
$handler->display->display_options['fields']['field_fixed_head']['id'] = 'field_fixed_head';
$handler->display->display_options['fields']['field_fixed_head']['table'] = 'field_data_field_fixed_head';
$handler->display->display_options['fields']['field_fixed_head']['field'] = 'field_fixed_head';
$handler->display->display_options['fields']['field_fixed_head']['settings'] = array(
  'thousand_separator' => ',',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 1,
);
/* Field: Content: Sima Case Status */
$handler->display->display_options['fields']['field_sima_case_status']['id'] = 'field_sima_case_status';
$handler->display->display_options['fields']['field_sima_case_status']['table'] = 'field_data_field_sima_case_status';
$handler->display->display_options['fields']['field_sima_case_status']['field'] = 'field_sima_case_status';
$handler->display->display_options['fields']['field_sima_case_status']['label'] = 'Base Line';
$handler->display->display_options['fields']['field_sima_case_status']['settings'] = array(
  'custom_link_to_entity' => 0,
  'custom_prefix' => '',
  'custom_suffix' => '',
  'custom_reverse' => 0,
  'custom_trim' => 0,
  'custom_strtolower' => 0,
  'custom_strtoupper' => 0,
  'custom_ucfirst' => 0,
  'custom_ucwords' => 0,
  'custom_strip_tags' => 0,
  'custom_strip_preserve' => '',
);
/* Sort criterion: Content: Post date */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'node';
$handler->display->display_options['sorts']['created']['field'] = 'created';
$handler->display->display_options['sorts']['created']['order'] = 'DESC';
/* Contextual filter: Content: Nid */
$handler->display->display_options['arguments']['nid']['id'] = 'nid';
$handler->display->display_options['arguments']['nid']['table'] = 'node';
$handler->display->display_options['arguments']['nid']['field'] = 'nid';
$handler->display->display_options['arguments']['nid']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['nid']['break_phrase'] = TRUE;
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'ror_hydropower_plant' => 'ror_hydropower_plant',
);

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['defaults']['header'] = FALSE;
$handler->display->display_options['defaults']['arguments'] = FALSE;
$handler->display->display_options['path'] = 'run-of-river-hydropower-plant';

/* Display: Entity Reference */
$handler = $view->new_display('entityreference', 'Entity Reference', 'entityreference_1');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'entityreference_style';
$handler->display->display_options['style_options']['search_fields'] = array(
  'title' => 'title',
  'field_ror_generating_efficiency' => 0,
  'field_ror_max_turbine_flow' => 0,
  'field_ror_plant_factor' => 0,
  'field_ror_hydropower_priority' => 0,
  'field_fixed_head' => 0,
);
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['row_plugin'] = 'entityreference_fields';
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Content: River */
$handler->display->display_options['fields']['field_river']['id'] = 'field_river';
$handler->display->display_options['fields']['field_river']['table'] = 'field_data_field_river';
$handler->display->display_options['fields']['field_river']['field'] = 'field_river';
$handler->display->display_options['fields']['field_river']['label'] = '';
$handler->display->display_options['fields']['field_river']['element_label_colon'] = FALSE;
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
/* Field: Content: Generating Efficiency */
$handler->display->display_options['fields']['field_ror_generating_efficiency']['id'] = 'field_ror_generating_efficiency';
$handler->display->display_options['fields']['field_ror_generating_efficiency']['table'] = 'field_data_field_ror_generating_efficiency';
$handler->display->display_options['fields']['field_ror_generating_efficiency']['field'] = 'field_ror_generating_efficiency';
$handler->display->display_options['fields']['field_ror_generating_efficiency']['label'] = '';
$handler->display->display_options['fields']['field_ror_generating_efficiency']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['field_ror_generating_efficiency']['alter']['text'] = '(Generating Efficiency: [field_ror_generating_efficiency])';
$handler->display->display_options['fields']['field_ror_generating_efficiency']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_ror_generating_efficiency']['settings'] = array(
  'thousand_separator' => ',',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 1,
);

/* Display: Entity Reference View Widget */
$handler = $view->new_display('entityreference_view_widget', 'Entity Reference View Widget', 'entityreference_view_widget_run_of_river');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['defaults']['use_ajax'] = FALSE;
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'entityreference_view_widget' => 'entityreference_view_widget',
  'field_river' => 'field_river',
  'title' => 'title',
  'field_ror_generating_efficiency' => 'field_ror_generating_efficiency',
  'field_ror_max_turbine_flow' => 'field_ror_max_turbine_flow',
  'field_ror_plant_factor' => 'field_ror_plant_factor',
  'field_fixed_head' => 'field_fixed_head',
  'field_sima_case_status' => 'field_sima_case_status',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'entityreference_view_widget' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_river' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'title' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_ror_generating_efficiency' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_ror_max_turbine_flow' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_ror_plant_factor' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_fixed_head' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_sima_case_status' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['defaults']['row_options'] = FALSE;
$translatables['run_of_river_hydropower_plant'] = array(
  t('Master'),
  t('Run of River Hydropower Plant'),
  t('more'),
  t('Apply'),
  t('Reset'),
  t('Sort by'),
  t('Asc'),
  t('Desc'),
  t('Items per page'),
  t('- All -'),
  t('Offset'),
  t('« first'),
  t('‹ previous'),
  t('next ›'),
  t('last »'),
  t('River'),
  t('Title'),
  t('Generating Efficiency'),
  t('Maximum Turbine Flow'),
  t('Plant Factor'),
  t('Fixed Head'),
  t('Base Line'),
  t('All'),
  t('Page'),
  t('Entity Reference'),
  t('(Generating Efficiency: [field_ror_generating_efficiency])'),
  t('Entity Reference View Widget'),
);
