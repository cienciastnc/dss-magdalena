<?php
/**
 * @file
 * Implements view for Change of Land Use Projects.
 */

$view = new view();
$view->name = 'productive_land_use_projects';
$view->description = t('List of Change of Land Use Projects');
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = t('Change of Land Use Projects');
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = t('Change of Land Use Projects');
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '20';
$handler->display->display_options['style_plugin'] = 'table_megarows';
$handler->display->display_options['style_options']['columns'] = array(
  'title' => 'title',
  'field_catchment' => 'field_catchment',
  'field_catchment_area' => 'field_catchment_area',
  'field_demand_priority' => 'field_demand_priority',
  'field_connection_to_water_source' => 'field_connection_to_water_source',
  'field_irr_perc_area' => 'field_irr_perc_area',
  'megarow_links' => 'megarow_links',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'title' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_catchment' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_catchment_area' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_demand_priority' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_connection_to_water_source' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_irr_perc_area' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'megarow_links' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
$handler->display->display_options['style_options']['scroll_padding'] = '120';
$handler->display->display_options['row_plugin'] = 'fields';
/* Field: Entity Reference View Widget Checkbox: Content */
$handler->display->display_options['fields']['entityreference_view_widget']['id'] = 'entityreference_view_widget';
$handler->display->display_options['fields']['entityreference_view_widget']['table'] = 'node';
$handler->display->display_options['fields']['entityreference_view_widget']['field'] = 'entityreference_view_widget';
$handler->display->display_options['fields']['entityreference_view_widget']['label'] = '';
$handler->display->display_options['fields']['entityreference_view_widget']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['entityreference_view_widget']['ervw']['force_single'] = 0;
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
/* Field: Content: Catchment */
$handler->display->display_options['fields']['field_catchment']['id'] = 'field_catchment';
$handler->display->display_options['fields']['field_catchment']['table'] = 'field_data_field_catchment';
$handler->display->display_options['fields']['field_catchment']['field'] = 'field_catchment';
$handler->display->display_options['fields']['field_catchment']['settings'] = array(
  'link' => 1,
);
/* Field: Content: Demand Priority */
$handler->display->display_options['fields']['field_demand_priority']['id'] = 'field_demand_priority';
$handler->display->display_options['fields']['field_demand_priority']['table'] = 'field_data_field_demand_priority';
$handler->display->display_options['fields']['field_demand_priority']['field'] = 'field_demand_priority';
$handler->display->display_options['fields']['field_demand_priority']['settings'] = array(
  'thousand_separator' => '',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 1,
);
/* Field: Content: Catchment Area (Km^2) */
$handler->display->display_options['fields']['field_catchment_area']['id'] = 'field_catchment_area';
$handler->display->display_options['fields']['field_catchment_area']['table'] = 'field_data_field_catchment_area';
$handler->display->display_options['fields']['field_catchment_area']['field'] = 'field_catchment_area';
$handler->display->display_options['fields']['field_catchment_area']['settings'] = array(
  'thousand_separator' => '',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 1,
  'custom_link_to_entity' => 0,
  'custom_prefix' => '',
  'custom_suffix' => '',
  'custom_reverse' => 0,
  'custom_trim' => 0,
  'custom_strtolower' => 0,
  'custom_strtoupper' => 0,
  'custom_ucfirst' => 0,
  'custom_ucwords' => 0,
  'custom_strip_tags' => 0,
  'custom_strip_preserve' => '',
);
/* Field: Content: Connection to Water Source */
$handler->display->display_options['fields']['field_connection_to_water_source']['id'] = 'field_connection_to_water_source';
$handler->display->display_options['fields']['field_connection_to_water_source']['table'] = 'field_data_field_connection_to_water_source';
$handler->display->display_options['fields']['field_connection_to_water_source']['field'] = 'field_connection_to_water_source';
$handler->display->display_options['fields']['field_connection_to_water_source']['settings'] = array(
  'custom_link_to_entity' => 0,
  'custom_prefix' => '',
  'custom_suffix' => '',
  'custom_reverse' => 0,
  'custom_trim' => 0,
  'custom_strtolower' => 0,
  'custom_strtoupper' => 0,
  'custom_ucfirst' => 0,
  'custom_ucwords' => 0,
  'custom_strip_tags' => 0,
  'custom_strip_preserve' => '',
);
/* Field: Field: % Irrigated Area */
$handler->display->display_options['fields']['field_irr_perc_area']['id'] = 'field_irr_perc_area';
$handler->display->display_options['fields']['field_irr_perc_area']['table'] = 'field_data_field_irr_perc_area';
$handler->display->display_options['fields']['field_irr_perc_area']['field'] = 'field_irr_perc_area';
$handler->display->display_options['fields']['field_irr_perc_area']['settings'] = array(
  'thousand_separator' => '',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 1,
  'custom_link_to_entity' => 0,
  'custom_prefix' => '',
  'custom_suffix' => '',
  'custom_reverse' => 0,
  'custom_trim' => 0,
  'custom_strtolower' => 0,
  'custom_strtoupper' => 0,
  'custom_ucfirst' => 0,
  'custom_ucwords' => 0,
  'custom_strip_tags' => 0,
  'custom_strip_preserve' => '',
);
/* Sort criterion: Content: Post date */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'node';
$handler->display->display_options['sorts']['created']['field'] = 'created';
/* Contextual filter: Content: Nid */
$handler->display->display_options['arguments']['nid']['id'] = 'nid';
$handler->display->display_options['arguments']['nid']['table'] = 'node';
$handler->display->display_options['arguments']['nid']['field'] = 'nid';
$handler->display->display_options['arguments']['nid']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['nid']['break_phrase'] = TRUE;
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'demand_sites_and_catchments' => 'demand_sites_and_catchments',
);

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['title'] = t('Change of Land Use Projects');
$handler->display->display_options['defaults']['header'] = FALSE;
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = 'Catchment';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
/* Field: Content: Catchment Area (Km^2) */
$handler->display->display_options['fields']['field_catchment_area']['id'] = 'field_catchment_area';
$handler->display->display_options['fields']['field_catchment_area']['table'] = 'field_data_field_catchment_area';
$handler->display->display_options['fields']['field_catchment_area']['field'] = 'field_catchment_area';
$handler->display->display_options['fields']['field_catchment_area']['label'] = 'Catchment Area';
$handler->display->display_options['fields']['field_catchment_area']['settings'] = array(
  'thousand_separator' => '',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 1,
  'custom_link_to_entity' => 0,
  'custom_prefix' => '',
  'custom_suffix' => '',
  'custom_reverse' => 0,
  'custom_trim' => 0,
  'custom_strtolower' => 0,
  'custom_strtoupper' => 0,
  'custom_ucfirst' => 0,
  'custom_ucwords' => 0,
  'custom_strip_tags' => 0,
  'custom_strip_preserve' => '',
);
/* Field: Content: Agricultural Area */
$handler->display->display_options['fields']['field_agricultural_area']['id'] = 'field_agricultural_area';
$handler->display->display_options['fields']['field_agricultural_area']['table'] = 'field_data_field_agricultural_area';
$handler->display->display_options['fields']['field_agricultural_area']['field'] = 'field_agricultural_area';
$handler->display->display_options['fields']['field_agricultural_area']['settings'] = array(
  'thousand_separator' => ',',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 1,
  'custom_link_to_entity' => 0,
  'custom_prefix' => '',
  'custom_suffix' => '',
  'custom_reverse' => 0,
  'custom_trim' => 0,
  'custom_strtolower' => 0,
  'custom_strtoupper' => 0,
  'custom_ucfirst' => 0,
  'custom_ucwords' => 0,
  'custom_strip_tags' => 0,
  'custom_strip_preserve' => '',
);
/* Field: Content: Forest / Industrial Area */
$handler->display->display_options['fields']['field_forest_industrial_area']['id'] = 'field_forest_industrial_area';
$handler->display->display_options['fields']['field_forest_industrial_area']['table'] = 'field_data_field_forest_industrial_area';
$handler->display->display_options['fields']['field_forest_industrial_area']['field'] = 'field_forest_industrial_area';
$handler->display->display_options['fields']['field_forest_industrial_area']['settings'] = array(
  'thousand_separator' => ',',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 1,
  'custom_link_to_entity' => 0,
  'custom_prefix' => '',
  'custom_suffix' => '',
  'custom_reverse' => 0,
  'custom_trim' => 0,
  'custom_strtolower' => 0,
  'custom_strtoupper' => 0,
  'custom_ucfirst' => 0,
  'custom_ucwords' => 0,
  'custom_strip_tags' => 0,
  'custom_strip_preserve' => '',
);
/* Field: Content: Mining Area */
$handler->display->display_options['fields']['field_mining_area']['id'] = 'field_mining_area';
$handler->display->display_options['fields']['field_mining_area']['table'] = 'field_data_field_mining_area';
$handler->display->display_options['fields']['field_mining_area']['field'] = 'field_mining_area';
$handler->display->display_options['fields']['field_mining_area']['settings'] = array(
  'thousand_separator' => ',',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 1,
  'custom_link_to_entity' => 0,
  'custom_prefix' => '',
  'custom_suffix' => '',
  'custom_reverse' => 0,
  'custom_trim' => 0,
  'custom_strtolower' => 0,
  'custom_strtoupper' => 0,
  'custom_ucfirst' => 0,
  'custom_ucwords' => 0,
  'custom_strip_tags' => 0,
  'custom_strip_preserve' => '',
);
/* Field: Content: Connection to Water Source */
$handler->display->display_options['fields']['field_connection_to_water_source']['id'] = 'field_connection_to_water_source';
$handler->display->display_options['fields']['field_connection_to_water_source']['table'] = 'field_data_field_connection_to_water_source';
$handler->display->display_options['fields']['field_connection_to_water_source']['field'] = 'field_connection_to_water_source';
$handler->display->display_options['fields']['field_connection_to_water_source']['label'] = 'Irrigated';
$handler->display->display_options['fields']['field_connection_to_water_source']['settings'] = array(
  'custom_link_to_entity' => 0,
  'custom_prefix' => '',
  'custom_suffix' => '',
  'custom_reverse' => 0,
  'custom_trim' => 0,
  'custom_strtolower' => 0,
  'custom_strtoupper' => 0,
  'custom_ucfirst' => 0,
  'custom_ucwords' => 0,
  'custom_strip_tags' => 0,
  'custom_strip_preserve' => '',
);
/* Field: Field: % Irrigated Area */
$handler->display->display_options['fields']['field_irr_perc_area']['id'] = 'field_irr_perc_area';
$handler->display->display_options['fields']['field_irr_perc_area']['table'] = 'field_data_field_irr_perc_area';
$handler->display->display_options['fields']['field_irr_perc_area']['field'] = 'field_irr_perc_area';
$handler->display->display_options['fields']['field_irr_perc_area']['settings'] = array(
  'thousand_separator' => '',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 1,
  'custom_link_to_entity' => 0,
  'custom_prefix' => '',
  'custom_suffix' => '',
  'custom_reverse' => 0,
  'custom_trim' => 0,
  'custom_strtolower' => 0,
  'custom_strtoupper' => 0,
  'custom_ucfirst' => 0,
  'custom_ucwords' => 0,
  'custom_strip_tags' => 0,
  'custom_strip_preserve' => '',
);
/* Field: Content: Megarow links */
$handler->display->display_options['fields']['megarow_links']['id'] = 'megarow_links';
$handler->display->display_options['fields']['megarow_links']['table'] = 'views_entity_node';
$handler->display->display_options['fields']['megarow_links']['field'] = 'megarow_links';
$handler->display->display_options['fields']['megarow_links']['label'] = 'Variants';
$handler->display->display_options['fields']['megarow_links']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['megarow_links']['megarow'] = array(
  'links' => 'list|productive-land-use-project-variations/[node:nid]
add|node/add/productive-land-use-variations?field_irrigation_district_ref=[node:nid]',
);
$handler->display->display_options['defaults']['arguments'] = FALSE;
$handler->display->display_options['path'] = 'productive-land-use-projects';

/* Display: Entity Reference */
$handler = $view->new_display('entityreference', 'Entity Reference', 'entityreference_1');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'entityreference_style';
$handler->display->display_options['style_options']['search_fields'] = array(
  'title' => 'title',
  'field_catchment' => 0,
  'field_demand_priority' => 0,
);
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['row_plugin'] = 'entityreference_fields';
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
/* Field: Content: Demand Priority */
$handler->display->display_options['fields']['field_demand_priority']['id'] = 'field_demand_priority';
$handler->display->display_options['fields']['field_demand_priority']['table'] = 'field_data_field_demand_priority';
$handler->display->display_options['fields']['field_demand_priority']['field'] = 'field_demand_priority';
$handler->display->display_options['fields']['field_demand_priority']['settings'] = array(
  'thousand_separator' => '',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 1,
);

/* Display: Entity Reference View Widget */
$handler = $view->new_display('entityreference_view_widget', 'Entity Reference View Widget', 'entityreference_view_widget_productive_landuse_projects');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['defaults']['use_ajax'] = FALSE;
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['grouping'] = array(
  0 => array(
    'field' => 'field_irrigation_district_ref',
    'rendered' => 1,
    'rendered_strip' => 0,
  ),
);
$handler->display->display_options['style_options']['columns'] = array(
  'entityreference_view_widget' => 'entityreference_view_widget',
  'title' => 'title',
  'field_irr_perc_area' => 'field_irr_perc_area',
  'field_irrigation_district_ref' => 'field_irrigation_district_ref',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'entityreference_view_widget' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'title' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_irr_perc_area' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_irrigation_district_ref' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['defaults']['relationships'] = FALSE;
/* Relationship: Entity Reference: Referenced Entity */
$handler->display->display_options['relationships']['field_irrigation_district_ref_target_id']['id'] = 'field_irrigation_district_ref_target_id';
$handler->display->display_options['relationships']['field_irrigation_district_ref_target_id']['table'] = 'field_data_field_irrigation_district_ref';
$handler->display->display_options['relationships']['field_irrigation_district_ref_target_id']['field'] = 'field_irrigation_district_ref_target_id';
$handler->display->display_options['relationships']['field_irrigation_district_ref_target_id']['required'] = TRUE;
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Entity Reference View Widget Checkbox: Content */
$handler->display->display_options['fields']['entityreference_view_widget']['id'] = 'entityreference_view_widget';
$handler->display->display_options['fields']['entityreference_view_widget']['table'] = 'node';
$handler->display->display_options['fields']['entityreference_view_widget']['field'] = 'entityreference_view_widget';
$handler->display->display_options['fields']['entityreference_view_widget']['label'] = '';
$handler->display->display_options['fields']['entityreference_view_widget']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['entityreference_view_widget']['ervw']['force_single'] = 0;
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
/* Field: Content: Agricultural Area */
$handler->display->display_options['fields']['field_agricultural_area']['id'] = 'field_agricultural_area';
$handler->display->display_options['fields']['field_agricultural_area']['table'] = 'field_data_field_agricultural_area';
$handler->display->display_options['fields']['field_agricultural_area']['field'] = 'field_agricultural_area';
$handler->display->display_options['fields']['field_agricultural_area']['settings'] = array(
  'thousand_separator' => ',',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 1,
  'custom_link_to_entity' => 0,
  'custom_prefix' => '',
  'custom_suffix' => '',
  'custom_reverse' => 0,
  'custom_trim' => 0,
  'custom_strtolower' => 0,
  'custom_strtoupper' => 0,
  'custom_ucfirst' => 0,
  'custom_ucwords' => 0,
  'custom_strip_tags' => 0,
  'custom_strip_preserve' => '',
);
/* Field: Content: Forest / Industrial Area */
$handler->display->display_options['fields']['field_forest_industrial_area']['id'] = 'field_forest_industrial_area';
$handler->display->display_options['fields']['field_forest_industrial_area']['table'] = 'field_data_field_forest_industrial_area';
$handler->display->display_options['fields']['field_forest_industrial_area']['field'] = 'field_forest_industrial_area';
$handler->display->display_options['fields']['field_forest_industrial_area']['settings'] = array(
  'thousand_separator' => ',',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 1,
  'custom_link_to_entity' => 0,
  'custom_prefix' => '',
  'custom_suffix' => '',
  'custom_reverse' => 0,
  'custom_trim' => 0,
  'custom_strtolower' => 0,
  'custom_strtoupper' => 0,
  'custom_ucfirst' => 0,
  'custom_ucwords' => 0,
  'custom_strip_tags' => 0,
  'custom_strip_preserve' => '',
);
/* Field: Content: Mining Area */
$handler->display->display_options['fields']['field_mining_area']['id'] = 'field_mining_area';
$handler->display->display_options['fields']['field_mining_area']['table'] = 'field_data_field_mining_area';
$handler->display->display_options['fields']['field_mining_area']['field'] = 'field_mining_area';
$handler->display->display_options['fields']['field_mining_area']['settings'] = array(
  'thousand_separator' => ',',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 1,
  'custom_link_to_entity' => 0,
  'custom_prefix' => '',
  'custom_suffix' => '',
  'custom_reverse' => 0,
  'custom_trim' => 0,
  'custom_strtolower' => 0,
  'custom_strtoupper' => 0,
  'custom_ucfirst' => 0,
  'custom_ucwords' => 0,
  'custom_strip_tags' => 0,
  'custom_strip_preserve' => '',
);
/* Field: Field: % Irrigated Area */
$handler->display->display_options['fields']['field_irr_perc_area']['id'] = 'field_irr_perc_area';
$handler->display->display_options['fields']['field_irr_perc_area']['table'] = 'field_data_field_irr_perc_area';
$handler->display->display_options['fields']['field_irr_perc_area']['field'] = 'field_irr_perc_area';
$handler->display->display_options['fields']['field_irr_perc_area']['settings'] = array(
  'thousand_separator' => '',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 1,
  'custom_link_to_entity' => 0,
  'custom_prefix' => '',
  'custom_suffix' => '',
  'custom_reverse' => 0,
  'custom_trim' => 0,
  'custom_strtolower' => 0,
  'custom_strtoupper' => 0,
  'custom_ucfirst' => 0,
  'custom_ucwords' => 0,
  'custom_strip_tags' => 0,
  'custom_strip_preserve' => '',
);
/* Field: Content: Irrigation District */
$handler->display->display_options['fields']['field_irrigation_district_ref']['id'] = 'field_irrigation_district_ref';
$handler->display->display_options['fields']['field_irrigation_district_ref']['table'] = 'field_data_field_irrigation_district_ref';
$handler->display->display_options['fields']['field_irrigation_district_ref']['field'] = 'field_irrigation_district_ref';
$handler->display->display_options['fields']['field_irrigation_district_ref']['label'] = t('Change of Land Use Project');
$handler->display->display_options['fields']['field_irrigation_district_ref']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_irrigation_district_ref']['settings'] = array(
  'link' => 1,
  'custom_link_to_entity' => 0,
  'custom_prefix' => '',
  'custom_suffix' => '',
  'custom_reverse' => 0,
  'custom_trim' => 0,
  'custom_strtolower' => 0,
  'custom_strtoupper' => 0,
  'custom_ucfirst' => 0,
  'custom_ucwords' => 0,
  'custom_strip_tags' => 0,
  'custom_strip_preserve' => '',
);
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'productive_land_use_variations' => 'productive_land_use_variations',
);
$translatables['productive_land_use_projects'] = array(
  t('Master'),
  t('Change of Land Use Projects'),
  t('more'),
  t('Apply'),
  t('Reset'),
  t('Sort by'),
  t('Asc'),
  t('Desc'),
  t('Items per page'),
  t('- All -'),
  t('Offset'),
  t('« first'),
  t('‹ previous'),
  t('next ›'),
  t('last »'),
  t('Title'),
  t('Catchment'),
  t('Demand Priority'),
  t('Catchment Area (Km^2)'),
  t('Connection to Water Source'),
  t('% Irrigated Area'),
  t('All'),
  t('Page'),
  t('Catchment Area'),
  t('Agricultural Area'),
  t('Forest / Industrial Area'),
  t('Mining Area'),
  t('Irrigated'),
  t('Variants'),
  t('Entity Reference'),
  t('Entity Reference View Widget'),
  t('Content entity referenced from field_irrigation_district_ref'),
  t('Change of Land Use Project'),
);
