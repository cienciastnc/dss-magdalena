<?php
/**
 * @file
 * Default view for Resource Entity References.
 */

$view = new view();
$view->name = 'weap_resources_references';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'WEAP Resources References';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['row_plugin'] = 'fields';
/* Relationship: Entity Reference: Referenced Entity */
$handler->display->display_options['relationships']['field_dataset_ref_target_id']['id'] = 'field_dataset_ref_target_id';
$handler->display->display_options['relationships']['field_dataset_ref_target_id']['table'] = 'field_data_field_dataset_ref';
$handler->display->display_options['relationships']['field_dataset_ref_target_id']['field'] = 'field_dataset_ref_target_id';
$handler->display->display_options['relationships']['field_dataset_ref_target_id']['label'] = 'Dataset';
$handler->display->display_options['relationships']['field_dataset_ref_target_id']['required'] = TRUE;
/* Field: Content: Dataset */
$handler->display->display_options['fields']['field_dataset_ref']['id'] = 'field_dataset_ref';
$handler->display->display_options['fields']['field_dataset_ref']['table'] = 'field_data_field_dataset_ref';
$handler->display->display_options['fields']['field_dataset_ref']['field'] = 'field_dataset_ref';
$handler->display->display_options['fields']['field_dataset_ref']['label'] = '';
$handler->display->display_options['fields']['field_dataset_ref']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['field_dataset_ref']['alter']['text'] = 'DSS Variable: [field_dataset_ref]';
$handler->display->display_options['fields']['field_dataset_ref']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_dataset_ref']['settings'] = array(
  'link' => 0,
);
$handler->display->display_options['fields']['field_dataset_ref']['delta_offset'] = '0';
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
/* Sort criterion: Content: Post date */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'node';
$handler->display->display_options['sorts']['created']['field'] = 'created';
$handler->display->display_options['sorts']['created']['order'] = 'DESC';
/* Contextual filter: Content: Variable Status (field_variable_status) */
$handler->display->display_options['arguments']['field_variable_status_tid']['id'] = 'field_variable_status_tid';
$handler->display->display_options['arguments']['field_variable_status_tid']['table'] = 'field_data_field_variable_status';
$handler->display->display_options['arguments']['field_variable_status_tid']['field'] = 'field_variable_status_tid';
$handler->display->display_options['arguments']['field_variable_status_tid']['relationship'] = 'field_dataset_ref_target_id';
$handler->display->display_options['arguments']['field_variable_status_tid']['default_action'] = 'not found';
$handler->display->display_options['arguments']['field_variable_status_tid']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['field_variable_status_tid']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['field_variable_status_tid']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['field_variable_status_tid']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['field_variable_status_tid']['specify_validation'] = TRUE;
$handler->display->display_options['arguments']['field_variable_status_tid']['validate']['type'] = 'taxonomy_term';
$handler->display->display_options['arguments']['field_variable_status_tid']['validate_options']['vocabularies'] = array(
  'variable_status' => 'variable_status',
);
$handler->display->display_options['arguments']['field_variable_status_tid']['validate_options']['type'] = 'convert';
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'resource' => 'resource',
);

/* Display: Entity Reference */
$handler = $view->new_display('entityreference', 'Entity Reference', 'entityreference_1');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['pager']['options']['items_per_page'] = '100';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'entityreference_style';
$handler->display->display_options['style_options']['grouping'] = array(
  0 => array(
    'field' => 'field_dataset_ref',
    'rendered' => 1,
    'rendered_strip' => 0,
  ),
);
$handler->display->display_options['style_options']['search_fields'] = array(
  'title' => 'title',
  'field_dataset_ref' => 0,
);
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['row_plugin'] = 'entityreference_fields';
$handler->display->display_options['defaults']['row_options'] = FALSE;
$translatables['weap_resources_references'] = array(
  t('Master'),
  t('more'),
  t('Apply'),
  t('Reset'),
  t('Sort by'),
  t('Asc'),
  t('Desc'),
  t('Items per page'),
  t('- All -'),
  t('Offset'),
  t('« first'),
  t('‹ previous'),
  t('next ›'),
  t('last »'),
  t('Dataset'),
  t('DSS Variable: [field_dataset_ref]'),
  t('All'),
  t('Entity Reference'),
);
