<?php
/**
 * @file
 * Interface to the Views API from the dss_engine.
 */

/**
 * Alter Views Data to create a new field: "model_status".
 *
 * @param array $data
 *   The views data array.
 */
function dss_engine_views_data_alter(&$data) {
  $data['node']['model_status'] = array(
    'title' => t('Modeling Status'),
    'help' => t('Displays the Modeling Status for a specific Case Study.'),
    'field' => array(
      'handler' => 'dss_engine_views_handler_field_model_status',
      'group' => 'Content',
      'click sortable' => FALSE,
    ),
  );
}
