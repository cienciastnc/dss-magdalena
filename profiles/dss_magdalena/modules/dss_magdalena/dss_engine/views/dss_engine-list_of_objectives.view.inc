<?php
/**
 * @file
 * List of Objectives View.
 */

$view = new view();
$view->name = 'list_of_objectives';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'List of Objectives';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Selected Objectives';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['pager']['options']['items_per_page'] = '5';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['row_plugin'] = 'fields';
/* No results behavior: Global: Text area */
$handler->display->display_options['empty']['area_1']['id'] = 'area_1';
$handler->display->display_options['empty']['area_1']['table'] = 'views';
$handler->display->display_options['empty']['area_1']['field'] = 'area';
$handler->display->display_options['empty']['area_1']['empty'] = TRUE;
$handler->display->display_options['empty']['area_1']['content'] = 'No tiene objetivos seleccionados. <a href="/node/add/selected-objectives?destination=[current_path]">Seleccionar Objetivos</a> para poder continuar.';
$handler->display->display_options['empty']['area_1']['format'] = 'filtered_html';
/* Field: Content: Nid */
$handler->display->display_options['fields']['nid']['id'] = 'nid';
$handler->display->display_options['fields']['nid']['table'] = 'node';
$handler->display->display_options['fields']['nid']['field'] = 'nid';
$handler->display->display_options['fields']['nid']['label'] = '';
$handler->display->display_options['fields']['nid']['exclude'] = TRUE;
$handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
/* Field: Global: Current Path */
$handler->display->display_options['fields']['current_path']['id'] = 'current_path';
$handler->display->display_options['fields']['current_path']['table'] = 'views';
$handler->display->display_options['fields']['current_path']['field'] = 'current_path';
$handler->display->display_options['fields']['current_path']['label'] = '';
$handler->display->display_options['fields']['current_path']['exclude'] = TRUE;
$handler->display->display_options['fields']['current_path']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['current_path']['qs_support_fieldset'] = array(
  'query_string_support' => NULL,
);
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['exclude'] = TRUE;
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
/* Field: Content: Selected Objectives */
$handler->display->display_options['fields']['field_selected_objectives']['id'] = 'field_selected_objectives';
$handler->display->display_options['fields']['field_selected_objectives']['table'] = 'field_data_field_selected_objectives';
$handler->display->display_options['fields']['field_selected_objectives']['field'] = 'field_selected_objectives';
$handler->display->display_options['fields']['field_selected_objectives']['label'] = '';
$handler->display->display_options['fields']['field_selected_objectives']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_selected_objectives']['settings'] = array(
  'link' => 1,
  'custom_link_to_entity' => 0,
  'custom_prefix' => '',
  'custom_suffix' => '',
  'custom_reverse' => 0,
  'custom_trim' => 0,
  'custom_strtolower' => 0,
  'custom_strtoupper' => 0,
  'custom_ucfirst' => 0,
  'custom_ucwords' => 0,
  'custom_strip_tags' => 0,
  'custom_strip_preserve' => '',
);
$handler->display->display_options['fields']['field_selected_objectives']['delta_offset'] = '0';
$handler->display->display_options['fields']['field_selected_objectives']['multi_type'] = 'ul';
/* Field: Global: Custom text */
$handler->display->display_options['fields']['nothing']['id'] = 'nothing';
$handler->display->display_options['fields']['nothing']['table'] = 'views';
$handler->display->display_options['fields']['nothing']['field'] = 'nothing';
$handler->display->display_options['fields']['nothing']['label'] = '';
$handler->display->display_options['fields']['nothing']['alter']['text'] = '<a href="/node/[nid]/edit?destination=[current_path]">Cambiar selección</a>';
$handler->display->display_options['fields']['nothing']['element_class'] = 'float-right';
$handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
/* Sort criterion: Content: Post date */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'node';
$handler->display->display_options['sorts']['created']['field'] = 'created';
$handler->display->display_options['sorts']['created']['order'] = 'DESC';
/* Contextual filter: Content: Author uid */
$handler->display->display_options['arguments']['uid']['id'] = 'uid';
$handler->display->display_options['arguments']['uid']['table'] = 'node';
$handler->display->display_options['arguments']['uid']['field'] = 'uid';
$handler->display->display_options['arguments']['uid']['default_action'] = 'default';
$handler->display->display_options['arguments']['uid']['default_argument_type'] = 'current_user';
$handler->display->display_options['arguments']['uid']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['uid']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['uid']['summary_options']['items_per_page'] = '25';
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'selected_objectives' => 'selected_objectives',
);

/* Display: Block */
$handler = $view->new_display('block', 'Block', 'block');
$translatables['list_of_objectives'] = array(
  t('Master'),
  t('Selected Objectives'),
  t('more'),
  t('Apply'),
  t('Reset'),
  t('Sort by'),
  t('Asc'),
  t('Desc'),
  t('No tiene objetivos seleccionados. <a href="/node/add/selected-objectives?destination=[current_path]">Seleccionar Objetivos</a> para poder continuar.'),
  t('<a href="/node/[nid]/edit?destination=[current_path]">Cambiar selección</a>'),
  t('All'),
  t('Block'),
);
