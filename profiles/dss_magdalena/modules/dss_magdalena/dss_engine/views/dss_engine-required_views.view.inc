<?php
/**
 * @file
 * View for Model "Required Variables".
 */

$view = new view();
$view->name = 'required_variables';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Required Variables';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['row_plugin'] = 'fields';
/* Field: Content: Variable Status */
$handler->display->display_options['fields']['field_variable_status']['id'] = 'field_variable_status';
$handler->display->display_options['fields']['field_variable_status']['table'] = 'field_data_field_variable_status';
$handler->display->display_options['fields']['field_variable_status']['field'] = 'field_variable_status';
$handler->display->display_options['fields']['field_variable_status']['label'] = '';
$handler->display->display_options['fields']['field_variable_status']['element_label_colon'] = FALSE;
/* Field: Content: Variable Category */
$handler->display->display_options['fields']['field_variable_category']['id'] = 'field_variable_category';
$handler->display->display_options['fields']['field_variable_category']['table'] = 'field_data_field_variable_category';
$handler->display->display_options['fields']['field_variable_category']['field'] = 'field_variable_category';
$handler->display->display_options['fields']['field_variable_category']['label'] = '';
$handler->display->display_options['fields']['field_variable_category']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['field_variable_category']['alter']['text'] = '([field_variable_category])';
$handler->display->display_options['fields']['field_variable_category']['element_label_colon'] = FALSE;
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
/* Sort criterion: Content: Variable Status (field_variable_status) */
$handler->display->display_options['sorts']['field_variable_status_tid']['id'] = 'field_variable_status_tid';
$handler->display->display_options['sorts']['field_variable_status_tid']['table'] = 'field_data_field_variable_status';
$handler->display->display_options['sorts']['field_variable_status_tid']['field'] = 'field_variable_status_tid';
/* Sort criterion: Content: Variable Category (field_variable_category) */
$handler->display->display_options['sorts']['field_variable_category_tid']['id'] = 'field_variable_category_tid';
$handler->display->display_options['sorts']['field_variable_category_tid']['table'] = 'field_data_field_variable_category';
$handler->display->display_options['sorts']['field_variable_category_tid']['field'] = 'field_variable_category_tid';
/* Sort criterion: Content: Post date */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'node';
$handler->display->display_options['sorts']['created']['field'] = 'created';
$handler->display->display_options['sorts']['created']['order'] = 'DESC';
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'dataset' => 'dataset',
);

/* Display: Entity Reference */
$handler = $view->new_display('entityreference', 'Entity Reference', 'entityreference_1');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['pager']['options']['items_per_page'] = '1000';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'entityreference_style';
$handler->display->display_options['style_options']['search_fields'] = array(
  'title' => 'title',
);
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['row_plugin'] = 'entityreference_fields';
$handler->display->display_options['defaults']['row_options'] = FALSE;
$translatables['required_variables'] = array(
  t('Master'),
  t('more'),
  t('Apply'),
  t('Reset'),
  t('Sort by'),
  t('Asc'),
  t('Desc'),
  t('Items per page'),
  t('- All -'),
  t('Offset'),
  t('« first'),
  t('‹ previous'),
  t('next ›'),
  t('last »'),
  t('([field_variable_category])'),
  t('Entity Reference'),
);
