<?php
/**
 * @file
 * View: Case Study Outputs.
 */

$view = new view();
$view->name = 'case_study_outputs';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Case Study Outputs';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Case Study Outputs';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '30';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['grouping'] = array(
  0 => array(
    'field' => 'field_variable_category',
    'rendered' => 1,
    'rendered_strip' => 0,
  ),
);
$handler->display->display_options['style_options']['columns'] = array(
  'title' => 'title',
  'field_variable_category' => 'field_variable_category',
  'field_subsystem' => 'field_subsystem',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'title' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_variable_category' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_subsystem' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
/* Header: Global: Text area */
$handler->display->display_options['header']['area']['id'] = 'area';
$handler->display->display_options['header']['area']['table'] = 'views';
$handler->display->display_options['header']['area']['field'] = 'area';
$handler->display->display_options['header']['area']['label'] = 'Sample Outputs';
$handler->display->display_options['header']['area']['content'] = 'Sample outputs
<ul>
<li><a href="/variables-per-catchments">Variables per Catchments</a></li>
<li><a href="/variables-per-river-reaches">Variables per River Reaches</a></li>
</ul>';
$handler->display->display_options['header']['area']['format'] = 'filtered_html';
/* Relationship: Entity Reference: Referenced Entity */
$handler->display->display_options['relationships']['field_weap_outputs_target_id']['id'] = 'field_weap_outputs_target_id';
$handler->display->display_options['relationships']['field_weap_outputs_target_id']['table'] = 'field_data_field_weap_outputs';
$handler->display->display_options['relationships']['field_weap_outputs_target_id']['field'] = 'field_weap_outputs_target_id';
$handler->display->display_options['relationships']['field_weap_outputs_target_id']['label'] = 'Resource';
$handler->display->display_options['relationships']['field_weap_outputs_target_id']['required'] = TRUE;
/* Relationship: Entity Reference: Referenced Entity */
$handler->display->display_options['relationships']['field_dataset_ref_target_id']['id'] = 'field_dataset_ref_target_id';
$handler->display->display_options['relationships']['field_dataset_ref_target_id']['table'] = 'field_data_field_dataset_ref';
$handler->display->display_options['relationships']['field_dataset_ref_target_id']['field'] = 'field_dataset_ref_target_id';
$handler->display->display_options['relationships']['field_dataset_ref_target_id']['relationship'] = 'field_weap_outputs_target_id';
$handler->display->display_options['relationships']['field_dataset_ref_target_id']['label'] = 'Dataset entity referenced from field_dataset_ref';
$handler->display->display_options['relationships']['field_dataset_ref_target_id']['required'] = TRUE;
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['relationship'] = 'field_weap_outputs_target_id';
$handler->display->display_options['fields']['title']['label'] = 'WEAP Outputs';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
/* Field: Content: Variable Category */
$handler->display->display_options['fields']['field_variable_category']['id'] = 'field_variable_category';
$handler->display->display_options['fields']['field_variable_category']['table'] = 'field_data_field_variable_category';
$handler->display->display_options['fields']['field_variable_category']['field'] = 'field_variable_category';
$handler->display->display_options['fields']['field_variable_category']['relationship'] = 'field_dataset_ref_target_id';
$handler->display->display_options['fields']['field_variable_category']['settings'] = array(
  'custom_link_to_entity' => 0,
  'custom_prefix' => '',
  'custom_suffix' => '',
  'custom_reverse' => 0,
  'custom_trim' => 0,
  'custom_strtolower' => 0,
  'custom_strtoupper' => 0,
  'custom_ucfirst' => 0,
  'custom_ucwords' => 0,
  'custom_strip_tags' => 0,
  'custom_strip_preserve' => '',
);
/* Field: Content: Subsystem */
$handler->display->display_options['fields']['field_subsystem']['id'] = 'field_subsystem';
$handler->display->display_options['fields']['field_subsystem']['table'] = 'field_data_field_subsystem';
$handler->display->display_options['fields']['field_subsystem']['field'] = 'field_subsystem';
$handler->display->display_options['fields']['field_subsystem']['relationship'] = 'field_dataset_ref_target_id';
$handler->display->display_options['fields']['field_subsystem']['settings'] = array(
  'custom_link_to_entity' => 0,
  'custom_prefix' => '',
  'custom_suffix' => '',
  'custom_reverse' => 0,
  'custom_trim' => 0,
  'custom_strtolower' => 0,
  'custom_strtoupper' => 0,
  'custom_ucfirst' => 0,
  'custom_ucwords' => 0,
  'custom_strip_tags' => 0,
  'custom_strip_preserve' => '',
);
/* Field: Content: Model Family */
$handler->display->display_options['fields']['field_model_family']['id'] = 'field_model_family';
$handler->display->display_options['fields']['field_model_family']['table'] = 'field_data_field_model_family';
$handler->display->display_options['fields']['field_model_family']['field'] = 'field_model_family';
$handler->display->display_options['fields']['field_model_family']['relationship'] = 'field_dataset_ref_target_id';
$handler->display->display_options['fields']['field_model_family']['settings'] = array(
  'custom_link_to_entity' => 0,
  'custom_prefix' => '',
  'custom_suffix' => '',
  'custom_reverse' => 0,
  'custom_trim' => 0,
  'custom_strtolower' => 0,
  'custom_strtoupper' => 0,
  'custom_ucfirst' => 0,
  'custom_ucwords' => 0,
  'custom_strip_tags' => 0,
  'custom_strip_preserve' => '',
);
/* Sort criterion: Content: Variable Category (field_variable_category) */
$handler->display->display_options['sorts']['field_variable_category_tid']['id'] = 'field_variable_category_tid';
$handler->display->display_options['sorts']['field_variable_category_tid']['table'] = 'field_data_field_variable_category';
$handler->display->display_options['sorts']['field_variable_category_tid']['field'] = 'field_variable_category_tid';
$handler->display->display_options['sorts']['field_variable_category_tid']['relationship'] = 'field_dataset_ref_target_id';
/* Sort criterion: Content: Post date */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'node';
$handler->display->display_options['sorts']['created']['field'] = 'created';
$handler->display->display_options['sorts']['created']['order'] = 'DESC';
/* Contextual filter: Content: Case Study (field_case_study_outputs) */
$handler->display->display_options['arguments']['field_case_study_outputs_target_id']['id'] = 'field_case_study_outputs_target_id';
$handler->display->display_options['arguments']['field_case_study_outputs_target_id']['table'] = 'field_data_field_case_study_outputs';
$handler->display->display_options['arguments']['field_case_study_outputs_target_id']['field'] = 'field_case_study_outputs_target_id';
$handler->display->display_options['arguments']['field_case_study_outputs_target_id']['title_enable'] = TRUE;
$handler->display->display_options['arguments']['field_case_study_outputs_target_id']['title'] = 'WEAP Outputs for %1';
$handler->display->display_options['arguments']['field_case_study_outputs_target_id']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['field_case_study_outputs_target_id']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['field_case_study_outputs_target_id']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['field_case_study_outputs_target_id']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['field_case_study_outputs_target_id']['specify_validation'] = TRUE;
$handler->display->display_options['arguments']['field_case_study_outputs_target_id']['validate']['type'] = 'node';
$handler->display->display_options['arguments']['field_case_study_outputs_target_id']['validate_options']['types'] = array(
  'case_study' => 'case_study',
);
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'case_outputs' => 'case_outputs',
);

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['path'] = 'case-study-outputs';
$translatables['case_study_outputs'] = array(
  t('Master'),
  t('Case Study Outputs'),
  t('more'),
  t('Apply'),
  t('Reset'),
  t('Sort by'),
  t('Asc'),
  t('Desc'),
  t('Items per page'),
  t('- All -'),
  t('Offset'),
  t('« first'),
  t('‹ previous'),
  t('next ›'),
  t('last »'),
  t('Sample Outputs'),
  t('Sample outputs
<ul>
<li><a href="/variables-per-catchments">Variables per Catchments</a></li>
<li><a href="/variables-per-river-reaches">Variables per River Reaches</a></li>
</ul>'),
  t('Resource'),
  t('Dataset entity referenced from field_dataset_ref'),
  t('WEAP Outputs'),
  t('Variable Category'),
  t('Subsystem'),
  t('Model Family'),
  t('All'),
  t('WEAP Outputs for %1'),
  t('Page'),
);
