<?php
/**
 * @file
 * Helper functions for the api script.
 */

/**
 * Loads a Dataset.
 *
 * @param string $machine_name
 *   The dataset machine name.
 *
 * @return bool|mixed|null
 *   The Drupal entity or FALSE.
 */
function dss_engine_services_load_dataset($machine_name) {
  // @codingStandardsIgnoreStart
  // $query = new EntityFieldQuery();
  //  $query->entityCondition('entity_type', 'node')
  //    ->entityCondition('bundle', 'dataset')
  //    ->propertyCondition('status', NODE_PUBLISHED)
  //    ->fieldCondition('field_machine_name', 'value', $machine_name, '=');
  //  $result = $query->execute();
  //
  //  if (isset($result['node'])) {
  //    // Here we are only taking the first result, because machine names are
  //    // uniques.
  //    $node = reset($result['node']);
  //    if (!empty($node)) {
  //      return node_load($node->nid);
  //    }
  //  }
  //  return FALSE;.
  // @codingStandardsIgnoreEnd
  return (object) [
    'nid' => 193,
  ];
}
