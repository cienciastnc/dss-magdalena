<?php
/**
 * @file
 * Implements field modifications to entity forms.
 */

use Drupal\dss_magdalena\DSS\Entity\SimaProductiveLandUseProjectVariant;
use Drupal\dss_magdalena\DSS\Entity\Alternatives\SimaHydropowerAlternative;

/**
 * Implements hook_field_attach_form().
 */
function dss_engine_field_attach_form($entity_type, $entity, &$form, &$form_state, $langcode) {
  if (!isset($entity->type)) {
    return;
  }
  switch ($entity->type) {
    case SimaProductiveLandUseProjectVariant::BUNDLE:
      $catchment_id = isset($form[SimaProductiveLandUseProjectVariant::FIELD_IRRIGATION_DISTRICT][LANGUAGE_NONE]['#default_value'][0]) ?
        $form[SimaProductiveLandUseProjectVariant::FIELD_IRRIGATION_DISTRICT][LANGUAGE_NONE]['#default_value'][0] : FALSE;
      if ($catchment = \Drupal\dss_magdalena\DSS\Entity\SimaCatchments::load($catchment_id)) {
        if ($landuse_variation = SimaProductiveLandUseProjectVariant::load($entity)) {
          /** @var \Drupal\dss_magdalena\DSS\Entity\Multifield\SimaLandUsePercentages[] $land_use_percentages */
          $land_use_percentages = $catchment->getLandUsePercentages();

          // Checking if this productive land use variant has already been
          // assigned land use from catchments or not. If we are editing an
          // existent node then it has already been assigned.
          $land_use = $landuse_variation->getLandUsePercentages();
          if (count($land_use) < 1) {

            // Converting Land Use Percentages to Simple Land Use Percentages.
            $simple_land_use_percentages = [];
            foreach ($land_use_percentages as $land_use_percentage) {
              $simple_land_use_percentages[] = $land_use_percentage->toSimpleLandUsePercentage();
            }

            // Assigning the simple land use percentage to this project
            // variation.
            $landuse_variation->setCatchmentArea($catchment->getCatchmentArea());
            $landuse_variation->setLandUsePercentage($simple_land_use_percentages);
            $entity = $landuse_variation->getDrupalEntity()->value();

            // If no language is provided use the default site language.
            unset($form[SimaProductiveLandUseProjectVariant::FIELD_LAND_USE]);
            unset($form_state['field']);
            $options['language'] = field_valid_language($langcode);
            $form += (array) _field_invoke_default('form', $entity_type, $entity, $form, $form_state, $options);
          }

          // Adding extra information to the entity.
          $entity->__extra = array(
            'catchment' => $catchment->getId(),
          );

          // Adding javascript file.
          $form['#attached']['js'] = array(
            array(
              'data' => drupal_get_path('module', 'dss_engine') . '/js/dss_engine.productive_landuse.js',
              'type' => 'file',
            ),
            array(
              'type' => 'setting',
              'data' => array(
                'dss_engine' => array(
                  'percentage_irrigated_area_base' => $catchment->getPercentageOfIrrigatedArea(),
                ),
              ),
            ),
          );
          $form['#attached']['css'] = array(
            drupal_get_path('module', 'dss_engine') . '/css/dss_engine-prod_landuse.css',
          );
        }
      }
      break;

    case SimaHydropowerAlternative::HYDROPOWER_ALTERNATIVE:
      if (!empty($entity->nid)) {
        return;
      }
      if ($hydro_alternative = SimaHydropowerAlternative::load($entity)) {
        $hydro_alternative->populateHydropowerAlternatives();
        $entity = $hydro_alternative->getDrupalEntity()->value();

        // If no language is provided use the default site language.
        unset($form[SimaHydropowerAlternative::HYDROPOWER_PROJECTS]);
        unset($form[SimaHydropowerAlternative::ROR_HYDROPOWER_PROJECTS]);
        unset($form_state['field']);
        $options['language'] = field_valid_language($langcode);
        $form += (array) _field_invoke_default('form', $entity_type, $entity, $form, $form_state, $options);
      }
      break;

  }
}
