<?php
/**
 * @file
 * Functions for objectives.
 */

use Drupal\dss_magdalena\DSS\Entity\SimaCaseStudy;
use Drupal\dss_magdalena\DSS\Entity\SimaResource;
use Drupal\dss_magdalena\DSS\Queue\SimaImportQueueItem;
use League\Csv\Writer;

/**
 * Creates sample objectives for a particular case study.
 *
 * @param int $cid
 *   The Case study ID.
 */
function dss_engine_create_resource_objectives_for_case_study($cid) {
  if ($case = SimaCaseStudy::load($cid)) {
    $dataset_machine_names = [
      'dss_ecosistema',
      'dss_costos',
      'dss_molestia',
      'dss_energia',
      'dss_riesgo',
    ];
    foreach ($dataset_machine_names as $dataset) {
      dss_engine_create_resource($cid, $dataset);
    }
  }
}

/**
 * Creates a Resource for a specific Objective.
 *
 * @param int $cid
 *   The case study ID.
 * @param string $dataset
 *   The dataset name.
 *
 * @return bool|\Drupal\dss_magdalena\DSS\Entity\SimaResource
 *   The SimaResource if it was created, FALSE otherwise.
 */
function dss_engine_create_resource($cid, $dataset) {
  $data[] = [
    'BranchID',
    'GeoBranchId',
    'Year',
    'Timestep',
    'Value',
  ];
  $data[] = [NULL, NULL, NULL, NULL, rand(0, 100) / 100];
  $directory = "public://objectives/{$cid}";
  if (file_prepare_directory($directory, FILE_CREATE_DIRECTORY)) {
    $destination = $directory . "/{$dataset}.csv";
    $csv = Writer::createFromFileObject(new SplTempFileObject());
    $csv->setOutputBOM("\xEF\xBB\xBF");
    $csv->insertAll($data);
    if ($resource_file = file_unmanaged_save_data($csv, $destination, FILE_EXISTS_REPLACE)) {
      $values = [
        'cid' => $cid,
        'case_output' => NULL,
        'file_uri' => $destination,
        'filename' => $dataset . '.csv',
        'dss_machine_name' => $dataset,
      ];
      $queue_item = new SimaImportQueueItem($values);
      if ($resource = SimaResource::newFullyFormedResource($queue_item)) {
        $resource->save();
        $title = t('Created Resource !name', [
          '!name' => $resource->getTitle(),
        ]);
        watchdog('status', $title);
        return $resource;
      }
    }
  }
  return FALSE;
}
