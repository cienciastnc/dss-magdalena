jQuery(document).ready(function($) {
    var geoJson;
    // Full GeoJson object
    var geoJsonProjects = Object();
    // Array containing each feature object
    var featuresArray = [];
    var wrongArcId = [];
    var geoJsonPointUrl;
    var geoJsonPointResponse;
    var map;
    var baseUrl = window.location.protocol + "//" + window.location.hostname;
    var dkanUrl = "/api/3/action/package_show?id=";
    var fullUrlDataset = baseUrl + dkanUrl + datasetUuid;
    console.log(fullUrlDataset);
    var datasetRequest = $.ajax({
        url: fullUrlDataset,
        type: 'GET',
    });
    listCaseStudies(baseUrl);
    datasetRequest.then(function(response) {
        console.log(response);
        var geoJsonUrl = response.result[0].resources[0].url;
        console.log("URL GeoJSON::" + geoJsonUrl);
        var geoJsonRequest = $.ajax({
            url: geoJsonUrl,
            type: 'GET',
        });
        geoJsonRequest.then(function(response) {
            console.log("Respuesta GeoJSON::" + response);
            geoJson = JSON.parse(response);
            callMap(geoJson);
        });

    });

    var geJsonPointUrl = "/api/3/action/package_show?id=75351835-434e-4b39-b7f9-e752ee32ba5e";
    var fullUrlDataset = baseUrl + geJsonPointUrl;
    var datasetPointRequest = $.ajax({
        url: fullUrlDataset,
        type: 'GET',
    });
    datasetPointRequest.then(function(response) {
        console.log(response)
        geoJsonPointUrl = response.result[0].resources[0].url;
        var geoJsonPointUrlRequest = $.ajax({
            url: geoJsonPointUrl,
            type: 'GET'
        });
        geoJsonPointUrlRequest.then(function(response) {
            geoJsonPointResponse = response;
        });
    });



    function listCaseStudies(baseUrl) {
        apiUrlBase = "/api/fishermanApp/entity_node.json?";
        apiParameters = "parameters[type]=case_study&fields=title,nid&page=0&pagesize=1000";
        fullUrlApi = baseUrl + apiUrlBase + apiParameters;
        var caseStudyRequest = $.ajax({
            url: fullUrlApi,
            type: 'GET',
        });

        caseStudyRequest.then(function(response) {
            console.log(response);
            var select = document.getElementById("selectMapa");
            for (index in response) {
                select.options[select.options.length] = new Option(response[index].title, response[index].nid);
            }
        });
    }

    function verMas() {
        var caseNid = $("#selectMapa").val();
        if (caseNid != 0) {
            window.location.href = 'sima-geovisor/index.html?study-case=' + caseNid;
        } else {
            alert("Seleccione un caso de estudio");
        }
    }

    function searchCaseStudy(caseNid) {
        $('#cover-spin').show(0);
        featuresArray = [];
        apiUrlBase = "/api/fishermanApp/entity_node.json?";
        datasetRequest
        apiParameters = "parameters[type]=case_study&parameters[nid]=" + caseNid + "&fields=field_integral_plan";
        fullUrlApi = baseUrl + apiUrlBase + apiParameters;
        var caseStudyRequest = $.ajax({
            url: fullUrlApi,
            type: 'GET'
        });

        caseStudyRequest.then(function(response) {
            console.log(response);
            planNid = response[0].field_integral_plan.und[0].target_id;
            apiParameters = "parameters[type]=integral_plan&parameters[nid]=" + planNid + "&fields=field_hydropower_alternatives";
            fullUrlApi = baseUrl + apiUrlBase + apiParameters;
            var integralPlanRequest = $.ajax({
                url: fullUrlApi,
                type: 'GET'
            });
            integralPlanRequest.then(function(response) {
                console.log(response);
                hydropowerAlternatives = response[0].field_hydropower_alternatives.und[0].target_id;
                apiParameters = "parameters[type]=hydropower_alternatives&parameters[nid]=" + hydropowerAlternatives + "&fields=field_hydropower_projects";
                fullUrlApi = baseUrl + apiUrlBase + apiParameters;
                var hydropowerAlternativesRequest = $.ajax({
                    url: fullUrlApi,
                    type: 'GET'
                });
                hydropowerAlternativesRequest.then(function(response) {
                    console.log(response);
                    hydropowerProjects = "";

                    hydropowerProjects = response[0].field_hydropower_projects.und;

                    var arrayProjects = [];
                    try {
                        hydropowerProjects.forEach(function(project) {
                            apiParameters = "parameters[type]=hydropower&parameters[nid]=" + project.target_id + "&fields=field_topology_arcid,title";
                            fullUrlApi = baseUrl + apiUrlBase + apiParameters;
                            var hydropowerProjectsRequest = $.ajax({
                                url: fullUrlApi,
                                type: 'GET'
                            });
                            arrayProjects.push(hydropowerProjectsRequest);
                        });
                    } catch (error) {
                        alert("Este caso no tiene proyectos hidroeléctricos");
                    }
                    processProjects(arrayProjects);
                });
            });

        });
    }
    async function processProjects(array) {
        for (const item of array) {
            await delayedLog(item);
        }
        // Type properties set
        geoJsonProjects.type = geoJsonPointResponse.type;
        var propertiesName = Object();

        //Name key inside of properties
        propertiesName.name = geoJsonPointResponse.crs.properties.name;

        var properties = Object();

        // Properties key inside of crs 
        properties.properties = propertiesName;

        console.log(properties);
        //Crs properties set
        geoJsonProjects.crs = properties;
        geoJsonProjects.crs.type = geoJsonPointResponse.crs.type;
        console.log('Done!');
        console.log(wrongArcId);
        console.log(featuresArray);
        var features = Object();
        features = featuresArray;
        geoJsonProjects.features = features;
        callMapCaseStudies(geoJson, geoJsonProjects);
    }

    async function delayedLog(item) {
        // notice that we can await a function
        // that returns a promise
        await item.then(function(response) {
            console.log(response);
            projectArcId = response[0].field_topology_arcid.und[0].value;
            projectArcId = projectArcId.split(".");
            projectArcId = projectArcId[0];
            console.log(projectArcId);
            geoJsonFeatures = geoJsonPointResponse.features;
            var filterResource = geoJsonFeatures.filter(geo => geo.properties.GeoBranchI == projectArcId);
            console.log(filterResource);

            if (filterResource.length == 0) {
                var coordinatesPoint = ["-75.48495605583057", "5.6700537052093"];
                wrongArcId.push(projectArcId);
            } else {
                var coordinatesPoint = filterResource[0].geometry.coordinates;
            }

            // Object than contains coordinates X,Y
            var coordinates = Object();

            // Array with the coordinates
            var coordinatesArray = [coordinatesPoint[0], coordinatesPoint[1]];

            // Asign coordinates to the object
            coordinates.coordinates = coordinatesArray;

            coordinates.type = geoJsonPointResponse.features[0].geometry.type;

            // Global objecto for each feature in the GeoJSON
            var featureObject = Object();

            // Geometry property of features
            featureObject.geometry = coordinates;

            // Properties object for properties in features
            var properties = Object();
            properties.GeoBranchI = projectArcId;
            properties.Project = response[0].title;

            // Asign  type to features
            featureObject.type = geoJsonPointResponse.features[0].type;

            // Asign  type to features
            featureObject.properties = properties;

            // Add feature objecto to array
            featuresArray.push(featureObject);

        })
    }


    var click = document.getElementById("searchCaseStudy");
    click.addEventListener("click", function() {
        var caseNid = $("#selectMapa").val();
        console.log(caseNid);
        if (caseNid != "0") {
            searchCaseStudy(caseNid)
        } else {
            alert("Seleccione un caso de estudio");
        }
    });

    var click = document.getElementById("vermasMapa");
    click.addEventListener("click", function() { verMas() });

    function callMapCaseStudies(geoJson, geoJsonProjects) {
        map.eachLayer(function(layer) {
            map.removeLayer(layer);
        });

        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', { attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>' }).addTo(map);

        map.doubleClickZoom.disable();

        L.geoJson(geoJson, {
            onEachFeature: function(feature, layer) {

            }
        }).addTo(map);
        $('#cover-spin').hide();
        L.geoJson(geoJsonProjects, {
            onEachFeature: function(feature, layer) {
                layer.bindPopup('<h4>Proyecto:</h4><p>Nombre: ' + feature.properties.Project + '</p>')
            },
            pointToLayer: function(feature, latlng) {
                return L.circleMarker(latlng, {
                    radius: 6,
                    fillColor: "#FF7534",
                    color: "#2F2D2D",
                    weight: 1,
                    opacity: 1,
                    fillOpacity: 1
                });
            }
        }).addTo(map);
    }

    function callMap(geoJson) {

        var mapDiv = document.createElement("div");
        console.log("Geo" + geoJson);
        mapDiv.id = "map";
        var mainContainer = document.getElementById("mapa");
        mainContainer.append(mapDiv);

        // Web map code goes here
        map = L.map('map', { center: [5.547631, -74.945643], zoom: 6, scrollWheelZoom: false });
        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', { attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>' }).addTo(map);

        map.doubleClickZoom.disable();


        L.geoJson(geoJson, {
            onEachFeature: function(feature, layer) {

            }
        }).addTo(map);
    }

});