/**
 * @file
 * Adds UI capabilities to the Productive Land Use Page.
 */

(function($){

  Drupal.behaviors.dss_engine = {
    attach: function (context, settings) {

      $('#edit-field-agricultural-area-und-0-value', context).once('update_area', function () {
        reCalculateAreas();
      });

      $('#field-landuse-values .field-name-field-irr-land-use .form-select option:not(:selected)', context).once('disable_land_type', function() {
        $(this).prop('disabled', true);
      });

      // Setting up default values.
      $('.field-name-field-perc-landuse .form-text', context).once('landuse_default_values', function() {
        $(this).attr('default-value', $(this).val());
      });
      $('.field-name-field-irr-perc-area .form-text', context).once('irrigated_default_values', function() {
        $(this).attr('default-value', $(this).val());
      });

      // Recalculate every time we do a change.
      $('.field-name-field-perc-landuse .form-text', context).change(function() {
        var value = $(this).val();
        if (!$.isNumeric(value) || value < 0 || value > 100) {
          $(this).val($(this).attr('default-value'));
          alert(Drupal.t('Please insert a numeric percentage value between 0.00 and 100.00'));
        }
        // Recalculate Land Use.
        $(this).reCalculateNewColumnValues();

        // Recalculate Distribution Areas.
        reCalculateAreas();

        // Check for total not being higher than 100%.
        total = getTotalArea().toFixed(2);
        var message_error = Drupal.t('Total % Share of Land Use is @total%! Must be 100.00%.', {
          '@total': total
        });
        if (total != '100.00') {
          $('.field-name-field-perc-landuse .form-text').each(function () {
            $(this).css({'color': 'red'});
          });
          if ($('#edit-field-landuse .error-message').text() == '') {
            $('#edit-field-landuse').prepend('<div class = "error-message">' + message_error + '</div>');
          }
          else {
            $('#edit-field-landuse .error-message').text(message_error);
            $('#edit-field-landuse .error-message').fadeIn();
          }
          $('#edit-field-landuse .error-message').css({'color': 'red'})
        }
        else {
          $('.field-name-field-perc-landuse .form-text').each(function () {
            $(this).css({'color': 'green'});
          });
          var message_ok = Drupal.t('Total % Share of Land Use is @total%!', {
            '@total': total
          });
          $('#edit-field-landuse .error-message').text(message_ok);
          $('#edit-field-landuse .error-message').css({'color': 'green'}).fadeOut(2000);
        }
      });

      // Recalculate every time we do a change.
      $('.field-name-field-irr-perc-area .form-text', context).change(function() {
        var value = $(this).val();
        if (!$.isNumeric(value) || value < 0 || value > 100) {
          $(this).val($(this).attr('default-value'));
          alert(Drupal.t('Please insert a numeric percentage value between 0.00 and 100.00'));
        }
        $(this).reCalculateNewColumnValues();
        reCalculateAreas();
      });

      // Fix Areas before submitting.
      $('#productive-land-use-variations-node-form .form-actions .btn-primary', context).click(function() {
        if (!validateTypeChanges()) {
          return false;
        }
        total = getTotalArea().toFixed(2);
        if (total != '100.00') {
          alert(Drupal.t('You can only save this form if the Total % Share Land Use area is 100.00%, Your total is @total%', {'@total': total}));
          return false;
        }
        reCalculateAreas(true);
      });

      /**
       * Recalculate Distribution Areas.
       *
       * @param bool show_just_value
       *   Show just value or value with texts.
       */
      function reCalculateAreas(show_just_value) {
        calculateAgriculturalArea(show_just_value);
        calculateForestIndustrialArea(show_just_value);
        calculateMiningArea(show_just_value);
        calculateIrrigatedArea(show_just_value);
      }

      function calculateAgriculturalArea(show_just_value) {
        show_just_value = show_just_value || false;
        // Agricultural.
        var agricola             = getLandUseValue('Agricola');
        var agricola_permanente  = getLandUseValue('AgricolaPermanente');
        var agricola_transitorio = getLandUseValue('AgricolaTransitorio');
        var percentage_total = agricola + agricola_permanente + agricola_transitorio;
        var catchment_area = percentage_total * getCatchmentArea() / 100;
        if (show_just_value) {
          var text = catchment_area.toFixed(2);
        }
        else {
          var text = catchment_area.toFixed(2) + '  (' + percentage_total.toFixed(2) + '%)';
        }

        $('#edit-field-agricultural-area-und-0-value').val(text);
      }

      function calculateForestIndustrialArea(show_just_value) {
        show_just_value = show_just_value || false;
        // Forestal/Industrial.
        var bosque_plantado     = getLandUseValue('BosquePlantado');
        var percentage_total = bosque_plantado;
        var catchment_area = percentage_total * getCatchmentArea() / 100;
        if (show_just_value) {
          var text = catchment_area.toFixed(2);
        }
        else {
          var text = catchment_area.toFixed(2) + '  (' + percentage_total.toFixed(2) + '%)';
        }

        $('#edit-field-forest-industrial-area-und-0-value').val(text);
      }

      function calculateMiningArea(show_just_value) {
        show_just_value = show_just_value || false;
        // Minero.
        var minero             = getLandUseValue('Minero');
        var percentage_total = minero;
        var catchment_area = percentage_total * getCatchmentArea() / 100;
        if (show_just_value) {
          var text = catchment_area.toFixed(2);
        }
        else {
          var text = catchment_area.toFixed(2) + '  (' + percentage_total.toFixed(2) + '%)';
        }

        $('#edit-field-mining-area-und-0-value').val(text);
      }

      function calculateIrrigatedArea(show_just_value) {
        show_just_value = show_just_value || false;
        // Irrigated Area.
        var base_perc_irrigated_area = Drupal.settings.dss_engine.percentage_irrigated_area_base;
        var base_irrigated_area = base_perc_irrigated_area * getCatchmentArea() / 100;

        // For all agricultural areas.
        var agricola             = getNewIrrigatedAreaValue('Agricola');
        var agricola_permanente  = getNewIrrigatedAreaValue('AgricolaPermanente');
        var agricola_transitorio = getNewIrrigatedAreaValue('AgricolaTransitorio');

        var new_irrigated_area = base_irrigated_area + agricola + agricola_permanente + agricola_transitorio;
        var new_percentage_irrigated_area = (new_irrigated_area / getCatchmentArea()) * 100 || 0;

        if (show_just_value) {
          var text = new_percentage_irrigated_area.toFixed(2);
        }
        else {
          var text = new_irrigated_area.toFixed(2) + '  (' + new_percentage_irrigated_area.toFixed(2) + '%)';
        }
        $('#edit-field-irr-perc-area-und-0-value').val(text);
      }

      function validateTypeChanges() {
        var type = $('#edit-field-type-of-project-und').val();
        console.log(type);
        switch (type) {
          case 'mining':
            var value = parseFloat(getLandUseObject('Minero').closest('tr').find('.field-prod-land-use-incremented-area').text());
            if (value <= 0) {
              alert(Drupal.t('You have to increase the land use area for land type = @type: @value%', {
                '@type' : 'Minero',
                '@value': value,
              }));
              return false;
            }
            break;

          case 'forestall':
            var value = parseFloat(getLandUseObject('BosquePlantado').closest('tr').find('.field-prod-land-use-incremented-area').text());
            if (value <= 0) {
              alert(Drupal.t('You have to increase the land use area for land type = @type: @value%', {
                '@type' : 'BosquePlantado',
                '@value': value,
              }));
              return false;
            }
            break;

          case 'agricultural':
            var value = parseFloat(getLandUseObject('Agricola').closest('tr').find('.field-prod-land-use-incremented-area').text());
            if (value <= 0) {
              alert(Drupal.t('You have to increase the land use area for land type = @type: @value%', {
                '@type' : 'Agricola',
                '@value': value,
              }));
              return false;
            }
            break;

          case 'restoration':
            var value = parseFloat(getLandUseObject('Bosque').closest('tr').find('.field-prod-land-use-incremented-area').text());
            if (value < 0) {
              alert(Drupal.t('In restoration projects, the land use cannot decrease for land type = @type: @value%', {
                '@type' : 'Bosque',
                '@value': value,
              }));
              return false;
            }
            break;

        }
        return true;
      }

      function getTotalArea() {
        var total = 0;
        var agricola             = getLandUseValue('Agricola');
        var agricola_permanente  = getLandUseValue('AgricolaPermanente');
        var agricola_transitorio = getLandUseValue('AgricolaTransitorio');
        var arbusto = getLandUseValue('Arbusto');
        var bosque = getLandUseValue('Bosque');
        var bosque_plantado     = getLandUseValue('BosquePlantado');
        var cuerpo_agua = getLandUseValue('Cuerpo Agua');
        var cuerpoagua = getLandUseValue('CuerpoAgua');
        var herbazal = getLandUseValue('Herbazal');
        var hidrofita = getLandUseValue('Hidrofita');
        var minero             = getLandUseValue('Minero');
        var otros = getLandUseValue('Otros');
        var pastos = getLandUseValue('Pastos');
        var urbano = getLandUseValue('Urbano');
        var vegetacion_secundaria = getLandUseValue('Vegetacion Secundaria');
        var vegetacionsecundaria = getLandUseValue('VegetacionSecundaria');
        total = agricola + agricola_permanente + agricola_transitorio +
        arbusto + bosque + bosque_plantado + cuerpo_agua + cuerpoagua + herbazal +
        hidrofita + minero + otros + pastos + urbano + vegetacion_secundaria +
        vegetacionsecundaria;
        return total;
      }

      function getLandUseValue(type) {
        object = getLandUseObject(type);
        if (object !== undefined) {
          return parseFloat(object.getLandUsePercentage().val());
        }
        return 0;
      }

      function getNewIrrigatedAreaValue(type) {
        object = getLandUseObject(type);
        if (object !== undefined) {
          return object.getNewIrrigatedArea();
        }
        return 0;
      }

      /**
       * Obtains the row for a specific land use type.
       *
       * @param type
       *
       * @returns {*}
       */
      function getLandUseObject(type) {
        var object;
        $('.field-name-field-irr-land-use').find('select').children(':selected').each(function() {
          if ($(this).text() == type) {
            object = $(this).closest('tr');
          }
        });
        return object;
      }

      /**
       * Obtains the Catchment Area.
       *
       * @returns {Number}
       */
      function getCatchmentArea() {
        return parseFloat($('#edit-field-catchment-area-und-0-value').val());
      }

    }
  };

  /**
   * Obtains the % Share of Land Use for a specific row.
   *
   * @returns {*}
   */
  $.fn.getLandUsePercentage = function() {
    return $(this).find('.field-name-field-perc-landuse .form-text');
  }

  /**
   * Obtains the % Irrigated Area for a specific row.
   *
   * @returns {*}
   */
  $.fn.getIrrigatedAreaPercentage = function() {
    return parseFloat($(this).find('.field-name-field-irr-perc-area .form-text').val()) || 0;
  }

  /**
   * Obtains the New Irrigated Area for a specific row.
   *
   * @returns {*}
   */
  $.fn.getNewIrrigatedArea = function() {
    return parseFloat($(this).find('.field-prod-land-use-new-irrigated-area').text()) || 0;
  }

  /**
   * Recalculates the Actual Land Use Area for a particular row.
   */
  $.fn.reCalculateNewColumnValues = function() {
    var landuse = parseFloat($(this).closest('tr').find('.field-name-field-perc-landuse .form-text').val()) || 0;
    var perc_irrigated_area = parseFloat($(this).closest('tr').find('.field-name-field-irr-perc-area .form-text').val()) || 0;
    var old_landuse_area = parseFloat($(this).closest('tr').find('.field-prod-land-use-base-area').text()) || 0;
    var catchment_area = parseFloat($('#edit-field-catchment-area-und-0-value').val()) || 0;
    var landuse_area = landuse * catchment_area / 100 || 0;
    var incremented_area = landuse_area - old_landuse_area || 0;
    var new_irrigated_area = incremented_area * perc_irrigated_area / 100 || 0;
    $(this).closest('tr').find('.field-calculated-area').html(landuse_area.toFixed(2));
    $(this).closest('tr').find('.field-prod-land-use-incremented-area').html(incremented_area.toFixed(2));
    $(this).closest('tr').find('.field-prod-land-use-new-irrigated-area').html(new_irrigated_area.toFixed(2));
  }

})(jQuery);
