jQuery(document).ready(function ($) {

  /*
    Variable toponet defined in profiles/dss_magdalena/modules/dss_magdalena/dss_engine/js/toponet.js
  */
  const baseUrl = window.location.protocol + "//" + window.location.hostname;
  print_CaseInfo(id_Nodo);
  //console.log(outStatus.fragmentation.status)
  var colorwayGlob=['#A9CCE3', '#7fb3d5', '#5499c7', '#2980b9', '#2471a3', '#1f618d', '#1a5276','#154360','#ABEBC6', '#82E0AA', '#58D68D', '#2ECC71', '#288463', '#239B56', '#1D8348','#186A3B'];
  if (outStatus.fragmentation.status == 102) {
   
    graphFragmentation2("Frag_graph2");
  } else {
    $("#Frag_graph2").append("<H4 class='rtecenter'>El modelo fragmentación aún no se ha ejecutado</H4>")
    //console.log("frag no se ha ejecutado");    
  };
  if (outStatus.territorial_footprint.status == 102) {
    graphHuella('hue_graph');
  } else {
    $("#hue_graph").append("<H4 class='rtecenter'>El modelo Huella aún no se ha ejecutado</H4>")
   // console.log("huella no se ha ejecutado");
  }

  if (outStatus.sediments.status == 102) {
    graphSediments('sed_grap');
  } else {
    $("#sed_grap").append("<H4 class='rtecenter'>El modelo sedimentos aún no se ha ejecutado</H4>")

   // console.log("Sedimentos no se ha ejecutado");
  }

  if (outStatus.dor_h.status == 102) {
    graphDorH2('Dorh_graph');
  } else {
    $("#Dorh_graph").append("<H4 class='rtecenter'>El modelo DOR-H aún no se ha ejecutado</H4>")
  //  console.log("dor no se ha ejecutado");
    
  }





  function getNodeInfo(node_id) {

    var node_info_end_point = baseUrl + "/api/fishermanApp/node/" + node_id + ".json";

    var valueAltInfo = $.ajax({
      type: "get",
      url: node_info_end_point,
      dataType: "json",
      async: false,
    });
    return valueAltInfo.responseJSON;

  } 



  function print_Alt(node_id) {
    var node_info_end_point = baseUrl + "/api/fishermanApp/node/" + node_id + ".json";
    var alt_Hidro;
    var alt_M_H;
    var alt_U_P_S;
    var valueAltInfo = $.ajax({
      type: "get",
      url: node_info_end_point,
      dataType: "json",
    });
    valueAltInfo.then(function (response) {
      $("#div_alt").append('<h3 class=""><a href= "  ' + response.path + '" >' + response.title + ':</a></h3>');

      if (response.body.length !== 0) {
        $("#div_alt").append(response.body.und[0].safe_value);
      } else {
        $("#div_alt").append("");
      }

      $("#div_alt").append('<div class="row sidebar-box" id="AltInfo"></div>');

      $("#AltInfo").append('<div class="card col-xs-4"><div class="thumbnail"><h3>Hidroenergía  </h3><div class="alt1">  <div class="fa_i"><i class="fas fa-broadcast-tower"></i></div>  </div></div></div>');

      $("#AltInfo").append('<div class="card col-xs-4"><div class="thumbnail"><h3>Manejo de humedales  </h3><div class="alt2"><div class="fa_i"><i class="fas fa-water"></i></div>  </div></div></div>');

      $("#AltInfo").append('<div class="card col-xs-4"><div class="thumbnail"><h3>Uso productivo del suelo </h3><div class="alt3"><div class="fa_i"><i class="fas fa-globe-americas"></i> </div>  </div></div></div>');

      $("#ver_mas1").click(function () {
        if ($("#ver_mas1").text() == "Ver más") {
          $(".alt1 p").addClass("visible");
          $("#ver_mas1").text("Ver menos");
        } else {
          $(".alt1 p").removeClass("visible");
          $("#ver_mas1").text("Ver más");
        }
      });

      $("#ver_mas2").click(function () {
        if ($("#ver_mas2").text() == "Ver más") {
          $(".alt2 p").addClass("visible");
          $("#ver_mas2").text("Ver menos");
        } else {
          $(".alt2 p").removeClass("visible");
          $("#ver_mas2").text("Ver más");
        }
      });
      $("#ver_mas3").click(function () {
        if ($("#ver_mas3").text() == "Ver más") {
          $(".alt3 p").addClass("visible");
          $("#ver_mas3").text("Ver menos");
        } else {
          $(".alt3 p").removeClass("visible");
          $("#ver_mas3").text("Ver más");
        }
      });


      if (response.field_hydropower_alternatives.length != 0) {
        alt_Hidro = getNodeInfo(response.field_hydropower_alternatives.und[0].target_id);

        $("#AltInfo .alt1").append('<h4 class=""><a href= "  ' + alt_Hidro.path + '" >' + alt_Hidro.title + ':</a></h4>');
        // if( alt_Hidro.body.length !== 0){
        //   $("#AltInfo .alt1").append(alt_Hidro.body.und[0].safe_value );
        // }else{
        //   $("#AltInfo .alt1").append("<p>No hay descripción para la altenrativa de hidroenegía</p>");
        // }

      } else {
        //console.log("No hay alternativas de plan integral")
      }
      if (response.field_connectivity_alternatives.length != 0) {
        alt_M_H = getNodeInfo(response.field_connectivity_alternatives.und[0].target_id);
        //console.log("12345: ",alt_M_H)


        $("#AltInfo .alt2").append('<h4 class="rteleft"><a href= "  ' + alt_M_H.path + '" >' + alt_M_H.title + ':</a></h4>');


        //   if( alt_M_H.body.length !== 0){
        //     $("#AltInfo .alt2").append(alt_M_H.body.und[0].safe_value );
        //   }else{
        //     $("#AltInfo .alt2").append("<p>No hay descripción para la altenrativa de manejo de humedales</p>");
        // }


      } else {
       // console.log("No hay alternativas de plan integral")
      }
      if (response.field_prod_landuse_alternatives.length != 0) {

        alt_U_P_S = getNodeInfo(response.field_prod_landuse_alternatives.und[0].target_id);

        $("#AltInfo .alt3").append('<h4 class=""><a href= "  ' + alt_U_P_S.path + '" >' + alt_U_P_S.title + ':</a></h4>');
        // if( alt_U_P_S.body.length !== 0){
        //   $("#AltInfo .alt3").append(alt_U_P_S.body.und[0].safe_value );
        // }else{
        //   $("#AltInfo .alt3").append("<p>No hay descripción para la altenrativa de uso productivo del suelo</p>");
        // }

      } else {
        //console.log("No hay alternativas de plan integral")
      }
    });

  }
  function print_CaseInfo(node_id) {
    var node_info_end_point = baseUrl + "/api/fishermanApp/node/" + node_id + ".json";

    var valueCaseInfo = $.ajax({
      type: "get",
      url: node_info_end_point,
      dataType: "json",
    });
    valueCaseInfo.then(function (response) {
      
      $("#titulo_caso").append("<h1>" + response.title + "</h1>" + "<a  class='text-info edit-link' href='/node/" + node_id + "/edit'>" + "  [Editar]</a>"+"<a  class='text-info edit-link' href='/node/" + node_id + "/clone'>" + "  [Clonar]</a>");
      
      if (response.body.length !== 0) {
        $("#desc_caso").append(response.body.und[0].value);
      } else {
        $("#desc_caso").append("");
      }

      if (response.field_integral_plan.length != 0) {

        print_Alt(response.field_integral_plan.und[0].target_id)

      } else {
        $("#div_alt").append("<p>No existe una alternativa de plan integral para este caso de estudio<p>");
      }
      if (response.field_scenario.length != 0) {
        print_Esc(response.field_scenario.und[0].target_id)
      } else {
        $("#div_esc").append("<p>No existe un escenario para este caso de estudio<p>");
      }
    });
  }

  function parseFile2(csv) {
    //Use this to process each row
    var data = d3.csvParse(csv);
    return data
  }

  function print_Esc(node_id) {
    var node_info_end_point = baseUrl + "/api/fishermanApp/node/" + node_id + ".json";
    var escClima;
    var escEner;
    var estPob;
    var valueEscInfo = $.ajax({
      type: "get",
      url: node_info_end_point,
      dataType: "json",
    });
    valueEscInfo.then(function (response) {

      $("#div_esc").append('<h3 class=""><a href= "  ' + response.path + '" >' + response.title + ':</a></h3>')

      if (response.body.length !== 0) {
        $("#div_esc").append(response.body.und[0].safe_value);
      } else {
        $("#div_esc").append("");
      }


      $("#div_esc").append('<div class="row sidebar-box" id="escInfo"></div>');

      $("#escInfo").append('<div class="card col-xs-4"><div class="thumbnail"><h3>Climático  </h3><div class="esc1"><div class="fa_i"><i class="fas fa-cloud-sun"></i> </div></div> </div></div>');
      // <div id="ver_mas_e_1" class="btn btn-sima showbtn" tabindex="0" href="#">Ver más</div> </div>');

      $("#escInfo").append('<div class="card col-xs-4"><div class="thumbnail"><h3>Energético  </h3><div class="esc2"><div class="fa_i"><i class="fas fa-charging-station"></i> </div>  </div> </div></div>');
      // <div id="ver_mas_e_2" class="btn btn-sima showbtn" tabindex="0" href="#">Ver más</div> </div>');

      $("#escInfo").append('<div class="card col-xs-4"><div class="thumbnail"><h3>Poblacional  </h3><div class="esc3"> <div class="fa_i"><i class="fas fa-users"></i></div> </div> </div></div>');
      // <div id="ver_mas_e_3" class="btn btn-sima showbtn" tabindex="0" href="#">Ver más</div> </div>');

      if (response.field_climate_scenario.length != 0) {
        escClima = getNodeInfo(response.field_climate_scenario.und[0].target_id);
        $("#escInfo .esc1").append('<h4 class=""><a href= "  ' + escClima.path + '" >' + escClima.title + ':</a></h4>');
        /*
                if( escClima.body.length !== 0){
                  $("#escInfo .esc1").append(escClima.body.und[0].safe_value );
                }else{
                  $("#escInfo .esc1").append("<p>No existe una descripción para el escenario climatico</p>");
                }*/
      } else {
        //console.log("No hay escenario climatico")
      }
      if (response.field_energy_scenario.length != 0) {
        escEner = getNodeInfo(response.field_energy_scenario.und[0].target_id);

        $("#escInfo .esc2").append('<h4 class=""><a href= "  ' + escEner.path + '" >' + escEner.title + ':</a></h4>');

      } else {
       // console.log("No hay escenario energetico")
      }


      if (response.field_population_scenario.length != 0) {
        estPob = getNodeInfo(response.field_population_scenario.und[0].target_id);
        $("#escInfo .esc3").append('<h4 class=""><a href= "  ' + estPob.path + '" >' + estPob.title + ':</a></h4>');
      } else {
       // console.log("No hay Escenario de poblacion ")
      }
    });
  }



  function graphFragmentation2(div_graph) {
    let fullUrlLongTotSubRed = baseUrl + "/sites/default/files/case_resources/" + id_Nodo + "/dss_longitud_cada_subred.csv";
    let fragmentado=0;
    let conectado=0;


    var ltotal_sub_red_Request = $.ajax({
      type: "get",
      url: fullUrlLongTotSubRed,
      dataType: "text",
    });
    ltotal_sub_red_Request.then(function (response) {
      respuesta = parseFile2(response);
      let sum = [0,0,0,0,0,0]
      respuesta.forEach(function (element, index) {
        data = JSON.parse(element.Value);
        if (element.BranchID != 0) {
          fragmentado += data[5];
          
        }else{
            conectado += data[5];
        }

      });

      xAxis= [fragmentado.toFixed(2), conectado.toFixed(2)];
      data = JSON.parse(respuesta[0].Value);
      //xAxis=[data[0],data[1],data[2],data[3],data[4],data[5]];
      yAxis = ['red fragmentada', 'red conectada'];
      colores=['#154360','#A9CCE3']
      graphPie(xAxis, yAxis, div_graph, 'Gráfico de Fragmentación', colorW=colores);
    });
  }


  function graphFragmentation(div_graph) {
    let fullUrlLongTotSubRed = baseUrl + "/sites/default/files/case_resources/" + id_Nodo + "/dss_longitud_cada_subred.csv";
    var ltotal_sub_red_Request = $.ajax({
      type: "get",
      url: fullUrlLongTotSubRed,
      dataType: "text",
    });
    ltotal_sub_red_Request.then(function (response) {
      respuesta = parseFile2(response);
      let sum = [0,0,0,0,0,0]
      respuesta.forEach(function (element, index) {
        data = JSON.parse(element.Value);
        //if (element.BranchID != 0) {
          for (let i = 0; i < 6; i++) {
            sum[i] += data[i];
          }
        //}
      });
      console.log(sum)
      xAxis= sum.map(a => a.toFixed(2));
      data = JSON.parse(respuesta[0].Value);
      //xAxis=[data[0],data[1],data[2],data[3],data[4],data[5]];
      yAxis = ['horton 4', 'horton 5', 'horton 6', 'horton 7', 'horton 8', 'Horton 4 a 8'];
      graphPie(xAxis, yAxis, div_graph, 'Gráfico de Fragmentación');
    });
  }
  function graphDorH(div_graph) {
    let fullURLLongMaxSubRedes = baseUrl + "/sites/default/files/case_resources/" + id_Nodo + "/dss_longitud_max_subredes_dor_h.csv";
    var ltotal_sub_red_Request = $.ajax({
      type: "get",
      url: fullURLLongMaxSubRedes,
      dataType: "text",
    });
    ltotal_sub_red_Request.then(function (response) {
      let respuesta = parseFile2(response);

      let suma = [0, 0, 0, 0, 0, 0, 0, 0];
      respuesta.forEach(function (element, index) {
        data = JSON.parse(element.Value);

        if (element.BranchID != 0) {
          for (let i = 0; i < 8; i++) {
            suma[i] += data[i];
          }
        }
      });

      xAxis=suma.map(a => a.toFixed(2));
      //xAxis = suma;

      yAxis = ['dorH 0-20', 'dorH 20-40', 'dorH 40-60', 'dorH 60-80', 'dorH 80-100', 'dorH 100-150', 'dorH 150-200', 'dorH > 200'];
      graphPie(xAxis, yAxis, div_graph, 'Gráfico de DOR-H ', 255);

    });
  }


  function getLengthByARCID(id) {
    return toponet.filter(function(data){ return data.ARCID == id }
    );
  }
  


  function graphDorH2(div_graph) {
    let fullURLLongMaxSubRedes = baseUrl + "/sites/default/files/case_resources/" + id_Nodo + "/dss_dor_h.csv";
    var ltotal_sub_red_Request = $.ajax({
      type: "get",
      url: fullURLLongMaxSubRedes,
      dataType: "text",
    });
    ltotal_sub_red_Request.then(function (response) {
      let respuesta = parseFile2(response);    
      let suma = [0, 0, 0, 0, 0, 0, 0];
      yAxis = ['entre (0%-2%]', 'entre (2%-5%]', 'entre (5%-10%]', 'entre (10%-15%]', 'entre (15%-25%]', 'entre (25%-50%]', 'entre (50%,+)'];
      //console.log(respuesta)
      respuesta.forEach(function (element, index) {
        l=parseFloat((""+ getLengthByARCID(element.GeoBranchId)[0].length_km).replace(",", "."));;
        val=parseFloat((""+element.Value).replace(",", "."));
        if(val<=2){
          suma[0]+=l
        }else if(val<=5){
          suma[1]+=l
        }else if(val<=10){
          suma[2]+=l
        }
        else if(val<=15){
          suma[3]=l
        }else if(val<=25){
          suma[4]+=l
        }
        else if(val<=50){
          suma[5]+=l
        }
        else if(val>50){
          suma[6]=l
        }
        else{
        }
      });
      colores=['#1A5276','#D0ECE7','#A9CCE3','#73C6B6','#5499C7','#2471A3','#73C6B6']
      
      xAxis=suma.map(a => a.toFixed(2));
      console.log(xAxis) 
      graphPie(xAxis, yAxis, div_graph, 'Gráfico de DOR-H ', 255,colorW=colores);
    });
  }



  function graphSediments(div_graph) {


    let fullUrlQSolid = baseUrl + "/sites/default/files/case_resources/" + id_Nodo + "/dss_red_impactada_por_q_solido.csv";
    var redQsolid = $.ajax({
      type: "get",
      url: fullUrlQSolid,
      dataType: "text",
    });
    redQsolid.then(function (response) {

      let respuesta = parseFile2(response);
      //console.log(respuesta)
      let suma = [0, 0, 0, 0, 0];
      respuesta.forEach(function (element, index) {
        data = JSON.parse(element.Value);



        for (let i = 0; i < 5; i++) {
          suma[i] += data[i];
        }

      });
      yAxis = suma;

      xAxis = ['20', '40', '60', '80', '100'];
      var layout = {
        title: 'Gráfico de Sedimentos ',
        colorway:['#1B4F72','#2874A6','#3498DB','#85C1E9','#D6EAF8'],
        yaxis :{
          title:'km de rio'
        },

        xaxis :{
          ticksuffix:" %",
          type:"category",
          title:'Alteración de sedimentos'
        },
        showlegend: false

      };
      graphBar(xAxis, yAxis, layout, div_graph, 'v');

    });
  }
  function graphHuella(div_graph) {


    let fullUrlZon = baseUrl + "/sites/default/files/case_resources/" + id_Nodo + "/dss_huella_zonas_de_interes.csv";
    var zona_interes_Request = $.ajax({
      type: "get",
      url: fullUrlZon,
      dataType: "text",
    });
    zona_interes_Request.then(function (response) {
      let respuesta = parseFile2(response);
      //console.log(respuesta)
      let suma = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];


      respuesta.forEach(function (element, index) {
        data = JSON.parse(element.Value);

        if (element.BranchID != 0) {
          //console.log(data)
          for (let i = 1; i < 17; i++) {
            suma[i - 1] += data[i];
          }
        }
      });

      yAxis = suma;

      xAxis = [
        "Cobertura agrícola de cultivo"
        , "Cobertura agrícola de pastos"
        , "Area de compensación"
        , "Microfocalización"
        , "Paramos siac"
        , "Parques naturales regionales"
        , "Titulos mineros"
        , "Bosque seco"
        , "Comunidades negras"
        , "Humedales siac"
        , "Parques nacionales"
        , "Población hab."
        , "Reserva forestal protectora nacional"
        , "Reserva forestal protectora regional"
        , "Reservas ley2da"
        , "Resguardos indigenas"
      ];
      var layout = {
        title: 'Gráfico de Huella territorial ',
        xaxis: {
          automargin: true,
          showticklabels:false,
          ticksuffix:" m²",
          title:'Capa de cobertura',
        },
        yaxis:{
          title: 'Área afectada (m²)'
        },
        margin: {
          l: 200,
          r: 70,
          b:80
        },
        legend:{
          borderwidth:0.7,
          bgcolor:'#f3f3f3',
          ticksuffix:" m²",
        },
        colorway : colorwayGlob,
      };
      graphBar(xAxis, yAxis, layout, div_graph, 'v');

    });
  }


  function graphPie(xAxis, yAxis, div_graph, modelTitle, rotat = 0, colorW=colorwayGlob) {
    var data = [{
      type: 'pie',
      values: xAxis,
      labels: yAxis,
      rotation: rotat,
    }];


    var lay = {
      title: modelTitle,
      traceorder: 'normal',
      colorway:colorW
    };
    Plotly.newPlot(div_graph, data, lay, { responsive: true , showLink: false, linkText: 'Editar en plot.ly'  });
  }

  function graphBar(xAxis, yAxis, layout, div_graph, orientation = "h") {

    var data = [{
      type: 'bar',
      x: xAxis,
      y: yAxis,
      orientation: orientation,
      transforms: [{type: 'groupby',groups: xAxis}]

    }];



    Plotly.newPlot(div_graph, data, layout, { responsive: true, showLink: false , linkText: 'Editar en plot.ly'});

  }


});


