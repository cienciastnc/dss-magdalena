<?php
/**
 * @file
 * Organizes Variables per Thematic Tree branches.
 */

namespace Drupal\dss_magdalena\DSS;

/**
 * Class that handles relationships between the Thematic Tree and Datasets.
 *
 * Datasets Also referred as "DSS Variables".
 *
 * Class SimaThematicTreeVariables.
 *
 * @package Drupal\dss_magdalena\DSS
 */
class SimaThematicTreeVariables {

  const TABLE     = 'dss_engine_thematic_tree_dataset';
  const TARGET_ID = 'target_id';
  const PO        = 'po';
  const DPSIR     = 'dpsir';
  const SWOT      = 'swot';

  /**
   * Thematic Tree Datasets.
   *
   * @var object $thematicTreeDatasets
   *   Datasets IDs - Variable ID.
   */
  protected $thematicTreeDatasets = array();

  /**
   * Old Thematic Tree Datasets.
   *
   * @var object $thematicTreeDatasetsOld
   *   Old Datasets IDs - Variable ID.
   */
  protected $thematicTreeDatasetsOld = array();

  /**
   * Resets the Thematic Tree/Variable Relationship.
   */
  protected function reset() {
    $this->thematicTreeDatasets = array();
  }

  /**
   * Public Constructor.
   *
   * @param array $thematic_tree_datasets_old
   *   The old thematic tree datasets.
   */
  public function __construct($thematic_tree_datasets_old = array()) {
    $this->thematicTreeDatasetsOld = isset($thematic_tree_datasets_old) ? $thematic_tree_datasets_old : array();
  }

  /**
   * Sets the relation for Thematic Tree/Variable.
   *
   * @param int $thematic_tree_tid
   *   The Branch in the Thematic Tree.
   * @param int $nid
   *   The Dataset (Variable) node ID.
   * @param int $po
   *   The taxonomy term for PO.
   * @param int $dpsir
   *   The taxonomy term for DPSIR.
   * @param int $swot
   *   The taxonomy term for SWOT.
   *
   * @return \Drupal\dss_magdalena\DSS\SimaThematicTreeVariables $this
   *   This object.
   */
  public function setThematicTreeDataset($thematic_tree_tid, $nid, $po, $dpsir, $swot) {
    $thematic_tree_dataset = (object) array(
      'thematic_tree_tid' => $thematic_tree_tid,
      'dataset_id' => $nid,
      self::PO => $po,
      self::DPSIR => $dpsir,
      self::SWOT => $swot,
    );
    return $thematic_tree_dataset;
  }

  /**
   * Sets the Thematic Tree relationship with all its Datasets.
   *
   * @param int $thematic_tree_tid
   *   The Thematic Tree ID.
   * @param array $element
   *   The Element array.
   *
   * @return \Drupal\dss_magdalena\DSS\SimaThematicTreeVariables $this
   *   This object.
   */
  public function setThematicTreeDatasets($thematic_tree_tid, $element) {
    foreach ($element[LANGUAGE_NONE] as $key => $dataset) {
      if (is_numeric($key) && !empty($dataset[self::TARGET_ID])) {
        // Obtain variables.
        $nid = $dataset[self::TARGET_ID];
        $po = isset($dataset[self::PO]) ? $dataset[self::PO] : 0;
        $dpsir = isset($dataset[self::DPSIR]) ? $dataset[self::DPSIR] : 0;
        $swot = isset($dataset[self::SWOT]) ? $dataset[self::SWOT] : 0;

        // Assign object.
        $thematic_tree_dataset = $this->setThematicTreeDataset($thematic_tree_tid, $nid, $po, $dpsir, $swot);
        $this->thematicTreeDatasets[$nid] = $thematic_tree_dataset;
      }
    }
    return $this;
  }

  /**
   * Adds a new Thematic Tree / Dataset Relationship.
   *
   * @param int $thematic_tree_tid
   *   The Thematic Tree.
   * @param int $nid
   *   The Dataset ID.
   * @param int $po
   *   The taxonomy term for PO.
   * @param int $dpsir
   *   The taxonomy term for DPSIR.
   * @param int $swot
   *   The taxonomy term for SWOT.
   *
   * @return \Drupal\dss_magdalena\DSS\SimaThematicTreeVariables $this
   *   This object.
   */
  public function addThematicTreeDataset($thematic_tree_tid, $nid, $po, $dpsir, $swot) {
    // Obtain variables.
    if (isset($nid) && is_numeric($nid) && $nid) {
      $po = isset($po) ? $po : 0;
      $dpsir = isset($dpsir) ? $dpsir : 0;
      $swot = isset($swot) ? $swot : 0;

      // Assign object.
      $thematic_tree_dataset = $this->setThematicTreeDataset($thematic_tree_tid, $nid, $po, $dpsir, $swot);
      $this->thematicTreeDatasets[$nid] = $thematic_tree_dataset;
      return $this;
    }
    else {
      drupal_set_message(t('Cannot add Thematic Tree Relationship. Dataset Variable = %nid.', array(
        '%nid' => $nid,
      )), 'error');
      return $this;
    }
  }

  /**
   * Returns the Thematic Tree/Datasets Relationships.
   *
   * @return object
   *   The relationship object.
   */
  public function getThematicTreeDatasets() {
    return $this->thematicTreeDatasets;
  }

  /**
   * This function returns the list of deleted datasets.
   *
   * We assume that we have data in the old datasets and the new ones.
   */
  protected function getDeletedDatasets() {
    $deleted_datasets = array();
    foreach ($this->thematicTreeDatasetsOld as $old_dataset) {
      $found = FALSE;
      foreach ($this->thematicTreeDatasets as $dataset) {
        if ($dataset->thematic_tree_tid == $old_dataset->thematic_tree_tid && $dataset->dataset_id == $old_dataset->dataset_id) {
          $found = TRUE;
          break;
        }
      }
      if (!$found) {
        $deleted_datasets[] = $old_dataset;
      }
    }
    return $deleted_datasets;
  }

  /**
   * Saves all the relationships in the object.
   *
   * @return bool
   *   TRUE if all relationships are saved, FALSE otherwise.
   */
  public function save() {
    // First delete all the deleted datasets (relationships).
    $deleted_datasets = $this->getDeletedDatasets();
    foreach ($deleted_datasets as $deleted_dataset) {
      $this->delete($deleted_dataset->thematic_tree_tid, $deleted_dataset->dataset_id);
    }

    // Collect all datasets for this relationship.
    $result = TRUE;
    $variable_ids = array();
    foreach ($this->thematicTreeDatasets as $thematic_tree_dataset) {
      $save = $this->saveSingleDataset($thematic_tree_dataset);
      $variable_ids[$thematic_tree_dataset->thematic_tree_tid] = $thematic_tree_dataset->dataset_id;
      $result = $result && $save;
    }
    return $result;
  }

  /**
   * Saves a record of a single dataset relationship.
   *
   * @param object $thematic_tree_dataset
   *   The Dataset to save.
   *
   * @return bool
   *   TRUE if saving is successful, FALSE otherwise.
   */
  protected function saveSingleDataset($thematic_tree_dataset) {
    $success = FALSE;

    // Performing basic validations.
    $valid_input = isset($thematic_tree_dataset->thematic_tree_tid) && is_numeric($thematic_tree_dataset->thematic_tree_tid) && $thematic_tree_dataset->thematic_tree_tid;
    $valid_input = $valid_input && isset($thematic_tree_dataset->dataset_id) && is_numeric($thematic_tree_dataset->dataset_id) && $thematic_tree_dataset->dataset_id;
    if ($valid_input) {
      $result = db_merge(self::TABLE)
        ->key(array(
          'thematic_tree_tid' => $thematic_tree_dataset->thematic_tree_tid,
          'dataset_id' => $thematic_tree_dataset->dataset_id,
        ))
        ->fields(array(
          self::PO => (isset($thematic_tree_dataset->po) && is_numeric($thematic_tree_dataset->po)) ? $thematic_tree_dataset->po : 0,
          self::DPSIR => (isset($thematic_tree_dataset->dpsir) && is_numeric($thematic_tree_dataset->dpsir)) ? $thematic_tree_dataset->dpsir : 0,
          self::SWOT => (isset($thematic_tree_dataset->swot) && is_numeric($thematic_tree_dataset->swot)) ? $thematic_tree_dataset->swot : 0,
        ))
        ->execute();

      switch ($result) {
        case \MergeQuery::STATUS_INSERT:
        case \MergeQuery::STATUS_UPDATE:
          $success = TRUE;
          break;

        default:
          $success = FALSE;
          drupal_set_message(t('Could not save thematic_tree/dataset relatioship for thematic_tree_tid = %tid, nid = %nid (PO: %po, DPSIR: %dpsir, SWOT: %swot).', array(
            'tid' => $thematic_tree_dataset->thematic_tree_tid,
            'nid' => $thematic_tree_dataset->dataset_id,
            self::PO => $thematic_tree_dataset->po,
            self::DPSIR => $thematic_tree_dataset->dpsir,
            self::SWOT => $thematic_tree_dataset->swot,
          )), 'error');
          break;
      }
    }

    return $success;
  }

  /**
   * Loads a Thematic Tree / Dataset Relationship.
   *
   * @param int $thematic_tree_tid
   *   The Thematic Tree ID.
   * @param int|null $dataset_id
   *   The Dataset Node ID.
   *
   * @return bool|\Drupal\dss_magdalena\DSS\SimaThematicTreeVariables
   *   This class loaded if there are records in the database, FALSE otherwise.
   */
  public static function load($thematic_tree_tid, $dataset_id = NULL) {
    $result = db_select(self::TABLE, 'ttd')
      ->fields('ttd')
      ->condition('thematic_tree_tid', $thematic_tree_tid);

    // If Dataset ID is given.
    if (isset($dataset_id)) {
      $result = $result->condition('dataset_id', $dataset_id);
    }

    // Obtain values.
    $rows = $result->execute()->fetchAllAssoc('dataset_id');

    if (count($rows) > 0) {
      $thematic_tree_variables = new static();
      foreach ($rows as $row) {
        $thematic_tree_variables->addThematicTreeDataset($row->thematic_tree_tid, $row->dataset_id, $row->{self::PO}, $row->{self::DPSIR}, $row->{self::SWOT});
      }
      return $thematic_tree_variables;
    }

    return FALSE;
  }

  /**
   * Deletes a record from the Thematic Tree/Variable relationship.
   *
   * @param int $thematic_tree_tid
   *   The thematic tree ID.
   * @param int|null $dataset_id
   *   The dataset ID, if given.
   *
   * @return bool|\DatabaseStatementInterface
   *   The Database Statement.
   */
  protected function delete($thematic_tree_tid, $dataset_id = NULL) {
    if (isset($thematic_tree_tid)) {
      $result = db_delete(self::TABLE)
        ->condition('thematic_tree_tid', $thematic_tree_tid);

      // If Dataset ID is given.
      if (isset($dataset_id)) {
        $result = $result->condition('dataset_id', $dataset_id);
      }

      return $result->execute();
    }
    return FALSE;
  }

}
