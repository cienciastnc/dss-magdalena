<?php
/**
 * @file
 * Stores and reads a XLS WEAP MasterFile.
 */

namespace Drupal\dss_magdalena\DSS;

use Drupal\dss_magdalena\DSS\Utils\SimaUtils;

/**
 * Implements loading a Sima Master File.
 *
 * Class SimaWeapMasterFileExcel.
 *
 * @package Drupal\dss_magdalena\DSS
 */
class SimaWeapMasterFileExcel extends SimaUtils {

  const WEAP_MASTER_DIR  = "public://weap_files";

  /**
   * List of directories.
   *
   * @var array
   *   Array of directories.
   */
  protected $directories = [
    self::WEAP_MASTER_DIR,
  ];

  /**
   * The Master File.
   *
   * @var string $csvFile
   *   WEAP CSV Master File.
   */
  protected $masterFile;

  /**
   * Public Constructor.
   *
   * If the WEAP Master file is not set, returns FALSE.
   */
  public function __construct() {
    if ($this->initializeDirectories()) {
      $this->setWeapMasterFile();
    }
    else {
      return FALSE;
    }
  }

  /**
   * Sets the current WEAP Master File.
   */
  protected function setWeapMasterFile() {
    $fid = variable_get('dss_engine_weap_masterfile_excel_fid', FALSE);
    if ($fid) {
      $file = file_load($fid);
      $this->masterFile = drupal_realpath($file->uri);
    }
  }

  /**
   * Returns the WEAP Master File.
   *
   * @return null|object
   *   Returns the WEAP Master File Object or NULL.
   */
  public function getWeapMasterFile() {
    $fid = variable_get('dss_engine_weap_masterfile_excel_fid', FALSE);
    if ($fid) {
      $file = file_load($fid);
      return $file ?: NULL;
    }
    return NULL;
  }

  /**
   * Saves the WEAP Master File.
   *
   * @param string $url
   *   The URL of the file to download.
   * @param bool $retry
   *   TRUE if we want to retry, FALSE otherwise.
   *
   * @return bool|int
   *   The fid if succeeds, FALSE otherwise.
   */
  public function saveWeapMasterFile($url, $retry = FALSE) {
    $fid = variable_get('dss_engine_weap_masterfile_excel_fid', FALSE);

    // Obtain old fid and delete it if different to the new fid.
    if ($fid === FALSE || $retry) {
      $filename = SimaWeapMasterFileExcel::WEAP_MASTER_DIR . '/master-2.1.6.1.xlsx';
      if ($file = system_retrieve_file($url, $filename, TRUE, FILE_EXISTS_REPLACE)) {
        // Now attach new fid.
        $fid = $file->fid;
        variable_set('dss_engine_weap_masterfile_excel_fid', $fid);
      }
    }
    return $fid;
  }

  /**
   * Returns the WEAP Master File in CSV Format.
   *
   * @return null|object
   *   Returns the WEAP Master File Object or NULL.
   */
  public function getWeapMasterFileCsv() {
    $fid = variable_get('dss_engine_weap_masterfile_csv_fid', FALSE);
    if ($fid) {
      $file = file_load($fid);
      return $file ?: NULL;
    }
    return NULL;
  }

  /**
   * Saves the WEAP Master File in CSV Format.
   *
   * This file should be saved after converting the original XLSX to CSV not
   * by giving it explicitly.
   *
   * @param string $url
   *   The URL of the file to download.
   * @param bool $retry
   *   TRUE if we want to retry, FALSE otherwise.
   *
   * @deprecated
   *
   * @return bool
   *   TRUE if succeeds, FALSE otherwise.
   */
  public function saveWeapMasterFileCsv($url, $retry = FALSE) {
    $fid = variable_get('dss_engine_weap_masterfile_csv_fid', FALSE);

    // Obtain old fid and delete it if different to the new fid.
    if ($fid === FALSE || $retry) {
      $filename = SimaWeapMasterFileExcel::WEAP_MASTER_DIR . '/master-2.1.6.1.csv';
      if ($file = system_retrieve_file($url, $filename, TRUE, FILE_EXISTS_REPLACE)) {
        // Now attach new fid.
        $fid = $file->fid;
        variable_set('dss_engine_weap_masterfile_csv_fid', $fid);
      }
    }
    return $fid;
  }

}
