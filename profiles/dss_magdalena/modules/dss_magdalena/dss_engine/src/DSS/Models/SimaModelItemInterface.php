<?php
/**
 * @file
 * An model item to be executed during the iteration.
 */

namespace Drupal\dss_magdalena\DSS\Models;

/**
 * Interface SimaModelItemInterface.
 *
 * @package Drupal\dss_magdalena\DSS\Models
 */
interface SimaModelItemInterface {

  /**
   * Returns this item's label.
   *
   * @return string
   *   This is the name or label that identifies this item.
   */
  public function label();

  /**
   * Sets the Model Context ID.
   *
   * @param string $context
   *   The Context ID.
   */
  public function setContextId($context);

  /**
   * Executes the tasks to be performed by this item.
   *
   * @return bool
   *   TRUE if execution succeeds, FALSE otherwise.
   */
  public function execute();

  /**
   * Sets the Local Item's data.
   *
   * @param array $data
   *   The Data array.
   */
  public function setData(array $data);

  /**
   * Returns the local Item's data.
   *
   * @return array
   *   The Data array.
   */
  public function getData();

  /**
   * List of datasets to load as input variables.
   *
   * @param array $datasets
   *   The list of datasets to load as input variables.
   */
  public function setInputVariablesMachineNameList(array $datasets);

  /**
   * Obtains the list of loaded input variables.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaResource[]
   *   A list of loaded input variables.
   */
  public function getInputVariables();

  /**
   * Sets the Case Study ID.
   *
   * @param int $cid
   *   The Case Study ID.
   */
  public function setCaseStudyId($cid);

  /**
   * Returns the Case Study ID.
   *
   * @return int
   *   The Case Study ID.
   */
  public function getCaseStudyId();

  /**
   * Returns the loaded Case Study.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaCaseStudy
   *   The loaded Case Study.
   */
  public function getCaseStudy();

  /**
   * Checks whether this is a Resource Type Model Item.
   *
   * @return bool
   *   TRUE if it is a Resource Type Model item, FALSE otherwise.
   */
  public function isResourceType();

}
