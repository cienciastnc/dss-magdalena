<?php
/**
 * @file
 * Model executor for DOR-H.
 */

namespace Drupal\dss_magdalena\DSS\Models\Sediments;

use Drupal\dss_magdalena\DSS\Entity\SimaModel;
use Drupal\dss_magdalena\DSS\Models\SimaFragmentationDorHModelExecutor;
use Drupal\dss_magdalena\DSS\Models\SimaModelExecutorInterface;

/**
 * Class SimaDorHModelExecutor.
 *
 * @package Drupal\dss_magdalena\DSS\Models\DorH
 */
class SimaSedimentsModelExecutor extends SimaFragmentationDorHModelExecutor implements SimaModelExecutorInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct($cid, $uuid = NULL) {
    $this->model = SimaModel::MODEL_SEDIMENTS;
    parent::__construct($cid, $uuid);
  }

  /**
   * {@inheritdoc}
   */
  public function initialize() {
    $this->outputVariablesMachineNameList = [
      'dss_impacto_q_solido',
      'dss_red_impactada_por_q_solido',
    ];

    // Creating the Items to process.
    $label = t('Generating Barrier Data');
    $item = new SimaSedimentsModelItem();
    $item->setId($label);
    $item->setCaseStudyId($this->getCaseStudy()->getId());
    $item->setInputVariablesMachineNameList($this->getInputVariablesMachineNameList());
    $this->addItem($item);
    $this->saveItems();

    $label = t('Generating Outputs');
    $item = new SimaSedimentsModelItem();
    $item->setId($label);
    $this->addItem($item);
    $this->saveItems();

    // Set and Save Context.
    $this->setContext([
      'cid' => $this->getCaseStudy()->getId(),
      'process_datastore' => $this->getProcessDatastore(),
      'input_variables_machine_name_list' => $this->inputVariablesMachineNameList,
      'output_variables_machine_name_list' => $this->outputVariablesMachineNameList,
    ]);
    $this->saveContext();
  }

  /**
   * {@inheritdoc}
   */
  public function saveOutputVariables() {
    parent::saveOutputVariables();
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary($results) {

  }

}
