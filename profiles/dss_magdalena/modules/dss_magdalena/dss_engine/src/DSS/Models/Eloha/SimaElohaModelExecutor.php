<?php
/**
 * @file
 * Model executor for ELOHA.
 */

namespace Drupal\dss_magdalena\DSS\Models\Eloha;

use Drupal\dss_magdalena\DSS\Entity\SimaCaseStudy;
use Drupal\dss_magdalena\DSS\Models\SimaModelExecutor;
use Drupal\dss_magdalena\DSS\Models\SimaModelExecutorInterface;
use Drupal\dss_magdalena\DSS\Entity\SimaResource;
use Drupal\dss_magdalena\DSS\Queue\SimaImportQueueItem;
use Drupal\dss_magdalena\DSS\Entity\SimaModel;
// Use Drupal\dss_magdalena\DSS\Utils\SimaDirectoriesTrait;.
// use Drupal\dss_magdalena\DSS\Entity\File\SimaElohaQEcoTable;.
use Drupal\dss_magdalena\DSS\Utils\SimaCsvWriterTrait;
/**
 * Class SimaElohaModelExecutor.
 *
 * @package Drupal\dss_magdalena\DSS\Models\Eloha
 */
class SimaElohaModelExecutor extends SimaModelExecutor implements SimaModelExecutorInterface {
  use SimaCsvWriterTrait;

  /**
   * Sets the reference case study.
   *
   * @var \Drupal\dss_magdalena\DSS\Entity\SimaCaseStudy
   */
  protected $refCase;

  /**
   * From Interface.
   */

  /**
   * {@inheritdoc}
   */
  protected $inputVariablesMachineNameList = [
    'dss_caudal_saliente_tramo',
  ];

  /**
   * {@inheritdoc}
   */
  public function initialize() {
    // Get 'dss_caudal_saliente_tramo's geojson.
    $ref_geojson = $this->getBranches();
    if (empty($ref_geojson)) {
      return;
    }

    // Get Case study reference resource loaded.
    $this->setRefCaseStudy();
    if ($this->getRefCaseStudy() == NULL) {
      return;
    }

    // Creates processing item from geojson's branches.
    foreach ($ref_geojson->getFeatures() as $feature) {
      $properties = $feature->getProperties();
      $label = $properties['GeoBranchID'];

      $data = [
        'tipo_hydro' => isset($properties['TipoHydro']) ? $properties['TipoHydro'] : NULL,
        'tipo_eleva' => isset($properties['Tipo elev']) ? $properties['Tipo elev'] : NULL,
      ];
      $item = new SimaElohaModelItem();
      $item->setId($label);
      $item->setData($data);
      $this->addItem($item);
    }
    $this->saveItems();

    // Get resource ID from Analysis case and Reference Case.
    $rid_case = $this->getCaudalSaliente()->getId();
    $ref_rid_case = $this->getCaudalSalienteRefId();

    // Get Weighted Parameters.
    $weighted_values = $this->getWeightedParameters();

    // Set Duration Ranges
    // 0-Caudal Alto, 1-Caudal Estacional, 2-Caudal Bajo, 3-Extremo Minimo
    // Blocks 0-> QMax-Id(0), 1-> Id(0)-Id(1), 2-> Id(1)-Id(2), 3->Id(2)-QMin.
    $duration_ranges = array(10, 75, 95);
    $duration_names = array('Qmax-Q10', 'Q10-Q75', 'Q75-Q95', 'Q95-Qmin');

    // Store information used by all items, set context.
    $context = [
      'rid' => $rid_case,
      'ref_rid' => $ref_rid_case,
      'ws' => $weighted_values['ws'],
      'wg' => $weighted_values['wg'],
      'duration_ranges' => $duration_ranges,
      'duration_names' => $duration_names,
      'analysis_results' => array(),
      'cid' => $this->getCaseStudy()->getId(),
      'process_datastore' => $this->getProcessDatastore(),
    ];
    $this->setContext($context);
    $this->saveContext();
  }

  /**
   * {@inheritdoc}
   */
  public function saveOutputVariables() {
    // Get Context for results.
    $context = $this->getContext();

    // Get case output entity.
    $case_output = $this->getCaseStudy()->getCaseOutputs();
    $case_output_id = $case_output->getId();

    // Load analysis resource.
    $rid_resource = SimaResource::load($context['rid']);
    if (!isset($rid_resource)) {
      // TODO: set an error message for this error.
      return;
    }

    // Set Eloha indicator and index output variables.
    $eloha_output_variable_names = [
      'excess_indicator' => 'dss_indicadores_eloha_exceso',
      'deficit_indicator' => 'dss_indicadores_eloha_deficit',
      'excess_index' => 'dss_indice_eloha_exceso',
      'deficit_index' => 'dss_indice_eloha_deficit',
      'impact' => "dss_impactos_eloha_verbales",
      'verdict' => "dss_veredicto_eloha",
    ];

    // Create Indicators and Indexes outputs.
    foreach ($eloha_output_variable_names as $output_name => $machine_name) {
      // Set URI.
      $csv_uri = 'public://eloha/' . $this->getCaseStudy()->getId() . '/' . $machine_name . '.csv';

      // Write CSV.
      if ($this->createCsv($context, $output_name, $machine_name, $csv_uri) == TRUE) {
        // Set info for new resource.
        $values = [
          'cid' => $this->getCaseStudy()->getId(),
          'case_output' => $case_output_id,
          'file_uri' => $csv_uri,
          'filename' => $machine_name . '.csv',
          'dss_machine_name' => $machine_name,
        ];

        // Create Resource.
        $queue_item = new SimaImportQueueItem($values);
        if ($resource = SimaResource::newFullyFormedResource($queue_item)) {
          $resource->save();

          // Add output resource.
          $case_output->addOutputResource(SimaModel::MODEL_ELOHA, $resource);
          $case_output->save();

          // Check if data store should be processed.
          if ($context["process_datastore"]) {
            $resource->processDataStore();
          }
        }
      }
    }

    // Update Model status.
    $case = $this->getCaseStudy();
    $case->setModelingStatus(SimaModel::MODEL_ELOHA, SimaModel::STATUS_OK);
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary($results) {

  }

  /**
   * Protected Methods.
   */

  /**
   * Obtains the First Resource in the Input Variables;.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaResource
   *   The SimaResource.
   */
  protected function getCaudalSaliente() {
    $input_vars = $this->getInputVariables();
    return reset($input_vars);
  }

  /**
   * Obtains the ID for the Reference Resource.
   *
   * @return bool|int
   *   The Resource ID if exits, FALSE otherwise.
   */
  protected function getCaudalSalienteRefId() {
    $ref_case = $this->getRefCaseStudy();
    $var_machine_name = $this->inputVariablesMachineNameList[0];
    $rid = $ref_case->getCaseOutputs()->checkWeapOutputExistsForDataset($var_machine_name);
    return !empty($rid) ? $rid : FALSE;
  }

  /**
   * Loads the Reference Case Study.
   */
  protected function setRefCaseStudy() {
    $cid = variable_get('dss_engine_eloha_ref_case_study_cid', NULL);
    $this->refCase = isset($cid) ? SimaCaseStudy::load($cid) : NULL;
  }

  /**
   * Gets the Reference Case Study.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaCaseStudy
   *   The Reference Case Study.
   */
  protected function getRefCaseStudy() {
    return $this->refCase;
  }

  /**
   * Returns the list of branches.
   */
  protected function getBranches() {
    $resource = $this->getCaudalSaliente();
    if (!empty($resource)) {
      $json = $resource->getOriginalGeoJsonLayer();
      $json = json_decode($json);
      return \GeoJson\GeoJson::jsonUnserialize($json);
    }
    return NULL;
  }

  /**
   * Sets Ws and Wg weighted parameters.
   *
   * @return array
   *    Ws, Wg values.
   */
  protected function getWeightedParameters() {
    $ws = variable_get('dss_engine_eloha_parameter_ws', 0.5);
    $wg = variable_get('dss_engine_eloha_parameter_wg', 0.5);
    return array('ws' => floatval($ws), 'wg' => floatval($wg));
  }

  /**
   * Csv for variables with time and range.
   *
   * @param array $context
   *    The context of the model.
   * @param string $output_name
   *    The name of the variable.
   * @param string $machine_name
   *    The machine name.
   * @param string $csv_uri
   *    The resource values.
   *
   * @return null|string
   *    File path of the csv or null.
   */
  protected function createCsv($context, $output_name, $machine_name, $csv_uri) {
    // Output data to csv container.
    $output_data = array();

    // Csv head.
    $csv_head = [
      'BranchID', 'GeoBranchId', 'Year', 'Timestep', 'Value',
    ];
    array_push($output_data, $csv_head);

    // Processing results by branch.
    foreach ($context['analysis_results'] as $branch_label => $branch_results) {
      $branch_id = $branch_results['branchId'];
      $geo_branch_id = $branch_id;
      $init_year = '';
      $output_results = $branch_results['data'][$output_name];

      // Adding month data to output array.
      foreach ($output_results as $month => $data_array) {
        // Temporal container.
        $temp = array();

        // Set empty month for verdict.
        if ($month == 0) {
          // Detects verdict data.
          $month = '';
        }

        // Adding a CSV's row to output data array.
        array_push($temp, $branch_id, $geo_branch_id, $init_year, $month, json_encode($data_array, JSON_FORCE_OBJECT));
        array_push($output_data, $temp);
      }
    }

    // Writing output data to a CSV file with its corresponding machine name.
    $directory = 'public://eloha/' . $this->getCaseStudy()->getId() . '/';
    if (file_prepare_directory($directory, FILE_CREATE_DIRECTORY)) {
      $saved = $this->saveUnManagedCsvFile($output_data, $csv_uri, FILE_EXISTS_REPLACE);
      return $saved;
    }
    return NULL;
  }

}
