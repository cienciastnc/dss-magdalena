<?php
/**
 * @file
 * Sima Network Node.`.
 */

namespace Drupal\dss_magdalena\DSS\Models\Network;
/**
 * SimaNetworkArc Class.
 */
class SimaNetworkArc {

  protected $arcId;
  protected $fromNode;
  protected $toNode;
  protected $qMed;
  protected $length;
  protected $gridCode;
  protected $aporteQssMed;
  protected $reservoirStorageCapacity;
  protected $reservoirFlow;
  protected $retentionRate;
  protected $cumulativeSST;
  /**
   * SimaNetworkArc constructor.
   *
   * @param int $arc_id
   *   The Arcid.
   */
  public function __construct($arc_id) {
    $this->arcId = $arc_id;
  }

  /**
   * Get ArcId value.
   *
   * @return mixed
   *   The value.
   */
  public function getArcId() {
    return $this->arcId;
  }

  /**
   * Get toNode value.
   *
   * @return mixed
   *   The value.
   */
  public function getFromNode() {
    return $this->fromNode;
  }

  /**
   * Get toNode value.
   *
   * @return mixed
   *   The value.
   */
  public function getToNode() {
    return $this->toNode;
  }

  /**
   * Set fromNode value.
   *
   * @param int $from_node
   *   Value to set.
   */
  public function setFromNode($from_node) {
    $this->fromNode = $from_node;
  }

  /**
   * Set toNode value.
   *
   * @param int $to_node
   *   Value to set.
   */
  public function setToNode($to_node) {
    $this->toNode = $to_node;
  }

  /**
   * Get qMed value.
   *
   * @return mixed
   *   The value.
   */
  public function getQmed() {
    return $this->qMed;
  }

  /**
   * Set qMed value.
   *
   * @param int $value
   *   Value to set.
   */
  public function setQmed($value) {
    $this->qMed = $value;
  }

  /**
   * Get qMed value.
   *
   * @return mixed
   *   The value.
   */
  public function getLength() {
    return $this->length;
  }

  /**
   * Set length(km) value.
   *
   * @param float $value
   *   Value to set.
   */
  public function setLength($value) {
    $this->length = $value;
  }

  /**
   * Get grid code value.
   *
   * @return mixed
   *   The value.
   */
  public function getGridCode() {
    return $this->gridCode;
  }

  /**
   * Set gird code value.
   *
   * @param float $value
   *   Value to set.
   */
  public function setGridCode($value) {
    $this->gridCode = $value;
  }

  /**
   * Get AporteQssMed value.
   *
   * @return mixed
   *   The value.
   */
  public function getAporteQssMed() {
    return $this->aporteQssMed;
  }

  /**
   * Set AporteQssMed value.
   *
   * @param float $value
   *   Value to set.
   */
  public function setAporteQssMed($value) {
    $this->aporteQssMed = $value;
  }

  /**
   * Get Capacidad Del Embalse value.
   *
   * @return mixed
   *   The value.
   */
  public function getReservoirStorageCapacity() {
    return $this->reservoirStorageCapacity;
  }

  /**
   * Set Capacidad Del Embalse value.
   *
   * @param float $value
   *   Value to set.
   */
  public function setReservoirStorageCapacity($value) {
    $this->reservoirStorageCapacity = $value;
  }

  /**
   * Get Caudal En Cada Embalse value.
   *
   * @return mixed
   *   The value.
   */
  public function getReservoirFlow() {
    return $this->reservoirFlow;
  }

  /**
   * Set Caudal En Cada Embalse value.
   *
   * @param float $value
   *   Value to set.
   */
  public function setReservoirFlow($value) {
    $this->reservoirFlow = $value;
  }

  /**
   * Get Efficiencia Atrapamiento value.
   *
   * @return mixed
   *   The value.
   */
  public function getRetentionRate() {
    return $this->retentionRate;
  }

  /**
   * Set Efficiencia Atrapamiento value.
   *
   * @param float $value
   *   Value to set.
   */
  public function setRetentionRate($value) {
    $this->retentionRate = $value;
  }

  /**
   * Set CumulativeSST value.
   *
   * @param float $value
   *   The value to set.
   */
  public function setCumulativeSst($value) {
    $this->cumulativeSST = $value;
  }

  /**
   * Get CumulativeSST value.
   *
   * @return mixed
   *    The value.
   */
  public function getCumulativeSst() {
    return $this->cumulativeSST;
  }

}
