<?php
/**
 * @file
 * Model for Fragmentation and DOR-H.
 */

namespace Drupal\dss_magdalena\DSS\Models;
use Drupal\dss_magdalena\DSS\Entity\SimaController;
use Drupal\dss_magdalena\DSS\Models\Network\SimaNetwork;
use Drupal\dss_magdalena\DSS\Entity\SimaResource;
use Drupal\dss_magdalena\DSS\SimaWeapIndex;

/**
 * Class SimaFragmentationDorHModelExecutor.
 *
 * @package Drupal\dss_magdalena\DSS\Models
 */
class SimaFragmentationDorHModelItem extends SimaModelItem {

  protected $outputVariablesMachineNameList = [];
  protected $outputVariables;

  /**
   * {@inheritdoc}
   */
  public function execute() {
    $context = $this->loadContext();
    $this->outputVariablesMachineNameList = $context['output_variables_machine_name_list'];

    $type = $this->label();

    switch ($type) {

      case 'Generating Barrier Data':
      case 'Generando Datos de Barrera':
        $reservoirs = $this->generateBarrierData();
        $context = $this->loadContext();
        $context['barrier_data'] = $reservoirs;
        $this->saveContext($context);
        break;

      case 'Generating Outputs':
      case 'Generando Salidas':
        $context = $this->loadContext();
        $network = new SimaNetwork();
        $network->readNetworkTopologyFile();
        $network->setNetworkTopology();
        $network->setReservoirData($context['barrier_data']);

        // Load the previously calculated data from the cache.
        // If not calculated, save in cache.
        $expiration = time() + 10600;
        $cache_id = $context['cid'] . 'functionalBranchResult';
        $cache = cache_get($cache_id, 'cache');
        if ($cache && !empty($cache->data)) {
          $main_river_output = $cache->data;
        }
        else {
          $cumulative_status = $network->getCumulativeStatus();
          $arc_barrier = $network->getArcBarrier();
          $arc_start_nodes = $network->getArcStarNodes();
          $zero_init = $network->getZeroInit();

          $network_vars_to_propagate = $network->getReservoirsFlow();
          $propagation_status = $network_vars_to_propagate;
          $arc_retention_rate = $network->getRetentionRates();
          $network_var_to_acum_w_retention = $network->getAporteQssMed();
          $cumulative_upstream_var_w_retention = $network_var_to_acum_w_retention;
          $start_id_downstream = 11;
          $arcid_rm_i = $start_id_downstream;

          $main_river_output = $network->functionalBranch3($start_id_downstream, 0,
                                                           $cumulative_status, $arc_barrier, $arc_start_nodes, $zero_init,
                                                           $network_vars_to_propagate, $propagation_status,
                                                           $arc_retention_rate, $network_var_to_acum_w_retention, $cumulative_upstream_var_w_retention,
                                                           $arcid_rm_i);
          cache_set($cache_id, $main_river_output, 'cache', $expiration);
        }

        $this->generateOutputs($main_river_output, $network);

        $context = $this->loadContext();
        $context['output_variables'] = $this->outputVariables;
        $this->saveContext($context);
        break;
    }

  }

  /**
   * Create input Barrier Data.
   *
   * @return array
   *   A list with resevoirs
   */
  public function generateBarrierData() {
    $barrier_data = [];

    $dss_capacidad = $this->getArrayBranchResult($this->inputVariablesMachineNameList[0]);

    $dss_project_id_hres = $this->listProjectId('hydropower');
    $dss_topology_arcid_hres = $this->listTopologyArcId('hydropower');
    $dss_pertenencia_al_caso_hres = $this->getArrayBranchResult($this->inputVariablesMachineNameList[1]);

    $dss_project_id_hror = $this->listProjectId('ror_hydropower_plant');
    $dss_topology_arcid_hror = $this->listTopologyArcId('ror_hydropower_plant');
    $dss_pertenencia_al_caso_hror = $this->getArrayBranchResult($this->inputVariablesMachineNameList[2]);

    // Hres.
    foreach ($dss_project_id_hres as $branch => $result) {
      $arcid = isset($dss_topology_arcid_hres[$branch]) ? $dss_topology_arcid_hres[$branch] : 0;
      $project_id = $result;
      $storage_capacity = isset($dss_capacidad[$branch]) ? $dss_capacidad[$branch] : 0;
      $sima_case_status = isset($dss_pertenencia_al_caso_hres[$branch]) ? $dss_pertenencia_al_caso_hres[$branch] : 0;
      $dam_height = isset($dss_dam_heigth[$branch]) ? $dss_dam_heigth[$branch] : 0;

      if ($arcid > 0) {
        $reservoir = [];
        $reservoir['arcid'] = $arcid;
        $reservoir['project_id'] = $project_id;
        $reservoir['storage_capacity'] = $storage_capacity;
        $reservoir['sima_case_status'] = $sima_case_status;
        $reservoir['dam_height'] = $dam_height;
        $reservoir['weap_branch'] = $branch;
        $barrier_data[] = $reservoir;
      }
    }
    // Hror.
    foreach ($dss_project_id_hror as $branch => $result) {
      $arcid = isset($dss_topology_arcid_hror[$branch]) ? $dss_topology_arcid_hror[$branch] : 0;
      $project_id = $result;
      $storage_capacity = 0;
      $sima_case_status = isset($dss_pertenencia_al_caso_hror[$branch]) ? $dss_pertenencia_al_caso_hror[$branch] : 0;
      $dam_height = 0;
      if ($arcid > 0) {
        $reservoir = [];
        $reservoir['arcid'] = $arcid;
        $reservoir['project_id'] = $project_id;
        $reservoir['storage_capacity'] = $storage_capacity;
        $reservoir['sima_case_status'] = $sima_case_status;
        $reservoir['dam_height'] = $dam_height;
        $reservoir['weap_branch'] = $branch;
        $barrier_data[] = $reservoir;
      }
    }
    return $barrier_data;
  }

  /**
   * Extract branch and value of input variable.
   *
   * @param array $input_variable_machine_name
   *   Data input variable.
   *
   * @return array
   *   An array with branch as key and value.
   */
  public function getArrayBranchResult($input_variable_machine_name) {

    $input_variable = SimaResource::loadByCaseStudyAndDataset($this->cid, $input_variable_machine_name);
    $data = $input_variable->getDataCsv();
    $weap = SimaWeapIndex::loadAllRecords();
    $weap_data = $weap->getValues();
    // $data = $weap->combineWith($data, 'branch_id');.
    $branch_values = [];
    next($data);
    foreach ($data as $item) {
      $branch = '';
      $branch_id = $item['BranchID'];
      if ($item[$branch_id] != 'BranchID') {
        $branch = $weap_data[$branch_id]['level1'];
        if (!empty($weap_data[$branch_id]['level2'])) {
          $branch .= '\\' . $weap_data[$branch_id]['level2'];
        }
        if (!empty($weap_data[$branch_id]['level3'])) {
          $branch .= '\\' . $weap_data[$branch_id]['level3'];
        }
        if (!empty($weap_data[$branch_id]['level4'])) {
          $branch .= '\\' . $weap_data[$branch_id]['level4'];
        }
        $branch .= empty($weap_data[$branch_id]['level1']) ? $weap_data[$branch_id]['level1'] : '';
        $branch_values[$branch] = !empty($item['Value']) ? $item['Value'] : 0;
      }
    }
    return $branch_values;
  }

  /**
   * Obtain list of Project id's.
   *
   * @param string $type
   *   Type: hydropower or ror_hydropower_plant.
   *
   * @return array
   *   An array with Branch(Full Name) as key and value is Project Id.
   */
  public function listProjectId($type) {
    $data = [];
    $sc = new SimaController();
    $entities = $sc->listEntities($type);

    foreach ($entities as $ficha) {
      $data[$ficha->getFullName()] = $ficha->getProjectId();
    }

    return $data;
  }

  /**
   * Obtain list of Topology ArcId's.
   *
   * @param string $type
   *   Type: hydropower or ror_hydropower_plant.
   *
   * @return array
   *   An array with Branch(Full Name) as key and value is Topology ArcId.
   */
  public function listTopologyArcId($type) {
    $data = [];
    $sc = new SimaController();
    $entities = $sc->listEntities($type);

    foreach ($entities as $ficha) {
      $data[$ficha->getFullName()] = $ficha->getTopologyArcId();
    }

    return $data;
  }

  /**
   * Generate the outputs data for this model.
   */
  public function generateOutputs($main_river_output, SimaNetwork $network) {

    $stream_flow = $network->getStreamFlow();
    $arc_ids = $network->getArcIds();
    $river_order = $network->getRiverOrder();
    $arc_lengths = $network->getArcLengths();
    $barrier_names = $network->getBarrierNames();
    $arc_barrier = $network->getArcBarrier();
    $header = [
      'BranchID', 'GeoBranchId', 'Year', 'Timestep', 'Value',
    ];

    $context = $this->loadContext();
    $this->outputVariablesMachineNameList = $context['output_variables_machine_name_list'];

    $out_arc_ids = array_keys($main_river_output->fArcIdsN);
    $out_cumulative_storage = array_values($main_river_output->cumVars);
    $out_propagated_upstream_vars = array_values($main_river_output->propVars);

    $out3 = array_values($stream_flow);
    $dor = [];

    // NETWORK_CUMULATIVE_UPSTREAM_VOLUME, Output Fragmentation.
    if (in_array('dss_vtot_embalses_arriba', $this->outputVariablesMachineNameList)) {
      $this->outputVariables['dss_vtot_embalses_arriba'][] = $header;
    }

    // NETWORK_DOR, Output Dorh.
    if (in_array('dss_dor_h', $this->outputVariablesMachineNameList)) {
      $this->outputVariables['dss_dor_h'][] = $header;
    }
    // NETWORK_DOR, Output DorW.
    if (in_array('dss_dor_w', $this->outputVariablesMachineNameList)) {
      $this->outputVariables['dss_dor_w'][] = $header;
    }

    // Sedimentos(Transporte solido).
    if (in_array('dss_impacto_q_solido', $this->outputVariablesMachineNameList)) {
      $this->outputVariables['dss_impacto_q_solido'][] = $header;

      $cumulative_sst = $network->getCumulativeSst();

      $sum_length_mayor_20 = 0.0;
      $sum_length_mayor_40 = 0.0;
      $sum_length_mayor_60 = 0.0;
      $sum_length_mayor_80 = 0.0;
      $sum_length_mayor_100 = 0.0;

      foreach ($main_river_output->cumVarsWRetention as $arcid => $cum_var_with_retention) {
        if ($cumulative_sst[$arcid] != 0) {
          $value = sprintf("%01.2f", (1 - $cum_var_with_retention / $cumulative_sst[$arcid]) * 100);
        }
        else {
          $value = 0.00;
        }
        // Sum lengths.
        if ($value > 20) {
          $sum_length_mayor_20 += $arc_lengths[$arcid];
        }
        if ($value > 40) {
          $sum_length_mayor_40 += $arc_lengths[$arcid];
        }
        if ($value > 60) {
          $sum_length_mayor_60 += $arc_lengths[$arcid];
        }
        if ($value > 80) {
          $sum_length_mayor_80 += $arc_lengths[$arcid];
        }
        if ($value > 100) {
          $sum_length_mayor_100 += $arc_lengths[$arcid];
        }

        if ($value > 100.00) {
          $value = 100.00;
        }

        if ($value < 0.0) {
          $value = 0.0;
        }
        // dss_impacto_q_solido.
        $this->outputVariables['dss_impacto_q_solido'][] = [
          $arcid,
          $arcid,
          '',
          '',
          $value,
        ];
      }

      if (in_array('dss_red_impactada_por_q_solido', $this->outputVariablesMachineNameList)) {
        $this->outputVariables['dss_red_impactada_por_q_solido'][] = $header;

        $value = json_encode(array(
          $sum_length_mayor_20,
          $sum_length_mayor_40,
          $sum_length_mayor_60,
          $sum_length_mayor_80,
          $sum_length_mayor_100,
        ), JSON_FORCE_OBJECT);
        $this->outputVariables['dss_red_impactada_por_q_solido'][] = [
          '',
          '',
          '',
          '',
          $value,
        ];
      }
    }

    // NETWORK_ID, Output Fragmentation.
    if (in_array('dss_id_subred', $this->outputVariablesMachineNameList)) {
      $this->outputVariables['dss_id_subred'][] = $header;

      foreach ($main_river_output->fArcIdsN as $out_arc_id => $value) {
        $this->outputVariables['dss_id_subred'][] = [
          $out_arc_id,
          $out_arc_id,
          '',
          '',
          $value,
        ];
      }
    }

    $num_elements = count($main_river_output->fArcIdsN);

    for ($i = 0; $i < $num_elements; $i++) {
      $dor_i = round($out_cumulative_storage[$i] * 1000000 / ($out3[$i] * 365 * 86400) * 100, 2);
      $dor_w = $dor_i * $out_propagated_upstream_vars[$i] / $out3[$i];
      $dor[$out_arc_ids[$i]] = $dor_i;

      // Output DorH.
      if (in_array('dss_vtot_embalses_arriba', $this->outputVariablesMachineNameList)) {
        $value = $out_cumulative_storage[$i];
        $this->outputVariables['dss_vtot_embalses_arriba'][] = [
          $out_arc_ids[$i],
          $out_arc_ids[$i],
          '',
          '',
          $value,
        ];
      }

      // Output DorH.
      if (in_array('dss_dor_h', $this->outputVariablesMachineNameList)) {
        $value = sprintf("%01.2f", $dor_i);
        $this->outputVariables['dss_dor_h'][] = [
          $out_arc_ids[$i],
          $out_arc_ids[$i],
          '',
          '',
          $value,
        ];
      }

      // Output DorW.
      if (in_array('dss_dor_w', $this->outputVariablesMachineNameList)) {
        $value = sprintf("%01.2f", $dor_w);
        $this->outputVariables['dss_dor_w'][] = [
          $out_arc_ids[$i],
          $out_arc_ids[$i],
          '',
          '',
          $value,
        ];
      }
    }

    // Creates the summary files with statistics.
    $river_lengths_4 = [];
    $river_lengths_5 = [];
    $river_lengths_6 = [];
    $river_lengths_7 = [];
    $river_lengths_8 = [];
    $river_lengths_4_8 = [];

    $river_lengths_4_na = [];
    $river_lengths_5_na = [];
    $river_lengths_6_na = [];
    $river_lengths_7_na = [];
    $river_lengths_8_na = [];
    $river_lengths_4_8_na = [];

    $river_lengths_dor_0_2 = [];
    $river_lengths_dor_2_5 = [];
    $river_lengths_dor_5_10 = [];
    $river_lengths_dor_10_15 = [];
    $river_lengths_dor_15_25 = [];
    $river_lengths_dor_25_50 = [];
    $river_lengths_dor_50_100 = [];
    $river_lengths_dor_100_more = [];

    $river_lengths_sum_4 = 0;
    $river_lengths_sum_5 = 0;
    $river_lengths_sum_6 = 0;
    $river_lengths_sum_7 = 0;
    $river_lengths_sum_8 = 0;

    // HERE finish the summary.
    foreach ($arc_ids as $arc) {
      $frag_id = $main_river_output->fArcIdsN[$arc];
      $dor_id = $dor[$arc];

      if (($frag_id == 0) && ($arc_barrier[$arc] > 0) && (!in_array($arc_barrier[$arc], array_keys($river_lengths_4_na)))) {
        $river_lengths_4_na[$arc_barrier[$arc]] = 0.0;
        $river_lengths_5_na[$arc_barrier[$arc]] = 0.0;
        $river_lengths_6_na[$arc_barrier[$arc]] = 0.0;
        $river_lengths_7_na[$arc_barrier[$arc]] = 0.0;
        $river_lengths_8_na[$arc_barrier[$arc]] = 0.0;
      }

      if (!in_array($frag_id, array_keys($river_lengths_4))) {
        $river_lengths_4[$frag_id] = 0.0;
        $river_lengths_5[$frag_id] = 0.0;
        $river_lengths_6[$frag_id] = 0.0;
        $river_lengths_7[$frag_id] = 0.0;
        $river_lengths_8[$frag_id] = 0.0;

        $river_lengths_dor_0_2[$frag_id] = 0.0;
        $river_lengths_dor_2_5[$frag_id] = 0.0;
        $river_lengths_dor_5_10[$frag_id] = 0.0;
        $river_lengths_dor_10_15[$frag_id] = 0.0;
        $river_lengths_dor_15_25[$frag_id] = 0.0;
        $river_lengths_dor_25_50[$frag_id] = 0.0;
        $river_lengths_dor_50_100[$frag_id] = 0.0;
        $river_lengths_dor_100_more[$frag_id] = 0.0;
      }

      $reach_order = $river_order[$arc];
      switch ($reach_order) {
        case 4:
          $river_lengths_4[$frag_id] += $arc_lengths[$arc];
          if (($frag_id == 0) &&($arc_barrier[$arc] > 0)) {
            $river_lengths_4_na[$arc_barrier[$arc]] += $arc_lengths[$arc];
          }
          break;

        case 5:
          $river_lengths_5[$frag_id] += $arc_lengths[$arc];
          if (($frag_id == 0) &&($arc_barrier[$arc] > 0)) {
            $river_lengths_5_na[$arc_barrier[$arc]] += $arc_lengths[$arc];
          }
          break;

        case 6:
          $river_lengths_6[$frag_id] += $arc_lengths[$arc];
          if (($frag_id == 0) &&($arc_barrier[$arc] > 0)) {
            $river_lengths_6_na[$arc_barrier[$arc]] += $arc_lengths[$arc];
          }
          break;

        case 7:
          $river_lengths_7[$frag_id] += $arc_lengths[$arc];
          if (($frag_id == 0) &&($arc_barrier[$arc] > 0)) {
            $river_lengths_7_na[$arc_barrier[$arc]] += $arc_lengths[$arc];
          }
          break;

        case 8:
          $river_lengths_8[$frag_id] += $arc_lengths[$arc];
          if (($frag_id == 0) &&($arc_barrier[$arc] > 0)) {
            $river_lengths_8_na[$arc_barrier[$arc]] += $arc_lengths[$arc];
          }
          break;
      }

      if (($dor_id >= 0.0) && ($dor_id <= 20.0)) {
        $river_lengths_dor_0_2[$frag_id] += $arc_lengths[$arc];
      }
      elseif (($dor_id > 20.0) && ($dor_id <= 40.0)) {
        $river_lengths_dor_2_5[$frag_id] += $arc_lengths[$arc];
      }
      elseif (($dor_id > 40.0) && ($dor_id <= 60.0)) {
        $river_lengths_dor_5_10[$frag_id] += $arc_lengths[$arc];
      }
      elseif (($dor_id > 60.0) && ($dor_id <= 80.0)) {
        $river_lengths_dor_10_15[$frag_id] += $arc_lengths[$arc];
      }
      elseif (($dor_id > 80.0) && ($dor_id <= 100.0)) {
        $river_lengths_dor_15_25[$frag_id] += $arc_lengths[$arc];
      }
      elseif (($dor_id > 100.0) && ($dor_id <= 150.0)) {
        $river_lengths_dor_25_50[$frag_id] += $arc_lengths[$arc];
      }
      elseif (($dor_id > 150.0) && ($dor_id <= 200.0)) {
        $river_lengths_dor_50_100[$frag_id] += $arc_lengths[$arc];
      }
      else {
        $river_lengths_dor_100_more[$frag_id] += $arc_lengths[$arc];
      }
    }

    // Output Fragmentation.
    if (in_array('dss_longitud_cada_subred', $this->outputVariablesMachineNameList)) {
      $this->outputVariables['dss_longitud_cada_subred'][] = $header;
    }

    // Output DorH.
    if (in_array('dss_longitud_max_subredes_dor_h', $this->outputVariablesMachineNameList)) {
      $this->outputVariables['dss_longitud_max_subredes_dor_h'][] = $header;
    }

    foreach ($river_lengths_4 as $net => $value) {
      $river_lengths_sum_4 += $river_lengths_4[$net];
      $river_lengths_sum_5 += $river_lengths_5[$net];
      $river_lengths_sum_6 += $river_lengths_6[$net];
      $river_lengths_sum_7 += $river_lengths_7[$net];
      $river_lengths_sum_8 += $river_lengths_8[$net];

      $river_lengths_4_8[$net] = $river_lengths_4[$net] + $river_lengths_5[$net] + $river_lengths_6[$net] + $river_lengths_7[$net] + $river_lengths_8[$net];

      // Output Fragmentation.
      if (in_array('dss_longitud_cada_subred', $this->outputVariablesMachineNameList)) {
        $value = json_encode(array(
          $river_lengths_4[$net],
          $river_lengths_5[$net],
          $river_lengths_6[$net],
          $river_lengths_7[$net],
          $river_lengths_8[$net],
          $river_lengths_4_8[$net],
        ), JSON_FORCE_OBJECT);
        $this->outputVariables['dss_longitud_cada_subred'][] = [
          $net,
          $net,
          '',
          '',
          $value,
        ];
      }

      // Output DorH.
      if (in_array('dss_longitud_max_subredes_dor_h', $this->outputVariablesMachineNameList)) {
        $value = json_encode(array(
          $river_lengths_dor_0_2[$net],
          $river_lengths_dor_2_5[$net],
          $river_lengths_dor_5_10[$net],
          $river_lengths_dor_10_15[$net],
          $river_lengths_dor_15_25[$net],
          $river_lengths_dor_25_50[$net],
          $river_lengths_dor_50_100[$net],
          $river_lengths_dor_100_more[$net],
        ), JSON_FORCE_OBJECT);
        $this->outputVariables['dss_longitud_max_subredes_dor_h'][] = [
          $net,
          $net,
          '',
          '',
          $value,
        ];
      }
    }

    $river_lengths_sum_4_8 = $river_lengths_sum_4 + $river_lengths_sum_5 + $river_lengths_sum_6 + $river_lengths_sum_7 + $river_lengths_sum_8;

    // Output Fragmentation.
    if (in_array('dss_longitud_total_subredes', $this->outputVariablesMachineNameList)) {
      $this->outputVariables['dss_longitud_total_subredes'][] = $header;

      $value = json_encode(array(
        $river_lengths_sum_4,
        $river_lengths_sum_5,
        $river_lengths_sum_6,
        $river_lengths_sum_7,
        $river_lengths_sum_8,
        $river_lengths_sum_4_8,
      ), JSON_FORCE_OBJECT);
      $this->outputVariables['dss_longitud_total_subredes'][] = [
        '',
        '',
        '',
        '',
        $value,
      ];
    }
    foreach ($river_lengths_4_na as $net => $value) {
      $river_lengths_4_8_na[$net] = $river_lengths_4_na[$net] + $river_lengths_5_na[$net] + $river_lengths_6_na[$net] + $river_lengths_7_na[$net] + $river_lengths_8_na[$net];
    }
    // Output Fragmentation.
    if (in_array('dss_longitud_relativa_no_afectada', $this->outputVariablesMachineNameList)) {
      $this->outputVariables['dss_longitud_relativa_no_afectada'][] = $header;

      foreach ($river_lengths_4_na as $net => $value) {
        $value = json_encode(array(
          !empty($river_lengths_4[$net]) ? $river_lengths_4_na[$net] / $river_lengths_4[$net] : "",
          !empty($river_lengths_5[$net]) ? $river_lengths_5_na[$net] / $river_lengths_5[$net] : "",
          !empty($river_lengths_6[$net]) ? $river_lengths_6_na[$net] / $river_lengths_6[$net] : "",
          !empty($river_lengths_7[$net]) ? $river_lengths_7_na[$net] / $river_lengths_7[$net] : "",
          !empty($river_lengths_8[$net]) ? $river_lengths_8_na[$net] / $river_lengths_8[$net] : "",
          !empty($river_lengths_4_8[$net]) ? $river_lengths_4_8_na[$net] / $river_lengths_4_8[$net] : "",
        ), JSON_FORCE_OBJECT);
        $this->outputVariables['dss_longitud_relativa_no_afectada'][] = [
          $net,
          $net,
          '',
          '',
          $value,
        ];
      }
    }
  }

}
