<?php
/**
 * @file
 * Implements an item for Fragmentation model.
 */

namespace Drupal\dss_magdalena\DSS\Models\Fragmentation;

use Drupal\dss_magdalena\DSS\Models\SimaFragmentationDorHModelItem;
use Drupal\dss_magdalena\DSS\Models\SimaModelItemInterface;

/**
 * Class SimaFragmentationModelItem.
 *
 * @package Drupal\dss_magdalena\DSS\Models\Fragmentation
 */
class SimaFragmentationModelItem extends SimaFragmentationDorHModelItem implements SimaModelItemInterface {

  /**
   * {@inheritdoc}
   */
  public function execute() {
    parent::execute();
  }

}
