<?php
/**
 * @file
 * Interface for SimaModelExecutorInterface.
 */

namespace Drupal\dss_magdalena\DSS\Models;

/**
 * Interface SimaModelExecutorInterface.
 *
 * @package Drupal\dss_magdalena\DSS\Models
 */
interface SimaModelExecutorInterface {

  /**
   * Gets the Executor's UUID.
   *
   * @return string
   *   The Executor's UUID.
   */
  public function getUuid();

  /**
   * Instructs the executor to process or not the datatore for all resources.
   *
   * @param bool $process_datastore
   *   TRUE to process the Datastore, FALSE otherwise.
   */
  public function setProcessDatastore($process_datastore);

  /**
   * Pre-Initializes the model.
   *
   * This method is executed to add model items for creating missing resources
   * needed by the model.
   */
  public function preInitialize();

  /**
   * Initializes the model.
   */
  public function initialize();

  /**
   * Obtains the number of model items to process.
   *
   * This function can be used to obtain the number of items that will be used
   * for processing.
   *
   * @return int
   *   The items to process.
   */
  public function getNumberOfItems();

  /**
   * Adds an item to the pool of items to execute.
   *
   * @param \Drupal\dss_magdalena\DSS\Models\SimaModelItemInterface $item
   *   An item to add to the pool of model items.
   */
  public function addItem(SimaModelItemInterface $item);

  /**
   * Obtains an item from the pool.
   *
   * @param int $key
   *   The item key.
   *
   * @return \Drupal\dss_magdalena\DSS\Models\SimaModelItemInterface $item
   *   An item from the pool of model items.
   */
  public function getItem($key);

  /**
   * Processes a single item from the model.
   *
   * @param \Drupal\dss_magdalena\DSS\Models\SimaModelItemInterface $item
   *   The item to process.
   *
   * @return bool
   *   TRUE if processing succeeds, FALSE otherwise.
   */
  public function processItem(SimaModelItemInterface $item);

  /**
   * Saves all the data as model output variables (Resources).
   *
   * @return bool
   *   TRUE if saving output variables succeeds, FALSE otherwise.
   */
  public function saveOutputVariables();

  /**
   * Gets a result summary.
   *
   * @param array $results
   *   The results of the processing.
   *
   * @return mixed
   *   The processing output summary.
   */
  public function getSummary($results);

  /**
   * Returns the list of Input Variables Machine Names.
   *
   * @return array
   *   A list of input variables machine names.
   */
  public function getInputVariablesMachineNameList();

}
