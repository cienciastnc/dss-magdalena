<?php
/**
 * @file
 * Model executor for Fragmentation.
 */

namespace Drupal\dss_magdalena\DSS\Models\Fragmentation;

use Drupal\dss_magdalena\DSS\Entity\SimaModel;
use Drupal\dss_magdalena\DSS\Models\SimaFragmentationDorHModelExecutor;
use Drupal\dss_magdalena\DSS\Models\SimaModelExecutorInterface;

/**
 * Class SimaFragmentationModelExecutor.
 *
 * @package Drupal\dss_magdalena\DSS\Models\Fragmentation
 */
class SimaFragmentationModelExecutor extends SimaFragmentationDorHModelExecutor implements SimaModelExecutorInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct($cid, $uuid = NULL) {
    $this->model = SimaModel::MODEL_FRAGMENTATION;
    parent::__construct($cid, $uuid);
  }

  /**
   * {@inheritdoc}
   */
  public function initialize() {
    $this->outputVariablesMachineNameList = [
      'dss_id_subred',
      'dss_longitud_cada_subred',
      'dss_longitud_total_subredes',
      'dss_longitud_relativa_no_afectada',
    ];

    // Creating the Items to process.
    $label = t('Generating Barrier Data');
    $item = new SimaFragmentationModelItem();
    $item->setId($label);
    $item->setCaseStudyId($this->getCaseStudy()->getId());
    $item->setInputVariablesMachineNameList($this->getInputVariablesMachineNameList());
    $this->addItem($item);
    $this->saveItems();

    $label = t('Generating Outputs');
    $item = new SimaFragmentationModelItem();
    $item->setId($label);
    $this->addItem($item);
    $this->saveItems();

    // Set and Save Context.
    $this->setContext([
      'cid' => $this->getCaseStudy()->getId(),
      'process_datastore' => $this->getProcessDatastore(),
      'input_variables_machine_name_list' => $this->inputVariablesMachineNameList,
      'output_variables_machine_name_list' => $this->outputVariablesMachineNameList,
    ]);
    $this->saveContext();
  }

  /**
   * {@inheritdoc}
   */
  public function saveOutputVariables() {
    parent::saveOutputVariables();
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary($results) {

  }

}
