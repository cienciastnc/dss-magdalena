<?php
/**
 * @file
 * Model executor for DOR-H.
 */

namespace Drupal\dss_magdalena\DSS\Models\DorH;

use Drupal\dss_magdalena\DSS\Entity\SimaModel;
use Drupal\dss_magdalena\DSS\Models\SimaFragmentationDorHModelExecutor;
use Drupal\dss_magdalena\DSS\Models\SimaModelExecutorInterface;

/**
 * Class SimaDorHModelExecutor.
 *
 * @package Drupal\dss_magdalena\DSS\Models\DorH
 */
class SimaDorHModelExecutor extends SimaFragmentationDorHModelExecutor implements SimaModelExecutorInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct($cid, $uuid = NULL) {
    $this->model = SimaModel::MODEL_DOR_H;
    parent::__construct($cid, $uuid);
  }

  /**
   * {@inheritdoc}
   */
  public function initialize() {
    $this->outputVariablesMachineNameList = [
      'dss_longitud_max_subredes_dor_h',
      'dss_dor_h',
      'dss_vtot_embalses_arriba',
    ];

    // Creating the Items to process.
    $label = t('Generating Barrier Data');
    $item = new SimaDorHModelItem();
    $item->setId($label);
    $item->setCaseStudyId($this->getCaseStudy()->getId());
    $item->setInputVariablesMachineNameList($this->getInputVariablesMachineNameList());
    $this->addItem($item);
    $this->saveItems();

    $label = t('Generating Outputs');
    $item = new SimaDorHModelItem();
    $item->setId($label);
    $this->addItem($item);
    $this->saveItems();

    // Set and Save Context.
    $this->setContext([
      'cid' => $this->getCaseStudy()->getId(),
      'process_datastore' => $this->getProcessDatastore(),
      'input_variables_machine_name_list' => $this->inputVariablesMachineNameList,
      'output_variables_machine_name_list' => $this->outputVariablesMachineNameList,
    ]);
    $this->saveContext();
  }

  /**
   * {@inheritdoc}
   */
  public function saveOutputVariables() {
    parent::saveOutputVariables();
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary($results) {

  }

}
