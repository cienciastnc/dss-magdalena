<?php
/**
 * @file
 * Implements an item for ELOHA model.
 */

namespace Drupal\dss_magdalena\DSS\Models\Eloha;

use Drupal\dss_magdalena\DSS\Entity\SimaResource;
use Drupal\dss_magdalena\DSS\Models\SimaModelItem;
use Drupal\dss_magdalena\DSS\Models\SimaModelItemInterface;
use Drupal\dss_magdalena\DSS\Entity\File\SimaElohaQEcoTable;

/**
 * Class SimaElohaModelItem.
 *
 * @package Drupal\dss_magdalena\DSS\Models\Eloha
 */
class SimaElohaModelItem extends SimaModelItem implements SimaModelItemInterface {

  // Indicator_type.
  const ELOHA_EXCESS = 0;
  const ELOHA_DEFICIT = 1;

  // Flow Component type.
  const ELOHA_HIGH_FLOW = 0;
  const ELOHA_MEDIUM_FLOW = 1;
  const ELOHA_LOW_FLOW = 2;
  const ELOHA_MINIMUM_FLOW = 3;

  // Eloha.
  const ELOHA_EXCESS_SEARCH_ARRAY_STRING = array('Exceso');
  const ELOHA_DEFICIT_SEARCH_ARRAY_STRING = array('Deficit');
  const ELOHA_HIGH_FLOW_SEARCH_ARRAY_STRING = array(
    'Caudales Altos. Qmax-Q10', 'Caudales Altos', 'Qmax-Q10',
  );
  const ELOHA_MEDIUM_FLOW_SEARCH_ARRAY_STRING = array(
    'Caudales Medios. Q10-Q75', 'Caudales Medios', 'Q10-Q75',
  );
  const ELOHA_LOW_FLOW_SEARCH_ARRAY_STRING = array(
    'Caudales Bajos. Q75-Q95', 'Caudales Bajos', 'Q75-Q95',
  );
  const ELOHA_MINIMUM_FLOW_SEARCH_ARRAY_STRING = array(
    'Extremos minimos. Q95-Qmin', 'Extremos minimos', 'Q95-Qmin',
  );

  // Eloha Indexes.
  const ELOHA_INDEX_CRITICAL = 2;
  const ELOHA_INDEX_ALERT = 1;
  const ELOHA_INDEX_NORMAL = 0;

  // Eloha Alteration modes.
  const ELOHA_CRITICAL_ALTERATION = "0";
  const ELOHA_ALERT_ALTERATION = "1";

  /**
   * Input variable: the branch characterization.
   *
   * @var string
   */
  protected $elevationType;

  /**
   * Input variable: the branch characterization.
   *
   * @var string
   */
  protected $regimeType;

  /**
   * The reference q_eco_table.
   *
   * @var array
   */
  protected $qEcoTable;

  /**
   * The filtered q_eco_table by elevation, regime and flow attribute.
   *
   * @var array
   */
  protected $filteredQecoTable;


  /**
   * Reference to context var.
   *
   * @var array
   */
  protected $context;

  /**
   * The resource ID of analisys case.
   *
   * @var int
   */
  protected $rid;

  /**
   * The resource ID of analisys case.
   *
   * @var int
   */
  protected $refRid;

  /**
   * Reference to rid_data var.
   *
   * @var array
   */
  protected $ridData;

  /**
   * Reference to ref_rid_data var.
   *
   * @var array
   */
  protected $refRidData;

  /**
   * Reference to Duration Ranges Data & names.
   *
   * @var array
   */
  protected $durationRanges;
  protected $durationNames;

  /**
   * Reference to Ws and Wg weighted parameters.
   *
   * @var array
   */
  protected $ws;
  protected $wg;

  /**
   * {@inheritdoc}
   */
  public function execute() {
    // Get Context.
    $this->context = $this->loadContext();

    // Set array entry for the item to be analysed.
    $this->context['analysis_results'][$this->id] = array();
    $this->context['analysis_results'][$this->id]['data'] = array();
    $this->context['analysis_results'][$this->id]['data']['excess_indicator'] = array();
    $this->context['analysis_results'][$this->id]['data']['deficit_indicator'] = array();
    $this->context['analysis_results'][$this->id]['data']['excess_index'] = array();
    $this->context['analysis_results'][$this->id]['data']['deficit_index'] = array();
    $this->context['analysis_results'][$this->id]['data']['impact'] = array();
    $this->context['analysis_results'][$this->id]['data']['verdict'] = array();

    // $check = $this->context['analysis_results'][$this->id];
    // $check_data = $this->context['analysis_results'][$this->id]['data'];
    // Set initial values from context to run the model.
    $this->setAnalysisAndReferenceRid();
    $this->setWeightedParameters();
    $this->setQecoTable();
    $this->setDurationRanges();
    $this->setDurationNames();

    // Get data needed by the item model.
    $this->setBranchIdAndYear($this->rid);
    $this->setDataFromCases($this->rid, $this->refRid);
    $this->regimeType = $this->data["tipo_hydro"];
    $this->elevationType = $this->data["tipo_eleva"];

    // Sanity check.
    if ($this->qEcoTable == NULL) {
      $this->context['analysis_results'][$this->id]['log_message'] = "QEcoTable no válida";
      // 1-error.
      $this->context['analysis_results'][$this->id]['success'] = 1;
      $this->saveContext($this->context);
      return;
    }

    if (empty($this->ridData)) {
      $this->context['analysis_results'][$this->id]['log_message'] = "Resource de caso de estudio no válido";
      $this->context['analysis_results'][$this->id]['success'] = 1;
      $this->saveContext($this->context);
      return;
    }

    if (empty($this->refRidData)) {
      $this->context['analysis_results'][$this->id]['log_message'] = "Resource de referencia no válido";
      $this->context['analysis_results'][$this->id]['success'] = 1;
      $this->saveContext($this->context);
      return;
    }

    // Run model for the current branch.
    $this->processIndicators();
    $this->processQecoTable();
    $this->processIndexesAndImpactsAndVerdict();

    // Process verdict.
    // $this->context['branch_id'][] = $this->id;
    // Save Context.
    $this->saveContext($this->context);
  }

  /**
   * Loads and returns the Q-Eco Table Array.
   */
  protected function setAnalysisAndReferenceRid() {
    $this->rid = $this->context['rid'];
    $this->refRid = $this->context['ref_rid'];
  }

  /**
   * Loads and returns the Q-Eco Table Array.
   */
  protected function setQecoTable() {
    if ($eloha_qeco_table = SimaElohaQEcoTable::loadFromSystem()) {
      $this->qEcoTable = $eloha_qeco_table->getData();
    }
  }

  /**
   * Returns the Q-Eco Table array.
   *
   * @return array
   *   An array with all rows of the Q-Eco Table.
   */
  protected function getQecoTable() {
    return $this->qEcoTable;
  }

  /**
   * Sets Ws and Wg weighted parameters.
   */
  protected function setWeightedParameters() {
    $this->ws = $this->context['ws'];
    $this->wg = $this->context['wg'];
  }

  /**
   * Sets Ws and Wg weighted parameters.
   */
  protected function setDurationRanges() {
    $this->durationRanges = $this->context['duration_ranges'];
  }

  /**
   * Sets Ws and Wg weighted parameters.
   */
  protected function setDurationNames() {
    $this->durationNames = $this->context['duration_names'];
  }

  /**
   * Returns Ws weighted parameter.
   *
   * @return float
   *   The Ws parameter value.
   */
  protected function getWs() {
    return $this->ws;
  }

  /**
   * Returns Wg weighted parameter.
   *
   * @return float
   *   The Wg parameter value.
   */
  protected function getWg() {
    return $this->wg;
  }

  /**
   * Set the branchid and year for the analysis case.
   *
   * Using the case being analyzed..
   *
   * @param int $rid_var
   *   Resource ID for the analysis case.
   */
  protected function setBranchIdAndYear($rid_var) {
    // Set resource field request.
    $fields = "DISTINCT MIN(branchid) as branchid";

    // Get data from analysis resource.
    if ($resource = SimaResource::load($rid_var)) {
      $rid_data = $resource->getDataStoreResultsNew($fields, FALSE, $this->id);

      // Sets analysis case info, needed by the creation of the output CSV.
      $rid_info = current($rid_data['items']);
      $this->context['analysis_results'][$this->id]['branchId'] = $rid_info['branchid'];
    }
    else {
      $this->context['analysis_results'][$this->id]['branchId'] = '';
    }
  }

  /**
   * Obtains the Resource (Caudal Saliente de Tramo) for both cases.
   *
   * Using the case being analyzed and the reference case study.
   *
   * @param int $rid_var
   *   Resource ID for the analysis case.
   * @param int $ref_rid_var
   *   Resource ID for the reference case.
   */
  protected function setDataFromCases($rid_var, $ref_rid_var) {
    $this->ridData = array();
    $this->refRidData = array();

    // Set resource field request.
    // This query has been tested in Postgresql and Mysql.
    $fields = "CONCAT(year,'-',RIGHT(CONCAT('0',CAST(timestep AS char(2))),2),'-01') AS ttime, timestep, value";

    // Loading reference Resource for "Caudal Saliente de Tramo".
    $rid_data = array();
    $rid_data_keys = array();
    if ($resource = SimaResource::load($rid_var)) {
      $rid_data = $resource->getDataStoreResultsNew($fields, FALSE, $this->id);

      // Get ttime as keys for latter comparison.
      $rid_data_keys = array_column($rid_data['items'], 'ttime');
    }

    // Loading analysis Resource for "Caudal Saliente de Tramo".
    $ref_rid_data = array();
    $ref_rid_data_keys = array();
    if ($ref_resource = SimaResource::load($ref_rid_var)) {
      $ref_rid_data = $ref_resource->getDataStoreResultsNew($fields, FALSE, $this->id);

      // Get ttime as keys for latter comparison.
      $ref_rid_data_keys = array_column($ref_rid_data['items'], 'ttime');
    }

    // Compare data keys to handle the same data by year and month.
    $permitted_keys_array = array_intersect($ref_rid_data_keys, $rid_data_keys);

    // Set Analysis data.
    foreach ($rid_data['items'] as $data) {
      // Check if data element is in the permitted keys arrays, if not continue.
      if (!in_array($data['ttime'], $permitted_keys_array)) {
        continue;
      }

      // Add it to the data array.
      if (!isset($this->ridData[$data['timestep']])) {
        $this->ridData[$data['timestep']] = array();
      }
      array_push($this->ridData[$data['timestep']], $data['value']);
    }

    // Set Reference data.
    foreach ($ref_rid_data['items'] as $data) {
      // Check if data element is in the permitted keys arrays, if not continue.
      if (!in_array($data['ttime'], $permitted_keys_array)) {
        continue;
      }

      // Add it to the data array.
      if (!isset($this->refRidData[$data['timestep']])) {
        $this->refRidData[$data['timestep']] = array();
      }
      array_push($this->refRidData[$data['timestep']], $data['value']);
    }

    // Sort Analysis Data by month.
    foreach ($this->ridData as &$month_array) {
      rsort($month_array, SORT_NUMERIC);
    }

    // Sort Reference Data by month.
    foreach ($this->refRidData as &$month_array) {
      rsort($month_array, SORT_NUMERIC);
    }
  }

  /**
   * Calc the excess and indicators.
   */
  protected function processIndicators() {
    // Process by month.
    // This is set just for easy to understand.
    $months_total = 12;
    for ($month = 0; $month < $months_total; $month++) {
      // Create result excess accumulator.
      if (!isset($this->context['analysis_results'][$this->id]['data']['excess_indicator'][$month + 1])) {
        $this->context['analysis_results'][$this->id]['data']['excess_indicator'][$month + 1] = array();
      }

      // Create result deficit accumulator.
      if (!isset($this->context['analysis_results'][$this->id]['data']['deficit_indicator'][$month + 1])) {
        $this->context['analysis_results'][$this->id]['data']['deficit_indicator'][$month + 1] = array();
      }

      // Calculate difference between reference and analysis case.
      $data_diff = array_map(function($a, $b) {
        return $a - $b;
      }, $this->ridData[$month + 1], $this->refRidData[$month + 1]);

      // Conversion from data duration range to data difference keys.
      $data_diff_size = count($data_diff);
      $duration_ranges_to_diff_keys = array_map(function($duration_key) use ($data_diff_size) {
        return ($duration_key * ($data_diff_size + 1) / 100.0) - 1;
      }, $this->durationRanges);

      // Add hack to process last range, duplicate last element of the array.
      $dur_diff_keys_size = count($duration_ranges_to_diff_keys);
      if ($dur_diff_keys_size > 0) {
        array_push($duration_ranges_to_diff_keys, $duration_ranges_to_diff_keys[$dur_diff_keys_size - 1]);

        // Update size of "duration ranges to diff keys" due to Hack.
        $dur_diff_keys_size = count($duration_ranges_to_diff_keys);
      }

      // Calc indicators - Get keys between the duration ranges.
      foreach ($duration_ranges_to_diff_keys as $dur_diff_key => $dur_diff_value) {
        // Accumulators.
        $excess_accumulator = 0;
        $deficit_accumulator = 0;
        $total_reference_sum = 0;

        // Calc excess and deficiency.
        if ($dur_diff_key == 0) {
          $upper_bound = (int) floor($dur_diff_value) + 1;
          $lower_bound = 0;
          for ($y = $lower_bound; $y < $upper_bound; $y++) {
            // Sum Excess.
            if ($data_diff[$y] >= 0) {
              $excess_accumulator += $data_diff[$y];
              // Sum deficit.
            }
            else {

              $deficit_accumulator += $data_diff[$y];
            }
            $total_reference_sum += $this->refRidData[$month + 1][$y];
          }
        }
        elseif ($dur_diff_key == ($dur_diff_keys_size - 1)) {
          $upper_bound = $data_diff_size;
          $lower_bound = (int) ceil($dur_diff_value);
          for ($y = $lower_bound; $y < $upper_bound; $y++) {
            // Sum Excess.
            if ($data_diff[$y] >= 0) {
              $excess_accumulator += $data_diff[$y];
              // Sum deficit.
            }
            else {

              $deficit_accumulator += $data_diff[$y];
            }
            $total_reference_sum += $this->refRidData[$month + 1][$y];
          }
        }
        else {
          $upper_bound = (int) floor($dur_diff_value) + 1;
          $lower_bound = (int) ceil($duration_ranges_to_diff_keys[$dur_diff_key - 1]);
          for ($y = $lower_bound; $y < $upper_bound; $y++) {
            // Sum Excess.
            if ($data_diff[$y] >= 0) {
              $excess_accumulator += $data_diff[$y];
              // Sum deficit.
            }
            else {

              $deficit_accumulator += $data_diff[$y];
            }
            $total_reference_sum += $this->refRidData[$month + 1][$y];
          }
        }

        // Normalize indicators.
        $excess_indicator = ($total_reference_sum != 0) ? $excess_accumulator / $total_reference_sum : 0;
        $deficit_indicator = abs((($total_reference_sum != 0) ? $deficit_accumulator / $total_reference_sum : 0));

        // Set excess and deficit indicators results.
        array_push($this->context['analysis_results'][$this->id]['data']['excess_indicator'][$month + 1], $excess_indicator);
        array_push($this->context['analysis_results'][$this->id]['data']['deficit_indicator'][$month + 1], $deficit_indicator);
      }
    }

    // Test result
    // $test_result = $this->context['analysis_results'][$this->id]['data'];.
  }

  /**
   * To clean string of special characters and turn to lower case.
   *
   * @param string $text
   *    The text to clean.
   * @param string $replacement
   *    The text to clean.
   *
   * @return string
   *    The clean text.
   */
  protected function cleanString($text, $replacement = '') {
    return strtolower(preg_replace('([^A-Za-z0-9])', $replacement, $text));
  }

  /**
   * To convert text percentage to decimal.
   *
   * @param string $percentage
   *    The percentage to convert.
   *
   * @return float|int
   *    The decimal number.
   */
  protected function percentageToFloat($percentage) {
    return floatval(str_replace('%', '', $percentage)) / 100.;
  }

  /**
   * Obtain the indexes from outputs variables.
   */
  protected function processQecoTable() {
    // Set filter keys.
    $cleaned_duration_string = $this->cleanString("Duracion");
    $cleaned_regime_type_string = $this->cleanString($this->regimeType);
    $cleaned_elevation_type_string = $this->cleanString($this->elevationType);

    // Filter Q Eco Table.
    $this->filteredQecoTable = array_filter($this->qEcoTable, function ($item_array) use ($cleaned_duration_string, $cleaned_regime_type_string, $cleaned_elevation_type_string) {
      // Filter by elevation.
      if ($this->cleanString($item_array['tipo_de_rio_elevacion']) == $cleaned_elevation_type_string) {
        // Filter by regime.
        if ($this->cleanString($item_array['tipo_de_regimen']) == $cleaned_regime_type_string) {
          // Filter by flow attribute.
          if ($this->cleanString($item_array['atributo_caudal']) == $cleaned_duration_string) {
            return TRUE;
          }
        }
      }
      return FALSE;
    });
  }

  /**
   * To convert text percentage to decimal.
   *
   * @param int $indicator_type
   *    The percentage to convert.
   * @param int $analysis_month
   *    The percentage to convert.
   * @param int $flow_component_type
   *    The percentage to convert.
   *
   * @return array
   *    Array containing Eco Table filtered data rows.
   */
  protected function filterEcoTable($indicator_type, $analysis_month, $flow_component_type) {
    // Set filter keys - Indicator string search array.
    if ($indicator_type == self::ELOHA_EXCESS) {
      $indicator_search_array = self::ELOHA_EXCESS_SEARCH_ARRAY_STRING;
    }
    elseif ($indicator_type == self::ELOHA_DEFICIT) {
      $indicator_search_array = self::ELOHA_DEFICIT_SEARCH_ARRAY_STRING;
    }
    else {
      $indicator_search_array = self::ELOHA_EXCESS_SEARCH_ARRAY_STRING;
    }

    // Set filter keys - Flow Component string search array.
    if ($flow_component_type == self::ELOHA_HIGH_FLOW) {
      $flow_component_search_array = self::ELOHA_HIGH_FLOW_SEARCH_ARRAY_STRING;
    }
    elseif ($flow_component_type == self::ELOHA_MEDIUM_FLOW) {
      $flow_component_search_array = self::ELOHA_MEDIUM_FLOW_SEARCH_ARRAY_STRING;
    }
    elseif ($flow_component_type == self::ELOHA_LOW_FLOW) {
      $flow_component_search_array = self::ELOHA_LOW_FLOW_SEARCH_ARRAY_STRING;
    }
    elseif ($flow_component_type == self::ELOHA_MINIMUM_FLOW) {
      $flow_component_search_array = self::ELOHA_MINIMUM_FLOW_SEARCH_ARRAY_STRING;
    }
    else {
      $flow_component_search_array = self::ELOHA_HIGH_FLOW_SEARCH_ARRAY_STRING;
    }

    // Filter Q Eco Table.
    $result_array = array_filter($this->filteredQecoTable, function ($item_array) use ($indicator_search_array, $analysis_month, $flow_component_search_array) {
      // Filter by indicator type.
      foreach ($indicator_search_array as $string_indicator) {
        if (strpos($this->cleanString($item_array['exceso_deficit']), $this->cleanString($string_indicator)) !== FALSE) {
          // Filter by Flow Component.
          foreach ($flow_component_search_array as $string_flow_component) {
            if (strpos($this->cleanString($item_array['componente_flujo']), $this->cleanString($string_flow_component)) !== FALSE) {
              // Filter by Month.
              $init_month = intval($item_array['mes_inicio']);
              $final_month = intval($item_array['mes_fin']);
              $check_month = intval($analysis_month);
              if ($init_month < $final_month) {
                if (($init_month <= $check_month) && ($check_month <= $final_month)) {
                  return TRUE;
                }
              }
              elseif ($init_month > $final_month) {
                $check_month_updated = $init_month > $check_month ? ($check_month + 12) : $check_month;
                $final_month_updated = $final_month + 12;
                if (($init_month <= $check_month_updated) && ($check_month_updated <= $final_month_updated)) {
                  return TRUE;
                }
              }
              elseif ($init_month == $final_month) {
                if ($init_month == $check_month) {
                  return TRUE;
                }
              }
              return FALSE;
            }
          }
          return FALSE;
        }
      }
      return FALSE;
    });

    // Return results.
    return $result_array;
  }

  /**
   * Obtain the indexes from outputs variables.
   */
  protected function processIndexesAndImpactsAndVerdict() {
    $total_number_critical = 0;
    $total_number_alert = 0;

    // Calc of the excess indexes and impacts.
    foreach ($this->context['analysis_results'][$this->id]['data']['excess_indicator'] as $month => $indicator_array) {
      // Check impact by month array exist.
      if (!isset($this->context['analysis_results'][$this->id]['data']['impact'][$month])) {
        $this->context['analysis_results'][$this->id]['data']['impact'][$month] = array();
      }

      // Calc index per each indicator on flow component.
      foreach ($indicator_array as $flow_component => $indicator_value) {
        // Intermediate calculated indexes container.
        $calculated_index_array = array();

        // Get filtered Q Eco Table data.
        $filtered_qecotable_array = $this->filterEcoTable(self::ELOHA_EXCESS, $month, $flow_component);

        // Check impact by flow component exist.
        if (!isset($this->context['analysis_results'][$this->id]['data']['impact'][$month][self::ELOHA_CRITICAL_ALTERATION])) {
          $this->context['analysis_results'][$this->id]['data']['impact'][$month][self::ELOHA_CRITICAL_ALTERATION] = array();
        }
        if (!isset($this->context['analysis_results'][$this->id]['data']['impact'][$month][self::ELOHA_ALERT_ALTERATION])) {
          $this->context['analysis_results'][$this->id]['data']['impact'][$month][self::ELOHA_ALERT_ALTERATION] = array();
        }

        // Calc index by each filtered row found in Q Eco Table.
        foreach ($filtered_qecotable_array as $qecotable_row) {
          // Calc index due to Q Eco Table.
          if ($this->percentageToFloat($qecotable_row['umbral_cr_tico']) <= $indicator_value) {
            // Critical Index - 2.
            $calculated_index = self::ELOHA_INDEX_CRITICAL;
            array_push($calculated_index_array, $calculated_index);
          }
          elseif ($this->percentageToFloat($qecotable_row['umbral_alerta']) <= $indicator_value) {
            // Alert Index - 1.
            $calculated_index = self::ELOHA_INDEX_ALERT;
            array_push($calculated_index_array, $calculated_index);
          }
          else {
            // Normal Index - 0.
            $calculated_index = self::ELOHA_INDEX_NORMAL;
            array_push($calculated_index_array, $calculated_index);
          }

          // Set impact due to the calculated final index.
          if ($calculated_index != self::ELOHA_INDEX_NORMAL) {
            // Critical.
            if ($calculated_index == self::ELOHA_INDEX_CRITICAL) {
              $alteration_type = self::ELOHA_CRITICAL_ALTERATION;
              // Alert.
            }
            else {

              $alteration_type = self::ELOHA_ALERT_ALTERATION;
            }

            // Set impact string.
            if (!in_array(utf8_encode($qecotable_row['relaci_n_atributo_variable_respuesta-caudal']), $this->context['analysis_results'][$this->id]['data']['impact'][$month][$alteration_type])) {
              array_push($this->context['analysis_results'][$this->id]['data']['impact'][$month][$alteration_type], utf8_encode($qecotable_row['relaci_n_atributo_variable_respuesta-caudal']));
              $b = $this->context['analysis_results'][$this->id]['data']['impact'][$month][$alteration_type];
            }
          }
        }

        // Set final index as the greatest per flow component and per month.
        if (!isset($this->context['analysis_results'][$this->id]['data']['excess_index'][$month])) {
          $this->context['analysis_results'][$this->id]['data']['excess_index'][$month] = array();
        }
        if (count($calculated_index_array) > 0) {
          $max_calculated_index = max($calculated_index_array);

          // Set counter for verdict.
          if (self::ELOHA_INDEX_CRITICAL <= $max_calculated_index) {
            $total_number_critical++;
          }
          elseif (self::ELOHA_INDEX_ALERT <= $max_calculated_index) {
            $total_number_alert++;
          }
        }
        else {
          $max_calculated_index = 0;
        }
        $this->context['analysis_results'][$this->id]['data']['excess_index'][$month][$flow_component] = $max_calculated_index;
      }
    }

    // Calc of the deficit indexes and impacts.
    foreach ($this->context['analysis_results'][$this->id]['data']['deficit_indicator'] as $month => $indicator_array) {
      // Check impact by month exist.
      if (!isset($this->context['analysis_results'][$this->id]['data']['impact'][$month])) {
        $this->context['analysis_results'][$this->id]['data']['impact'][$month] = array();
      }

      // Calc index per each indicator on flow component.
      foreach ($indicator_array as $flow_component => $indicator_value) {
        // Intermediate calculated indexes container.
        $calculated_index_array = array();

        // Get filtered Q Eco Table data.
        $filtered_qecotable_array = $this->filterEcoTable(self::ELOHA_DEFICIT, $month, $flow_component);

        // Check impact by flow component exist.
        if (!isset($this->context['analysis_results'][$this->id]['data']['impact'][$month][self::ELOHA_CRITICAL_ALTERATION])) {
          $this->context['analysis_results'][$this->id]['data']['impact'][$month][self::ELOHA_CRITICAL_ALTERATION] = array();
        }
        if (!isset($this->context['analysis_results'][$this->id]['data']['impact'][$month][self::ELOHA_ALERT_ALTERATION])) {
          $this->context['analysis_results'][$this->id]['data']['impact'][$month][self::ELOHA_ALERT_ALTERATION] = array();
        }

        // Calc index by each filtered row found in Q Eco Table.
        foreach ($filtered_qecotable_array as $qecotable_row) {
          // Calc index due to Q Eco Table.
          if ($this->percentageToFloat($qecotable_row['umbral_cr_tico']) <= $indicator_value) {
            // Critical Index - 2.
            $calculated_index = self::ELOHA_INDEX_CRITICAL;
            array_push($calculated_index_array, $calculated_index);
          }
          elseif ($this->percentageToFloat($qecotable_row['umbral_alerta']) <= $indicator_value) {
            // Alert Index - 1.
            $calculated_index = self::ELOHA_INDEX_ALERT;
            array_push($calculated_index_array, $calculated_index);
          }
          else {
            // Normal Index - 0.
            $calculated_index = self::ELOHA_INDEX_NORMAL;
            array_push($calculated_index_array, $calculated_index);
          }

          // Set impact due to the calculated final index.
          if ($calculated_index != self::ELOHA_INDEX_NORMAL) {
            // Critical.
            if ($calculated_index == self::ELOHA_INDEX_CRITICAL) {
              $alteration_type = self::ELOHA_CRITICAL_ALTERATION;
              // Alert.
            }
            else {

              $alteration_type = self::ELOHA_ALERT_ALTERATION;
            }

            // Set impact string.
            if (!in_array(utf8_encode($qecotable_row['relaci_n_atributo_variable_respuesta-caudal']), $this->context['analysis_results'][$this->id]['data']['impact'][$month][$alteration_type])) {
              array_push($this->context['analysis_results'][$this->id]['data']['impact'][$month][$alteration_type], utf8_encode($qecotable_row['relaci_n_atributo_variable_respuesta-caudal']));
              $b = $this->context['analysis_results'][$this->id]['data']['impact'][$month][$alteration_type];
            }
          }
        }

        // Set final index as the greatest per month.
        if (!isset($this->context['analysis_results'][$this->id]['data']['deficit_index'][$month])) {
          $this->context['analysis_results'][$this->id]['data']['deficit_index'][$month] = array();
        }
        if (count($calculated_index_array) > 0) {
          $max_calculated_index = max($calculated_index_array);

          // Set counter for verdict.
          if (self::ELOHA_INDEX_CRITICAL <= $max_calculated_index) {
            $total_number_critical++;
          }
          elseif (self::ELOHA_INDEX_ALERT <= $max_calculated_index) {
            $total_number_alert++;
          }
        }
        else {
          $max_calculated_index = 0;
        }
        $this->context['analysis_results'][$this->id]['data']['deficit_index'][$month][$flow_component] = $max_calculated_index;
      }
    }

    // Calc verdict for the total branch.
    // it sets month to zero cause is month independent.
    $this->context['analysis_results'][$this->id]['data']['verdict']['0'] = $this->getWs() * $total_number_alert + $this->getWg() * $total_number_critical;
  }

}
