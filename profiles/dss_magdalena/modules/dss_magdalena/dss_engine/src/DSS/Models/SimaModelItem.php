<?php
/**
 * @file
 * Implements the abstract class for a Model Item.
 */

namespace Drupal\dss_magdalena\DSS\Models;

use Drupal\dss_magdalena\DSS\Entity\SimaCaseStudy;
use Drupal\dss_magdalena\DSS\Entity\SimaResource;

/**
 * Class SimaModelItem.
 *
 * @package Drupal\dss_magdalena\DSS\Models
 */
abstract class SimaModelItem extends \ArrayObject {

  /**
   * Minimum time in seconds we can be sure this item exists in the cache.
   *
   * Timout = 3 hours by default.
   */
  const TIMEOUT = 10800;

  /**
   * This item's identifier (label).
   *
   * @var string
   */
  protected $id;

  /**
   * The Case Study ID.
   *
   * @var int
   */
  protected $cid;

  /**
   * The Context ID.
   *
   * @var string
   */
  protected $contextId;

  /**
   * Model Input Variables Machine Name List.
   *
   * @var array
   */
  protected $inputVariablesMachineNameList;

  /**
   * The local item data.
   *
   * @var array
   */
  protected $data = [];

  /**
   * Public Constructor.
   */
  public function __construct() {
    $this->setFlags(\ArrayObject::ARRAY_AS_PROPS);
  }

  /**
   * Sets this item's identifier or label.
   *
   * @param string $label
   *   The Label to assign to this item.
   */
  public function setId($label) {
    $this->id = $label;
  }

  /**
   * Returns this item's label.
   *
   * @return string
   *   The label assigned to this item.
   */
  public function label() {
    return $this->id;
  }

  /**
   * Sets the Case Study ID.
   *
   * @param int $cid
   *   The Case Study ID.
   */
  public function setCaseStudyId($cid) {
    $this->cid = $cid;
  }

  /**
   * Returns the Case Study ID.
   *
   * @return int
   *   The Case Study ID.
   */
  public function getCaseStudyId() {
    return $this->cid;
  }

  /**
   * Returns the loaded Case Study.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaCaseStudy|FALSE
   *   The loaded Case Study.
   */
  public function getCaseStudy() {
    return SimaCaseStudy::load($this->cid);
  }

  /**
   * List of datasets to load as input variables.
   *
   * @param array $datasets
   *   The list of datasets to load as input variables.
   */
  public function setInputVariablesMachineNameList(array $datasets) {
    $this->inputVariablesMachineNameList = $datasets;
  }

  /**
   * Obtains the list of loaded input variables.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaResource[]
   *   A list of loaded input variables.
   */
  public function getInputVariables() {
    $input_variables = array();
    if (empty($this->cid)) {
      return [];
    }
    if ($case = SimaCaseStudy::load($this->cid)) {
      foreach ($this->inputVariablesMachineNameList as $var_machine_name) {
        $rid = $case->getCaseOutputs()->checkWeapOutputExistsForDataset($var_machine_name);
        if ($rid) {
          $input_variables[] = SimaResource::load($rid);
        }
        else {
          $input_variables[] = SimaResource::loadByCaseStudyAndDataset($this->cid, $var_machine_name);
        }
      }
    }
    return $input_variables;
  }

  /**
   * Sets the local data for this item.
   *
   * @param array $data
   *   The local data array.
   *
   * @return $this
   *   This item.
   */
  public function setData(array $data) {
    $this->data = $data;
    return $this;
  }

  /**
   * The local item's data.
   *
   * @return array
   *   The data array.
   */
  public function getData() {
    return $this->data;
  }

  /**
   * Checks whether this is a Resource Type Model Item.
   *
   * This is an item whose unique purpose is to create resources.
   *
   * @return bool
   *   TRUE if it is a Resource Type Model item, FALSE otherwise.
   */
  public function isResourceType() {
    return FALSE;
  }

  /**
   * Sets the Context ID.
   *
   * @param string $context_id
   *   The Context ID.
   */
  public function setContextId($context_id) {
    $this->contextId = $context_id;
  }

  /**
   * Obtains the Model's context.
   *
   * @return array|bool
   *   The model context if exists, FALSE otherwise.
   */
  protected function loadContext() {
    if ($context = cache_get($this->contextId)) {
      if (isset($context->data)) {
        return $context->data;
      }
    }
    return FALSE;
  }

  /**
   * Saves the Model Context Data.
   *
   * @param array $context
   *   The Context Data.
   */
  protected function saveContext($context = []) {
    $expiration = time() + self::TIMEOUT;
    cache_set($this->contextId, $context, 'cache', $expiration);
  }

}
