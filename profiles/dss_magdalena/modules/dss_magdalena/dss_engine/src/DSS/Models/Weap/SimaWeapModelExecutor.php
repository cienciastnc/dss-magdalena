<?php
/**
 * @file
 * Model executor for WEAP.
 */

namespace Drupal\dss_magdalena\DSS\Models\Weap;

use Drupal\dss_magdalena\DSS\Models\SimaModelExecutorInterface;
use Drupal\dss_magdalena\DSS\Models\SimaModelExecutor;
use Drupal\dss_magdalena\DSS\Entity\SimaModel;

/**
 * Class SimaWeapModelExecutor.
 *
 * @package Drupal\dss_magdalena\DSS\Models\Weap
 */
class SimaWeapModelExecutor extends SimaModelExecutor implements SimaModelExecutorInterface {

  /**
   * {@inheritdoc}
   */
  public function initialize() {
    // Set and Save Context.
    $this->setContext([
      'cid' => $this->getCaseStudy()->getId(),
      'process_datastore' => $this->getProcessDatastore(),
    ]);
    $this->saveContext();

  }

  /**
   * {@inheritdoc}
   */
  public function saveOutputVariables() {
    $this->execute();

  }

  /**
   * {@inheritdoc}
   */
  public function getSummary($results) {

  }

  /**
   * Executes the Case Study.
   */
  protected function execute() {
    $case = $this->getCaseStudy();

    // Export Case Study.
    $success = $case->export();
    if ($success) {
      // Set modeling status as "101|PROCESSING".
      $case->setModelingStatus(SimaModel::MODEL_WEAP, SimaModel::STATUS_PROCESSING);
    }
  }

}
