<?php
/**
 * @file
 * Implements an item for Territorial Footprint model.
 */

namespace Drupal\dss_magdalena\DSS\Models\TerritorialFootprint;

use Drupal\dss_magdalena\DSS\Entity\File\SimaTerritorialFootprintFile;
use Drupal\dss_magdalena\DSS\Models\SimaModelItem;
use Drupal\dss_magdalena\DSS\Models\SimaModelItemInterface;
use Drupal\dss_magdalena\DSS\SimaWeapIndex;
use Drupal\dss_magdalena\DSS\Entity\SimaResource;

/**
 * Class SimaDorHModelItem.
 *
 * @package Drupal\dss_magdalena\DSS\Models\DorH
 */
class SimaTerritorialFootprintModelItem extends SimaModelItem implements SimaModelItemInterface {

  /**
   * List with precalculated values for this model.
   *
   * @var array analyticalFootprintProjectedReservoirsData
   */
  protected $analyticalFootprintProjectedReservoirsData = [];
  protected $calculatedReservoirs = [];
  protected $outputVariablesMachineNameList = [];
  protected $outputVariables;

  /**
   * A list of calculated results values for this model.
   *
   * @var array result
   */
  protected $result = [];
  /**
   * {@inheritdoc}
   */
  public function execute() {
    $context = $this->loadContext();
    $this->outputVariablesMachineNameList = $context['output_variables_machine_name_list'];
    $this->generateOutputData();
  }
  /**
   * Load Analytical Footprint Projected Reservoirs Data.
   */
  protected function loadTerritorialFootprintData() {
    $this->analyticalFootprintProjectedReservoirsData = SimaTerritorialFootprintFile::loadFromSystem()->getData();
    $this->calculatedReservoirs = array_unique(array_column($this->analyticalFootprintProjectedReservoirsData, 'branch'));
  }

  /**
   * Calling functions to generate outputs.
   */
  public function generateOutputData() {
    $context = $this->loadContext();
    $this->input_variables_machine_name_list = $context['input_variables_machine_name_list'];
    $this->output_variables_machine_name_list = $context['output_variables_machine_name_list'];
    $this->loadTerritorialFootprintData();
    $this->calculateResultForAllReservoirs();
    $this->fillOutputVariables();
    $context['output_variables'] = $this->outputVariables;
    $this->saveContext($context);
  }

  /**
   * Fill output variables.
   */
  public function fillOutputVariables() {
    // Header.
    $header = [
      'BranchID', 'GeoBranchId', 'Year', 'Timestep', 'Value',
    ];
    $this->outputVariables[$this->outputVariablesMachineNameList[0]][] = $header;
    $this->outputVariables[$this->outputVariablesMachineNameList[1]][] = $header;
    $this->outputVariables[$this->outputVariablesMachineNameList[2]][] = $header;

    // Data.
    foreach ($this->result as $row) {
      // Superficie inundable de embalses.
      $value = json_encode(array(
        $row['project_id'],
        $row['area_m2'],
      ), JSON_FORCE_OBJECT);
      $this->outputVariables[$this->outputVariablesMachineNameList[0]][] = [
        $row['BranchID'],
        $row['GeoBranchID'],
        '',
        '',
        $value,
      ];

      // Huella Territorial por zonas de interes.
      $value = json_encode(array(
        $row['project_id'],
        $row['cob_agricolas_cultivo_m2'],
        $row['cob_agricolas_pastos_m2'],
        $row['area_Compensacion_m2'],
        $row['microfocalizacion_m2'],
        $row['paramos_siac_m2'],
        $row['parques_naturales_regionales_m2'],
        $row['titulos_mineros_m2'],
        $row['bosque_seco_m2'],
        $row['comunidades_negras_m2'],
        $row['humedales_siac_m2'],
        $row['parques_nacionales_m2'],
        $row['poblacion_hab'],
        $row['reserva_forestal_protectora_nacional_m2'],
        $row['reserva_forestal_protectora_regional_m2'],
        $row['reservas_ley2da_m2'],
        $row['resguardos_indigenas_m2'],
      ), JSON_FORCE_OBJECT);
      $this->outputVariables[$this->outputVariablesMachineNameList[1]][] = [
        $row['BranchID'],
        $row['GeoBranchID'],
        '',
        '',
        $value,
      ];

      // Huella Territorial por categoria.
      $value = json_encode(array(
        $row['project_id'],
        $row['a_economica'],
        $row['a_ambiental'],
        $row['a_social'],
        $row['poblacion'],
      ), JSON_FORCE_OBJECT);
      $this->outputVariables[$this->outputVariablesMachineNameList[2]][] = [
        $row['BranchID'],
        $row['GeoBranchID'],
        '',
        '',
        $value,
      ];
    }
  }

  /**
   * Calcula data for all reservoirs.
   */
  public function calculateResultForAllReservoirs() {
    // Generate output data for this model.
    $reservoirs = $this->getReservoirs();
    foreach ($reservoirs as $reservoir) {
      if (((float) $reservoir['Value'] > 0) && (in_array($reservoir['Level4'], $this->calculatedReservoirs))) {
        if ($this->calculateResultForReservoir($reservoir) != FALSE) {
          $this->result[] = $this->calculateResultForReservoir($reservoir);
        }
      }
    }
  }

  /**
   * Calculates data for a reservoir.
   *
   * @param array $reservoir
   *   Reservoir properties values to calculate interpolation.
   *
   * @return array|bool
   *   Reservoir with calculated values or false.
   */
  public function calculateResultForReservoir(array $reservoir) {
    $numrows = count($this->analyticalFootprintProjectedReservoirsData);
    $interpolate = FALSE;
    $i = 1;
    for (; $i < $numrows; $i++) {
      if ($this->analyticalFootprintProjectedReservoirsData[$i]['branch'] == $reservoir['Level4']) {
        break;
      }
    }

    if ($i < $numrows) {
      $item_prev = $this->analyticalFootprintProjectedReservoirsData[$i];
      for (; $i < $numrows; $i++) {
        if (($reservoir['Value'] * 1000000 > $item_prev['volumen_m3']) &&
          ($reservoir['Value'] * 1000000 <= $this->analyticalFootprintProjectedReservoirsData[$i]['volumen_m3'])
        ) {
          $interpolate = TRUE;
          $item = $this->analyticalFootprintProjectedReservoirsData[$i];
          break;
        }
        if ($this->analyticalFootprintProjectedReservoirsData[$i]['branch'] != $reservoir['Level4']) {
          break;
        }
        $item_prev = $this->analyticalFootprintProjectedReservoirsData[$i];
      }

      if (!$interpolate) {
        $item_prev = $this->analyticalFootprintProjectedReservoirsData[$i - 2];
        $item = $this->analyticalFootprintProjectedReservoirsData[$i - 1];
      }
      return $this->interpolateReservoir($item_prev, $item, $reservoir);
    }
    return FALSE;
  }

  /**
   * Interpolae a reservoir properties.
   *
   * @param array $item_prev
   *   Item 1 values.
   * @param array $item
   *   Item 2 values.
   * @param array $reservoir
   *   Resevoir value.
   *
   * @return mixed
   *   A reservoir with calculated data for this model.
   */
  public function interpolateReservoir($item_prev, $item, $reservoir) {
    $result = $reservoir;
    $result['BranchID'] = $reservoir['BranchID'];
    $result['GeoBranchID'] = $reservoir['GeoBranchID'];
    $result['project_id'] = $item_prev['project_id'];
    $result['area_m2'] = $this->interpolate($item_prev['volumen_m3'], $item_prev['area_m2'], $item['volumen_m3'], $item['area_m2'], $reservoir['Value'] * 1000000);
    $result['cob_agricolas_cultivo_m2'] = $this->interpolate($item_prev['volumen_m3'], $item_prev['cob_agricolas_cultivo_m2'], $item['volumen_m3'], $item['cob_agricolas_cultivo_m2'], $reservoir['Value'] * 1000000);
    $result['cob_agricolas_pastos_m2'] = $this->interpolate($item_prev['volumen_m3'], $item_prev['cob_agricolas_pastos_m2'], $item['volumen_m3'], $item['cob_agricolas_pastos_m2'], $reservoir['Value'] * 1000000);
    $result['area_Compensacion_m2'] = $this->interpolate($item_prev['volumen_m3'], $item_prev['area_Compensacion_m2'], $item['volumen_m3'], $item['area_Compensacion_m2'], $reservoir['Value'] * 1000000);
    $result['microfocalizacion_m2'] = $this->interpolate($item_prev['volumen_m3'], $item_prev['microfocalizacion_m2'], $item['volumen_m3'], $item['microfocalizacion_m2'], $reservoir['Value'] * 1000000);
    $result['paramos_siac_m2'] = $this->interpolate($item_prev['volumen_m3'], $item_prev['paramos_siac_m2'], $item['volumen_m3'], $item['paramos_siac_m2'], $reservoir['Value'] * 1000000);
    $result['parques_naturales_regionales_m2'] = $this->interpolate($item_prev['volumen_m3'], $item_prev['parques_naturales_regionales_m2'], $item['volumen_m3'], $item['parques_naturales_regionales_m2'], $reservoir['Value'] * 1000000);
    $result['titulos_mineros_m2'] = $this->interpolate($item_prev['volumen_m3'], $item_prev['titulos_mineros_m2'], $item['volumen_m3'], $item['titulos_mineros_m2'], $reservoir['Value'] * 1000000);
    $result['bosque_seco_m2'] = $this->interpolate($item_prev['volumen_m3'], $item_prev['bosque_seco_m2'], $item['volumen_m3'], $item['bosque_seco_m2'], $reservoir['Value'] * 1000000);
    $result['comunidades_negras_m2'] = $this->interpolate($item_prev['volumen_m3'], $item_prev['comunidades_negras_m2'], $item['volumen_m3'], $item['comunidades_negras_m2'], $reservoir['Value'] * 1000000);
    $result['humedales_siac_m2'] = $this->interpolate($item_prev['volumen_m3'], $item_prev['humedales_siac_m2'], $item['volumen_m3'], $item['humedales_siac_m2'], $reservoir['Value'] * 1000000);
    $result['parques_nacionales_m2'] = $this->interpolate($item_prev['volumen_m3'], $item_prev['parques_nacionales_m2'], $item['volumen_m3'], $item['parques_nacionales_m2'], $reservoir['Value'] * 1000000);
    $result['poblacion_hab'] = $this->interpolate($item_prev['volumen_m3'], $item_prev['poblacion_hab'], $item['volumen_m3'], $item['poblacion_hab'], $reservoir['Value'] * 1000000);
    $result['reserva_forestal_protectora_nacional_m2'] = $this->interpolate($item_prev['volumen_m3'], $item_prev['reserva_forestal_protectora_nacional_m2'], $item['volumen_m3'], $item['reserva_forestal_protectora_nacional_m2'], $reservoir['Value'] * 1000000);
    $result['reserva_forestal_protectora_regional_m2'] = $this->interpolate($item_prev['volumen_m3'], $item_prev['reserva_forestal_protectora_regional_m2'], $item['volumen_m3'], $item['reserva_forestal_protectora_regional_m2'], $reservoir['Value'] * 1000000);
    $result['reservas_ley2da_m2'] = $this->interpolate($item_prev['volumen_m3'], $item_prev['reservas_ley2da_m2'], $item['volumen_m3'], $item['reservas_ley2da_m2'], $reservoir['Value'] * 1000000);
    $result['resguardos_indigenas_m2'] = $this->interpolate($item_prev['volumen_m3'], $item_prev['resguardos_indigenas_m2'], $item['volumen_m3'], $item['resguardos_indigenas_m2'], $reservoir['Value'] * 1000000);
    $result['a_economica'] = $this->interpolate($item_prev['volumen_m3'], $item_prev['a_economica'], $item['volumen_m3'], $item['a_economica'], $reservoir['Value'] * 1000000);
    $result['a_ambiental'] = $this->interpolate($item_prev['volumen_m3'], $item_prev['a_ambiental'], $item['volumen_m3'], $item['a_ambiental'], $reservoir['Value'] * 1000000);
    $result['a_social'] = $this->interpolate($item_prev['volumen_m3'], $item_prev['a_social'], $item['volumen_m3'], $item['a_social'], $reservoir['Value'] * 1000000);
    $result['poblacion'] = $this->interpolate($item_prev['volumen_m3'], $item_prev['poblacion'], $item['volumen_m3'], $item['poblacion'], $reservoir['Value'] * 1000000);
    return $result;
  }

  /**
   * Interpolate function.
   *
   * @param float $x1
   *   Parameter to interpolate x1.
   * @param float $y1
   *   Parameter to interpolate y1.
   * @param float $x2
   *   Parameter to interpolate x2.
   * @param float $y2
   *   Parameter to interpolate y2.
   * @param float $x
   *   Parameter to interpolate x.
   *
   * @return float
   *   Return y, interpolation result.
   */
  public function interpolate($x1, $y1, $x2, $y2, $x) {
    $result = $y1 + ($x - $x1) * ($y2 - $y1) / ($x2 - $x1);
    return $result;
  }

  /**
   * Extract branch and value of input variable.
   *
   * @return array
   *   An array with branch as key and value.
   */
  public function getReservoirs() {

    $input_variable = SimaResource::loadByCaseStudyAndDataset($this->cid, $this->inputVariablesMachineNameList[0]);
    $data = $input_variable->getDataCsv();
    $weap = SimaWeapIndex::loadAllRecords();
    $weap_data = $weap->getValues();
    // $data = $weap->combineWith($data, 'branch_id');.
    $branch_values = [];
    $reservoirs = [];
    next($data);
    foreach ($data as $item) {
      $branch = '';
      $branch_id = $item['BranchID'];
      // 'If' only first element.
      if ($item[$branch_id] != 'BranchID') {
        $branch = $weap_data[$branch_id]['level1'];
        if (!empty($weap_data[$branch_id]['level2'])) {
          $branch .= '\\' . $weap_data[$branch_id]['level2'];
        }
        if (!empty($weap_data[$branch_id]['level3'])) {
          $branch .= '\\' . $weap_data[$branch_id]['level3'];
        }
        if (!empty($weap_data[$branch_id]['level4'])) {
          $branch .= '\\' . $weap_data[$branch_id]['level4'];
        }
        $branch .= empty($weap_data[$branch_id]['level1']) ? $weap_data[$branch_id]['level1'] : '';
        $branch_values[$branch] = !empty($item['Value']) ? $item['Value'] : 0;

        $reservoirs[$branch_id] = $item;
        $reservoirs[$branch_id]['Branch'] = $branch;
        $reservoirs[$branch_id]['Level1'] = $weap_data[$branch_id]['level1'];
        $reservoirs[$branch_id]['Level2'] = $weap_data[$branch_id]['level2'];
        $reservoirs[$branch_id]['Level3'] = $weap_data[$branch_id]['level3'];
        $reservoirs[$branch_id]['Level4'] = $weap_data[$branch_id]['level4'];
        $reservoirs[$branch_id]['Value']  = !empty($item['Value']) ? $item['Value'] : 0;

      }
    }
    return $reservoirs;
  }

}
