<?php
/**
 * @file
 * Implements a Network Topology Model.
 */

namespace Drupal\dss_magdalena\DSS\Models\Network;

use Drupal\dss_magdalena\DSS\Entity\File\SimaStreamslineTopologyFile;
/**
 * SimaNetwork Class.
 */
class SimaNetwork {

  protected $network = [];
  protected $reservoirs = [];
  protected $startArcidDownstream = 11;
  protected $simaStreamslineTopologyData;

  /**
   * Load data from streamslineTopology File.
   *
   * @return array
   *   StreamslineTopology data.
   */
  public function readNetworkTopologyFile() {
    return SimaStreamslineTopologyFile::loadFromSystem()->getData();
  }

  /**
   * Load reservoirs to array data.
   *
   * @param array $reservoir_data
   *   Barrier data.
   */
  public function setReservoirData(array $reservoir_data = []) {
    foreach ($reservoir_data as $item) {
      $arc_id = (int) $item['arcid'];
      $project_id = (int) $item['project_id'];
      $storage_capacity = (float) $item['storage_capacity'];
      $sima_case_status = (int) $item['sima_case_status'];
      $dam_height = $item['dam_height'];
      $weap_branch = $item['weap_branch'];

      $reservoir = new SimaReservoir();
      $reservoir->setArcId($arc_id);
      $reservoir->setProjectId($project_id);
      $reservoir->setStorageCapacity($storage_capacity);
      $reservoir->setSimaCaseStatus($sima_case_status);
      $reservoir->setDamHeight($dam_height);
      $reservoir->setWeapBranch($weap_branch);

      $this->reservoirs[$arc_id] = $reservoir;
    }
  }

  /**
   * Load nodes to network.
   */
  public function setNetworkTopology() {
    $network_items = $this->readNetworkTopologyFile();
    foreach ($network_items as $item) {
      // Determine the arc properties.
      $arc_id = (int) $item['arcid'];
      $from_node = (int) $item['from_node'];
      $to_node = (int) $item['to_node'];
      $q_med = (float) $item['q_med_m3s'];
      $grid_code = (int) $item['grid_code'];
      $length = (float) $item['length_km'];
      $aporte_sss_med = (float) $item['aporte_qss_med'];
      $reservoir_storage_capacity = (float) $item['capacidad_del_embalse'];
      $reservoir_flow = (float) $item['caudal_en_cada_embalse'];
      $retention_rate = (float) $item['efficiencia_atrapamiento'];
      $cumulative_sst  = (float) $item['cumulative_sst'];

      if ($arc_id > 0) {
        $node = new SimaNetworkArc($arc_id);
        $node->setFromNode($from_node);
        $node->setToNode($to_node);
        $node->setQmed($q_med);
        $node->setGridCode($grid_code);
        $node->setLength($length);
        $node->setAporteQssMed($aporte_sss_med);
        $node->setReservoirStorageCapacity($reservoir_storage_capacity);
        $node->setReservoirFlow($reservoir_flow);
        $node->setRetentionRate($retention_rate);
        $node->setCumulativeSST($cumulative_sst);
        $this->network[$arc_id] = $node;
      }
    }
  }

  /**
   * FunctionalBranch3 function.
   *
   * @param int $start_id_downstream
   *   The Star id downstream.
   * @param int $current_network_id
   *   The current network id.
   * @param array $cumulative_status
   *   List of storage capacity.
   * @param array $arc_barrier
   *   List of project id's.
   * @param array $arc_start_nodes
   *   List of start nodes.
   * @param array $zero_init
   *   Initial List of key's arc id's and value CERO.
   * @param array $network_vars_to_propagate
   *    Set of variables in each river section that propagate downstream.
   *    It can be for example, the load of conservative contaminants
   *    discharged in a river section.
   * @param array $propagation_status
   *   Use $network_vars_to_propagate.
   * @param array $arc_retention_rate
   *   A Vector that contains for each river section  % the retention rate
   *   parameter.
   * @param array $network_var_to_acum_w_retention
   *   Variable in each river arc that to accumulate downstream,
   *   considering the retention process.
   *   This variable can be the contribution of sediments in each river section.
   * @param array $acum_w_retention_status
   *   Use $network_var_to_acum_w_retention.
   * @param int $arcid_rm_i
   *   Value for validate exit.
   *
   * @return RiverNetworkData
   *   List of cumulative upstream vars.
   */
  public function functionalBranch3($start_id_downstream, $current_network_id,
                                   &$cumulative_status,
                                   &$arc_barrier, &$arc_start_nodes, &$zero_init,
                                   &$network_vars_to_propagate, &$propagation_status,
                                   &$arc_retention_rate,
                                   &$network_var_to_acum_w_retention, &$acum_w_retention_status,
                                   $arcid_rm_i) {

    $functional_network_arcs = $zero_init;
    $propagated_upstream_vars = $propagation_status;
    $cumulative_upstream_vars = $cumulative_status;
    $cumulative_upstream_var_w_retention = $acum_w_retention_status;

    $current_id = $start_id_downstream;
    $n = $current_id;
    $num_branches = 1;
    $posi_uni = [];

    while ($num_branches == 1) {
      $functional_network_arcs[$n] = $current_network_id;
      $new_network_id = $current_network_id;
      // A barrier was found, a new functional network must be assigned to
      // upstream reaches: Uses barrier_id as network_id for upstream network.
      $sum_barrier = 0;
      // $num_element = count($arc_barrier);
      // for($l=0 ; $l < $num_element ; $l++){.
      if (($arc_barrier[$n] > 0)) {
        $sum_barrier++;
      }
      // }.
      if ($sum_barrier > 0) {
        $new_network_id = ($arc_barrier[$n] > 0) ? $arc_barrier[$n] : $current_network_id;
        $posi_uni = $n;
      }

      $n_prev = $n;

      // Keeps going upstream.
      $index_function_slope = 1;
      $index_function_intercept = -20;
      $num_items_to_find = 2;
      $n2 = $this->fastFind($arc_start_nodes[$n], TRUE, $index_function_slope, $index_function_intercept, $num_items_to_find);

      if (count($n2) == 1) {
        $valid_arcs = $this->fastFind($arc_start_nodes[$n2[0]], TRUE, $index_function_slope, $index_function_intercept, $num_items_to_find);
        $valid_arcs2 = $this->fastFind($arc_start_nodes[$valid_arcs[0]], TRUE, $index_function_slope, $index_function_intercept, $num_items_to_find);
      }
      else {
        $valid_arcs = [0, 1, 0, 1];
        $valid_arcs2 = [0, 1, 0, 1];
      }

      $num_branches = count($n2);

      if (count($n2) == 1 && empty($valid_arcs2)) {
        $sum_barrier = 0;
        // $num_element = count($arc_barrier);
        // for($l=0 ; $l < $num_element ; $l++){.
        if ($arc_barrier[$current_id] > 0) {
          $sum_barrier++;
        }
        // }.
        if ($sum_barrier > 0) {
          $new_network_id = ($arc_barrier[$n2[0]] > 0) ? $arc_barrier[$n2[0]] : $current_network_id;
        }
        // $new_network_id = ($arc_barrier[$current_id] > 0) ?
        // $arc_barrier[$current_id] : $current_network_id;.
      }

      if ($num_branches == 0) {
        $valid_arcs = [];
      }

      $up_river_data = new RiverNetworkData();

      if ((($num_branches == 1)  && empty($valid_arcs))) {
        // Fragmentacion.
        $functional_network_arcs[$n2[0]] = $arc_barrier[$n2[0]];

        // Dorh.
        if (isset($cumulative_status[$n2[0]])) {
          $cumulative_upstream_vars = $cumulative_status;
          $cumulative_upstream_vars[$n_prev] += $cumulative_status[$n2[0]];
        }
        // Dorw.
        // Propagated vars are accumulated at confluence points.
        if (isset($propagation_status[$n2[0]])) {
          $propagated_upstream_vars = $propagation_status;
          $propagated_upstream_vars[$n_prev] += $propagation_status[$n2[0]];
        }
        // Sedimentos.
        if (isset($acum_w_retention_status[$n2[0]])) {
          $cumulative_upstream_var_w_retention = $acum_w_retention_status;
          $cumulative_upstream_var_w_retention[$n_prev] += $acum_w_retention_status[$n2[0]];
        }
        $num_branches = 0;
      }
      elseif ((($num_branches > 1)  || !empty($valid_arcs)) || (($num_branches == 1) || empty($valid_arcs))) {
        $num = $num_branches;
        for ($i = 0; $i < $num; $i++) {
          $start_sub_id = $n2[$i];

          $up_river_data = $this->functionalBranch3($start_sub_id, $new_network_id,
                                                    $cumulative_upstream_vars,
                                                    $arc_barrier, $arc_start_nodes, $zero_init,
                                                    $network_vars_to_propagate, $propagated_upstream_vars,
                                                    $arc_retention_rate,
                                                    $network_var_to_acum_w_retention, $cumulative_upstream_var_w_retention,
                                                    $arcid_rm_i);

          // Fragmentacion.
          foreach ($functional_network_arcs as $key => $value) {
            $functional_network_arcs[$key] += $up_river_data->fArcIdsN[$key];
          }
          // Dorh.
          if (isset($up_river_data->cumVars[$n2[$i]])) {
            $cumulative_upstream_vars = $up_river_data->cumVars;
            $cumulative_upstream_vars[$n_prev] += $up_river_data->cumVars[$n2[$i]];
          }
          // Dorw.
          // Propagated vars are accumulated at confluence points.
          if (isset($up_river_data->propVars[$n2[$i]])) {
            $propagated_upstream_vars = $up_river_data->propVars;
            $propagated_upstream_vars[$n_prev] += $up_river_data->propVars[$n2[$i]];
          }
          // Sedimentos.
          if (isset($up_river_data->cumVarsWRetention[$n2[$i]])) {
            $cumulative_upstream_var_w_retention = $up_river_data->cumVarsWRetention;
            $cumulative_upstream_var_w_retention[$n_prev] += $up_river_data->cumVarsWRetention[$n2[$i]];
          }

          if ($num_branches == 1) {
            $num_branches = 0;
          }
        }
      }
      if (empty($arc_barrier[$n2[0]])&&($num_branches == 0)&&(!empty($posi_uni))) {
        $functional_network_arcs[$posi_uni] = $new_network_id;
      }
      if (!empty($up_river_data->cumVarsWRetention[$n_prev])) {
        $cumulative_upstream_var_w_retention[$n_prev] *= (1 - $arc_retention_rate[$n_prev] / 100);
        // $cumulative_upstream_var_w_retention[$n_prev] =
        // $cumulative_upstream_var_w_retention[$n_prev]*
        // (1 - $arc_retention_rate[$n_prev] / 100);.
      }
      if ($network_vars_to_propagate[$n_prev] > 0) {
        $propagated_upstream_vars[$n_prev] = $network_vars_to_propagate[$n_prev];
      }
      // % !!! Solved !!!
      if ($n_prev == $arcid_rm_i) {
        break;
      }

    }
    $river_network_data = new RiverNetworkData();
    $river_network_data->fArcIdsN = $functional_network_arcs;
    $river_network_data->cumVars = $cumulative_upstream_vars;
    $river_network_data->propVars = $propagated_upstream_vars;
    $river_network_data->cumVarsWRetention = $cumulative_upstream_var_w_retention;

    return $river_network_data;
  }

  /**
   * Find node ids.
   *
   * @param int $node_id
   *   The id to find.
   * @param bool $ordered
   *   The order.
   * @param int $index_function_slope
   *   The index function slope.
   * @param int $index_function_intercept
   *   The index function intercept.
   * @param int $num_items_to_find
   *   The number items to find.
   *
   * @return array
   *   A list of ids found.
   */
  public function fastFind($node_id, $ordered, $index_function_slope, $index_function_intercept, $num_items_to_find) {
    $found_ids = [];
    if ($node_id > 0) {
      $search_start = $ordered ? round($node_id * $index_function_slope, 0) + $index_function_intercept : 0;
      if ($search_start < 0) {
        $search_start = 0;
      }

      $i = 0;

      for ($j = 0; $j < $search_start; $j++) {
        next($this->network);
      }

      foreach ($this->network as $arc_id => $node) {
        if (($node->getToNode() == $node_id) && ($i >= $search_start)) {
          $found_ids[] = $node->getArcId();
          // Bifurcation found".
          if (count($found_ids) == $num_items_to_find) {
            break;
          }
        }
        $i++;
      }
    }
    return $found_ids;
  }

  /**
   * Create a list of arc id'ss.
   *
   * @return array
   *   A list.
   */
  public function getArcIds() {
    $arc_ids = [];
    foreach ($this->network as $arc_id => $node) {
      $arc_ids[$arc_id] = $arc_id;
    }
    return $arc_ids;
  }

  /**
   * Create an array with keys: arcid's and values: 0.0.
   *
   * @return array
   *   A list with CERO values.
   */
  public function getZeroInit() {
    $zero_init = [];
    foreach ($this->network as $arc_id => $node) {
      $zero_init[$node->getArcId()] = 0.0;
    }
    return $zero_init;
  }

  /**
   * Create a list.
   *
   * @return array
   *   A list of project id's.
   */
  public function getArcBarrier() {
    $arc_barrier = [];
    foreach ($this->network as $arc_id => $node) {
      $arc_barrier[$arc_id] = 0;
      if (isset($this->reservoirs[$arc_id]) && ($this->reservoirs[$arc_id]->getSimaCaseStatus() > 0)) {
        $arc_barrier[$arc_id] = $this->reservoirs[$arc_id]->getProjectId();
      }
    }
    return $arc_barrier;
  }

  /**
   * Create a list.
   *
   * @return array
   *   A list of storage capacity values.
   */
  public function getCumulativeStatus() {
    $cumulative_status = [];
    foreach ($this->network as $arc_id => $node) {
      $cumulative_status[$arc_id] = 0.0;
      if (isset($this->reservoirs[$arc_id]) && ($this->reservoirs[$arc_id]->getSimaCaseStatus() > 0)) {
        $cumulative_status[$arc_id] = $this->reservoirs[$arc_id]->getStorageCapacity();
      }
    }
    return $cumulative_status;
  }

  /**
   * Create a list.
   *
   * @return array
   *   A list of arc end start nodes id's.
   */
  public function getArcStarNodes() {
    $arc_start_nodes = [];
    foreach ($this->network as $arc_id => $node) {
      $arc_start_nodes[$arc_id] = $node->getFromNode();
    }
    return $arc_start_nodes;
  }

  /**
   * Create a list.
   *
   * @return array
   *   A list of arc end nodes id's.
   */
  public function getArcEndNodes() {
    $arc_end_nodes = [];
    foreach ($this->network as $arc_id => $node) {
      $arc_end_nodes[$arc_id] = $node->getToNode();
    }
    return $arc_end_nodes;
  }

  /**
   * Create a list.
   *
   * @return array
   *   A list of Q med values.
   */
  public function getStreamFlow() {
    $arc_end_nodes = [];
    foreach ($this->network as $arc_id => $node) {
      $arc_end_nodes[$arc_id] = $node->getQmed();
    }
    return $arc_end_nodes;
  }

  /**
   * Create a list.
   *
   * @return array
   *   A list of river order values.
   */
  public function getRiverOrder() {
    $river_order = [];
    foreach ($this->network as $arc_id => $node) {
      $river_order[$arc_id] = $node->getGridCode();
    }
    return $river_order;
  }

  /**
   * Create a list.
   *
   * @return array
   *   A list of river order values.
   */
  public function getArcLengths() {
    $arc_lengths = [];
    foreach ($this->network as $arc_id => $node) {
      $arc_lengths[$arc_id] = $node->getLength();
    }
    return $arc_lengths;
  }

  /**
   * Create a list.
   *
   * @return array
   *   A list of river order values.
   */
  public function getBarrierNames() {
    $barrier_names = [];
    foreach ($this->reservoirs as $arc_id => $reservoir) {
      $project_id = $reservoir->getProjectId();
      $barrier_names[$project_id] = $reservoir->getWeapBranch();
    }
    return $barrier_names;
  }

  /**
   * Create a list.
   *
   * @return array
   *   A list of Reservoir flow values.
   */
  public function getReservoirsFlow() {
    $list = [];
    foreach ($this->network as $arc_id => $node) {
      $list[$arc_id] = 0;
      if (isset($this->reservoirs[$arc_id]) && ($this->reservoirs[$arc_id]->getSimaCaseStatus() > 0)) {
        $list[$arc_id] = $node->getReservoirFlow();
      }
    }
    return $list;
  }

  /**
   * Create a list.
   *
   * @return array
   *   A list of retention rate values.
   */
  public function getRetentionRates() {
    $list = [];
    foreach ($this->network as $arc_id => $node) {
      $list[$arc_id] = 0;
      if (isset($this->reservoirs[$arc_id]) && ($this->reservoirs[$arc_id]->getSimaCaseStatus() > 0)) {
        $list[$arc_id] = $node->getRetentionRate();
      }
    }
    return $list;
  }

  /**
   * Create a list.
   *
   * @return array
   *   A list of aporteQssMed values.
   */
  public function getAporteQssMed() {
    $list = [];
    foreach ($this->network as $arc_id => $node) {
      $list[$arc_id] = $node->getAporteQssMed();
    }
    return $list;
  }

  /**
   * Create a list.
   *
   * @return array
   *   A list of CumulativeSst values.
   */
  public function getCumulativeSst() {
    $list = [];
    foreach ($this->network as $arc_id => $node) {
      $list[$arc_id] = $node->getCumulativeSst();
    }
    return $list;
  }

}
/**
 * Class RiverNetworkData.
 *
 * @package Drupal\dss_magdalena\DSS\Networks
 */
class RiverNetworkData {
  public $fArcIdsN;
  public $cumVars;
  public $propVars;
  public $cumVarsWRetention;

}
