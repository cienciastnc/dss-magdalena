<?php
/**
 * @file
 * Sima Network Node.
 */

namespace Drupal\dss_magdalena\DSS\Models\Network;
/**
 * SimaReservoir class.
 */
class SimaReservoir {

  protected $arcId;

  protected $projectId;

  protected $storageCapacity;

  protected $simaCaseStatus;

  protected $damHeight;

  protected $weapBranch;

  /**
   * Get ArcId value.
   */
  public function getArcId() {
    return $this->arcId;
  }

  /**
   * Set ArcId value.
   *
   * @param int $value
   *   Value to set.
   */
  public function setArcId($value) {
    $this->arcId = $value;
  }

  /**
   * Get ProjectId value.
   */
  public function getProjectId() {
    return $this->projectId;
  }

  /**
   * Set ProjectId value.
   *
   * @param int $value
   *   Value to set.
   */
  public function setProjectId($value) {
    $this->projectId = $value;
  }

  /**
   * Get StorageCapacity value.
   */
  public function getStorageCapacity() {
    return $this->storageCapacity;
  }

  /**
   * Set StorageCapacity value.
   *
   * @param float $value
   *   Value to set.
   */
  public function setStorageCapacity($value) {
    $this->storageCapacity = $value;
  }

  /**
   * Get SimaCaseStatus value.
   */
  public function getSimaCaseStatus() {
    return $this->simaCaseStatus;
  }

  /**
   * Set SimaCaseStatus value.
   *
   * @param int $value
   *   Value to set.
   */
  public function setSimaCaseStatus($value) {
    $this->simaCaseStatus = $value;
  }

  /**
   * Get DamHeight value.
   */
  public function getDamHeight() {
    return $this->damHeight;
  }

  /**
   * Set DamHeight value.
   *
   * @param float $value
   *   Value to set.
   */
  public function setDamHeight($value) {
    $this->damHeight = $value;
  }

  /**
   * Get WeapBranch value.
   */
  public function getWeapBranch() {
    return $this->weapBranch;
  }

  /**
   * Set WeapBranch value.
   *
   * @param string $value
   *   Value to set.
   */
  public function setWeapBranch($value) {
    $this->weapBranch = $value;
  }

}
