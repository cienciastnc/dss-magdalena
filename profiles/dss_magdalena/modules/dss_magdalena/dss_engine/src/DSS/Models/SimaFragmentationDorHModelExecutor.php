<?php
/**
 * @file
 * Model for Fragmentation and DOR-H.
 */

namespace Drupal\dss_magdalena\DSS\Models;
use Drupal\dss_magdalena\DSS\Entity\SimaResource;
use Drupal\dss_magdalena\DSS\Queue\SimaImportQueueItem;
use Drupal\dss_magdalena\DSS\Entity\SimaModel;
use Drupal\dss_magdalena\DSS\Utils\SimaCsvWriterTrait;

/**
 * Class SimaFragmentationDorHModelExecutor.
 *
 * @package Drupal\dss_magdalena\DSS\Models
 */
class SimaFragmentationDorHModelExecutor extends SimaModelExecutor {
  use SimaCsvWriterTrait;

  /**
   * {@inheritdoc}
   */
  protected $inputVariablesMachineNameList = [
    'dss_capacidad',
    'dss_pertenencia_al_caso_hres',
    'dss_pertenencia_al_caso_hror',
  ];

  protected $model;

  /**
   * Output variables list.
   *
   * @var array
   */
  protected $outputVariablesMachineNameList = [];

  /**
   * Output variables data.
   *
   * @var array
   */
  protected $outputVariables = [];

  /**
   * Overrides the loading of Input Variables (Resources / Caja de Datos).
   *
   * These input variables are the Resources (Caja de Datos) that are needed
   * by the model to run.
   */
  protected function setInputVariables() {
    foreach ($this->inputVariablesMachineNameList as $var_machine_name) {
      $cid = $this->case->getId();
      $this->inputVariables[] = SimaResource::loadByCaseStudyAndDataset($cid, $var_machine_name);
    }
  }

  /**
   * Csv for variables with time and range.
   *
   * @param array $context
   *    The context of the model.
   * @param string $machine_name
   *    The machine name.
   * @param string $model
   *    The model.
   *
   * @return null|string
   *    File path of the csv or null.
   */
  protected function getCsvFile($context, $machine_name, $model) {

    $input_variable = $context['output_variables'][$machine_name];

    $directory = 'public://' . $model . '/' . $this->getCaseStudy()->getId() . '/';

    if (file_prepare_directory($directory, FILE_CREATE_DIRECTORY)) {
      $destination = 'public://' . $model . '/' . $this->getCaseStudy()->getId() . '/' . $machine_name . '.csv';
      $saved = $this->saveUnManagedCsvFile($input_variable, $destination, FILE_EXISTS_REPLACE);
      return $saved;
    }
    return NULL;
  }

  /**
   * Save the outputs in CSV files.
   */
  public function saveOutputVariables() {

    $context = $this->getContext();
    $this->outputVariablesMachineNameList = $context['output_variables_machine_name_list'];
    // $context['output_variables'];.
    $context = $this->getContext();
    $case_outputs = $this->getCaseStudy()->getCaseOutputs();
    $case_output_id = $case_outputs->getId();
    foreach ($this->outputVariablesMachineNameList as $variable) {
      if ($this->getCsvFile($context, $variable, $this->model)) {
        $values = [
          'cid' => $this->getCaseStudy()->getId(),
          'case_output' => $case_output_id,
          'file_uri' => 'public://' . $this->model . '/' . $this->getCaseStudy()->getId() . '/' . $variable . '.csv',
          'filename' => $variable . '.csv',
          'dss_machine_name' => $variable,
        ];

        $queue_item = new SimaImportQueueItem($values);

        if ($resource = SimaResource::newFullyFormedResource($queue_item)) {
          $resource->save();

          $case_outputs->addOutputResource($this->model, $resource);
          $case_outputs->save();

          if ($this->getProcessDatastore()) {
            $resource->processDataStore();
          }
        }

      }
    }
    // Set Status.
    $case = $this->getCaseStudy();
    $case->setModelingStatus($this->model, SimaModel::STATUS_OK);
  }

}
