<?php
/**
 * @file
 * Implements an item for generating Resources (Cajas de Datos).
 */

namespace Drupal\dss_magdalena\DSS\Models;

use Drupal\dss_magdalena\DSS\Entity\SimaController;
use Drupal\dss_magdalena\DSS\Models\SimaModelItem;
use Drupal\dss_magdalena\DSS\Models\SimaModelItemInterface;

/**
 * Class SimaModelResourceItem.
 *
 * @package Drupal\dss_magdalena\DSS\Models
 */
class SimaModelResourceItem extends SimaModelItem implements SimaModelItemInterface {

  /**
   * Checks whether this is a Resource Type Model Item.
   *
   * This is an item whose unique purpose is to create resources.
   *
   * @return bool
   *   TRUE if it is a Resource Type Model item, FALSE otherwise.
   */
  public function isResourceType() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function execute() {
    // Only process this if it is a Resource Type.
    if ($context = $this->loadContext()) {
      $data = $this->getData();
      $alternative = SimaController::load($data['nid']);

      $dataset_machine_name = key($data['item']);
      $alternative->createResourceForDataset($context['cid'], $dataset_machine_name, $context['process_datastore']);
    }
  }

}
