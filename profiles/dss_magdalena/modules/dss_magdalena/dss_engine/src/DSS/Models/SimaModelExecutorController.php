<?php
/**
 * @file
 * Controls and loads appropriate model for execution.
 */

namespace Drupal\dss_magdalena\DSS\Models;

use Drupal\dss_magdalena\DSS\Models\Weap\SimaWeapModelExecutor;
use Drupal\dss_magdalena\DSS\Models\Eloha\SimaElohaModelExecutor;
use Drupal\dss_magdalena\DSS\Models\Fragmentation\SimaFragmentationModelExecutor;
use Drupal\dss_magdalena\DSS\Models\DorH\SimaDorHModelExecutor;
use Drupal\dss_magdalena\DSS\Models\DorW\SimaDorWModelExecutor;
use Drupal\dss_magdalena\DSS\Models\Sediments\SimaSedimentsModelExecutor;
use Drupal\dss_magdalena\DSS\Models\TerritorialFootprint\SimaTerritorialFootprintModelExecutor;
use Drupal\dss_magdalena\DSS\Entity\SimaModel;

/**
 * Class SimaModelExecutorController.
 *
 * @package Drupal\dss_magdalena\DSS\Models
 */
class SimaModelExecutorController {

  /**
   * Loads the appropriate SimaModelExecutor.
   *
   * If no UUID is given, it will create a new executor. Otherwise it will load
   * the executor with the correspondent UUID.
   *
   * @param string $model
   *   The model machine name.
   * @param int $cid
   *   The Case ID (nid).
   * @param string $uuid
   *   The Executor's UUID.
   *
   * @return \Drupal\dss_magdalena\DSS\Models\Fragmentation\SimaFragmentationModelExecutor|\Drupal\dss_magdalena\DSS\Models\DorH\SimaDorHModelExecutor|\Drupal\dss_magdalena\DSS\Models\Eloha\SimaElohaModelExecutor|\Drupal\dss_magdalena\DSS\Models\TerritorialFootprint\SimaTerritorialFootprintModelExecutor|bool
   *   The loaded model executor object or FALSE.
   */
  public static function load($model, $cid, $uuid = NULL) {
    switch ($model) {
      case SimaModel::MODEL_WEAP:
        if (empty($uuid)) {
          return new SimaWeapModelExecutor($cid);
        }
        return SimaWeapModelExecutor::load($cid, $uuid);

      case SimaModel::MODEL_ELOHA:
        if (empty($uuid)) {
          return new SimaElohaModelExecutor($cid);
        }
        return SimaElohaModelExecutor::load($cid, $uuid);

      case SimaModel::MODEL_FRAGMENTATION:
        if (empty($uuid)) {
          return new SimaFragmentationModelExecutor($cid);
        }
        return SimaFragmentationModelExecutor::load($cid, $uuid);

      case SimaModel::MODEL_DOR_H:
        if (empty($uuid)) {
          return new SimaDorHModelExecutor($cid);
        }
        return SimaDorHModelExecutor::load($cid, $uuid);

      case SimaModel::MODEL_DOR_W:
        if (empty($uuid)) {
          return new SimaDorWModelExecutor($cid);
        }
        return SimaDorWModelExecutor::load($cid, $uuid);

      case SimaModel::MODEL_SEDIMENTS:
        if (empty($uuid)) {
          return new SimaSedimentsModelExecutor($cid);
        }
        return SimaSedimentsModelExecutor::load($cid, $uuid);

      case SimaModel::MODEL_TERRITORIAL_FOOTPRINT:
        if (empty($uuid)) {
          return new SimaTerritorialFootprintModelExecutor($cid);
        }
        return SimaTerritorialFootprintModelExecutor::load($cid, $uuid);
    }
    return FALSE;
  }

}
