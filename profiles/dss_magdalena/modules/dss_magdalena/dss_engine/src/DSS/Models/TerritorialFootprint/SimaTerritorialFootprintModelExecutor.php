<?php
/**
 * @file
 * Model executor for Territorial Footprint Model.
 */

namespace Drupal\dss_magdalena\DSS\Models\TerritorialFootprint;

use Drupal\dss_magdalena\DSS\Models\SimaModelExecutor;
use Drupal\dss_magdalena\DSS\Models\SimaModelExecutorInterface;
use Drupal\dss_magdalena\DSS\Entity\SimaResource;
use Drupal\dss_magdalena\DSS\Entity\SimaModel;
use Drupal\dss_magdalena\DSS\Queue\SimaImportQueueItem;
use Drupal\dss_magdalena\DSS\Utils\SimaCsvWriterTrait;
/**
 * Class SimaTerritorialFootprintModelExecutor.
 *
 * @package Drupal\dss_magdalena\DSS\Models\TerritorialFootprint
 */
class SimaTerritorialFootprintModelExecutor extends SimaModelExecutor implements SimaModelExecutorInterface {
  use SimaCsvWriterTrait;
  /**
   * {@inheritdoc}
   */
  protected $inputVariablesMachineNameList = [
    'dss_capacidad',
  ];

  /**
   * Output variables list.
   *
   * @var array
   */
  protected $outputVariablesMachineNameList = [];

  /**
   * Output variables data.
   *
   * @var array
   */
  protected $outputVariables = [];

  /**
   * {@inheritdoc}
   */
  public function initialize() {

    $this->outputVariablesMachineNameList = [
      'dss_superficie_inundable_embalses',
      'dss_huella_zonas_de_interes',
      'dss_huella_territorial_por_categoria',
    ];

    // Creating the Items to process.
    $label = t('Generating Outputs of Territorial Footprint Model');
    $item = new SimaTerritorialFootprintModelItem();
    $item->setId($label);
    $item->setCaseStudyId($this->getCaseStudy()->getId());
    $item->setInputVariablesMachineNameList($this->getInputVariablesMachineNameList());
    $item->setData($this->inputVariables);
    $this->addItem($item);
    $this->saveItems();

    // Set and Save Context.
    $this->setContext([
      'cid' => $this->getCaseStudy()->getId(),
      'process_datastore' => $this->getProcessDatastore(),
      'input_variables_machine_name_list' => $this->inputVariablesMachineNameList,
      'output_variables_machine_name_list' => $this->outputVariablesMachineNameList,
    ]);
    $this->saveContext();
  }

  /**
   * {@inheritdoc}
   */
  public function saveOutputVariables() {
    $context = $this->getContext();
    $this->outputVariablesMachineNameList = $context['output_variables_machine_name_list'];
    $case_outputs = $this->getCaseStudy()->getCaseOutputs();
    $case_output_id = $case_outputs->getId();
    foreach ($this->outputVariablesMachineNameList as $variable) {
      if ($this->getCsvFile($context, $variable, SimaModel::MODEL_TERRITORIAL_FOOTPRINT)) {
        $values = [
          'cid' => $this->getCaseStudy()->getId(),
          'case_output' => $case_output_id,
          'file_uri' => 'public://' . SimaModel::MODEL_TERRITORIAL_FOOTPRINT . '/' . $this->getCaseStudy()->getId() . '/' . $variable . '.csv',
          'filename' => $variable . '.csv',
          'dss_machine_name' => $variable,
        ];

        $queue_item = new SimaImportQueueItem($values);

        if ($resource = SimaResource::newFullyFormedResource($queue_item)) {
          $resource->save();

          $case_outputs->addOutputResource(SimaModel::MODEL_TERRITORIAL_FOOTPRINT, $resource);
          $case_outputs->save();

          if ($this->getProcessDatastore()) {
            $resource->processDataStore();
          }
        }

      }
    }
    // Set Status.
    $case = $this->getCaseStudy();
    $case->setModelingStatus(SimaModel::MODEL_TERRITORIAL_FOOTPRINT, SimaModel::STATUS_OK);
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary($results) {

  }

  /**
   * Overrides the loading of Input Variables (Resources / Caja de Datos).
   *
   * These input variables are the Resources (Caja de Datos) that are needed
   * by the model to run.
   */
  protected function setInputVariables() {
    foreach ($this->inputVariablesMachineNameList as $var_machine_name) {
      $cid = $this->case->getId();
      $this->inputVariables[] = SimaResource::loadByCaseStudyAndDataset($cid, $var_machine_name);
    }
  }
  /**
   * Csv for variables with time and range.
   *
   * @param array $context
   *    The context of the model.
   * @param string $machine_name
   *    The machine name.
   * @param string $model
   *    The model.
   *
   * @return null|string
   *    File path of the csv or null.
   */
  protected function getCsvFile($context, $machine_name, $model) {

    $input_variable = $context['output_variables'][$machine_name];

    $directory = 'public://' . $model . '/' . $this->getCaseStudy()->getId() . '/';

    if (file_prepare_directory($directory, FILE_CREATE_DIRECTORY)) {
      $destination = 'public://' . $model . '/' . $this->getCaseStudy()->getId() . '/' . $machine_name . '.csv';
      $saved = $this->saveUnManagedCsvFile($input_variable, $destination, FILE_EXISTS_REPLACE);
      return $saved;
    }
    return NULL;
  }

}
