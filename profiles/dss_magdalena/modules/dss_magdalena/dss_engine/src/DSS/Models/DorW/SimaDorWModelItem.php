<?php
/**
 * @file
 * Implements an item for DorH model.
 */

namespace Drupal\dss_magdalena\DSS\Models\DorW;

use Drupal\dss_magdalena\DSS\Models\SimaFragmentationDorHModelItem;
use Drupal\dss_magdalena\DSS\Models\SimaModelItemInterface;

/**
 * Class SimaDorHModelItem.
 *
 * @package Drupal\dss_magdalena\DSS\Models\DorH
 */
class SimaDorWModelItem extends SimaFragmentationDorHModelItem implements SimaModelItemInterface {

  /**
   * {@inheritdoc}
   */
  public function execute() {
    parent::execute();
  }

}
