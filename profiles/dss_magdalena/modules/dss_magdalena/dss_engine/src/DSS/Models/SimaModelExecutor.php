<?php
/**
 * @file
 * An abstract Model Executor.
 */

namespace Drupal\dss_magdalena\DSS\Models;

use Drupal\dss_magdalena\DSS\Entity\SimaCaseStudy;
use Drupal\dss_magdalena\DSS\Entity\SimaResource;
use Drupal\dss_magdalena\DSS\Entity\SimaAlternative;
use Drupal\dss_magdalena\DSS\Models\SimaModelResourceItem;

/**
 * Class SimaModelExecutor.
 *
 * @package Drupal\dss_magdalena\DSS\Models
 */
abstract class SimaModelExecutor {

  /**
   * Minimum time in seconds we can be sure this executor exists in the cache.
   *
   * Timout = 3 hours by default.
   */
  const TIMEOUT = 10800;

  /**
   * Process the Datastore or not.
   *
   * @var bool
   */
  private $processDatastore;

  /**
   * The Executor's UUID.
   *
   * @var string
   */
  private $uuid;

  /**
   * The Case Study.
   *
   * @var int
   */
  protected $case;

  /**
   * The list of input dataset machine names (variable machine names).
   *
   * @var array
   */
  protected $inputVariablesMachineNameList = [];

  /**
   * The List of Input variables needed by the model.
   *
   * @var array
   */
  protected $inputVariables = [];

  /**
   * The array of model items.
   *
   * @var \ArrayObject
   */
  protected $items;

  /**
   * The Model Context.
   *
   * Use this variable to share global data to the model items.
   *
   * @var array
   */
  protected $context = [];

  /**
   * Public Constructor.
   *
   * @param int $cid
   *   The Case ID.
   * @param string $uuid
   *   The Executor's UUID, if given. If empty, it will be generated.
   */
  public function __construct($cid, $uuid = NULL) {
    // Generate the UUID if not given.
    $this->uuid = empty($uuid) ? uuid_generate() : $uuid;

    // Loading Case Study and Input Variables.
    $this->case = SimaCaseStudy::load($cid);
    $this->setInputVariables();

    // Initializing the items.
    $this->items = new \ArrayObject();
    $this->items->setFlags(\ArrayObject::ARRAY_AS_PROPS);
  }

  /**
   * Loads an executor, having a UUID.
   *
   * @param int $cid
   *   The Case Study ID.
   * @param string $uuid
   *   The Executor's UUID.
   *
   * @return \Drupal\dss_magdalena\DSS\Models\SimaModelExecutorInterface
   *   The Model Executor.
   */
  public static function load($cid, $uuid) {
    // Check if data is available in the cache.
    $executor = new static($cid, $uuid);
    $executor->getItems();
    $executor->loadContext();
    return $executor;
  }

  /**
   * Returns the Executor's UUID.
   *
   * @return string
   *   The Executor's UUID.
   */
  public function getUuid() {
    return $this->uuid;
  }

  /**
   * Adds an item to the pool of items to execute.
   *
   * @param \Drupal\dss_magdalena\DSS\Models\SimaModelItemInterface $item
   *   An item to add to the pool of model items.
   */
  public function addItem(SimaModelItemInterface $item) {
    $item->setContextId($this->getContextId());
    $this->items->append($item);
  }

  /**
   * Obtains an item from the pool.
   *
   * @param int $key
   *   The item key.
   *
   * @return \Drupal\dss_magdalena\DSS\Models\SimaModelItemInterface $item
   *   An item from the pool of model items.
   */
  public function getItem($key) {
    return $this->items[$key];
  }

  /**
   * Saves items in the cache.
   */
  public function saveItems() {
    $cid = str_replace('-', '_', $this->uuid);
    $expiration = time() + self::TIMEOUT;
    cache_set($cid, $this->items, 'cache', $expiration);
  }

  /**
   * Obtains the items from the cache.
   */
  public function getItems() {
    $cid = str_replace('-', '_', $this->uuid);
    if ($items = cache_get($cid)) {
      if (isset($items->data)) {
        $this->items = $items->data;
      }
    }
  }

  /**
   * Obtains the number of model items to process.
   *
   * This function can be used to obtain the number of items that will be used
   * for processing.
   *
   * @return int
   *   The items to process.
   */
  public function getNumberOfItems() {
    return $this->items->count();
  }

  /**
   * Processes a model item.
   *
   * @param \Drupal\dss_magdalena\DSS\Models\SimaModelItemInterface $item
   *   The model item.
   *
   * @return bool
   *   TRUE if succeeds, FALSE otherwise.
   */
  public function processItem(SimaModelItemInterface $item) {
    return $item->execute();
  }

  /**
   * Instructs the executor to process or not the datatore for all resources.
   *
   * @param bool $process_datastore
   *   TRUE to process the Datastore, FALSE otherwise.
   */
  public function setProcessDatastore($process_datastore) {
    $this->processDatastore = $process_datastore;
  }

  /**
   * Returns the flag that indicates whether to process the Datastore.
   *
   * @return bool
   *   The Indicator to process the datastore.
   */
  protected function getProcessDatastore() {
    return $this->processDatastore;
  }

  /**
   * Sets the Model Context.
   *
   * @param array $context
   *   An object that stores shared context information.
   */
  protected function setContext($context) {
    $this->context = $context;
  }

  /**
   * Gets the model context.
   *
   * @return array
   *   The model context.
   */
  protected function getContext() {
    return $this->context;
  }

  /**
   * Sets the Context Id.
   *
   * @return string
   *   The Context ID.
   */
  protected function getContextId() {
    $cid = str_replace('-', '_', $this->uuid);
    return 'c_' . $cid;
  }

  /**
   * Saves the Model Context Data.
   */
  protected function saveContext() {
    $expiration = time() + self::TIMEOUT;
    cache_set($this->getContextId(), $this->context, 'cache', $expiration);
  }

  /**
   * Loads the Model Context Data.
   */
  protected function loadContext() {
    if ($context = cache_get($this->getContextId())) {
      if (isset($context->data)) {
        $this->context = $context->data;
      }
    }
  }

  /**
   * Obtains the loaded Case Study.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaCaseStudy|FALSE
   *   The SimaCaseStudy object.
   */
  protected function getCaseStudy() {
    return $this->case;
  }

  /**
   * Sets the Input Variables (Resources / Caja de Datos).
   *
   * These input variables are the Resources (Caja de Datos) that are needed
   * by the model to run.
   */
  protected function setInputVariables() {
    foreach ($this->inputVariablesMachineNameList as $var_machine_name) {
      $rid = $this->case->getCaseOutputs()->checkWeapOutputExistsForDataset($var_machine_name);
      if ($rid) {
        $this->inputVariables[] = SimaResource::load($rid);
      }
    }
  }

  /**
   * Returns the loaded list of Resources (Input variables).
   *
   * @return array
   *   An array of loaded SimaResources.
   */
  protected function getInputVariables() {
    return $this->inputVariables;
  }

  /**
   * Returns the list of Input Variables Machine Names.
   *
   * @return array
   *   A list of input variables machine names.
   */
  public function getInputVariablesMachineNameList() {
    return $this->inputVariablesMachineNameList;
  }

  /**
   * Pre-Initializes the model.
   *
   * This method is executed to add model items for creating missing resources
   * needed by the model.
   */
  public function preInitialize() {
    // Create tasks to generate missing resources.
    $tasks = $this->calculateMissingResources();
    foreach ($tasks as $task) {
      $label = t('Creating resource for variable: !machine_name', [
        '!machine_name' => key($task['item']),
      ]);

      // Creating the Items to process.
      $item = new SimaModelResourceItem();
      $item->setId($label);
      $item->setData($task);
      $this->addItem($item);
    }
  }

  /**
   * Calculate Missing Resources in Case Alternatives.
   *
   * @return array
   *   An array of tasks to execute.
   */
  protected function calculateMissingResources() {
    $cid = $this->getCaseStudy()->getId();
    // @codingStandardsIgnoreStart
    // Calculate missing resources for Agricultural Alternatives.
    // $prod_landuse_alternatives = $this->getCaseStudy()->getProductiveLandUseAlternatives();
    // $sima_alternative = SimaAlternative::load($prod_landuse_alternatives);
    // $missing_resources = $sima_alternative->listMissingResourcesFromProjects($cid);
    // $tasks = $this->generateResourceTasks('alternative_resources', $sima_alternative->getId(), $missing_resources);

    // Calculate missing resources for Wetland Management Alternatives.
    // @TODO: Should we create resources for Wetlands Management Alternatives?
    // $wetland_alternatives = $this->getCaseStudy()->getWetlandManagementAlternatives();
    // $sima_alternative = SimaAlternative::load($wetland_alternatives);
    // $missing_resources = $sima_alternative->listMissingResourcesFromProjects($cid);
    // $tasks += $this->generateResourceTasks('alternative_resources', $sima_alternative->getId(), $missing_resources);
    // @codingStandardsIgnoreEnd

    // Calculate missing resources for Hydropower Alternatives.
    $hydropower_alternatives = $this->getCaseStudy()->getHydropowerAlternatives();
    $sima_alternative = SimaAlternative::load($hydropower_alternatives);
    $missing_resources = $sima_alternative->listMissingResourcesFromProjects($cid);

    // Check that there is not already a resource assigned to this dataset.
    // We can ONLY have a single resource assigned to this case study for this
    // particular dataset.
    foreach ($missing_resources as $key => $dataset_machine_name) {
      if (SimaResource::loadByCaseStudyAndDataset($cid, $dataset_machine_name)) {
        // If the resource already exists, then do not create a task to process
        // it.
        unset($missing_resources[$key]);
      }
    }

    $tasks = $this->generateResourceTasks($sima_alternative->getId(), $missing_resources);

    return $tasks;
  }

  /**
   * Generates Resource Tasks.
   *
   * @param int $nid
   *   The node NID.
   * @param array $items
   *   The items array.
   *
   * @return array
   *   An array of tasks.
   */
  protected function generateResourceTasks($nid, $items) {
    $tasks = [];
    foreach ($items as $key => $item) {
      $tasks[] = [
        'nid' => $nid,
        'item' => [
          $key => $item,
        ],
      ];
    }
    return $tasks;
  }

}
