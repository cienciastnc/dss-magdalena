<?php
/**
 * @file
 * Specifies a Ficha Interface.
 */

namespace Drupal\dss_magdalena\DSS\Entity\Ficha;

/**
 * Interface SimaFichaInterface.
 *
 * @package Drupal\dss_magdalena\DSS\Entity\Ficha
 */
interface SimaFichaInterface {

  /**
   * Returns the Datasets/WEAP Variable relationship.
   *
   * @return array
   *   The relationship Dataset <-> WEAP.
   */
  public function getDatasetsWeap();

  /**
   * Returns the referenced Dataset.
   */
  public function getReferenceDataset();

  /**
   * Loads the SimaFicha.
   */
  static public function load($nid);

  /**
   * Builds a new Ficha.
   */
  static public function newFicha($project_data, $ficha_data);

  /**
   * Returns the Dataset MachineName referenced from it.
   */
  public function getDatasetMachineName($weap_variable);

  /**
   * Gets the projects associated with this Ficha.
   */
  public function getProject();

}
