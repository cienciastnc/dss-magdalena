<?php
/**
 * @file
 * Defines an Entity Model for a Multifield.
 */

namespace Drupal\dss_magdalena\DSS\Entity\Multifield;

use Drupal\dss_magdalena\DSS\Entity\EntityAbstract;

/**
 * Models a Multifield Entity.
 *
 * Class EntityMultifieldAbstract.
 *
 * @package Drupal\dss_magdalena\DSS\Entity\Multifield
 */
abstract class EntityMultifieldAbstract extends EntityAbstract {

  const BUNDLE   = '';

  /**
   * Public constructor.
   *
   * @param \EntityDrupalWrapper|object $drupal_entity
   *   The multifield entity that maps to a Sima Multifield.
   */
  public function __construct($drupal_entity) {
    $this->setDrupalEntity('multifield', $drupal_entity);
  }

  /**
   * Loads a SimaMultifield from a Drupal entity or a Drupal entity ID.
   *
   * @param mixed $entity
   *   The multifield id or loaded object.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\EntityAbstract|FALSE
   *   The loaded EntityAbstract object if exists, FALSE otherwise.
   */
  public static function load($entity) {
    if ($entity instanceof \EntityDrupalWrapper || is_object($entity)) {
      return new static($entity);
    }
    elseif (is_array($entity)) {
      return static::newMultifieldEntity($entity);
    }
    elseif (is_int($entity) || (intval($entity) !== 0 && !is_array($entity))) {
      $entity_multifield = entity_load('multifield', array($entity));
      $entity_multifield = reset($entity_multifield);
      if ($entity_multifield === FALSE) {
        return FALSE;
      }
      return new static($entity_multifield);
    }
    else {
      return FALSE;
    }
  }

  /**
   * Creates a new Multifield Entity.
   *
   * @param array $values
   *   An array of values.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\EntityAbstract|bool
   *   The Multifield entity or FALSE.
   */
  public static function newMultifieldEntity($values = []) {
    // Create a new entity.
    $e = (object) ($values + array('id' => NULL));
    $e->type = static::BUNDLE;
    return new static($e);
  }

}
