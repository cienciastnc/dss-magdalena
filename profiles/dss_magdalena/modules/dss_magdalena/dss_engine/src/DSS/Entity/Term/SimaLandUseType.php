<?php
/**
 * @file
 * Models the Land Use Type Taxonomy Term.
 */

namespace Drupal\dss_magdalena\DSS\Entity\Term;

/**
 * Models a WEAP Level1 taxonomy term.
 *
 * Class SimaLandUseType.
 *
 * @package Drupal\dss_magdalena\DSS\Entity\Term
 */
class SimaLandUseType extends EntityTermAbstract {

  const BUNDLE                     = 'land_use_types';

  const NAME_AGRICOLA              = 'Agricola';
  const NAME_AGRICOLA_PERMANENTE   = 'AgricolaPermanente';
  const NAME_AGRICOLA_TRANSITORIO  = 'AgricolaTransitorio';
  const NAME_ARBUSTO               = 'Arbusto';
  const NAME_BOSQUE                = 'Bosque';
  const NAME_BOSQUE_PLANTADO       = 'BosquePlantado';
  const NAME_CUERPO_AGUA           = 'Cuerpo Agua';
  const NAME_CUERPOAGUA            = 'CuerpoAgua';
  const NAME_HERBAZAL              = 'Herbazal';
  const NAME_HIDROFITA             = 'Hidrofita';
  const NAME_MINERO                = 'Minero';
  const NAME_URBANO                = 'Urbano';
  const NAME_VEGETACION_SECUNDARIA = 'Vegetacion Secundaria';
  const NAME_VEGETACIONSECUNDARIA  = 'VegetacionSecundaria';
  const NAME_PASTOS                = 'Pastos';
  const NAME_OTROS                 = 'Otros';

  // Land Type.
  const TYPE_AGRICULTURE           = 'Agriculture';
  const TYPE_FOREST_INDUSTRIAL     = 'Forest_Industrial';
  const TYPE_MINING                = 'Mining';


  /**
   * Loads a SimaLandUseType from a Drupal entity or a Drupal entity ID.
   *
   * @param mixed $landuse
   *   The taxonomy_term tid or loaded object.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Term\SimaLandUseType|FALSE
   *   A loaded SimaLandUseType object if exists, FALSE otherwise.
   */
  public static function load($landuse) {
    if ($landuse instanceof SimaLandUseType) {
      return $landuse;
    }
    else {
      if ($entity = parent::load($landuse)) {
        return ($entity->getDrupalBundle() == self::BUNDLE) ? $entity : FALSE;
      }
      return FALSE;
    }
  }

  /**
   * Obtains the Land Category: Agricultural, Forest/Industrial, Mining.
   *
   * @return bool|string
   *   The Land Type Category or FALSE.
   */
  public function getLandCategory() {
    switch ($this->getName()) {
      case self::NAME_AGRICOLA:
      case self::NAME_AGRICOLA_PERMANENTE:
      case self::NAME_AGRICOLA_TRANSITORIO:
        return self::TYPE_AGRICULTURE;

      case self::NAME_BOSQUE_PLANTADO:
        return self::TYPE_FOREST_INDUSTRIAL;

      case self::NAME_MINERO:
        return self::TYPE_MINING;

      default:
        return FALSE;
    }
  }

  /**
   * Is this an Agricultural Land Type?
   *
   * @return bool
   *   TRUE if it is an agricultural land type, FALSE otherwise.
   */
  public function isAgriculturalLand() {
    return $this->getLandCategory() === self::TYPE_AGRICULTURE;
  }

  /**
   * Is this a Forest-Industrial Land Type?
   *
   * @return bool
   *   TRUE if this is a Forest/Industrial Land Type, FALSE otherwise.
   */
  public function isForestAndIndustrialLand() {
    return $this->getLandCategory() === self::TYPE_FOREST_INDUSTRIAL;
  }

  /**
   * Is this an Mining Land Type?
   *
   * @return bool
   *   TRUE if it is a mining land type, FALSE otherwise.
   */
  public function isMiningLand() {
    return $this->getLandCategory() === self::TYPE_MINING;
  }

  /**
   * Returns a list of All Agricultural Land Types.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Term\SimaLandUseType[]
   *   An array of loaded SimaLandUseType objects.
   */
  public static function getAgriculturalLandUseTypes() {
    $land_types = [
      SimaLandUseType::loadByTaxonomyTermName(self::NAME_AGRICOLA),
      SimaLandUseType::loadByTaxonomyTermName(self::NAME_AGRICOLA_PERMANENTE),
      SimaLandUseType::loadByTaxonomyTermName(self::NAME_AGRICOLA_TRANSITORIO),
    ];
    return array_filter($land_types);
  }

}
