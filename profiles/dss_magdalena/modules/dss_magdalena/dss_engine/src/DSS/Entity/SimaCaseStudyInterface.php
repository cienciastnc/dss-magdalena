<?php
/**
 * @file
 * Interface definition for SimaCaseStudy.
 */

namespace Drupal\dss_magdalena\DSS\Entity;

/**
 * Defines the Interface for SimaCaseStudy.
 *
 * Interface SimaCaseStudyInterface.
 *
 * @package Drupal\dss_magdalena\DSS
 */
interface SimaCaseStudyInterface {

  /**
   * Returns the Agricultural Alternatives.
   */
  public function getProductiveLandUseAlternatives();

  /**
   * Returns the Hydropower Alternatives.
   */
  public function getHydropowerAlternatives();

  /**
   * Returns the Wetland Management Alternatives.
   */
  public function getWetlandManagementAlternatives();

  /**
   * Returns the Climate Scenarios.
   */
  public function getClimateScenarios();

  /**
   * Returns the Energy Scenarios.
   */
  public function getEnergyScenarios();

  /**
   * Returns the Population Scenarios.
   */
  public function getPopulationScenarios();

  /**
   * Returns the model status for all or one specific model.
   */
  public function getModelingStatus($model_nid);

  /**
   * Sets the model status for a single model.
   *
   * @param string $model_machine_name
   *   The model machine_name.
   * @param int $status
   *   The status of the model.
   */
  public function setModelingStatus($model_machine_name, $status);

  /**
   * Exports the current Case Study to WEAP.
   */
  public function export();

  /**
   * Imports the current Case Study from WEAP.
   */
  public function import();

  /**
   * Imports the Zip File into WEAP Outputs.
   *
   * @param string $zip_file
   *   An URI that notes the Zip file.
   */
  public function importWeapOutputs($zip_file);

  /**
   * Import WEAP Outputs from a particular directory.
   *
   * @param string $directory
   *   The full path directory.
   */
  public function importWeapOutputsFromDirectory($directory);

  /**
   * Gets case readiness.
   *
   * @param string $model_machine_name
   *   The machine name.
   *
   * @return array
   *   The Case Readiness array.
   */
  public function getCaseReadiness($model_machine_name);

  /**
   * Returns the Case Outputs.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaCaseOutputs
   *   The loaded SimaCaseOutputs object.
   */
  public function getCaseOutputs();

}
