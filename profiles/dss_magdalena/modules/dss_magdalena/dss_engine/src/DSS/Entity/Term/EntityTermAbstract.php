<?php
/**
 * @file
 * Defines an Entity Model for a Taxonomy Term.
 */

namespace Drupal\dss_magdalena\DSS\Entity\Term;

use Drupal\dss_magdalena\DSS\Entity\EntityAbstract;

/**
 * Models a Taxonomy Term Entity.
 *
 * Class EntityTermAbstract.
 *
 * @package Drupal\dss_magdalena\DSS\Entity\Term
 */
abstract class EntityTermAbstract extends EntityAbstract {

  const BUNDLE   = '';

  /**
   * Public constructor.
   *
   * @param \EntityDrupalWrapper|object $drupal_entity
   *   The taxonomy_term that maps to a Sima Taxonomy Term.
   */
  public function __construct($drupal_entity) {
    $this->setDrupalEntity('taxonomy_term', $drupal_entity);
  }

  /**
   * Loads a SimaScenario from a Drupal entity or a Drupal entity ID.
   *
   * @param mixed $entity
   *   The taonomy_term tid or loaded object.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\EntityAbstract|FALSE
   *   The loaded EntityAbstract object if exists, FALSE otherwise.
   */
  public static function load($entity) {
    if ($entity instanceof \EntityDrupalWrapper || is_object($entity)) {
      return new static($entity);
    }
    elseif (is_array($entity)) {
      $entity_term = entity_create('taxonomy_term', $entity);
      return new static($entity_term);
    }
    elseif (is_int($entity) || (intval($entity) !== 0 && !is_array($entity))) {
      $entity_term = taxonomy_term_load($entity);
      if ($entity_term === FALSE) {
        return FALSE;
      }
      return new static($entity_term);
    }
    else {
      return FALSE;
    }
  }

  /**
   * Creates a new Taxonomy Term Entity.
   *
   * @param string $name
   *   The term name.
   * @param int $parent
   *   The parent entity tid.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\EntityAbstract|bool
   *   The Taxonomy term entity of the specific vocabulary or FALSE.
   */
  public static function newEntity($name, $parent = 0, $uid = 1) {
    // Obtaining the vocabulary.
    if ($vocabulary = static::getVocabulary()) {
      // Create a new entity.
      $e = entity_create('taxonomy_term', array(
        'vid' => $vocabulary->vid,
        'name' => $name,
        'parent' => $parent,
      ));
      return new static($e);
    }
    return FALSE;
  }

  /**
   * Sets up the Taxonomy Term Name.
   *
   * @param string $title
   *   The title to set.
   * @param string $lang
   *   The Language Code.
   */
  public function setName($title, $lang = LANGUAGE_NONE) {
    $this->set('name', $title, $lang);
  }

  /**
   * Returns the Taxonomy term Name.
   *
   * @param string $lang
   *   The Language Code.
   *
   * @return string
   *   The taxonomy_term name.
   */
  public function getName($lang = LANGUAGE_NONE) {
    return $this->get('name', $lang);
  }

  /**
   * Obtain the URL Alias.
   *
   * @return string
   *   The URL Alias for the taxonomy term.
   */
  public function getUrlAlias() {
    $url = taxonomy_term_uri($this->getDrupalEntity()->value());
    return url($url['path']);
  }

  /**
   * Loads a term by Taxonomy Term name.
   *
   * @param string $name
   *   The Term Name.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Term\EntityTermAbstract|bool
   *   The Term Entity if exists, FALSE otherwise.
   */
  public static function loadByTaxonomyTermName($name) {
    $terms = taxonomy_get_term_by_name($name, static::BUNDLE);
    $term = reset($terms);
    // We expect a single match.
    if (!empty($term)) {
      return new static($term);
    }
    return FALSE;
  }

  /**
   * Returns the Vocabulary object.
   *
   * @return mixed
   *   The vocabulary object with all metadata if exists, FALSE otherwise.
   *   Results are statically cached.
   */
  public static function getVocabulary() {
    return taxonomy_vocabulary_machine_name_load(static::BUNDLE);
  }

}
