<?php
/**
 * @file
 * Sima Basic Page.
 */

namespace Drupal\dss_magdalena\DSS\Entity;

/**
 * Models a Sima Basic Page.
 *
 * Class SimaBasicPage.
 *
 * @package Drupal\dss_magdalena\DSS\Entity
 */
class SimaBasicPage extends EntityNodeAbstract {

  const BUNDLE                              = 'page';

  /**
   * Loads a SimaBasicPage from a Drupal entity or a Drupal entity ID.
   *
   * @param mixed $page
   *   The nid or loaded object.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaBasicPage|FALSE
   *   The loaded SimaBasicPage object if exists, FALSE otherwise.
   */
  public static function load($page) {
    if ($page instanceof SimaBasicPage) {
      return $page;
    }
    else {
      if ($entity = parent::load($page)) {
        return ($entity->getDrupalBundle() == self::BUNDLE) ? $entity : FALSE;
      }
      return FALSE;
    }
  }

  /**
   * Loads a SimaBasicPage given its title.
   *
   * @param string $title
   *   The Sima Basic Page Title.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaBasicPage|FALSE
   *   The loaded SimaBasicPage object if exists, FALSE otherwise.
   */
  public static function loadPageByTitle($title) {
    return static::loadByTitle($title, static::BUNDLE);
  }

}
