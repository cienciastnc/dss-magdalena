<?php
/**
 * @file
 * Models the Water Balance Storage for a Water Balance Template.
 */

namespace Drupal\dss_magdalena\DSS\Entity\Multifield;

use Drupal\dss_magdalena\DSS\Entity\SimaDataset;

/**
 * Class SimaWaterBalanceStorage.
 *
 * @package Drupal\dss_magdalena\DSS\Entity\Multifield
 */
class SimaWaterBalanceStorage extends EntityMultifieldAbstract {

  const BUNDLE                        = 'field_storage';

  const FIELD_STORAGE_VARIABLE        = 'field_storevar';
  const FIELD_STORAGE_FACTOR          = 'field_storefac';

  /**
   * Loads a SimaWaterBalanceStorage from a Drupal entity or a Drupal entity ID.
   *
   * @param mixed $water_balance_storage
   *   The multifield id or loaded object.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Multifield\SimaWaterBalanceStorage|FALSE
   *   A loaded SimaWaterBalanceStorage object if exists, FALSE otherwise.
   */
  public static function load($water_balance_storage) {
    if ($water_balance_storage instanceof SimaWaterBalanceStorage) {
      return $water_balance_storage;
    }
    else {
      if ($entity = parent::load($water_balance_storage)) {
        return ($entity->getDrupalBundle() == self::BUNDLE) ? $entity : FALSE;
      }
      return FALSE;
    }
  }

  /**
   * Returns the Variable (Dataset).
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaDataset|FALSE
   *   The SimaDataset object loaded if found, FALSE otherwise.
   */
  public function getVariable() {
    $storage_variable = $this->get(self::FIELD_STORAGE_VARIABLE);
    return SimaDataset::load($storage_variable);
  }

  /**
   * Gets the Factor.
   *
   * @return string
   *   The Factor.
   */
  public function getFactor() {
    return $this->get(self::FIELD_STORAGE_FACTOR);
  }

}
