<?php
/**
 * @file
 * Models a Variable Range taxonomy term.
 */

namespace Drupal\dss_magdalena\DSS\Entity\Term;

/**
 * Implements a Variable Range taxonomy term.
 *
 * Class SimaVariableRange.
 *
 * @package Drupal\dss_magdalena\DSS\Entity\Term
 */
class SimaVariableRange extends EntityTermAbstract {

  const BUNDLE         = 'value_range';

  /**
   * Loads a SimaVariableRange from a Drupal entity or a Drupal entity ID.
   *
   * @param mixed $variable_range
   *   The taxonomy_term tid or loaded object.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Term\SimaVariableRange|FALSE
   *   The loaded SimaVariableRange object if exists, FALSE otherwise.
   */
  public static function load($variable_range) {
    if ($variable_range instanceof SimaVariableRange) {
      return $variable_range;
    }
    else {
      if ($entity = parent::load($variable_range)) {
        return ($entity->getDrupalBundle() == self::BUNDLE) ? $entity : FALSE;
      }
      return FALSE;
    }
  }

}
