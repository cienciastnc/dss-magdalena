<?php
/**
 * @file
 * Sima Scenarios.
 */

namespace Drupal\dss_magdalena\DSS\Entity;

/**
 * Models a Sima Scenario.
 *
 * Class SimaScenario.
 *
 * @package Drupal\dss_magdalena\DSS\Entity
 */
class SimaScenario extends EntityNodeAbstract {

  const CLIMATE_SCENARIO                    = 'climate_scenario';
  const ENERGY_SCENARIO                     = 'energy_scenario';
  const POPULATION_SCENARIO                 = 'population_scenario';

  const RESOURCES_CLIMATE_SCENARIOS         = 'field_variable_databoxes_cli';
  const RESOURCES_ENERGY_SCENARIOS          = 'field_variable_databoxes_ene';
  const RESOURCES_POPULATION_SCENARIOS      = 'field_variable_databoxes_pop';

  // Fields.
  const FIELD_CLIMATE_ID                    = 'field_climate_id';
  const FIELD_POPULATION_ID                 = 'field_population_id';

  /**
   * Loads a SimaScenario from a Drupal entity or a Drupal entity ID.
   *
   * @param mixed $scenario
   *   The nid or loaded object.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaScenario|FALSE
   *   The loaded SimaScenario object if exists, FALSE otherwise.
   */
  public static function load($scenario) {
    if ($scenario instanceof SimaScenario) {
      return $scenario;
    }
    else {
      $bundles = [
        self::CLIMATE_SCENARIO,
        self::ENERGY_SCENARIO,
        self::POPULATION_SCENARIO,
      ];
      if ($entity = parent::load($scenario)) {
        return in_array($entity->getDrupalBundle(), $bundles) ? $entity : FALSE;
      }
      return FALSE;
    }
  }

  /**
   * Returns the Climate ID.
   *
   * @return mixed
   *   The Climate ID.
   */
  public function getClimateId() {
    return $this->get(self::FIELD_CLIMATE_ID);
  }

  /**
   * Returns the Population ID.
   *
   * @return mixed
   *   The Population ID.
   */
  public function getPopulationId() {
    return $this->get(self::FIELD_POPULATION_ID);
  }

  /**
   * Gets all the resources linked to this scenario.
   *
   * Resources === "Cajas de Datos".
   *
   * @param int $cid
   *   The Case Study ID.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaResource[]
   *   An array of resources linked to this Scenario.
   */
  public function getResources($cid = NULL) {
    $resources = [];
    switch ($this->getDrupalBundle()) {
      case self::CLIMATE_SCENARIO:
        $resources = $this->getMultivalued(self::RESOURCES_CLIMATE_SCENARIOS);
        break;

      case self::ENERGY_SCENARIO:
        $resources = $this->getMultivalued(self::RESOURCES_ENERGY_SCENARIOS);
        break;

      case self::POPULATION_SCENARIO:
        $resources = $this->getMultivalued(self::RESOURCES_POPULATION_SCENARIOS);
        break;
    }
    foreach ($resources as $key => $resource) {
      $resources[$key] = SimaResource::load($resource);
    }
    return $resources;
  }

}
