<?php
/**
 * @file
 * Models the Water Balance Input for a Water Balance Template.
 */

namespace Drupal\dss_magdalena\DSS\Entity\Multifield;

use Drupal\dss_magdalena\DSS\Entity\SimaDataset;

/**
 * Class SimaWaterBalanceInput.
 *
 * @package Drupal\dss_magdalena\DSS\Entity\Multifield
 */
class SimaWaterBalanceInput extends EntityMultifieldAbstract {

  const BUNDLE                      = 'field_input';

  const FIELD_INPUT_VARIABLE        = 'field_inputvar';
  const FIELD_INPUT_FACTOR          = 'field_inputfac';

  /**
   * Loads a SimaWaterBalanceInput from a Drupal entity or a Drupal entity ID.
   *
   * @param mixed $water_balance_input
   *   The multifield id or loaded object.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Multifield\SimaWaterBalanceInput|FALSE
   *   A loaded SimaWaterBalanceInput object if exists, FALSE otherwise.
   */
  public static function load($water_balance_input) {
    if ($water_balance_input instanceof SimaWaterBalanceInput) {
      return $water_balance_input;
    }
    else {
      if ($entity = parent::load($water_balance_input)) {
        return ($entity->getDrupalBundle() == self::BUNDLE) ? $entity : FALSE;
      }
      return FALSE;
    }
  }

  /**
   * Returns the Variable (Dataset).
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaDataset|FALSE
   *   The SimaDataset object loaded if found, FALSE otherwise.
   */
  public function getVariable() {
    $input_variable = $this->get(self::FIELD_INPUT_VARIABLE);
    return SimaDataset::load($input_variable);
  }

  /**
   * Gets the Factor.
   *
   * @return string
   *   The Input Factor.
   */
  public function getFactor() {
    return $this->get(self::FIELD_INPUT_FACTOR);
  }

}
