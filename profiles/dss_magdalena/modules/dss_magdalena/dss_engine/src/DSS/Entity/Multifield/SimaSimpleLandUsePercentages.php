<?php
/**
 * @file
 * Models Simple Land Use Percentages for Productive Land Use Alternatives.
 */

namespace Drupal\dss_magdalena\DSS\Entity\Multifield;

use Drupal\dss_magdalena\DSS\Entity\Term\SimaLandUseType;

/**
 * Class SimaSimpleLandUsePercentages.
 *
 * @package Drupal\dss_magdalena\DSS\Entity\Multifield
 */
class SimaSimpleLandUsePercentages extends EntityMultifieldAbstract {

  const BUNDLE                           = 'field_landuse';

  const FIELD_LAND_USE_TYPE              = 'field_irr_land_use';
  const FIELD_IRRIGATION_PERCENTAGE_AREA = 'field_irr_perc_area';
  const FIELD_PERCENTAGE_SHARE           = 'field_perc_landuse';

  /**
   * Loads a SimaSimpleLandUsePercentages from a Drupal entity or a entity ID.
   *
   * @param mixed $land_use_percentage
   *   The multifield id or loaded object.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Multifield\SimaSimpleLandUsePercentages|FALSE
   *   A loaded SimaSimpleLandUsePercentages object if exists, FALSE otherwise.
   */
  public static function load($land_use_percentage) {
    if ($land_use_percentage instanceof SimaSimpleLandUsePercentages) {
      return $land_use_percentage;
    }
    else {
      if ($entity = parent::load($land_use_percentage)) {
        return ($entity->getDrupalBundle() == self::BUNDLE) ? $entity : FALSE;
      }
      return FALSE;
    }
  }

  /**
   * Returns the Land Use Type Taxonomy Term.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Term\SimaLandUseType|FALSE
   *   The SimaLandUseType object loaded if found, FALSE otherwise.
   */
  public function getLandUseType() {
    $land_use = $this->get(self::FIELD_LAND_USE_TYPE);
    return SimaLandUseType::load($land_use);
  }

  /**
   * Sets the Land Use Type.
   *
   * @param string $name
   *   The Land Use Type name.
   */
  public function setLandUseType($name) {
    if ($land_use_type = SimaLandUseType::loadByTaxonomyTermName($name)) {
      $this->set(self::FIELD_LAND_USE_TYPE, $land_use_type->getDrupalEntity()->value());
    }
  }

  /**
   * Gets the Percentage Share of this Land Type.
   *
   * @return float
   *   The Percentage of shared area.
   */
  public function getPercentage() {
    return floatval($this->get(self::FIELD_PERCENTAGE_SHARE));
  }

  /**
   * Sets the Percentage Share for this Land Type.
   *
   * @param string $percentage
   *   The percentage. Should be a decimal value.
   */
  public function setPercentage($percentage) {
    $percentage = floatval($percentage);
    $this->set(self::FIELD_PERCENTAGE_SHARE, number_format($percentage, 2, '.', ''));
  }

  /**
   * Returns the Irrigated Percentage Area.
   *
   * @return mixed
   *   The irrigated area.
   */
  public function getIrrigatedPercentageArea() {
    return $this->get(self::FIELD_IRRIGATION_PERCENTAGE_AREA);
  }

}
