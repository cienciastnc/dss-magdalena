<?php
/**
 * @file
 * Interface for Sima Alternatives.
 */

namespace Drupal\dss_magdalena\DSS\Entity\Alternatives;

/**
 * Interface SimaAlternativeInterface.
 *
 * @package Drupal\dss_magdalena\DSS\Entity\Alternatives
 */
interface SimaAlternativeInterface {

  /**
   * Generates a Resource for a particular Dataset in this Case Study.
   *
   *   Use this method in cases where a resource needs to be generated before
   *   sending the model to WEAP for execution. This method translates the
   *   selection of projects by the user to WEAP variable resources (cajas de
   *   datos).
   *   Note: It should NOT need a Case ID (because in order to execute this, it
   *   has to belong to a particular Case Study.
   *
   * @param int $cid
   *   The Case Study ID.
   * @param string $dataset_machine_name
   *   The Dataset Machine Name.
   * @param bool $process_datastore
   *   TRUE to process the Datastore, FALSE otherwise.
   *
   * @TODO: Get rid of the $cid in this method.
   */
  public function createResourceForDataset($cid, $dataset_machine_name, $process_datastore);

}
