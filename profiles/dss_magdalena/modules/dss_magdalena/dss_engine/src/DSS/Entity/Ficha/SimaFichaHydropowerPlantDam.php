<?php
/**
 * @file
 * A Hydropower Plant or Dam. Ficha Type 1.
 */

namespace Drupal\dss_magdalena\DSS\Entity\Ficha;

use Drupal\dss_magdalena\DSS\Entity\SimaProject;

/**
 * Implements a Ficha for Hydropower Plants or Dams.
 *
 * Class SimaFichaHydropowerPlantDam.
 *
 * @package Drupal\dss_magdalena\DSS\Entity\Ficha
 */
class SimaFichaHydropowerPlantDam extends SimaFicha implements SimaFichaInterface {

  const BUNDLE                   = 'hydropower';

  // Hydropower Plant or Dam Fields.
  const STORAGE_CAPACITY         = 'field_storage_capacity';
  const TOP_OF_INACTIVE          = 'field_top_inactive';
  const TOP_OF_CONSERVATION      = 'field_top_conservation';
  const TOP_OF_BUFFER            = 'field_top_buffer';
  const MAX_HYDRAULIC_OUTFLOW    = 'field_max_hydraulic_outflow';
  const INITIAL_STORAGE          = 'field_initial_volume';
  const VOLUME_ELEVATION_CURVE   = 'field_volume_elevation_curve';
  const BUFFER_COEFFICIENT       = 'field_buffer_coefficient';
  const PRIORITY                 = 'field_priority';
  const LOSS_TO_GROUNDWATER      = 'field_loss_groundwater';
  const NET_EVAPORATION          = 'field_net_evaporation';
  const TAILWATER_ELEVATION      = 'field_tailwater_elevation';
  const MAX_TURBINE_FLOW         = 'field_max_turbine_flow';
  const GENERATING_EFFICIENCY    = 'field_generating_efficiency';
  const PLANT_FACTOR             = 'field_plant_factor';
  const HYDROPOWER_PRIORITY      = 'field_hydropower_priority';
  const RIVER                    = 'field_river';
  const FULL_NAME                = 'field_full_name';

  // New.
  const SIMA_CASE_STATUS         = 'field_sima_case_status';
  const PROJECT_ID               = 'field_project_id';
  const TOPOLOGY_ARCID           = 'field_topology_arcid';
  /**
   * {@inheritdoc}
   */
  protected $datasets = [
    'dss_capacidad' => 'Storage Capacity',
    'dss_volumen_muerto' => 'Top of inactive',
    'dss_vol_max_normal' => 'Top of Conservation',
    'dss_vol_min_normal' => 'Top of Buffer',
    'dss_capacidad_descarga' => 'Maximum Hydraulic Outflow',
    'dss_volumen_inicial_acuifero' => 'Initial Storage',
    'dss_curva_vz' => 'Volume Elevation Curve',
    'dss_fraccion_reserva' => 'Buffer Coefficient',
    'dss_prioridad_fuente' => 'Priority',
    'dss_infiltracion' => 'Loss to Groundwater',
    'dss_evaporacion_neta' => 'Net Evaporation',
    'dss_contracarga' => 'Tailwater Elevation',
    'dss_capacidad_turbinas' => 'Max. Turbine Flow',
    'dss_eficiencia' => 'Generating Efficiency',
    'dss_porc_utilizacion_ror' => 'Plant Factor',
    'dss_prioridad_demanda_energia' => 'Hydropower Priority',
    'dss_pertenencia_al_caso_hres' => 'SIMA Case status',
    'dss_project_id_hres' => 'Project_ID',
    'dss_topology_arcid_hres' => 'Topology_ARCID',
  ];

  /**
   * {@inheritdoc}
   */
  protected $referenceDataset = 'dss_capacidad';

  /**
   * Loads a SimaFichaHydropowerPlantDam from a Drupal entity or Entity ID.
   *
   * @param mixed $ficha
   *   The ficha nid or loaded node.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Ficha\SimaFichaHydropowerPlantDam|FALSE
   *   The loaded SimaFichaHydropowerPlantDam object if exists, FALSE otherwise.
   */
  public static function load($ficha) {
    if ($ficha instanceof SimaFichaHydropowerPlantDam) {
      return $ficha;
    }
    else {
      parent::load($ficha);
    }
  }

  /**
   * Creates an new empty Ficha.
   *
   * @param array $project_data
   *   The Project Data.
   * @param array $ficha_data
   *   The Ficha Data.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Ficha\SimaFichaHydropowerPlantDam
   *   The SimaFicha object, FALSE otherwise.
   */
  static public function newFicha($project_data, $ficha_data) {
    global $user;
    // Create the new empty Ficha Node.
    $ficha = static::newEntity('node', self::BUNDLE, $user->uid);

    // Build the node object and save it.
    $ficha->newHydropowerPlantOrDam($project_data, $ficha_data);
    return $ficha;
  }

  /**
   * Returns the Full Branch Name.
   *
   * @return string
   *   The Full Name of this Project.
   */
  public function getFullBranchName() {
    return $this->get(self::FULL_NAME);
  }

  /**
   * Returns the SIMA Case Status.
   *
   * @return string
   *   1 if Enabled, 0 if disabled.
   */
  public function getSimaCaseStatus() {
    return $this->get(self::SIMA_CASE_STATUS);
  }

  /**
   * Returns the Project Id.
   *
   * @return int
   *   The project id
   */
  public function getProjectId() {
    return $this->get(self::PROJECT_ID);
  }

  /**
   * Returns the Full Name.
   *
   * @return string
   *   The Full Name
   */
  public function getFullName() {
    return $this->get(self::FULL_NAME);
  }

  /**
   * Returns the Topology ArcId.
   *
   * @return int
   *   The Topology ArcId
   */
  public function getTopologyArcId() {
    return $this->get(self::TOPOLOGY_ARCID);
  }

  /**
   * Creates a new Hydropower Plant or Dam.
   *
   * @param array $project_data
   *   Project Data.
   * @param array $ficha_data
   *   Ficha Data.
   *
   * @return $this
   *
   * @throws \Exception
   */
  protected function newHydropowerPlantOrDam($project_data, $ficha_data) {
    // Creates a new Project.
    $project = SimaProject::newProject();

    // Assign basic fields to Ficha.
    $level1 = $this->getRowKeyLevel($project_data, 1);
    $level2 = $this->getRowKeyLevel($project_data, 2);
    $level3 = $this->getRowKeyLevel($project_data, 3);
    $level4 = $this->getRowKeyLevel($project_data, 4);
    $full_name = (isset($level3) && isset($level4)) ? $level3 . '\\' . $level4 : t('None');
    $full_name = $level1 . '\\' . $level2 . '\\' . $full_name;
    $river = isset($level3) ? $level3 : t('None');
    $title = isset($level4) ? explode('\\', $level4)[1] : t('None');

    $this->getDrupalEntity()->title = $title;

    // Building body.
    // @codingStandardsIgnoreStart
    //    $header = array_keys(reset($ficha_data));
    //    $rows = array_map('array_values', $ficha_data);
    //    $raw_data = theme('table', array(
    //      'header' => $header,
    //      'rows' => $rows,
    //      'attributes' => array('class' => array('ficha-project')),
    //    ));
    //    $body = '';
    //    // $body = $raw_data;.
    //    $this->getDrupalEntity()->body->set(array(
    //      'value' => $body,
    //      'format' => 'full_html',
    //    ));.
    // @codingStandardsIgnoreEnd
    // Assigning title to Project.
    $project->getDrupalEntity()->title = $title;

    $values = array_column($ficha_data, 'Value', 'Variable');

    // Assigning values.
    $this->setFieldValue(self::STORAGE_CAPACITY, 'Storage Capacity', $values);
    $this->setFieldValue(self::INITIAL_STORAGE, 'Initial Storage', $values);
    $this->setFieldValue(self::NET_EVAPORATION, 'Net Evaporation', $values);
    $this->setFieldValue(self::MAX_HYDRAULIC_OUTFLOW, 'Maximum Hydraulic Outflow', $values);
    $this->setFieldValue(self::LOSS_TO_GROUNDWATER, 'Loss to Groundwater', $values);
    // $this->setFieldValue(self::OBSERVED_VOLUME, 'Observed Volume', $values);.
    $this->setFieldValue(self::TOP_OF_CONSERVATION, 'Top of Conservation', $values);
    $this->setFieldValue(self::TOP_OF_BUFFER, 'Top of Buffer', $values);
    $this->setFieldValue(self::TOP_OF_INACTIVE, 'Top of Inactive', $values);
    $this->setFieldValue(self::BUFFER_COEFFICIENT, 'Buffer Coefficient', $values);
    $this->setFieldValue(self::MAX_TURBINE_FLOW, 'Max. Turbine Flow', $values);
    $this->setFieldValue(self::TAILWATER_ELEVATION, 'Tailwater Elevation', $values);
    $this->setFieldValue(self::PLANT_FACTOR, 'Plant Factor', $values);
    $this->setFieldValue(self::GENERATING_EFFICIENCY, 'Generating Efficiency', $values);
    $this->setFieldValue(self::HYDROPOWER_PRIORITY, 'Hydropower Priority', $values);
    $this->setFieldValue(self::SIMA_CASE_STATUS, 'SIMA Case status', $values);
    $this->set(self::RIVER, $river);
    $this->set(self::FULL_NAME, $full_name);
    $this->setFieldValue(self::PROJECT_ID, 'Project_ID', $values);
    $this->setFieldValue(self::TOPOLOGY_ARCID, 'Topology_ARCID', $values);

    // @codingStandardsIgnoreStart
    // $this->setFieldValue(self::ENERGY_DEMAND, 'Energy Demand', $values);
    // $this->setFieldValue(self::CAPITAL_COSTS, 'Capital Costs', $values);
    // $this->setFieldValue(self::VARIABLE_OPERATING_COSTS, 'Variable Operating Costs', $values);
    // $this->setFieldValue(self::FIXED_OPERATING_COSTS, 'Fixed Operating Costs', $values);
    // $this->setFieldValue(self::VARIABLE_BENEFITS, 'Variable Benefits', $values);
    // $this->setFieldValue(self::FIXED_BENEFITS, 'Fixed Benefits', $values);
    // $this->setFieldValue(self::ELECTRICITY_REVENUE, 'Electricity Revenue', $values);.
    // @codingStandardsIgnoreEnd

    $this->setFieldValue(self::PRIORITY, 'Priority', $values);
    // $this->setFieldValue(self::BUFFER_PRIORITY, 'Buffer Priority', $values);
    // All values have been assigned. Save Project and Ficha.
    $project->save();
    $this->getDrupalEntity()->{self::PROJECT}->set($project->getDrupalEntity()->value());
    $this->save();
    return $this;
  }

}
