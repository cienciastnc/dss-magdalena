<?php

/**
 * @file
 * Models a Sima Resource (or "Caja de Datos").
 */

namespace Drupal\dss_magdalena\DSS\Entity;

use DateTime;
use Drupal\dss_magdalena\DSS\SimaWeapIndex;
use Drupal\dss_magdalena\DSS\Utils\SimaCsvReaderTrait;
use PDO;
use EntityFieldQuery;
use Drupal\dss_magdalena\DSS\Entity\SimaLayer;
use Drupal\dss_magdalena\DSS\Entity\Term\SimaResourceFormat;
use Drupal\dss_magdalena\DSS\Entity\Term\SimaModelFamily;
use Drupal\dss_magdalena\DSS\Entity\Term\SimaVariableType;
use Drupal\dss_magdalena\DSS\Queue\SimaImportQueueItem;

/**
 * Models a Sima Resource (Caja de Datos).
 *
 * Class SimaResource.
 *
 * @package Drupal\dss_magdalena\DSS\Entity
 */
class SimaResource extends EntityNodeAbstract
{

  use SimaCsvReaderTrait;

  const BUNDLE                  = 'resource';
  const UUID                    = 'uuid';
  const FIELD_FULL_NAME         = 'field_full_name';
  const FIELD_CREATION_DATE     = 'field_creation_date';
  const FIELD_DATASETS          = 'field_dataset_ref';
  const FIELD_PROCESSING_TYPE   = 'field_processing_type';
  const FIELD_FORMAT            = 'field_format';
  const FIELD_MODEL_FAMILY      = 'field_model_family';
  const FIELD_MODEL_CASE_ID     = 'field_model_case_study_id';
  const FIELD_MODEL_DECLARATION = 'field_model_declaration';
  const FIELD_LAYER             = 'field_spatial_layer';
  const FIELD_RESOURCE_CSV_FILE = 'field_upload';
  const CSV_FILE_RESOURCE_DIR   = 'public://case_resources/!CASE_ID';

  /**
   * Loads a SimaResource from a Drupal entity or a Drupal entity ID.
   *
   * @param mixed $resource
   *   The nid or loaded object.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaResource|FALSE
   *   The loaded SimaResource object if exists, FALSE otherwise.
   */
  public static function load($resource)
  {
    if ($resource instanceof SimaResource) {
      return $resource;
    } else {
      if ($entity = parent::load($resource)) {
        return ($entity->getDrupalBundle() == self::BUNDLE) ? $entity : FALSE;
      }
      return FALSE;
    }
  }

  /**
   * Get full name of loaded resource
   *
   * @return String full name  
   */
  public function getFullname()
  {
    return $this->get(self::FIELD_FULL_NAME);
  }

  /**
   * Get UUID of loaded resource
   *
   * @return String full name  
   */
  public function getUuid()
  {
    return $this->get(self::UUID);
  }
  /**
   * Loads a list of SimaResources given the Case Study.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaResource[]
   *   An array of loaded SimaResource objects.
   */
  public static function listByCaseStudy($cid)
  {
    $resources = [];
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', self::BUNDLE)
      ->propertyCondition('status', NODE_PUBLISHED)
      ->fieldCondition(self::FIELD_MODEL_CASE_ID, 'target_id', $cid);
    $result = $query->execute();

    if (!empty($result['node'])) {
      foreach ($result['node'] as $node) {
        $resource = static::load($node->nid);
        $resources[$node->nid] = $resource;
      }
    }
    return $resources;
  }

  /**
   * Loads a list of SimaResources given the Case Study.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaResource[]
   *   An array of loaded SimaResource objects.
   */
  public static function listByDataset($did)
  {
    $resources = [];
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', self::BUNDLE)
      ->propertyCondition('status', NODE_PUBLISHED)
      ->fieldCondition(self::FIELD_DATASETS, 'target_id', $did);
    $result = $query->execute();

    if (!empty($result['node'])) {
      foreach ($result['node'] as $node) {
        $resource = static::load($node->nid);
        $resources[$node->nid] = $resource;
      }
    }
    return $resources;
  }

  /**
   * Loads a Resource given a Case Study ID and Machine Name.
   *
   * @param int $cid
   *   The Case Study ID.
   * @param string $dataset_machine_name
   *   The Dataset machine name.
   *
   * @return bool|\Drupal\dss_magdalena\DSS\Entity\SimaResource|FALSE
   *   The Resource if found, FALSE otherwise.
   */
  public static function loadByCaseStudyAndDataset($cid, $dataset_machine_name)
  {
    if ($dataset = SimaDataset::loadByMachineName($dataset_machine_name)) {
      $query = new EntityFieldQuery();
      $query->entityCondition('entity_type', 'node')
        ->entityCondition('bundle', self::BUNDLE)
        ->propertyCondition('status', NODE_PUBLISHED)
        ->fieldCondition(self::FIELD_MODEL_CASE_ID, 'target_id', $cid)
        ->fieldCondition(self::FIELD_DATASETS, 'target_id', $dataset->getId(), '=');
      $result = $query->execute();

      if (!empty($result['node'])) {
        foreach ($result['node'] as $node) {
          // Assuming there is ONLY a single result.
          return static::load($node->nid);
        }
      }
    }
    return FALSE;
  }

  /**
   * Returns the file destination URI for the Resource CSV File.
   *
   * This is the location where the CSV File will be saved.
   *
   * @param int $cid
   *   The Case ID.
   * @param string $filename
   *   The CSV filename.
   *
   * @return string
   *   The destination URI.
   */
  public static function getResourceCsvFileDestinationUri($cid, $filename)
  {
    // Make sure the directories exist, otherwise create them.
    $directories = [
      str_replace('/!CASE_ID', '', self::CSV_FILE_RESOURCE_DIR),
      str_replace('!CASE_ID', $cid, self::CSV_FILE_RESOURCE_DIR),
    ];
    foreach ($directories as $directory) {
      file_prepare_directory($directory, FILE_CREATE_DIRECTORY);
    }
    return str_replace('!CASE_ID', $cid, self::CSV_FILE_RESOURCE_DIR) . '/' . $filename;
  }

  /**
   * Gets all the datasets (variables) associated to this resource.
   *
   * @return array
   *   An array of referenced datasets (variables).
   */
  public function getReferencedVariables()
  {
    return $this->getMultivalued(self::FIELD_DATASETS);
  }

  /**
   * Obtains the Dataset this Resource references to.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaDataset|FALSE
   *   The Dataset if exists or FALSE.
   */
  public function getDataset()
  {
    $datasets = $this->getReferencedVariables();
    $dataset = reset($datasets);
    return SimaDataset::load($dataset);
  }

  /**
   * Obtains the Layer Node.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaLayer
   *   Returns the Spatial Field Entity.
   */
  public function getLayer()
  {
    $spatial_field = $this->get(self::FIELD_LAYER);
    return $layer = SimaLayer::load($spatial_field);
  }

  /**
   * Obtains the Spatial Layer in GeoJson Format.
   *
   * Use this to obtain the layer after it has been attached to the node field.
   * This layer does not have any attributes.
   *
   * @return string|NULL
   *   The Layer in GeoJson format.
   */
  public function getGeoJsonLayer()
  {
    $layer = $this->getLayer();
    if ($layer) {
      return $layer->getGeoJson();
    }
    return NULL;
  }

  /**
   * Obtains the Original GeoJson Spatial Layer.
   *
   * Important: Use this method to get the original layer with all attributes.
   *
   * @return null|string
   *   The Layer in GeoJson format.
   */
  public function getOriginalGeoJsonLayer()
  {
    $layer = $this->getLayer();
    if ($layer) {
      return $layer->getOriginalGeoJson();
    }
    return NULL;
  }

  /**
   * Returns a new Resource.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaResource
   *   The loaded SimaResource object.
   */
  public static function newResource()
  {
    global $user;
    // Create the new Resource Node.
    return self::newEntity('node', self::BUNDLE, $user->uid);
  }

  /**
   * Returns a new Resource.
   *
   * This is a special type of resource. It contains special fields.
   *
   * @param \Drupal\dss_magdalena\DSS\Queue\SimaImportQueueItem $item
   *   The Queue item.
   * @param string $creation_date
   *   The Creation Date.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaResource|bool
   *   The loaded SimaResource object if succeeds, FALSE otherwise.
   */
  public static function newFullyFormedResource(SimaImportQueueItem $item, $creation_date = 'NOW')
  {
    global $user;

    // Check that there is not already a resource assigned to this dataset.
    // We can ONLY have a single resource assigned to this case study for this
    // particular dataset.
    if (SimaResource::loadByCaseStudyAndDataset($item->getCaseStudyId(), $item->getDssMachineName())) {
      return FALSE;
    }

    // Copy file from source to destination and save it to the database.
    $source = [
      'uri' => $item->getFileUri(),
      // Adding additional attributes to file_upload.
      'delimiter' => ',',
      'grid' => 1,
      'graph' => 1,
      'map' => 0,
      'embed' => 0,
    ];
    $source = (object) $source;
    $destination_uri = self::getResourceCsvFileDestinationUri($item->getCaseStudyId(), $item->getFilename());

    // Only proceed if we are able to copy the CSV file to the destination
    // directory.
    if ($file_upload = file_copy($source, $destination_uri, FILE_EXISTS_REPLACE)) {

      // Setting up Creation Date.
      // Entity API cannot set date field values so the 'old' method must
      // be used.
      $creation_date = new DateTime('NOW');
      $timezone = drupal_get_user_timezone();

      // Add Resource CSV File into this Dataset.
      $values = array(
        'type' => self::BUNDLE,
        'uid' => $user->uid,
        'status' => 1,
        'comment' => 0,
        'promote' => 0,
        'revision' => 0,

        // We are setting the file_upload field in advance. It does not work
        // setting it up on the entity_metadata_wrapper because we lose
        // the information about graph, grid, map.
        self::FIELD_RESOURCE_CSV_FILE => array(
          LANGUAGE_NONE => array(
            0 => (array) $file_upload,
          ),
        ),
        self::FIELD_CREATION_DATE => array(
          LANGUAGE_NONE => array(
            0 => array(
              'value' => date_format($creation_date, 'Y-m-d'),
              'timezone' => $timezone,
              'timezone_db' => $timezone,
              'date_type' => 'datetime',
            ),
          ),
        ),
      );
      $node = entity_create('node', $values);
      $entity = entity_metadata_wrapper('node', $node);

      // Obtaining dataset from the variable machine name.
      if ($dataset = SimaDataset::loadByMachineName($item->getDssMachineName())) {

        // Assigning entity reference to dataset.
        $entity->{self::FIELD_DATASETS}[0]->set($dataset->getDrupalEntity()->value());

        // Setting up the Field Layer.
        if ($layer = SimaLayer::loadByDatasetMachineName($item->getDssMachineName())) {
          $entity->{self::FIELD_LAYER}->set($layer->getId());
        }

        // Assigning Model Case ID.
        if ($case_study = SimaCaseStudy::load($item->getCaseStudyId())) {
          $entity->{self::FIELD_MODEL_CASE_ID}->set($case_study->getDrupalEntity()->value());
        }

        // Sets the title like 'WEAP Output: Variable Title - Case Title'.
        // We assume Case Output has the same title as the case study.
        $title = t('!dataset - !case', array(
          '!dataset' => $dataset->getDrupalEntity()->label(),
          '!case' => $case_study ? $case_study->getDrupalEntity()->label() : t('Undefined Case Study'),
        ));

        $full_name = $dataset->getModelingVariable() . ': ' . $title;
        $body = '<p>' . $dataset->getModelingVariable() . ': ' . $title . '</p>';
        $dimension_labels = $dataset->getDimensionLabels();
        if (!empty($dimension_labels)) {
          $body .= '<p>Dimensiones:<br /> <ul>';
          foreach ($dimension_labels as $dimension_label) {
            $body .= '<li>' . $dimension_label . '</li>';
          }
          $body .= '</ul></p>';
        }

        $entity->title = $title;
        $entity->{self::FIELD_FULL_NAME} = $full_name;
        $entity->body->set(array(
          'value' => $body,
          'format' => 'filtered_html',
        ));

        // Obtaining the taxonomy terms to save.
        // Assigning Model Family.
        if ($model_family = SimaModelFamily::loadByModelMachineName($item->getDssMachineName())) {
          $entity->{self::FIELD_MODEL_FAMILY}->set($model_family->getDrupalEntity()->value());

          // Assigning Model Version.
          $entity->{self::FIELD_MODEL_DECLARATION} = SimaModel::WEAP_MODEL_VERSION;
        }

        // Setting up Variable Type as 'Natural'.
        if ($variable_type = SimaVariableType::loadByTaxonomyTermName(SimaVariableType::NAME_NATURAL)) {
          $entity->{self::FIELD_PROCESSING_TYPE}->set($variable_type->getDrupalEntity()->value());
        }

        // Assigning format.
        if ($format = SimaResourceFormat::loadByTaxonomyTermName(SimaResourceFormat::NAME_CSV)) {
          $entity->{self::FIELD_FORMAT}->set($format->getDrupalEntity()->value());
        }
        return new static($entity);
      }
    }
    return FALSE;
  }

  /**
   * Sets a Dataset (Referenced Variable) into the current resource.
   *
   * @param object $dataset
   *   The dataset.
   */
  public function setReferencedVariable($dataset)
  {
    $this->set(self::FIELD_DATASETS, $dataset);
  }

  /**
   * Gets the Processing Type.
   *
   * @return string|NULL
   *   The processing type value if set, NULL otherwise.
   */
  public function getProcessingType()
  {
    if ($processing_type = $this->get(self::FIELD_PROCESSING_TYPE)) {
      return $processing_type->name;
    }
    return NULL;
  }

  /**
   * Gets the Model Family.
   *
   * @return string|NULL
   *   The model family value if set, NULL otherwise.
   */
  public function getModelFamily()
  {
    if ($model_family = $this->get(self::FIELD_MODEL_FAMILY)) {
      return $model_family->name;
    }
    return NULL;
  }

  /**
   * Returns the Resource's creation date.
   *
   * @return string
   *   The Creation Date.
   */
  public function getCreationDate()
  {
    $date = $this->get(self::FIELD_CREATION_DATE);
    return format_date($date);
  }

  /**
   * Sets the Resource CSV File.
   *
   * @param object $file
   *   A file object.
   */
  public function setResourceCsvFile($file)
  {
    // Assigning values for recycle field.
    $file->delimiter = ',';
    $file->grid = 1;
    $file->graph = 1;
    $file->map = 0;
    $file->embed = 0;

    $file_field = dkan_datastore_file_upload_field();

    $this->getDrupalEntity()->value()->{$file_field} = array(
      LANGUAGE_NONE => array(
        0 => (array) $file,
      ),
    );

    node_save($this->getDrupalEntity()->value());
    feeds_node_insert($this->getDrupalEntity()->value());
  }

  /**
   * Obtains the Resource CSV File.
   *
   * @return mixed
   *   The File Object if exists, NULL otherwise.
   */
  public function getResourceCsvFile()
  {
    return $this->get(self::FIELD_RESOURCE_CSV_FILE);
  }

  /**
   * Returns the resource data in the CSV file as an array.
   *
   * @param array|NULL $header
   *   Header Array.
   *
   * @return array
   *   An array of all resource data.
   */
  public function getDataCsv($header = NULL)
  {
    $csv_file = $this->getResourceCsvFile();
    $path = drupal_realpath($csv_file->uri);
    $data = $this->extractRows('filterByNoFilter', $path, $header);
    return iterator_to_array($data);
  }

  /**
   * Process DataStore in a Resource.
   */
  public function processDataStore()
  {
    $resource = $this->getDrupalEntity()->value();
    $form_state = array();
    $form_state['values'] = array(
      'FeedsCSVParser' => array(
        'delimiter' => ',',
        'no_headers' => FALSE,
        'encoding' => 'UTF-8',
      ),
      'FeedsFlatstoreProcessor' => array(
        'geolocate' => FALSE,
        'geolocater' => 'google',
        'geolocate_addresses' => 1,
      ),
      'confirm feeds update' => 1,
      'submit' => 'Import',
      'importer_id' => 'dkan_file',
      'op' => 'Import',
    );
    module_load_include('inc', 'dkan_datastore', 'dkan_datastore.pages');
    drupal_form_submit('dkan_datastore_import_tab_form', $form_state, $resource);
  }

  /**
   * Returns the Status of the Datastore.
   *
   * @return int
   *   DKAN_DATASTORE_EXISTS if datastore exists,
   *   DKAN_DATASTORE_FILE_EXISTS if the file exists but has not been added to
   *     the datastore,
   *   In other cases, the a file has to be added to the resource.
   */
  public function getDatastoreStatus()
  {
    return dkan_datastore_status($this->getDrupalEntity()->value());
  }

  /**
   * Checks whether the Data has been imported into the DataStore.
   *
   * @return bool
   *   TRUE if it has been imported, FALSE otherwise.
   */
  public function isDataStoreImported()
  {
    $status = $this->getDatastoreStatus();
    if ($status == DKAN_DATASTORE_EXISTS) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Obtains the Resource Datastore data.
   *
   * @param string $fields
   *   A string of fields, separated by comma.
   * @param bool $aggregated_data
   *   Obtain aggregated values if true.
   * @param string $by_branch
   *   SIMA Branch ID.
   *
   * @return array|int
   *   An array of results if success, the datastore status if fails.
   */
  public function getDataStoreResults($fields = NULL, $aggregated_data = TRUE, $by_branch = NULL)
  {
    $status = $this->getDatastoreStatus();
    if ($status == DKAN_DATASTORE_EXISTS) {
      // Creating a cache to store results.
      try {
        // Fields to be requested.
        $default_fields = 'branchid,geobranchid,year,timestep,value';
        /** @var \Drupal\dss_magdalena\DSS\SimaWeapIndex $weap */
        $fields = isset($fields) ? $fields : $default_fields;

        if (isset($by_branch)) {
          $weap = SimaWeapIndex::loadByFullBranchName($by_branch);
        } else {
          $weap = SimaWeapIndex::loadAllRecords(TRUE);
        }

        // Ask DB for information.
        $table = dkan_datastore_api_tablename($this->getUuid());
        $data = db_query("SELECT $fields FROM $table")->fetchAll(PDO::FETCH_ASSOC);

        $data = array_values($weap->combineWithWeap($data));

        // Also obtain the maximum/minimum values for all data.
        if ($aggregated_data) {
          // @TODO: The 'where clause' was taken out. Fix it.
          // Otherwise this won't coincide with the data.
          $fields = 'max(year) as max_year, min(year) as min_year, max(value) as max_value, min(value) as min_value, max(timestep) as max_timestep, min(timestep) as min_timestep';
          $data_ext = db_query("SELECT $fields FROM $table")->fetchAll(PDO::FETCH_ASSOC);
        } else {
          $data_ext = array();
        }

        return [
          'title' => $this->getDrupalEntity()->label(),
          'description' => t('Full Data'),
          'link' => '',
          'items' => $data,
          'ext' => reset($data_ext),
        ];
      } catch (\Exception $ex) {
        return $status;
      }
    } elseif ($status == DKAN_DATASTORE_FILE_EXISTS) {
      $datastore = dkan_datastore_go($this->getUuid());
      $source = $datastore->source();
      $data = (array) $source->preview();
      $data['title'] = $this->getDrupalEntity()->label();
      $data['description'] = t('This preview data was obtained from the CSV file.');
      $data['ext'] = array();
      return $data;
    }

    // If there is no file uploaded then return the datastore status.
    return $status;
  }

  /**
   * Obtains the new Resource Datastore data.
   *
   * @param string $fields
   *   A string of fields, separated by comma.
   * @param bool $aggregated_data
   *   Obtain aggregated values if true.
   * @param string $by_branch
   *   SIMA Branch ID.
   *
   * @return array|int
   *   An array of results if success, the datastore status if fails.
   */
  public function getDataStoreResultsNew($fields = NULL, $aggregated_data = TRUE, $by_branch = NULL)
  {
    $status = $this->getDatastoreStatus();
    if ($status == DKAN_DATASTORE_EXISTS) {
      // Creating a cache to store results.
      try {
        $default_fields = 'branchid,geobranchid,year,timestep,value';
        $fields = isset($fields) ? $fields : $default_fields;

        // Sets where clause defined by "by_branch" parameter.
        $where_field = '';
        if (isset($by_branch)) {
          // Where clause.
          $where_field = "WHERE branchid='" . $by_branch . "'";
        }

        // Ask DB for information.
        $table = dkan_datastore_api_tablename($this->getUuid());
        $data = db_query("SELECT $fields FROM $table $where_field")->fetchAll(PDO::FETCH_ASSOC);

        // Also obtain the maximum/minimum values for all data.
        if ($aggregated_data) {
          $fields = 'max(year) as max_year, min(year) as min_year, max(value) as max_value, min(value) as min_value, max(timestep) as max_timestep, min(timestep) as min_timestep';
          $data_ext = db_query("SELECT $fields FROM $table $where_field")->fetchAll(PDO::FETCH_ASSOC);
        } else {
          $data_ext = array();
        }

        return [
          'title' => $this->getDrupalEntity()->label(),
          'description' => t('Full Data'),
          'link' => '',
          'items' => $data,
          'ext' => reset($data_ext),
        ];
      } catch (\Exception $ex) {
        return $status;
      }
    } elseif ($status == DKAN_DATASTORE_FILE_EXISTS) {
      // Getting data from CSV file.
      $header_array = ["branchid", "geobranchid", "year", "timestep", "value"];
      $data_array = $this->getDataCsv($header_array);
      array_shift($data_array);

      // Filter data by fields.
      if (($fields != NULL) && (strcmp(trim($fields), '*') != 0)) {
        $fields_requested = array_map('trim', explode(",", $fields));
        $array_filtered_fields = array();
        foreach ($data_array as $key => $value_array) {
          $item_found = array();
          foreach ($fields_requested as $keys_value) {
            if (array_key_exists($keys_value, $value_array)) {
              $item_found[$keys_value] = $value_array[$keys_value];
            }
          }
          array_push($array_filtered_fields, $item_found);
        }
        $data_array = $array_filtered_fields;
      }

      // Filter data by branch.
      if ($by_branch != NULL) {
        $array_filtered_branch = array_filter($data_array, function ($test_array, $by_branch) {
          if (array_key_exists("branchid", $test_array)) {
            if (strcmp($test_array["branchid"], $by_branch) == 0) {
              return TRUE;
            }
          }
          return FALSE;
        });
        $data_array = $array_filtered_branch;
      }

      // Extended data.
      $data_ext = array();
      if ($aggregated_data) {
        // Get an element to test.
        $test_array_element = reset($data_array);

        // Calculate extended data.
        $max_year = NULL;
        $min_year = NULL;
        if (array_key_exists("year", $test_array_element) && (strcmp(trim($test_array_element["year"]), "") != 0)) {

          $numbers = array_map('floatval', array_column($data_array, 'year'));
          $max_year = max($numbers) . "";
          $min_year = min($numbers) . "";
        }
        $max_value = NULL;
        $min_value = NULL;
        if (array_key_exists("value", $test_array_element) && (strcmp(trim($test_array_element["value"]), "") != 0)) {
          $numbers = array_map('floatval', array_column($data_array, 'value'));
          $max_value = max($numbers) . "";
          $min_value = min($numbers) . "";
        }
        $max_timestep = NULL;
        $min_timestep = NULL;
        if (array_key_exists("timestep", $test_array_element) && (strcmp(trim($test_array_element["timestep"]), "") != 0)) {
          $numbers = array_map('floatval', array_column($data_array, 'timestep'));
          $max_timestep = max($numbers) . "";
          $min_timestep = min($numbers) . "";
        }

        // Sets extended data array.
        $data_ext = [
          "max_year" => $max_year,
          "min_year" => $min_year,
          "max_value" => $max_value,
          "min_value" => $min_value,
          "max_timestep" => $max_timestep,
          "min_timestep" => $min_timestep,
        ];
      }

      // Return results.
      return [
        'title' => $this->getDrupalEntity()->label(),
        'description' => t('Full Data From CSV'),
        'link' => '',
        'items' => $data_array,
        'ext' => $data_ext,
      ];
    }

    // If there is no file uploaded then return the datastore status.
    return $status;
  }

  /**
   * Get the Case Study associated to the resource.
   *
   * @return bool
   *   TRUE if it has been imported, FALSE otherwise.
   */
  public function getCaseStudy()
  {
    if ($case_study = $this->get(self::FIELD_MODEL_CASE_ID)) {
      $case_study_obj = SimaCaseStudy::load($case_study);
      if ($case_study_obj) {
        return $case_study_obj;
      }
    }
    return FALSE;
  }

  /**
   * Obtains the Resource Datastore data.
   *
   * @return array
   *   An array of results if success, the datastore status if fails.
   */
  public function getWeapLevelData()
  {
    $data = array();
    $status = $this->getDatastoreStatus();
    if ($status == DKAN_DATASTORE_EXISTS) {
      // Creating a cache to store results.
      try {
        // Ask DB for information.
        $table = dkan_datastore_api_tablename($this->getUuid());

        // Gets weap level.
        $dataset = $this->getDataset();
        $num_weap_level = intval($dataset->getWeapLevel());
        if ($num_weap_level > 4) {
          $num_weap_level = 4;
        }

        // Make DB Request.
        for ($i = 2; $i < $num_weap_level; $i++) {
          $fields = 'level_' . strval($i + 1);
          $conditions = 'level_' . strval($i + 1) . " <> '' OR level_" . strval($i + 1) . ' IS NOT NULL';
          $data_row = db_query("SELECT DISTINCT $fields FROM $table WHERE $conditions")->fetchAll(PDO::FETCH_COLUMN, 0);
          array_push($data, $data_row);
        }

        return $data;
      } catch (\Exception $ex) {
        return array();
      }
    }

    // If there is no file uploaded then return the datastore status.
    return $data;
  }

  /**
   * Obtains the Resource Datastore data.
   *
   * @return array
   *   An array of results if success, the datastore status if fails.
   */
  public function getDataDimension()
  {
    $data = array();
    $status = $this->getDatastoreStatus();
    if ($status == DKAN_DATASTORE_EXISTS) {
      // Creating a cache to store results.
      try {
        // Ask DB for information.
        $table = dkan_datastore_api_tablename($this->getUuid());

        // Make DB Request.
        $data_row = db_query("SELECT value FROM $table limit 1")->fetchAll(PDO::FETCH_ASSOC);
        if (!isset($data_row[0]['value'])) {
          return $data;
        }
        $data_value_example = $data_row[0]['value'];

        // Check its content is a json.
        $json_result = json_decode($data_value_example, TRUE);
        if (json_last_error() != JSON_ERROR_NONE) {
          return $data;
        }
        if (!is_array($json_result)) {
          return $data;
        }
        $data = array_keys($json_result);

        return $data;
      } catch (\Exception $ex) {
        return array();
      }
    }

    // If there is no file uploaded then return the datastore status.
    return $data;
  }
}
