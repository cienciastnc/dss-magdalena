<?php
/**
 * @file
 * Models the Water Balance Consumption for a Water Balance Template.
 */

namespace Drupal\dss_magdalena\DSS\Entity\Multifield;

use Drupal\dss_magdalena\DSS\Entity\SimaDataset;

/**
 * Class SimaWaterBalanceConsumption.
 *
 * @package Drupal\dss_magdalena\DSS\Entity\Multifield
 */
class SimaWaterBalanceConsumption extends EntityMultifieldAbstract {

  const BUNDLE                            = 'field_consumption';

  const FIELD_CONSUMPTION_VARIABLE        = 'field_consvar';
  const FIELD_CONSUMPTION_FACTOR          = 'field_consfac';

  /**
   * Loads a SimaWaterBalanceConsumption from a Drupal entity or an entity ID.
   *
   * @param mixed $water_balance_consumption
   *   The multifield id or loaded object.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Multifield\SimaWaterBalanceConsumption|FALSE
   *   A loaded SimaWaterBalanceConsumption object if exists, FALSE otherwise.
   */
  public static function load($water_balance_consumption) {
    if ($water_balance_consumption instanceof SimaWaterBalanceConsumption) {
      return $water_balance_consumption;
    }
    else {
      if ($entity = parent::load($water_balance_consumption)) {
        return ($entity->getDrupalBundle() == self::BUNDLE) ? $entity : FALSE;
      }
      return FALSE;
    }
  }

  /**
   * Returns the Variable (Dataset).
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaDataset|FALSE
   *   The SimaDataset object loaded if found, FALSE otherwise.
   */
  public function getVariable() {
    $consumption_variable = $this->get(self::FIELD_CONSUMPTION_VARIABLE);
    return SimaDataset::load($consumption_variable);
  }

  /**
   * Gets the Factor.
   *
   * @return string
   *   The Factor.
   */
  public function getFactor() {
    return $this->get(self::FIELD_CONSUMPTION_FACTOR);
  }

}
