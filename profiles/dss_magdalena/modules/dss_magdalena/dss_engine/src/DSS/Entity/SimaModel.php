<?php
/**
 * @file
 * Defines a SimaModel.
 */

namespace Drupal\dss_magdalena\DSS\Entity;

use Drupal\dss_magdalena\DSS\Entity\Term\SimaVariableStatus;
use Drupal\dss_magdalena\DSS\Entity\SimaDataset;
use EntityFieldQuery;
use Drupal\dss_magdalena\DSS\Entity\File\SimaStreamslineTopologyFile;
use Drupal\dss_magdalena\DSS\Entity\File\SimaTerritorialFootprintFile;
use Drupal\dss_magdalena\DSS\Entity\File\SimaElohaQEcoTable;
// Use Drupal\dss_magdalena\WeapController;.
/**
 * Models a Sima Model.
 *
 * Class SimaModel.
 *
 * @package Drupal\dss_magdalena\DSS\Entity
 */
class SimaModel extends EntityNodeAbstract {

  const BUNDLE                      = 'model';
  const REQUIRED_VARIABLES          = 'field_required_variables';
  const MACHINE_NAME                = 'field_code';
  const WEAP_MODEL_VERSION          = 'MAGDALENA_V2.1.6.1';

  // List of ALL models machine_names (model codes) defined in Sima.
  const MODEL_WEAP                  = 'weap';
  const MODEL_ELOHA                 = 'eloha';
  const MODEL_FRAGMENTATION         = 'fragmentation';
  const MODEL_DOR_H                 = 'dor_h';
  const MODEL_DOR_W                 = 'dor_w';
  const MODEL_SEDIMENTS             = 'sediments';
  const MODEL_TERRITORIAL_FOOTPRINT = 'territorial_footprint';

  const MODEL_TIER1                 = 'tier1';
  const MODEL_TIER2                 = 'tier2';

  // Lists all the Model Readable Names.
  const MODEL_WEAP_NAME                  = 'Hidrología';
  const MODEL_ELOHA_NAME                 = 'Biota Fluvial';
  const MODEL_FRAGMENTATION_NAME         = 'Fragmentación';
  const MODEL_DOR_H_NAME                 = 'DOR-H';
  const MODEL_DOR_W_NAME                 = 'DOR-W';
  const MODEL_SEDIMENTS_NAME             = 'Sedimentos';
  const MODEL_TERRITORIAL_FOOTPRINT_NAME = 'Huella Territorial';

  // Model Status.
  const STATUS_PROCESSING           = 101;
  const STATUS_LABEL_PROCESSING     = 'PROCESSING';
  const STATUS_OK                   = 102;
  const STATUS_LABEL_OK             = 'OK';

  // CSS CLASSES.
  const CSS_MODELING_STATUS         = 'dss-engine-modeling-status';
  const CSS_STATUS_OK               = 'dss_engine-modeling-status-ok';
  const CSS_STATUS_PROCESSING       = 'dss_engine-modeling-status-processing';
  const CSS_STATUS_READY            = 'dss_engine-modeling-status-ready';
  const CSS_STATUS_NOT_READY        = 'dss_engine-modeling-status-not-ready';

  /**
   * Loads a SimaModel from a Drupal entity or a Drupal entity ID.
   *
   * @param mixed $model
   *   The nid or loaded object.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaModel|FALSE
   *   The loaded SimaModel object if exists, FALSE otherwise.
   */
  public static function load($model) {
    if ($model instanceof SimaModel) {
      return $model;
    }
    else {
      if ($entity = parent::load($model)) {
        return ($entity->getDrupalBundle() == self::BUNDLE) ? $entity : FALSE;
      }
      return FALSE;
    }
  }

  /**
   * Validates if the given model machine name is valid.
   *
   * @param string $model
   *   The model machine name.
   *
   * @return bool
   *   TRUE if this is a valid model machine name, FALSE otherwise.
   */
  public static function isValidModel($model) {
    return in_array($model, array(
      self::MODEL_WEAP,
      self::MODEL_ELOHA,
      self::MODEL_FRAGMENTATION,
      self::MODEL_DOR_H,
      self::MODEL_DOR_W,
      self::MODEL_SEDIMENTS,
      self::MODEL_TERRITORIAL_FOOTPRINT,
    ));
  }

  /**
   * Whether this is a WEAP model or not.
   *
   * @return bool
   *   TRUE if it is WEAP, FALSE otherwise.
   */
  public function isWeap() {
    return $this->getMachineName() == self::MODEL_WEAP;
  }


  /**
   * List all models machine names.
   *
   * @return array
   *   A list of all model machine names.
   */
  public static function listModels() {
    return array(
      self::MODEL_WEAP,
      self::MODEL_ELOHA,
      self::MODEL_FRAGMENTATION,
      self::MODEL_DOR_H,
      self::MODEL_DOR_W,
      self::MODEL_SEDIMENTS,
      self::MODEL_TERRITORIAL_FOOTPRINT,
    );
  }

  /**
   * List all "Tier 1" models.
   *
   * @return array
   *   A list of all model keyed by machine names.
   */
  public static function listTier1Models() {
    return array(
      self::MODEL_FRAGMENTATION => self::MODEL_FRAGMENTATION_NAME,
      self::MODEL_DOR_H => self::MODEL_DOR_H_NAME,
      self::MODEL_DOR_W => self::MODEL_DOR_W_NAME,
      self::MODEL_SEDIMENTS => self::MODEL_SEDIMENTS_NAME,
      self::MODEL_TERRITORIAL_FOOTPRINT => self::MODEL_TERRITORIAL_FOOTPRINT_NAME,
    );
  }

  /**
   * List all "Tier 2" models.
   *
   * @return array
   *   A list of all model keyed by machine names.
   */
  public static function listTier2Models() {
    return array(
      self::MODEL_WEAP => self::MODEL_WEAP_NAME,
      self::MODEL_ELOHA => self::MODEL_ELOHA_NAME,
      self::MODEL_FRAGMENTATION => self::MODEL_FRAGMENTATION_NAME,
      self::MODEL_DOR_H => self::MODEL_DOR_H_NAME,
      self::MODEL_DOR_W => self::MODEL_DOR_W_NAME,
      self::MODEL_SEDIMENTS => self::MODEL_SEDIMENTS_NAME,
      self::MODEL_TERRITORIAL_FOOTPRINT => self::MODEL_TERRITORIAL_FOOTPRINT_NAME,
    );
  }

  /**
   * Returns the Status Label, having the Status Code.
   *
   * @param string|int $status_code
   *   The model status code.
   * @param bool $pretty_print
   *   Print the modeling status with appropriate CSS class.
   *
   * @return int|string
   *   The Status Label.
   */
  public static function getStatusLabel($status_code, $pretty_print = FALSE) {
    $status_code = intval($status_code);
    $status = 0;
    $css = self::CSS_STATUS_NOT_READY;
    if (0 <= $status_code && $status_code <= self::STATUS_OK) {
      switch ($status_code) {
        case self::STATUS_OK:
          $status = self::STATUS_LABEL_OK;
          $css = self::CSS_STATUS_OK;
          break;

        case self::STATUS_PROCESSING:
          $status = self::STATUS_LABEL_PROCESSING;
          $css = self::CSS_STATUS_PROCESSING;
          break;

        case 100:
          $status = $status_code;
          $css = self::CSS_STATUS_READY;
          break;

        default:
          $status = $status_code;
          $css = self::CSS_STATUS_NOT_READY;
      }
    }
    if ($pretty_print) {
      return theme('dss_engine_modeling_status',
        array(
          'css_status' => self::CSS_MODELING_STATUS,
          'css_specific' => $css,
          'status' => is_int($status) ? $status . '%' : $status,
        ));
    }
    return $status;
  }

  /**
   * Returns the Model Machine Name.
   *
   * @return string
   *   The Model Machine Name.
   */
  public function getMachineName() {
    return $this->get(self::MACHINE_NAME);
  }

  /**
   * Loads a model given the machine_name.
   *
   * @param string $model_machine_name
   *   The model machine_name.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaModel|FALSE
   *   The loaded SimaModel object if exists, FALSE otherwise.
   */
  public static function loadByMachineName($model_machine_name) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', self::BUNDLE)
      ->propertyCondition('status', NODE_PUBLISHED)
      ->fieldCondition(self::MACHINE_NAME, 'value', $model_machine_name, '=');
    $result = $query->execute();

    if (isset($result['node'])) {
      $node = reset($result['node']);
      return new static($node->nid);
    }
    return FALSE;
  }

  /**
   * Gets all the required variables for this model to run.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaDataset[]
   *   An array of SimaDatasets required for this model to run.
   */
  public function getRequiredDatasets() {
    $datasets = $this->getMultivalued(self::REQUIRED_VARIABLES);

    $output = [];

    // @TODO: FIX for WETLAND Management.
    // @TODO: REMOVE THIS AFTER FIXING WETLAND MANAGEMENT PROJECTS
    $filtered_wetlands_management = [
      'dss_umbral_desborde',
      'dss_fraccion_caudal_desborde',
      'dss_retorno_desborde',
      'dss_distribucion_inundacion',
    ];

    foreach ($datasets as $key => $dataset) {
      $dataset = SimaDataset::load($dataset);
      if (!in_array($dataset->getMachineName(), $filtered_wetlands_management)) {
        $output[] = $dataset;
      }
    }
    return $output;
  }

  /**
   * Obtains list of required input datasets needed to run model.
   *
   * Obtains the list of required Dataset Machine Names that would need to
   * exist for a particular case to run the specific model.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaDataset[]
   *   An array of all model input datasets.
   */
  public function getRequiredInputDatasets() {
    $required_variables_status = [];
    switch ($this->getMachineName()) {
      case self::MODEL_WEAP:
        $required_variables_status = array(
          SimaVariableStatus::NAME_INPUT,
          SimaVariableStatus::NAME_INPUT_SCENARIO,
          SimaVariableStatus::NAME_INPUT_ALTERNATIVES,
          SimaVariableStatus::NAME_INPUT_CONTEXT,
          // @codingStandardsIgnoreStart
          //  'Infobase',
          //  'Initial State',
          //  'Parameter',
          //  'Observation',
          // 'Output',
          // 'Output WEAP',
          // 'Output ELOHA',
          // @codingStandardsIgnoreEnd
        );
        break;

      case self::MODEL_ELOHA:
        $required_variables_status = array(
          SimaVariableStatus::NAME_OUTPUT_WEAP,
        );
        break;

      case self::MODEL_FRAGMENTATION:
        $required_variables_status = array(
          SimaVariableStatus::NAME_INPUT,
          SimaVariableStatus::NAME_INPUT_SCENARIO,
          SimaVariableStatus::NAME_INPUT_ALTERNATIVES,
          SimaVariableStatus::NAME_INPUT_CONTEXT,
        );
        break;

      case self::MODEL_DOR_H:
      case self::MODEL_DOR_W:
      case self::MODEL_SEDIMENTS:
        $required_variables_status = array(
          SimaVariableStatus::NAME_INPUT,
          SimaVariableStatus::NAME_INPUT_SCENARIO,
          SimaVariableStatus::NAME_INPUT_ALTERNATIVES,
          SimaVariableStatus::NAME_INPUT_CONTEXT,
        );
        break;

      case self::MODEL_TERRITORIAL_FOOTPRINT:
        $required_variables_status = array(
          SimaVariableStatus::NAME_INPUT,
          SimaVariableStatus::NAME_INPUT_SCENARIO,
          SimaVariableStatus::NAME_INPUT_ALTERNATIVES,
          SimaVariableStatus::NAME_INPUT_CONTEXT,
        );
        break;
    }
    $required_datasets = [];

    // Obtaining the list of Variable Status we need to check for.
    $variable_status_tids = array();
    foreach ($required_variables_status as $taxonomy_name) {
      if ($variable_status = SimaVariableStatus::loadByTaxonomyTermName($taxonomy_name)) {
        $variable_status_tids[] = $variable_status->getId();
      }
    }

    // For every required dataset, check that it is a required Input, as
    // determined by the variable_status_tids.
    $datasets = $this->getRequiredDatasets();
    foreach ($datasets as $dataset) {
      if ($var_status = $dataset->getVariableStatus()) {
        if (in_array($var_status->getId(), $variable_status_tids)) {
          $required_datasets[] = $dataset;
        }
      }
    }

    return $required_datasets;
  }

  /**
   * Check if the model is configured.
   *
   * @return bool
   *   Model configured
   */
  public static function isModelProperlyConfigured($model_machine_name) {
    $configuration_file = '';
    switch ($model_machine_name) {
      case SimaModel::MODEL_WEAP:
        $settings = dss_engine_weap_client_load_settings();
        if (!empty($settings['webdav_username'])) {
          return TRUE;
        }
        break;

      case SimaModel::MODEL_FRAGMENTATION:
      case SimaModel::MODEL_DOR_H:
      case SimaModel::MODEL_DOR_W:
      case SimaModel::MODEL_SEDIMENTS:
        $configuration_file = SimaStreamslineTopologyFile::loadFromSystem();
        break;

      case SimaModel::MODEL_TERRITORIAL_FOOTPRINT:
        $configuration_file = SimaTerritorialFootprintFile::loadFromSystem();
        break;

      case SimaModel::MODEL_ELOHA:
        $configuration_file = SimaElohaQEcoTable::loadFromSystem();
        break;
    }
    if (is_object($configuration_file)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Obtains the readable name of the model.
   *
   * @param string $model_machine_name
   *   The model machine name.
   *
   * @return string
   *   The readable human name of the model.
   */
  public static function getModelName($model_machine_name) {
    switch ($model_machine_name) {
      case SimaModel::MODEL_WEAP:
        return self::MODEL_WEAP_NAME;

      case SimaModel::MODEL_FRAGMENTATION:
        return self::MODEL_FRAGMENTATION_NAME;

      case SimaModel::MODEL_DOR_H:
        return self::MODEL_DOR_H_NAME;

      case SimaModel::MODEL_DOR_W:
        return self::MODEL_DOR_W_NAME;

      case SimaModel::MODEL_SEDIMENTS:
        return self::MODEL_SEDIMENTS_NAME;

      case SimaModel::MODEL_TERRITORIAL_FOOTPRINT:
        return self::MODEL_TERRITORIAL_FOOTPRINT_NAME;

      case SimaModel::MODEL_ELOHA:
        return self::MODEL_ELOHA_NAME;

      default:
        return strtoupper($model_machine_name);
    }
  }

  /**
   * Prints the model description.
   *
   * @param string $model_machine_name
   *   The model machine name.
   *
   * @return null|string
   *   The description.
   */
  public static function getModelDescription($model_machine_name) {
    switch ($model_machine_name) {
      case SimaModel::MODEL_WEAP:
        return t('WEAP es un modelo de Sistemas de Recursos Hídricos que incorpora un componente de modelación hidrológica precipitación-escorrentía. 
        </br></br>A partir del clima (series temporales de precipitación, temperatura, humedad), el uso del suelo actual y de las decisiones que pueden modificarlo (desarrollo de proyectos productivos agrícola, mineros y forestales industriales), determina la hidrología de cada subcuenca. También está previsto el rol de una variable de escenario que expresa el cambio de uso del suelo no controlado.
        </br></br>
Con la hidrología, la presencia y manejo de represas, la demanda de agua por uso agrícola y domestico, WEAP determina los flujos en la red hidrológica y artificial consecuente (series de tiempo de paso mensual, para todo el horizonte de planificación, actualmente de aprox. 30 años). </br></br>
Con estos flujos y las demandas puede determinar indicadores de satisfacción de abastecimiento.</br></br>
Con el régimen hidrológico y la presencia y manejo de las plantas hidroeléctricas, WEAP determina la producción de energía hidroeléctrica y, con el escenario de demanda, es posible determinar el objetivo de seguridad energética.</br></br>
Con el régimen hídrico (6) se tiene uno de los factores causales importantes para determinar la alteración de ecosistemas acuáticos-ríos, aunque evaluar la alteración en si’ es tarea de otro modelo (ELOHA); análogamente, con el régimen hídrico (en particular los intercambios ríos-humedales), modificado por las decisiones de control de inundaciones para el manejo de humedales, se determinan indicadores importantes para el estado ecológico de ecosistemas acuáticos-humedales (objeto de otro modulo aun no implementado).</br></br>

<b>Limitaciones</b>:</br>
Aunque WEAP permite incorporar de cierta forma el intercambio cauces-flujos hiporreicos (sub-superficiales) , en ocasiones muy importante, en esta aplicación no ha sido incorporado por falta de información.
Analogamente, el intercambio con acuíferos ha sido incorporado de forma muy limitada.
Manejo del sistema: esta’ basado en prioridades asignadas a priori (primero el civil, luego el riego, luego el hidroeléctrico); pero cada represa hidroeléctrica es tratada como si fuera un ulterior centro de demanda hídrica donde la demanda es determinada previamente como aquella que maximizaría su producción si fuera aislado; este enfoque es cuestionable.
</br></br>
El gráfico a continuación, explica las variables contempladas en SIMA.


');

      case SimaModel::MODEL_FRAGMENTATION:
        return t('FRAGMENTACION no es propiamente un modelo, sino más bien un algoritmo que explora la topología de la red hidrológica (o sea la representación cartográfica de la red hidrológica, dividida en tramos de distintos orden de Strahler con sus conexiones lógicas: quien le sigue a cada tramo), para determinar cuáles partes de la misma no sufren de interrupciones de continuidad (no son fragmentadas) y con ello calcula una serie de indicadores útiles (por ej. la longitud máxima de subred conectada).

Limitaciones: al momento sólo considera la presencia y posición (tramo afectado) de las represas. NO distingue el tipo de barrera (o sea su “permeabilidad” –en particular la presencia de aparatos de remonta para peces- que sin embargo puede fácilmente ser incluido, como también la presencia de saltos naturales, si se cuenta con la información necesaria). No incluye otros factores que pueden determinar de hecho fragmentaciones (ej. cambios abruptos de calidad del agua).');

      case SimaModel::MODEL_DOR_H:
        return t('DOR-H (Degree Of Regulation Hydrological) es un indicador que en cada tramo de la red hidrologica expresa el potencial de alteracion del regimen natural consecuente a la presencia de embalses : cuanto mas grande es la capacidad total aguas arriba con respecto al flujo natural en esa seccion, tanto mas grande es la alteracion potencial.
Obviamente es un indice aproximado que puede inclusive ser distorsionado en el caso los embalses se utilicen para mantener una carga hidraulica alta (o sea llenos, sin aprovechar el volumen embalsado en epoca de estiaje) en cual caso desde el punto de vista de la regulacion es como si no existieran.
Ademas de la configuracion del sistema hidroelectricos (ubicacion y capacidad de los embalses), DOR-H necesita el regimen hidrico de referencia (mas precisamente el caudal promedio en cada tramo de rio del regimen hidrico inalterado o «pristino»). Por tanto, en rigor, este debiera ser una elaboracion del output del mismo modelo hidrologico de que se disponga en el aparato de modelacion: solo de esta forma, seria coherente y ademas permitiria ver los efectos del cambio climatico. Sin embargo, como la idea es utilizar DOR justamente como herramienta de analisis exploratorio simplificado, no tiene sentido implicar la corrida de un modelo muy pesado como lo es el hidrologico. Es sensato entonces adoptar una hidrologia aproximada construida con un modelo simplificado pre-corrido; la desventaja (ademas de la menor precision) es que de esta forma el analisis ya seria insensible al cambio climatico. Es esta la forma adoptada actualmente, donde la hidrologia es deterinada con un simple metodo que va sumando la precipitacion neta (menos la evapotranspiracion) en cada subcuencua, acumulando progresivamente aguas abajo en la red hidrologica.');

      case SimaModel::MODEL_DOR_W:
        return t('DOR-W es del todo analogo, pero pondera el efecto de los embalses con la razon del caudal regulado (es el caudal promedio natural de la seccion del primer embalse aguas arriba en el tramo considerado con respecto a la seccion de interes en ese tramo; abajo de una confluencia es la suma de los Q regulados en los dos tramos) con respecto al caudal promedio natural en la seccion considerada. Resulta por definicion siempre DOR-W <= DOR-H');

      case SimaModel::MODEL_SEDIMENTS:
        return t('SEDIMENTOS-0 (“Transporte_solido”) es un modelo básico y preliminar de balance de sedimentos que incluye:
El aporte de laderas por erosion (a través de la ecuación RUSLE modificada)
La captura por parte de embalses (con formula de eficiencia de atrapamiento función del tiempo de retención)
El aporte de tributarios
y produce como output un indicador de alteración (%) del transporte solido natural y otros derivados (como longitud de red con afectación encima de cierto umbral).

Limitaciones:
- No considera muchas mas cosas como el aporte por carcavas (“gullies”), las perdidas por desbordamientos, la perdida por deposición en el mismo cauce o el aporte debido a su incisión, ni los aportes por divagación en planicies con niveles diferenciados. Es necesario seguir con refinamientos. Por ello, no se atreve a brindar como output el caudal solido, sino solo el impacyo sobre ello (disminución %) que es un poco mas creíble.
- Tampoco incluye la asociada modificación morfológica de los cauces (solo se cuenta con una primera idea de enfoque conceptual para ello, desarrollada en el ámbito de la iniciativa TNC “Mesa geomorfológica”).
- Utiliza como base hidrológica los caudales naturales de largo plazo (no los asociados a la real configuración de embalses, usos, y uso del suelo de un caso de estudio dado); es decir , sufre del mismo problema de DOR. En la versión implementada, la hidrología es asumida como exógena (determinada con un modelo simplificado) y por tanto no es coherente con la que se obtenga del modelo hidrológico adoptado en el DSS y es insensible a escenarios de cambio climático.');

      case SimaModel::MODEL_TERRITORIAL_FOOTPRINT:
        return t('Huella no es propiamente un modelo, sino más bien un algoritmo que explora la posición y magnitud de proyectos de desarrollo y con ello pretende contabilizar el impacto que la ocupación de superficie en la cuenca por parte de proyectos areales, como lo es por ejemplo un embalse con su área de inundación, conllevan en términos de áreas sensibles. Lo determina como superposición con capas de varios tipos de bienes territoriales.
Entre estas tenemos: zonas de protección o de alto valor ambiental (ej. el Portafolio de conservación de TNC), usos del suelo que se quiere mantener (ej. bosque natural), o bien aspectos sociales como rancherías que no se quisiera afectar o zonas con cierto uso del suelo socio-económicamente valioso.
 
La respuesta, además de mapas de impacto (huellas), es un conjunto de indicadores que traducen numéricamente lo mismo.
 
 
Limitaciones:
- al momento (DSS 2.0), TNC sólo se ha concentrado en el efecto provocado por los embalses (zona sumergida) ; pero el sistema esta’ diseñado para incluir el efecto de otros proyectos, en particular los de uso productivo (agrícola, minero, forestal). Será tarea de la Etapa II.
- Cabe recordar además que el actual Huella disponible utiliza un algoritmo simplificado “off-line” que aprovecha una tabla de intersecciones espaciales ya pre-calculadas para un conjunto numeroso de posibles localizaciones de represas (“llenado” local del DTM en correspondencia de varias altura de cada represa + intersección de las superficies resultantes con distintas capas de interés) y requiere de una interpolación entre alturas disponibles, por tanto no maneja la localización espacial de los elementos geográficos (superficies embalses, áreas de intersección). Por tanto, por un lado no permite la visualización espacial de las “huellas” (realizando el “llenado DEM” y luego simplemente permitiendo superponer en visualización las demás capas para observar donde se superponen), sino solo el cálculo de indicadores numéricos.
- Por otro lado, y más importante, se corre el peligro de que con el tiempo se vaya actualizando las capas de uso del suelo y valores anexos, pero quede en el olvido la correspondiente y coherente actualización de la tabla de intersecciones, con el resultado de producir indicadores no certeros.');

      case SimaModel::MODEL_ELOHA:
        return t('ELOHA (Ecological Limits of Hydrologic Alteration) es un esquema de evaluación de la alteración del ecosistema fluvial basado en el conocimiento de expertos de TNC. </br></br>Una vez determinado el régimen hídrico (el conjunto de series de tiempo de flujos en todos los tramos de interés) correspondiente a una alterativa de desarrollo dada, y asumido un régimen hídrico de referencia, determina una batería de indicadores que ilustran las alteraciones y permiten calcular un índice “veredicto” y una tabla de síntesis verbal.
</br></br>
Limitaciones:
La principal es que solo considera el régimen hídrico; no ve nada de otros importantísimos factores, como por ejemplo la calidad del agua, el estado de las zonas ripariales, la modificación del transporte sólido, las alteraciones físicas (como la configuración de obras artificiales presentes o actividades de dragado o extracción de áridos) y la consecuente alteración de los procesos geomorfológicos consecuentes, o el manejo de la actividad pesquera.
Ademas, solo ve el régimen hídrico en términos de paso mensual, lo que puede obscurecer muchos procesos entre los cuales seguramente el de hydropeaking.
</br></br>
El gráfico a continuación, explica las variables contempladas en SIMA.

');

      case SimaModel::MODEL_TIER1:
        return t('Nivel de Analisis Tear 1
(con linea punteada se presentan las relaciones ya previstas , pero aun no implementadas en EtapaI)
NOTA: este nivel de analisis no es necesariamente producto de la sumatoria de los grafos de los modelos involucrados porque algunos elementos se determinan con relaciones estaticas sencillas que no alcanzan el rango de «modelo», sino mas bien de indices; por ejemplo, en este caso (sin modelo hidrologico y del Sistema de Recursos Hidricos), la produccion hidro-electrica es simplemente determinada a partir de la capacidad instalada y asume el significado de «produccion potencial».
');

      case SimaModel::MODEL_TIER2:
        return t('NIVEL Basico (Tier 2)

Es el conjunto de modelos Tier 1 mas los dependientes de la modelación hidrológica: WEAP y ELOHA. Referirse a cada modelo para entender capacidades y limitaciones.
Se destaca que esta combinación contiene una cierta redundancia porque en particular la alteración del régimen hídrico estimada por los modelos DOR (H,W) es un proxy de lo determinado (supuestamente con mas precisión y detalle) por el verdadero modelo hidrológico (aquí WEAP).
');
    }
  }

}
