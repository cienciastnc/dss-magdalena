<?php
/**
 * @file
 * Implements a model for SimaElohaQEcoTable files.
 */

namespace Drupal\dss_magdalena\DSS\Entity\File;

use Drupal\dss_magdalena\DSS\Utils\SimaCsvReaderTrait;

/**
 * Class SimaElohaQEcoTable.
 *
 * @package Drupal\dss_magdalena\DSS\Entity\File
 */
class SimaElohaQEcoTable extends EntityFileAbstract {

  use SimaCsvReaderTrait;

  const BUNDLE                       = 'file';
  const BUNDLE_UNDEFINED             = 'undefined';
  const VAR_QECO_TABLE               = 'dss_engine_eloha_qeco_table_fid';

  /**
   * Defines the header for the imported file.
   *
   * @var array
   */
  protected $requiredHeaders = [
    'Tipo_de_rio_elevacion',
    'Tipo_de_regimen',
    'Grupo',
    'Sub_grupo',
    'Fase ciclo de vida',
    'Factor',
    'Mes inicio',
    'Mes fin',
    'Componente_Flujo',
    'Atributo caudal',
    'Umbral alerta',
    'Umbral crítico',
    'Exceso/deficit',
    'Relación atributo variable respuesta-caudal',
  ];

  /**
   * Directories that should be initialized.
   *
   * @var array
   */
  protected $directories = [
    self::DATA_DIR,
  ];

  /**
   * Loads a  from a Drupal entity or a Drupal entity ID.
   *
   * @param mixed $file
   *   The file fid or loaded object.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\File\SimaElohaQEcoTable|FALSE
   *   The loaded SimaElohaQEcoTable object if exists, FALSE otherwise.
   */
  public static function load($file) {
    if ($file instanceof SimaElohaQEcoTable) {
      return $file;
    }
    else {
      $entity = parent::load($file);

      // We are also considering BUNDLE_UNDEFINED because files might have this
      // value set if it is not known its bundle.
      return (in_array($entity->getDrupalBundle(), [self::BUNDLE, self::BUNDLE_UNDEFINED])) ? $entity : FALSE;
    }
  }

  /**
   * Loads a SimaElohaQEcoTable from a Drupal variable.
   *
   * @return bool|\Drupal\dss_magdalena\DSS\Entity\File\SimaElohaQEcoTable|FALSE
   *   The loaded SimaElohaQEcoTable object if succeeds, FALSE
   *   otherwise.
   */
  public static function loadFromSystem($auto_detect_line_endings = 1) {
    $fid = variable_get(self::VAR_QECO_TABLE, FALSE);
    if ($fid) {
      $file = static::load($fid);
      $file->setAutodetectLineEndings($auto_detect_line_endings);
      return $file;
    }
    else {
      $file = new static(NULL);
      $file->setAutodetectLineEndings($auto_detect_line_endings);
      return $file->initializeDirectories();
    }
  }

  /**
   * Saves the current.
   *
   * @return \EntityDrupalWrapper
   *   The Drupal Entity wrapper.
   */
  public function save() {
    variable_set(self::VAR_QECO_TABLE, $this->getId());
    return parent::save();
  }

  /**
   * Save from Temporal File.
   *
   * @param object $file
   *   The Temporal file.
   *
   * @return bool|\Drupal\dss_magdalena\DSS\Entity\File\SimaElohaQEcoTable|FALSE
   *   The loaded SimaElohaQEcoTable object if succeeds, FALSE
   *   otherwise.
   */
  public static function saveFromTemporalFile($file) {
    // Move the temporary file into the final location.
    if ($file = file_move($file, SimaElohaQEcoTable::DATA_DIR, FILE_EXISTS_REPLACE)) {
      $file->status = FILE_STATUS_PERMANENT;
      $file = file_save($file);

      // Now that the file is properly saved. Notify the class to save it.
      variable_set(self::VAR_QECO_TABLE, $file->fid);
      return new static($file);
    }
    return FALSE;
  }

  /**
   * Returns the file type.
   *
   * @return string
   *   The File Type.
   */
  public function getFileType() {
    return $this->get('type');
  }

  /**
   * Verifies that ALL the required headers are contained in this CSV File.
   */
  public function verifyRequiredHeaders() {
    $file_path = drupal_realpath($this->getUri());
    $headers = $this->extractHeader($file_path);
    foreach ($this->requiredHeaders as $h) {
      if (!in_array($h, $headers)) {
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Obtains the ELOHA Q-Eco Table.
   */
  public function getData() {
    $file_path = drupal_realpath($this->getUri());

    // Extract the header information.
    $headers = $this->extractHeader($file_path);

    // Converting headers to machine names.
    foreach ($headers as $key => $header) {
      $headers[$key] = preg_replace('@[^a-z0-9-]+@', '_', strtolower($header));
    }
    $results = $this->extractRows('filterByNoFilter', $file_path, $headers);
    return iterator_to_array($results, FALSE);
  }

}
