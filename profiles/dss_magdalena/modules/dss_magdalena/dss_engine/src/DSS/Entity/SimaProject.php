<?php
/**
 * @file
 * Models a Sima Project.
 */

namespace Drupal\dss_magdalena\DSS\Entity;

/**
 * Models a Sima Project.
 *
 * Class SimaProject.
 *
 * @package Drupal\dss_magdalena\DSS\Entity
 */
class SimaProject extends EntityNodeAbstract {

  const BUNDLE = 'project';

  /**
   * Loads a SimaProject from a Drupal entity or a Drupal entity ID.
   *
   * @param mixed $project
   *   The nid or loaded object.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaProject|FALSE
   *   The loaded SimaProject object if exists, FALSE otherwise.
   */
  public static function load($project) {
    if ($project instanceof SimaProject) {
      return $project;
    }
    else {
      if ($entity = parent::load($project)) {
        return ($entity->getDrupalBundle() == self::BUNDLE) ? $entity : FALSE;
      }
      return FALSE;
    }
  }

  /**
   * Returns a new Project.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaProject
   *   The loaded SimaProject.
   */
  static public function newProject() {
    global $user;
    // Create the new Project Node.
    return self::newEntity('node', self::BUNDLE, $user->uid);
  }

}
