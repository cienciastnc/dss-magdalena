<?php
/**
 * @file
 * Models a Sima Objective.
 */

namespace Drupal\dss_magdalena\DSS\Entity;

use EntityFieldQuery;

/**
 * Models a Sima Objective.
 *
 * Class SimaObjective.
 *
 * @package Drupal\dss_magdalena\DSS\Entity
 */
class SimaObjective extends EntityNodeAbstract {

  const BUNDLE                    = 'selected_objectives';

  const FIELD_AUTHOR              = 'uid';
  const FIELD_SELECTED_OBJECTIVES = 'field_selected_objectives';

  /**
   * Loads a SimaObjective from a Drupal entity or a Drupal entity ID.
   *
   * @param mixed $objective
   *   The nid or loaded object.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaObjective|FALSE
   *   The loaded SimaObjective object if exists, FALSE otherwise.
   */
  public static function load($objective) {
    if ($objective instanceof SimaObjective) {
      return $objective;
    }
    else {
      if ($entity = parent::load($objective)) {
        return ($entity->getDrupalBundle() == self::BUNDLE) ? $entity : FALSE;
      }
      return FALSE;
    }
  }

  /**
   * Returns a new Objective.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaObjective
   *   The loaded SimaObjective.
   */
  static public function newProject() {
    global $user;
    // Create the new Objective Node.
    return self::newEntity('node', self::BUNDLE, $user->uid);
  }

  /**
   * Loads a SimaObjective by current user.
   *
   * @return bool|\Drupal\dss_magdalena\DSS\Entity\SimaObjective
   *   A loaded SimaObjective object or FALSE.
   */
  static public function loadByCurrentUser() {
    global $user;
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', self::BUNDLE)
      ->propertyCondition('status', NODE_PUBLISHED)
      ->propertyCondition(self::FIELD_AUTHOR, $user->uid)
      ->propertyOrderBy('changed', 'ASC');
    $result = $query->execute();

    if (isset($result['node'])) {
      // Here we are only taking the first result because we are only interested
      // in the only one oldest node.
      $node = reset($result['node']);
      if (!empty($node)) {
        return new static($node->nid);
      }
    }
    return FALSE;
  }

  /**
   * Returns a list of Datasets for the selected Objectives.
   *
   * @param string $lang
   *   The language code.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaDataset[]
   *   An array of SimaDataset objects.
   */
  public function getSelectedObjectives($lang = LANGUAGE_NONE) {
    $datasets = $this->get(self::FIELD_SELECTED_OBJECTIVES, $lang);
    foreach ($datasets as $key => $dataset) {
      $datasets[$key] = SimaDataset::load($dataset);
    }
    return $datasets;
  }

}
