<?php
/**
 * @file
 * Models a Sima Water Balance.
 */

namespace Drupal\dss_magdalena\DSS\Entity;

use PDO;
use Drupal\dss_magdalena\DSS\Entity\SimaWaterBalanceTemplate;
use Drupal\dss_magdalena\DSS\Entity\SimaResource;
use Drupal\dss_magdalena\DSS\Entity\SimaCaseStudy;

/**
 * Models a Sima Water Balance.
 *
 * Class SimaWaterBalance.
 *
 * @package Drupal\dss_magdalena\DSS\Entity
 */
class SimaWaterBalance extends EntityNodeAbstract {

  const BUNDLE                       = 'water_balance';

  const TABLE_PREFIX                 = 'dss_wb_';

  // Const FIELD_TEMPLATE               = 'field_wb_template_type';.
  const FIELD_TYPE                   = 'field_water_balance_type';
  const FIELD_MODEL                  = 'field_model';
  const FIELD_CASE_STUDY             = 'field_case_study';

  const WATER_BALANCE_INPUT          = 'input';
  const WATER_BALANCE_OUTPUT         = 'output';
  const WATER_BALANCE_STORAGE        = 'storage';
  const WATER_BALANCE_CONSUMPTION    = 'consumption';

  /**
   * Loads a SimaWaterBalance from a Drupal entity or a Drupal entity ID.
   *
   * @param mixed $water_balance
   *   The nid or loaded object.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaWaterBalance|FALSE
   *   The loaded SimaWaterBalance object if exists, FALSE otherwise.
   */
  public static function load($water_balance) {
    if ($water_balance instanceof SimaWaterBalance) {
      return $water_balance;
    }
    else {
      if ($entity = parent::load($water_balance)) {
        return ($entity->getDrupalBundle() == self::BUNDLE) ? $entity : FALSE;
      }
      return FALSE;
    }
  }

  /**
   * Returns a new WaterBalance.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaWaterBalance
   *   The loaded SimaWaterBalance.
   */
  static public function newWaterBalance() {
    global $user;
    // Create the new WaterBalanceTemplate Node.
    return self::newEntity('node', self::BUNDLE, $user->uid);
  }

  /**
   * Obtains the Water Balance Template associated to this Water Balance.
   *
   * @return bool|\Drupal\dss_magdalena\DSS\Entity\SimaWaterBalanceTemplate|FALSE
   *   The Water Balance Template if exists, FALSE otherwise.
   */
  /*
  public function getWaterBalanceTemplate() {
  if ($water_balance_template = $this->get(self::FIELD_TEMPLATE)) {
  return SimaWaterBalanceTemplate::load($water_balance_template);
  }
  return FALSE;
  }
   */
  /**
   * Obtains the Case Study associated to this Water Balance.
   *
   * @return bool|\Drupal\dss_magdalena\DSS\Entity\SimaCaseStudy|FALSE
   *   The Case Study if exists, FALSE otherwise.
   */
  public function getCaseStudy() {
    if ($case_study = $this->get(self::FIELD_CASE_STUDY)) {
      return SimaCaseStudy::load($case_study);
    }
    return FALSE;
  }

  /**
   * Obtains the Model associated to this Water Balance.
   *
   * @return bool|\Drupal\dss_magdalena\DSS\Entity\SimaModel|FALSE
   *   The Model if exists, FALSE otherwise.
   */
  public function getModel() {
    if ($model = $this->get(self::FIELD_MODEL)) {
      return SimaModel::load($model);
    }
    return FALSE;
  }

  /**
   * Obtains the Model associated to this Water Balance.
   *
   * @return bool|\Drupal\dss_magdalena\DSS\Entity\SimaModel|FALSE
   *   The Model if exists, FALSE otherwise.
   */
  public function getWaterBalanceType() {
    if ($water_balance_type = $this->get(self::FIELD_TYPE)) {
      return $water_balance_type;
    }
    return FALSE;
  }
  /**
   * Generate the list of Resources associated to this Water Balance.
   *
   * @return array
   *   An array of SimaResources organized by type.
   */
  public function getWaterBalanceResources() {
    $water_balance_template = $this->getWaterBalanceTemplate();
    $case_study = $this->getCaseStudy();
    $resources = self::listWaterBalanceResources($water_balance_template, $case_study->getId());
    return $resources;
  }

  /**
   * Obtains a list of Water Balance Resources per type.
   *
   * @param \Drupal\dss_magdalena\DSS\Entity\SimaWaterBalanceTemplate $water_balance_template
   *   A Water Balance Template object.
   * @param int $cid
   *   The Case Study ID.
   *
   * @return array
   *   The list of SimaResources organized by type.
   */
  public static function listWaterBalanceResourcesw(SimaWaterBalanceTemplate $water_balance_template, $cid) {
    $resources_list = [];

    // Make sure data is given.
    if (!isset($water_balance_template) || !is_numeric($cid)) {
      return $resources_list;
    }

    // Build a list of.
    $multifield_list = [
      'input' => $water_balance_template->getWaterBalanceInput(),
      'output' => $water_balance_template->getWaterBalanceOutput(),
      'storage' => $water_balance_template->getWaterBalanceStorage(),
      'consumption' => $water_balance_template->getWaterBalanceConsumption(),
    ];

    foreach ($multifield_list as $type => $multifields) {
      $resources = [];
      foreach ($multifields as $multifield) {
        $dataset = $multifield->getVariable();
        if ($dataset) {
          $resources[] = SimaResource::loadByCaseStudyAndDataset($cid, $dataset->getMachineName());
        }
      }
      $resources_list[$type] = $resources;
    }
    return $resources_list;
  }

  /**
   * Obtains a list of Water Balance Resources per type.
   *
   * @param string $water_balance_type_id
   *   A Water Balance Type ID.
   * @param string $model_id
   *   The model ID.
   * @param int $case_id
   *   The Case Study ID.
   * @param string $type
   *   The type of resources.
   *
   * @return array
   *   The list of SimaResources organized by type.
   */
  public static function listWaterBalanceResources($water_balance_type_id, $model_id, $case_id, $type = 'all') {
    $query = db_select('field_data_field_water_balance_type', 't');
    $result = $query
      ->fields('t', array('entity_id'))
      ->condition('field_water_balance_type_tid', $water_balance_type_id)
      ->condition('bundle', 'water_balance_template')
      ->execute();

    if (!empty($result)) {
      foreach ($result as $water_balance_template) {
        $water_balance_template = SimaWaterBalanceTemplate::load($water_balance_template->entity_id);
        if ($water_balance_template->getModelId() == $model_id) {
          break;
        }
      }
    }

    $resources_list = [];

    // Make sure data is given.
    if (!isset($water_balance_template) || !is_numeric($case_id)) {
      return $resources_list;
    }

    // Build a list of.
    switch ($type) {
      case 'all':
        $multifield_list = [
          'input' => $water_balance_template->getWaterBalanceInput(),
          'output' => $water_balance_template->getWaterBalanceOutput(),
          'storage' => $water_balance_template->getWaterBalanceStorage(),
          'consumption' => $water_balance_template->getWaterBalanceConsumption(),
        ];
        break;

      case 'input':
        $multifield_list = [
          'input' => $water_balance_template->getWaterBalanceInput(),
        ];
        break;

      case 'output':
        $multifield_list = [
          'output' => $water_balance_template->getWaterBalanceOutput(),
        ];
        break;

      case 'storage':
        $multifield_list = [
          'storage' => $water_balance_template->getWaterBalanceStorage(),
        ];
        break;

      case 'consumption':
        $multifield_list = [
          'consumption' => $water_balance_template->getWaterBalanceConsumption(),
        ];
        break;
    }

    foreach ($multifield_list as $type => $multifields) {
      $resources = [];
      foreach ($multifields as $multifield) {
        $dataset = $multifield->getVariable();
        if ($dataset) {
          $resources[] = SimaResource::loadByCaseStudyAndDataset($case_id, $dataset->getMachineName());
        }
      }
      $resources_list[$type] = $resources;
    }
    return $resources_list;
  }

  /**
   * Obtains a list of outputs not imported per type.
   *
   * @param string $water_balance_type_id
   *   A Water Balance Type ID.
   * @param string $model_id
   *   The model ID.
   * @param int $case_id
   *   The Case Study ID.
   * @param string $type
   *   The type of resources.
   *
   * @return array
   *   The list of SimaResources organized by type.
   */
  public static function listWaterBalanceNotImported($water_balance_type_id, $model_id, $case_id, $type = 'all') {
    $query = db_select('field_data_field_water_balance_type', 't');
    $result = $query
      ->fields('t', array('entity_id'))
      ->condition('field_water_balance_type_tid', $water_balance_type_id)
      ->condition('bundle', 'water_balance_template')
      ->execute();

    if (!empty($result)) {
      foreach ($result as $water_balance_template) {
        $water_balance_template = SimaWaterBalanceTemplate::load($water_balance_template->entity_id);
        if ($water_balance_template->getModelId() == $model_id) {
          break;
        }
      }
    }

    $resources_list = [];

    // Make sure data is given.
    if (!isset($water_balance_template) || !is_numeric($case_id)) {
      return $resources_list;
    }

    // Build a list of.
    switch ($type) {
      case 'all':
        $multifield_list = [
          'input' => $water_balance_template->getWaterBalanceInput(),
          'output' => $water_balance_template->getWaterBalanceOutput(),
          'storage' => $water_balance_template->getWaterBalanceStorage(),
          'consumption' => $water_balance_template->getWaterBalanceConsumption(),
        ];
        break;

      case 'input':
        $multifield_list = [
          'input' => $water_balance_template->getWaterBalanceInput(),
        ];
        break;

      case 'output':
        $multifield_list = [
          'output' => $water_balance_template->getWaterBalanceOutput(),
        ];
        break;

      case 'storage':
        $multifield_list = [
          'storage' => $water_balance_template->getWaterBalanceStorage(),
        ];
        break;

      case 'consumption':
        $multifield_list = [
          'consumption' => $water_balance_template->getWaterBalanceConsumption(),
        ];
        break;
    }

    foreach ($multifield_list as $type => $multifields) {
      $resources = [];
      foreach ($multifields as $multifield) {
        $dataset = $multifield->getVariable();
        if ($dataset) {
          $resource = SimaResource::loadByCaseStudyAndDataset($case_id, $dataset->getMachineName());
          if (!$resource->isDataStoreImported()) {
            $resources[] = $resource;
          }
        }
      }
      $resources_list[$type] = $resources;
    }
    return $resources_list;
  }

  /**
   * Returns the Water Balance Template Model and Type.
   *
   * @param string $model_id
   *   The model ID.
   * @param string $water_balance_type_id
   *   The Water Balance Type Id.
   *
   * @return bool|\Drupal\dss_magdalena\DSS\Entity\Alternatives\SimaAlternativeInterface|\Drupal\dss_magdalena\DSS\SimaAlternative|\Drupal\dss_magdalena\DSS\SimaScenario
   *   The Sima Alternative.
   */
  public static function getWaterBalanceTemplateByModelAndType($model_id, $water_balance_type_id) {
    // Search for Case Outputs.
    $query = db_select('field_data_field_water_balance_type', 't');
    $result = $query
      ->fields('t', array('entity_id'))
      ->condition('field_water_balance_type_tid', $water_balance_type_id)
      ->condition('bundle', 'water_balance_template')
      ->execute();

    if (!empty($result)) {
      foreach ($result as $wb_balance_template) {
        $water_balance_template = SimaWaterBalanceTemplate::load($wb_balance_template->entity_id);
        if ($water_balance_template->getModelId() == $model_id) {
          return $water_balance_template;
        }
      }
    }
    return FALSE;
  }

  /**
   * Sets all the data store tables for Water Balance Data.
   */
  public function setWaterBalanceDataStore() {
    $water_balance_types = [
      self::WATER_BALANCE_INPUT,
      self::WATER_BALANCE_OUTPUT,
      self::WATER_BALANCE_CONSUMPTION,
      self::WATER_BALANCE_STORAGE,
    ];
    foreach ($water_balance_types as $water_balance_type) {
      // Define the datastore table name.
      $table_name = self::TABLE_PREFIX . $water_balance_type . '_' . $this->getId();
      // Build datastore table.
      $this->buildDataStoreTableSchema($table_name);
    }
  }

  /**
   * Obtains the Water Balance Input Data.
   *
   * @return array
   *   An array with water balance input data.
   */
  public function getWaterBalanceInput() {
    return $this->getWaterBalanceDataStore(self::WATER_BALANCE_INPUT);
  }

  /**
   * Inserts values into the Water Balance Input Variable.
   *
   * @param array $values
   *   An array of the values to store in the table.
   *   The values in each row has to be keyed with these:
   *   [
   *     'branchid',
   *     'branch',
   *     'level_1',
   *     'level_2',
   *     'level_3',
   *     'level_4',
   *     'year',
   *     'timestep',
   *     'variable',
   *     'units',
   *     'scenario',
   *     'value',
   *   ].
   *
   * @return bool|\DatabaseStatementInterface|int
   *   The number of entities inserted or FALSE.
   */
  public function setWaterBalanceInput($values) {
    return $this->setWaterBalanceDataStoreValues(self::WATER_BALANCE_INPUT, $values);
  }

  /**
   * Obtains the Water Balance Output Data.
   *
   * @return array
   *   An array with water balance output data.
   */
  public function getWaterBalanceOutput() {
    return $this->getWaterBalanceDataStore(self::WATER_BALANCE_OUTPUT);
  }

  /**
   * Inserts values into the Water Balance Output Variable.
   *
   * @param array $values
   *   An array of the values to store in the table.
   *   The values in each row has to be keyed with these:
   *   [
   *     'branchid',
   *     'branch',
   *     'level_1',
   *     'level_2',
   *     'level_3',
   *     'level_4',
   *     'year',
   *     'timestep',
   *     'variable',
   *     'units',
   *     'scenario',
   *     'value',
   *   ].
   *
   * @return bool|\DatabaseStatementInterface|int
   *   The number of entities inserted or FALSE.
   */
  public function setWaterBalanceOutput($values) {
    return $this->setWaterBalanceDataStoreValues(self::WATER_BALANCE_OUTPUT, $values);
  }

  /**
   * Obtains the Water Balance Storage Data.
   *
   * @return array
   *   An array with water balance storage data.
   */
  public function getWaterBalanceStorage() {
    return $this->getWaterBalanceDataStore(self::WATER_BALANCE_STORAGE);
  }

  /**
   * Inserts values into the Water Balance Storage Variable.
   *
   * @param array $values
   *   An array of the values to store in the table.
   *   The values in each row has to be keyed with these:
   *   [
   *     'branchid',
   *     'branch',
   *     'level_1',
   *     'level_2',
   *     'level_3',
   *     'level_4',
   *     'year',
   *     'timestep',
   *     'variable',
   *     'units',
   *     'scenario',
   *     'value',
   *   ].
   *
   * @return bool|\DatabaseStatementInterface|int
   *   The number of entities inserted or FALSE.
   */
  public function setWaterBalanceStorage($values) {
    return $this->setWaterBalanceDataStoreValues(self::WATER_BALANCE_STORAGE, $values);
  }

  /**
   * Obtains the Water Balance Consumption Data.
   *
   * @return array
   *   An array with water balance consumption data.
   */
  public function getWaterBalanceConsumption() {
    return $this->getWaterBalanceDataStore(self::WATER_BALANCE_CONSUMPTION);
  }

  /**
   * Inserts values into the Water Balance Consumption Variable.
   *
   * @param array $values
   *   An array of the values to store in the table.
   *   The values in each row has to be keyed with these:
   *   [
   *     'branchid',
   *     'branch',
   *     'level_1',
   *     'level_2',
   *     'level_3',
   *     'level_4',
   *     'year',
   *     'timestep',
   *     'variable',
   *     'units',
   *     'scenario',
   *     'value',
   *   ].
   *
   * @return bool|\DatabaseStatementInterface|int
   *   The number of entities inserted or FALSE.
   */
  public function setWaterBalanceConsumption($values) {
    return $this->setWaterBalanceDataStoreValues(self::WATER_BALANCE_CONSUMPTION, $values);
  }

  /**
   * Deletes all the data store tables for Water Balance Data.
   */
  public function deleteWaterBalanceDataStore() {
    $water_balance_types = [
      self::WATER_BALANCE_INPUT,
      self::WATER_BALANCE_OUTPUT,
      self::WATER_BALANCE_CONSUMPTION,
      self::WATER_BALANCE_STORAGE,
    ];
    foreach ($water_balance_types as $water_balance_type) {
      // Define the datastore table name.
      $table_name = self::TABLE_PREFIX . $water_balance_type . '_' . $this->getId();
      if (db_table_exists($table_name)) {
        db_drop_table($table_name);
      }
    }
  }

  /**
   * Obtains the data array from the Water Balance DataStore.
   *
   * @param string $water_balance_type
   *   The Water Balance type.
   *
   * @return array
   *   An array of all the datastore.
   */
  protected function getWaterBalanceDataStore($water_balance_type) {
    // Initializing data.
    $data = [];
    $data_ext = [[]];

    // Define the datastore table name.
    $table_name = self::TABLE_PREFIX . $water_balance_type . '_' . $this->getId();

    if (db_table_exists($table_name)) {
      // Obtain all data from the table.
      $data = db_query("SELECT * FROM {$table_name} ORDER BY branch,year,timestep")->fetchAll(PDO::FETCH_ASSOC);

      // Also obtain the maximum/minimum values for all data.
      $fields = 'max(year) as max_year, min(year) as min_year, max(value) as max_value, min(value) as min_value, max(timestep) as max_timestep, min(timestep) as min_timestep';
      $data_ext = db_query("SELECT $fields FROM {$table_name}")->fetchAll(PDO::FETCH_ASSOC);
    }

    // Return an array with all compiled data.
    return [
      'title' => $this->getDrupalEntity()->label(),
      'description' => t('Water Balance Data'),
      'link' => url('node/' . $this->getId(), array('absolute' => TRUE)),
      'items' => $data,
      'ext' => reset($data_ext),
    ];
  }

  /**
   * Sets Values into the DataStore.
   *
   * @param string $water_balance_type
   *   The Water Balance Type (INPUT, OUTPUT, STORAGE, CONSUMPTION).
   * @param array $values
   *   An array of the values to store in the table.
   *   The values in each row has to be keyed with these:
   *   [
   *     'branchid',
   *     'branch',
   *     'level_1',
   *     'level_2',
   *     'level_3',
   *     'level_4',
   *     'year',
   *     'timestep',
   *     'variable',
   *     'units',
   *     'scenario',
   *     'value',
   *   ].
   *
   * @return bool|\DatabaseStatementInterface|int
   *   The number of entities inserted or FALSE.
   */
  protected function setWaterBalanceDataStoreValues($water_balance_type, array $values) {
    $table_name = self::TABLE_PREFIX . $water_balance_type . '_' . $this->getId();
    return $this->insertValues($table_name, $values);
  }

  /**
   * Inserts Values into a table.
   *
   * @param string $table_name
   *   The table name.
   * @param array $values
   *   An array of the values to store in the table.
   *   The values in each row has to be keyed with these:
   *   [
   *     'branchid',
   *     'branch',
   *     'level_1',
   *     'level_2',
   *     'level_3',
   *     'level_4',
   *     'year',
   *     'timestep',
   *     'variable',
   *     'units',
   *     'scenario',
   *     'value',
   *   ].
   *
   * @return bool|\DatabaseStatementInterface|int
   *   The number of entities inserted or FALSE.
   *
   * @throws \Exception
   *   If error, report.
   */
  protected function insertValues($table_name, array $values) {
    if (count($values) == 0) {
      return 0;
    }
    try {
      // First delete existent values.
      db_delete($table_name)->execute();

      // Insert all values.
      $query = db_insert($table_name)->fields(array(
        'branchid',
        'branch',
        'level_1',
        'level_2',
        'level_3',
        'level_4',
        'year',
        'timestep',
        'variable',
        'units',
        'scenario',
        'value',
      ));
      foreach ($values as $record) {
        $query->values($record);
      }
      return $query->execute();
    }
    catch (\Exception $ex) {
      drupal_set_message(t('Could not save values into table "%table": (Error = "%error")', array(
        '%table' => $table_name,
        '%error' => $ex->getMessage(),
      )), 'error');
      return FALSE;
    }
  }

  /**
   * Builds a Water Balance Table.
   *
   * @param string $table_name
   *   The Table name.
   */
  protected function buildDataStoreTableSchema($table_name) {
    $schema[$table_name] = array(
      'description' => 'Table for storing information about static media asset .',
      'fields' => array(
        'branchid' => array(
          'type' => 'int',
          'length' => 1,
          'not null' => TRUE,
          'default' => 0,
          'description' => 'Branch ID.',
        ),
        'branch' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'default' => '',
          'description' => 'Branch Full Name',
        ),
        'level_1' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'default' => '',
          'description' => 'WEAP Level 1',
        ),
        'level_2' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'default' => '',
          'description' => 'WEAP Level 2',
        ),
        'level_3' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'default' => '',
          'description' => 'WEAP Level 3',
        ),
        'level_4' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'default' => '',
          'description' => 'WEAP Level 4',
        ),
        'year' => array(
          'type' => 'int',
          'length' => 1,
          'not null' => TRUE,
          'default' => 0,
          'description' => 'Year.',
        ),
        'timestep' => array(
          'type' => 'int',
          'length' => 1,
          'not null' => TRUE,
          'default' => 0,
          'description' => 'Timestep.',
        ),
        'variable' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'default' => '',
          'description' => 'Variable.',
        ),
        'units' => array(
          'type' => 'varchar',
          'length' => 32,
          'not null' => TRUE,
          'default' => '',
          'description' => 'Units.',
        ),
        'scenario' => array(
          'type' => 'varchar',
          'length' => 32,
          'not null' => TRUE,
          'default' => '',
          'description' => 'Scenario.',
        ),
        'value' => array(
          'type' => 'varchar',
          'length' => 64,
          'not null' => TRUE,
          'default' => '',
          'description' => 'Value.',
        ),
      ),
      'indexes' => array(
        'branchtime' => array('branch', 'year', 'timestep'),
      ),
    );
    if (!db_table_exists($table_name)) {
      db_create_table($table_name, $schema[$table_name]);
    }
  }

}
