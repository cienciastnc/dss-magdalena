<?php
/**
 * @file
 * Models Sima Case Outputs.
 */

namespace Drupal\dss_magdalena\DSS\Entity;

use EntityFieldQuery;

/**
 * Models Sima Case Study Outputs.
 *
 * Class SimaCaseOutputs.
 *
 * @package Drupal\dss_magdalena\DSS\Entity
 */
class SimaCaseOutputs extends EntityNodeAbstract {

  const BUNDLE                              = 'case_outputs';
  const FIELD_CASE_STUDY_REF                = 'field_case_study_outputs';

  const FIELD_WEAP_STATUS                   = 'field_weap_status';
  const FIELD_ELOHA_STATUS                  = 'field_eloha_status';
  const FIELD_FRAGMENTATION_STATUS          = 'field_fragmentation_status';
  const FIELD_DOR_H_STATUS                  = 'field_dor_h_status';
  const FIELD_DOR_W_STATUS                  = 'field_dor_w_status';
  const FIELD_SEDIMENTS_STATUS              = 'field_sediments_status';
  const FIELD_TERRITORIAL_FOOTPRINT_STATUS  = 'field_territorial_footprint_stat';

  const FIELD_WEAP_OUTPUTS                  = 'field_weap_outputs';
  const FIELD_ELOHA_OUTPUTS                 = 'field_eloha_outputs';
  const FIELD_FRAGMENTATION_OUTPUTS         = 'field_fragmentation_outputs';
  const FIELD_DOR_H_OUTPUTS                 = 'field_dor_h_outputs';
  const FIELD_DOR_W_OUTPUTS                 = 'field_dor_w_outputs';
  const FIELD_SEDIMENTS_OUTPUTS             = 'field_sediments_outputs';
  const FIELD_TERRITORIAL_FOOTPRINT_OUTPUTS = 'field_territorial_print_outputs';

  const STATUS_IMPORTED                     = 'IMPORTED';
  const STATUS_NOT_IMPORTED                 = 'NOT IMPORTED';

  /**
   * Loads a SimaCaseOutputs from a Drupal entity or a Drupal entity ID.
   *
   * @param mixed $case_output
   *   The nid or loaded object.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaCaseOutputs|FALSE
   *   The loaded SimaCaseOutputs object if exists, FALSE otherwise.
   */
  public static function load($case_output) {
    if ($case_output instanceof SimaCaseOutputs) {
      return $case_output;
    }
    else {
      if ($entity = parent::load($case_output)) {
        return ($entity->getDrupalBundle() == self::BUNDLE) ? $entity : FALSE;
      }
      return FALSE;
    }
  }

  /**
   * Returns a new SimaCaseOutputs.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaCaseOutputs
   *   The loaded SimaCaseOutputs.
   */
  static public function newCaseOutput() {
    global $user;
    // Create the new Project Node.
    return self::newEntity('node', self::BUNDLE, $user->uid);
  }

  /**
   * Sets the Case Study to the current Case Output.
   *
   * @param int $cid
   *   The Case Study ID (nid).
   */
  public function setCaseStudy($cid) {
    if ($case_study = SimaCaseStudy::load($cid)) {
      // Assign the same name as the Case Study.
      $title = t('Case Output for !case', array(
        '!case' => $case_study->get('title'),
      ));
      $this->set('title', $title);
      $this->set(self::FIELD_CASE_STUDY_REF, $case_study->getDrupalEntity());
    }
  }

  /**
   * The Sima Case Study it references to.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaCaseStudy|FALSE
   *   The loaded Case Study or FALSE otherwise.
   */
  public function getCaseStudy() {
    $case = $this->get(self::FIELD_CASE_STUDY_REF);
    return SimaCaseStudy::load($case);
  }

  /**
   * Loads a SimaCaseOutputs node given a Case Study ID.
   *
   * @param int $cid
   *   The Case Study ID (nid).
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaCaseOutputs|FALSE
   *   The loaded SimaCaseOutputs object if exists, FALSE otherwise.
   */
  public static function loadByCaseStudy($cid) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', self::BUNDLE)
      ->propertyCondition('status', NODE_PUBLISHED)
      ->fieldCondition(self::FIELD_CASE_STUDY_REF, 'target_id', $cid, '=');
    $result = $query->execute();

    if (isset($result['node'])) {
      $node = reset($result['node']);
      if (!empty($node)) {
        return new static($node->nid);
      }
    }
    return FALSE;
  }

  /**
   * Returns a SimaCaseOutput node linked to a Case Study.
   *
   * @param int $cid
   *   The Case Study ID.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaCaseOutputs
   *   The SimaCaseOutputs node.
   */
  public static function loadByCaseStudyCreateIfNeeded($cid) {
    $case_output = SimaCaseOutputs::loadByCaseStudy($cid);
    if ($case_output === FALSE) {
      $case_output = SimaCaseOutputs::newCaseOutput();
      $case_output->setCaseStudy($cid);
      $case_output->save();
    }
    return $case_output;
  }

  /**
   * Adds a WEAP Output Resource to the Case Output.
   *
   * @param string $model
   *   The Model machine name.
   * @param \Drupal\dss_magdalena\DSS\Entity\SimaResource $resource
   *   The SimaResource object.
   */
  public function addOutputResource($model, SimaResource $resource, $language = LANGUAGE_NONE) {
    $entity = $resource->getDrupalEntity()->value();
    switch ($model) {
      case SimaModel::MODEL_WEAP:
        $this->add(self::FIELD_WEAP_OUTPUTS, $entity, $language);
        break;

      case SimaModel::MODEL_ELOHA:
        $this->add(self::FIELD_ELOHA_OUTPUTS, $entity, $language);
        break;

      case SimaModel::MODEL_FRAGMENTATION:
        $this->add(self::FIELD_FRAGMENTATION_OUTPUTS, $entity, $language);
        break;

      case SimaModel::MODEL_DOR_H:
        $this->add(self::FIELD_DOR_H_OUTPUTS, $entity, $language);
        break;

      case SimaModel::MODEL_DOR_W:
        $this->add(self::FIELD_DOR_W_OUTPUTS, $entity, $language);
        break;

      case SimaModel::MODEL_SEDIMENTS:
        $this->add(self::FIELD_SEDIMENTS_OUTPUTS, $entity, $language);
        break;

      case SimaModel::MODEL_TERRITORIAL_FOOTPRINT:
        $this->add(self::FIELD_TERRITORIAL_FOOTPRINT_OUTPUTS, $entity, $language);
        break;
    }
  }

  /**
   * Returns the list of models' name and status keyed by model machine name.
   *
   * @return array
   *   An array of models' names and status keyed by Model machine name.
   */
  public function getModelingStatusList() {
    $controller = new SimaController();
    $models = $controller->listEntities(SimaModel::BUNDLE);
    $model_status_list = [];
    /** @var \Drupal\dss_magdalena\DSS\Entity\SimaModel $model */
    foreach ($models as $model) {
      $model_machine_name = $model->getMachineName();
      $model_status_list[$model_machine_name]['name'] = $model->getDrupalEntity()->label();
      $model_status_list[$model_machine_name]['status'] = $this->getModelingStatus($model_machine_name);
    }
    return $model_status_list;
  }

  /**
   * Sets the model status for all models at once.
   *
   * @param array $modeling_status_list
   *   An array of model status keyed by model_machine_name.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaCaseOutputs
   *   The current SimaCaseOutputs object.
   */
  public function setModelingStatusList($modeling_status_list) {
    foreach ($modeling_status_list as $model_machine_name => $status) {
      $this->setModelingStatus($model_machine_name, $status);
    }
    return $this;
  }

  /**
   * Returns the Modeling Status for a specific model.
   *
   * @param string $model_machine_name
   *   The Model machine name.
   *
   * @return mixed
   *   The Model Status.
   */
  public function getModelingStatus($model_machine_name) {
    switch ($model_machine_name) {
      case SimaModel::MODEL_WEAP:
        return $this->get(self::FIELD_WEAP_STATUS);

      case SimaModel::MODEL_ELOHA:
        return $this->get(self::FIELD_ELOHA_STATUS);

      case SimaModel::MODEL_FRAGMENTATION:
        return $this->get(self::FIELD_FRAGMENTATION_STATUS);

      case SimaModel::MODEL_DOR_H:
        return $this->get(self::FIELD_DOR_H_STATUS);

      case SimaModel::MODEL_DOR_W:
        return $this->get(self::FIELD_DOR_W_STATUS);

      case SimaModel::MODEL_SEDIMENTS:
        return $this->get(self::FIELD_SEDIMENTS_STATUS);

      case SimaModel::MODEL_TERRITORIAL_FOOTPRINT:
        return $this->get(self::FIELD_TERRITORIAL_FOOTPRINT_STATUS);
    }
  }

  /**
   * Sets the Status Value for a specific model.
   *
   * @param string $model_machine_name
   *   The model machine name.
   * @param string $value
   *   A number between 1 to 100, 'OK', or 'Done'.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaCaseOutputs
   *   The current SimaCaseOutputs object.
   */
  public function setModelingStatus($model_machine_name, $value) {
    switch ($model_machine_name) {
      case SimaModel::MODEL_WEAP:
        return $this->set(self::FIELD_WEAP_STATUS, $value);

      case SimaModel::MODEL_ELOHA:
        return $this->set(self::FIELD_ELOHA_STATUS, $value);

      case SimaModel::MODEL_FRAGMENTATION:
        return $this->set(self::FIELD_FRAGMENTATION_STATUS, $value);

      case SimaModel::MODEL_DOR_H:
        return $this->set(self::FIELD_DOR_H_STATUS, $value);

      case SimaModel::MODEL_DOR_W:
        return $this->set(self::FIELD_DOR_W_STATUS, $value);

      case SimaModel::MODEL_SEDIMENTS:
        return $this->set(self::FIELD_SEDIMENTS_STATUS, $value);

      case SimaModel::MODEL_TERRITORIAL_FOOTPRINT:
        return $this->set(self::FIELD_TERRITORIAL_FOOTPRINT_STATUS, $value);
    }
  }

  /**
   * Gets the Imported Status for a model in this Case Output.
   *
   * @param string $model
   *   Model Identifier.
   * @param string $language
   *   The language code.
   *
   * @return string
   *   The Import Status.
   */
  public function getImportStatus($model, $language = LANGUAGE_NONE) {
    $resources = $this->getOutputResources($model, $language);
    if (empty($resources)) {
      return self::STATUS_NOT_IMPORTED;
    }
    return self::STATUS_IMPORTED;
  }

  /**
   * Obtains the list of Output Resources (Cajas de Datos) for a specific model.
   *
   * @param string $model
   *   Model Identifier.
   * @param string $language
   *   The language code.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaResource[]
   *   An array of resources.
   */
  public function getOutputResources($model, $language = LANGUAGE_NONE) {
    $field_resource = '';
    switch ($model) {
      case SimaModel::MODEL_WEAP:
        $field_resource = self::FIELD_WEAP_OUTPUTS;
        break;

      case SimaModel::MODEL_ELOHA:
        $field_resource = self::FIELD_ELOHA_OUTPUTS;
        break;

      case SimaModel::MODEL_FRAGMENTATION:
        $field_resource = self::FIELD_FRAGMENTATION_OUTPUTS;
        break;

      case SimaModel::MODEL_DOR_H:
        $field_resource = self::FIELD_DOR_H_OUTPUTS;
        break;

      case SimaModel::MODEL_DOR_W:
        $field_resource = self::FIELD_DOR_W_OUTPUTS;
        break;

      case SimaModel::MODEL_SEDIMENTS:
        $field_resource = self::FIELD_SEDIMENTS_OUTPUTS;
        break;

      case SimaModel::MODEL_TERRITORIAL_FOOTPRINT:
        $field_resource = self::FIELD_TERRITORIAL_FOOTPRINT_OUTPUTS;
        break;
    }
    $resources = $this->getMultivalued($field_resource, $language);
    foreach ($resources as $key => $resource) {
      $resources[$key] = SimaResource::load($resource);
    }
    return $resources;
  }

  /**
   * Returns all the list of Output Resources associated to this Case Study.
   *
   * @return array|\Drupal\dss_magdalena\DSS\Entity\SimaResource[]
   *   The Resources associated to this Case Study.
   */
  public function getAllResources() {
    $resources = $this->getOutputResources(SimaModel::MODEL_WEAP);
    $resources = array_merge($resources, $this->getOutputResources(SimaModel::MODEL_ELOHA));
    $resources = array_merge($resources, $this->getOutputResources(SimaModel::MODEL_FRAGMENTATION));
    $resources = array_merge($resources, $this->getOutputResources(SimaModel::MODEL_DOR_H));
    $resources = array_merge($resources, $this->getOutputResources(SimaModel::MODEL_TERRITORIAL_FOOTPRINT));
    return $resources;
  }

  /**
   * Verifies if the WEAP output already exists for this variable (dataset).
   *
   * @param string $dataset_machine_name
   *   The DSS variable machine name (dataset machine_name).
   *
   * @return int|bool
   *    The Resource NID, if it exists in WEAP ouputs, FALSE otherwise.
   */
  public function checkWeapOutputExistsForDataset($dataset_machine_name) {
    // Obtains the list of resources (cajas de datos).
    $resources = $this->getMultivalued(self::FIELD_WEAP_OUTPUTS);

    foreach ($resources as $key => $resource) {
      $sima_resource = SimaResource::load($resource);
      $datasets = $sima_resource->getReferencedVariables();
      foreach ($datasets as $dataset) {
        /** @var \Drupal\dss_magdalena\DSS\Entity\SimaDataset $sima_dataset */
        $sima_dataset = SimaDataset::load($dataset);
        if ($sima_dataset->getMachineName() == $dataset_machine_name) {
          return $sima_resource->getId();
        }
      }
    }
    return FALSE;
  }

}
