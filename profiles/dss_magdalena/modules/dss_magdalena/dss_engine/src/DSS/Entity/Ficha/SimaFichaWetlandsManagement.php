<?php
/**
 * @file
 * Wetlands Management. Ficha Type 3.
 */

namespace Drupal\dss_magdalena\DSS\Entity\Ficha;

use Drupal\dss_magdalena\DSS\Entity\SimaController;
use Drupal\dss_magdalena\DSS\Entity\SimaProject;
use Drupal\dss_magdalena\DSS\Entity\SimaRiverReaches;
use Drupal\dss_magdalena\DSS\Entity\SimaCatchments;
use Drupal\dss_magdalena\DSS\Entity\SimaWetlandsManagementVariation;

/**
 * Implements a Ficha for Wetlands Management.
 *
 * Class SimaFichaWetlandsManagement.
 *
 * @package Drupal\dss_magdalena\DSS\Entity\Ficha
 */
class SimaFichaWetlandsManagement extends SimaFicha implements SimaFichaInterface {

  const BUNDLE                                     = 'wetlands_management';

  // Type of West Management Projects.
  const TYPE_RIVER_FLOODING_THRESHOLD              = 'cambio_umbral_inundacion';
  const TYPE_RIVER_FLOODING_FRACTION               = 'cambio_capacidad_rio_planicie';
  const TYPE_FLOOD_RETURN_FRACTION                 = 'cambio_capacidad_planicie_rio';

  // Wetland Management Project Fields.
  const FIELD_TOPOLOGY                             = 'field_typology';
  const RIVER_FLOODING_THRESHOLD_RIVER_REACHES_REF = 'field_flooding_threshold_river';
  const RIVER_FLOODING_FRACTION_RIVER_REACHES_REF  = 'field_flooding_fraction_river';
  const FLOOD_RETURN_FRACTION_CATCHMENT_REF        = 'field_fraction_flow_return_catch';

  /**
   * {@inheritdoc}
   */
  protected $datasets = [
    'dss_umbral_desborde' => 'River Flooding Threshold',
    'dss_fraccion_caudal_desborde' => 'River Flooding Fraction',
    'dss_retorno_desborde' => 'Flood Return Fraction',
    'dss_distribucion_inundacion' => 'Fraction Flooding Received',
  ];

  /**
   * {@inheritdoc}
   */
  protected $referenceDataset = 'dss_umbral_desborde';

  /**
   * Loads a SimaFichaWetlandsManagement from a Drupal entity or Entity ID.
   *
   * @param mixed $ficha
   *   The Ficha to load.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Ficha\SimaFichaWetlandsManagement|FALSE
   *   A SimaFichaWetlandsManagement object if succeeds, FALSE otherwise.
   */
  public static function load($ficha) {
    if ($ficha instanceof SimaFichaWetlandsManagement) {
      return $ficha;
    }
    else {
      if ($entity = parent::load($ficha)) {
        return ($entity->getDrupalBundle() == self::BUNDLE) ? $entity : FALSE;
      }
      return FALSE;
    }
  }

  /**
   * Creates an new empty Ficha.
   *
   * @param array $project_data
   *   The Project Data.
   * @param array $ficha_data
   *   The Ficha Data.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Ficha\SimaFichaWetlandsManagement
   *   The SimaFicha object, FALSE otherwise.
   */
  static public function newFicha($project_data, $ficha_data) {
    global $user;
    // Create the new empty Ficha Node.
    $ficha = self::newEntity('node', self::BUNDLE, $user->uid);

    // Build the node object and save it.
    $ficha->newWetlandsManagement($project_data, $ficha_data);
    return $ficha;
  }

  /**
   * Obtains the topology for this Wetlands Management Project.
   *
   * @return string
   *   The Typology.
   */
  public function getTypology() {
    return $this->get(self::FIELD_TOPOLOGY);
  }

  /**
   * Builds the data for a new Wetlands Management Project.
   *
   * @param array $project_data
   *   The project data.
   * @param array $ficha_data
   *   The ficha data.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Ficha\SimaFichaWetlandsManagement
   *   A ficha of this type.
   *
   * @throws \Exception
   *   If it cannot create it, throw exception.
   */
  protected function newWetlandsManagement($project_data, $ficha_data) {
    // Creates a new Project.
    $project = SimaProject::newProject();

    // Assign basic fields to Ficha.
    $title = isset($project_data['proj_name']) ? $project_data['proj_name'] : t('No title');

    $this->getDrupalEntity()->title = $title;

    // @codingStandardsIgnoreStart
    // Building body.
    //    $header = array_keys(reset($ficha_data));
    //    $rows = array_map('array_values', $ficha_data);
    //    $raw_data = theme('table', array(
    //      'header' => $header,
    //      'rows' => $rows,
    //      'attributes' => array('class' => array('ficha-project'))
    //    ));
    //    $body = $raw_data;
    // @codingStandardsIgnoreEnd
    $this->getDrupalEntity()->body->set(array(
      'value' => '',
      'format' => 'full_html',
    ));

    // Assigning title to Project.
    $project->getDrupalEntity()->title = $title;

    // Create a default variation.
    $body = 'Es un terraplen exagerado que no permite practicamente desbordamientos';
    $variation = SimaWetlandsManagementVariation::newVariation();
    $variation->setTitle(t('!title - Variante EXAGERA', array(
      '!title' => $title,
    )));
    $variation->setBody($body);
    $values = explode(';', $project_data['Expression']);

    // @TODO: Assign default value for these variables.
    $value = $project_data['Level 3'] === self::TYPE_RIVER_FLOODING_THRESHOLD ? 300000.00 : 100.00;
    // $value = isset($values[0]) ? floatval($values[0]) : 0.0;.
    // Assigning values.
    switch ($project_data['Level 3']) {
      case self::TYPE_RIVER_FLOODING_THRESHOLD:
        $this->set(self::FIELD_TOPOLOGY, $project_data['Level 3']);
        $river_reach_id = $this->getRiverReachTargetIdFromBranch($project_data['affected']);
        $this->set(self::RIVER_FLOODING_THRESHOLD_RIVER_REACHES_REF, $river_reach_id);
        $variation->setRiverFloodingThreshold($value);
        break;

      case self::TYPE_RIVER_FLOODING_FRACTION:
        $this->set(self::FIELD_TOPOLOGY, $project_data['Level 3']);
        $river_reach_id = $this->getRiverReachTargetIdFromBranch($project_data['affected']);
        $this->set(self::RIVER_FLOODING_FRACTION_RIVER_REACHES_REF, $river_reach_id);
        $variation->setRiverFloodingFraction($value);
        break;

      case self::TYPE_FLOOD_RETURN_FRACTION:
        $this->set(self::FIELD_TOPOLOGY, $project_data['Level 3']);
        $catchment_id = $this->getCatchmentTargetIdFromBranch($project_data['affected']);
        $this->set(self::FLOOD_RETURN_FRACTION_CATCHMENT_REF, $catchment_id);
        $variation->setFloodReturnFraction($value);
        break;
    }

    // All values have been assigned. Save Project and Ficha.
    $project->save();
    $this->getDrupalEntity()->{self::PROJECT}->set($project->getDrupalEntity()->value());
    $this->save();

    // Save default project variation.
    $variation->setWetlandsManagementProject($this);
    $variation->save();
    return $this;
  }

  /**
   * Obtains the RiverReach ID given a Full Branch Name.
   *
   * @param string $branch_name
   *   The Branch ID.
   *
   * @return int
   *   The Target ID.
   */
  protected function getRiverReachTargetIdFromBranch($branch_name) {
    // Obtaining the river reach that corresponds to this Branch ID.
    if ($river_reach = SimaRiverReaches::loadByFullBranchName($branch_name)) {
      return intval($river_reach->getId());
    }
    return NULL;
  }

  /**
   * Obtains the Catchments ID given a Full Branch Name.
   *
   * @param string $branch_name
   *   The Branch ID.
   *
   * @return int
   *   The Target ID.
   */
  protected function getCatchmentTargetIdFromBranch($branch_name) {
    // Obtaining the river reach that corresponds to this Branch ID.
    if ($catchment = SimaCatchments::loadByFullBranchName($branch_name)) {
      return intval($catchment->getId());
    }
    return NULL;
  }

  /**
   * Returns a list of Projects Variations for this particular project.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Ficha\SimaFichaWetlandsManagement[]
   *   An array of Project Variations.
   */
  public function getProjectVariations() {
    return SimaWetlandsManagementVariation::getProjectVariations($this->getId());
  }

  /**
   * Lists all Wetlands Management Projects available in the system.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Ficha\SimaFichaWetlandsManagement[]
   *   An array of loaded Sima Wetlands Management Projects.
   */
  public static function listEntities() {
    return SimaController::listEntities(self::BUNDLE);
  }

}
