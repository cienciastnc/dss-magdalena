<?php
/**
 * @file
 * Models a Variable Type taxonomy term.
 */

namespace Drupal\dss_magdalena\DSS\Entity\Term;

/**
 * Implements a Variable Type taxonomy term.
 *
 * Class SimaVariableType.
 *
 * @package Drupal\dss_magdalena\DSS\Entity\Term
 */
class SimaVariableType extends EntityTermAbstract {

  const BUNDLE         = 'variable_type';

  const NAME_INDICADOR = 'Indicador';
  const NAME_INDICE    = 'Indice';
  const NAME_NATURAL   = 'Natural';
  const NAME_OBJETIVO  = 'Objetivo';

  /**
   * Loads a SimaVariableType from a Drupal entity or a Drupal entity ID.
   *
   * @param mixed $variable_type
   *   The taxonomy_term tid or loaded object.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Term\SimaVariableType|FALSE
   *   The loaded SimaVariableType object if exists, FALSE otherwise.
   */
  public static function load($variable_type) {
    if ($variable_type instanceof SimaVariableType) {
      return $variable_type;
    }
    else {
      if ($entity = parent::load($variable_type)) {
        return ($entity->getDrupalBundle() == self::BUNDLE) ? $entity : FALSE;
      }
      return FALSE;
    }
  }

  /**
   * Check whether this is an Indicator.
   *
   * @return bool
   *   TRUE if it is an indicator, FALSE otherwise.
   */
  public function isIndicator() {
    return $this->getName() == self::NAME_INDICADOR;
  }

  /**
   * Check whether this is an Index.
   *
   * @return bool
   *   TRUE if it is an index, FALSE otherwise.
   */
  public function isIndex() {
    return $this->getName() == self::NAME_INDICE;
  }

  /**
   * Check whether this is a Natural.
   *
   * @return bool
   *   TRUE if it is natural, FALSE otherwise.
   */
  public function isNatural() {
    return $this->getName() == self::NAME_NATURAL;
  }

  /**
   * Check whether this is an Objective.
   *
   * @return bool
   *   TRUE if it is an objective, FALSE otherwise.
   */
  public function isObjective() {
    return $this->getName() == self::NAME_OBJETIVO;
  }

}
