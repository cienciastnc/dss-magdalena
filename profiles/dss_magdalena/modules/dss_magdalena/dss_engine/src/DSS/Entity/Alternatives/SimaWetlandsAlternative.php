<?php
/**
 * @file
 * Implements Sima Wetlands Management Alternative.
 */

namespace Drupal\dss_magdalena\DSS\Entity\Alternatives;

use Drupal\dss_magdalena\DSS\Entity\SimaAlternative;
use Drupal\dss_magdalena\DSS\Entity\SimaCaseStudy;
use Drupal\dss_magdalena\DSS\Entity\SimaDefaultCaseStudy;
use Drupal\dss_magdalena\DSS\Entity\SimaResource;
use Drupal\dss_magdalena\DSS\Utils\SimaCsvReaderTrait;
use Drupal\dss_magdalena\DSS\Utils\SimaCsvWriterTrait;
use Drupal\dss_magdalena\DSS\Utils\SimaDirectoriesTrait;
use Drupal\dss_magdalena\DSS\Queue\SimaImportQueueItem;

/**
 * Class SimaWetlandsAlternative.
 *
 * @package Drupal\dss_magdalena\DSS\Entity\Alternatives
 */
class SimaWetlandsAlternative extends SimaAlternative implements SimaAlternativeInterface {

  use SimaCsvReaderTrait;
  use SimaCsvWriterTrait;
  use SimaDirectoriesTrait;

  /**
   * {@inheritdoc}
   */
  public function createResourceForDataset($cid, $dataset_machine_name, $process_datastore = FALSE) {
    // We are going to load the Resource CSV for the Extreme Case (Default Case)
    // and use it to delete the projects that we are not concerned about,
    // because they have not been selected.
    $case_ref = SimaDefaultCaseStudy::loadExtreme();
    if ($resource_ref = SimaResource::loadByCaseStudyAndDataset($case_ref->getId(), $dataset_machine_name)) {
      if ($resource_file = $resource_ref->getResourceCsvFile()) {
        $projects = $this->getFichaProjects();
        $destination = "temporary://{$cid}/weap/input/{$dataset_machine_name}.csv";

        if ($this->createPathToFile($destination)) {
          if ($csvfile = $this->prepareResourceCsvFile($projects, $resource_file, $destination)) {
            // Resource File has been saved. Now attach it to the resource.
            $case = SimaCaseStudy::load($cid);
            $case_outputs = $case->getCaseOutputs();
            $values = [
              'cid' => $cid,
              'case_output' => $case_outputs->getId(),
              'file_uri' => $csvfile,
              'filename' => basename($csvfile),
              'dss_machine_name' => $dataset_machine_name,
            ];
            $item = new SimaImportQueueItem($values);

            /** @var \Drupal\dss_magdalena\DSS\Entity\SimaResource $resource */
            if ($resource = SimaResource::newFullyFormedResource($item)) {
              $resource->save();

              // Do we want to process the datastore?
              if ($process_datastore) {
                $resource->processDataStore();
              }
              return TRUE;
            }
          }
        }

      }
    }
  }

  /**
   * Saves A Resource CSV File and leaves it ready to be attached to a resource.
   *
   * @param \Drupal\dss_magdalena\DSS\Entity\Ficha\SimaFichaInterface[] $projects
   *   An array of all projects for this alternative.
   * @param object $resource_file
   *   The reference file object (for the extreme case study).
   * @param string $destination
   *   A string containing the destination URI. This must be a stream wrapper
   *   URI. If no value is provided, a randomized name will be generated and
   *   the file will be saved using Drupal's default files scheme, usually
   *   "public://".
   *
   * @TODO: Should we remove this altogether?
   *
   * @return bool|\string
   *   The path to saved resource file, FALSE otherwise.
   */
  protected function prepareResourceCsvFile($projects, $resource_file, $destination) {
    $csv_realpath = drupal_realpath($resource_file->uri);
    $proj_names = [];
    foreach ($projects as $project) {
      // River Flooding Threshold.
      // Fraction of Flood Returning.
      // Fraction of Flow Received.
    }

    // @TODO: Do whatever is needed to change the resource.
    $csv_reader = $this->extractRows('filterByNoFilter', $csv_realpath);

    // Save File.
    return $this->saveUnManagedCsvFile($csv_reader, $destination);
  }

}
