<?php
/**
 * @file
 * Defines and handles all Default Case Studies.
 */

namespace Drupal\dss_magdalena\DSS\Entity;
use League\Csv\Reader;

/**
 * Class SimaDefaultCaseStudies.
 *
 * @package Drupal\dss_magdalena\DSS\Entity
 */
class SimaDefaultCaseStudy extends SimaCaseStudy {

  const VARIABLE_DEFAULT_CASES = 'dss_engine_default_case_studies';

  /**
   * List of Default Case Studies.
   */
  const DEFAULT_CASE_STUDIES = [
    'extreme' => [
      'line' => 1,
      'title' => 'Caso Casi Extremo',
    ],
    'baseline' => [
      'line' => 2,
      'title' => 'Caso Línea Base',
    ],
    'pristine' => [
      'line' => 3,
      'title' => 'Caso Pristino',
    ],
    'baseline_fa' => [
      'line' => 4,
      'title' => 'Línea Base Humedales FA',
    ],
    'baseline_strong' => [
      'line' => 5,
      'title' => 'Línea Base Escenario Fuerte',
    ],
    'high_magdalena_lb' => [
      'line' => 6,
      'title' => 'Alto Magdalena Escenario LB',
    ],
    'high_magdalena_strong' => [
      'line' => 7,
      'title' => 'Alto Magdalena Fuerte',
    ],
    'restoration' => [
      'line' => 8,
      'title' => 'Caso Restauración bosques',
    ],
    'moderate' => [
      'line' => 9,
      'title' => 'Caso Actual Moderado',
    ],
  ];

  /**
   * Loads a Case Study from a Drupal entity or a Drupal entity ID.
   *
   * @param mixed $case_study
   *   The nid or loaded object.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaDefaultCaseStudy|\Drupal\dss_magdalena\DSS\Entity\SimaCaseStudy|FALSE
   *   The loaded SimaCaseStudy object if exists, FALSE otherwise.
   */
  public static function load($case_study) {
    $machine_names = static::getDefaultMachineNames();
    if (is_string($case_study) && in_array($case_study, $machine_names)) {
      return static::loadPredefinedCaseStudy($case_study);
    }
    return parent::load($case_study);
  }

  /**
   * Defines the list of Default Case Studies.
   *
   * @return array
   *   An array with the list of default Case Studies.
   */
  protected static function getDefinition() {
    return variable_get(self::VARIABLE_DEFAULT_CASES, self::DEFAULT_CASE_STUDIES);
  }

  /**
   * Sets the Definitions for the Default Case Studies.
   */
  public static function setDefinition() {
    $definition = self::DEFAULT_CASE_STUDIES;
    foreach ($definition as $machine_name => $data) {
      if ($case = SimaCaseStudy::loadCaseByTitle($data['title'])) {
        $definition[$machine_name]['cid'] = $case->getId();
      }
    }
    variable_set(self::VARIABLE_DEFAULT_CASES, $definition);
  }

  /**
   * Obtains a list of all machine names of predefined case studies.
   *
   * @return array
   *   An array with machine names of default case studies.
   */
  public static function getDefaultMachineNames() {
    return array_keys(self::getDefinition());
  }

  /**
   * Loads the Default Case Study.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaDefaultCaseStudy|FALSE
   *   The Default loaded Case Study if found, FALSE otherwise.
   */
  public static function loadPredefinedCaseStudy($case = 'extreme') {
    $definitions = self::getDefinition();
    if (!isset($definitions[$case])) {
      return NULL;
    }
    if (isset($definitions[$case]['cid'])) {
      return self::load($definitions[$case]['cid']);
    }

    // If the variable is not set, then search for the first case study and set
    // the variable up to be that case.
    $csv_file = DRUPAL_ROOT . base_path() . drupal_get_path('module', 'dss_import') . '/import/dss_import.nodes.case_study.csv';
    $csv_file = Reader::createFromPath($csv_file);
    $headers = $csv_file->fetchOne();
    // If filter is not defined, then do not filter results.
    module_load_include('inc', 'dss_engine', 'src/DSS/inc/sima_import_weap_databox_csv-filters');
    $filter = isset($filter) ? $filter : 'filterByNoFilter';
    $results = $csv_file
      ->stripBom(FALSE)
      ->addFilter($filter)
      ->fetchAssoc($headers);
    $file = iterator_to_array($results);
    if (!empty($file)) {
      $line = $definitions[$case]['line'];
      $title = $file[$line]['Name'];
    }
    else {
      $title = $definitions[$case]['title'];
    }
    return $case = self::loadCaseByTitle($title);
  }

  /**
   * Loads the Extreme Case "Caso Extremo".
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaDefaultCaseStudy|FALSE
   *   The Extreme Case Study loaded if found, FALSE otherwise.
   */
  public static function loadExtreme() {
    return self::loadPredefinedCaseStudy('extreme');
  }

  /**
   * Loads the Baseline Case "Caso Linea Base".
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaDefaultCaseStudy|FALSE
   *   The Baseline Case Study loaded if found, FALSE otherwise.
   */
  public static function loadBaseline() {
    return self::loadPredefinedCaseStudy('baseline');
  }

  /**
   * Loads the Baseline Case "Caso Pristino".
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaDefaultCaseStudy|FALSE
   *   The Baseline Case Study loaded if found, FALSE otherwise.
   */
  public static function loadPristine() {
    return self::loadPredefinedCaseStudy('pristine');
  }

  /**
   * Checks whether this Case Study is the Extreme Case Study or not.
   *
   * @return bool
   *   TRUE if it is the extreme case study, FALSE otherwise.
   */
  public function isExtreme() {
    $extreme_case_study = SimaDefaultCaseStudy::loadExtreme();
    return $this->getId() === $extreme_case_study->getId();
  }

  /**
   * Checks whether this Case Study is the Baseline Case Study or not.
   *
   * @return bool
   *   TRUE if it is the baseline case study, FALSE otherwise.
   */
  public function isBaseline() {
    $baseline_case_study = SimaDefaultCaseStudy::loadBaseline();
    return $this->getId() === $baseline_case_study->getId();
  }

  /**
   * Checks whether this Case Study is the Pristine Case Study or not.
   *
   * @return bool
   *   TRUE if it is the pristine case study, FALSE otherwise.
   */
  public function isPristine() {
    $pristine_case_study = SimaDefaultCaseStudy::loadPristine();
    return $this->getId() === $pristine_case_study->getId();
  }

  /**
   * Checks whether this is a default case study.
   *
   * @return bool
   *   TRUE if it is a default Case Study, FALSE otherwise.
   */
  public function isDefault() {
    $defaults = $this->getDefinition();
    $cids = array_column($defaults, 'cid');
    if (in_array($this->getId(), $cids)) {
      return TRUE;
    }
    return FALSE;
  }

}
