<?php
/**
 * @file
 * Entity for TIER1 Project Catalog
 */

namespace Drupal\dss_magdalena\DSS\Entity;

/**
 * Project Catalogs
 *
 * Class SimaProjectCatalog.
 *
 * @package Drupal\dss_magdalena\DSS\Entity
 */
class SimaProjectCatalog extends EntityNodeAbstract {

  const BUNDLE = 'project_catalog';
  const TITLE  = 'title';
  const DESCRIPTION= 'field_description';
  const PROJECTS= 'field_projects';
  
  /**
   * Loads a Project Catalog from a Drupal entity or a Drupal entity ID.
   *
   * @param $catalog
   *   The nid or loaded object.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaProjectCatalog|FALSE
   *   The loaded SimaProjectCatalog object if exists, FALSE otherwise.
   */
  public static function load($catalog) {
    if ($catalog instanceof SimaProjectCatalog) {
      return $catalog;
    }
    else {
      if ($entity = parent::load($catalog)) {
        return ($entity->getDrupalBundle() == self::BUNDLE) ? $entity : FALSE;
      }
      return FALSE;
    }
  }

  /**
   * Returns a new Project Catalog
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaProjectCatalog
   *   The loaded SimaProjectCatalog.
   */
  static public function newProjectCatalog() {
    global $user;
    // Create the new Integral Plan Node.
    return self::newEntity('node', self::BUNDLE, $user->uid);
  }

  public function getCatalog() {
    return $this->getDrupalEntity();
  }

  public function getTitle(){
    return $this->get(self::TITLE);
  }

  public function getDescription(){
    return $this->get(self::DESCRIPTION);
  }

  public function getProjects(){
    return $this->get(self::PROJECTS);
  }

}
