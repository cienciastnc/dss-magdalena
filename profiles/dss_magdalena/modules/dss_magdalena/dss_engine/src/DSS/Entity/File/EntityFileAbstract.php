<?php
/**
 * @file
 * Defines an Entity model for a file entity.
 */

namespace Drupal\dss_magdalena\DSS\Entity\File;

use Drupal\dss_magdalena\DSS\Entity\EntityAbstract;
use Drupal\dss_magdalena\DSS\Utils\SimaAutodetectLineEndingsTrait;
use Drupal\dss_magdalena\DSS\Utils\SimaDirectoriesTrait;

/**
 * Models a File Entity.
 *
 * Class EntityFileAbstract.
 *
 * @package Drupal\dss_magdalena\DSS\Entity\File
 */
abstract class EntityFileAbstract extends EntityAbstract {

  use SimaDirectoriesTrait;
  use SimaAutodetectLineEndingsTrait;

  const BUNDLE        = '';
  const DATA_DIR      = "public://weap_files";

  /**
   * Directories that should be initialized.
   *
   * @var array
   */
  protected $directories = [];

  /**
   * Public constructor.
   *
   * @param \EntityDrupalWrapper|object $drupal_entity
   *   The taxonomy_term that maps to a Sima File.
   */
  public function __construct($drupal_entity) {
    // We set this first because we want to make sure the directories are
    // initialized before we even attempt to save the files.
    if ($this->initializeDirectories() === FALSE) {
      return FALSE;
    }

    // Now assign the file entity.
    $this->setDrupalEntity('file', $drupal_entity);
  }

  /**
   * Loads a SimaScenario from a Drupal entity or a Drupal entity ID.
   *
   * @param mixed $entity
   *   The file fid or loaded object.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\EntityAbstract|FALSE
   *   The loaded EntityAbstract object if exists, FALSE otherwise.
   */
  public static function load($entity) {
    if ($entity instanceof \EntityDrupalWrapper || is_object($entity)) {
      return new static($entity);
    }
    elseif (is_array($entity)) {
      // Make an initial call to just initialize the directories.
      $file = new static(NULL);
      if ($file === FALSE) {
        return FALSE;
      }
      // Create file entity. "entity_create()" function cannot be used with
      // files, so we need to explicitly create the file object.
      // We assume we provide a file uri, destination and managed or not.
      $entity['managed'] = isset($entity['managed']) ? $entity['managed'] : FALSE;
      $file = system_retrieve_file($entity['uri'], static::DATA_DIR, $entity['managed']);
      return new static($file);
    }
    elseif (is_int($entity) || (intval($entity) !== 0 && !is_array($entity))) {
      $file = file_load($entity);
      if ($file === FALSE) {
        return FALSE;
      }
      return new static($file);
    }
    else {
      return FALSE;
    }
  }

  /**
   * Loads a term by File name.
   *
   * @param string $uri
   *   The File URI.
   * @param bool|TRUE $use_existing
   *   Whether to load an existing managed file or create it if it doesnt exist.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\File\EntityFileAbstract|bool
   *   The File Entity if exists, FALSE otherwise.
   */
  public static function loadByUri($uri, $use_existing = TRUE) {
    $file = file_uri_to_object($uri, $use_existing);
    // We expect a single match.
    if (!empty($file)) {
      return new static($file);
    }
    return FALSE;
  }

  /**
   * Initializes local directories.
   *
   * Creates the local directories that will store the import
   * master Databox CSV files.
   */
  public function initializeDirectories() {
    foreach ($this->directories as $directory) {
      if ($this->createLocalDirectory($directory) === FALSE) {
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Sets up the File Name.
   *
   * @param string $filename
   *   The filename to set.
   * @param string $lang
   *   The Language Code.
   */
  public function setFileName($filename, $lang = LANGUAGE_NONE) {
    $this->set('filename', $filename, $lang);
  }

  /**
   * Obtains the filename.
   *
   * @return string|NULL
   *   The Filename or NULL.
   */
  public function getFileName() {
    if (!empty($this->getFileObject())) {
      return $this->getFileObject()->filename;
    }
    return NULL;
  }

  /**
   * Obtains the File URI.
   *
   * @return string|NULL
   *   The File URI or NULL.
   */
  public function getUri() {
    if (!empty($this->getFileObject())) {
      return $this->getFileObject()->uri;
    }
    return NULL;
  }

  /**
   * Returns the File Object.
   *
   * @return bool|mixed|null
   *   The File object.
   */
  public function getFileObject() {
    return $this->getDrupalEntity()->value();
  }

  /**
   * Returns the Data Directory where the file will be saved.
   *
   * @return string
   *   The Directory where the file will be saved.
   */
  public function getDataDirectory() {
    return static::DATA_DIR;
  }

}
