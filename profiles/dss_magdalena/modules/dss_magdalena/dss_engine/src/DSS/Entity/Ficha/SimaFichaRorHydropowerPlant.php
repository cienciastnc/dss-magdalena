<?php
/**
 * @file
 * Run-of-River Hydropower Plant. Ficha Type 2.
 */

namespace Drupal\dss_magdalena\DSS\Entity\Ficha;

use Drupal\dss_magdalena\DSS\Entity\SimaProject;

/**
 * Implements a Run-Of-River Hydropower Plant.
 *
 * Class SimaFichaRorHydropowerPlant.
 *
 * @package Drupal\dss_magdalena\DSS\Entity\Ficha
 */
class SimaFichaRorHydropowerPlant extends SimaFicha implements SimaFichaInterface {

  const BUNDLE = 'ror_hydropower_plant';

  // ROR Hydropower Plant Fields.
  const GENERATING_EFFICIENTY = 'field_ror_generating_efficiency';
  const MAX_TURBINE_FLOW      = 'field_ror_max_turbine_flow';
  const PLANT_FACTOR          = 'field_ror_plant_factor';
  const HYDROPOWER_PRIORITY   = 'field_ror_hydropower_priority';
  const FIXED_HEAD            = 'field_fixed_head';
  const RIVER                 = 'field_river';
  const FULL_NAME             = 'field_full_name';

  // New.
  const SIMA_CASE_STATUS      = 'field_sima_case_status';
  const PROJECT_ID            = 'field_project_id';
  const TOPOLOGY_ARCID        = 'field_topology_arcid';
  /**
   * {@inheritdoc}
   */
  protected $datasets = [
    'dss_eficiencia_ror' => 'Generating Efficiency',
    'dss_capacidad_turbinas_ror' => 'Max Turbine Flow',
    'dss_porc_utilizacion' => 'Plant Factor',
    'dss_prioridad_demanda_energia_ror' => 'Hydropower Priority',
    'dss_salto_ror' => 'Fixed Head',
    'dss_pertenencia_al_caso_hror' => 'SIMA Case status',
    'dss_project_id_hror' => 'Project_ID',
    'dss_topology_arcid_hror' => 'Topology_ARCID',
  ];

  /**
   * {@inheritdoc}
   */
  protected $referenceDataset = 'dss_capacidad_turbinas_ror';

  /**
   * Loads a SimaFichaRorHydropowerPlant from a Drupal entity or Entity ID.
   *
   * @param mixed $ficha
   *   The ficha nid or node object.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Ficha\SimaFichaRorHydropowerPlant|FALSE
   *   The loaded SimaFichaRorHydropowerPlant object.
   */
  public static function load($ficha) {
    if ($ficha instanceof SimaFichaRorHydropowerPlant) {
      return $ficha;
    }
    else {
      parent::load($ficha);
    }
  }

  /**
   * Creates an new empty Ficha.
   *
   * @param array $project_data
   *   The Project Data.
   * @param array $ficha_data
   *   The Ficha Data.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Ficha\SimaFichaRorHydropowerPlant
   *   The SimaFicha object.
   */
  static public function newFicha($project_data, $ficha_data) {
    global $user;
    // Create the new empty Ficha Node.
    $ficha = self::newEntity('node', self::BUNDLE, $user->uid);

    // Build the node object and save it.
    $ficha->newRorHydropowerPlant($project_data, $ficha_data);
    return $ficha;
  }

  /**
   * Returns the Full Branch Name.
   *
   * @return string
   *   The Full Name of this Project.
   */
  public function getFullBranchName() {
    return $this->get(self::FULL_NAME);
  }

  /**
   * Returns the SIMA Case Status.
   *
   * @return string
   *   1 if Enabled, 0 if disabled.
   */
  public function getSimaCaseStatus() {
    return $this->get(self::SIMA_CASE_STATUS);
  }

  /**
   * Returns the Project Id.
   *
   * @return int
   *   The project id
   */
  public function getProjectId() {
    return $this->get(self::PROJECT_ID);
  }

  /**
   * Returns the Full Name.
   *
   * @return string
   *   The Full Name
   */
  public function getFullName() {
    return $this->get(self::FULL_NAME);
  }

  /**
   * Returns the Topology ArcId.
   *
   * @return int
   *   The Topology ArcId
   */
  public function getTopologyArcId() {
    return $this->get(self::TOPOLOGY_ARCID);
  }

  /**
   * Builds the data for a new ROR Hydropower Plant.
   *
   * @param array $project_data
   *   The project data.
   * @param array $ficha_data
   *   The ficha data.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Ficha\SimaFichaRorHydropowerPlant
   *   The SimaFicha object.
   *
   * @throws \Exception
   */
  protected function newRorHydropowerPlant($project_data, $ficha_data) {
    // Creates a new Project.
    $project = SimaProject::newProject();

    // Assign basic fields to Ficha.
    $level1 = $this->getRowKeyLevel($project_data, 1);
    $level2 = $this->getRowKeyLevel($project_data, 2);
    $level3 = $this->getRowKeyLevel($project_data, 3);
    $level4 = $this->getRowKeyLevel($project_data, 4);
    $full_name = (isset($level3) && isset($level4)) ? $level3 . '\\' . $level4 : t('None');
    $full_name = $level1 . '\\' . $level2 . '\\' . $full_name;
    $river = isset($level3) ? $level3 : t('None');
    $title = isset($level4) ? explode('\\', $level4)[1] : t('None');
    $this->getDrupalEntity()->title = $title;

    // Building body.
    $header = array_keys(reset($ficha_data));
    $rows = array_map('array_values', $ficha_data);
    $raw_data = theme('table', array(
      'header' => $header,
      'rows' => $rows,
      'attributes' => array('class' => array('ficha-project')),
    ));
    $body = $raw_data;

    $this->getDrupalEntity()->body->set(array(
      'value' => $body,
      'format' => 'full_html',
    ));

    // Assigning title to Project.
    $project->getDrupalEntity()->title = $title;

    $values = array_column($ficha_data, 'Value', 'Variable');

    // Assigning values.
    $this->setFieldValue(self::GENERATING_EFFICIENTY, 'Generating Efficiency', $values);
    $this->setFieldValue(self::MAX_TURBINE_FLOW, 'Max Turbine Flow', $values);
    $this->setFieldValue(self::PLANT_FACTOR, 'Plant Factor', $values);
    $this->setFieldValue(self::HYDROPOWER_PRIORITY, 'Hydropower Priority', $values);
    $this->setFieldValue(self::FIXED_HEAD, 'Fixed Head', $values);
    $this->setFieldValue(self::SIMA_CASE_STATUS, 'SIMA Case status', $values);
    $this->set(self::RIVER, $river);
    $this->set(self::FULL_NAME, $full_name);
    $this->setFieldValue(self::PROJECT_ID, 'Project_ID', $values);
    $this->setFieldValue(self::TOPOLOGY_ARCID, 'Topology_ARCID', $values);

    // All values have been assigned. Save Project and Ficha.
    $project->save();
    $this->getDrupalEntity()->{self::PROJECT}->set($project->getDrupalEntity()->value());
    $this->save();
    return $this;
  }

}
