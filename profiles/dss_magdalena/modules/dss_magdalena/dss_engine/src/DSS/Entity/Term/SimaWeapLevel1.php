<?php
/**
 * @file
 * Models the WEAP Level 1 Term.
 */

namespace Drupal\dss_magdalena\DSS\Entity\Term;

/**
 * Models a WEAP Level1 taxonomy term.
 *
 * Class SimaWeapLevel1
 *
 * @package Drupal\dss_magdalena\DSS\Entity\Term
 */
class SimaWeapLevel1 extends EntityTermAbstract {

  const BUNDLE                             = 'level_1';

  const NAME_KEY_ASSUMPTIONS               = 'Key Assumptions';
  const NAME_DEMAND_SITES_AND_CATCHMENTS   = 'Demand Sites and Catchments';
  const NAME_SUPPLY_AND_RESOURCES          = 'Supply and Resources';
  const NAME_BLANK                         = '[blank]';

  /**
   * Loads a SimaWeapLevel1 from a Drupal entity or a Drupal entity ID.
   *
   * @param mixed $weap_level
   *   The taxonomy_term tid or loaded object.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Term\SimaWeapLevel1|FALSE
   *   A loaded SimaWeapLevel1 object if exists, FALSE otherwise.
   */
  public static function load($weap_level) {
    if ($weap_level instanceof SimaWeapLevel1) {
      return $weap_level;
    }
    else {
      if ($entity = parent::load($weap_level)) {
        return ($entity->getDrupalBundle() == self::BUNDLE) ? $entity : FALSE;
      }
      return FALSE;
    }
  }

}
