<?php
/**
 * @file
 * Defines an Entity Model.
 */

namespace Drupal\dss_magdalena\DSS\Entity;

/**
 * Models a generic Drupal Entity.
 *
 * Class EntityAbstract.
 *
 * @package Drupal\dss_magdalena\DSS\Entity
 */
abstract class EntityAbstract {

  const FIELD_TEXT                    = 'text';
  const FIELD_INTEGER                 = 'integer';
  const FIELD_DECIMAL                 = 'decimal';
  const FIELD_REFERENCE_NODE          = 'node';
  const LIST_REFERENCE_NODE           = 'list<node>';
  const FIELD_REFERENCE_TAXONOMY_TERM = 'taxonomy_term';
  const LIST_REFERENCE_TAXONOMY_TERM  = 'list<taxonomy_term>';
  const LIST_FIELD_DOUBLE             = 'list<double_field>';
  const FIELD_MULTIFIELD              = 'multifield';
  const LIST_MULTIFIELD               = 'list<multifield>';
  const FIELD_GEOFIELD                = 'geofield';

  /**
   * The Drupal Entity Type.
   *
   * @var string
   */
  protected $drupalEntityType;

  /**
   * The Drupal Entity Metadata Wrapper.
   *
   * @var \EntityDrupalWrapper
   */
  protected $drupalEntity;

  /**
   * Returns the Drupal ID (NID).
   *
   * @return bool|mixed|null
   *   The Entity ID.
   */
  public function getId() {
    return $this->drupalEntity->getIdentifier();
  }

  /**
   * Returns the Drupal UUID (UUID).
   *
   * @return string
   *   The Entity UUID.
   */
  public function getUuid() {
    return $this->drupalEntity->uuid->value();
  }

  /**
   * Returns the Drupal entity type, e.g. "node", "taxonomy_term".
   *
   * @return string
   *   The entity type.
   */
  public function getDrupalEntityType() {
    return $this->drupalEntityType;
  }

  /**
   * Returns the Drupal entity's bundle.
   *
   * @return string
   *   The entity bundle.
   */
  public function getDrupalBundle() {
    return $this->drupalEntity->getBundle();
  }

  /**
   * Returns the Drupal Entity.
   *
   * @return \EntityDrupalWrapper
   *   The Drupal Entity metadata wrapper.
   */
  public function getDrupalEntity() {
    return $this->drupalEntity;
  }

  /**
   * Sets the Drupal entity that models the Sima Data Structure.
   *
   * @param string $entity_type
   *   The Drupal Entity type.
   * @param \EntityDrupalWrapper|object $entity
   *   EntityDrupalWrapper entity.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\EntityAbstract
   *   The loaded Object.
   *
   * @throws \InvalidArgumentException
   */
  protected function setDrupalEntity($entity_type, $entity) {
    $this->drupalEntityType = $entity_type;
    if (!$entity instanceof \EntityDrupalWrapper) {
      $this->drupalEntity = entity_metadata_wrapper($entity_type, $entity);
      if (!$this->drupalEntity instanceof \EntityDrupalWrapper) {
        throw new \InvalidArgumentException('Invalid entity passed to model');
      }
    }
    else {
      $this->drupalEntity = $entity;
    }

    return $this;
  }

  /**
   * Creates a new Entity.
   *
   * @param string $type
   *   The Entity type.
   * @param string $bundle
   *   The Entity bundle.
   * @param int $uid
   *   The user UID.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\EntityAbstract
   *   The empty Node object of the specific type.
   */
  public static function newEntity($type, $bundle, $uid = 1) {
    // Create a new entity.
    $e = entity_create($type, array('type' => $bundle));

    // Specify the author.
    $e->uid = $uid;

    return new static($e);
  }

  /**
   * Returns the value of a field by language.
   *
   * @param string $field_name
   *   The field name.
   * @param string $language
   *   The language code.
   * @param bool $raw
   *   TRUE if we want the raw value, FALSE otherwise.
   *
   * @return mixed
   *   The field value in specified language.
   */
  public function get($field_name, $language = LANGUAGE_NONE, $raw = FALSE) {
    try {
      $method = $raw ? 'raw' : 'value';
      if ($language == LANGUAGE_NONE) {
        $value = $this->drupalEntity->{$field_name}->$method();
      }
      else {
        $value = $this->drupalEntity->language($language)->{$field_name}->$method();
      }
    }
    catch (\EntityMetadataWrapperException $e) {
      $value = NULL;

      $args = array(
        '@name' => $field_name,
        '@label' => $this->drupalEntity->label(),
        '@error' => $e->getMessage(),
      );
      drupal_set_message(t('Error getting @name property for @label: @error', $args), 'error');
      watchdog_exception('dss_engine', $e);
    }

    return $value;
  }

  /**
   * Sets the value of a field by language.
   *
   * @param string $field_name
   *   The field name.
   * @param string $field_value
   *   The field value to assign.
   * @param string $language
   *   The language code.
   *
   * @return mixed
   *   This object.
   */
  public function set($field_name, $field_value, $language = LANGUAGE_NONE) {
    try {
      // Preparing value according to the field type.
      $field_type = $this->getDrupalEntity()->$field_name->type();
      switch ($field_type) {
        case self::LIST_FIELD_DOUBLE:
          // A double field contains two fields within it. We need to check each
          // one of them individually to be able to cast to the correct type.
          if (is_array($field_value)) {
            $type_first = $this->getDrupalEntity()
              ->getPropertyInfo()[$field_name]['property info']['first']['type'];
            $type_second = $this->getDrupalEntity()
              ->getPropertyInfo()[$field_name]['property info']['second']['type'];
            foreach ($field_value as $key => $value) {
              $field_value[$key]['first'] = $this->prepareFieldCastValueToType($value['first'], $type_first);
              $field_value[$key]['second'] = $this->prepareFieldCastValueToType($value['second'], $type_second);
            }
          }
          break;

        case self::FIELD_REFERENCE_NODE:
          $field_value = is_int($field_value) ? node_load($field_value) : $field_value;
          break;

        case self::FIELD_REFERENCE_TAXONOMY_TERM:
          $field_value = is_int($field_value) ? taxonomy_term_load($field_value) : $field_value;
          break;

        case self::LIST_REFERENCE_NODE:
        case self::LIST_REFERENCE_TAXONOMY_TERM:
          $field_value = !is_array($field_value) ? array($field_value) : $field_value;
          break;

        case self::FIELD_MULTIFIELD:
          $field_value = !is_array($field_value) ? array($field_value) : $field_value;
          break;

        case self::LIST_MULTIFIELD:
          $field_value = !is_array($field_value) ? array($field_value) : $field_value;
          break;

        case self::FIELD_GEOFIELD:
          // Assuming we are passing the value in WKT.
          $wkt = array(
            'wkt' => $field_value,
          );
          $field_value = geofield_compute_values($wkt, 'wkt');
          break;

        case self::FIELD_TEXT:
        case self::FIELD_INTEGER:
        case self::FIELD_DECIMAL:
        default:
          $field_value = $this->prepareFieldCastValueToType($field_value, $field_type);
          break;
      }

      // Setting value to the field.
      if ($language == LANGUAGE_NONE) {
        $this->drupalEntity->$field_name->set($field_value);
      }
      else {
        $this->drupalEntity->language($language)->$field_name->set($field_value);
      }
    }
    catch (\EntityMetadataWrapperException $e) {
      $args = array(
        '@name' => $field_name,
        '@value' => $field_value,
        '@label' => $this->drupalEntity->label(),
        '@error' => $e->getMessage(),
      );
      drupal_set_message(t('Error setting @name property/field with value @value for @label: @error', $args), 'error');
      watchdog_exception('dss_engine', $e);
    }

    return $this;
  }

  /**
   * Adds a single value (entity) to an multiple entity reference field.
   *
   * @param string $field_name
   *   The entity field name.
   * @param object $field_value
   *   The field value (in this case an entity).
   * @param string $language
   *   The language code.
   */
  public function add($field_name, $field_value, $language = LANGUAGE_NONE) {
    // Only possible for List of Entity Reference Fields (\EntityListWrapper).
    if ($this->getDrupalEntity()->$field_name instanceof \EntityListWrapper) {
      try {
        // We expect $field_value to be an entity.
        // Setting value to the field.
        if ($language == LANGUAGE_NONE) {
          $this->drupalEntity->{$field_name}[] = $field_value;
        }
        else {
          $this->drupalEntity->language($language)->{$field_name}[] = $field_value;
        }
      }
      catch (\EntityMetadataWrapperException $e) {
        $args = array(
          '@name' => $field_name,
          '@value' => $field_value,
          '@label' => $this->drupalEntity->label(),
          '@error' => $e->getMessage(),
        );
        drupal_set_message(t('Error setting @name property/field with value @value for @label: @error', $args), 'error');
        watchdog_exception('dss_engine', $e);
      }
    }
  }

  /**
   * Casts a value to its appropriate type.
   *
   * @param mixed $value
   *   The value to cast.
   * @param string $type
   *   The type to cast to.
   *
   * @return float|string|mixed
   *   The value casted to the appropriate type.
   */
  protected function prepareFieldCastValueToType($value, $type) {
    switch ($type) {
      case self::FIELD_INTEGER:
        return intval($value);

      case self::FIELD_DECIMAL:
        return floatval($value);

      case self::FIELD_TEXT:
      default:
        return (string) $value;
    }
  }


  /**
   * Getter for multivalued fields, ensures an array is always returned.
   *
   * @param string $field_name
   *   The field name.
   * @param string $language
   *   The language code.
   *
   * @return array
   *   An array of all multivalues values.
   */
  public function getMultivalued($field_name, $language = LANGUAGE_NONE) {
    return (array) $this->get($field_name, $language);
  }

  /**
   * If the return value is an array, get by key.
   *
   * @param string $field_name
   *   The field name.
   * @param string $key
   *   The delta key for this field.
   * @param string $language
   *   The language code.
   *
   * @return mixed
   *   The nested values.
   */
  public function getNestedValue($field_name, $key, $language = LANGUAGE_NONE) {
    $value = $this->get($field_name, $language, TRUE);
    return isset($value[$key]) ? $value[$key] : NULL;
  }

  /**
   * Returns the "safe_value" key for text_long fields.
   *
   * @param string $field_name
   *   The field name.
   * @param string $language
   *   The language code.
   *
   * @return mixed
   *   The long text safe value.
   */
  public function getLongTextSafeValue($field_name, $language = LANGUAGE_NONE) {
    return $this->getNestedValue($field_name, 'safe_value', $language);
  }

  /**
   * Wrapper around \EntityDrupalWrapper::save().
   *
   * @return \EntityDrupalWrapper
   *   Entity Drupal Wrapper.
   *
   * @throws \EntityMetadataWrapperException
   */
  public function save() {
    return $this->drupalEntity->save();
  }

  /**
   * Wrapper around \EntityDrupalWrapper::delete().
   *
   * @return \EntityDrupalWrapper
   *   Entity Drupal Wrapper.
   *
   * @throws \EntityMetadataWrapperException
   */
  public function delete() {
    return $this->getDrupalEntity()->delete();
  }

}
