<?php
/**
 * @file
 * Defines a SimaLayer.
 */

namespace Drupal\dss_magdalena\DSS\Entity;

use EntityFieldQuery;

/**
 * Layers a Sima Layer.
 *
 * Class SimaLayer.
 *
 * @package Drupal\dss_magdalena\DSS\Entity
 */
class SimaLayer extends EntityNodeAbstract {

  const BUNDLE                      = 'layers';
  const MACHINE_NAME                = 'field_machine_name';
  const SPATIALIZATION              = 'field_spatialization';

  /**
   * Loads a SimaLayer from a Drupal entity or a Drupal entity ID.
   *
   * @param mixed $layer
   *   The nid or loaded object.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaLayer|FALSE
   *   The loaded SimaLayer object if exists, FALSE otherwise.
   */
  public static function load($layer) {
    if ($layer instanceof SimaLayer) {
      return $layer;
    }
    else {
      if ($entity = parent::load($layer)) {
        return ($entity->getDrupalBundle() == self::BUNDLE) ? $entity : FALSE;
      }
      return FALSE;
    }
  }

  /**
   * Returns the Layer Machine Name.
   *
   * @return string
   *   The Layer Machine Name.
   */
  public function getMachineName() {
    return $this->get(self::MACHINE_NAME);
  }

  /**
   * Loads a SimaLayer given its machine_name.
   *
   * @param string $layer_machine_name
   *   The model machine_name.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaLayer|FALSE
   *   The loaded SimaLayer object if exists, FALSE otherwise.
   */
  public static function loadByMachineName($layer_machine_name) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
            ->entityCondition('bundle', self::BUNDLE)
            ->propertyCondition('status', NODE_PUBLISHED)
            ->fieldCondition(self::MACHINE_NAME, 'value', $layer_machine_name, '=');
    $result = $query->execute();

    if (isset($result['node'])) {
      $node = reset($result['node']);
      return new static($node->nid);
    }
    return FALSE;
  }

  /**
   * Returns a SimaLayer, given the Dataset Machine name.
   *
   * @param string $dataset_machine_name
   *   The Dataset Machine name.
   *
   * @TODO-Instream-Flow: Remaining Layers include the following machine names:
   * dss_caudal_abajo_nodo_cme, dss_caudal_ecologico_efectivo,
   * dss_caudal_entrante_nodo_cme, dss_caudal_saliente_nodo_cme,
   * dss_deficit_cme, dss_caudal_minimo_ecologico_cme,
   * dss_seguridad_cme, dss_satisfaccion_cme
   *
   * @TODO-Land-Use: Remaining include the following machine names:
   * dss_et_potencial_uso_suelo, dss_et_efectiva_uso_suelo,
   * dss_flujo_subsuperficial_uso_suelo, dss_area_calculada_uso_suelo,
   * dss_humedad_1_uso_suelo, dss_flujo_basal_uso_suelo,
   * dss_escorrentia_superficial_uso_suelo, dss_irrigacion_uso_suelo,
   * dss_desbordamiento_uso_suelo, dss_area_inundada_uso_suelo,
   * dss_volumen_inundacion_uso_suelo, dss_deficit_et_uso_suelo,
   * dss_evapotranspiracion_uso_suelo, dss_profundidad_inundacion_uso_suelo
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaLayer|FALSE
   *   The loaded SimaLayer object if exists, FALSE otherwise.
   */
  public static function loadByDatasetMachineName($dataset_machine_name) {

    // Defines a relationship between the dataset machine name (variable machine
    // name) and the layer machine name.
    $mappings = [
      'dss_precipitacion' => 'catchments',
      'dss_tempertura' => 'catchments',
      'dss_humedad' => 'catchments',
      'dss_viento' => 'catchments',
      'dss_nubosidad' => 'catchments',
      'dss_poblacion_pronosticada' => 'demand_sites',
      'dss_demanda_hidrica_civil' => 'demand_sites',
      'dss_patron_demanda_poblacion' => 'demand_sites',
      'dss_demanda_electrica_agregada' => '',
      'dss_capacidad' => 'reservoirs',
      'dss_volumen_muerto' => 'reservoirs',
      'dss_capacidad_turbinas' => 'reservoirs',
      'dss_potencia_nominal' => 'reservoirs',
      'dss_eficiencia' => 'reservoirs',
      'dss_vol_max_normal' => 'reservoirs',
      'dss_vol_min_normal' => 'reservoirs',
      'dss_fraccion_reserva' => 'reservoirs',
      'dss_prioridad_fuente' => 'reservoirs',
      'dss_prioridad_demanda_energia' => 'reservoirs',
      'dss_criterio_respeto_cme' => 'river_reaches',
      'dss_respeto_cme' => 'river_reaches',
      'dss_explotacion_forestal' => 'catchments',
      'dss_explotacion_minera' => 'catchments',
      'dss_desarrollo_agricola' => 'catchments',
      'dss_porcentaje_regadio' => 'catchments',
      'dss_eficiencia_ror' => 'run_of_river_hydro',
      'dss_capacidad_turbinas_ror' => 'run_of_river_hydro',
      'dss_porc_utilizacion_ror' => 'run_of_river_hydro',
      'dss_prioridad_demanda_energia_ror' => 'run_of_river_hydro',
      'dss_salto_ror' => 'run_of_river_hydro',
      'dss_pertenencia_al_caso_hres' => 'reservoirs',
      'dss_pertenencia_al_caso_hror' => 'run_of_river_hydro',
      'dss_z1_inicial' => 'catchments',
      'dss_z2_inicial' => 'catchments',
      'dss_reparticion_flujo_radical' => 'catchments',
      'dss_capacidad_retencion_superficial' => 'catchments',
      'dss_capacidad_retencion_profunda' => 'catchments',
      'dss_conductividad_radical' => 'catchments',
      'dss_conductividad_profunda' => 'catchments',
      'dss_factor_escorrentia' => 'catchments',
      'dss_kc' => 'catchments',
      'dss_retorno_desborde' => 'catchments',
      'dss_distribucion_inundacion' => 'catchments',
      'dss_umbral_desborde' => 'river_reaches',
      'dss_fraccion_caudal_desborde' => 'river_reaches',
      'dss_volumen_inicial_acuifero' => 'groundwater',
      'dss_capacidad_acuifero' => 'groundwater',
      'dss_infiltracion' => 'reservoirs',
      'dss_s0_supuesto' => 'reservoirs',
      'dss_capacidad_descarga' => 'reservoirs',
      'dss_contracarga' => 'reservoirs',
      'dss_porc_utilizacion' => 'reservoirs',
      'dss_evaporacion_neta' => 'catchments',
      'dss_cme' => '',
      'dss_area_subcuenca' => 'catchments',
      'dss_uso_suelo_real' => 'catchments',
      'dss_uso_suelo_porc_simulacion' => 'catchments',
      'dss_capacidad_conduccion' => '',
      'dss_porc_capacidad' => '',
      'dss_preferencia_conexion' => '',
      'dss_perdida_conduccion_sin_destino' => '',
      'dss_perdida_conduccion_acuifero' => '',
      'dss_max_derivacion' => '',
      'dss_ineficiencia_demanda_civil' => 'demand_sites',
      'dss_nivel_consumo_hidrico' => 'demand_sites',
      'dss_nivel_re-uso' => 'demand_sites',
      'dss_prioridad_usuario_civil' => 'demand_sites',
      'dss_inundacion_inicial' => 'catchments',
      'dss_existencia_hres' => 'reservoirs',
      'dss_existencia_hror' => 'run_of_river_hydro',
      'dss_demanda_neta_riego' => 'irrigation_districts',
      'dss_demanda_bruta_riego' => 'irrigation_districts',
      'dss_satisfaccion_demanda_riego' => 'irrigation_districts',
      'dss_entrega_efectiva' => 'irrigation_districts',
      'dss_deficit_riego' => 'irrigation_districts',
      'dss_seguridad_riego' => 'irrigation_districts',
      'dss_et_potencial' => 'irrigation_districts',
      'dss_et_efectiva' => 'irrigation_districts',
      'dss_p_eficaz' => 'irrigation_districts',
      'dss_et_potencial_referencia' => 'irrigation_districts',
      'dss_retorno_riego_superficial' => 'irrigation_districts',
      'dss_humedad_1' => 'irrigation_districts',
      'dss_humedad_2' => 'irrigation_districts',
      'dss_evapotranspiracion' => 'irrigation_districts',
      'dss_flujo_gw' => 'irrigation_districts',
      'dss_retorno_riego_subterraneo' => 'irrigation_districts',
      'dss_flujo_subsuperficial' => 'irrigation_districts',
      'dss_flujo_basal' => 'irrigation_districts',
      'dss_escorrentia_superficial' => 'irrigation_districts',
      'dss_irrigacion' => 'irrigation_districts',
      'dss_desbordamiento' => 'catchments',
      'dss_area_inundada' => 'catchments',
      'dss_volumen_inundacion' => 'catchments',
      'dss_profundidad_inundacion' => 'catchments',
      'dss_q_in' => 'river_reaches',
      'dss_aporte_superficial' => 'river_reaches',
      'dss_intercambio_acuifero_rio' => 'river_reaches',
      'dss_caudal_tramo_acuifero' => 'river_reaches',
      'dss_evaporacion_rio' => 'river_reaches',
      'dss_umbral_inundacion' => 'river_reaches',
      'dss_caudal_desborde' => 'river_reaches',
      'dss_caudal_saliente_tramo' => 'river_reaches',
      'dss_cota_rio' => 'river_reaches',
      'dss_velocidad' => 'river_reaches',
      'dss_caudal_entregado' => 'reservoirs',
      'dss_volumen_embalsado' => 'reservoirs',
      'dss_hidroenergia_mensual_de_embalse' => 'reservoirs',
      'dss_cota_lamina_embalse' => 'reservoirs',
      'dss_evaporacion_neta_desde_embalse' => 'reservoirs',
      'dss_demanda_energia' => 'reservoirs',
      'dss_deficit_hidroelectrico' => 'reservoirs',
      'dss_demanda_hidroelectrica_caudal' => 'reservoirs',
      'dss_caudal_turbinado' => 'reservoirs',
      'dss_vertido' => 'reservoirs',
      'dss_demanda_neta_civil' => 'demand_sites',
      'dss_demanda_civil_fuentes' => 'demand_sites',
      'dss_entrega_usuario_civil' => 'demand_sites',
      'dss_deficit_usuario_civil' => 'demand_sites',
      'dss_volumen_acuifero' => 'groundwater',
      'dss_flujo_acuifero_exceso_capacidad' => 'groundwater',
      'dss_delta_cota_acuifero_rio' => 'river_reaches',
      'dss_aporte_intercambio_acuifero-rio' => 'river_reaches',
      'dss_caudal_entregado_planta_de_pasada' => 'run_of_river_hydro',
      'dss_hidroenergia_mensual_de_pasada' => 'run_of_river_hydro',
      'dss_caudal_turbinado_planta_de_pasada' => 'run_of_river_hydro',
      'dss_caudal_abajo_derivacion' => 'run_of_river_hydro',
      'dss_caudal_derivado' => 'run_of_river_hydro',
      'dss_caudal_abajo_nodo_cme' => '',
      'dss_satisfaccion_cme' => '',
      'dss_caudal_minimo_ecologico_cme' => '',
      'dss_caudal_ecologico_efectivo' => '',
      'dss_deficit_cme' => '',
      'dss_seguridad_cme' => '',
      'dss_input_conduccion' => '',
      'dss_caudal_transferido' => '',
      'dss_input_enlace_retorno' => '',
      'dss_caudal_retorno' => '',
      'dss_indicadores_eloha_deficit' => 'river_reaches',
      'dss_indicadores_eloha_exceso' => 'river_reaches',
      'dss_indice_eloha_deficit' => 'river_reaches',
      'dss_indice_eloha_exceso' => 'river_reaches',
      'dss_impactos_eloha_verbales' => 'river_reaches',
      'dss_impactos_eloha_periodos' => 'river_reaches',
      'dss_veredicto_eloha' => 'river_reaches',
      'dss_superficie_inundable_embalses' => 'reservoirs',
      'dss_huella_zonas_de_interes' => 'reservoirs',
      'dss_huella_territorial_por_categoria' => 'reservoirs',
      'dss_id_subred' => 'magdalena_streamsline_topology',
      'dss_longitud_cada_subred' => '',
      'dss_longitud_total_subredes' => '',
      'dss_longitud_relativa_no_afectada' => '',
      'dss_dor_h' => 'magdalena_streamsline_topology',
      'dss_longitud_max_subredes_dor_h' => '',
      'dss_vtot_embalses_arriba' => 'magdalena_streamsline_topology',
      'dss_dor_w' => 'magdalena_streamsline_topology',
      'dss_impacto_q_solido' => 'magdalena_streamsline_topology',
      'dss_red_impactada_por_q_solido' => '',
      'dss_q_medido' => 'hydrometric_stations',
      'dss_s_medido' => 'reservoirs',
      'dss_a_medido' => 'reservoirs',
      'dss_v_medido' => 'reservoirs',
      'dss_qturb_medido' => 'reservoirs',
    ];
    $layer_machine_name = isset($mappings[$dataset_machine_name]) ? $mappings[$dataset_machine_name] : NULL;
    return SimaLayer::loadByMachineName($layer_machine_name);
  }

  /**
   * Gets the Spatialization field.
   *
   * @return array
   *   The field that defines this map layer.
   */
  public function getSpatialization() {
    return $this->get(self::SPATIALIZATION);
  }

  /**
   * Obtains the Spatialization field in GeoJson format.
   *
   * Important: The geojson format for the "geoPHP" class is different than the
   * normal geojson and has the layer attributes stripped out because it cannot
   * read geojson attributes. If we want to get the original GeoJson for this
   * layer that also contains the attributes, use method "getOriginalGeoJson".
   *
   * @return string|NULL
   *   The Spatialization in GeoJson format or NULL.
   */
  public function getGeoJson() {
    module_load_include('inc', 'geophp', 'geoPHP/geoPHP');
    if (!empty($field = $this->getSpatialization())) {
      $geom = \geoPHP::load($field['wkt'], 'wkt');
      $geojson = $geom->out('json');
      return $geojson;
    }
    return NULL;
  }

  /**
   * Returns the original GeoJson layer.
   *
   * This GeoJson is the original layer and contains all attributes.
   *
   * @return string
   *   The Spatialization in GeoJson format or NULL.
   */
  public function getOriginalGeoJson() {
    $filename = $this->getMachineName() . '.geojson';
    $geojson_file = drupal_get_path('module', 'dss_import') . '/import/layers/' . $filename;
    $geojson = file_get_contents($geojson_file);
    return $geojson ? $geojson : NULL;
  }

}
