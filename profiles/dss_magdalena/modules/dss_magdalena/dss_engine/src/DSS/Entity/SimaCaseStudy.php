<?php
/**
 * @file
 * Sima Case Study.
 */

namespace Drupal\dss_magdalena\DSS\Entity;

use Drupal\dss_magdalena\DSS\Queue\SimaImportQueueItem;
use Drupal\dss_magdalena\DSS\Queue\SimaImportQueue;
use Drupal\dss_magdalena\DSS\Utils\SimaMasterFileGenerator;
use Drupal\dss_magdalena\WeapController;
use Drupal\dss_magdalena\DSS\Entity\SimaIntegralPlan;

/**
 * Models a Sima Case Study.
 *
 * Class SimaCaseStudy.
 *
 * @package Drupal\dss_magdalena\DSS\Entity
 */
class SimaCaseStudy extends EntityNodeAbstract implements SimaCaseStudyInterface {

  /**
   * Constants that manage Field References.
   */
  const BUNDLE                              = 'case_study';
  const INTEGRAL_PLAN                       = 'field_integral_plan';
  const SCENARIO                            = 'field_scenario';
  const PRODUCTIVE_LANDUSE_ALTERNATIVES     = 'field_prod_landuse_alternatives';
  const HYDROPOWER_ALTERNATIVES             = 'field_hydropower_alternatives';
  const WETLANDS_MANAGEMENT_ALTERNATIVE     = 'field_connectivity_alternatives';
  const CLIMATE_SCENARIOS                   = 'field_climate_scenario';
  const ENERGY_SCENARIOS                    = 'field_energy_scenario';
  const POPULATION_SCENARIOS                = 'field_population_scenario';

  // Default Case Study ID.
  const VARIABLE_BASELINE_CASE_STUDY_ID     = 'dss_engine_baseline_case_study_id';
  const VARIABLE_EXTREME_CASE_STUDY_ID      = 'dss_engine_extreme_case_study_id';

  /**
   * Loads a Case Study from a Drupal entity or a Drupal entity ID.
   *
   * @param mixed $case_study
   *   The nid or loaded object.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaCaseStudy|FALSE
   *   The loaded SimaCaseStudy object if exists, FALSE otherwise.
   */
  public static function load($case_study) {
    if ($case_study instanceof SimaCaseStudy) {
      return $case_study;
    }
    else {
      if ($entity = parent::load($case_study)) {
        return ($entity->getDrupalBundle() == self::BUNDLE) ? $entity : FALSE;
      }
      return FALSE;
    }
  }

  /**
   * Obtains the SimaDefaultCaseStudy Object to use its methods..
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaDefaultCaseStudy|FALSE
   *   The SimaDefaultCaseStudy object after loading the current case.
   */
  public function getDefaultCaseStudy() {
    return SimaDefaultCaseStudy::load($this->getCaseStudy()->value());
  }

  /**
   * Returns the Case Study.
   *
   * @return \EntityMetadataWrapper|object
   *   The loaded Drupal EntityMetadataWrapper object.
   */
  public function getCaseStudy() {
    return $this->getDrupalEntity();
  }

  /**
   * Returns the Integral Plan.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaIntegralPlan|FALSE
   *   The Integral Plan loaded in SimaIntegralPlan object.
   */
  public function getIntegralPlan() {
    $integral_plan = $this->get(self::INTEGRAL_PLAN);
    return SimaIntegralPlan::load($integral_plan);
  }

  /**
   * Returns the Scenario.
   *
   * @return mixed
   *   The Drupal Node for Scenario.
   */
  public function getScenarios() {
    return $this->get(self::SCENARIO);
  }

  /**
   * Returns the Productive Land Use Alternatives.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaAlternative|FALSE
   *   Returns the Productive Land Use Alternatives.
   */
  public function getProductiveLandUseAlternatives() {
    if ($integral_plan = $this->getIntegralPlan()) {
      return $integral_plan->getProductiveLandUseAlternative();
    }
    return FALSE;
  }

  /**
   * Returns the Hydropower Alternatives.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaAlternative|FALSE
   *   Returns the list of Hydropower Alternatives.
   */
  public function getHydropowerAlternatives() {
    if ($integral_plan = $this->getIntegralPlan()) {
      return $integral_plan->getHydropowerAlternative();
    }
    return FALSE;
  }

  /**
   * Returns the Wetland Management Alternatives.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaAlternative|FALSE
   *   Returns the list of Wetland Management Alternatives.
   */
  public function getWetlandManagementAlternatives() {
    if ($integral_plan = $this->getIntegralPlan()) {
      return $integral_plan->getWetlandManagementAlternative();
    }
    return FALSE;
  }

  /**
   * Returns the Climate Scenarios.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaScenario|FALSE
   *   Returns the list of Climate Scenarios.
   */
  public function getClimateScenarios() {
    $scenario = $this->getCaseStudy()->{self::SCENARIO}->{self::CLIMATE_SCENARIOS};
    return SimaScenario::load($scenario);
  }

  /**
   * Returns the Energy Scenarios.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaScenario|FALSE
   *   Returns the list of Energy Scenarios.
   */
  public function getEnergyScenarios() {
    $scenario = $this->getCaseStudy()->{self::SCENARIO}->{self::ENERGY_SCENARIOS};
    return SimaScenario::load($scenario);
  }

  /**
   * Returns the Population Scenarios.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaScenario|FALSE
   *   Returns the list of Population Scenarios.
   */
  public function getPopulationScenarios() {
    $scenario = $this->getCaseStudy()->{self::SCENARIO}->{self::POPULATION_SCENARIOS};
    return SimaScenario::load($scenario);
  }

  /**
   * Get the list of datasets machine names of all resources in this case study.
   *
   * Obtains the list of all dataset machine names for which resources exists
   * in this Case Study. This is useful to calculate if the case is ready to
   * be executed (we have all the required datasets needed to run the models).
   * If the $ready parameter is TRUE, it will evaluate case readiness: In this
   * case alternatives which have at least one project selected are assumed to
   * have all datasets required by the alternative. If FALSE, we are getting
   * the actual resources linked to this case study.
   *
   * @param bool|TRUE $ready
   *   TRUE if we are evaluating case readiness, FALSE to get actual list.
   *
   * @return array
   *   An array of dataset machine names.
   */
  public function getReferencedDatasetMachineNames($ready = TRUE) {
    /** @var \Drupal\dss_magdalena\DSS\Entity\SimaResource[] $resources */
    $resources = array();
    $datasets = [];

    if ($ready) {
      // Get the list of Scenarios and Alternatives.
      $scenarios = array(
        SimaScenario::CLIMATE_SCENARIO => $this->getClimateScenarios(),
        SimaScenario::POPULATION_SCENARIO => $this->getPopulationScenarios(),
        SimaScenario::ENERGY_SCENARIO => $this->getEnergyScenarios(),
      );

      // Obtain the list of resources for each of them.
      foreach ($scenarios as $key => $scenario) {
        // Obtaining the resources from the scenario.
        $resources = array_merge($resources, $scenario->getResources());
      }

      /** @var \Drupal\dss_magdalena\DSS\Entity\SimaAlternative[] $alternatives */
      $alternatives = array();
      if ($alternative = $this->getProductiveLandUseAlternatives()) {
        $alternatives[SimaAlternative::PRODUCTIVE_LANDUSE_ALTERNATIVE] = $alternative;
      }
      if ($alternative = $this->getHydropowerAlternatives()) {
        $alternatives[SimaAlternative::HYDROPOWER_ALTERNATIVE] = $alternative;
      }
      if ($alternative = $this->getWetlandManagementAlternatives()) {
        $alternatives[SimaAlternative::WETLANDS_MANAGEMENT_ALTERNATIVE] = $alternative;
      }
      foreach ($alternatives as $type => $alternative) {
        $projects = $alternative->getFichaProjects();
        $default_prod_landuse = $this->getDefaultCaseStudy()->isExtreme() && $type == SimaAlternative::PRODUCTIVE_LANDUSE_ALTERNATIVE;
        $empty_hydropower_alternative = $type == SimaAlternative::HYDROPOWER_ALTERNATIVE;

        // Only allow the extreme case study to not have productive land use
        // projects.
        if (count($projects) > 0 || $default_prod_landuse || $empty_hydropower_alternative) {
          $mappings = $alternative->getMappingToDatasets();
          foreach ($mappings as $mapping) {
            $datasets = array_merge($datasets, array_keys($mapping));
          }
        }

      }
    }
    else {
      $case_resources = $this->getResources();
      foreach ($case_resources as $key => $resource) {
        $resources = array_merge($resources, $resource);
      }
    }

    // Extracting the datasets linked to resources.
    foreach ($resources as $resource) {
      if ($dataset = $resource->getDataset()) {
        $datasets[] = $dataset->getMachineName();
      }
    }
    return $datasets;
  }

  /**
   * Gets Case Readiness.
   *
   * @param string $model_machine_name
   *   The model machine name.
   *
   * @return array|bool
   *   An array of required and unset variables.
   */
  public function getCaseReadiness($model_machine_name) {
    $dataset_machine_names = [];

    switch ($model_machine_name) {
      case SimaModel::MODEL_WEAP:
        // Obtain the list of resources attached to the case + scenarios.
        // The Alternatives do not have resources attached to them, they have
        // projects, which in turn are "linked" to a number of resources.
        $dataset_machine_names = $this->getReferencedDatasetMachineNames();
        break;

      case SimaModel::MODEL_ELOHA:
        // In order to calculate the ELOHA Outputs, we require the WEAP Model
        // Outputs as ELOHA Inputs.
        $resources = $this->getCaseOutputs()->getOutputResources(SimaModel::MODEL_WEAP);
        foreach ($resources as $resource) {
          if ($resource->getDataset()) {
            $dataset_machine_names[] = $resource->getDataset()
              ->getMachineName();
          }
          else {
            drupal_set_message(t('The resource "%title" (nid = %id) is not linked to a Dataset.', array(
              '%title' => $resource->getTitle(),
              '%id' => $resource->getId(),
            )), 'warning');
          }
        }
        break;

      case SimaModel::MODEL_DOR_H:
      case SimaModel::MODEL_DOR_W:
      case SimaModel::MODEL_SEDIMENTS:
      case SimaModel::MODEL_FRAGMENTATION:
      case SimaModel::MODEL_TERRITORIAL_FOOTPRINT:
        $resources = SimaResource::listByCaseStudy($this->getId());
        foreach ($resources as $resource) {
          if ($resource->getDataset()) {
            $dataset_machine_names[] = $resource->getDataset()
              ->getMachineName();
          }
          else {
            drupal_set_message(t('The resource "%title" (nid = %id) is not linked to a Dataset.', array(
              '%title' => $resource->getTitle(),
              '%id' => $resource->getId(),
            )), 'warning');
          }
        }
        break;

    }

    // Load all the required input datasets for this particular model.
    $model = SimaModel::loadByMachineName($model_machine_name);
    $required_datasets = $model ? $model->getRequiredInputDatasets() : [];

    // Compare required input datasets to datasets in the case study.
    $unset_datasets = $required_datasets;
    foreach ($required_datasets as $key => $required_dataset) {
      if (in_array($required_dataset->getMachineName(), $dataset_machine_names)) {
        unset($unset_datasets[$key]);
      }
    }

    // Now we have the list of undefined variables (datasets).
    return array(
      'required_variables' => $required_datasets,
      'unset_variables' => $unset_datasets,
    );
  }

  /**
   * Checks if the model is ready for execution.
   *
   * @param string $model_machine_name
   *   The model machine name.
   *
   * @return array|bool
   *   An array of required and unset variables.
   */
  public function isModelReadyForExecution($model_machine_name) {
    $data = $this->getCaseReadiness($model_machine_name);
    $datasets = [];
    switch ($model_machine_name) {
      case SimaModel::MODEL_WEAP:
      case SimaModel::MODEL_FRAGMENTATION:
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          case SimaModel::MODEL_DOR_H:
      case SimaModel::MODEL_DOR_W:
      case SimaModel::MODEL_SEDIMENTS:
        $datasets = [
          'dss_capacidad' => 'Storage Capacity',
          'dss_pertenencia_al_caso_hres' => 'SIMA Case status',
          'dss_project_id_hres' => 'Project ID Hres',
          'dss_topology_arcid_hres' => 'Topology ARCID Hres',
          'dss_pertenencia_al_caso_hror' => 'SIMA Case status',
          'dss_project_id_hror' => 'Project ID HRoR',
          'dss_topology_arcid_hror' => 'Topology ARCID HRoR',
        ];
        break;

      case SimaModel::MODEL_ELOHA:
        break;

      case SimaModel::MODEL_TERRITORIAL_FOOTPRINT:
        $datasets = [
          'dss_capacidad' => 'Storage Capacity',
        ];
        break;
    }

    /** @var \Drupal\dss_magdalena\DSS\Entity\SimaDataset $dataset */
    foreach ($data['unset_variables'] as $key => $dataset) {
      if (in_array($dataset->getMachineName(), array_keys($datasets))) {
        unset($data['unset_variables'][$key]);
      }
    }
    return $data;
  }

  /**
   * Gets the list of Resources (Caja de Datos) for the current case study.
   *
   * @return array
   *   An array of resources.
   */
  public function getResources() {
    $resources = array();

    // Get the list of Scenarios and Alternatives.
    $scenarios_alternatives = array(
      SimaScenario::CLIMATE_SCENARIO => $this->getClimateScenarios(),
      SimaScenario::POPULATION_SCENARIO => $this->getPopulationScenarios(),
      SimaScenario::ENERGY_SCENARIO => $this->getEnergyScenarios(),
      SimaAlternative::PRODUCTIVE_LANDUSE_ALTERNATIVE => $this->getProductiveLandUseAlternatives(),
      // SimaAlternative::MINING_ALTERNATIVE => $this->getMiningAlternatives(),
      // SimaAlternative::FOREST_ALTERNATIVE => $this->getForestAlternatives(),
      SimaAlternative::HYDROPOWER_ALTERNATIVE => $this->getHydropowerAlternatives(),
      SimaAlternative::WETLANDS_MANAGEMENT_ALTERNATIVE => $this->getWetlandManagementAlternatives(),
    );

    // Obtain the list of resources for each of them.
    foreach ($scenarios_alternatives as $key => $scenario_alternative) {
      // Obtaining the resources from the scenario or alternative.
      $resources[$key] = $scenario_alternative->getResources($this->getId());
    }

    return $resources;
  }

  /**
   * Gets the list of entities.
   *
   * @param \EntityListWrapper $entity_list_wrapper
   *   A list of scenarios or alternatives.
   *
   * @return array
   *   An array of Scenarios/Alternatives.
   */
  protected function getListOfEntities(\EntityListWrapper $entity_list_wrapper) {
    $entities = array();
    foreach ($entity_list_wrapper->getIterator() as $entity) {
      $entities[] = SimaController::load($entity);
    }
    return $entities;
  }

  /**
   * Generates the contents for the file masterfile.xlsx.
   *
   * @return string
   *   The contents of the masterfile.
   */
  protected function generateMasterFile() {
    // Create here logic to generate contents of the Master File.
    $masterfile_generator = new SimaMasterFileGenerator($this->getId());
    $contents = $masterfile_generator->generate();
    return $contents;
  }

  /**
   * Exports the current Case Study to WebDAV/WEAP Server.
   *
   * @return bool
   *   TRUE if exported successfully, FALSE otherwise.
   */
  public function export() {
    $cid = $this->getId();

    $status = 'NEW';
    $masterfile_content = $this->generateMasterFile();

    $weap_connector = new WeapController();
    $success = $weap_connector->exportCaseStudy($cid, $status, $masterfile_content);

    // If there was a problem exporting, print an error.
    if (!$success) {
      watchdog(WATCHDOG_ERROR, 'Case Study %title (CID = %id) could not be exported.', array(
        '%title' => $this->getCaseStudy()->title->value(),
        '%id' => $this->getId(),
      ));
      drupal_set_message(t('Case Study %title (CID = %id) could not be exported.', array(
        '%title' => $this->getCaseStudy()->title->value(),
        '%id' => $this->getId(),
      )), 'error');
      return FALSE;
    }
    else {
      drupal_set_message(t('Case Study %title (CID = %id) was exported successfully.', array(
        '%title' => $this->getCaseStudy()->title->value(),
        '%id' => $this->getId(),
      )), 'status');
      return TRUE;
    }
  }

  /**
   * Imports the modelation results from WEAP into the case study.
   */
  public function import() {
    $cid = $this->getId();
    $weap_connector = new WeapController();
    if ($zip_file = $weap_connector->importCaseStudy($cid)) {
      $this->importWeapOutputs($zip_file);
    }
  }

  /**
   * Imports the Zip File into WEAP Outputs.
   *
   * @param string $zip_file
   *   An URI that notes the Zip file.
   */
  public function importWeapOutputs($zip_file) {
    // Unzip file.
    // Create for each unzipped file a resource linked to the case.
    // Create a 'case_output' that links the case to all resources (weap
    // outputs).
    /** @var ArchiverInterface $archiver */
    $archiver = archiver_get_archiver($zip_file);
    $directory = dirname($zip_file);
    if ($archiver->extract($directory)) {
      file_unmanaged_delete($zip_file);
      $this->ImportWeapOutputsFromDirectory($directory);
    }
  }

  /**
   * Import WEAP Outputs from a particular directory.
   *
   * @param string $directory
   *   The full path directory.
   */
  public function importWeapOutputsFromDirectory($directory) {
    // First create a case output if there is none yet.
    $case_output = SimaCaseOutputs::loadByCaseStudyCreateIfNeeded($this->getId());

    // In the case directory, scan all files that are named *.csv.
    $files = array();
    $files += file_scan_directory($directory, '/.csv$/');

    foreach ($files as $file) {
      // Adds a new item to the queue, if the resource does not exist yet.
      if ($case_output->checkWeapOutputExistsForDataset($file->name) === FALSE) {
        // Creating a queue item for every item in the queue.
        $item = new SimaImportQueueItem();
        $item->setCaseStudyId($this->getId());
        $item->setCaseOutputId($case_output->getId());
        $item->setFile($file);

        // Saving item in the queue.
        $sima_import_queue = new SimaImportQueue();
        $sima_import_queue->createItem($item);
      }
    }

  }

  /**
   * Returns an array of all models this Case Study can run.
   *
   * @return array|\Drupal\dss_magdalena\DSS\Entity\SimaModel[]
   *   Returns an array of SimaModel entities.
   */
  public function getModels() {
    // Read all models from Database.
    $sima_controller = new SimaController();
    return $sima_controller->listEntities(SimaModel::BUNDLE);
  }

  /**
   * Returns the model status for all or one specific model.
   *
   * @param string $model_machine_name
   *   The model machine_name.
   * @param bool $print_value
   *   TRUE to print the status value, FALSE to print the status code.
   *
   * @return array|int
   *   The Model Status for all models or a single one, if given.
   */
  public function getModelingStatus($model_machine_name = NULL, $print_value = FALSE) {
    $status = intval($this->getCaseOutputs()->getModelingStatus($model_machine_name));
    if ($print_value) {
      return SimaModel::getStatusLabel($status, $print_value);
    }
    return $status;
  }

  /**
   * Obtain whether the current case is being processed in WEAP.
   *
   * @return bool
   *   TRUE if WEAP modeling status is processing, FALSE otherwise.
   */
  public function isProcessingWeapModelingStatus() {
    $status = $this->getModelingStatus(SimaModel::MODEL_WEAP);
    return ($status == SimaModel::STATUS_PROCESSING);
  }

  /**
   * Obtain whether the current case is being set as OK in WEAP.
   *
   * @return bool
   *   TRUE if WEAP modeling status is OK, FALSE otherwise.
   */
  public function isOkWeapModelingStatus() {
    $status = $this->getModelingStatus(SimaModel::MODEL_WEAP);
    return ($status == SimaModel::STATUS_OK);
  }

  /**
   * Sets the model status for all models at once.
   *
   * @param array $modeling_status_list
   *   An array of model status keyed by model_machine_name.
   */
  public function setModelingStatusList($modeling_status_list) {
    $this->getCaseOutputs()->setModelStatusList($modeling_status_list)->save();
  }

  /**
   * Sets the model status for a single model.
   *
   * @param string $model_machine_name
   *   The model machine_name.
   * @param int $status
   *   The status of the model.
   */
  public function setModelingStatus($model_machine_name, $status) {
    $this->getCaseOutputs()->setModelingStatus($model_machine_name, $status)->save();
  }

  /**
   * Returns the Case Outputs.
   *
   * Searches for Case Outputs. If there isn't then it creates one.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaCaseOutputs
   *   The loaded SimaCaseOutputs object.
   */
  public function getCaseOutputs() {
    // Get the Case Output related to this Case Study or create one if it does
    // not exist yet.
    $sima_case_output = SimaCaseOutputs::loadByCaseStudyCreateIfNeeded($this->getId());
    return $sima_case_output;
  }

  /**
   * Obtains the Import Status for a model of this particular Case Study.
   *
   * @param string $model
   *   Model Identifier.
   * @param string $language
   *   The language code.
   *
   * @return string
   *   The Import Status.
   */
  public function getImportStatus($model, $language = LANGUAGE_NONE) {
    if ($case_outputs = $this->getCaseOutputs()) {
      return $case_outputs->getImportStatus($model, $language);
    }
    return SimaCaseOutputs::STATUS_NOT_IMPORTED;
  }

  /**
   * Loads a SimaCaseStudy given its title.
   *
   * @param string $title
   *   The Case Study Title.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaCaseStudy|FALSE
   *   The loaded SimaCaseStudy object if exists, FALSE otherwise.
   */
  public static function loadCaseByTitle($title) {
    return static::loadByTitle($title, static::BUNDLE);
  }

  /**
   * Validates that the Case Study is valid to run models.
   *
   * @return bool
   *   TRUE if it validates, FALSE otherwise.
   */
  public function validate() {
    $destination = array('query' => array(drupal_get_destination()));

    // Check that It has an Integral Plan attached to it.
    $integral_plan = $this->getIntegralPlan();
    if (empty($integral_plan)) {
      drupal_set_message(t('The Case Study "@case" does not have an Integral Plan. Please add an !integral_plan to this Case.', array(
        '@case' => $this->getTitle(),
        '!integral_plan' => l(t('Integral Plan'), 'node/' . $this->getId() . '/edit', $destination),
      )), 'error');
      return FALSE;
    }

    // Now check that it has an scenario attached to it.
    $scenarios = $this->getScenarios();
    if (empty($scenarios)) {
      drupal_set_message(t('The Case Study "@case" does not have a Scenario attached to it. Please add an !scenario to this Case.', array(
        '@case' => $this->getTitle(),
        '!scenario' => l(t('Scenario'), 'node/' . $this->getId() . '/edit', $destination),
      )), 'error');
      return FALSE;
    }

    // Verify Productive Land Use Alternatives.
    $prod_landuse_alternative = $this->getProductiveLandUseAlternatives();
    if (empty($prod_landuse_alternative)) {
      drupal_set_message(t('The Integral Plan "@integral_plan" attached to Case Study "@case" does not have a Productive Land Use Alternative. !add.', array(
        '@integral_plan' => $integral_plan->title,
        '@case' => $this->getTitle(),
        '!add' => l(t('Please add it'), 'node/' . $integral_plan->nid . '/edit', $destination),
      )), 'error');
      return FALSE;
    }

    // Verify Hydropower Alternatives.
    $hydropower_alternative = $this->getHydropowerAlternatives();
    if (empty($hydropower_alternative)) {
      drupal_set_message(t('The Integral Plan "@integral_plan" attached to Case Study "@case" does not have Hydropower Alternatives. !add.', array(
        '@integral_plan' => $integral_plan->title,
        '@case' => $this->getTitle(),
        '!add' => l(t('Please add it'), 'node/' . $integral_plan->nid . '/edit', $destination),
      )), 'error');
      return FALSE;
    }

    // Verify Wetland Management Alternatives.
    // @TODO: Fix Wetlands Management Projects.
    $wetland_alternative = $this->getWetlandManagementAlternatives();
    // If (empty($wetland_alternative)) {.
    if (FALSE) {
      drupal_set_message(t('The Integral Plan "@integral_plan" attached to Case Study "@case" does not have Wetland Management Alternatives. !add.', array(
        '@integral_plan' => $scenarios->title,
        '@case' => $this->getTitle(),
        '!add' => l(t('Please add it'), 'node/' . $integral_plan->nid . '/edit', $destination),
      )), 'error');
      return FALSE;
    }

    // Verify Climate Scenarios.
    $climate_scenario = $this->getClimateScenarios();
    if (empty($climate_scenario)) {
      drupal_set_message(t('The Scenario "@scenario" attached to Case Study "@case" does not have a Climate Sub-scenario. !add.', array(
        '@scenario' => $scenarios->title,
        '@case' => $this->getTitle(),
        '!add' => l(t('Please add it'), 'node/' . $scenarios->nid . '/edit'),
      )), 'error');
      return FALSE;
    }

    // Verify Population Scenarios.
    $population_scenario = $this->getPopulationScenarios();
    if (empty($population_scenario)) {
      drupal_set_message(t('The Scenario "@scenario" attached to Case Study "@case" does not have a Population Sub-scenario. !add.', array(
        '@scenario' => $scenarios->title,
        '@case' => $this->getTitle(),
        '!add' => l(t('Please add it'), 'node/' . $scenarios->nid . '/edit', $destination),
      )), 'error');
      return FALSE;
    }

    // Verify that the existent Alternatives do have projects attached to them.
    // If they do not, do not trigger a validation error, however, we still
    // need to print a message to the user.
    if ($prod_landuse_alternative) {
      $projects = $prod_landuse_alternative->getFichaProjects();
      if (empty($projects)) {
        drupal_set_message(t('The Productive Land Use Alternative "@alternative" (Case: "@case") does not have projects attached to it . Please select some !projects.', array(
          '@alternative' => $prod_landuse_alternative->getTitle(),
          '@case' => $this->getTitle(),
          '!projects' => l(t('productive land use projects'), 'node/' . $prod_landuse_alternative->getId() . '/edit', $destination),
        )), 'error');
      }
    }

    // @codingStandardsIgnoreStart
    // If ($hydropower_alternative) {
    //      $projects = $hydropower_alternative->getFichaProjects();
    //      if (empty($projects)) {
    //        drupal_set_message(t('The Hydropower Alternative "@alternative" (Case: "@case") does not have projects attached to it . Please select some !projects.', array(
    //          '@alternative' => $hydropower_alternative->getTitle(),
    //          '@case' => $this->getTitle(),
    //          '!projects' => l(t('hydropower dams/plants or ROR hydropower plants'), 'node/' . $hydropower_alternative->getId() . '/edit', $destination),
    //        )), 'error');
    //      }
    //    }
    // @codingStandardsIgnoreEnd
    if (FALSE) {
      // If ($wetland_alternative) {.
      $projects = $wetland_alternative->getFichaProjects();
      if (empty($projects)) {
        drupal_set_message(t('The Wetlands Management Alternative "@alternative" (Case: "@case") does not have projects attached to it . Please select some !projects.', array(
          '@alternative' => $wetland_alternative->getTitle(),
          '@case' => $this->getTitle(),
          '!projects' => l(t('wetland management projects'), 'node/' . $wetland_alternative->getId() . '/edit', $destination),
        )), 'error');
      }
    }
    return TRUE;

  }

  /**
   * Populates this case's Hydropower alternative with baseline power plants.
   *
   * Baseline powerplants are the ones that have "DSS existencia HRES/HROR"
   * set to 1.
   */
  public function populateBaselinePowerPlants() {
    // Loading hydropower alternatives.
    if ($alternatives = $this->getHydropowerAlternatives()) {
      $alternatives->populateHydropowerAlternatives();
      // Saving hydropower alternative.
      $alternatives->save();
    }
  }

  /**
   * Creates objectives for the case study.
   *
   * @deprecated
   */
  public function createObjectives() {
    module_load_include('inc', 'dss_engine', 'dss_engine.objectives');
    dss_engine_create_resource_objectives_for_case_study($this->getId());
  }

}
