<?php
/**
 * @file
 * Models the Taxonomy term for vocabulary 'format'.
 */

namespace Drupal\dss_magdalena\DSS\Entity\Term;

/**
 * Implements a taxonomy term for vocabulary 'format'.
 *
 * Class SimaResourceFormat.
 *
 * @package Drupal\dss_magdalena\DSS\Entity\Term
 */
class SimaResourceFormat extends EntityTermAbstract {

  const BUNDLE          = 'format';

  const NAME_CSV        = 'csv';
  const NAME_ARCGIS     = 'arcgis';
  const NAME_GEOJSON    = 'geojson';
  const NAME_KML        = 'kml';
  const NAME_EXCEL      = 'excel';
  const NAME_ESRI_REST  = 'esri rest';
  const NAME_OPENXML    = 'openxml';
  const NAME_XLS        = 'xls';

  /**
   * Loads a SimaResourceFormat from a Drupal entity or a Drupal entity ID.
   *
   * @param mixed $format
   *   The taonomy_term tid or loaded object.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Term\SimaResourceFormat|FALSE
   *   The loaded SimaResourceFormat object if exists, FALSE otherwise.
   */
  public static function load($format) {
    if ($format instanceof SimaResourceFormat) {
      return $format;
    }
    else {
      if ($entity = parent::load($format)) {
        return ($entity->getDrupalBundle() == self::BUNDLE) ? $entity : FALSE;
      }
      return FALSE;
    }
  }

}
