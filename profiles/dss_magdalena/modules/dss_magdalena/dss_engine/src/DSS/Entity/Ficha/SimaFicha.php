<?php
/**
 * @file
 * Sima Fichas.
 */

namespace Drupal\dss_magdalena\DSS\Entity\Ficha;

use Drupal\dss_magdalena\DSS\Entity\EntityNodeAbstract;
use Drupal\dss_magdalena\DSS\Entity\SimaDataset;

/**
 * Class SimaFicha.
 *
 * @package Drupal\dss_magdalena\DSS
 *
 * Manages Ficha content types.
 */
class SimaFicha extends EntityNodeAbstract {

  const BUNDLE                   = '';

  // @codingStandardsIgnoreStart
  // const DOCUMENT                 = 'document';
  // const ORGANIZATION             = 'organization';
  // const PERSONA                  = 'persona';
  // const PHOTOGRAPHY              = 'photography';
  // const ROR_HYDROPOWER_PLANT     = 'ror_hydropower_plant';
  // @codingStandardsIgnoreEnd
  const PROJECT                  = 'field_project';

  /**
   * Defines the Datasets that are needed to create this particular Ficha.
   *
   * This is the list of all WEAP variables (Datasets) that is required to
   * obtain information to fill the Ficha Data.
   *
   * The information lists WEAP variables keyed by dataset machine names.
   *
   * @var array
   */
  protected $datasets = [];

  /**
   * Reference Dataset that will be used to gather the list of Projects.
   *
   * By filtering according to the specific filter defined for this purpose,
   * we will use this dataset to determine the list of projects. Usually this
   * list should be the same as the same list from any of the other datasets.
   *
   * @var string
   */
  protected $referenceDataset = '';

  /**
   * Returns the Datasets/WEAP Variable relationship.
   *
   * @return array
   *   The relationship Dataset <-> WEAP.
   */
  public function getDatasetsWeap() {
    return $this->datasets;
  }

  /**
   * Gets the Dataset, given the WEAP Variable.
   *
   * @param string $weap_variable
   *   The WEAP Variable name.
   *
   * @TODO: See if returning the first occurrence can generate a problem.
   *
   * @return string
   *   The Dataset machine name.
   */
  public function getDatasetMachineName($weap_variable) {
    // We are only searching for the first occurrence. This is not ideal but
    // will work for now. This should be checked in cases when we have
    // datasets with the same WEAP variable name.
    return array_search($weap_variable, $this->getDatasetsWeap());
  }

  /**
   * Returns the name of the Reference Dataset.
   *
   * @return string
   *   The referenced dataset.
   */
  public function getReferenceDataset() {
    return $this->referenceDataset;
  }

  /**
   * Returns the Project associated to this Ficha.
   *
   * @return mixed
   *   The project object.
   */
  public function getProject() {
    return $this->getDrupalEntity()->{self::PROJECT};
  }

  /**
   * Sets a value for a specific Ficha field.
   *
   * This will determine.
   *
   * @param string $field
   *   The field name.
   * @param string $variable
   *   The variable name.
   * @param array $values
   *   The values to assign.
   */
  protected function setFieldValue($field, $variable, $values) {
    $dataset_name = $this->getDatasetMachineName($variable);

    if (!isset($values[$variable]) || $values[$variable] == 'DEFAULT') {
      // Find default value.
      $dataset = SimaDataset::loadByMachineName($dataset_name);
      $default_value = $dataset ? $dataset->getDefaultValue() : 0.0;
    }

    // If we had to search for a default value, then use it.
    $value = isset($default_value) ? $default_value : $values[$variable];

    // Set Field value.
    $this->set($field, $value);
  }

  /**
   * Returns a new fake SimaFicha for simplicity of use.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Ficha\SimaFichaInterface
   *   The newly created SimaFichaInterface object.
   */
  public static function newFakeFicha() {
    global $user;
    // Create the new empty Ficha Node.
    $ficha = static::newEntity('node', static::BUNDLE, $user->uid);
    return $ficha;
  }


  /**
   * Obtains the key 'levelx' or 'Level x' from the array.
   *
   * @param array $data
   *   The data array with levels.
   * @param int $level
   *   The level to obtain.
   *
   * @return string|null
   *   The array value for the key 'levelx' or 'Level x'.
   */
  protected function getRowKeyLevel(array $data, $level = 1) {
    return isset($data['level' . $level]) ? $data['level' . $level] :
      (isset($data['Level ' . $level]) ? $data['Level ' . $level] : NULL);
  }

  /**
   * Obtains the value for a particular key from the array.
   *
   * @param array $data
   *   The data array with levels.
   * @param string $key
   *   The key to obtain the value from.
   *
   * @return string|null
   *   The array value for the key provided.
   */
  protected function getRowKey(array $data, $key = 'level1') {
    return isset($data[$key]) ? $data[$key] : NULL;
  }

}
