<?php
/**
 * @file
 * Agricultural Development. Ficha Type 4a.
 */

namespace Drupal\dss_magdalena\DSS\Entity\Ficha;

use Drupal\dss_magdalena\DSS\Entity\SimaProject;
use Drupal\dss_magdalena\DSS\Entity\SimaCatchments;
use Drupal\dss_magdalena\DSS\Entity\Multifield\SimaLandUsePercentages;

/**
 * Implements a Ficha for Productive Land Use.
 *
 * Class SimaFichaProductiveLandUse.
 *
 * @package Drupal\dss_magdalena\DSS\Entity\Ficha
 */
class SimaFichaProductiveLandUse extends SimaFicha implements SimaFichaInterface {

  const BUNDLE                                         = 'productive_land_use';

  // Productive Land Use Fields.
  const FIELD_PROJECT_REF                              = 'field_project';
  const FIELD_CATCHMENT_REF                            = 'field_catchment';
  const FIELD_CATCHMENT_AREA                           = 'field_catchment_area';
  const FIELD_DEMAND_PRIORITY                          = 'field_demand_priority';
  const FIELD_CONNECTION_TO_WATER_SOURCE               = 'field_connection_to_water_source';
  const FIELD_PERCENTAGE_OF_IRRIGATED_AREA             = 'field_irr_perc_area';
  const FIELD_LAND_USE                                 = 'field_land_use';

  /**
   * {@inheritdoc}
   */
  protected $datasets = [
    'dss_area_subcuenca' => 'Area',
    'dss_uso_suelo_porc_simulacion' => 'Area',
    'dss_porcentaje_regadio' => 'Irrigated Area',
    'dss_prioridad_usuario_riego' => 'Demand priority',
    'dss_umbral_humedad_riego' => 'Lower threshold',
    'dss_umbral_humedad_parar_riego' => 'Upper threshold',
  ];

  /**
   * {@inheritdoc}
   */
  protected $referenceDataset = 'dss_porcentaje_regadio';

  /**
   * Loads a SimaFichaProductiveLandUse from a Drupal entity or an ID.
   *
   * @param mixed $ficha
   *   The ficha nid or loaded node.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Ficha\SimaFichaProductiveLandUse|FALSE
   *   The SimaFichaProductiveLandUse object if exists, FALSE otherwise.
   */
  public static function load($ficha) {
    if ($ficha instanceof SimaFichaProductiveLandUse) {
      return $ficha;
    }
    else {
      if ($entity = parent::load($ficha)) {
        return ($entity->getDrupalBundle() == self::BUNDLE) ? $entity : FALSE;
      }
      return FALSE;
    }
  }

  /**
   * Creates an new empty Ficha.
   *
   * @param array $project_data
   *   The Project Data.
   * @param array $ficha_data
   *   The Ficha Data.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Ficha\SimaFichaProductiveLandUse
   *   The SimaFicha object, FALSE otherwise.
   */
  static public function newFicha($project_data, $ficha_data) {
    global $user;
    // Create the new empty Ficha Node.
    $ficha = self::newEntity('node', self::BUNDLE, $user->uid);

    // Build the node object and save it.
    $ficha->newProductiveLandUse($project_data, $ficha_data);
    return $ficha;
  }

  /**
   * Builds the data for a new Agricultural Development Project.
   *
   * @param array $project_data
   *   The Project Data.
   * @param array $ficha_data
   *   The Ficha Data.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Ficha\SimaFichaProductiveLandUse
   *   The SimaFicha object.
   */
  protected function newProductiveLandUse($project_data, $ficha_data) {
    // Creates a new Project.
    $project = SimaProject::newProject();

    // Assign basic fields to Ficha.
    $catchment_name = $project_data['name'];
    $title = isset($catchment_name) ? t('Distrito de Riego: !title', array('!title' => $catchment_name)) : t('None');
    $this->getDrupalEntity()->title = $title;
    $this->getDrupalEntity()->body->set(array(
      'value' => '',
      'format' => 'full_html',
    ));

    // Assigning title to Project.
    $project->getDrupalEntity()->title = $title;

    // Assigning Demand Priority.
    if (!empty($project_data['demand_priority'])) {
      $this->set(self::FIELD_DEMAND_PRIORITY, $project_data['demand_priority']);
    }

    // Assigning Connection to Water Source (Transmission Link).
    $connection_to_water_source = $project_data['transmission_link'] ? 1 : 0;
    $this->set(self::FIELD_CONNECTION_TO_WATER_SOURCE, $connection_to_water_source);

    // Assigning Percentage of Irrigated Area.
    if (!empty($project_data['irrigated_area'])) {
      $this->set(self::FIELD_PERCENTAGE_OF_IRRIGATED_AREA, $project_data['irrigated_area']);
    }

    // Having the project data, we can locate the Catchment it is linked to.
    if ($sima_catchment = SimaCatchments::loadByName($catchment_name)) {
      $this->set(self::FIELD_CATCHMENT_REF, $sima_catchment->getDrupalEntity()->value());
    }

    // Assigning Percentage of Irrigated Area.
    if (!empty($project_data['area'])) {
      $this->set(self::FIELD_CATCHMENT_AREA, $project_data['area']);
    }

    $values = $this->parseLandUseValues($project_data);
    $this->set(self::FIELD_LAND_USE, $values);

    // All values have been assigned. Save Project and Ficha.
    $project->save();
    $this->getDrupalEntity()->{self::PROJECT}->set($project->getDrupalEntity());
    $this->save();
    return $this;

  }

  /**
   * Prepares the values to save to the fields.
   *
   * @param array $project_data
   *   The Fields array.
   *
   * @return array
   *   The same array after re-arranging its values.
   */
  protected function parseLandUseValues($project_data) {
    $values = [];
    foreach ($project_data as $branch_id => $data) {
      if (is_array($data)) {
        /** @var \Drupal\dss_magdalena\DSS\Entity\Multifield\SimaLandUsePercentages $land_use_percentages */
        $land_use_percentages = SimaLandUsePercentages::newEntity();
        $land_use_percentages->setBranchId($branch_id);
        $land_use_percentages->setLandUseType($data['Level 3']);
        $land_use_percentages->setPercentage($data['Value']);
        $values[] = (array) $land_use_percentages->getDrupalEntity()->value();
      }
    }
    return $values;
  }

  /**
   * Gets the catchment associated to this Agricultural Project.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaCatchments|FALSE
   *   A loaded SimaCatchments object if linked, FALSE otherwise.
   */
  public function getCatchment() {
    $catchment = $this->get(self::FIELD_CATCHMENT_REF);
    return SimaCatchments::load($catchment);
  }

  /**
   * Obtains the Catchment Area.
   *
   * @return string
   *   The Catchment Area.
   */
  public function getCatchmentArea() {
    return $this->get(self::FIELD_CATCHMENT_AREA);
  }

  /**
   * Obtains the Demand Priority.
   *
   * @return mixed
   *   The Demand Priority.
   */
  public function getDemandPriority() {
    return $this->get(self::FIELD_DEMAND_PRIORITY);
  }

  /**
   * Obtains the Demand Priority.
   *
   * @return bool
   *   TRUE if it has a connection to Water Source, FALSE otherwise.
   */
  public function getConnectionToWaterSource() {
    return (bool) $this->get(self::FIELD_CONNECTION_TO_WATER_SOURCE);
  }

  /**
   * Obtains the Percentages of Land Use area.
   *
   * @return mixed
   *   The Multifield Land Use Percentage.
   */
  public function getLandUsePercentages() {
    $irrigation_data = $this->get(self::FIELD_LAND_USE);
    foreach ($irrigation_data as $key => $item) {
      $irrigation_data[$key] = SimaLandUsePercentages::load($item);
    }
    return $irrigation_data;
  }

  /**
   * Returns the raw value for Land Use Percentages.
   *
   * @return mixed
   *   The raw value of Land Use percentages.
   */
  public function getRawLandUsePercentages() {
    return $this->get(self::FIELD_LAND_USE);
  }

}
