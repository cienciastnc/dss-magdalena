<?php
/**
 * @file
 * Defines an Node Entity Model.
 */

namespace Drupal\dss_magdalena\DSS\Entity;

use EntityFieldQuery;

/**
 * Models a Node Entity.
 *
 * Class EntityNodeAbstract.
 *
 * @package Drupal\dss_magdalena\DSS\Entity
 */
abstract class EntityNodeAbstract extends EntityAbstract {

  /**
   * Public constructor.
   *
   * @param \EntityDrupalWrapper|object $drupal_entity
   *   The node that maps to a Sima Node Entity.
   */
  public function __construct($drupal_entity) {
    $this->setDrupalEntity('node', $drupal_entity);
  }

  /**
   * Loads a SimaScenario from a Drupal entity or a Drupal entity ID.
   *
   * @param mixed $entity
   *   The nid or loaded object.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\EntityNodeAbstract|FALSE
   *   The loaded EntityNodeAbstract object if exists, FALSE otherwise.
   */
  public static function load($entity) {
    if ($entity instanceof \EntityDrupalWrapper || is_object($entity)) {
      return new static($entity);
    }
    elseif (is_array($entity)) {
      $entity_node = entity_create('node', $entity);
      return new static($entity_node);
    }
    elseif (is_int($entity) || (intval($entity) !== 0 && !is_array($entity))) {
      $entity_node = node_load($entity);
      if ($entity_node === FALSE) {
        return FALSE;
      }
      return new static($entity_node);
    }
    else {
      return FALSE;
    }
  }

  /**
   * Loads an entity using the UUID.
   *
   * @param string $uuid
   *   The entity's UUID.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\EntityNodeAbstract|FALSE
   *   The loaded entity if found, FALSE otherwise.
   */
  public static function loadByUuid($uuid) {
    if ($entity_node = entity_uuid_load('node', array($uuid))) {
      $entity_node = array_shift($entity_node);
      $nid = $entity_node->nid;
      return self::load($nid);
    }
    return FALSE;
  }

  /**
   * Sets up the Title.
   *
   * @param string $title
   *   The title to set.
   * @param string $lang
   *   The Language Code.
   */
  public function setTitle($title, $lang = LANGUAGE_NONE) {
    $this->set('title', $title, $lang);
  }

  /**
   * Sets up the Body Field.
   *
   * @param string $text
   *   The string to set the body to.
   */
  public function setBody($text) {
    $this->getDrupalEntity()->body->set(array(
      'value' => $text,
      'format' => 'full_html',
    ));
  }

  /**
   * Loads the entity by Title.
   *
   * @param string $title
   *   The Node title.
   * @param string $bundle
   *   The Entity bundle.
   *
   * @return object
   *   The Object loaded for the specific node type.
   */
  public static function loadByTitle($title, $bundle) {
    // Search for Case Outputs.
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', $bundle)
      ->propertyCondition('status', NODE_PUBLISHED)
      ->propertyCondition('title', $title);
    $result = $query->execute();

    if (!empty($result['node'])) {
      $node = reset($result['node']);
      return new static($node->nid);
    }
    return FALSE;
  }

  /**
   * Loads the entity by Title.
   *
   * @param array $titles
   *   An array of node titles.
   * @param string $bundle
   *   The Entity bundle.
   *
   * @return object[]
   *   The Objects loaded for the specific node type.
   */
  public static function loadListByTitles($titles, $bundle) {
    // Search for Case Outputs.
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', $bundle)
      ->propertyCondition('status', NODE_PUBLISHED)
      ->propertyCondition('title', $titles, 'IN');
    $result = $query->execute();

    if (!empty($result['node'])) {
      $results = [];
      foreach ($result['node'] as $node) {
        $results[] = new static($node->nid);
      }
      return $results;
    }
    return [];
  }

  /**
   * Returns the Entity Title.
   *
   * @return string
   *   The Entity Title.
   */
  public function getTitle() {
    return $this->getDrupalEntity()->label();
  }

  /**
   * Returns the Node Author.
   *
   * @return \EntityDrupalWrapper
   *   The Author entity wrapper.
   */
  public function getAuthor() {
    return $this->getDrupalEntity()->author;
  }

}
