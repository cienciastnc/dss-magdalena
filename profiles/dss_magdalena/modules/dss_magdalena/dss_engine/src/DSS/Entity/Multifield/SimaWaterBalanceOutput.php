<?php
/**
 * @file
 * Models the Water Balance Output for a Water Balance Template.
 */

namespace Drupal\dss_magdalena\DSS\Entity\Multifield;

use Drupal\dss_magdalena\DSS\Entity\SimaDataset;

/**
 * Class SimaWaterBalanceOutput.
 *
 * @package Drupal\dss_magdalena\DSS\Entity\Multifield
 */
class SimaWaterBalanceOutput extends EntityMultifieldAbstract {

  const BUNDLE                       = 'field_output';

  const FIELD_OUTPUT_VARIABLE        = 'field_outputvar';
  const FIELD_OUTPUT_FACTOR          = 'field_outputfac';

  /**
   * Loads a SimaWaterBalanceOutput from a Drupal entity or a Drupal entity ID.
   *
   * @param mixed $water_balance_input
   *   The multifield id or loaded object.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Multifield\SimaWaterBalanceOutput|FALSE
   *   A loaded SimaWaterBalanceOutput object if exists, FALSE otherwise.
   */
  public static function load($water_balance_input) {
    if ($water_balance_input instanceof SimaWaterBalanceOutput) {
      return $water_balance_input;
    }
    else {
      if ($entity = parent::load($water_balance_input)) {
        return ($entity->getDrupalBundle() == self::BUNDLE) ? $entity : FALSE;
      }
      return FALSE;
    }
  }

  /**
   * Returns the Variable (Dataset).
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaDataset|FALSE
   *   The SimaDataset object loaded if found, FALSE otherwise.
   */
  public function getVariable() {
    $output_variable = $this->get(self::FIELD_OUTPUT_VARIABLE);
    return SimaDataset::load($output_variable);
  }

  /**
   * Gets the Factor.
   *
   * @return string
   *   The Factor.
   */
  public function getFactor() {
    return $this->get(self::FIELD_OUTPUT_FACTOR);
  }

}
