<?php
/**
 * @file
 * Models Demand Sites and Catchments.
 */

namespace Drupal\dss_magdalena\DSS\Entity;

use Drupal\dss_magdalena\DSS\Entity\Term\SimaWeapLevel1;
use Drupal\dss_magdalena\DSS\Entity\Multifield\SimaLandUsePercentages;
use Drupal\dss_magdalena\DSS\Entity\Term\SimaLandUseType;
use EntityFieldQuery;

/**
 * Implements Sima Demand Sites and Catchments.
 *
 * Class SimaCatchments.
 *
 * @package Drupal\dss_magdalena\DSS\Entity
 */
class SimaCatchments extends EntityNodeAbstract {

  const BUNDLE                             = 'demand_sites_and_catchments';
  const FIELD_VARIABLE_ID                  = 'field_weap_variable_id';
  const FIELD_LEVEL1                       = 'field_level_1';
  const FIELD_LEVEL2                       = 'field_level_2';
  const FIELD_LEVEL3                       = 'field_level_3';
  const FIELD_LEVEL4                       = 'field_level_4';
  const FIELD_SPATIALIZATION               = 'field_spatialization';
  const FIELD_LAND_USE                     = 'field_land_use';
  const FIELD_AGRICULTURAL_AREA            = 'field_agricultural_area';
  const FIELD_FOREST_INDUSTRIAL_AREA       = 'field_forest_industrial_area';
  const FIELD_MINING_AREA                  = 'field_mining_area';
  const FIELD_CATCHMENT_REF                = 'field_catchment';
  const FIELD_CATCHMENT_AREA               = 'field_catchment_area';
  const FIELD_DEMAND_PRIORITY              = 'field_demand_priority';
  const FIELD_CONNECTION_TO_WATER_SOURCE   = 'field_connection_to_water_source';
  const FIELD_PERCENTAGE_OF_IRRIGATED_AREA = 'field_irr_perc_area';

  const FIELD_BRANCH_ID                    = 'field_branch_id';
  const FIELD_LAND_USE_REF                 = 'field_land_use_ref';
  const FIELD_PERCENTAGE_VALUE             = 'field_percentage_value';

  /**
   * Loads a SimaCatchments from a Drupal entity or a Drupal entity ID.
   *
   * @param mixed $catchments
   *   The nid or loaded object.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaCatchments|FALSE
   *   The loaded SimaCatchments object if exists, FALSE otherwise.
   */
  public static function load($catchments) {
    if ($catchments instanceof SimaCatchments) {
      return $catchments;
    }
    else {
      if ($entity = parent::load($catchments)) {
        return ($entity->getDrupalBundle() == self::BUNDLE) ? $entity : FALSE;
      }
      return FALSE;
    }
  }

  /**
   * Loads a Catchment given the Title (Catchment Name).
   *
   * @param int $name
   *   The Catchment Name (Title).
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaCatchments|FALSE
   *   The loaded SimaCatchments object if exists, FALSE otherwise.
   */
  public static function loadByName($name) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', self::BUNDLE)
      ->propertyCondition('status', NODE_PUBLISHED)
      ->propertyCondition('title', $name, '=');
    $result = $query->execute();

    if (isset($result['node'])) {
      $node = reset($result['node']);
      return new static($node->nid);
    }
    return FALSE;
  }

  /**
   * Loads a Catchment given the Branch ID.
   *
   * @param int $branch_id
   *   The Branch ID.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaCatchments|FALSE
   *   The loaded SimaCatchments object if exists, FALSE otherwise.
   */
  public static function loadByBranchId($branch_id) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', self::BUNDLE)
      ->propertyCondition('status', NODE_PUBLISHED)
      ->fieldCondition(self::FIELD_BRANCH_ID, 'value', $branch_id, '=');
    $result = $query->execute();

    if (isset($result['node'])) {
      $node = reset($result['node']);
      return new static($node->nid);
    }
    return FALSE;
  }

  /**
   * Loads a Catchment given the Full Branch Name.
   *
   * @param string $branch_name
   *   The Full Branch Name.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaCatchments|FALSE
   *   The loaded SimaCatchments object if exists, FALSE otherwise.
   */
  public static function loadByFullBranchName($branch_name) {
    // Separating branch name parts.
    $branch = explode('\\', $branch_name);
    if (empty($branch[1])) {
      return FALSE;
    }
    $name = $branch[1];
    return self::loadByName($name);
  }

  /**
   * Returns a new SimaCatchments node.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaCatchments
   *   The loaded SimaCatchments object if exists, FALSE otherwise.
   */
  static public function newCatchment() {
    global $user;
    // Create the new SimaCatchments Node.
    return self::newEntity('node', self::BUNDLE, $user->uid);
  }

  /**
   * Returns a SimaCatchments node.
   *
   * @param string $name
   *   The Catchment name.
   * @param array $rows
   *   The Masterfile records related to this catchment.
   * @param string $geojson_field
   *   The GeoJson field.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaCatchments
   *   The loaded SimaCatchments object if exists, FALSE otherwise.
   */
  static public function fromMasterfileRecord($name, $rows, $geojson_field = NULL) {
    // We could use any record to read basic information, so we take the first
    // one and later iterate over all records to fill land use fields.
    $catchment = self::newCatchment();

    foreach ($rows as $row) {
      if (isset($row['level1']) && isset($row['level2'])) {
        // We need a special treatment to 'level1' because it is a taxonomy
        // term reference.
        if ($weap_level1 = SimaWeapLevel1::loadByTaxonomyTermName($row['level1'])) {
          $catchment->set(self::FIELD_LEVEL1, $weap_level1->getDrupalEntity()->value());
        }
        $catchment->set(self::FIELD_LEVEL2, $row['level2']);

        // @TODO: Fix the Variable ID.
        // Setting up the Variable ID.
        // $variable_id = intval($row['VariableID']);
        // if ($variable_id !== 0) {
        // $catchment->set(self::FIELD_VARIABLE_ID, $variable_id);
        // }
        // break;
      }
    }

    // Assigning Demand Priority.
    if (!empty($rows['demand_priority'])) {
      $catchment->set(self::FIELD_DEMAND_PRIORITY, $rows['demand_priority']);
    }

    // Assigning Connection to Water Source (Transmission Link).
    $connection_to_water_source = $rows['transmission_link'] ? 1 : 0;
    $catchment->set(self::FIELD_CONNECTION_TO_WATER_SOURCE, $connection_to_water_source);

    // Assigning Percentage of Irrigated Area.
    if (!empty($rows['irrigated_area'])) {
      $catchment->set(self::FIELD_PERCENTAGE_OF_IRRIGATED_AREA, $rows['irrigated_area']);
    }

    // Assigning Catchment Area.
    if (!empty($rows['area'])) {
      $catchment->set(self::FIELD_CATCHMENT_AREA, $rows['area']);
    }

    // Assigning the values for Land Type Categories.
    $catchment->setLandTypesValues($rows);

    // We are eliminating Level 3 to be just [blank].
    // $row['Level 3'] = '';
    // $catchment->set(self::FIELD_LEVEL3, $row['Level 3']);.
    // We know Level 4 is [blank] but just to make sure.
    // $row['Level 4...'] = '';
    // $catchment->set(self::FIELD_LEVEL4, $row['Level 4...']);.
    // Setting title the "Level 2" name.
    $catchment->set('title', $name);

    // Setting up the GeoJson Field.
    if (!empty($geojson_field)) {
      $catchment->set(self::FIELD_SPATIALIZATION, $geojson_field);
    }

    // Now that basic fields are set, iterate over rows to get land use records.
    $values = $catchment->parseLandUseValues($rows);
    $catchment->set(self::FIELD_LAND_USE, $values);

    return $catchment;
  }

  /**
   * Sets the Agricultural Area for this Catchment.
   *
   * @param string $area
   *   The agricultural area in Km2.
   */
  public function setAgriculturalArea($area) {
    $this->set(self::FIELD_AGRICULTURAL_AREA, $area);
  }

  /**
   * Obtains the Agricultural Area for this Catchment.
   *
   * @param string $lang
   *   The Language Code.
   *
   * @return float
   *   The Agricultural area in Km2.
   */
  public function getAgriculturalArea($lang = LANGUAGE_NONE) {
    return floatval($this->get(self::FIELD_AGRICULTURAL_AREA, $lang));
  }

  /**
   * Sets the Forest/Industrial Area for this Catchment.
   *
   * @param string $area
   *   The agricultural area in Km2.
   */
  public function setForestAndIndustrialArea($area) {
    $this->set(self::FIELD_FOREST_INDUSTRIAL_AREA, $area);
  }

  /**
   * Obtains the Forest/Industrial  Area for this Catchment.
   *
   * @param string $lang
   *   The Language Code.
   *
   * @return float
   *   The Forest/Industrial  area in Km2.
   */
  public function getForestAndIndustrialArea($lang = LANGUAGE_NONE) {
    return floatval($this->get(self::FIELD_FOREST_INDUSTRIAL_AREA, $lang));
  }

  /**
   * Sets the Mining Area for this Catchment.
   *
   * @param string $area
   *   The agricultural area in Km2.
   */
  public function setMiningArea($area) {
    $this->set(self::FIELD_MINING_AREA, $area);
  }

  /**
   * Obtains the Mining Area for this Catchment.
   *
   * @param string $lang
   *   The Language Code.
   *
   * @return float
   *   The Mining area in Km2.
   */
  public function getMiningArea($lang = LANGUAGE_NONE) {
    return floatval($this->get(self::FIELD_MINING_AREA, $lang));
  }

  /**
   * Obtains the Percentage of Irrigated Area for this Catchment.
   *
   * @param string $lang
   *   The Language Code.
   *
   * @return float
   *   The Percentage of Irrigated area in %.
   */
  public function getPercentageOfIrrigatedArea($lang = LANGUAGE_NONE) {
    return floatval($this->get(self::FIELD_PERCENTAGE_OF_IRRIGATED_AREA, $lang));
  }

  /**
   * Prepares the values to save to the fields.
   *
   * @param array $project_data
   *   The Fields array.
   *
   * @return array
   *   The same array after re-arranging its values.
   */
  public function parseLandUseValues($project_data) {
    $values = [];
    foreach ($project_data as $branch_id => $data) {
      if (is_array($data)) {
        /** @var \Drupal\dss_magdalena\DSS\Entity\Multifield\SimaLandUsePercentages $land_use_percentages */
        $land_use_percentages = SimaLandUsePercentages::newMultifieldEntity();
        $land_use_percentages->setBranchId($branch_id);
        $land_use_percentages->setLandUseType($data['level3'], TRUE);
        $land_use_percentages->setPercentage($data['Value']);
        $values[] = (array) $land_use_percentages->getDrupalEntity()->value();
      }
    }
    return $values;
  }

  /**
   * Sets the values for the Categories of Land Types.
   *
   * @param array $project_data
   *   An array of Project data.
   */
  public function setLandTypesValues($project_data) {
    $agricultural_land = 0;
    $forest_industrial_land = 0;
    $mining_land = 0;
    $area = isset($project_data['area']) ? floatval($project_data['area']) : 0;
    foreach ($project_data as $branch_id => $data) {
      if (is_array($data)) {
        $name = $data['level3'];
        /** @var \Drupal\dss_magdalena\DSS\Entity\Term\SimaLandUseType $land_use_type */
        if ($land_use_type = SimaLandUseType::loadByTaxonomyTermName($name)) {
          if ($land_use_type->isAgriculturalLand()) {
            $agricultural_land += floatval($data['Value']);
          }
          elseif ($land_use_type->isForestAndIndustrialLand()) {
            $forest_industrial_land += floatval($data['Value']);
          }
          elseif ($land_use_type->isMiningLand()) {
            $mining_land += floatval($data['Value']);
          }
        }
      }
    }

    // Calculating real values in Km2 by multiplying percentages by total area.
    $this->set(self::FIELD_AGRICULTURAL_AREA, number_format($area * $agricultural_land / 100, 2, '.', ''));
    $this->set(self::FIELD_FOREST_INDUSTRIAL_AREA, number_format($area * $forest_industrial_land / 100, 2, '.', ''));
    $this->set(self::FIELD_MINING_AREA, number_format($area * $mining_land / 100, 2, '.', ''));
  }

  /**
   * Gets the catchment associated to this Agricultural Project.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaCatchments|FALSE
   *   A loaded SimaCatchments object if linked, FALSE otherwise.
   */
  public function getCatchment() {
    $catchment = $this->get(self::FIELD_CATCHMENT_REF);
    return SimaCatchments::load($catchment);
  }

  /**
   * Obtains the Catchment Area.
   *
   * @return float
   *   The Catchment Area.
   */
  public function getCatchmentArea() {
    return floatval($this->get(self::FIELD_CATCHMENT_AREA));
  }

  /**
   * Obtains the Demand Priority.
   *
   * @return mixed
   *   The Demand Priority.
   */
  public function getDemandPriority() {
    return $this->get(self::FIELD_DEMAND_PRIORITY);
  }

  /**
   * Gets the WEAP Level 1 Field Value.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Term\SimaWeapLevel1|FALSE
   *   THe loaded SimaWeapLevel1 object.
   */
  public function getWeapLevel1() {
    $level1 = $this->get(self::FIELD_LEVEL1);
    return SimaWeapLevel1::load($level1);
  }

  /**
   * Gets the WEAP Level 2 Field Value.
   *
   * @return mixed
   *   Return the Value for Level 2.
   */
  public function getWeapLevel2() {
    return $this->get(self::FIELD_LEVEL2);
  }

  /**
   * Obtains the Demand Priority.
   *
   * @return bool
   *   TRUE if it has a connection to Water Source, FALSE otherwise.
   */
  public function getConnectionToWaterSource() {
    return (bool) $this->get(self::FIELD_CONNECTION_TO_WATER_SOURCE);
  }

  /**
   * Obtains the Percentages of Land Use area.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Multifield\SimaLandUsePercentages[]
   *   An array of Multifield Land Use Percentages.
   */
  public function getLandUsePercentages() {
    $irrigation_data = $this->get(self::FIELD_LAND_USE);
    foreach ($irrigation_data as $key => $item) {
      $irrigation_data[$key] = SimaLandUsePercentages::load($item->id);
    }
    return $irrigation_data;
  }

  /**
   * Obtains the Simple Land Use Percentages of Land Use area.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Multifield\SimaSimpleLandUsePercentages[]
   *   An array of Multifield Land Use Percentages.
   */
  public function getSimpleLandUsePercentages() {
    $simple_landuse_percentage = $this->getLandUsePercentages();
    foreach ($simple_landuse_percentage as $key => $landuse_percentage) {
      $simple_landuse_percentage[$key] = $landuse_percentage->toSimpleLandUsePercentage();
    }
    return $simple_landuse_percentage;
  }

  /**
   * Obtains an array of Land Use Percentages per Land Use Type.
   *
   * @return array
   *   An array of percentages keyed by land type.
   */
  public function getConciseLandUsePercentages() {
    $land_use_percentages = $this->getLandUsePercentages();
    $percentages = array();
    /** @var \Drupal\dss_magdalena\DSS\Entity\Multifield\SimaLandUsePercentages $land_use_percentage */
    foreach ($land_use_percentages as $land_use_percentage) {
      $land_use_ref = $land_use_percentage->getLandUseType()->getId();
      $percentage_value = $land_use_percentage->getPercentage();
      $percentages[$land_use_ref] = $percentage_value;
    }
    return $percentages;
  }

  /**
   * Obtains an array of Land Use Areas per Land Use Type.
   *
   * @return array
   *   An array of areas in Km2 per land type.
   */
  public function getConciseLandUseAreas() {
    $catchment_area = $this->getCatchmentArea();
    $percentages = $this->getConciseLandUsePercentages();
    foreach ($percentages as $land_use_ref => $percentage) {
      $percentages[$land_use_ref] = floatval($percentage) * floatval($catchment_area);
    }
    return $percentages;
  }

}
