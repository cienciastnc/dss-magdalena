<?php
/**
 * @file
 * Models a SimaRiverReaches.
 */

namespace Drupal\dss_magdalena\DSS\Entity;

use EntityFieldQuery;
use Drupal\dss_magdalena\DSS\Entity\Term\SimaWeapLevel1;

/**
 * Implements Sima River Reach objects.
 *
 * Class SimaRiverReaches.
 *
 * @package Drupal\dss_magdalena\DSS\Entity
 */
class SimaRiverReaches extends EntityNodeAbstract {

  const BUNDLE               = 'river_reaches';
  const FIELD_BRANCH_ID      = 'field_branch_id';
  const FIELD_LEVEL1         = 'field_level_1';
  const FIELD_LEVEL2         = 'field_level_2';
  const FIELD_LEVEL3         = 'field_level_3';
  const FIELD_LEVEL4         = 'field_level_4';
  const FIELD_SPATIALIZATION = 'field_spatialization';

  /**
   * Loads a SimaRiverReaches from a Drupal entity or a Drupal entity ID.
   *
   * @param mixed $river_reach
   *   The nid or loaded object.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaRiverReaches|FALSE
   *   The loaded SimaRiverReaches object if exists, FALSE otherwise.
   */
  public static function load($river_reach) {
    if ($river_reach instanceof SimaRiverReaches) {
      return $river_reach;
    }
    else {
      if ($entity = parent::load($river_reach)) {
        return ($entity->getDrupalBundle() == self::BUNDLE) ? $entity : FALSE;
      }
      return FALSE;
    }
  }

  /**
   * Loads a River Reach given the Branch ID.
   *
   * @param int $branch_id
   *   The Branch ID.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaRiverReaches|FALSE
   *   The loaded SimaRiverReaches object if exists, FALSE otherwise.
   */
  public static function loadByBranchId($branch_id) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', self::BUNDLE)
      ->propertyCondition('status', NODE_PUBLISHED)
      ->fieldCondition(self::FIELD_BRANCH_ID, 'value', $branch_id, '=');
    $result = $query->execute();

    if (isset($result['node'])) {
      $node = reset($result['node']);
      return new static($node->nid);
    }
    return FALSE;
  }

  /**
   * Loads a River Reach given the Full Branch Name.
   *
   * @param string $branch_name
   *   The Full Branch Name.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaRiverReaches|FALSE
   *   The loaded SimaRiverReaches object if exists, FALSE otherwise.
   */
  public static function loadByFullBranchName($branch_name) {
    // Separating branch name parts.
    $branch = explode('\\', $branch_name);
    if (count($branch) < 5) {
      return FALSE;
    }
    $weap_level1 = SimaWeapLevel1::loadByTaxonomyTermName($branch[0]);
    $branches = array(
      'Level 1' => (int) $weap_level1->getId(),
      'Level 2' => $branch[1],
      'Level 3' => $branch[2],
      'Level 4' => $branch[3] . '\\' . $branch[4],
    );

    // Try to find this River Reach.
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', self::BUNDLE)
      ->propertyCondition('status', NODE_PUBLISHED)
      ->fieldCondition(self::FIELD_LEVEL1, 'tid', $branches['Level 1'], '=')
      ->fieldCondition(self::FIELD_LEVEL2, 'value', $branches['Level 2'], '=')
      ->fieldCondition(self::FIELD_LEVEL3, 'value', $branches['Level 3'], '=')
      ->fieldCondition(self::FIELD_LEVEL4, 'value', $branches['Level 4'], '=');
    $result = $query->execute();

    if (isset($result['node'])) {
      $node = reset($result['node']);
      return new static($node->nid);
    }
    return FALSE;
  }

  /**
   * Returns a new SimaRiverReaches node.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaRiverReaches
   *   The loaded SimaRiverReaches object if exists, FALSE otherwise.
   */
  static public function newRiverReach() {
    global $user;
    // Create the new SimaRiverReaches Node.
    return self::newEntity('node', self::BUNDLE, $user->uid);
  }

  /**
   * Returns a River Reach node.
   *
   * @param array $row
   *   The Masterfile record.
   * @param string $geojson_field
   *   The GeoJson field.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaRiverReaches
   *   The loaded SimaRiverReaches object.
   */
  static public function fromMasterfileRecord($row, $geojson_field = NULL) {
    $river_reach = self::newRiverReach();
    $river_reach->set(self::FIELD_BRANCH_ID, $row['BranchID']);

    // We need a special treatment to 'Level 1' because it is a taxonomy term
    // reference.
    if ($weap_level1 = SimaWeapLevel1::loadByTaxonomyTermName($row['Level 1'])) {
      $river_reach->set(self::FIELD_LEVEL1, $weap_level1->getDrupalEntity()->value());
    }
    $river_reach->set(self::FIELD_LEVEL2, $row['Level 2']);
    $river_reach->set(self::FIELD_LEVEL3, $row['Level 3']);
    $river_reach->set(self::FIELD_LEVEL4, $row['Level 4...']);

    // Setting up the GeoJson Field.
    if (!empty($geojson_field)) {
      $river_reach->set(self::FIELD_SPATIALIZATION, $geojson_field);
    }

    // Setting title the same as level 4.
    $river_reach->set('title', $row['Level 4...']);
    return $river_reach;
  }

}
