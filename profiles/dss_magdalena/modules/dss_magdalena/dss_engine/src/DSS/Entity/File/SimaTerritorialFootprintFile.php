<?php
/**
 * @file
 * Territorial Footprint files.
 */

namespace Drupal\dss_magdalena\DSS\Entity\File;

use Drupal\dss_magdalena\DSS\Utils\SimaCsvReaderTrait;

/**
 * Class SimaTerritorialFootprintFile.
 *
 * @package Drupal\dss_magdalena\DSS\Entity\File
 */
class SimaTerritorialFootprintFile extends EntityFileAbstract {

  use SimaCsvReaderTrait;

  const BUNDLE                         = 'file';
  const BUNDLE_UNDEFINED               = 'undefined';
  const VAR_TERRITORIAL_FOOTPRINT_FILE = 'SimaTerritorialFoottprint_fid';

  /**
   * Defines the header for the imported file.
   *
   * @var array
   */
  protected $requiredHeaders = [
    'Project_ID',
    'Branch',
    'Branch2',
    'FACTOR_ELEVACION',
    'Min_elev_masl',
    'DamHeight_m',
    'Elevacion',
    'Area_m2',
    'Volumen_m3',
    'Q_avg_m3s',
    'Q95_avg_m3s',
    'COB_agricolas_cultivo_m2',
    'COB_agricolas_pastos_m2',
    'Area_Compensacion_m2',
    'Microfocalizacion_m2',
    'Paramos_SIAC_m2',
    'Parques_Naturales_Regionales_m2',
    'Titulos_Mineros_m2',
    'bosque_seco_m2',
    'comunidades_negras_m2',
    'humedales_SIAC_m2',
    'parques_nacionales_m2',
    'poblacion_hab',
    'reserva_forestal_protectora_nacional_m2',
    'reserva_forestal_protectora_regional_m2',
    'reservas_Ley2da_m2',
    'resguardos_indigenas_m2',
    'A_ECONOMICA',
    'A_AMBIENTAL',
    'A_SOCIAL',
    'POBLACION',
    'A_COMPENSACION',
  ];

  /**
   * Directories that should be initialized.
   *
   * @var array
   */
  protected $directories = [
    self::DATA_DIR,
  ];

  /**
   * Loads a  from a Drupal entity or a Drupal entity ID.
   *
   * @param mixed $file
   *   The file fid or loaded object.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\File\SimaTerritorialFootpintFile|FALSE
   *   The loaded SimaTerritorialFootprintFile object if exists,
   *   FALSE otherwise.
   */
  public static function load($file) {
    if ($file instanceof SimaTerritorialFootprintFile) {
      return $file;
    }
    else {
      $entity = parent::load($file);

      // We are also considering BUNDLE_UNDEFINED because files might have this
      // value set if it is not known its bundle.
      return (in_array($entity->getDrupalBundle(), [self::BUNDLE, self::BUNDLE_UNDEFINED])) ? $entity : FALSE;
    }
  }

  /**
   * Loads a SimaTerritorialFootprintFile from a Drupal variable.
   *
   * @return bool|\Drupal\dss_magdalena\DSS\Entity\File\SimaTerritorialFootprintFile|FALSE
   *   The loaded SimaTerritorialFootprintFile object if succeeds,
   *   FALSE  otherwise.
   */
  public static function loadFromSystem($auto_detect_line_endings = 1) {
    $fid = variable_get(self::VAR_TERRITORIAL_FOOTPRINT_FILE, FALSE);
    if ($fid) {
      $file = static::load($fid);
      $file->setAutodetectLineEndings($auto_detect_line_endings);
      return $file;
    }
    else {
      $file = new static(NULL);
      $file->setAutodetectLineEndings($auto_detect_line_endings);
      return $file->initializeDirectories();
    }
  }

  /**
   * Saves the current.
   *
   * @return \EntityDrupalWrapper
   *   The Drupal Entity wrapper.
   */
  public function save() {
    variable_set(self::VAR_TERRITORIAL_FOOTPRINT_FILE, $this->getId());
    return parent::save();
  }

  /**
   * Save from Temporal File.
   *
   * @param object $file
   *   The Temporal file.
   *
   * @return bool|\Drupal\dss_magdalena\DSS\Entity\File\SimaTerritorialFootprintFile|FALSE
   *   The loaded SimaTerritorialFootprintFile object if succeeds, FALSE
   *   otherwise.
   */
  public static function saveFromTemporalFile($file) {
    // Move the temporary file into the final location.
    if ($file = file_move($file, SimaTerritorialFootprintFile::DATA_DIR, FILE_EXISTS_REPLACE)) {
      $file->status = FILE_STATUS_PERMANENT;
      $file = file_save($file);

      // Now that the file is properly saved. Notify the class to save it.
      variable_set(self::VAR_TERRITORIAL_FOOTPRINT_FILE, $file->fid);
      return new static($file);
    }
    return FALSE;
  }

  /**
   * Returns the file type.
   *
   * @return string
   *   The File Type.
   */
  public function getFileType() {
    return $this->get('type');
  }

  /**
   * Verifies that ALL the required headers are contained in this CSV File.
   */
  public function verifyRequiredHeaders() {
    $file_path = drupal_realpath($this->getUri());
    $headers = $this->extractHeader($file_path);
    foreach ($this->requiredHeaders as $h) {
      if (!in_array($h, $headers)) {
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Obtains the Sima Territorial Footprint File.
   */
  public function getData() {
    $file_path = drupal_realpath($this->getUri());

    // Extract the header information.
    $headers = $this->extractHeader($file_path);

    // Converting headers to machine names.
    foreach ($headers as $key => $header) {
      $headers[$key] = preg_replace('@[^a-z0-9-]+@', '_', strtolower($header));
    }
    $results = $this->extractRows('filterByNoFilter', $file_path, $headers);
    return iterator_to_array($results, FALSE);
  }

}
