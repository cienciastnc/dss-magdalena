<?php
/**
 * @file
 * Models a Sima Integral Plan.
 */

namespace Drupal\dss_magdalena\DSS\Entity;

/**
 * Models a Sima Integral Plan.
 *
 * Class SimaIntegralPlan.
 *
 * @package Drupal\dss_magdalena\DSS\Entity
 */
class SimaIntegralPlan extends EntityNodeAbstract {

  const BUNDLE                                 = 'integral_plan';

  const FIELD_PRODUCTIVE_LAND_USE_ALTERNATIVES = 'field_prod_landuse_alternatives';
  const FIELD_HYDROPOWER_ALTERNATIVES          = 'field_hydropower_alternatives';
  const FIELD_WETLAND_MANAGEMENT_ALTERNATIVES  = 'field_connectivity_alternatives';

  /**
   * Loads a SimaIntegralPlan from a Drupal entity or a Drupal entity ID.
   *
   * @param mixed $integral_plan
   *   The nid or loaded object.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaIntegralPlan|FALSE
   *   The loaded SimaIntegralPlan object if exists, FALSE otherwise.
   */
  public static function load($integral_plan) {
    if ($integral_plan instanceof SimaIntegralPlan) {
      return $integral_plan;
    }
    else {
      if ($entity = parent::load($integral_plan)) {
        return ($entity->getDrupalBundle() == self::BUNDLE) ? $entity : FALSE;
      }
      return FALSE;
    }
  }

  /**
   * Returns a new Integral Plan.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaIntegralPlan
   *   The loaded SimaIntegralPlan.
   */
  static public function newIntegralPlan() {
    global $user;
    // Create the new Integral Plan Node.
    return self::newEntity('node', self::BUNDLE, $user->uid);
  }

  /**
   * Obtains the Productive Land Use Alternative.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaAlternative|FALSE
   *   The loaded SimaAlternative object if it exists, FALSE otherwise.
   */
  public function getProductiveLandUseAlternative() {
    if ($alternative = $this->get(self::FIELD_PRODUCTIVE_LAND_USE_ALTERNATIVES)) {
      return SimaAlternative::load($alternative);
    }
    return FALSE;
  }

  /**
   * Obtains the Hydropower Alternative.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaAlternative|FALSE
   *   The loaded SimaAlternative object if it exists, FALSE otherwise.
   */
  public function getHydropowerAlternative() {
    if ($alternative = $this->get(self::FIELD_HYDROPOWER_ALTERNATIVES)) {
      return SimaAlternative::load($alternative);
    }
    return FALSE;
  }

  /**
   * Obtains the Wetland Management Alternative.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaAlternative|FALSE
   *   The loaded SimaAlternative object if it exists, FALSE otherwise.
   */
  public function getWetlandManagementAlternative() {
    if ($alternative = $this->get(self::FIELD_WETLAND_MANAGEMENT_ALTERNATIVES)) {
      return SimaAlternative::load($alternative);
    }
    return FALSE;
  }

  /**
   * Provides a list of Integral Plans.
   *
   * @param int $limit
   *   The number of entities to list.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaIntegralPlan[]
   *   An array of loaded Integral Plans.
   */
  public static function listEntities($limit = 500) {
    $controller = new SimaController();
    return $controller->listEntities(self::BUNDLE, $limit);
  }

}
