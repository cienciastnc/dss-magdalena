<?php
/**
 * @file
 * Models Land Use Percentages for Productive Land Use Alternatives.
 */

namespace Drupal\dss_magdalena\DSS\Entity\Multifield;

use Drupal\dss_magdalena\DSS\Entity\Term\SimaLandUseType;
use Drupal\dss_magdalena\DSS\Entity\Multifield\SimaSimpleLandUsePercentages;

/**
 * Class SimaLandUsePercentages.
 *
 * @package Drupal\dss_magdalena\DSS\Entity\Multifield
 */
class SimaLandUsePercentages extends EntityMultifieldAbstract {

  const BUNDLE                 = 'field_land_use';

  const FIELD_BRANCH_ID        = 'field_branch_id';
  const FIELD_LAND_USE_TYPE    = 'field_land_use_ref';
  const FIELD_PERCENTAGE_SHARE = 'field_percentage_value';

  /**
   * Loads a SimaLandUsePercentages from a Drupal entity or a Drupal entity ID.
   *
   * @param mixed $land_use_percentage
   *   The multifield id or loaded object.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Multifield\SimaLandUsePercentages|FALSE
   *   A loaded SimaLandUsePercentages object if exists, FALSE otherwise.
   */
  public static function load($land_use_percentage) {
    if ($land_use_percentage instanceof SimaLandUsePercentages) {
      return $land_use_percentage;
    }
    else {
      if ($entity = parent::load($land_use_percentage)) {
        return ($entity->getDrupalBundle() == self::BUNDLE) ? $entity : FALSE;
      }
      return FALSE;
    }
  }

  /**
   * Obtains the Branch ID.
   *
   * @return int
   *   The branch ID.
   */
  public function getBranchId() {
    return intval($this->get(self::FIELD_BRANCH_ID));
  }

  /**
   * Sets the Branch ID.
   *
   * @param int $branch_id
   *   The Branch ID.
   */
  public function setBranchId($branch_id) {
    $branch_id = intval($branch_id);
    $this->set(self::FIELD_BRANCH_ID, $branch_id);
  }

  /**
   * Returns the Land Use Type Taxonomy Term.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Term\SimaLandUseType|FALSE
   *   The SimaLandUseType object loaded if found, FALSE otherwise.
   */
  public function getLandUseType() {
    $land_use = $this->get(self::FIELD_LAND_USE_TYPE);
    return SimaLandUseType::load($land_use);
  }

  /**
   * Sets the Land Use Type.
   *
   * @param string $name
   *   The Land Use Type name.
   * @param bool $create_term
   *   TRUE if we want to create this term, FALSE otherwise.
   */
  public function setLandUseType($name, $create_term = FALSE) {
    if ($land_use_type = SimaLandUseType::loadByTaxonomyTermName($name)) {
      $this->set(self::FIELD_LAND_USE_TYPE, $land_use_type->getDrupalEntity()->value());
    }
    elseif ($create_term) {
      $land_use_type = SimaLandUseType::newEntity($name);
      $land_use_type->save();
      $this->set(self::FIELD_LAND_USE_TYPE, $land_use_type->getDrupalEntity()->value());
    }
  }

  /**
   * Gets the Percentage Share of this Land Type.
   *
   * @return float
   *   The Percentage of shared area.
   */
  public function getPercentage() {
    return floatval($this->get(self::FIELD_PERCENTAGE_SHARE));
  }

  /**
   * Sets the Percentage Share for this Land Type.
   *
   * @param string $percentage
   *   The percentage. Should be a decimal value.
   */
  public function setPercentage($percentage) {
    $percentage = floatval($percentage);
    $this->set(self::FIELD_PERCENTAGE_SHARE, number_format($percentage, 2, '.', ''));
  }

  /**
   * Converts a SimaLandUsePercentages object to SimaSimpleLandUsePercentages.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Multifield\SimaSimpleLandUsePercentages
   *   The converted and loaded SimaSimpleLandUsePercentages object.
   */
  public function toSimpleLandUsePercentage() {
    $land_type = $this->getLandUseType();
    $percentage = $this->getPercentage();
    /** @var \Drupal\dss_magdalena\DSS\Entity\Multifield\SimaSimpleLandUsePercentages $simple_land_use */
    $simple_land_use = SimaSimpleLandUsePercentages::newMultifieldEntity();
    $simple_land_use->setLandUseType($land_type->getName());
    $simple_land_use->setPercentage($percentage);
    return $simple_land_use;
  }

}
