<?php
/**
 * @file
 * Models The taxonomy term Model Family.
 */

namespace Drupal\dss_magdalena\DSS\Entity\Term;
use Drupal\dss_magdalena\DSS\Entity\SimaModel;

/**
 * Implements a Model Family taxonomy term.
 *
 * Class SimaModelFamily.
 *
 * @package Drupal\dss_magdalena\DSS\Entity\Term
 */
class SimaModelFamily extends EntityTermAbstract {

  const BUNDLE                         = 'model_family';

  const NAME_CONCEPTUAL                = 'Conceptual';
  const NAME_CONCEPTUAL_WEAP           = 'Conceptual WEAP';
  const NAME_CONCEPTUAL_FRAGMENTATION  = 'Conceptual Fragmentacion';
  const NAME_CONCEPTUAL_DORH           = 'Conceptual DOR-H';
  const NAME_CONCEPTUAL_DORW           = 'Conceptual DOR-W';
  const NAME_EXPERT                    = 'De Experto';
  const NAME_ELOHA                     = 'De Experto ELOHA';
  const NAME_PHYSICAL_BASED            = 'Fisicamente Basados';
  const NAME_HUELLA_TNC                = 'Fisicamente Basado Huella TNC';
  const NAME_EMPIRIC_ARMAX             = 'Empiricos ARMAX';
  const NAME_EMPIRIC_NEURAL            = 'Empirico Red Neural';

  /**
   * Loads a SimaModelFamily from a Drupal entity or a Drupal entity ID.
   *
   * @param mixed $model_family
   *   The taonomy_term tid or loaded object.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Term\SimaModelFamily|FALSE
   *   The loaded SimaModelFamily object if exists, FALSE otherwise.
   */
  public static function load($model_family) {
    if ($model_family instanceof SimaModelFamily) {
      return $model_family;
    }
    else {
      if ($entity = parent::load($model_family)) {
        return ($entity->getDrupalBundle() == self::BUNDLE) ? $entity : FALSE;
      }
      return FALSE;
    }
  }

  /**
   * Loads the SimaModelFamily Term given the Model Machine Name.
   *
   * @param string $model_machine_name
   *   The model machine name.
   *
   * @return bool|\Drupal\dss_magdalena\DSS\Entity\Term\EntityTermAbstract
   *   The loaded Taxonomy Term.
   */
  public static function loadByModelMachineName($model_machine_name) {
    switch ($model_machine_name) {
      case SimaModel::MODEL_ELOHA:
        return static::loadByTaxonomyTermName(self::NAME_ELOHA);

      case SimaModel::MODEL_FRAGMENTATION:
        return static::loadByTaxonomyTermName(self::NAME_CONCEPTUAL_FRAGMENTATION);

      case SimaModel::MODEL_DOR_H:
        return static::loadByTaxonomyTermName(self::NAME_CONCEPTUAL_DORH);

      case SimaModel::MODEL_DOR_W:
        return static::loadByTaxonomyTermName(self::NAME_CONCEPTUAL_DORW);

      case SimaModel::MODEL_TERRITORIAL_FOOTPRINT:
        return static::loadByTaxonomyTermName(self::NAME_HUELLA_TNC);

      case SimaModel::MODEL_SEDIMENTS:
        return static::loadByTaxonomyTermName(self::NAME_CONCEPTUAL);

      case SimaModel::MODEL_WEAP:
      default:
        return static::loadByTaxonomyTermName(self::NAME_CONCEPTUAL_WEAP);

    }
  }

}
