<?php
/**
 * @file
 * Implements Sima Hydropower Development Alternative.
 */

namespace Drupal\dss_magdalena\DSS\Entity\Alternatives;

use Drupal\dss_magdalena\DSS\Entity\Ficha\SimaFichaHydropowerPlantDam;
use Drupal\dss_magdalena\DSS\Entity\Ficha\SimaFichaRorHydropowerPlant;
use Drupal\dss_magdalena\DSS\Entity\SimaAlternative;
use Drupal\dss_magdalena\DSS\Entity\SimaCaseStudy;
use Drupal\dss_magdalena\DSS\Entity\SimaDefaultCaseStudy;
use Drupal\dss_magdalena\DSS\Entity\SimaResource;
use Drupal\dss_magdalena\DSS\Queue\SimaImportQueueItem;
use Drupal\dss_magdalena\DSS\SimaWeapIndex;
use Drupal\dss_magdalena\DSS\Utils\SimaCsvReaderTrait;
use Drupal\dss_magdalena\DSS\Utils\SimaCsvWriterTrait;
use Drupal\dss_magdalena\DSS\Utils\SimaDirectoriesTrait;

/**
 * Class SimaHydropowerAlternative.
 *
 * @package Drupal\dss_magdalena\DSS\Entity\Alternatives
 */
class SimaHydropowerAlternative extends SimaAlternative implements SimaAlternativeInterface {

  use SimaCsvReaderTrait;
  use SimaCsvWriterTrait;
  use SimaDirectoriesTrait;

  const DEFAULT_BASELINE_RESERVOIR_PLANTS = 'dss_engine_baseline_reservoirs_plants';
  const DEFAULT_BASELINE_ROR_PLANTS       = 'dss_engine_baseline_ror_plants';

  /**
   * {@inheritdoc}
   */
  public function createResourceForDataset($cid, $dataset_machine_name, $process_datastore = FALSE) {
    // We are going to load the Resource CSV for the Extreme Case (Default Case)
    // and use it to delete the projects that we are not concerned about,
    // because they have not been selected.
    $case_ref = SimaDefaultCaseStudy::loadExtreme();
    if ($resource_ref = SimaResource::loadByCaseStudyAndDataset($case_ref->getId(), $dataset_machine_name)) {
      if ($resource_file = $resource_ref->getResourceCsvFile()) {
        $project_type = $this->getProjectType($dataset_machine_name);
        $projects = $this->getFichaProjects();

        $destination = "temporary://{$cid}/weap/input/{$dataset_machine_name}.csv";
        if ($this->createPathToFile($destination)) {
          if ($csvfile = $this->prepareResourceCsvFile($projects, $project_type, $resource_file, $destination)) {
            // Resource File has been saved. Now attach it to the resource.
            $case = SimaCaseStudy::load($cid);
            $case_outputs = $case->getCaseOutputs();
            $values = [
              'cid' => $cid,
              'case_output' => $case_outputs->getId(),
              'file_uri' => $csvfile,
              'filename'  => basename($csvfile),
              'dss_machine_name' => $dataset_machine_name,
            ];
            $item = new SimaImportQueueItem($values);

            /** @var \Drupal\dss_magdalena\DSS\Entity\SimaResource $resource */
            if ($resource = SimaResource::newFullyFormedResource($item)) {
              $resource->save();

              // Do we want to process the datastore?
              if ($process_datastore) {
                $resource->processDataStore();
              }
              return TRUE;
            }
          }
        }
      }
    }
  }

  /**
   * Sets the default "baseline" Reservoirs for the Basin.
   *
   * These are all the reservoirs that are active at the starting year of
   * simulation. Those are the current active reservoirs.
   *
   * @param array $reservoirs
   *   An array of HYDROPOWER_PROJECTS IDs.
   */
  public static function setBaselineReservoirPlants($reservoirs = array()) {
    variable_set(self::DEFAULT_BASELINE_RESERVOIR_PLANTS, $reservoirs);
  }

  /**
   * Obtains the default "baseline" Reservoirs for the Basin.
   *
   * @return array
   *   An array of HYDROPOWER_PROJECTS IDs.
   */
  public function getBaselineReservoirPlants() {
    return variable_get(self::DEFAULT_BASELINE_RESERVOIR_PLANTS, array());
  }

  /**
   * Sets the default "baseline" ROR plants for the Basin.
   *
   * These are all the ROR plants that are active at the starting year of
   * simulation. Those are the current active ROR plants.
   *
   * @param array $rors
   *   An array of ROR_HYDROPOWER_PROJECTS IDs.
   */
  public static function setBaselineRorPlants($rors = array()) {
    variable_set(self::DEFAULT_BASELINE_ROR_PLANTS, $rors);
  }

  /**
   * Obtains the default "baseline" ROR plants for the Basin.
   *
   * @return array
   *   An array of ROR_HYDROPOWER_PROJECTS IDs.
   */
  public function getBaselineRorPlants() {
    return variable_get(self::DEFAULT_BASELINE_ROR_PLANTS, array());
  }

  /**
   * Saves A Resource CSV File and leaves it ready to be attached to a resource.
   *
   * @param \Drupal\dss_magdalena\DSS\Entity\Ficha\SimaFichaInterface[] $projects
   *   An array of all projects for this alternative.
   * @param string $project_type
   *   The project type.
   * @param object $resource_file
   *   The reference file object (for the extreme case study).
   * @param string $destination
   *   A string containing the destination URI. This must be a stream wrapper
   *   URI. If no value is provided, a randomized name will be generated and
   *   the file will be saved using Drupal's default files scheme, usually
   *   "public://".
   *
   * @return bool|\string
   *   The path to saved resource file, FALSE otherwise.
   */
  protected function prepareResourceCsvFile($projects, $project_type, $resource_file, $destination) {
    $csv_realpath = drupal_realpath($resource_file->uri);
    $proj_names = [];
    foreach ($projects as $project) {
      if ($project->getDrupalBundle() == $project_type) {
        $proj_names[] = $project->getFullBranchName();
      }
    }
    $csv_reader = $this->extractRows('filterByNoFilter', $csv_realpath);
    $data = iterator_to_array($csv_reader);

    // Adding WEAP Index.
    /** @var \Drupal\dss_magdalena\DSS\SimaWeapIndex $weap_index */
    $weap_index = SimaWeapIndex::loadAllRecords();
    $data_combined = $weap_index->combineWithWeap($data, [
      'level1',
      'level2',
      'level3',
      'level4',
    ]);
    $output_data = [];
    foreach ($data_combined as $key => $value) {
      if (!empty($value['level4'])) {
        $branch = $value['level1'] . '\\' . $value['level2'] . '\\' . $value['level3'] . '\\' . $value['level4'];
      }
      else {
        if (!empty($value['level3'])) {
          $branch = $value['level1'] . '\\' . $value['level2'] . '\\' . $value['level3'];
        }
        else {
          $branch = $value['level1'] . '\\' . $value['level2'];
        }
      }
      if (!in_array($branch, $proj_names)) {
        $value['Value'] = 0;
      }
      $output_data[$key] = array_splice($value, 0, 5);
    }

    array_unshift($output_data, $this->resourceHeader);

    return $this->saveUnManagedCsvFile($output_data, $destination);
  }

  /**
   * Returns the Project type according to the dataset machine name.
   *
   * @param string $dataset_machine_name
   *   The Dataset machine name.
   *
   * @return null|string
   *   The project type.
   */
  protected function getProjectType($dataset_machine_name) {
    $project_type = NULL;
    $mappings = $this->getMappingToDatasets();
    $dataset_machine_names_hydam = array_keys($mappings[SimaFichaHydropowerPlantDam::BUNDLE]);
    $dataset_machine_names_hyror = array_keys($mappings[SimaFichaRorHydropowerPlant::BUNDLE]);

    if (in_array($dataset_machine_name, $dataset_machine_names_hydam)) {
      $project_type = SimaFichaHydropowerPlantDam::BUNDLE;
    }
    elseif (in_array($dataset_machine_name, $dataset_machine_names_hyror)) {
      $project_type = SimaFichaRorHydropowerPlant::BUNDLE;
    }
    return $project_type;
  }

}
