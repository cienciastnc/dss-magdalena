<?php
/**
 * @file
 * Models a Sima Dataset or DSS Variable.
 */

namespace Drupal\dss_magdalena\DSS\Entity;

use Drupal\dss_magdalena\DSS\Entity\Term\SimaTemporality;
use Drupal\dss_magdalena\DSS\Entity\Term\SimaVariableStatus;
use EntityFieldQuery;

/**
 * Models a Sima Dataset (Variable).
 *
 * Class SimaDataset.
 *
 * @package Drupal\dss_magdalena\DSS\Entity
 */
class SimaDataset extends EntityNodeAbstract {

  const BUNDLE               = 'dataset';
  const UUID                 = 'uuid';
  const VARIABLE_STATUS      = 'field_variable_status';
  const MACHINE_NAME         = 'field_machine_name';
  const DEFAULT_VALUE        = 'field_default_value';
  const UNIT_OF_MEASURE      = 'field_unit_of_measure';
  const RANGE                = 'field_value_range';
  const TEMPORALITY          = 'field_temporality';
  const TITLE                = 'title';
  const GEOMETRY             = 'field_geometry';
  const DIMENSIONS           = 'field_dimensions';
  const WEAP_LEVEL           = 'field_weap_level';
  const WEAP_VARIABLE_NAME   = 'field_weap_name';
  const CONTEXT              = 'field_context';
  const DIMENSION_LABELS     = 'field_dimensions_labels';
  const VARIABLE_CATEGORY    = 'field_variable_category';
  const OBJECT_TYPE          = 'field_weap_object_type';
  const SUBSYSTEM            = 'field_subsystem';
  const NODE_CATEGORY        = 'field_node_category';
  const SPATIAL_SCALE        = 'field_appropriate_spatial_scale';
  const NUMERIC_SCALE        = 'field_numeric_scale';
  const DPSIR                = 'field_dpsir';
  const ASSOC_THEMATIC_TREE  = 'field_associated_thematic_tree';

  /**
   * Loads a Sima Dataset from a Drupal entity or a Drupal entity ID.
   *
   * @param mixed $dataset
   *   The nid or loaded object.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaDataset|FALSE
   *   The loaded SimaDataset object if exists, FALSE otherwise.
   */
  public static function load($dataset) {
    if ($dataset instanceof SimaDataset) {
      return $dataset;
    }
    else {
      if ($entity = parent::load($dataset)) {
        return ($entity->getDrupalBundle() == self::BUNDLE) ? $entity : FALSE;
      }
      return FALSE;
    }
  }

  /**
   * Loads a dataset given the machine_name.
   *
   * @param string $dataset_machine_name
   *   The Dataset machine_name.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaDataset|FALSE
   *   The loaded SimaDataset object if exists, FALSE otherwise.
   */
  public static function loadByMachineName($dataset_machine_name) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', self::BUNDLE)
      ->propertyCondition('status', NODE_PUBLISHED)
      ->fieldCondition(self::MACHINE_NAME, 'value', $dataset_machine_name, '=');
    $result = $query->execute();

    if (isset($result['node'])) {
      // Here we are only taking the first result, because machine names are
      // uniques.
      $node = reset($result['node']);
      if (!empty($node)) {
        return new static($node->nid);
      }
    }
    return FALSE;
  }

  /**
   * Loads a list of Datasets given the machine names.
   *
   * @param array $dataset_machine_names
   *   An array of dataset machine_names.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaDataset[]|FALSE
   *   An array of loaded SimaDataset objects.
   */
  public static function listByMachineNames($dataset_machine_names) {
    $datasets = [];
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', self::BUNDLE)
      ->propertyCondition('status', NODE_PUBLISHED)
      ->fieldCondition(self::MACHINE_NAME, 'value', $dataset_machine_names);
    $result = $query->execute();

    if (!empty($result['node'])) {
      foreach ($result['node'] as $node) {
        $dataset = static::load($node->nid);
        $datasets[$node->nid] = $dataset;
      }
    }
    return $datasets;
  }

  /**
   * Gets the Variable Status.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Term\SimaVariableStatus
   *   The variable status taxonomy term.
   */
  public function getVariableStatus() {
    $variable_status = $this->get(self::VARIABLE_STATUS);
    return SimaVariableStatus::load($variable_status);
  }
/**
   * Get UUID of loaded dataset
   *
   * @return String full name
   */
  public function getUuid()
  {
    return $this->get(self::UUID);
  }
  /**
   * Returns the Modeling Variable.
   *
   * @return string|NULL
   *   The modeling variable if set, NULL otherwise.
   */
  public function getModelingVariable() {
    if ($variable = $this->getVariableStatus()) {
      return $variable->getName();
    }
    return NULL;
  }

  /**
   * Returns whether the current dataset is a Modeling Input Variable.
   *
   * @return bool
   *   TRUE if it is an INPUT variable, FALSE otherwise.
   */
  public function isModelingInput() {
    if ($variable_status = $this->getVariableStatus()) {
      return $variable_status->isModelingInput();
    }
    return FALSE;
  }

  /**
   * Returns whether the current dataset is a Modeling Output Variable.
   *
   * @return bool
   *   TRUE if it is an OUTPUT variable, FALSE otherwise.
   */
  public function isModelingOutput() {
    if ($variable_status = $this->getVariableStatus()) {
      return $variable_status->isModelingOutput();
    }
    return FALSE;
  }

  /**
   * Gets the Return Value.
   *
   * @return mixed
   *   The default value.
   */
  public function getDefaultValue() {
    return $this->get(self::DEFAULT_VALUE);
  }

  /**
   * Returns the Machine Name.
   *
   * @return string
   *   The Machine Name.
   */
  public function getMachineName() {
    return $this->get(self::MACHINE_NAME);
  }

  /**
   * Returns the Unit of Measure.
   *
   * @return string
   *   The Unit of Measure.
   */
  public function getUnitOfMeasure() {
    if ($unit_measure = $this->get(self::UNIT_OF_MEASURE)) {
      return $unit_measure->name;
    }
    return NULL;
  }

  /**
   * Returns the Value Range.
   *
   * @return string
   *   The Value Range.
   */
  public function getValueRange() {
    if ($range = $this->get(self::RANGE)) {
      return $range->name;
    }
    return NULL;
  }

  /**
   * Returns the Weap Variable Name.
   *
   * @return string
   *   The WEAP Variable Name.
   */
  public function getWeapVariableName() {
    return $this->get(self::WEAP_VARIABLE_NAME);
  }

  /**
   * Returns the Title.
   *
   * @return string
   *   The Title.
   */
  public function getTitle() {
    if ($title_field = $this->get(self::TITLE)) {
      return $title_field;
    }
    return NULL;
  }

  /**
   * Returns the Temporality.
   *
   * @return string
   *   The Temporality.
   */
  public function getTemporality() {
    if ($temporality_field = $this->get(self::TEMPORALITY)) {
      return $temporality_field->name;
    }
    return NULL;
  }

  /**
   * Returns the Temporality.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Term\SimaTemporality|FALSE|null
   *   The loaded SimaTemporality object, FALSE otherwise.
   */
  public function getTemporalityEntity() {
    if ($temporality = $this->get(self::TEMPORALITY)) {
      return SimaTemporality::load($temporality);
    }
    return FALSE;
  }

  /**
   * Returns the Geometry.
   *
   * @return string
   *   The Geometry.
   */
  public function getGeometry() {
    if ($geometry_field = $this->get(self::GEOMETRY)) {
      return $geometry_field->name;
    }
    return NULL;
  }

  /**
   * Returns the Dimension.
   *
   * @return string
   *   The Geometry.
   */
  public function getDimensions() {
    if ($dimensions_field = $this->get(self::DIMENSIONS)) {
      return $dimensions_field->name;
    }
    return NULL;
  }

  /**
   * Returns list of Context layer machine names.
   *
   * @return array
   *   An array of Context Names from dataset.
   */
  public function getContextMachineNames() {
    // Default return data.
    $data = [];

    // Getting context layer obj.
    $context_obj_array = $this->get(self::CONTEXT);
    if (!isset($context_obj_array)) {
      return $data;
    }

    // Getting machine name from context layers.
    foreach ($context_obj_array as $obj) {
      if ($context_obj = SimaLayer::load($obj)) {
        $context_name = $context_obj->getTitle();
        $context_machine_name = $context_obj->getMachineName();
        $data[$context_name] = $context_machine_name;
      }
    }

    // Return results.
    return $data;
  }

  /**
   * Returns the Weap Level.
   *
   * @return string
   *   The Geometry.
   */
  public function getWeapLevel() {
    if ($weap_level_field = $this->get(self::WEAP_LEVEL)) {
      return $weap_level_field->name;
    }
    return NULL;
  }

  /**
   * Returns the Weap Level.
   *
   * @return array
   *   The Geometry.
   */
  public function getDimensionLabels() {
    if ($weap_level_field = $this->get(self::DIMENSION_LABELS)) {
      return $weap_level_field;
    }
    return array();
  }

  /**
   * Returns the Weap Level.
   *
   * @return string
   *   The Geometry.
   */
  public function getVariableCategory() {
    if ($o_field = $this->get(self::VARIABLE_CATEGORY)) {
      return $o_field->name;
    }
    return NULL;
  }

  /**
   * Returns the Weap Level.
   *
   * @return string
   *   The Geometry.
   */
  public function getObjectType() {
    if ($o_field = $this->get(self::OBJECT_TYPE)) {
      return $o_field->name;
    }
    return NULL;
  }

  /**
   * Returns the Weap Level.
   *
   * @return string
   *   The Geometry.
   */
  public function getSubsystem() {
    if ($o_field = $this->get(self::SUBSYSTEM)) {
      return $o_field->name;
    }
    return NULL;
  }

  /**
   * Returns the Weap Level.
   *
   * @return string
   *   The Geometry.
   */
  public function getNodeCategory() {
    if ($o_field = $this->get(self::NODE_CATEGORY)) {
      return $o_field->name;
    }
    return NULL;
  }

  /**
   * Returns the Weap Level.
   *
   * @return string
   *   The Geometry.
   */
  public function getSpatialScale() {
    if ($o_field = $this->get(self::SPATIAL_SCALE)) {
      return $o_field->name;
    }
    return NULL;
  }

  /**
   * Returns the Weap Level.
   *
   * @return string
   *   The Geometry.
   */
  public function getNumericScale() {
    if ($o_field = $this->get(self::NUMERIC_SCALE)) {
      return $o_field->name;
    }
    return NULL;
  }

  /**
   * Returns the Weap Level.
   *
   * @return string
   *   The Geometry.
   */
  public function getDpsir() {
    if ($o_field = $this->get(self::DPSIR)) {
      return $o_field->name;
    }
    return NULL;
  }

  /**
   * Returns the Weap Level.
   *
   * @return string
   *   The Geometry.
   */
  public function getAssociatedThematicTree() {
    if ($o_field = $this->get(self::ASSOC_THEMATIC_TREE)) {
      return $o_field->name;
    }
    return NULL;
  }

}
