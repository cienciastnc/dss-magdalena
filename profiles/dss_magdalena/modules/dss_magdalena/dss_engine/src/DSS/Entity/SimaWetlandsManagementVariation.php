<?php
/**
 * @file
 * Models a Sima Wetlands Management Project Variation.
 */

namespace Drupal\dss_magdalena\DSS\Entity;

use Drupal\dss_magdalena\DSS\Entity\Ficha\SimaFichaWetlandsManagement;
use EntityFieldQuery;

/**
 * Models a Sima Wetlands Management Project Variation.
 *
 * Class SimaWetlandsManagementVariation.
 *
 * @package Drupal\dss_magdalena\DSS\Entity
 */
class SimaWetlandsManagementVariation extends EntityNodeAbstract {

  const BUNDLE = 'wetlands_management_variations';

  const FIELD_WETLANDS_MANAGEMENT_PROJ_REF = 'field_wet_mgmt_project_ref';
  const FIELD_RIVER_FLOODING_THRESHOLD     = 'field_river_flooding_threshold_v';
  const FIELD_FIVER_FLOODING_FRACTION      = 'field_river_flooding_fraction_v';
  const FIELD_FLOOD_RETURN_FRACTION        = 'field_flood_return_fraction_v';

  /**
   * Loads a SimaProject from a Drupal entity or a Drupal entity ID.
   *
   * @param mixed $variation
   *   The nid or loaded object.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaWetlandsManagementVariation|FALSE
   *   The loaded SimaWetlandsManagementVariation if exists, FALSE otherwise.
   */
  public static function load($variation) {
    if ($variation instanceof SimaWetlandsManagementVariation) {
      return $variation;
    }
    else {
      if ($entity = parent::load($variation)) {
        return ($entity->getDrupalBundle() == self::BUNDLE) ? $entity : FALSE;
      }
      return FALSE;
    }
  }

  /**
   * Returns a new Sima Wetlands Management Project Variation.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaWetlandsManagementVariation
   *   The loaded SimaWetlandsManagementVariation.
   */
  static public function newVariation() {
    global $user;
    // Create the new Project Node.
    return self::newEntity('node', self::BUNDLE, $user->uid);
  }

  /**
   * Obtains the topology for the project variant.
   *
   * @return null|string
   *   The topology.
   */
  public function getTopology() {
    if ($project = $this->getWetlandsManagementProject()) {
      return $project->getTypology();
    }
    return NULL;
  }

  /**
   * Returns the Modified value for this variation.
   *
   * @return int|mixed
   *   The modified value.
   */
  public function getModifiedValue() {
    switch ($this->getTopology()) {
      case SimaFichaWetlandsManagement::TYPE_FLOOD_RETURN_FRACTION:
        return $this->getFloodReturnFraction();

      case SimaFichaWetlandsManagement::TYPE_RIVER_FLOODING_FRACTION:
        return $this->getRiverFloodingFraction();

      case SimaFichaWetlandsManagement::TYPE_RIVER_FLOODING_THRESHOLD:
        return $this->getRiverFloodingThreshold();
    }
    return 0;
  }

  /**
   * Sets the Modified value for this variation.
   *
   * @param string $value
   *   The modified value.
   */
  public function setModifiedValue($value) {
    switch ($this->getTopology()) {
      case SimaFichaWetlandsManagement::TYPE_FLOOD_RETURN_FRACTION:
        $this->setFloodReturnFraction($value);
        break;

      case SimaFichaWetlandsManagement::TYPE_RIVER_FLOODING_FRACTION:
        $this->setRiverFloodingFraction($value);
        break;

      case SimaFichaWetlandsManagement::TYPE_RIVER_FLOODING_THRESHOLD:
        $this->setRiverFloodingThreshold($value);
        break;
    }
  }

  /**
   * Returns the Wetlands Management Project.
   *
   * @param string $lang
   *   The language.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Ficha\SimaFichaWetlandsManagement|FALSE
   *   The loaded SimaFichaWetlandsManagement if exists, FALSE otherwise.
   */
  public function getWetlandsManagementProject($lang = LANGUAGE_NONE) {
    $project = $this->get(self::FIELD_WETLANDS_MANAGEMENT_PROJ_REF, $lang);
    return SimaFichaWetlandsManagement::load($project);
  }

  /**
   * Sets the Wetlands Management Project for this variation.
   *
   * @param mixed $wetlands_management
   *   The wetlands Management Project.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Ficha\SimaFichaWetlandsManagement|FALSE
   *   The loaded SimaFichaWetlandsManagement if exists, FALSE otherwise.
   */
  public function setWetlandsManagementProject($wetlands_management) {
    $wetlands_management = SimaFichaWetlandsManagement::load($wetlands_management);
    return $this->set(self::FIELD_WETLANDS_MANAGEMENT_PROJ_REF, $wetlands_management->getId());
  }

  /**
   * Obtains the River Flooding Threshold.
   *
   * @return mixed
   *   The value of River Flooding Threshold.
   */
  public function getRiverFloodingThreshold() {
    return $this->get(self::FIELD_RIVER_FLOODING_THRESHOLD);
  }

  /**
   * Sets the Value of the River Flooding Threshold.
   *
   * @param float $value
   *   The value to set.
   */
  public function setRiverFloodingThreshold($value) {
    $this->set(self::FIELD_RIVER_FLOODING_THRESHOLD, $value);
  }

  /**
   * Obtains the River Flooding Fraction.
   *
   * @return mixed
   *   The value of River Flooding Fraction.
   */
  public function getRiverFloodingFraction() {
    return $this->get(self::FIELD_FIVER_FLOODING_FRACTION);
  }

  /**
   * Sets the Value of the River Flooding Fraction.
   *
   * @param float $value
   *   The value to set.
   */
  public function setRiverFloodingFraction($value) {
    $this->set(self::FIELD_FIVER_FLOODING_FRACTION, $value);
  }

  /**
   * Obtains the Flood Return Fraction.
   *
   * @return mixed
   *   The value of Flood Return Fraction.
   */
  public function getFloodReturnFraction() {
    return $this->get(self::FIELD_FLOOD_RETURN_FRACTION);
  }

  /**
   * Sets the Value of the Flood Return Fraction.
   *
   * @param float $value
   *   The value to set.
   */
  public function setFloodReturnFraction($value) {
    $this->set(self::FIELD_FLOOD_RETURN_FRACTION, $value);
  }

  /**
   * Loads a bunch of Project Variations, given a Project ID.
   *
   * @param int $wetlands_management_id
   *   The Wetlands Management Project NID.
   * @param int $limit
   *   The maximum number of entities to load.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Ficha\SimaFichaWetlandsManagement[]
   *   An array of loaded Project Variations.
   */
  public static function getProjectVariations($wetlands_management_id, $limit = 10) {
    // Search for Case Outputs.
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', self::BUNDLE)
      ->propertyCondition('status', NODE_PUBLISHED)
      ->fieldCondition(self::FIELD_WETLANDS_MANAGEMENT_PROJ_REF, 'target_id', $wetlands_management_id, '=')
      ->range(0, $limit);
    $result = $query->execute();
    $entities = [];

    if (!empty($result['node'])) {
      foreach ($result['node'] as $node) {
        $entity = SimaController::load($node->nid);
        $entities[$node->nid] = $entity;
      }
    }

    return $entities;
  }

}
