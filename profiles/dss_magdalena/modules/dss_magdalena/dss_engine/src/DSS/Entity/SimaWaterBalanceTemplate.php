<?php
/**
 * @file
 * Models a Sima Water Balance Template.
 */

namespace Drupal\dss_magdalena\DSS\Entity;

use Drupal\dss_magdalena\DSS\Entity\Multifield\SimaWaterBalanceInput;
use Drupal\dss_magdalena\DSS\Entity\Multifield\SimaWaterBalanceOutput;
use Drupal\dss_magdalena\DSS\Entity\Multifield\SimaWaterBalanceStorage;
use Drupal\dss_magdalena\DSS\Entity\Multifield\SimaWaterBalanceConsumption;

/**
 * Models a Sima Water Balance Template.
 *
 * Class SimaWaterBalanceTemplate.
 *
 * @package Drupal\dss_magdalena\DSS\Entity
 */
class SimaWaterBalanceTemplate extends EntityNodeAbstract {

  const BUNDLE                          = 'water_balance_template';

  const FIELD_TYPE                      = 'field_water_balance_type';
  const FIELD_MODEL                     = 'field_model';
  const FIELD_WATER_BALANCE_INPUT       = 'field_input';
  const FIELD_WATER_BALANCE_OUTPUT      = 'field_output';
  const FIELD_WATER_BALANCE_STORAGE     = 'field_storage';
  const FIELD_WATER_BALANCE_CONSUMPTION = 'field_consumption';

  /**
   * Loads SimaWaterBalanceTemplate from a Drupal entity or a Drupal entity ID.
   *
   * @param mixed $water_balance_template
   *   The nid or loaded object.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaWaterBalanceTemplate|FALSE
   *   The loaded SimaWaterBalanceTemplate object if exists, FALSE otherwise.
   */
  public static function load($water_balance_template) {
    if ($water_balance_template instanceof SimaWaterBalanceTemplate) {
      return $water_balance_template;
    }
    else {
      if ($entity = parent::load($water_balance_template)) {
        return ($entity->getDrupalBundle() == self::BUNDLE) ? $entity : FALSE;
      }
      return FALSE;
    }
  }

  /**
   * Returns a new WaterBalanceTemplate.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaWaterBalanceTemplate
   *   The loaded SimaWaterBalanceTemplate.
   */
  static public function newWaterBalanceTemplate() {
    global $user;
    // Create the new WaterBalanceTemplate Node.
    return self::newEntity('node', self::BUNDLE, $user->uid);
  }

  /**
   * Gets the Water Balance Input.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Multifield\SimaWaterBalanceInput[]
   *   An array of SimaWaterBalanceInput.
   */
  public function getWaterBalanceInput() {
    $water_balance_input = $this->get(self::FIELD_WATER_BALANCE_INPUT);
    foreach ($water_balance_input as $key => $item) {
      $water_balance_input[$key] = SimaWaterBalanceInput::load($item);
    }
    return $water_balance_input;
  }

  /**
   * Gets the Water Balance Output.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Multifield\SimaWaterBalanceOutput[]
   *   An array of SimaWaterBalanceOutput.
   */
  public function getWaterBalanceOutput() {
    $water_balance_output = $this->get(self::FIELD_WATER_BALANCE_OUTPUT);
    foreach ($water_balance_output as $key => $item) {
      $water_balance_output[$key] = SimaWaterBalanceOutput::load($item);
    }
    return $water_balance_output;
  }

  /**
   * Gets the Water Balance Storage.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Multifield\SimaWaterBalanceStorage[]
   *   An array of SimaWaterBalanceStorage.
   */
  public function getWaterBalanceStorage() {
    $water_balance_storage = $this->get(self::FIELD_WATER_BALANCE_STORAGE);
    foreach ($water_balance_storage as $key => $item) {
      $water_balance_storage[$key] = SimaWaterBalanceStorage::load($item);
    }
    return $water_balance_storage;
  }

  /**
   * Gets the Water Balance Consumption.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Multifield\SimaWaterBalanceConsumption[]
   *   An array of SimaWaterBalanceConsumption.
   */
  public function getWaterBalanceConsumption() {
    $water_balance_consumption = $this->get(self::FIELD_WATER_BALANCE_CONSUMPTION);
    foreach ($water_balance_consumption as $key => $item) {
      $water_balance_consumption[$key] = SimaWaterBalanceConsumption::load($item);
    }
    return $water_balance_consumption;
  }

  /**
   * Obtains the model ID.
   */
  public function getModelId() {
    $model = $this->get(self::FIELD_MODEL);
    return $model->nid;
  }

}
