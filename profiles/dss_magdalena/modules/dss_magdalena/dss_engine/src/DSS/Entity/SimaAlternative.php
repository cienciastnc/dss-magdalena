<?php
/**
 * @file
 * A Sima Alternative.
 */

namespace Drupal\dss_magdalena\DSS\Entity;

use Drupal\dss_magdalena\DSS\Entity\Ficha\SimaFichaProductiveLandUse;
use Drupal\dss_magdalena\DSS\Entity\Ficha\SimaFichaHydropowerPlantDam;
use Drupal\dss_magdalena\DSS\Entity\Ficha\SimaFichaInterface;
use Drupal\dss_magdalena\DSS\Entity\Ficha\SimaFichaRorHydropowerPlant;
use Drupal\dss_magdalena\DSS\Entity\Ficha\SimaFichaWetlandsManagement;
use Drupal\dss_magdalena\DSS\SimaWeapIndex;

/**
 * Models a Sima Alternative.
 *
 * Class SimaAlternative.
 *
 * @package Drupal\dss_magdalena\DSS\Entity
 */
class SimaAlternative extends EntityNodeAbstract {

  const PRODUCTIVE_LANDUSE_ALTERNATIVE      = 'productive_land_use_alternatives';
  const HYDROPOWER_ALTERNATIVE              = 'hydropower_alternatives';
  const WETLANDS_MANAGEMENT_ALTERNATIVE     = 'river_floodplain_connectivity';

  const PRODUCTIVE_LANDUSE_PROJECTS         = 'field_prod_land_use_projs';
  const HYDROPOWER_PROJECTS                 = 'field_hydropower_projects';
  const ROR_HYDROPOWER_PROJECTS             = 'field_ror_hydropower_projects';
  const WETLAND_MANAGEMENT_PROJECTS         = 'field_wetlands_manag_projects';

  /**
   * Loads a SimaAlternative from a Drupal entity or a Drupal entity ID.
   *
   * @param mixed $alternative
   *   The nid or loaded object.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaAlternative|FALSE
   *   The loaded SimaAlternative object if exists, FALSE otherwise.
   */
  public static function load($alternative) {
    if ($alternative instanceof SimaAlternative) {
      return $alternative;
    }
    else {
      $bundles = [
        self::PRODUCTIVE_LANDUSE_ALTERNATIVE,
        self::HYDROPOWER_ALTERNATIVE,
        self::WETLANDS_MANAGEMENT_ALTERNATIVE,
      ];
      if ($entity = parent::load($alternative)) {
        return in_array($entity->getDrupalBundle(), $bundles) ? $entity : FALSE;
      }
      return FALSE;
    }
  }

  /**
   * Gets all the existent resources provided by this this case alternative.
   *
   * Resources === "Cajas de Datos". The resources have to be linked to a
   * particular case study, because the alternative might be used by different
   * case studies.
   *
   * @param int $cid
   *   The Case Study ID.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaResource[]
   *   An array of resources linked to this case study and alternative.
   */
  public function getResources($cid) {
    $resources = [];

    // If no Case Study is given, return empty array.
    if (empty($cid)) {
      return $resources;
    }

    // Obtain the list of datasets mapped to this alternative.
    $mappings = $this->getMappingToDatasets();
    $dataset_machine_names = [];
    foreach ($mappings as $type => $mapping) {
      $dataset_machine_names = array_merge($dataset_machine_names, array_keys($mapping));
    }

    // Get all the resources for this case study and only select the ones that
    // match the list of datasets mapped to this alternative.
    $case_resources = SimaResource::listByCaseStudy($cid);
    foreach ($case_resources as $case_resource) {
      if ($dataset = $case_resource->getDataset()) {
        if (in_array($dataset->getMachineName(), $dataset_machine_names)) {
          $resources[] = $case_resource;
        }
      }
    }
    return $resources;
  }

  /**
   * Whether this is a default alternative (from the default case study) or not.
   *
   * @return bool
   *   TRUE if this is default data, FALSE otherwise.
   */
  public function isDefault() {
    // The default alternatives are the ones associated to the default case
    // study.
    if ($default_case = SimaDefaultCaseStudy::loadExtreme()) {
      switch ($this->getDrupalBundle()) {
        case self::HYDROPOWER_ALTERNATIVE:
          $alternative = $default_case->getHydropowerAlternatives();
          return $alternative->getId() === $this->getId();

        case self::PRODUCTIVE_LANDUSE_ALTERNATIVE:
          $alternative = $default_case->getProductiveLandUseAlternatives();
          return $alternative->getId() === $this->getId();

        case self::WETLANDS_MANAGEMENT_ALTERNATIVE:
          $alternative = $default_case->getWetlandManagementAlternatives();
          return $alternative->getId() === $this->getId();
      }
      return FALSE;
    }

    // If no default case study has been found, then read the following titles
    // as default alternatives.
    $default_titles = [];
    $csv = drupal_get_path('module', 'dss_import') . '/import/dss_import.nodes.integral_plan.csv';
    $file = file($csv);
    if (!empty($file)) {
      $fields = explode(',', $file[1]);
      $default_titles[] = $fields[2];
      $default_titles[] = $fields[3];
      $default_titles[] = $fields[4];
    }
    else {
      // Use this as last resource to set default titles.
      $default_titles = [
        'Linea Base 2016 + Hidro China Plan',
        'Prototipo de cambio productivo',
        'Manejo de Humedales - EXAGERA',
      ];
    }
    $title = $this->getDrupalEntity()->title->value();
    if (in_array($title, $default_titles)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Returns the Datasets / Weap Variables related to Alternatives.
   *
   * @return array
   *   An array of datasets/weap variables keyed by ficha node type.
   */
  public function getMappingToDatasets() {
    $datasets = [];
    switch ($this->getDrupalBundle()) {
      case self::HYDROPOWER_ALTERNATIVE:
        // Hydropower Plants.
        $hydropower_plant = SimaFichaHydropowerPlantDam::newFakeFicha();
        $bundle = $hydropower_plant->getDrupalBundle();
        $datasets[$bundle] = $hydropower_plant->getDatasetsWeap();

        // ROR Hydropower Plants.
        $ror_hydropower_plant = SimaFichaRorHydropowerPlant::newFakeFicha();
        $bundle = $ror_hydropower_plant->getDrupalBundle();
        $datasets[$bundle] = $ror_hydropower_plant->getDatasetsWeap();
        return $datasets;

      case self::PRODUCTIVE_LANDUSE_ALTERNATIVE:
        // Productive Land Use Projects.
        $productive_landuse_projects = SimaFichaProductiveLandUse::newFakeFicha();
        $bundle = $productive_landuse_projects->getDrupalBundle();
        $datasets[$bundle] = $productive_landuse_projects->getDatasetsWeap();
        return $datasets;

      case self::WETLANDS_MANAGEMENT_ALTERNATIVE:
        // Wetland Management Projects.
        $wetland_management_projects = SimaFichaWetlandsManagement::newFakeFicha();
        $bundle = $wetland_management_projects->getDrupalBundle();
        $datasets[$bundle] = $wetland_management_projects->getDatasetsWeap();
        return $datasets;

    }
  }

  /**
   * Obtains the list of projects (Fichas) for the respective alternative.
   *
   * @param string $type
   *   The Ficha/Project type.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Ficha\SimaFichaInterface[]
   *   A list of Fichas (projects).
   */
  public function getFichaProjects($type = NULL) {
    $projects = [];
    switch ($this->getDrupalBundle()) {
      case self::HYDROPOWER_ALTERNATIVE:
        // Hydropower Plants and ROR Hydropower Plants.
        $hydropower_plants = [];
        $ror_hydropower_plants = [];
        if ($type == SimaFichaHydropowerPlantDam::BUNDLE || empty($type)) {
          $hydropower_plants = $this->getMultivalued(self::HYDROPOWER_PROJECTS);
        }
        if ($type == SimaFichaRorHydropowerPlant::BUNDLE || empty($type)) {
          $ror_hydropower_plants = $this->getMultivalued(self::ROR_HYDROPOWER_PROJECTS);
        }
        $projects = array_merge($hydropower_plants, $ror_hydropower_plants);
        break;

      case self::PRODUCTIVE_LANDUSE_ALTERNATIVE:
        // Productive Land Use Projects.
        $projects = $this->getMultivalued(self::PRODUCTIVE_LANDUSE_PROJECTS);
        break;

      case self::WETLANDS_MANAGEMENT_ALTERNATIVE:
        // Wetland Management Projects.
        $projects = $this->getMultivalued(self::WETLAND_MANAGEMENT_PROJECTS);
        break;
    }

    // Loading the appropriate Sima class.
    foreach ($projects as $key => $project) {
      $projects[$key] = SimaController::load($project);
    }
    return $projects;
  }

  /**
   * Adds the Ficha Project to this Alternative.
   *
   * @param \Drupal\dss_magdalena\DSS\Entity\Ficha\SimaFichaInterface $ficha_project
   *   The Development alternative or Ficha (Project).
   */
  public function addFichaProject(SimaFichaInterface $ficha_project, $language = LANGUAGE_NONE) {
    // Validating that the Ficha (Project) corresponds to this alternative.
    $entity = $ficha_project->getDrupalEntity()->value();
    switch ($this->getDrupalBundle()) {
      case self::PRODUCTIVE_LANDUSE_ALTERNATIVE:
        if ($ficha_project->getDrupalBundle() === SimaFichaProductiveLandUse::BUNDLE) {
          // $this->add(self::PRODUCTIVE_LANDUSE_PROJECTS, $entity, $language);.
        }
        break;

      case self::HYDROPOWER_ALTERNATIVE:
        if ($ficha_project->getDrupalBundle() === SimaFichaHydropowerPlantDam::BUNDLE) {
          $this->add(self::HYDROPOWER_PROJECTS, $entity, $language);
        }
        elseif ($ficha_project->getDrupalBundle() === SimaFichaRorHydropowerPlant::BUNDLE) {
          $this->add(self::ROR_HYDROPOWER_PROJECTS, $entity, $language);
        }
        break;

      case self::WETLANDS_MANAGEMENT_ALTERNATIVE:
        if ($ficha_project->getDrupalBundle() === SimaFichaWetlandsManagement::BUNDLE) {
          $project_variations = $ficha_project->getProjectVariations();
          /** @var \Drupal\dss_magdalena\DSS\Entity\SimaWetlandsManagementVariation $project_variation */
          foreach ($project_variations as $project_variation) {
            $this->add(self::WETLAND_MANAGEMENT_PROJECTS, $project_variation->getDrupalEntity()->value(), $language);
          }
        }
        break;
    }
  }


  /**
   * List missing resources, given the list of Projects for the Case Study.
   *
   * @param int $cid
   *   The Case Study ID.
   *
   * @return array
   *   The list of missing resources (dataset machine names).
   */
  public function listMissingResourcesFromProjects($cid) {
    /** @var \Drupal\dss_magdalena\DSS\Entity\SimaResource[] $resources */
    $resources = $this->getResources($cid);

    // This is what should be there to create ALL resources.
    // $mappings = $this->getMappingToDatasets();
    // We are only going to create 3 resources.
    $mappings = [
      SimaFichaHydropowerPlantDam::BUNDLE => [
        'dss_capacidad' => 'Storage Capacity',
        'dss_pertenencia_al_caso_hres' => 'SIMA Case status',
        'dss_project_id_hres' => 'Project ID Hres',
        'dss_topology_arcid_hres' => 'Topology ARCID Hres',
      ],
      SimaFichaRorHydropowerPlant::BUNDLE => [
        'dss_pertenencia_al_caso_hror' => 'SIMA Case status',
        'dss_project_id_hror' => 'Project ID HRoR',
        'dss_topology_arcid_hror' => 'Topology ARCID HRoR',
      ],
    ];
    $dataset_machine_names = [];
    $unset_mappings = [];
    foreach ($mappings as $mapping) {
      $unset_mappings = array_merge($unset_mappings, $mapping);
      $dataset_machine_names = array_merge($dataset_machine_names, array_keys($mapping));
    }

    // Possible values for The Input Category.
    $variable_categories = [
      'Input',
      'Input Alternatives',
      'Input Context',
    ];
    // Checking that the list of Resources is complete or some are missing.
    foreach ($resources as $resource) {
      $resource = SimaResource::load($resource);
      $dataset = $resource->getDataset();
      $dataset_machine_name = $dataset->getMachineName();
      $variable_category = $dataset ? $dataset->getVariableStatus() : FALSE;
      $variable_category = $variable_category ? $variable_category->getName() : '';

      // Check that the existent resources are all we need.
      if (in_array($variable_category, $variable_categories) && in_array($dataset_machine_name, $dataset_machine_names)) {
        unset($unset_mappings[$dataset_machine_name]);
      }
    }

    // Return the list of missing resources for this particular alternative.
    return $unset_mappings;
  }

  /**
   * Sets Baseline plants to the hydropower alternative.
   */
  public function populateHydropowerAlternatives() {
    if ($this->getDrupalBundle() === self::HYDROPOWER_ALTERNATIVE) {
      $default_case = SimaDefaultCaseStudy::loadBaseline();
      $baseline_hres = SimaResource::loadByCaseStudyAndDataset($default_case->getId(), 'dss_pertenencia_al_caso_hres');
      $baseline_hror = SimaResource::loadByCaseStudyAndDataset($default_case->getId(), 'dss_pertenencia_al_caso_hror');

      /** @var \Drupal\dss_magdalena\DSS\SimaWeapIndex $weap_index */
      $weap_index = SimaWeapIndex::loadAllRecords();
      $fields = [
        'branch',
        'level1',
        'level2',
        'level3',
        'level4',
      ];

      $hres = $baseline_hres->getDataCsv();
      $hres = $weap_index->combineWithWeap($hres, $fields);
      $hror = $baseline_hror->getDataCsv();
      $hror = $weap_index->combineWithWeap($hror, $fields);

      // Setting up baseline reservoirs.
      $reservoir_ids = [];
      foreach ($hres as $project) {
        list($res, $name) = explode('\\', $project['level4']);
        $active = (bool) intval($project['Value']);
        if (!$active) {
          // If project is not active, go to next one.
          continue;
        }
        if ($reservoir = SimaFichaHydropowerPlantDam::loadByTitle($name, SimaFichaHydropowerPlantDam::BUNDLE)) {
          $reservoir_ids[] = $reservoir->getId();
        }
      }
      $this->set(SimaAlternative::HYDROPOWER_PROJECTS, array_unique($reservoir_ids));

      // Setting up baseline run of river plants.
      $ror_ids = [];
      foreach ($hror as $project) {
        list($res, $name) = explode('\\', $project['level4']);
        $active = (bool) intval($project['Value']);
        if (!$active) {
          // If project is not active, go to next one.
          continue;
        }
        if ($ror = SimaFichaRorHydropowerPlant::loadByTitle($name, SimaFichaRorHydropowerPlant::BUNDLE)) {
          $ror_ids[] = $ror->getId();
        }
      }
      $this->set(SimaAlternative::ROR_HYDROPOWER_PROJECTS, array_unique($ror_ids));
    }
  }

}
