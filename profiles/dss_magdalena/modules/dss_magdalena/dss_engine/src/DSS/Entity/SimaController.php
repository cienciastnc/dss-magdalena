<?php
/**
 * @file
 * Controls different Entities.
 */

namespace Drupal\dss_magdalena\DSS\Entity;

use Drupal\dss_magdalena\DSS\Entity\Alternatives\SimaProductiveLandUseAlternative;
use Drupal\dss_magdalena\DSS\Entity\Alternatives\SimaHydropowerAlternative;
use Drupal\dss_magdalena\DSS\Entity\Alternatives\SimaWetlandsAlternative;
use Drupal\dss_magdalena\DSS\Entity\Ficha\SimaFichaProductiveLandUse;
use Drupal\dss_magdalena\DSS\Entity\Ficha\SimaFichaHydropowerPlantDam;
use Drupal\dss_magdalena\DSS\Entity\Ficha\SimaFichaRorHydropowerPlant;
use Drupal\dss_magdalena\DSS\Entity\Ficha\SimaFichaWetlandsManagement;
use EntityFieldQuery;

/**
 * Allows to load an appropriate DSS class having the Drupal entity.
 *
 * Class SimaController.
 *
 * @package Drupal\dss_magdalena\DSS\Entity
 */
class SimaController {

  /**
   * Loads an appropriate class according to.
   *
   * @param object|\EntityDrupalWrapper $entity
   *   The Drupal entity.
   *
   * @return \Drupal\dss_magdalena\DSS\SimaAlternative|\Drupal\dss_magdalena\DSS\Entity\Alternatives\SimaAlternativeInterface|\Drupal\dss_magdalena\DSS\SimaScenario
   *   An Scenario or alternative.
   */
  static public function load($entity) {
    if (!$entity instanceof \EntityDrupalWrapper) {
      $entity = entity_metadata_wrapper('node', $entity);
    }
    switch ($entity->getBundle()) {
      case SimaScenario::CLIMATE_SCENARIO:
      case SimaScenario::ENERGY_SCENARIO:
      case SimaScenario::POPULATION_SCENARIO:
        return new SimaScenario($entity);

      case SimaAlternative::PRODUCTIVE_LANDUSE_ALTERNATIVE:
        return new SimaProductiveLandUseAlternative($entity);

      case SimaAlternative::HYDROPOWER_ALTERNATIVE:
        return new SimaHydropowerAlternative($entity);

      case SimaAlternative::WETLANDS_MANAGEMENT_ALTERNATIVE:
        return new SimaWetlandsAlternative($entity);

      case SimaIntegralPlan::BUNDLE:
        return new SimaIntegralPlan($entity);

      case SimaProject::BUNDLE:
        return new SimaProject($entity);

      case SimaModel::BUNDLE:
        return new SimaModel($entity);

      case SimaCaseStudy::BUNDLE:
        return new SimaCaseStudy($entity);

      case SimaResource::BUNDLE:
        return new SimaResource($entity);

      case SimaCatchments::BUNDLE:
        return new SimaCatchments($entity);

      case SimaFichaHydropowerPlantDam::BUNDLE:
        return new SimaFichaHydropowerPlantDam($entity);

      case SimaFichaRorHydropowerPlant::BUNDLE:
        return new SimaFichaRorHydropowerPlant($entity);

      case SimaFichaWetlandsManagement::BUNDLE:
        return new SimaFichaWetlandsManagement($entity);

      case SimaFichaProductiveLandUse::BUNDLE:
        return new SimaFichaProductiveLandUse($entity);

      case SimaProductiveLandUseProjectVariant::BUNDLE:
        return new SimaProductiveLandUseProjectVariant($entity);

      case SimaWetlandsManagementVariation::BUNDLE:
        return new SimaWetlandsManagementVariation($entity);

      default:
        return FALSE;
    }
  }

  /**
   * Provides a list of entities of a certain bundle.
   *
   * @param string $bundle
   *   The Node bundle.
   * @param int $limit
   *   The number of entities to list.
   *
   * @return array
   *   An array of loaded Sima entities.
   */
  public function listEntities($bundle, $limit = 500) {
    // Search for Case Outputs.
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', $bundle)
      ->propertyCondition('status', NODE_PUBLISHED)
      ->range(0, $limit);
    $result = $query->execute();
    $entities = [];

    if (!empty($result['node'])) {
      foreach ($result['node'] as $node) {
        $entity = SimaController::load($node->nid);
        $entities[$node->nid] = $entity;
      }
    }

    return $entities;
  }

}
