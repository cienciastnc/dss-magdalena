<?php
/**
 * @file
 * Models the Temporality.
 */

namespace Drupal\dss_magdalena\DSS\Entity\Term;

/**
 * Models a Sima Temporality.
 *
 * Class SimaTemporality.
 *
 * @package Drupal\dss_magdalena\DSS\Entity\Term
 */
class SimaTemporality extends EntityTermAbstract {

  const BUNDLE          = 'temporality';

  const NAME_NONE       = 'None';
  const NAME_DAY        = 'Day';
  const NAME_MONTH      = 'Month';
  const NAME_YEAR       = 'Year';
  const NAME_CONSTANT   = 'Constant';
  const NAME_DECADIC    = 'Decadic';
  const NAME_INSTANT    = 'Instant';
  const NAME_PERIODIC   = 'Periodic';

  /**
   * Loads a SimaTemporality from a Drupal entity or a Drupal entity ID.
   *
   * @param mixed $temporality
   *   The taxonomy_term tid or loaded object.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Term\SimaTemporality|FALSE
   *   A loaded SimaTemporality object if exists, FALSE otherwise.
   */
  public static function load($temporality) {
    if ($temporality instanceof SimaTemporality) {
      return $temporality;
    }
    else {
      if ($entity = parent::load($temporality)) {
        return ($entity->getDrupalBundle() == self::BUNDLE) ? $entity : FALSE;
      }
      return FALSE;
    }
  }

  /**
   * Whether this has temporality None.
   *
   * @return bool
   *   TRUE if temporality = NONE, FALSE otherwise.
   */
  public function isTemporalityNone() {
    return $this->getName() == self::NAME_NONE;
  }

  /**
   * Whether this has temporality Periodic.
   *
   * @return bool
   *   TRUE if temporality = Periodic, FALSE otherwise.
   */
  public function isTemporalityPeriodic() {
    return $this->getName() == self::NAME_PERIODIC;
  }

}
