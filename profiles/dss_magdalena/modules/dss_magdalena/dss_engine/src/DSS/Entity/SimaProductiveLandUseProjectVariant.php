<?php
/**
 * @file
 * Models a Sima Productive Land Use Project Variant.
 */

namespace Drupal\dss_magdalena\DSS\Entity;

use Drupal\dss_magdalena\DSS\Entity\SimaCatchments;
use Drupal\dss_magdalena\DSS\Entity\Multifield\SimaSimpleLandUsePercentages;

/**
 * Models a Sima Productive Land Use Project Variant.
 *
 * Class SimaProductiveLandUseProjectVariant.
 *
 * @package Drupal\dss_magdalena\DSS\Entity
 */
class SimaProductiveLandUseProjectVariant extends EntityNodeAbstract {

  const BUNDLE = 'productive_land_use_variations';

  const TYPE_AGRICULTURAL                  = 'agricultural';
  const TYPE_AGRICULTURAL_NAME             = 'Explotación Agrícola';
  const TYPE_MINING                        = 'mining';
  const TYPE_MINING_NAME                   = 'Explotación Minera';
  const TYPE_FORESTALL                     = 'forestall';
  const TYPE_FORESTALL_NAME                = 'Explotación Forestal';
  const TYPE_RESTORATION                   = 'restoration';
  const TYPE_RESTORATION_NAME              = 'Restauración';


  const FIELD_IRRIGATION_DISTRICT          = 'field_irrigation_district_ref';
  const FIELD_TYPE_OF_PROJECT              = 'field_type_of_project';
  const FIELD_CATCHMENT_AREA               = 'field_catchment_area';
  const FIELD_AGRICULTURAL_AREA            = 'field_agricultural_area';
  const FIELD_FOREST_INDUSTRIAL_AREA       = 'field_forest_industrial_area';
  const FIELD_MINING_AREA                  = 'field_mining_area';
  const FIELD_PERCENTAGE_OF_IRRIGATED_AREA = 'field_irr_perc_area';
  const FIELD_LAND_USE                     = 'field_landuse';

  /**
   * Loads SimaProductiveLandUseProjectVariant from Drupal entity or entity ID.
   *
   * @param mixed $project
   *   The nid or loaded object.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaProductiveLandUseProjectVariant|FALSE
   *   Loaded SimaProductiveLandUseProject object if exists, FALSE otherwise.
   */
  public static function load($project) {
    if ($project instanceof SimaProductiveLandUseProjectVariant) {
      return $project;
    }
    else {
      if ($entity = parent::load($project)) {
        return ($entity->getDrupalBundle() == self::BUNDLE) ? $entity : FALSE;
      }
      return FALSE;
    }
  }

  /**
   * Returns a new Project.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaProductiveLandUseProjectVariant
   *   The loaded SimaProductiveLandUseProjectVariant.
   */
  static public function newProductiveLandUseProjectVariant() {
    global $user;
    // Create the new Project Node.
    return self::newEntity('node', self::BUNDLE, $user->uid);
  }

  /**
   * Sets the land use project type.
   */
  public function setLandUseProjectType($type) {
    return $this->set(self::FIELD_TYPE_OF_PROJECT, $type);
  }

  /**
   * Obtains the land use project type.
   *
   * @return string
   *   The project type.
   */
  public function getLandUseProjectType() {
    return $this->get(self::FIELD_TYPE_OF_PROJECT);
  }

  /**
   * Obtains all the available project types.
   *
   * @return array
   *   An array of available project types.
   */
  public function getProjectTypes() {
    return [
      self::TYPE_AGRICULTURAL => self::TYPE_AGRICULTURAL_NAME,
      self::TYPE_FORESTALL => self::TYPE_FORESTALL_NAME,
      self::TYPE_MINING => self::TYPE_MINING_NAME,
      self::TYPE_RESTORATION => self::TYPE_RESTORATION_NAME,
    ];
  }

  /**
   * Checks whether this is an Agricultural Project.
   *
   * @return bool
   *   TRUE if it is Agricultural Project, FALSE otherwise.
   */
  public function isAgriculturalProject() {
    return $this->getLandUseProjectType() == self::TYPE_AGRICULTURAL;
  }

  /**
   * Checks whether this is an Mining Project.
   *
   * @return bool
   *   TRUE if it is Mining Project, FALSE otherwise.
   */
  public function isMiningProject() {
    return $this->getLandUseProjectType() == self::TYPE_MINING;
  }

  /**
   * Checks whether this is an Forestall Project.
   *
   * @return bool
   *   TRUE if it is Forestall Project, FALSE otherwise.
   */
  public function isForestallProject() {
    return $this->getLandUseProjectType() == self::TYPE_FORESTALL;
  }

  /**
   * Checks whether this is an Restoration Project.
   *
   * @return bool
   *   TRUE if it is Restoration Project, FALSE otherwise.
   */
  public function isRestorationProject() {
    return $this->getLandUseProjectType() == self::TYPE_RESTORATION;
  }

  /**
   * Returns the Catchment Irrigation District.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaCatchments|FALSE
   *   The loaded SimaCatchments object if set, FALSE otherwise.
   */
  public function getCatchment() {
    $irrigation_district = $this->get(self::FIELD_IRRIGATION_DISTRICT);
    return SimaCatchments::load($irrigation_district);
  }

  /**
   * Sets the Catchment (Irrigation District).
   *
   * @param \Drupal\dss_magdalena\DSS\Entity\SimaCatchments $catchment
   *   The SimaCatchment object.
   */
  public function setCatchment(SimaCatchments $catchment) {
    $this->set(self::FIELD_IRRIGATION_DISTRICT, $catchment->getDrupalEntity()->value());
  }

  /**
   * Returns the Catchment Area.
   *
   * @return string
   *   The catchment area.
   */
  public function getCatchmentArea() {
    return $this->get(self::FIELD_CATCHMENT_AREA);
  }

  /**
   * Sets the Catchment Area.
   *
   * @param string $catchment_area
   *   The Catchment Area as a Float.
   * @param string $language
   *   The Language Code.
   */
  public function setCatchmentArea($catchment_area, $language = LANGUAGE_NONE) {
    $this->set(self::FIELD_CATCHMENT_AREA, $catchment_area, $language);
  }

  /**
   * Returns the Agricultural Area in Km^2.
   *
   * @return mixed
   *   The Agricultural Area.
   */
  public function getAgriculturalArea() {
    return $this->get(self::FIELD_AGRICULTURAL_AREA);
  }

  /**
   * Sets the Agricultural Area in Km^2.
   *
   * @param float $area
   *   The agricultural area we want to set.
   */
  public function setAgriculturalArea($area) {
    $this->set(self::FIELD_AGRICULTURAL_AREA, $area);
  }

  /**
   * Returns the Forest/Industrual Area in Km^2.
   *
   * @return mixed
   *   The Forest/Industrial Area.
   */
  public function getForestIndustrialArea() {
    return $this->get(self::FIELD_FOREST_INDUSTRIAL_AREA);
  }

  /**
   * Sets the Forest/Industrual Area in Km^2.
   *
   * @param float $area
   *   The forest/industrial area we want to set.
   */
  public function setForestIndustrialArea($area) {
    $this->set(self::FIELD_FOREST_INDUSTRIAL_AREA, $area);
  }

  /**
   * Returns the Mining Area in Km^2.
   *
   * @return mixed
   *   The Mining Area.
   */
  public function getMiningArea() {
    return $this->get(self::FIELD_MINING_AREA);
  }

  /**
   * Returns the Mining Area in Km^2.
   *
   * @param float $area
   *   The mining area we want to set.
   */
  public function setMiningArea($area) {
    $this->set(self::FIELD_MINING_AREA, $area);
  }

  /**
   * Obtains the percentage of irrigated area.
   *
   * @return string
   *   The Percentage of Irrigated Area.
   */
  public function getPercentageOfIrrigatedArea() {
    return $this->get(self::FIELD_PERCENTAGE_OF_IRRIGATED_AREA);
  }

  /**
   * Obtains the Percentages of Land Use area.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Multifield\SimaSimpleLandUsePercentages[]
   *   The Multifield Land Use Percentage.
   */
  public function getLandUsePercentages() {
    $irrigation_data = $this->get(self::FIELD_LAND_USE);
    foreach ($irrigation_data as $key => $item) {
      $irrigation_data[$key] = SimaSimpleLandUsePercentages::load($item);
    }
    return $irrigation_data;
  }

  /**
   * Sets the Land Use Percentage for the current Productive Land Use.
   *
   * @param \Drupal\dss_magdalena\DSS\Entity\Multifield\SimaSimpleLandUsePercentages[] $irrigation_data
   *   An array of SimaLandUsePercentages.
   */
  public function setLandUsePercentage(array $irrigation_data) {
    $data = [];
    foreach ($irrigation_data as $key => $landuse_percentage) {
      $data[$key] = (array) $landuse_percentage->getDrupalEntity()->value();
    }
    $this->set(self::FIELD_LAND_USE, $data);
    $this->recalculateAreas($irrigation_data);
  }

  /**
   * Produces all the changed records that have to exported to the master file.
   *
   * @return array
   *   An array of changes.
   */
  public function getItemsToExportInMasterFile() {
    $changes = [];
    $catchment = $this->getCatchment();
    $level1 = $catchment->getWeapLevel1()->getName();
    $level2 = $catchment->getWeapLevel2();
    $irrigated_area = $catchment->getLandUsePercentages();
    foreach ($irrigated_area as $irr_area) {
      $branch_id = $irr_area->getBranchId();
      $level3 = $irr_area->getLandUseType()->getName();
      $changes[$level3]['branch_id'] = $branch_id;
      $changes[$level3]['level1'] = $level1;
      $changes[$level3]['level2'] = $level2;
      $changes[$level3]['level3'] = $level3;
      $changes[$level3]['percentage'] = $irr_area->getPercentage();
    }
    $irrigation_data = $this->getLandUsePercentages();
    $output = [];
    foreach ($irrigation_data as $irr_data) {
      $level3 = $irr_data->getLandUseType()->getName();
      $changes[$level3]['percentage'] = $irr_data->getPercentage();
      $changes[$level3]['irrigated_area'] = $irr_data->getIrrigatedPercentageArea();
      $branch_id = $changes[$level3]['branch_id'];
      $output[$branch_id] = $changes[$level3];
    }
    return $output;
  }


  /**
   * Recalculates Agricultural, Forest/Industrial and Mining areas.
   *
   * These recalculations are done based on the new land use areas.
   *
   * @param \Drupal\dss_magdalena\DSS\Entity\Multifield\SimaSimpleLandUsePercentages[] $irrigation_data
   *   An array of SimaLandUsePercentages.
   */
  protected function recalculateAreas(array $irrigation_data) {
    $agricultural_area = 0;
    $forest_area = 0;
    $mining_area = 0;
    // Calculating areas.
    foreach ($irrigation_data as $landuse) {
      if ($landuse->getLandUseType()->isAgriculturalLand()) {
        $agricultural_area += $landuse->getPercentage();
      }
      if ($landuse->getLandUseType()->isForestAndIndustrialLand()) {
        $forest_area += $landuse->getPercentage();
      }
      if ($landuse->getLandUseType()->isMiningLand()) {
        $mining_area += $landuse->getPercentage();
      }
    }
    $agricultural_area = $agricultural_area * $this->getCatchmentArea() / 100;
    $forest_area = $forest_area * $this->getCatchmentArea() / 100;
    $mining_area = $mining_area * $this->getCatchmentArea() / 100;

    // Re-setting areas.
    $this->setAgriculturalArea($agricultural_area);
    $this->setForestIndustrialArea($forest_area);
    $this->setMiningArea($mining_area);
  }

}
