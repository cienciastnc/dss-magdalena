<?php
/**
 * @file
 * Models a Variable Status taxonomy term.
 */

namespace Drupal\dss_magdalena\DSS\Entity\Term;
use Drupal\dss_magdalena\DSS\Entity\SimaModel;

/**
 * Implements a Variable Status taxonomy term.
 *
 * Class SimaVariableStatus.
 *
 * @package Drupal\dss_magdalena\DSS\Entity\Term
 */
class SimaVariableStatus extends EntityTermAbstract {

  const BUNDLE                            = 'variable_status';

  const NAME_INPUT                        = 'Input';
  const NAME_INPUT_SCENARIO               = 'Input Scenario';
  const NAME_INPUT_ALTERNATIVES           = 'Input Alternatives';
  const NAME_INPUT_CONTEXT                = 'Input Context';
  const NAME_INPUT_CONTEXT_INFOBASE       = 'Infobase';
  const NAME_INPUT_CONTEXT_INITIAL_STATE  = 'Initial State';
  const NAME_INPUT_CONTEXT_PARAMETER      = 'Parameter';
  const NAME_INPUT_CONTEXT_OBSERVATION    = 'Observation';
  const NAME_OUTPUT                       = 'Output';
  const NAME_OUTPUT_WEAP                  = 'Output WEAP';
  const NAME_OUTPUT_ELOHA                 = 'Output ELOHA';
  const NAME_OUTPUT_FRAGMENTATION         = 'Output Fragmentacion';
  const NAME_OUTPUT_DOR_H                 = 'Output DOR-H';
  const NAME_OUTPUT_DOR_W                 = 'Output DOR-W';
  const NAME_OUTPUT_SEDIMENTS             = 'Output Transporte Solido';
  const NAME_OUTPUT_TERRITORIAL_FOOTPRINT = 'Output Huella Territorial';

  /**
   * Loads a SimaVariableStatus from a Drupal entity or a Drupal entity ID.
   *
   * @param mixed $variable_status
   *   The taxonomy_term tid or loaded object.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Term\SimaVariableStatus|FALSE
   *   The loaded SimaVariableType object if exists, FALSE otherwise.
   */
  public static function load($variable_status) {
    if ($variable_status instanceof SimaVariableStatus) {
      return $variable_status;
    }
    else {
      if ($entity = parent::load($variable_status)) {
        return ($entity->getDrupalBundle() == self::BUNDLE) ? $entity : FALSE;
      }
      return FALSE;
    }
  }

  /**
   * Figures out if the current taxonomy term is a Modeling Input Resource.
   *
   * @return bool
   *   TRUE if it is an INPUT resource, FALSE otherwise.
   */
  public function isModelingInput() {
    $name = $this->getName();
    $input_vars = [
      self::NAME_INPUT,
      self::NAME_INPUT_SCENARIO,
      self::NAME_INPUT_ALTERNATIVES,
      self::NAME_INPUT_CONTEXT,
      self::NAME_INPUT_CONTEXT_INFOBASE,
      self::NAME_INPUT_CONTEXT_INITIAL_STATE,
      self::NAME_INPUT_CONTEXT_PARAMETER,
      self::NAME_INPUT_CONTEXT_OBSERVATION,
    ];
    return in_array($name, $input_vars);
  }

  /**
   * If it is an Input Alternatives.
   *
   * @return bool
   *   TRUE if it is an Input Alternative, FALSE otherwise.
   */
  public function isInputAlternative() {
    return $this->getName() == self::NAME_INPUT_ALTERNATIVES;
  }

  /**
   * Figures out if the current taxonomy term is a Modeling Output Resource.
   *
   * @return bool
   *   TRUE if it is an OUTPUT resource, FALSE otherwise.
   */
  public function isModelingOutput() {
    $name = $this->getName();
    $input_vars = [
      self::NAME_OUTPUT,
      self::NAME_OUTPUT_WEAP,
      self::NAME_OUTPUT_ELOHA,
      self::NAME_OUTPUT_FRAGMENTATION,
      self::NAME_OUTPUT_DOR_H,
      self::NAME_OUTPUT_DOR_W,
      self::NAME_OUTPUT_SEDIMENTS,
      self::NAME_OUTPUT_TERRITORIAL_FOOTPRINT,
    ];
    return in_array($name, $input_vars);
  }

  /**
   * Loads a Modeling Output Taxonomy Term by Model Name.
   *
   * @param string $model
   *   The Model Name.
   *
   * @return bool|\Drupal\dss_magdalena\DSS\Entity\Term\SimaVariableStatus
   *   The loaded SimaVariableStatus Object if model exists, FALSE otherwise.
   */
  public static function loadOutputTermByModel($model) {
    switch ($model) {
      case SimaModel::MODEL_WEAP:
        return static::loadByTaxonomyTermName(self::NAME_OUTPUT_WEAP);

      case SimaModel::MODEL_ELOHA:
        return static::loadByTaxonomyTermName(self::NAME_OUTPUT_ELOHA);

      case SimaModel::MODEL_FRAGMENTATION:
        return static::loadByTaxonomyTermName(self::NAME_OUTPUT_FRAGMENTATION);

      case SimaModel::MODEL_TERRITORIAL_FOOTPRINT:
        return static::loadByTaxonomyTermName(self::NAME_OUTPUT_TERRITORIAL_FOOTPRINT);

      case SimaModel::MODEL_DOR_H:
        return static::loadByTaxonomyTermName(self::NAME_OUTPUT_DOR_H);

      case SimaModel::MODEL_DOR_W:
        return static::loadByTaxonomyTermName(self::NAME_OUTPUT_DOR_W);

      case SimaModel::MODEL_SEDIMENTS:
        return static::loadByTaxonomyTermName(self::NAME_OUTPUT_SEDIMENTS);

      default:
        return FALSE;
    }
  }

}
