<?php
/**
 * @file
 * Generates a Master File based on the information contained in a case study.
 */

namespace Drupal\dss_magdalena\DSS\Utils;

use Drupal\dss_magdalena\DSS\Entity\Ficha\SimaFichaHydropowerPlantDam;
use Drupal\dss_magdalena\DSS\Entity\Ficha\SimaFichaRorHydropowerPlant;
use Drupal\dss_magdalena\DSS\Entity\SimaCaseStudy;
use Drupal\dss_magdalena\DSS\Entity\SimaDefaultCaseStudy;
use Drupal\dss_magdalena\DSS\Entity\SimaResource;
use Drupal\dss_magdalena\DSS\SimaWeapMasterFileExcel;
use Drupal\dss_magdalena\DSS\Utils\SimaCsvReaderTrait;
use Drupal\dss_magdalena\DSS\Utils\SimaCsvWriterTrait;
use Drupal\dss_magdalena\DSS\Utils\SimaDirectoriesTrait;
use Drupal\dss_magdalena\DSS\Utils\SimaMasterfileXlsXWriter;
use Drupal\dss_magdalena\DSS\SimaWeapIndex;

/**
 * Generates a WEAP Master file for the given Case Study.
 *
 * Class SimaMasterFileGenerator.
 *
 * @package Drupal\dss_magdalena\DSS\Utils
 */
class SimaMasterFileGenerator {

  const MASTER_FILE = 'public://sima/masterfiles/!cid/masterfile';

  use SimaCsvReaderTrait;
  use SimaCsvWriterTrait;
  use SimaDirectoriesTrait;
  use SimaMasterfileXlsXWriter;

  /**
   * The Case Study from which to generate the Master File.
   *
   * @var \Drupal\dss_magdalena\DSS\Entity\SimaCaseStudy|FALSE
   *   The Case Study.
   */
  protected $case;

  /**
   * The real path location to the original Master File in CSV format.
   *
   * @var false|string
   *   The real path location to the original Weap Masterfile in CSV format.
   */
  protected $original;

  /**
   * An array of all lines of the new Master File before saving.
   *
   * @var array
   *   An array of the contents of the newly generated master file.
   */
  protected $file;

  /**
   * Public Constructor.
   *
   * @param int $cid
   *   The Case Study ID.
   */
  public function __construct($cid) {
    $this->case = SimaCaseStudy::load($cid);
  }

  /**
   * Returns the Master file URI in CSV format.
   *
   * @return string
   *   The master file URI in CSV Format.
   */
  protected function getMasterfileCsvUri() {
    return str_replace('!cid', $this->case->getId(), self::MASTER_FILE . '.csv');
  }

  /**
   * Returns the Master file URI in XLSX format.
   *
   * @return string
   *   The master file URI in XLSX Format.
   */
  protected function getMasterfileXlsxUri() {
    return str_replace('!cid', $this->case->getId(), self::MASTER_FILE . '.xlsx');
  }

  /**
   * Initializes the Generation of the Masterfile.
   *
   * @return bool
   *   TRUE if initialization succeeded, FALSE otherwise.
   */
  protected function initialize() {
    // Obtaining a reference to the original Master File.
    $original_master_file = new SimaWeapMasterFileExcel();
    if ($file = $original_master_file->getWeapMasterFileCsv()) {
      $this->original = drupal_realpath($file->uri);

      // Initialize output directory.
      $file_uri = $this->getMasterfileCsvUri();
      $this->createPathToFile($file_uri);
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Generates the WEAP Master File in CSV/XLSX formats.
   *
   * @return null|string
   *   The contents of the master file if succeeds, NULL otherwise.
   */
  public function generate() {
    if ($this->initialize()) {
      // Generate Scenarios.
      $this->generateScenarios();

      // Generate Development Alternatives.
      $this->generateWetlandsManagementAlternatives();
      $this->generateHydroelectricDevelopmentAlternatives();
      $this->generateProductiveLandUseAlternatives();

      // Save results.
      if ($file = $this->saveMasterFile()) {
        return file_get_contents($file);
      }
      return NULL;
    }
  }

  /**
   * Generates the lines in the master file for scenario selection.
   *
   * @TODO: Use machine names to identify scenarios instead of titles.
   */
  protected function generateScenarios() {
    // Check which Climate Scenario we are using.
    $climate_scenario = $this->case->getClimateScenarios();
    $climate = $climate_scenario->getClimateId();
    $this->file[] = [
      '9455',
      '27',
      '1',
      'Key Assumptions',
      'SIMA_MasterKeys',
      'ClimateScenario',
      '',
      'Annual Activity Level',
      'Current Accounts',
      '',
      isset($climate) ? $climate : 1,
    ];

    // Check which Population Scenario we are using.
    $population_scenario = $this->case->getPopulationScenarios();
    $population = $population_scenario->getPopulationId();
    $this->file[] = [
      '16304',
      '27',
      '1',
      'Key Assumptions',
      'SIMA_MasterKeys',
      'PopulationScenario',
      '',
      'Annual Activity Level',
      'Current Accounts',
      '',
      isset($population) ? $population : 1,
    ];
  }

  /**
   * Generates the lines in the master file for Hydroelectric alternatives.
   */
  protected function generateHydroelectricDevelopmentAlternatives() {
    $dataset_machine_names = [
      'dss_pertenencia_al_caso_hror',
      'dss_pertenencia_al_caso_hres',
    ];
    /** @var \Drupal\dss_magdalena\DSS\SimaWeapIndex $weap */
    $weap = SimaWeapIndex::loadAllRecords();

    // Obtain the projects for Hydropower Dams/ROR.
    $projects_dam = $this->case->getHydropowerAlternatives()->getFichaProjects(SimaFichaHydropowerPlantDam::BUNDLE);
    $projects_ror = $this->case->getHydropowerAlternatives()->getFichaProjects(SimaFichaRorHydropowerPlant::BUNDLE);

    if (count($projects_dam) > 0 || count($projects_ror) > 0) {

      foreach ($dataset_machine_names as $dataset_machine_name) {
        // When we load the resources, we have to use the Extreme Master Case:
        // "Case Study 1".
        $case_ref = SimaDefaultCaseStudy::loadExtreme();
        if ($resource = SimaResource::loadByCaseStudyAndDataset($case_ref->getId(), $dataset_machine_name)) {
          $data = $this->getResourceCsvContentsAsArray($resource);
          unset($data[0]);
          $data = $weap->combineWith($data);
          $data_left = $data;
          foreach ($data as $key => $item) {
            $projects = strpos($item['level4'], 'Run of River Hydro') !== FALSE ? $projects_ror : $projects_dam;

            // @TODO: Read the $variable_id from the Project Information!!
            $variable_id = strpos($item['level4'], 'Run of River Hydro') !== FALSE ? '2033' : '2032';
            $item['branch'] = $item['level1'] . "\\" . $item['level2'] . "\\" . $item['level3'] . "\\" . $item['level4'];
            foreach ($projects as $project) {
              // Check that names coincide.
              if ($project->getFullBranchName() == $item['branch']) {
                // It matches, add it to the master file.
                $this->file[] = [
                  $item['branch_id'],
                  $variable_id,
                  '1',
                  $item['level1'],
                  $item['level2'],
                  $item['level3'],
                  $item['level4'],
                  'SIMA Case status',
                  'Current Accounts',
                  '',
                  '1',
                ];
                // Delete it from the array.
                unset($data_left[$key]);
              }
            }
          }
          // Now process the data that's left.
          foreach ($data_left as $item) {
            $variable_id = strpos($item['level4'], 'Run of River Hydro') !== FALSE ? '2033' : '2032';
            $this->file[] = [
              $item['branch_id'],
              $variable_id,
              '1',
              $item['level1'],
              $item['level2'],
              $item['level3'],
              $item['level4'],
              'SIMA Case status',
              'Current Accounts',
              '',
              '0',
            ];
          }
        }
      }
    }
  }

  /**
   * Generates the lines in the master file for Agricultural alternatives.
   */
  protected function generateProductiveLandUseAlternatives() {
    $dataset_machine_names = [
      'dss_uso_suelo_porc_simulacion',
      'dss_porcentaje_regadio',
    ];

    /** @var \Drupal\dss_magdalena\DSS\SimaWeapIndex $weap */
    $weap = SimaWeapIndex::loadAllRecords();

    // Obtain the projects for Hydropower Dams/ROR.
    $items = [];
    $projects = $this->case->getProductiveLandUseAlternatives()->getFichaProjects();
    foreach ($projects as $project) {
      $item = $project->getItemsToExportInMasterFile();
      $items = $items + $item;
    }

    if (count($projects) > 0) {

      foreach ($dataset_machine_names as $dataset_machine_name) {
        // When we load the resources, we have to use the Extreme Master Case:
        // "Case Study 1".
        $case_ref = SimaDefaultCaseStudy::loadExtreme();
        if ($resource = SimaResource::loadByCaseStudyAndDataset($case_ref->getId(), $dataset_machine_name)) {
          $data = $this->getResourceCsvContentsAsArray($resource);
          unset($data[0]);
          $data = $weap->combineWith($data);

          foreach ($items as $branch_id => $item) {
            $data[$branch_id]['Value'] = $item['percentage'];
            $data[$branch_id]['Value2'] = $item['irrigated_area'];
          }
          foreach ($data as $key => $item) {
            // @TODO: Read the $variable_id from the Project Information!!
            $variable_id = $dataset_machine_name == 'dss_uso_suelo_porc_simulacion' ? '137' : '212';
            $variable = $dataset_machine_name == 'dss_uso_suelo_porc_simulacion' ? 'Area' : 'Irrigated Area';
            // It matches, add it to the master file.
            if (isset($item['branch_id'])) {
              $this->file[] = [
                $item['branch_id'],
                $variable_id,
                '1',
                $item['level1'],
                $item['level2'],
                $item['level3'],
                $item['level4'],
                $variable,
                'Current Accounts',
                '',
                // @TODO: This value should come from the Project.
                $dataset_machine_name == 'dss_uso_suelo_porc_simulacion' ? $item['Value'] : $item['Value2'],
              ];
            }
          }
        }
      }
    }
  }

  /**
   * Generates lines in the master file for Wetlands Management alternatives.
   */
  protected function generateWetlandsManagementAlternatives() {
    $wetland_management_projects = variable_get('dss_engine_rows_masterfile_westland_projects');
    if (empty($wetland_management_projects)) {
      drupal_set_message(t('There are not wetland management projects found, thus no variants could be created in the masterfile.'), 'warning');
      return;
    }
    $projects = $this->case->getWetlandManagementAlternatives()->getFichaProjects();
    if (count($projects) > 0) {
      foreach ($projects as $project) {
        $name = $project->getWetlandsManagementProject()->getTitle();
        $item = $wetland_management_projects[$name];
        $value = $project->getModifiedValue();
        $this->file[] = [
          $item['BranchID'],
          $item['VariableID'],
          $item['ScenarioID'],
          isset($item['Level 1']) ? $item['Level 1'] : '',
          isset($item['Level 2']) ? $item['Level 2'] : '',
          isset($item['Level 3']) ? $item['Level 3'] : '',
          isset($item['Level 4']) ? $item['Level 4'] : '',
          $item['Variable'],
          $item['Scenario'],
          '',
          $value,
        ];
      }
    }
  }

  /**
   * Saves the Master File in CSV and XLSX formats.
   *
   * @return bool|string
   *   The Real Filepath to XLSX format if succeeds, FALSE otherwise.
   */
  protected function saveMasterFile() {
    // Save the master file in CSV format.
    $file_uri_csv = $this->getMasterfileCsvUri();
    if ($this->saveUnManagedCsvFile($this->file, $file_uri_csv)) {
      // The CSV file was successfully saved. Now save it to XLSX format.
      return $this->saveMasterFileXlsx($this->file, $this->getMasterfileXlsxUri(), $this->case->getTitle());
    }
    return FALSE;
  }

  /**
   * Obtains the CSV Resource File contents as an array.
   *
   * @param \Drupal\dss_magdalena\DSS\Entity\SimaResource $resource
   *   The SimaResource.
   *
   * @return array
   *   An array with the contents of the variable.
   */
  protected function getResourceCsvContentsAsArray(SimaResource $resource) {
    $file = $resource->getResourceCsvFile();
    $csv_realpath = drupal_realpath($file->uri);
    $results = $this->extractRows('filterByNoFilter', $csv_realpath);
    return iterator_to_array($results);
  }

}
