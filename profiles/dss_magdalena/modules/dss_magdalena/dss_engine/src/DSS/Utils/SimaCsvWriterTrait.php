<?php
/**
 * @file
 * Provides methods to write to a CSV File.
 */

namespace Drupal\dss_magdalena\DSS\Utils;

use League\Csv\Writer;
use SplTempFileObject;

/**
 * Class SimaCsvWriterTrait.
 *
 * @package Drupal\dss_magdalena\DSS\Utils
 */
trait SimaCsvWriterTrait {

  /**
   * Saves a Managed CSV File, given an array of rows (lines).
   *
   * It outputs a file object, if succeeds.
   *
   * @param array $data
   *   A string containing the contents of the file.
   * @param string $destination
   *   A string containing the destination URI. This must be a stream wrapper
   *   URI. If no value is provided, a randomized name will be generated and
   *   the file will be saved using Drupal's default files scheme, usually
   *   "public://".
   * @param int $replace
   *   Replace behavior when the destination file already exists:
   *   - FILE_EXISTS_REPLACE - Replace the existing file. If a managed file with
   *       the destination name exists then its database entry will be updated.
   *       If no database entry is found then a new one will be created.
   *   - FILE_EXISTS_RENAME - Append _{incrementing number} until the filename
   *       is unique.
   *   - FILE_EXISTS_ERROR - Do nothing and return FALSE.
   *
   * @return bool|\stdClass
   *   A file object, or FALSE on error.
   */
  protected function saveManagedCsvFile($data, $destination, $replace = FILE_EXISTS_REPLACE) {
    $csv = Writer::createFromFileObject(new SplTempFileObject());
    $csv->insertAll($data);
    return file_save_data($csv, $destination, $replace);
  }

  /**
   * Saves an Un-managed CSV File, given an array of rows (lines).
   *
   * It outputs the path of the resulting file, if succeeds.
   *
   * @param array $data
   *   A string containing the contents of the file.
   * @param string $destination
   *   A string containing the destination location. This must be a stream
   *   wrapper URI. If no value is provided, a randomized name will be generated
   *   and the file will be saved using Drupal's default files scheme, usually
   *   "public://".
   * @param int $replace
   *   Replace behavior when the destination file already exists:
   *   - FILE_EXISTS_REPLACE - Replace the existing file.
   *   - FILE_EXISTS_RENAME - Append _{incrementing number} until the filename
   *                          is unique.
   *   - FILE_EXISTS_ERROR - Do nothing and return FALSE.
   *
   * @return bool|string
   *   A string with the path of the resulting file, or FALSE on error.
   */
  protected function saveUnManagedCsvFile($data, $destination, $replace = FILE_EXISTS_REPLACE) {
    $csv = Writer::createFromFileObject(new SplTempFileObject());
    $csv->setOutputBOM("\xEF\xBB\xBF");
    $csv->insertAll($data);
    return file_unmanaged_save_data($csv, $destination, $replace);
  }

}
