<?php
/**
 * @file
 * Downloads a file and stores it locally.
 */

namespace Drupal\dss_magdalena\DSS\Utils;

/**
 * Trait to download external files.
 *
 * Class SimaDownload.
 *
 * @package Drupal\dss_magdalena\DSS\Utils
 */
trait SimaDownload {

  /**
   * Downloads an external file.
   *
   * @param string $url
   *   The External file url.
   * @param string $destination
   *   The local directory.
   * @param string $variable
   *   The drupal variable it will store the fid.
   *
   * @return bool|object
   *   The File object if exists, FALSE otherwise.
   */
  public function getExternalFile($url, $destination = 'public://', $variable = NULL) {
    // Verify it is a url.
    if (empty($url) || filter_var($url, FILTER_VALIDATE_URL) === FALSE) {
      return FALSE;
    }

    // Now that the file is properly saved. Notify the class to save it.
    $fid = variable_get($variable, FALSE);

    // Obtain old fid and delete it if different to the new fid.
    if ($fid === FALSE) {
      $path = pathinfo($url);
      $filename = $destination . $path['filename'];
      if ($file = system_retrieve_file($url, $filename, TRUE, FILE_EXISTS_RENAME)) {
        // Now attach new fid.
        $fid = $file->fid;
        variable_set($variable, $fid);
        return $file;
      }
    }
    return file_load($fid);
  }

}
