<?php
/**
 * @file
 * Imports Sima Catchments.
 */

namespace Drupal\dss_magdalena\DSS\Utils;

use Drupal\dss_magdalena\DSS\SimaWeapIndex;
use Drupal\dss_magdalena\DSS\SimaWeapMasterFileExcel;
use Drupal\dss_magdalena\DSS\Entity\SimaCatchments;
use Drupal\dss_magdalena\DSS\Entity\SimaLayer;
use EntityMetadataWrapperException;

/**
 * Implements import of Catchments.
 *
 * Class SimaImportCatchments.
 *
 * @package Drupal\dss_magdalena\DSS\Utils
 */
class SimaImportCatchments extends SimaUtils {

  const DIRECTORY                     = 'public://weap_files';
  const CATCHMENT_LAYER               = 'catchments';
  const VAR_GEOJSON_FEATURES          = 'dss_engine_catchments_geojson_features';
  const CATCHMENTS_EXTRACTED_RECORDS  = 'dss_engine_weap_catchments_irrigated_areas';

  /**
   * Public constructor.
   *
   * @param int $autodetect_line_endings
   *   Autodetect line endings.
   */
  public function __construct($autodetect_line_endings = 1) {
    $this->setAutodetectLineEndings($autodetect_line_endings);
  }

  /**
   * Loads the Catchments GeoJson into a FeatureCollection object.
   *
   * @return bool|\GeoJson\Feature\FeatureCollection
   *   A loaded GeoJson FeatureCollection object if exists, FALSE otherwise.
   */
  protected function getLoadedGeoJson() {
    if ($layer = SimaLayer::loadByMachineName(self::CATCHMENT_LAYER)) {
      $json = $layer->getOriginalGeoJson();
      $json = json_decode($json);
      $geojson = \GeoJson\GeoJson::jsonUnserialize($json);
      return $geojson;
    }
    return FALSE;
  }

  /**
   * Returns an array of GeoJson Features keyed by Branch full name.
   *
   * @return array
   *   An array of GeoJson Features keyed by branch.
   */
  public function getGeoJsonFeatures() {
    $json_features = variable_get(self::VAR_GEOJSON_FEATURES, []);
    if (count($json_features) == 0) {
      if ($geojson = $this->getLoadedGeoJson()) {
        foreach ($geojson->getFeatures() as $feature) {
          $properties = $feature->getProperties();
          $branch = isset($properties['Branch']) ? $properties['Branch'] : FALSE;
          if ($branch) {
            list($prefix, $branch_name) = explode('\\', $branch);
            $json_field = $feature->jsonSerialize();
            $json_features[$branch_name] = json_encode($json_field);
          }
        }
        variable_set(self::VAR_GEOJSON_FEATURES, $json_features);
      }
    }
    return $json_features;
  }

  /**
   * Extracts Catchments from the Master File.
   *
   * @return array
   *   An array of catchments rows from the master file.
   */
  public function extractObjects() {
    // Load all stored WEAP Branches.
    /** @var \Drupal\dss_magdalena\DSS\SimaWeapIndex $weap */
    $weap = SimaWeapIndex::loadAllRecords();

    // Because at this moment we are creating productive land use projects, one
    // per catchment, then we will have to obtain the complete list of
    // catchments and set them up like one per catchment.
    $variable_items = self::CATCHMENTS_EXTRACTED_RECORDS;

    // Variable we will take the number of fichas.
    $items = variable_get($variable_items, array());
    if (count($items) <= 0) {

      // Adding Catchment Area.
      $rows = $this->extractDatasetCsv('dss_area_subcuenca', 'filterByNoFilter');
      $rows = $weap->combineWith($rows);

      foreach ($rows as $row) {
        $name = $row['level2'];
        $value = !empty($row['Value']) ? $row['Value'] : 0;
        $items[$name]['area'] = $value;
        $items[$name]['irrigated_area'] = NULL;
        $items[$name]['demand_priority'] = NULL;
        $items[$name]['transmission_link'] = FALSE;
        $items[$name]['max_flow_value'] = FALSE;
      }

      // Adding land type percentage of share.
      $rows = $this->extractDatasetCsv('dss_uso_suelo_porc_simulacion', 'filterByNoFilter');
      $rows = $weap->combineWith($rows);
      foreach ($rows as $row) {
        $name = $row['level2'];
        $branch_id = $row['BranchID'];
        $items[$name][$branch_id] = $row;
      }

      // Adding Demand Priority.
      $rows = $this->extractDatasetCsv('dss_prioridad_usuario_riego', 'filterByNoFilter');
      $rows = $weap->combineWith($rows);
      foreach ($rows as $row) {
        $demand_priority = $row['Value'];
        if (!empty($demand_priority)) {
          $items[$name]['demand_priority'] = $demand_priority;
        }
      }

      // Adding Irrigated Area.
      $irrigated_areas = [];
      $rows = $this->extractDatasetCsv('dss_porcentaje_regadio', 'filterByNoFilter');
      $rows = $weap->combineWith($rows);
      foreach ($rows as $row) {
        if ($row['level3'] === 'Agricola') {
          $name = $row['level2'];
          $irrigated_area = $row['Value'];
          $items[$name]['irrigated_area'] = $irrigated_area;
          if (intval($irrigated_area) == 100) {
            $irrigated_areas[] = $name;
          }
        }
      }

      // Searching if any of the irrigated areas is a destination of any of the
      // transmission links.
      $master_file = SimaWeapMasterFileExcel::getWeapMasterFileCsv();
      $master_file_csv = drupal_realpath($master_file->uri);
      $results = $this->extractRows('filterByTransmissionLinks', $master_file_csv, TRUE);
      $transmission_links = iterator_to_array($results, FALSE);
      foreach ($transmission_links as $transmission_link) {
        $destination = str_replace('to ', '', $transmission_link['level3']);

        // Checking if catchment is the destination of a transmission link.
        if (in_array($destination, $irrigated_areas)) {
          $items[$destination]['transmission_link'] = TRUE;
        }

        // Checking the max flow volume.
        if ($transmission_link['Variable'] === 'Maximum Flow   Volume' && isset($items[$destination])) {
          $items[$destination]['max_flow_value'] = $transmission_link['Expression'];
        }
      }

      // Now store this variable.
      variable_set($variable_items, $items);
    }
    return $items;
  }

  /**
   * Imports Catchments Content Types.
   *
   * @param array $objects
   *   The list of catchments.
   * @param array $geojson_features
   *   An array of GeoJson Features keyed by Branch full name.
   * @param string $name
   *   The Name.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaCatchments|FALSE
   *   A loaded SimaCatchments if it can be imported, FALSE otherwise.
   */
  public function import($objects = [], $geojson_features = [], $name = NULL) {
    if ($name === NULL) {
      return FALSE;
    }
    // Creates a new Catchment from only this record.
    $catchment = isset($objects[$name]) ? $objects[$name] : [];

    // We cannot import empty data.
    if (empty($catchment)) {
      return FALSE;
    }

    // Obtaining the GeoJson feature field.
    $geojson_field = isset($geojson_features[$name]) ? $geojson_features[$name] : '';

    // Converting GeoJson to WKT.
    if (empty($geojson_field)) {
      $geojson_field_wkt = NULL;
    }
    else {
      module_load_include('inc', 'geophp', 'geoPHP/geoPHP');
      $geom = \geoPHP::load($geojson_field, 'json');
      $geojson_field_wkt = $geom->out('wkt');
    }

    // We have data, then proceed with importing.
    try {
      $sima_catchment = SimaCatchments::fromMasterfileRecord($name, $catchment, $geojson_field_wkt);
      $sima_catchment->save();
      return $sima_catchment;
    }
    catch (EntityMetadataWrapperException $ex) {
      return FALSE;
    }
  }

}
