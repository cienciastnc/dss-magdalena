<?php
/**
 * @file
 * Provides methods to read from a CSV File.
 */

namespace Drupal\dss_magdalena\DSS\Utils;

use League\Csv\Reader;

trait SimaCsvReaderTrait {

  /**
   * The Header for Resource Files.
   *
   * @var array
   *   THe header for Resource Files.
   */
  protected $masterHeader = array(
    'BranchID',
    'Branch',
    'Level 1',
    'Level 2',
    'Level 3',
    'Level 4',
    'Year',
    'Timestep',
    'Variable',
    'Units',
    'Scenario',
    'Alias',
    'Value',
  );

  protected $resourceHeader = array(
    'BranchID',
    'GeoBranchID',
    'Year',
    'Timestep',
    'Value',
  );

  /**
   * Extract rows from the CSV File based on a filter.
   *
   * @param string $filter
   *   The filter name to apply.
   * @param string $csv_file
   *   The CSV File to process the filter on.
   * @param array|NULL $header
   *   Header Array.
   * @param bool $to_utf8
   *   Whether to convert to file to UTF-8.
   *
   * @return mixed
   *   The Results.
   */
  protected function extractRows($filter, $csv_file, $header = NULL, $to_utf8 = FALSE) {
    $csv_file = Reader::createFromPath($csv_file);

    // The encoding of the file 'master-2.0.5\ Beta.csv' is WINDOWS-1250 but
    // we need UTF-8, then we convert to UTF-8.
    // Also, the InputBOM should return WINDOWS-1250 but it doesn't because it
    // cannot detect the encoding but we know it is that one so we just force
    // it to change.
    $input_bom = $csv_file->getInputBOM();
    if ($input_bom === '' && $to_utf8) {
      $csv_file->appendStreamFilter('convert.iconv.WINDOWS-1250/UTF-8');
    }

    // If the header is given, use it. Otherwise use the known Resource Header.
    $header = is_array($header) ? $header : $this->resourceHeader;

    module_load_include('inc', 'dss_engine', 'src/DSS/inc/sima_import_weap_databox_csv-filters');
    // If filter is not defined, then do not filter results.
    $filter = isset($filter) ? $filter : 'filterByNoFilter';
    $results = $csv_file
      ->stripBom(FALSE)
      ->addFilter($filter)
      ->fetchAssoc($header);
    return $results;
  }

  /**
   * Extract rows from the CSV File based on a filter.
   *
   * @param string $csv_file
   *   The CSV File to process the filter on.
   * @param string $filter
   *   The filter name to apply.
   * @param bool $to_utf8
   *   Whether to convert to file to UTF-8.
   *
   * @return mixed
   *   The Results.
   */
  protected function extractResourceRows($csv_file, $filter = NULL, $to_utf8 = FALSE) {
    $csv_file = Reader::createFromPath($csv_file);
    $headers = $csv_file->fetchOne();

    // The encoding of the file 'master-2.0.5\ Beta.csv' is WINDOWS-1250 but
    // we need UTF-8, then we convert to UTF-8.
    // Also, the InputBOM should return WINDOWS-1250 but it doesn't because it
    // cannot detect the encoding but we know it is that one so we just force
    // it to change.
    $input_bom = $csv_file->getInputBOM();
    if ($input_bom === '' && $to_utf8) {
      $csv_file->appendStreamFilter('convert.iconv.WINDOWS-1250/UTF-8');
    }

    module_load_include('inc', 'dss_engine', 'src/DSS/inc/sima_import_weap_databox_csv-filters');
    // If filter is not defined, then do not filter results.
    $filter = isset($filter) ? $filter : 'filterByNoFilter';
    $results = $csv_file
      ->stripBom(FALSE)
      ->addFilter($filter)
      ->fetchAssoc($headers);
    return $results;
  }

  /**
   * Extracts the Header from the CSV File.
   *
   * We Assume the header is contained in the first initial row of the CSV. If
   * it doesn't, please insert the header row index.
   *
   * @param string $csv_file
   *   The CSV File to extract the data from.
   * @param int $offset
   *   The row index where we should extract the CSV header.
   *
   * @return array
   *   The header array.
   */
  protected function extractHeader($csv_file, $offset = 0) {
    $reader = Reader::createFromPath($csv_file);
    $headers = $reader->fetchOne($offset);

    // Make sure the headers are UTF-8 encoded and not empty.
    foreach ($headers as $key => $header) {
      if (empty($header)) {
        $headers[$key] = "column_{$key}";
      }
      else {
        $headers[$key] = utf8_encode($header);
      }
    }

    return $headers;
  }

}
