<?php
/**
 * @file
 * Imports Projects from the imported resources (Cajas de Datos).
 */

namespace Drupal\dss_magdalena\DSS\Utils;

use Drupal\dss_magdalena\DSS\Entity\Ficha as Ficha;
use Drupal\dss_magdalena\DSS\SimaWeapIndex;
use Drupal\dss_magdalena\DSS\SimaWeapMasterFileExcel;
use League\Csv\Reader;
use League\Csv\Writer;
use SplTempFileObject;

/**
 * Class SimaImportProjects.
 *
 * @package Drupal\dss_magdalena\DSS\Utils
 */
class SimaImportProjects extends SimaUtils implements SimaImportProjectInterface {

  const WEAP_MASTER_DIR               = "public://weap_files";
  const KEY_ASSUMPTIONS_VARIABLE      = 'dss_engine_weap_masterfile_key_assumptions_fid';

  // Project Types.
  const TYPE_HYDROPOWER_PLANT_OR_DAM  = 1;
  const TYPE_ROR_HYDROPOWER_PLANT     = 2;
  const TYPE_WETLAND_MANAGEMENT_PROJ  = 3;
  const TYPE_PRODUCTIVE_LAND_USE_PROJ = 4;

  /**
   * Directories that should be initialized.
   *
   * @var array
   *   A list of directories.
   */
  protected $directories = [
    self::WEAP_MASTER_DIR,
  ];

  /**
   * The Header.
   *
   * @var array
   *   THe header.
   */
  protected $header = array(
    'BranchID',
    'GeoBranchId',
    'Year',
    'Timestep',
    'Value',
  );

  /**
   * Defines an empty ficha to determine the type of ficha we are importing.
   *
   * @var \Drupal\dss_magdalena\DSS\Entity\Ficha\SimaFichaInterface|NULL
   *   A decoy ficha object.
   */
  protected $fichaType = NULL;

  /**
   * Public constructor.
   *
   * @param int $type
   *   The Project type.
   * @param int $autodetect_line_endings
   *   Pass 1 to autodetect line endings, 0 otherwise.
   */
  public function __construct($type = 1, $autodetect_line_endings = 1) {
    // Initialize directories.
    $this->initializeDirectories();

    $this->setAutodetectLineEndings($autodetect_line_endings);
    switch ($type) {
      case self::TYPE_HYDROPOWER_PLANT_OR_DAM:
        $this->fichaType = Ficha\SimaFichaHydropowerPlantDam::newFakeFicha();
        break;

      case self::TYPE_ROR_HYDROPOWER_PLANT:
        $this->fichaType = Ficha\SimaFichaRorHydropowerPlant::newFakeFicha();
        break;

      case self::TYPE_WETLAND_MANAGEMENT_PROJ:
        $this->fichaType = Ficha\SimaFichaWetlandsManagement::newFakeFicha();
        break;

      case self::TYPE_PRODUCTIVE_LAND_USE_PROJ:
        $this->fichaType = Ficha\SimaFichaProductiveLandUse::newFakeFicha();
        break;

    }
  }

  /**
   * Returns the target bundle.
   *
   * @return string
   *   The target bundle.
   */
  public function getTargetBundle() {
    return $this->fichaType->getDrupalBundle();
  }

  /**
   * Returns the directory for 'resources' CSV files.
   *
   * @return string
   *   The Resources CSV Directory.
   */
  protected function getResourcesCsvDirectory() {
    $resources_directory = drupal_get_path('module', 'dss_import') . '/import/resources/';
    return $resources_directory;
  }

  /**
   * Obtains the relationships between Datasets/WEAP Variables.
   *
   * @return array
   *   An array of Datasets / WEAP Variables.
   */
  protected function getDatasetsWeap() {
    return $this->fichaType->getDatasetsWeap();
  }

  /**
   * Obtains the Reference Dataset.
   *
   * @return mixed
   *   The name of the Reference Dataset.
   */
  protected function getReferenceDataset() {
    return $this->fichaType->getReferenceDataset();
  }

  /**
   * Obtains the Drupal variable name to store fichas of this type.
   *
   * @return string
   *   Returns a drupal variable name to store the fichas.
   */
  protected function getVariableListOfItems() {
    $bundle = $this->fichaType->getDrupalBundle();
    return 'dss_engine_ficha_' . $bundle . '_items';
  }

  /**
   * Obtains the CSV File variable name to story fichas of this type.
   *
   * @return string
   *   Returns a drupal variable name to store the filename.
   */
  protected function getVariableCsvFileName() {
    $bundle = $this->fichaType->getDrupalBundle();
    return 'dss_engine_ficha_' . $bundle . '_csv_file_fid';
  }

  /**
   * Obtains the key Assumptions.
   */
  public function extractKeyAssumptions() {
    $fid = variable_get(self::KEY_ASSUMPTIONS_VARIABLE, FALSE);
    if ($fid === FALSE) {
      // Obtain lines for Key Assumptions.
      $master_file = SimaWeapMasterFileExcel::getWeapMasterFileCsv();
      $master_file_csv = drupal_realpath($master_file->uri);
      $results = $this->extractRows('filterByKeyAssumptions', $master_file_csv, TRUE);
      $key_assumptions = iterator_to_array($results, FALSE);

      // Initialize output CSV file.
      $csv_output = Writer::createFromFileObject(new SplTempFileObject());

      // Insert the header.
      $csv_output->insertAll($key_assumptions);

      // Save Key Assumptions.
      $key_assumptions_uri = self::WEAP_MASTER_DIR . '/key_assumptions.csv';

      if ($key_assumptions_file = file_save_data($csv_output, $key_assumptions_uri, FILE_EXISTS_REPLACE)) {
        variable_set(self::KEY_ASSUMPTIONS_VARIABLE, $key_assumptions_file->fid);
        return $key_assumptions_file->fid;
      }
      return FALSE;
    }
    return $fid;
  }

  /**
   * Extracts the last row from the CSV File based on a filter.
   *
   * @param string $filter
   *   The filter name to apply.
   * @param string $csv_file
   *   The CSV File to process the filter on.
   *
   * @return mixed
   *   The Results.
   */
  protected function extractLastRow($filter, $csv_file) {
    $master_file = Reader::createFromPath($csv_file);
    module_load_include('inc', 'dss_engine', 'src/DSS/inc/sima_import_weap_databox_csv-filters');
    // If filter is not defined, then do not filter results.
    $filter = isset($filter) ? $filter : 'filterByNoFilter';
    $results = $master_file
      ->stripBom(FALSE)
      ->addFilter($filter)
      ->fetchAssoc($this->header);

    // Only provide the last row.
    $rows = iterator_to_array($results, FALSE);
    $result = isset($rows[count($rows) - 1]) ? $rows[count($rows) - 1] : [];
    return $result;
  }

  /**
   * Gets all the list of items (Fichas) to search for.
   *
   * This could return a list of Hydropower plants, dams, wetlands management
   * projects, agricultural development projects, etc. This list will later
   * be used to search for projects of this type.
   *
   * @return array
   *   An array of all the fichas found.
   */
  public function getListOfItems() {
    // Load all stored WEAP Branches.
    /** @var \Drupal\dss_magdalena\DSS\SimaWeapIndex $weap */
    $weap = SimaWeapIndex::loadAllRecords();

    $reference_dataset = $this->getReferenceDataset();
    $variable_items = $this->getVariableListOfItems();

    // Variable we will take the number of fichas.
    $items = variable_get($variable_items, array());
    if (count($items) <= 0) {

      // Reading CSV File.
      $csv_file = $this->getResourcesCsvDirectory() . "{$reference_dataset}.csv";
      $reader = Reader::createFromPath($csv_file);
      module_load_include('inc', 'dss_engine', 'src/DSS/inc/sima_import_weap_databox_csv-filters');
      $filter = 'filterByNoFilter';
      $results = $reader
        ->stripBom(FALSE)
        ->addFilter($filter)
        ->fetchAssoc($this->header);
      foreach ($results as $row) {
        if (array_keys($row) == array_values($row)) {
          continue;
        }

        $items[] = $row;
      }
      $items = $weap->combineWith($items, 'branch_id');

      // Now store this variable.
      variable_set($variable_items, $items);
    }
    return $items;
  }


  /**
   * Generates the Projects CSV File.
   *
   * This joined CSV File is generated by merging all CSV files from all
   * resources (cajas de datos) that relate to Projects.
   *
   * @param bool $regenerate
   *   TRUE if we want to regenerate the file, FALSE otherwise.
   *
   * @return object|bool
   *   The File object if succeeds, FALSE otherwise.
   */
  public function generateProjectCsvFile($regenerate = FALSE) {
    /** @var \Drupal\dss_magdalena\DSS\SimaWeapIndex $weap */
    $weap = SimaWeapIndex::loadAllRecords();
    $datasets_variables = $this->getDatasetsWeap();
    $variable_csv_file = $this->getVariableCsvFileName();

    // Has it been generated before?
    $fid = variable_get($variable_csv_file, FALSE);

    if ($fid === FALSE || $regenerate) {

      // Only proceed to generate file if we can initialize the directory.
      if ($this->createLocalDirectory(self::WEAP_MASTER_DIR)) {

        // Setting up a persistent variable.
        dss_engine_persistent_variable($this->header);

        // Initialize output CSV file.
        $csv_output = Writer::createFromFileObject(new SplTempFileObject());

        // Insert the header.
        $csv_output->insertOne($this->header);

        foreach (array_keys($datasets_variables) as $dataset) {
          $variable_name = $datasets_variables[$dataset];
          $filter = 'filterByStrippingOneRecord';
          $csv_file = $this->getResourcesCsvDirectory() . "{$dataset}.csv";
          // $file_url = file_create_url($csv_file);
          if (file_exists($csv_file)) {
            $results = $this->extractRows($filter, $csv_file);
            $results = iterator_to_array($results, FALSE);
            $rows = $weap->combineWith($results, 'branch_id');

            // Obtaining last row per each branch ID.
            $last_rows = [];
            foreach ($rows as $key => $row) {
              $row['Variable'] = $variable_name;
              $next = isset($rows[$key + 1]) ? $rows[$key + 1] : ['BranchID' => 0];
              if ($row['BranchID'] !== $next['BranchID']) {

                // Adding the dataset to the lines coming.
                $last_rows[] = $row;
              }
            }
            $csv_output->insertAll($last_rows);
          }
          else {
            watchdog('dss_engine', 'We could not find file %file when generating project type %type for WEAP Variable %weap.', array(
              '%file' => $csv_file,
              '%type' => $this->fichaType->getDrupalEntityType(),
              '%weap' => $datasets_variables[$dataset],
            ), WATCHDOG_ERROR);
          }
        }

        // Saving the CSV File.
        $csv_file = self::WEAP_MASTER_DIR . "/{$variable_csv_file}.csv";
        $file = file_save_data($csv_output, $csv_file, FILE_EXISTS_REPLACE);

        // Saving fid in a variable.
        variable_set($variable_csv_file, $file->fid);

        // Resetting the persistent variable.
        dss_engine_persistent_variable(NULL);

        return isset($file->fid) ? $file->fid : FALSE;
      }
    }
    return $fid;
  }

  /**
   * Imports a Ficha Project.
   *
   * @param array $project
   *   The project data.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Ficha\SimaFichaInterface|bool
   *   The SimaFicha or FALSE.
   */
  public function importFichaProject($project) {

    // Setting up a persistent variable.
    dss_engine_persistent_variable($project);

    // Capture Data in the Ficha Array.
    $ficha_data = array();

    // Obtaining records for this variable.
    $variable_csv_file = $this->getVariableCsvFileName();
    $fid = variable_get($variable_csv_file, FALSE);
    $csv_file = file_load($fid);
    if ($csv_file !== FALSE) {
      $csv_filepath = drupal_realpath($csv_file->uri);
      // Get CSV File.
      $filter = 'filterByLevelsLow';
      $header = [
        'Branch',
        'level1',
        'level2',
        'level3',
        'level4',
        'BranchID',
        'GeoBranchId',
        'Year',
        'Timestep',
        'Value',
        'Variable',
      ];
      $results = $this->extractRowsFromFile($filter, $csv_filepath, $header);
      $ficha_data = iterator_to_array($results);

      // Creating a new SimaFicha for the specified type.
      $ficha = $this->fichaType->newFicha($project, $ficha_data);

      // Resetting the persistent variable.
      dss_engine_persistent_variable(NULL);

      return $ficha;
    }
    else {
      watchdog('dss_engine', 'We could not import project %project. File %filename was not found.', array(
        '%project' => implode('\\', $project),
        '%file' => self::WEAP_MASTER_DIR . "/{$variable_csv_file}.csv",
      ), WATCHDOG_ERROR);
      return FALSE;
    }
  }

}
