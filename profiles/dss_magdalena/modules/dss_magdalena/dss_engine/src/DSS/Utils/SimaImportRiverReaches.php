<?php
/**
 * @file
 * Imports Sima River Reaches.
 */

namespace Drupal\dss_magdalena\DSS\Utils;

use Drupal\dss_magdalena\DSS\SimaWeapMasterFileExcel;
use Drupal\dss_magdalena\DSS\Entity\SimaRiverReaches;
use Drupal\dss_magdalena\DSS\Entity\SimaLayer;
use EntityMetadataWrapperException;

/**
 * Implements import of River Reaches.
 *
 * Class SimaImportRiverReaches.
 *
 * @package Drupal\dss_magdalena\DSS\Utils
 */
class SimaImportRiverReaches extends SimaUtils {

  const DIRECTORY                        = 'public://weap_files';
  const RIVER_REACHES_EXTRACTED_RECORDS  = 'dss_engine_river_reaches_records';
  const RIVER_REACHES_LAYER              = 'river_reaches';
  const VAR_GEOJSON_FEATURES             = 'dss_engine_river_reaches_geojson_features';

  /**
   * Public constructor.
   *
   * @param int $autodetect_line_endings
   *   Autodetect line endings.
   */
  public function __construct($autodetect_line_endings = 1) {
    $this->setAutodetectLineEndings($autodetect_line_endings);
  }

  /**
   * Loads the River Reaches GeoJson into a FeatureCollection object.
   *
   * @return bool|\GeoJson\Feature\FeatureCollection
   *   A loaded GeoJson FeatureCollection object if exists, FALSE otherwise.
   */
  protected function getLoadedGeoJson() {
    if ($layer = SimaLayer::loadByMachineName(self::RIVER_REACHES_LAYER)) {
      $json = $layer->getOriginalGeoJson();
      $json = json_decode($json);
      $geojson = \GeoJson\GeoJson::jsonUnserialize($json);
      return $geojson;
    }
    return FALSE;
  }

  /**
   * Returns an array of GeoJson Features keyed by Branch full name.
   *
   * @return array
   *   An array of GeoJson Features keyed by branch.
   */
  public function getGeoJsonFeatures() {
    $json_features = variable_get(self::VAR_GEOJSON_FEATURES, []);
    if (count($json_features) == 0) {
      if ($geojson = $this->getLoadedGeoJson()) {
        foreach ($geojson->getFeatures() as $feature) {
          $properties = $feature->getProperties();
          $branch = isset($properties['Branch']) ? $properties['Branch'] : FALSE;
          if ($branch) {
            $json_field = $feature->jsonSerialize();
            $json_features[$branch] = json_encode($json_field);
          }
        }
        variable_set(self::VAR_GEOJSON_FEATURES, $json_features);
      }
    }
    return $json_features;
  }

  /**
   * Extracts River Reaches from the Master File.
   *
   * @return array
   *   An array of river reaches records from the master file.
   */
  public function extractObjects() {
    $river_reaches = variable_get(self::RIVER_REACHES_EXTRACTED_RECORDS, FALSE);
    if ($river_reaches === FALSE) {
      // Obtain lines for Key Assumptions.
      $master_file = SimaWeapMasterFileExcel::getWeapMasterFileCsv();
      $master_file_csv = drupal_realpath($master_file->uri);
      $results = $this->extractRows('filterByRiverReaches', $master_file_csv, TRUE);
      $rows = iterator_to_array($results, FALSE);

      foreach ($rows as $row) {
        $branch_id = $row['BranchID'];
        if (!isset($river_reaches[$branch_id])) {
          $river_reaches[$branch_id] = $row;
        }
      }
      variable_set(self::RIVER_REACHES_EXTRACTED_RECORDS, $river_reaches);
    }
    return $river_reaches;
  }


  /**
   * Imports River Reaches Content Types.
   *
   * @param array $objects
   *   The list of river reaches.
   * @param array $geojson_features
   *   An array of GeoJson Features keyed by Branch full name.
   * @param int $key
   *   The Branch ID or key.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\SimaRiverReaches|FALSE
   *   The SimaRiverReaches object if it can be imported, FALSE otherwise.
   */
  public function import($objects = [], $geojson_features = [], $key = NULL) {
    if ($key === NULL) {
      return FALSE;
    }
    // Creates a new River reach for only this record.
    $river_reach = isset($objects[$key]) ? $objects[$key] : [];

    // Obtaining the GeoJson feature field.
    $branch_key = sprintf("%s\\%s\\%s\\%s", $river_reach['Level 1'], $river_reach['Level 2'], $river_reach['Level 3'], $river_reach['Level 4...']);
    $geojson_field = isset($geojson_features[$branch_key]) ? $geojson_features[$branch_key] : '';

    // Converting GeoJson to WKT.
    if (empty($geojson_field)) {
      $geojson_field_wkt = NULL;
    }
    else {
      module_load_include('inc', 'geophp', 'geoPHP/geoPHP');
      $geom = \geoPHP::load($geojson_field, 'json');
      $geojson_field_wkt = $geom->out('wkt');
    }

    try {
      $sima_river_reach = SimaRiverReaches::fromMasterfileRecord($river_reach, $geojson_field_wkt);
      $sima_river_reach->save();
      return $sima_river_reach;
    }
    catch (EntityMetadataWrapperException $ex) {
      return FALSE;
    }
  }

}
