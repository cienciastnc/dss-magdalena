<?php
/**
 * @file
 * Provides some common functions for Import/Export.
 */

namespace Drupal\dss_magdalena\DSS\Utils;

use League\Csv\Reader;

/**
 * SimaUtils base class.
 *
 * Class SimaUtils.
 *
 * @package Drupal\dss_magdalena\DSS\Utils
 */
abstract class SimaUtils {

  use SimaDirectoriesTrait;

  /**
   * Directories that should be initialized.
   *
   * @var array
   */
  protected $directories = [];

  /**
   * The Header.
   *
   * @var array
   */
  protected $header = [
    'BranchID',
    'GeoBranchId',
    'Year',
    'Timestep',
    'Value',
  ];

  /**
   * This is the header that should be used to read the master file.
   *
   * @var array
   */
  protected $masterfileHeader = [
    'BranchID',
    'VariableID',
    'ScenarioID',
    'Level 1',
    'Level 2',
    'Level 3',
    'Level 4...',
    'Variable',
    'Scenario',
    'Unit',
    'Expression',
  ];

  /**
   * A flag to set the system to autodetect line endindgs.
   *
   * @param int $autodetect
   *   Autodetect: 1 TRUE, 0 FALSE.
   */
  protected function setAutodetectLineEndings($autodetect = 1) {
    if ((bool) $autodetect) {
      ini_set("auto_detect_line_endings", '1');
    }
    else {
      ini_set("auto_detect_line_endings", '0');
    }
  }

  /**
   * Initializes local directories.
   *
   * Creates the local directories that will store the import
   * master Databox CSV files.
   */
  public function initializeDirectories() {
    foreach ($this->directories as $directory) {
      if ($this->createLocalDirectory($directory) === FALSE) {
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Extract rows from a Resource CSV File or WEAP Masterfile based on a filter.
   *
   * @param string $filter
   *   The filter name to apply.
   * @param string $csv_file
   *   The CSV File to process the filter on.
   * @param bool $is_masterfile
   *   TRUE if we are reading from the mater file, FALSE otherwise.
   *
   * @return mixed
   *   The Results.
   */
  protected function extractRows($filter, $csv_file, $is_masterfile = FALSE) {
    $master_file = Reader::createFromPath($csv_file);

    $input_bom = $master_file->getInputBOM();

    // The encoding of the file 'master-2.0.5\ Beta.csv' is WINDOWS-1250 but
    // we need UTF-8, then we convert to UTF-8.
    // Also, the InputBOM should return WINDOWS-1250 but it doesn't because it
    // cannot detect the encoding but we know it is that one so we just force
    // it to change.
    if ($input_bom === '' && $is_masterfile) {
      $master_file->appendStreamFilter('convert.iconv.WINDOWS-1250/UTF-8');
    }

    module_load_include('inc', 'dss_engine', 'src/DSS/inc/sima_import_weap_databox_csv-filters');
    $header = $is_masterfile ? $this->masterfileHeader : $this->header;
    // If filter is not defined, then do not filter results.
    $filter = isset($filter) ? $filter : 'filterByNoFilter';
    $results = $master_file
      ->stripBom(FALSE)
      ->addFilter($filter)
      ->fetchAssoc($header);
    return $results;
  }


  /**
   * Extract rows from the CSV File based on a filter.
   *
   * @param string $filter
   *   The filter name to apply.
   * @param string $csv_file
   *   The CSV File to process the filter on.
   * @param array $header
   *   The header to extract data from.
   *
   * @return mixed
   *   The Results.
   */
  protected function extractRowsFromFile($filter, $csv_file, array $header = []) {
    $master_file = Reader::createFromPath($csv_file);

    $input_bom = $master_file->getInputBOM();

    // The encoding of the file 'master-2.0.5\ Beta.csv' is WINDOWS-1250 but
    // we need UTF-8, then we convert to UTF-8.
    // Also, the InputBOM should return WINDOWS-1250 but it doesn't because it
    // cannot detect the encoding but we know it is that one so we just force
    // it to change.
    if ($input_bom === '') {
      $master_file->appendStreamFilter('convert.iconv.WINDOWS-1250/UTF-8');
    }

    module_load_include('inc', 'dss_engine', 'src/DSS/inc/sima_import_weap_databox_csv-filters');
    $header = !empty($header) ? $header : $this->header;
    // If filter is not defined, then do not filter results.
    $filter = isset($filter) ? $filter : 'filterByNoFilter';
    $results = $master_file
      ->stripBom(FALSE)
      ->addFilter($filter)
      ->fetchAssoc($header);
    return $results;
  }


  /**
   * Extracts the CSV Data for a specific dataset and returns it as an array.
   *
   * @param string $dataset_machine_name
   *   The dataset machine name.
   * @param string $filter
   *   The filter to use to filter the results.
   *
   * @return array
   *   An array of the data coming from the CSV, keyed by its headers.
   */
  protected function extractDatasetCsv($dataset_machine_name, $filter = 'filterByNoFilter') {
    $csv_file = drupal_get_path('module', 'dss_import') . '/import/resources/' . $dataset_machine_name . '.csv';
    $dataset_file = Reader::createFromPath($csv_file);
    module_load_include('inc', 'dss_engine', 'src/DSS/inc/sima_import_weap_databox_csv-filters');
    $headers = $dataset_file->fetchOne();

    // If filter is not defined, then do not filter results.
    $results = $dataset_file
      ->setOffset(1)
      ->addFilter($filter)
      ->fetchAssoc($headers);
    $rows = iterator_to_array($results, FALSE);
    return $rows;
  }

}
