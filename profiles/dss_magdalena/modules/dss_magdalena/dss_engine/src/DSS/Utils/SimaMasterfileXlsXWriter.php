<?php
/**
 * @file
 * Writes data to an Excel Masterfile in XLSX format.
 */

namespace Drupal\dss_magdalena\DSS\Utils;

use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Style_Fill;

trait SimaMasterfileXlsXWriter {

  /**
   * The Excel Spreadsheet.
   *
   * @var PHPExcel
   */
  protected $excel;

  /**
   * Saves a Master file in XLSX format.
   *
   * @param array $data
   *   The data array.
   * @param string $destination
   *   The destination file URI.
   * @param string $case_title
   *   The Case Study title we are creating the masterfile for.
   *
   * @return bool|string
   *   The File Real destination if succeeds, FALSE otherwise.
   *
   * @throws \PHPExcel_Exception
   *   Throws error if it cannot be saved.
   * @throws \PHPExcel_Reader_Exception
   *   Throws error if it cannot be saved.
   */
  public function saveMasterFileXlsx($data, $destination, $case_title) {
    $this->init($case_title);

    // Create first worksheet.
    $ws = $this->excel->getSheet(0);
    $ws->setTitle('Sheet1');

    // Add Headers.
    $ws->setCellValue('D1', 'WEAP Area:');
    $model_version = variable_get('weap_master_model_version', 'MAGDALENA_V2.1.6.1');
    $ws->setCellValue('E1', $model_version);
    $ws->fromArray($this->getHeaders(), ' ', 'A3');

    // Add Data.
    $ws->fromArray($data, ' ', 'A4');

    // Format headers.
    $header = 'd1:q3';
    $ws->getStyle($header)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00a4a4a4');
    $style = array(
      'font' => array('bold' => TRUE),
    );
    $ws->getStyle($header)->applyFromArray($style);

    // Hide first 3 columns.
    $ws->getColumnDimension('A')->setVisible(FALSE);
    $ws->getColumnDimension('B')->setVisible(FALSE);
    $ws->getColumnDimension('C')->setVisible(FALSE);

    // Adjust width to fit the maximum displayed length in each visible column.
    for ($col = ord('D'); $col <= ord('K'); $col++) {
      $ws->getColumnDimension(chr($col))->setAutoSize(TRUE);
    }

    // Save Excel file.
    try {
      $writer = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
      $real_destination = drupal_realpath($destination);
      $writer->save($real_destination);
      return $real_destination;
    }
    catch (\PHPExcel_Writer_Exception $ex) {
      watchdog('dss_engine', 'There was an error trying to save Masterfile "!file" for Case "!case" (!code: "!error", Line: !line)', array(
        '!file' => $ex->getFile(),
        '!case' => $case_title,
        '!code' => $ex->getCode(),
        '!error' => $ex->getMessage(),
        '!line' => $ex->getLine(),
      ), WATCHDOG_ERROR);
      drupal_set_message(t('There was an error trying to save Masterfile "!file" for Case "!case" (!code: "!error", Line: !line)', array(
        '!file' => $ex->getFile(),
        '!case' => $case_title,
        '!code' => $ex->getCode(),
        '!error' => $ex->getMessage(),
        '!line' => $ex->getLine(),
      )), 'error');
    }
    return FALSE;
  }

  /**
   * Initializes the Excel Spreadsheet.
   *
   * @param string $case_title
   *   The Case Study title.
   */
  protected function init($case_title) {
    global $user;
    $title = t('SIMA Master file for Case Study "!case"', array(
      '!case' => $case_title,
    ));
    $description = $title;
    $subject = $title;

    // Initializing Excel File.
    $this->excel = new PHPExcel();
    $this->excel->getProperties()
      ->setCreator('Produced by: SIMA-DSS (TNC)')
      ->setTitle($title)
      ->setLastModifiedBy($user->name)
      ->setDescription($description)
      ->setSubject($subject)
      ->setKeywords('excel php office sima dss tnc')
      ->setCategory('dss');
  }

  /**
   * Defines the Masterfile Headers.
   *
   * @return array
   *   The header array.
   */
  protected function getHeaders() {
    return [
      'BranchID',
      'VariableID',
      'ScenarioID',
      'Level 1',
      'Level 2',
      'Level 3',
      'Level 4...',
      'Variable',
      'Scenario',
      'Unit',
      'Expression',
    ];
  }

}
