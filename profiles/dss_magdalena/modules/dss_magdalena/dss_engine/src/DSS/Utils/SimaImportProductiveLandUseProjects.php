<?php
/**
 * @file
 * Import of Productive Land Use Projects.
 */

namespace Drupal\dss_magdalena\DSS\Utils;

use Drupal\dss_magdalena\DSS\SimaWeapMasterFileExcel;

/**
 * Imports Productive Land Use Projects.
 *
 * Agricultural Projects have their own logic to extract results.
 *
 * Class SimaImportProductiveLandUseProjects
 *
 * @package Drupal\dss_magdalena\DSS\Utils
 */
class SimaImportProductiveLandUseProjects extends SimaImportProjects implements SimaImportProjectInterface {

  const VAR_AREA            = 'Area';
  const VAR_IRRIGATED_AREA  = 'Irrigated Area';
  const VAR_DEMAND_PRIORITY = 'Demand priority';
  const VAR_LOWER_THRESHOLD = 'Lower threshold';
  const VAR_UPPER_THRESHOLD = 'Upper threshold';


  /**
   * Gets all the list of items (Fichas) to search for.
   *
   * This could return a list of agricultural development projects.
   * This list will later be used to search for projects of this type.
   *
   * @return array
   *   An array of all the fichas found.
   */
  public function getListOfItems() {
    // Becase at this moment we are creating agricultural projects one per
    // catchment, then we will have to obtain the complete list of catchments
    // and set them up like one per catchment.
    $variable_items = $this->getVariableListOfItems();

    // Variable we will take the number of fichas.
    $items = variable_get($variable_items, array());
    if (count($items) <= 0) {

      // Adding Catchment Area.
      $rows = $this->extractDatasetCsv('dss_area_subcuenca', 'filterByYear1981');
      foreach ($rows as $row) {
        // $name = $row['Level 1'] . '\\' . $row['Level 2'];.
        $name = $row['Level 2'];
        $value = !empty($row['Value']) ? $row['Value'] : 0;
        $items[$name]['area'] = $value;
        $items[$name]['irrigated_area'] = NULL;
        $items[$name]['demand_priority'] = NULL;
        $items[$name]['transmission_link'] = FALSE;
        $items[$name]['max_flow_value'] = FALSE;
      }

      // Adding land type percentage of share.
      $rows = $this->extractDatasetCsv('dss_uso_suelo_porc_simulacion', 'filterByYear1981');
      foreach ($rows as $row) {
        $name = $row['Level 2'];
        $branch_id = $row['BranchID'];
        $items[$name][$branch_id] = $row;
      }

      // Adding Demand Priority.
      $rows = $this->extractDatasetCsv('dss_prioridad_usuario_riego', 'filterByYear1981');
      foreach ($rows as $row) {
        $name = $row['Level 2'];
        $demand_priority = $row['Value'];
        if (!empty($demand_priority)) {
          $items[$name]['demand_priority'] = $demand_priority;
        }
      }

      // Adding Irrigated Area.
      $irrigated_areas = [];
      $rows = $this->extractDatasetCsv('dss_porcentaje_regadio', 'filterByYear1981');
      foreach ($rows as $row) {
        if ($row['Level 3'] === 'Agricola') {
          // $name = $row['Level 1'] . '\\' . $row['Level 2'];.
          $name = $row['Level 2'];
          $irrigated_area = $row['Value'];
          $items[$name]['irrigated_area'] = $irrigated_area;
          if (intval($irrigated_area) == 100) {
            $irrigated_areas[] = $name;
          }
        }
      }

      // Searching if any of the irrigated areas is a destination of any of the
      // transmission links.
      $master_file = SimaWeapMasterFileExcel::getWeapMasterFileCsv();
      $master_file_csv = drupal_realpath($master_file->uri);
      $results = $this->extractRows('filterByTransmissionLinks', $master_file_csv, TRUE);
      $transmission_links = iterator_to_array($results, FALSE);
      foreach ($transmission_links as $transmission_link) {
        $destination = str_replace('to ', '', $transmission_link['Level 3']);

        // Checking if catchment is the destination of a transmission link.
        if (in_array($destination, $irrigated_areas)) {
          $items[$destination]['transmission_link'] = TRUE;
        }

        // Checking the max flow volume.
        if ($transmission_link['Variable'] === 'Maximum Flow   Volume' && isset($items[$destination])) {
          $items[$destination]['max_flow_value'] = $transmission_link['Expression'];
        }
      }

      // Now store this variable.
      variable_set($variable_items, $items);
    }
    return $items;
  }

  /**
   * Generates projects CSV file.
   *
   * @inheritdoc
   */
  public function generateProjectCsvFile($regenerate = FALSE) {
    // Do nothing.
  }

  /**
   * Imports a Ficha Project.
   *
   * @param array $project
   *   The project data.
   *
   * @return \Drupal\dss_magdalena\DSS\Entity\Ficha\SimaFichaInterface|bool
   *   The SimaFicha or FALSE.
   */
  public function importFichaProject($project) {

    if (!empty($project)) {
      foreach ($project as $item) {
        if (is_array($item) && isset($item['Level 2'])) {
          $project['name'] = $item['Level 2'];
          break;
        }
      }
      $ficha_data = array();
      $ficha = $this->fichaType->newFicha($project, $ficha_data);
      return $ficha;
    }
    else {
      watchdog('dss_engine', 'We could not import project. Data given was empty.', array(), WATCHDOG_ERROR);
      return FALSE;
    }
  }

  /**
   * Checks if there is enough data in the ficha to create a project.
   *
   * @param array $ficha_data
   *   The rows obtained from the master file.
   *
   * @return bool
   *   TRUE if there is not enough data, FALSE otherwise.
   */
  protected function notEnoughFichaData($ficha_data) {
    if (count($ficha_data) <= 1) {
      $ficha_data_element = reset($ficha_data);
      if ($ficha_data_element['Variable'] == 'Area' && empty($ficha_data_element['Level 3'])) {
        return TRUE;
      }
    }
    return FALSE;
  }

}
