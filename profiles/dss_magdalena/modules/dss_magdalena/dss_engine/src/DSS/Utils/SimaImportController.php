<?php
/**
 * @file
 * Implements a Controller for SimaImportProjects.
 */

namespace Drupal\dss_magdalena\DSS\Utils;

/**
 * Controller for loading the correct class for SimaImportProjects.
 *
 * Class SimaImportController.
 *
 * @package Drupal\dss_magdalena\DSS\Utils
 */
class SimaImportController {

  /**
   * Loads the correct class for Importing Projects according to type.
   *
   * @param string $type
   *   The Project type.
   *
   * @return \Drupal\dss_magdalena\DSS\Utils\SimaImportProjectInterface
   *   The SimaImportProject class.
   */
  static public function load($type) {
    switch ($type) {
      case SimaImportProjects::TYPE_HYDROPOWER_PLANT_OR_DAM:
      case SimaImportProjects::TYPE_ROR_HYDROPOWER_PLANT:
        return new SimaImportProjects($type);

      case SimaImportProjects::TYPE_WETLAND_MANAGEMENT_PROJ:
        return new SimaImportWetlandManagementProjects($type);

      case SimaImportProjects::TYPE_PRODUCTIVE_LAND_USE_PROJ:
        return new SimaImportProductiveLandUseProjects($type);

      default:
        return new SimaImportProjects($type);
    }
  }

}
