<?php
/**
 * @file
 * A helper method to autodetect line endings.
 *
 * This method changes the PHP.INI configuration to enable autodetection of
 * line endings. Useful for MAC-OSX.
 */

namespace Drupal\dss_magdalena\DSS\Utils;

/**
 * Trait SimaAutodetectLineEndingsTrait.
 *
 * @package Drupal\dss_magdalena\DSS\Utils
 */
trait SimaAutodetectLineEndingsTrait {

  /**
   * A flag to set the system to autodetect line endindgs.
   *
   * @param int $autodetect
   *   Autodetect: 1 TRUE, 0 FALSE.
   */
  protected function setAutodetectLineEndings($autodetect = 1) {
    if ((bool) $autodetect) {
      ini_set("auto_detect_line_endings", '1');
    }
    else {
      ini_set("auto_detect_line_endings", '0');
    }
  }

}
