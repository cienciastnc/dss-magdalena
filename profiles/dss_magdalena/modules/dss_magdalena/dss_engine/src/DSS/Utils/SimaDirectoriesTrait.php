<?php
/**
 * @file
 * Defines functions for creating directories.
 */

namespace Drupal\dss_magdalena\DSS\Utils;

/**
 * Implements creation of directories.
 *
 * Trait SimaDirectoriesTrait.
 *
 * @package Drupal\dss_magdalena\DSS\Utils
 */
trait SimaDirectoriesTrait {

  /**
   * Creates a local directory.
   *
   * @param string $directory
   *   The local directory.
   *
   * @return bool
   *   TRUE if created successfully, FALSE otherwise.
   */
  protected function createLocalDirectory($directory) {
    if (!file_prepare_directory($directory, FILE_CREATE_DIRECTORY)) {
      return FALSE;
    }
    // file_create_htaccess($directory, TRUE);.
    return TRUE;
  }

  /**
   * Creates all the directories in the path to make sure the file can be saved.
   *
   * @param string $destination
   *   A string containing the destination URI. This must be a stream wrapper
   *   URI. If no value is provided, a randomized name will be generated and
   *   the file will be saved using Drupal's default files scheme, usually
   *   "public://".
   *
   * @return bool
   *   TRUE if the directories were created, FALSE otherwise.
   */
  protected function createPathToFile($destination) {
    if (!file_valid_uri($destination)) {
      watchdog('file', 'The data could not be saved because the destination %destination is invalid. This may be caused by improper use of file_save_data() or a missing stream wrapper.', array('%destination' => $destination));
      drupal_set_message(t('The data could not be saved, because the destination is invalid. More information is available in the system log.'), 'error');
      return FALSE;
    }

    $path = pathinfo($destination);
    $directory = $path['dirname'];
    list($scheme, $dirs) = explode('://', $directory);
    $directories = explode('/', $dirs);
    $path = $scheme . ':/';
    foreach ($directories as $directory) {
      $path = $path . '/' . $directory;
      $this->createLocalDirectory($path);
    }
    return TRUE;
  }

}
