<?php
/**
 * @file
 * Sima WEAP Index Class.
 *
 * Saves a table with information about all WEAP Branch IDs and their levels.
 */

namespace Drupal\dss_magdalena\DSS;

use Drupal\dss_magdalena\DSS\Utils\SimaCsvReaderTrait;
use PDO;

/**
 * Class SimaWeapIndex.
 *
 * @package Drupal\dss_magdalena\DSS
 */
class SimaWeapIndex {

  use SimaCsvReaderTrait;

  /**
   * The table name.
   */
  const TABLE       = 'dss_engine_weap_index';

  /**
   * The CSV Header.
   */
  const HEADER = [
    'branch_id',
    'level1',
    'level2',
    'level3',
    'level4',
  ];

  /**
   * The results array.
   *
   * @var array
   */
  protected $rows;

  /**
   * Public Constructor.
   *
   * @param array $rows
   *   An array of weap_levels.
   */
  public function __construct($rows = []) {
    if (is_array($rows)) {
      if (isset($rows['branch_id']) || isset($rows['level1'])) {
        $this->rows = [$rows];
      }
      else {
        $row = reset($rows);
        if (isset($row['branch_id']) || isset($row['level1'])) {
          $this->rows = $rows;
        }
        else {
          $this->rows = [];
        }
      }
    }
  }

  /**
   * Returns the CSV Real Path.
   *
   * @return false|string
   *   The real path to the CSV WEAP Index file or FALSE if it does not exist.
   */
  protected function getCsvFilePath() {
    $path = drupal_get_path('module', 'dss_import') . '/import/resources/index_weap.csv';
    return drupal_realpath($path);
  }

  /**
   * Returns the values of the object.
   *
   * @return array|mixed
   *   An array of results.
   */
  public function getValues() {
    if (count($this->rows) == 1) {
      return reset($this->rows);
    }
    return $this->rows;
  }

  /**
   * Sets the WEAP level values for the object.
   *
   * @param array $rows
   *   An array of values.
   *
   * @return $this
   *   This same object with values.
   */
  public function setValues($rows) {
    if (!empty($rows)) {
      if (isset($rows['branch_id']) || isset($rows['level1'])) {
        $this->rows = [$rows];
      }
      else {
        $this->rows = $rows;
      }
    }
    return $this;
  }

  /**
   * Loads the WEAP index.
   *
   * @param string $branch_id
   *   The Branch ID.
   *
   * @return bool|static
   *   The loaded class, or FALSE.
   */
  public static function loadByBranchId($branch_id, $include_branch = FALSE) {
    $query = db_select(self::TABLE, 'ci')->fields('ci');
    if ($include_branch) {
      // Get correct type of back slash string depending on the DB used.
      $slash_string = "\\";
      if (isset($GLOBALS['databases']['default']['default']['driver']) && ($GLOBALS['databases']['default']['default']['driver'] == 'mysql')) {
        $slash_string = "\\\\";
      }
      $query->addExpression("TRIM(TRAILING '{$slash_string}' FROM CONCAT(ci.level1, '{$slash_string}', ci.level2, '{$slash_string}', ci.level3, '{$slash_string}', ci.level4))", 'branch');
    }
    $result = $query->condition('branch_id', $branch_id)
      ->execute()
      ->fetchAssoc();

    if ($result) {
      return new static($result);
    }
    return FALSE;
  }
  /**
   * Loads the WEAP index.
   *
   * @param string $branch
   *   The full Branch name.
   *
   * @return bool|static
   *   The loaded class, or FALSE.
   */
  public static function loadByFullBranchName($branch) {
    // Get correct type of back slash string depending on the DB used.
    $slash_string = "\\";
    if (isset($GLOBALS['databases']['default']['default']['driver']) && ($GLOBALS['databases']['default']['default']['driver'] == 'mysql')) {
      $slash_string = "\\\\";
    }

    $query = db_select(self::TABLE, 'ci')->fields('ci');
    $query->addExpression("TRIM(TRAILING '{$slash_string}' FROM CONCAT(ci.level1, '{$slash_string}', ci.level2, '{$slash_string}', ci.level3, '{$slash_string}', ci.level4))", 'branch');
    $query->where("TRIM(TRAILING '{$slash_string}' FROM CONCAT(ci.level1, '{$slash_string}', ci.level2, '{$slash_string}', ci.level3, '{$slash_string}', ci.level4)) = '{$branch}'");
    $result = $query->execute();
    $result_data = $result->fetchAssoc();

    if ($result_data) {
      return new static($result_data);
    }
    return FALSE;
  }

  /**
   * Loads all the WEAP Branches stored in the database.
   *
   * @return bool|static
   *   The loaded SimaWeapIndex object, FALSE otherwise.
   */
  public static function loadAllRecords($include_branch = FALSE) {
    $query = db_select(self::TABLE, 'ci')->fields('ci');
    if ($include_branch) {
      // Get correct type of back slash string depending on the DB used.
      $slash_string = "\\";
      if (isset($GLOBALS['databases']['default']['default']['driver']) && ($GLOBALS['databases']['default']['default']['driver'] == 'mysql')) {
        $slash_string = "\\\\";
      }
      $query->addExpression("TRIM(TRAILING '{$slash_string}' FROM CONCAT(ci.level1, '{$slash_string}', ci.level2, '{$slash_string}', ci.level3, '{$slash_string}', ci.level4))", 'branch');
    }
    $result = $query->execute()->fetchAllAssoc('branch_id', PDO::FETCH_ASSOC);

    if ($result) {
      return new static($result);
    }
    return FALSE;
  }

  /**
   * Combines loaded WEAP records of branches with $data.
   *
   * @param array $data
   *   An array of data.
   * @param string $output_key
   *   The key to use for the output combined array.
   * @param int $offset
   *   The offset to obtain from the resulting array.
   * @param int $length
   *   The number of columns to obtain from the resulting array.
   * @param array $filter
   *   A filter to compare results to.
   *
   * @return array
   *   An array of combined data.
   */
  public function combineWith(array $data, $output_key = 'branch_id', $offset = 0, $length = NULL, $filter = []) {
    $output = [];

    $values = $this->getValues();
    foreach ($data as $row) {
      $branch_id = isset($row['BranchID']) ? $row['BranchID'] : (isset($row['branchid']) ? $row['branchid'] : NULL);
      foreach ($row as $key => $value) {
        $values[$branch_id][$key] = $value;
      }

      // If we are filtering results.
      if (!empty($filter)) {
        $validates = TRUE;
        foreach ($filter as $key => $val) {
          if ($values[$branch_id][$key] !== $val) {
            $validates = FALSE;
          }
        }

        // If filter does not validates, then do not take the result.
        if (!$validates) {
          continue;
        }
      }

      $key = $values[$branch_id][$output_key];
      $output[$key] = $values[$branch_id];

      $length = ($length > count($output[$key])) ? count($output[$key]) : $length;
      $output[$key] = array_slice($output[$key], $offset, $length, TRUE);
    }

    return $output;
  }


  /**
   * Combines loaded WEAP records of branches with $data.
   *
   * @param array $data
   *   An array of data.
   * @param array $fields
   *   An array with the list of fields to add to the data.
   * @param array $filter
   *   A filter to compare results to.
   *
   * @return array
   *   An array of combined data.
   */
  public function combineWithWeap(array $data, $fields = [
    'branch',
    'level1',
    'level2',
  ], $filter = []) {
    $output = [];
    foreach ($data as $i => $row) {
      $values = $this->getValues();
      $branch_id = isset($row['BranchID']) ? $row['BranchID'] : (isset($row['branchid']) ? $row['branchid'] : NULL);
      if (!is_numeric($branch_id)) {
        continue;
      }
      $value = $values[$branch_id];
      $skip = FALSE;
      foreach ($value as $key => $val) {

        // If we are filtering results.
        if (!empty($filter)) {
          $validates = TRUE;
          foreach ($filter as $filter_key => $filter_val) {
            if ($value[$filter_key] !== $filter_val) {
              $validates = FALSE;
              continue;
            }
          }

          // If filter does not validates, then do not take the result.
          if (!$validates) {
            $skip = TRUE;
            continue;
          }
        }
        // Filters have been applied.
        if (in_array($key, $fields)) {
          $row[$key] = $val;
        }
      }
      if (!$skip) {
        $output[] = $row;
      }
    }

    return $output;
  }

  /**
   * Loads the WEAP index.
   *
   * @param array $weap_levels
   *   The WEAP levels.
   *
   * @return bool|static
   *   The loaded class or FALSE.
   */
  public static function loadByWeapLevel($weap_levels) {
    $query = db_select(self::TABLE, 'ci')->fields('ci');
    if (!empty($weap_levels['level1'])) {
      $query = $query->condition('level1', $weap_levels['level1']);
    }
    if (!empty($weap_levels['level2'])) {
      $query = $query->condition('level2', $weap_levels['level2']);
    }
    if (!empty($weap_levels['level3'])) {
      $query = $query->condition('level3', $weap_levels['level3']);
    }
    if (!empty($weap_levels['level4'])) {
      $query = $query->condition('level4', $weap_levels['level4']);
    }
    $result = $query->execute()->fetchAllAssoc('branch_id', PDO::FETCH_ASSOC);
    return new static($result);
  }

  /**
   * Saves CSV Data into the database.
   */
  public function saveCsvData() {
    $weap_csv = $this->getCsvFilePath();
    $rows = $this->extractRows('filterByNoFilter', $weap_csv, self::HEADER);
    foreach ($rows as $key => $row) {
      // Skip header.
      if ($key === 0) {
        continue;
      }

      // Save data.
      $result = db_merge(self::TABLE)
        ->key([
          'branch_id' => $row['branch_id'],
        ])
        ->fields([
          'level1' => $row['level1'],
          'level2' => $row['level2'],
          'level3' => $row['level3'],
          'level4' => $row['level4'],
        ])
        ->execute();
      if ($result !== \MergeQuery::STATUS_INSERT && $result !== \MergeQuery::STATUS_UPDATE) {
        watchdog(WATCHDOG_ERROR, 'Could not save WEAP index record for branch_id = %bid, level1 = %l1, level2 = %l2, level3 = %l3, level4 = %l4.', [
          '%bid' => $row['branch_id'],
          '%l1' => $row['level1'],
          '%l2' => $row['level2'],
          '%l3' => $row['level3'],
          '%l4' => $row['level4'],
        ]);
      }
    }
  }

}
