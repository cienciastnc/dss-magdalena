<?php
/**
 * @file
 * Define the filters to be used in the SimaImportWeapDataboxCSV class.
 */

// @codingStandardsIgnoreStart

function filterByNoFilter($row) {
  return TRUE;
}

function filterByTransmissionLinks($row) {
  return ($row[3] == 'Supply and Resources' && $row[4] == 'Transmission Links' && $row[5] != '' && $row[6] != '') ? $row : FALSE;
}

function filterByKeyAssumptions($row) {
  return ($row[3] == 'Key Assumptions') ? $row : FALSE;
}

function filterByFullBranchNamesList($row) {
  // Get data from the persistent variable.
  // This should contain a list of branch full names to compare with.
  $data = dss_engine_persistent_variable();
  return (in_array($row[1], $data)) ? $row : FALSE;
}

function filterByYear1981($row) {
  return ($row[6] == '1981') ? $row : FALSE;
}

function filterByLevels($row) {
  // Get data from the persistent variable..
  $data = dss_engine_persistent_variable();
  return ($row[2] == $data['Level 1'] && $row[3] == $data['Level 2'] && $row[4] == $data['Level 3'] && $row[5] == $data['Level 4']) ? $row : FALSE;
}

function filterByLevelsLow($row) {
  // Get data from the persistent variable..
  $data = dss_engine_persistent_variable();
  return ($row[1] == $data['level1'] && $row[2] == $data['level2'] && $row[3] == $data['level3'] && $row[4] == $data['level4']) ? $row : FALSE;
}

function filterByStrippingOneRecord($row) {
  // Get data from the persistent variable..
  $data = dss_engine_persistent_variable();
  return ($data !== array_values($row));
}

function filterByWetlandManagementProjects($row) {
  return ($row[3] == 'Key Assumptions' && $row[4] == 'intervenciones_humedales') ? $row : FALSE;
}

function filterByWetlandManagementProjectsExpressions($row) {
  // Get data from the persistent variable..
  $data = dss_engine_persistent_variable();
  return (in_array($row[10], $data)) ? $row : FALSE;
}

function filterByRiverReaches($row) {
  return ($row[3] == 'Supply and Resources' && $row[4] == 'River' && $row[6] !== '') ? $row : FALSE;
}

function filterByCatchments($row) {
  return ($row[3] == 'Demand Sites and Catchments' && $row[5] !== '') ? $row : FALSE;
}

function filterBySpecificCatchment($row) {
  $data = dss_engine_persistent_variable();
  // return ($row[2] == 'Demand Sites and Catchments'  && $row[3] == $data['Level 2'] && $row[4] !== '') ? $row : FALSE;
  return ($row[2] == 'Demand Sites and Catchments'  && $row[3] == $data['Level 2']) ? $row : FALSE;
}

// Scenario Variables.

/**
 * Filter for Precipitation.
 *
 *  $row[3]                    | $row[4]    | $row[5]   | $row[6]   | $row[7]       |
 * Level 1	                   | Level 2	| Level 3	| Level 4	| Variable	    | clave
 * Demand Sites and Catchments | instancia	| blank	    | blank	    | Precipitation	| 5
 *
 * @param $row
 * @return bool
 */
function filterByPrecipitation($row) {
  return ($row[7] == 'Precipitation' && $row[3] == 'Demand Sites and Catchments' && $row[4] !== '' && $row[5] == '' && $row[6] == '') ? $row : FALSE;
}

function filterByTemperature($row) {
  return ($row[7] == 'Temperature' && $row[3] == 'Demand Sites and Catchments' && $row[4] !== '' && $row[5] == '' && $row[6] == '') ? $row : FALSE;
}

function filterByHumidity($row) {
  return ($row[7] == 'Humidity' && $row[3] == 'Demand Sites and Catchments' && $row[4] !== '' && $row[5] == '' && $row[6] == '') ? $row : FALSE;
}

function filterByWind($row) {
  return ($row[7] == 'Wind' && $row[3] == 'Demand Sites and Catchments' && $row[4] !== '' && $row[5] == '' && $row[6] == '') ? $row : FALSE;
}

function filterByCloudinessFraction($row) {
  return ($row[7] == 'Cloudiness Fraction' && $row[3] == 'Demand Sites and Catchments' && $row[4] !== '' && $row[5] == '' && $row[6] == '') ? $row : FALSE;
}

function filterByNetEvaporation($row) {
  return ($row[7] == 'Net Evaporation' && $row[3] == 'Supply and Resources' && $row[4] == 'River' && $row[5] != '' && substr($row[6], 0, strlen('Reservoirs')) == 'Reservoirs') ? $row : FALSE;
}

// Alternative Variables.

function filterAnnualActivityLevel($row) {
  return ($row[7] == 'Annual Activity Level' && $row[3] == 'Demand Sites and Catchments' && $row[4] != '' && $row[5] == '' && $row[6] == '') ? $row : FALSE;
}

function filterAnnualWaterUseRate($row) {
  return ($row[7] == 'Annual Water Use Rate' && $row[3] == 'Demand Sites and Catchments' && $row[4] != '' && $row[5] == '' && $row[6] == '') ? $row : FALSE;
}

function filterByMonthlyVariationOfActivityLevel($row) {
  return ($row[7] == 'Monthly Variation' && $row[3] == 'Demand Sites and Catchments' && $row[4] != '' && $row[5] == '' && $row[6] == '') ? $row : FALSE;
}

// @TODO: This is not found in the Master File I have.  => Verify why it is not in the file.
function filterByStartupYear($row) {
  return ($row[7] == 'Startup Year' && $row[3] == 'Supply and Resources' && $row[4] == 'River' && $row[5] != '' && substr($row[6], 0, strlen('Reservoirs')) == 'Reservoirs') ? $row : FALSE;
}

function filterByStorageCapacity($row) {
  return ($row[7] == 'Storage Capacity' && $row[3] == 'Supply and Resources' && $row[4] == 'River' && $row[5] != '' && substr($row[6], 0, strlen('Reservoirs')) == 'Reservoirs') ? $row : FALSE;
}

function filterByTopOfInactive($row) {
  return ($row[7] == 'Top of Inactive' && $row[3] == 'Supply and Resources' && $row[4] == 'River' && $row[5] != '' && substr($row[6], 0, strlen('Reservoirs')) == 'Reservoirs') ? $row : FALSE;
}

function filterByMaxTurbineFlow($row) {
  return ($row[7] == 'Max. Turbine Flow' && $row[3] == 'Supply and Resources' && $row[4] == 'River' && $row[5] != '' && substr($row[6], 0, strlen('Reservoirs')) == 'Reservoirs') ? $row : FALSE;
}

function filterByGeneratingEfficiency($row) {
  return ($row[7] == 'Generating Efficiency' && $row[3] == 'Supply and Resources' && $row[4] == 'River' && $row[5] != '' && substr($row[6], 0, strlen('Reservoirs')) == 'Reservoirs') ? $row : FALSE;
}

function filterByTopOfConservation($row) {
  return ($row[7] == 'Top of Conservation' && $row[3] == 'Supply and Resources' && $row[4] == 'River' && $row[5] != '' && substr($row[6], 0, strlen('Reservoirs')) == 'Reservoirs') ? $row : FALSE;
}

function filterByTopOfBuffer($row) {
  return ($row[7] == 'Top of Buffer' && $row[3] == 'Supply and Resources' && $row[4] == 'River' && $row[5] != '' && substr($row[6], 0, strlen('Reservoirs')) == 'Reservoirs') ? $row : FALSE;
}

function filterByBufferCoefficient($row) {
  return ($row[7] == 'Buffer Coefficient' && $row[3] == 'Supply and Resources' && $row[4] == 'River' && $row[5] != '' && substr($row[6], 0, strlen('Reservoirs')) == 'Reservoirs') ? $row : FALSE;
}

function filterByPriority($row) {
  return ($row[7] == 'Priority' && $row[3] == 'Supply and Resources' && $row[4] == 'River' && $row[5] != '' && substr($row[6], 0, strlen('Reservoirs')) == 'Reservoirs') ? $row : FALSE;
}

function filterByHydropowerPriority($row) {
  return ($row[7] == 'Hydropower Priority' && $row[3] == 'Supply and Resources' && $row[4] == 'River' && $row[5] != '' && substr($row[6], 0, strlen('Reservoirs')) == 'Reservoirs') ? $row : FALSE;
}

function filterByPriorityCME($row) {
  return ($row[7] == 'Priority' && $row[3] == 'Supply and Resources' && $row[4] == 'River' && $row[5] != '' && substr($row[6], 0, strlen('Flow Requirements')) == 'Flow Requirements') ? $row : FALSE;
}

function filterByRiverFloodingThreshold($row) {
  return ($row[7] == 'River Flooding Threshold' && $row[3] == 'Supply and Resources' && $row[4] == 'River' && $row[5] != '' && substr($row[6], 0, strlen('Reaches\Below')) == 'Reaches\Below') ? $row : FALSE;
}

function filterByRiverFloodingFraction($row) {
  return ($row[7] == 'River Flooding Fraction' && $row[3] == 'Supply and Resources' && $row[4] == 'River' && $row[5] != '' && substr($row[6], 0, strlen('Reaches\Below')) == 'Reaches\Below') ? $row : FALSE;
}

function filterByFloodReturnFraction($row) {
  return ($row[7] == 'Flood Return Fraction' && $row[3] == 'Demand Sites and Catchments' && $row[4] != '' && $row[5] != '' && $row[6] == '') ? $row : FALSE;
}

function filterByFractionFloodingReceived($row) {
  return ($row[7] == 'Fraction Flooding Received' && $row[3] == 'Demand Sites and Catchments' && $row[4] != '' && $row[5] != '' && $row[6] == '') ? $row : FALSE;
}

function filterByIrrigatedArea($row) {
  return ($row[7] == 'Irrigated Area' && $row[3] == 'Demand Sites and Catchments' && $row[4] != '' && $row[5] != '' && $row[6] == '') ? $row : FALSE;
}

function filterByDemandPriorityAgricultural($row) {
  return ($row[7] == 'Demand Priority' && $row[3] == 'Demand Sites and Catchments' && $row[4] != '' && $row[5] == '' && $row[6] == '') ? $row : FALSE;
}

function filterByLowerThreshold($row) {
  return ($row[7] == 'Lower Threshold' && $row[3] == 'Demand Sites and Catchments' && $row[4] != '' && $row[5] == 'Agricola' && $row[6] == '') ? $row : FALSE;
}

function filterByUpperThreshold($row) {
  return ($row[7] == 'Upper Threshold' && $row[3] == 'Demand Sites and Catchments' && $row[4] != '' && $row[5] == 'Agricola' && $row[6] == '') ? $row : FALSE;
}

// Context Variables.

function filterByInitialZ1($row) {
  return ($row[7] == 'Initial Z1' && $row[3] == 'Demand Sites and Catchments' && $row[4] != '' && $row[5] != '' && $row[6] == '') ? $row : FALSE;
}

function filterByInitialZ2($row) {
  return ($row[7] == 'Initial Z2' && $row[3] == 'Demand Sites and Catchments' && $row[4] != '' && $row[5] == '' && $row[6] == '') ? $row : FALSE;
}

function filterByPreferredFlowDirection($row) {
  return ($row[7] == 'Preferred Flow Direction' && $row[3] == 'Demand Sites and Catchments' && $row[4] != '' && $row[5] != '' && $row[6] == '') ? $row : FALSE;
}

function filterBySoilWaterCapacity($row) {
  return ($row[7] == 'Soil Water Capacity' && $row[3] == 'Demand Sites and Catchments' && $row[4] != '' && $row[5] != '' && $row[6] == '') ? $row : FALSE;
}

function filterByDeepWaterCapacity($row) {
  return ($row[7] == 'Deep Water Capacity' && $row[3] == 'Demand Sites and Catchments' && $row[4] != '' && $row[5] == '' && $row[6] == '') ? $row : FALSE;
}

function filterByRootZoneConductivity($row) {
  return ($row[7] == 'Root Zone Conductivity' && $row[3] == 'Demand Sites and Catchments' && $row[4] != '' && $row[5] != '' && $row[6] == '') ? $row : FALSE;
}

function filterByDeepConductivity($row) {
  return ($row[7] == 'Deep Conductivity' && $row[3] == 'Demand Sites and Catchments' && $row[4] != '' && $row[5] == '' && $row[6] == '') ? $row : FALSE;
}

function filterByRunoffResistanceFactor($row) {
  return ($row[7] == 'Runoff Resistance Factor' && $row[3] == 'Demand Sites and Catchments' && $row[4] != '' && $row[5] != '' && $row[6] == '') ? $row : FALSE;
}

function filterByCropCoefficient($row) {
  return ($row[7] == 'Kc' && $row[3] == 'Demand Sites and Catchments' && $row[4] != '' && $row[5] != '' && $row[6] == '') ? $row : FALSE;
}

function filterByInitialStorageAquifer($row) {
  return ($row[7] == 'Initial Storage' && $row[3] == 'Supply and Resources' && $row[4] == 'Groundwater' && $row[5] != '' && $row[6] == '') ? $row : FALSE;
}

function filterByStorageCapacityAquifer($row) {
  return ($row[7] == 'Storage Capacity' && $row[3] == 'Supply and Resources' && $row[4] == 'Groundwater' && $row[5] != '' && $row[6] == '') ? $row : FALSE;
}

function filterByInfiltration($row) {
  return ($row[7] == 'Loss to Groundwater' && $row[3] == 'Supply and Resources' && $row[4] == 'River' && $row[5] != '' && substr($row[6], 0, strlen('Reservoirs')) == 'Reservoirs') ? $row : FALSE;
}

// @TODO: Cannot find this var in the master file.
function filterByInitialVolume($row) {
  return ($row[7] == 'Initial Volume' && $row[3] == 'Supply and Resources' && $row[4] == 'River' && $row[5] != '' && substr($row[6], 0, strlen('Reservoirs')) == 'Reservoirs') ? $row : FALSE;
}

function filterByMaximumHydraulicOutflow($row) {
  return ($row[7] == 'Maximum Hydraulic Outflow' && $row[3] == 'Supply and Resources' && $row[4] == 'River' && $row[5] != '' && substr($row[6], 0, strlen('Reservoirs')) == 'Reservoirs') ? $row : FALSE;
}

function filterByTailwaterElevation($row) {
  return ($row[7] == 'Tailwater Elevation' && $row[3] == 'Supply and Resources' && $row[4] == 'River' && $row[5] != '' && substr($row[6], 0, strlen('Reservoirs')) == 'Reservoirs') ? $row : FALSE;
}

function filterByEnergyDemand($row) {
  return ($row[7] == 'Energy Demand' && $row[3] == 'Supply and Resources' && $row[4] == 'River' && $row[5] != '' && substr($row[6], 0, strlen('Reservoirs')) == 'Reservoirs') ? $row : FALSE;
}

function filterByPlantFactor($row) {
  return ($row[7] == 'Plant Factor' && $row[3] == 'Supply and Resources' && $row[4] == 'River' && $row[5] != '' && substr($row[6], 0, strlen('Reservoirs')) == 'Reservoirs') ? $row : FALSE;
}

function filterByMinimumFlowRequirement($row) {
  return ($row[7] == 'Minimum Flow Requirement' && $row[3] == 'Supply and Resources' && $row[4] == 'River' && $row[5] != '' && substr($row[6], 0, strlen('Flow Requirements')) == 'Flow Requirements') ? $row : FALSE;
}

function filterByAreaSubcuenca($row) {
  return ($row[7] == 'Area' && $row[3] == 'Demand Sites and Catchments' && $row[4] != '' && $row[5] == '' && $row[6] == '') ? $row : FALSE;
}

function filterByAreaSoilUsagePercentageOfSimulation($row) {
  return ($row[7] == 'Area' && $row[3] == 'Demand Sites and Catchments' && $row[4] != '' && $row[5] != '' && $row[6] == '') ? $row : FALSE;
}

function filterByMaximumFlowVolume($row) {
  return ($row[7] == 'Maximum Flow   Volume' && $row[3] == 'Supply and Resources' && $row[4] == 'Transmission Links' && $row[5] != '' && $row[6] != '') ? $row : FALSE;
}

function filterByMaximumFlowPercentOfDemand($row) {
  return ($row[7] == 'Maximum Flow   Percent of Demand' && $row[3] == 'Supply and Resources' && $row[4] == 'Transmission Links' && $row[5] != '' && substr($row[6], 0, strlen('from Withdrawal Node')) == 'from Withdrawal Node') ? $row : FALSE;
}

function filterBySupplyPreference($row) {
  return ($row[7] == 'Supply Preference' && $row[3] == 'Supply and Resources' && $row[4] == 'Transmission Links' && $row[5] != '' && substr($row[6], 0, strlen('from Withdrawal Node')) == 'from Withdrawal Node') ? $row : FALSE;
}

function filterByLossFromSystem($row) {
  return ($row[7] == 'Loss from System' && $row[3] == 'Supply and Resources' && $row[4] == 'Transmission Links' && $row[5] != '' && substr($row[6], 0, strlen('from Withdrawal Node')) == 'from Withdrawal Node') ? $row : FALSE;
}

function filterByLossToGroundwater($row) {
  return ($row[7] == 'Loss to Groundwater' && $row[3] == 'Supply and Resources' && $row[4] == 'Transmission Links' && $row[5] != '' && substr($row[6], 0, strlen('from Withdrawal Node')) == 'from Withdrawal Node') ? $row : FALSE;
}

function filterByMaximumDiversion($row) {
  return ($row[7] == 'Maximum Diversion' && $row[3] == 'Supply and Resources' && $row[4] == 'Diversion' && $row[5] != '' && $row[6] == '') ? $row : FALSE;
}

function filterByLossRate($row) {
  return ($row[7] == 'Loss Rate' && $row[3] == 'Demand Sites and Catchments' && $row[4] != '' && $row[5] == '' && $row[6] == '') ? $row : FALSE;
}

function filterByConsumption($row) {
  return ($row[7] == 'Consumption' && $row[3] == 'Demand Sites and Catchments' && $row[4] != '' && $row[5] == '' && $row[6] == '') ? $row : FALSE;
}

function filterByReuseRate($row) {
  return ($row[7] == 'Reuse Rate' && $row[3] == 'Demand Sites and Catchments' && $row[4] != '' && $row[5] == '' && $row[6] == '') ? $row : FALSE;
}

function filterByDemandPriorityCivil($row) {
  return ($row[7] == 'Demand Priority' && $row[3] == 'Demand Sites and Catchments' && $row[4] != '' && $row[5] == '' && $row[6] == '') ? $row : FALSE;
}

// @TODO: Too many Volume Area Elevation Curves: 3 Variables.
function filterByVolumeElevationCurve($row) {
  return ($row[7] == 'Volume Area Elevation Curve' && $row[3] == 'Supply and Resources' && $row[4] == 'River' && $row[5] != '' && substr($row[6], 0, strlen('Reservoirs')) == 'Reservoirs') ? $row : FALSE;
}

function filterByAreaElevationCurve($row) {
  return ($row[7] == 'Volume Area Elevation Curve' && $row[3] == 'Supply and Resources' && $row[4] == 'River' && $row[5] != '' && substr($row[6], 0, strlen('Reservoirs')) == 'Reservoirs') ? $row : FALSE;
}
function filterByStreamflowData($row) {
  return ($row[7] == 'Streamflow Data' && $row[3] == 'Supply and Resources' && $row[4] == 'River' && $row[5] != '' && substr($row[6], 0, strlen('Streamflow Gauges')) == 'Streamflow Gauges') ? $row : FALSE;
}

function filterByObservedVolume($row) {
  return ($row[7] == 'Observed Volume' && $row[3] == 'Supply and Resources' && $row[4] == 'River' && $row[5] != '' && substr($row[6], 0, strlen('Reservoirs')) == 'Reservoirs') ? $row : FALSE;
}

// @codingStandardsIgnoreEnd
