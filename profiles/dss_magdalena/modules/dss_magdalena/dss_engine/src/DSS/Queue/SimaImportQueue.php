<?php
/**
 * @file
 * Mananges the Sima Import Queue.
 */

namespace Drupal\dss_magdalena\DSS\Queue;

use Drupal\dss_magdalena\DSS\Entity\SimaCaseOutputs;
use Drupal\dss_magdalena\DSS\Entity\SimaModel;
use Drupal\dss_magdalena\DSS\Entity\SimaResource;
use DrupalQueue;

/**
 * Implements the Sima Import Queue.
 *
 * Class SimaImportQueue.
 *
 * @package Drupal\dss_magdalena\DSS\Queue
 */
class SimaImportQueue {

  const QUEUE_NAME           = 'dss_engine_sima_import_queue';
  const QUEUE_TITLE          = 'Sima Import Queue';
  const QUEUE_RUNNER         = 'dss_engine_sima_import_queue_worker';
  const QUEUE_CRON_RUNNER    = 'dss_engine_sima_import_queue_cron_worker';
  const QUEUE_BATCH_FINISHED = 'dss_engine_sima_import_queue_batch_finished';

  /**
   * Flag to determine whether we want to process the datastore or not.
   *
   * When importing items (Resources or Cajas de Datos), we might want to
   * process the datastore.
   *
   * @var bool
   *   TRUE to process datastore when importing, FALSE otherwise.
   */
  protected $processDatastore = FALSE;

  /**
   * Public constructor.
   *
   * @param bool|FALSE $process_datastore
   *   TRUE to process the datastore, FALSE otherwise.
   */
  public function __construct($process_datastore = FALSE) {
    $this->processDatastore = $process_datastore;
  }

  /**
   * Creates an Item in the Sima Import Queue.
   *
   * @param \Drupal\dss_magdalena\DSS\Queue\SimaImportQueueItem $item
   *   An item to be inserted in the Queue.
   */
  public function createItem(SimaImportQueueItem $item) {
    /** @var \DrupalQueueInterface $queue */
    $queue = DrupalQueue::get(self::QUEUE_NAME);
    $queue->createQueue();
    $queue->createItem($item->toArray());
  }


  /**
   * Processes a single item from the Queue.
   *
   * @return bool
   *   TRUE if it was processed successfully, FALSE otherwise.
   */
  public function processItem(array $item) {
    // Processing single item.
    $queue_item = new SimaImportQueueItem($item);

    // Loads the Case Outputs.
    $case_outputs = SimaCaseOutputs::load($queue_item->getCaseOutputId());

    // Adds a new resource if it does not exist yet.
    if ($case_outputs->checkWeapOutputExistsForDataset($queue_item->getDssMachineName()) === FALSE) {

      /** @var \Drupal\dss_magdalena\DSS\Entity\SimaResource $resource */
      if ($resource = SimaResource::newFullyFormedResource($queue_item)) {

        // Make sure this is a resource linked to an output variable, otherwise
        // we should not save it.
        $dataset = $resource->getDataset();
        $resource->save();

        if ($dataset->isModelingOutput()) {
          // Now save the Case Output to include this resource as a WEAP output.
          $case_outputs->addOutputResource(SimaModel::MODEL_WEAP, $resource);
          $case_outputs->save();
        }
        else {
          if ($dataset->isModelingInput()) {
            drupal_set_message(t('The resource "!resource" is NOT an output variable. Imported as an Input Variable.', array(
              '!resource' => $resource->getTitle(),
            )), 'warning');
          }
        }

        // Do we want to process the datastore?
        if ($this->processDatastore) {
          $resource->processDataStore();
        }
        return TRUE;
      }
    }
    drupal_set_message(t('There is already an existent resource associated to case study "!cid" and Variable "!dataset". Skipped.', array(
      '!cid' => $queue_item->getCaseStudyId(),
      '!dataset' => $queue_item->getDssMachineName(),
    )), 'warning');
    return FALSE;
  }

  /**
   * Lists the number of items in the Queue.
   *
   * @return int
   *   The number of items in the queue.
   */
  public function numberOfItems() {
    /** @var \DrupalQueueInterface $queue */
    $queue = DrupalQueue::get(self::QUEUE_NAME);
    $queue->createQueue();
    return $queue->numberOfItems();
  }

}
