<?php
/**
 * @file
 * Sima Import Queue Item.
 */

namespace Drupal\dss_magdalena\DSS\Queue;

/**
 * Implements a Queue for Import of WEAP Outputs.
 *
 * Class SimaImportQueueItem.
 *
 * @package Drupal\dss_magdalena\DSS\Queue
 */
class SimaImportQueueItem {

  /**
   * The Case ID (nid).
   *
   * @var int
   */
  protected $cid;

  /**
   * The Case Output ID (nid).
   *
   * @var int
   *   The Case Output ID.
   */
  protected $caseOutput;

  /**
   * The file URI to import.
   *
   * @var string
   *   The file URI.
   */
  protected $fileUri;

  /**
   * The filename to import.
   *
   * @var string
   *   The filename.
   */
  protected $filename;

  /**
   * The DSS variable machine name or Resource.
   *
   * @var string
   *   The Dataset machine name.
   */
  protected $dssMachineName;

  /**
   * Public constructor.
   *
   * @param array $values
   *   An array of values.
   */
  public function __construct($values = []) {
    $this->setValues($values);
  }

  /**
   * Sets the values according to an array.
   *
   * @param array $values
   *   The values for all variables.
   */
  public function setValues($values = []) {
    if (isset($values['cid'])) {
      $this->setCaseStudyId($values['cid']);
    }
    if (isset($values['case_output'])) {
      $this->setCaseOutputId($values['case_output']);
    }
    if (isset($values['file_uri'])) {
      $this->fileUri = $values['file_uri'];
    }
    if (isset($values['filename'])) {
      $this->filename = $values['filename'];
    }
    if (isset($values['dss_machine_name'])) {
      $this->dssMachineName = $values['dss_machine_name'];
    }
  }

  /**
   * Sets the Case Study ID.
   *
   * @param int $cid
   *   The Case Study ID (nid).
   */
  public function setCaseStudyId($cid) {
    $this->cid = $cid;
  }


  /**
   * Sets the Case Output ID.
   *
   * @param int $case_output
   *   The Case Output ID (nid).
   */
  public function setCaseOutputId($case_output) {
    $this->caseOutput = $case_output;
  }

  /**
   * Sets the File Data.
   *
   * @param object $file
   *   The file object.
   */
  public function setFile($file) {
    $this->fileUri = $file->uri;
    $this->filename = $file->filename;
    $this->dssMachineName = $file->name;
  }

  /**
   * Returns the Case Study ID.
   *
   * @return int
   *   The Case Study ID (nid).
   */
  public function getCaseStudyId() {
    return $this->cid;
  }

  /**
   * Returns the Case Ouptut ID.
   *
   * @return int
   *   The Case Outputs ID (nid).
   */
  public function getCaseOutputId() {
    return $this->caseOutput;
  }

  /**
   * Returns the file URI.
   *
   * @return string
   *   The File URI.
   */
  public function getFileUri() {
    return $this->fileUri;
  }

  /**
   * Returns the Filename.
   *
   * @return string
   *   The filename.
   */
  public function getFilename() {
    return $this->filename;
  }

  /**
   * Returns the DSS Machine Name.
   *
   * @return string
   *   The DSS Machine Name.
   */
  public function getDssMachineName() {
    return $this->dssMachineName;
  }

  /**
   * Converts the current object to an array representation.
   *
   * @return array
   *   The Array representation of the current object.
   */
  public function toArray() {
    return [
      'cid' => $this->getCaseStudyId(),
      'case_output' => $this->getCaseOutputId(),
      'file_uri' => $this->getFileUri(),
      'filename' => $this->getFilename(),
      'dss_machine_name' => $this->getDssMachineName(),
    ];
  }

}
