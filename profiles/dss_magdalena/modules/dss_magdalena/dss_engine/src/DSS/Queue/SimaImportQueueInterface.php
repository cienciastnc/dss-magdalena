<?php
/**
 * @file
 * Interface for Sima Import Queue.
 */

namespace Drupal\dss_magdalena\DSS\Queue;

/**
 * Defines the Interface for SimaImportQueue.
 *
 * This class imports Resources (Cajas de Datos) CSV files from a case study
 * into the Case Outputs. It only deals with WEAP variables because those are
 * the ones that have been transferred through WebDAV in a zip file.
 * One item in the queue imports a single CSV file into a WEAP output variable
 * or resource (Caja de Datos).
 *
 * Interface SimaImportQueueInterface
 *
 * @package Drupal\dss_magdalena\DSS
 */
interface SimaImportQueueInterface {

  /**
   * Creates an Item in the Sima Import Queue.
   *
   * @param \Drupal\dss_magdalena\DSS\Queue\SimaImportQueueItem $item
   *   An item to be inserted in the Queue.
   */
  public function createItem(SimaImportQueueItem $item);

  /**
   * Processes a single item from the Queue.
   *
   * @param array $item
   *   The queue item.
   *
   * @return bool
   *   TRUE if it was processed successfully, FALSE otherwise.
   */
  public function processItem($item);

  /**
   * Returns tbe number of items in the Queue.
   *
   * @return int
   *   The number of items in the queue.
   */
  public function numberOfItems();

}
