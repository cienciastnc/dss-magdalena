<?php
/**
 * @file
 * Imports WEAP CSV Master File into CSV Files.
 *
 * @deprecated
 */

namespace Drupal\dss_magdalena\DSS;

use Drupal\dss_magdalena\DSS\Entity as DSSEntity;
use Drupal\dss_magdalena\DSS\Utils\SimaUtils;
use League\Csv\Reader;
use League\Csv\Writer;
use SplTempFileObject;
use DateTime;

/**
 * Reads WEAP CSV Master File.
 *
 * Class SimaImportWeapDataboxCSV.
 *
 * @package Drupal\dss_magdalena\DSS
 */
class SimaImportWeapDataboxCSV extends SimaUtils {

  const WEAP_MASTER_DIR               = "public://weap_master_databox";
  const WEAP_MASTER_DIR_FILES         = "public://weap_master_databox/files";

  /**
   * The Master CSV file.
   *
   * @var string $csvFile
   *   WEAP CSV Master File.
   */
  protected $csvMasterFile;

  /**
   * The mappings.
   *
   * @var array $mappings
   *   The mappings from WEAP variables to its filter.
   *    [variable name, weap name, filter].
   */
  protected $mappings = array(

// @codingStandardsIgnoreStart
    // Scenario Variables.
  //    0 =>
  //      array(
  //        'dataset' => 'dss_precipitacion',
  //        'weap' => 'Precipitation',
  //        'filter' => 'filterByPrecipitation',
  //      ),
  //    1 =>
  //      array(
  //        'dataset' => 'dss_temperatura',
  //        'weap' => 'Temperature',
  //        'filter' => 'filterByTemperature',
  //      ),
  //    2 =>
  //      array(
  //        'dataset' => 'dss_humedad',
  //        'weap' => 'Humidity',
  //        'filter' => 'filterByHumidity',
  //      ),
  //    3 =>
  //      array(
  //        'dataset' => 'dss_viento',
  //        'weap' => 'Wind',
  //        'filter' => 'filterByWind',
  //      ),
  //    4 =>
  //      array(
  //        'dataset' => 'dss_nubosidad',
  //        'weap' => 'Cloudiness Fraction',
  //        'filter' => 'filterByCloudinessFraction',
  //      ),.
    5 =>
      array(
        'dataset' => 'dss_poblacion_pronosticada',
        'weap' => 'Annual Activity Level',
        'filter' => 'filterAnnualActivityLevel',
      ),
    6 =>
      array(
        'dataset' => 'dss_demanda_hidrica_civil',
        'weap' => 'Annual Water Use Rate',
        'filter' => 'filterAnnualWaterUseRate',
      ),

    // This one is called Monthly Variation of Activity level.
    7 =>
      array(
        'dataset' => 'dss_patron_demanda_poblacion',
        'weap' => 'Monthly Variation',
        'filter' => 'filterByMonthlyVariationOfActivityLevel',
      ),


    // Alternative Variables.
  //    8 =>
  //      array(
  //        'dataset' => 'dss_ano_inicio',
  //        'weap' => 'Startup Year',
  //        'filter' => 'filterByStartupYear',
  //      ),.
    9 =>
      array(
        'dataset' => 'dss_capacidad',
        'weap' => 'Storage Capacity',
        'filter' => 'filterByStorageCapacity',
      ),
  // 10 =>
  //      array(
  //        'dataset' => 'dss_volumen_muerto',
  //        'weap' => 'Top of Inactive',
  //        'filter' => 'filterByTopOfInactive',
  //      ),.
    11 =>
      array(
        'dataset' => 'dss_capacidad_turbinas',
        'weap' => 'Max Turbine Flow',
        'filter' => 'filterByMaxTurbineFlow',
      ),
    12 =>
      array(
        'dataset' => 'dss_eficiencia',
        'weap' => 'Generating Efficiency',
        'filter' => 'filterByGeneratingEfficiency',
      ),
  // 13 =>
  //      array(
  //        'dataset' => 'dss_vol_max_normal',
  //        'weap' => 'Top of Conservation',
  //        'filter' => 'filterByTopOfConservation',
  //      ),
  //    14 =>
  //      array(
  //        'dataset' => 'dss_vol_min_normal',
  //        'weap' => 'Top of Buffer',
  //        'filter' => 'filterByTopOfBuffer',
  //      ),.
    15 =>
      array(
        'dataset' => 'dss_fraccion_reserva',
        'weap' => 'Buffer Coefficient',
        'filter' => 'filterByBufferCoefficient',
      ),
    // Prioridad fuente.
    16 =>
      array(
        'dataset' => 'dss_prioridad_fuente',
        'weap' => 'Priority',
        'filter' => 'filterByPriority',
      ),
    17 =>
      array(
        'dataset' => 'dss_prioridad_demanda_energia',
        'weap' => 'Hydropower Priority',
        'filter' => 'filterByHydropowerPriority',
      ),
    // ***** This is the only variable that repeats itself.
    // respeto CME.
    18 =>
      array(
        'dataset' => 'dss_criterio_respeto_cme',
        'weap' => 'PriorityCME',
        'filter' => 'filterByPriorityCME',
      ),
  // 19 =>
  //      array(
  //        'dataset' => 'dss_umbral_inundacion',
  //        'weap' => 'River Flooding Threshold',
  //        'filter' => 'filterByRiverFloodingThreshold',
  //      ),
  //    20 =>
  //      array(
  //        'dataset' => 'dss_fraccion_caudal_desborde',
  //        'weap' => 'River Flooding Fraction',
  //        'filter' => 'filterByRiverFloodingFraction',
  //      ),
    // This one is called Flood return rate.
  //    21 =>
  //      array(
  //        'dataset' => 'dss_retorno_desborde',
  //        'weap' => 'Flood Return Fraction',
  //        'filter' => 'filterByFloodReturnFraction',
  //      ),
    // This one is called Flood fraction received.
  //    22 =>
  //      array(
  //        'dataset' => 'dss_distribucion_inundacion',
  //        'weap' => 'Fraction Flooding Received',
  //        'filter' => 'filterByFractionFloodingReceived',
  //      ),.
    23 =>
      array(
        'dataset' => 'dss_porcentaje_regadio',
        'weap' => 'Irrigated Area',
        'filter' => 'filterByIrrigatedArea',
      ),
    // ***** This variable also repeats itself during WEAP Declarations.
    // Priority Variable; is duplicated on postion 16 of the array.
    // It should be consulted if this is an error or is it allowed.
    24 =>
      array(
        'dataset' => 'dss_prioridad_usuario_riego',
        'weap' => 'Demand Priority',
        'filter' => 'filterByDemandPriorityAgricultural',
      ),

    // This one is called Lower soil moisture level to START irrigation.
    25 =>
      array(
        'dataset' => 'dss_umbral_humedad_riego',
        'weap' => 'Lower Threshold',
        'filter' => 'filterByLowerThreshold',
      ),

    // This one is called Upper soil moisture level to STOP irrigation.
    26 =>
      array(
        'dataset' => 'dss_umbral_humedad_parar_riego',
        'weap' => 'Upper Threshold',
        'filter' => 'filterByUpperThreshold',
      ),

  // 27 =>
  //      array(
  //        'dataset' => 'dss_eficiencia_hidro',
  //        'weap' => 'Generating Efficiency',
  //        'filter' => 'filterByGeneratingEfficiency',
  //      ),
  //    28 =>
  //      array(
  //        'dataset' => 'dss_capacidad_turbinas_hidro',
  //        'weap' => 'Max Turbine Flow',
  //        'filter' => 'filterByMaxTurbineFlow',
  //      ),
  //    29 =>
  //      array(
  //        'dataset' => 'dss_porc_utilizacion_hidro',
  //        'weap' => 'Plant Factor',
  //        'filter' => 'filterByPlantFactor',
  //      ),
  //    30 =>
  //      array(
  //        'dataset' => 'dss_prioridad_demanda_energia_hidro',
  //        'weap' => 'Hydropower Priority',
  //        'filter' => 'filterByHydropowerPriority',
  //      ),
  //    31 =>
  //      array(
  //        'dataset' => 'dss_salto',
  //        'weap' => 'Fixed Head',
  //        'filter' => 'filterByFixed Head',
  //      ),
    // Context Variables.
    32 =>
      array(
        'dataset' => 'dss_z1_inicial',
        'weap' => 'Initial Z1',
        'filter' => 'filterByInitialZ1',
      ),
    33 =>
      array(
        'dataset' => 'dss_z2_inicial',
        'weap' => 'Initial Z2',
        'filter' => 'filterByInitialZ2',
      ),
  // 34 =>
  //      array(
  //        'dataset' => 'dss_reparticion_flujo_radical',
  //        'weap' => 'Preferred Flow Direction',
  //        'filter' => 'filterByPreferredFlowDirection',
  //      ),
  //    35 =>
  //      array(
  //        'dataset' => 'dss_capacidad_retencion_superficial',
  //        'weap' => 'Soil Water Capacity',
  //        'filter' => 'filterBySoilWaterCapacity',
  //      ),
  //    36 =>
  //      array(
  //        'dataset' => 'dss_capacidad_retencion_profunda',
  //        'weap' => 'Deep Water Capacity',
  //        'filter' => 'filterByDeepWaterCapacity',
  //      ),
  //    37 =>
  //      array(
  //        'dataset' => 'dss_conductividad_radical',
  //        'weap' => 'Root Zone Conductivity',
  //        'filter' => 'filterByRootZoneConductivity',
  //      ),
  //    38 =>
  //      array(
  //        'dataset' => 'dss_conductividad_profunda',
  //        'weap' => 'Deep Conductivity',
  //        'filter' => 'filterByDeepConductivity',
  //      ),
  //    39 =>
  //      array(
  //        'dataset' => 'dss_factor_escorrentia',
  //        'weap' => 'Runoff Resistance Factor',
  //        'filter' => 'filterByRunoffResistanceFactor',
  //      ),
    // This one is called Crop Coefficient.
  //    40 =>
  //      array(
  //        'dataset' => 'dss_kc',
  //        'weap' => 'Kc',
  //        'filter' => 'filterByCropCoefficient',
  //      ),
    // Initial Storage, Storage Capacity = NEW ONES!!!!!
    // INITIAL STORAGE SHOULD BE READ FROM HERE>.
    // @TODO CHECK FOR MACHINE NAMES OF NEXT TWO VARIABLES.
    41 =>
      array(
        'dataset' => 'dss_volumen_inicial_acuifero',
        'weap' => 'Initial Storage',
        'filter' => 'filterByInitialStorageAquifer',
      ),
    42 =>
      array(
        'dataset' => 'dss_capacidad_acuifero',
        'weap' => 'Storage Capacity',
        'filter' => 'filterByStorageCapacityAquifer',
      ),


  // 43 =>
  //      array(
  //        'dataset' => 'dss_infiltracion',
  //        'weap' => 'Infiltration',
  //        'filter' => 'filterByLossToGroundwater',
  //      ),
  //    44 =>
  //      array(
  //        'dataset' => 'dss_s0_supuesto',
  //        'weap' => 'Initial Volume',
  //        'filter' => 'filterByInitialVolume',
  //      ),
  //    45 =>
  //      array(
  //        'dataset' => 'dss_capacidad_descarga',
  //        'weap' => 'Maximum Hydraulic Outflow',
  //        'filter' => 'filterByMaximumHydraulicOutflow',
  //      ),
  //    46 =>
  //      array(
  //        'dataset' => 'dss_contracarga',
  //        'weap' => 'Tailwater Elevation',
  //        'filter' => 'filterByTailwaterElevation',
  //      ),
  //    47 =>
  //      array(
  //        'dataset' => 'dss_energia_target',
  //        'weap' => 'Energy Demand',
  //        'filter' => 'filterByEnergyDemand',
  //      ),
  //    48 =>
  //      array(
  //        'dataset' => 'dss_porc_utilizacion',
  //        'weap' => 'Plant Factor',
  //        'filter' => 'filterByPlantFactor',
  //      ),
  //    49 =>
  //      array(
  //        'dataset' => 'dss_evaporacion_neta',
  //        'weap' => 'Net Evaporation',
  //        'filter' => 'filterByNetEvaporation',
  //      ),
    // This one is called Instream Minimum Flow Requirement.
  //    50 =>
  //      array(
  //        'dataset' => 'dss_cme',
  //        'weap' => 'Minimum Flow Requirement',
  //        'filter' => 'filterByMinimumFlowRequirement',
  //      ),
  //    51 =>
  //      array(
  //        'dataset' => 'dss_area_subcuenca',
  //        'weap' => 'Area Subcuenca',
  //        'filter' => 'filterByAreaSubcuenca',
  //      ),
  //    52 =>
  //      array(
  //        'dataset' => 'dss_uso_suelo_porc_simulacion',
  //        'weap' => 'Area Soil Usage Percentage Of Simulation',
  //        'filter' => 'filterByAreaSoilUsagePercentageOfSimulation',
  //      ),
    // This one is called Max Flow Volume.
  //    53 =>
  //      array(
  //        'dataset' => 'dss_capacidad_conduccion',
  //        'weap' => 'Maximum Flow   Volume',
  //        'filter' => 'filterByMaximumFlowVolume',
  //      ),
    // This one is called Max Monthly Flow.
    54 =>
      array(
        'dataset' => 'dss_porc_capacidad',
        'weap' => 'Maximum Flow   Percent of Demand',
        'filter' => 'filterByMaximumFlowPercentOfDemand',
      ),
  // 55 =>
  //      array(
  //        'dataset' => 'dss_preferencia_conexion',
  //        'weap' => 'Supply Preference',
  //        'filter' => 'filterBySupplyPreference',
  //      ),
  //    56 =>
  //      array(
  //        'dataset' => 'dss_perdida_conduccion_sin_destino',
  //        'weap' => 'Loss from System',
  //        'filter' => 'filterByLossFromSystem',
  //      ),
  //    57 =>
  //      array(
  //        'dataset' => 'dss_perdida_conduccion_acuifero',
  //        'weap' => 'Loss to Groundwater',
  //        'filter' => 'filterByLossToGroundwater',
  //      ),
    // NEW ONE: Maximum Diversion  ADD!! => COMENTADA.
    // @TODO CHECK FOR MACHINE NAMES OF THIS VARIABLE.

    58 =>
      array(
        'dataset' => 'dss_max_derivacion',
        'weap' => 'Maximum Diversion',
        'filter' => 'filterByMaximumDiversion',
      ),

  // 59 =>
  //      array(
  //        'dataset' => 'dss_ineficiencia_demanda_civil',
  //        'weap' => 'Loss Rate',
  //        'filter' => 'filterByLossRate',
  //      ),
  //    60 =>
  //      array(
  //        'dataset' => 'dss_nivel_consumo_hidrico',
  //        'weap' => 'Consumption',
  //        'filter' => 'filterByConsumption',
  //      ),
    // This one is called Re-use rate.
  //    61 =>
  //      array(
  //        'dataset' => 'dss_nivel_re-uso',
  //        'weap' => 'Reuse Rate',
  //        'filter' => 'filterByReuseRate',
  //      ),
  //    62 =>
  //      array(
  //        'dataset' => 'dss_prioridad_usuario_civil',
  //        'weap' => 'Demand Priority',
  //        'filter' => 'filterByDemandPriorityCivil',
  //      ),
  //    63 =>
  //      array(
  //        'dataset' => 'dss_inundacion_inicial',
  //        'weap' => 'Initial Surface Depth',
  //        'filter' => 'filterByInitialSurfaceDepth',
  //      ),
  //    64 =>
  //      array(
  //        'dataset' => 'dss_energia_target_hidro',
  //        'weap' => 'Energy Demand',
  //        'filter' => 'filterByEnergyDemand',
  //      ),
      // RELATIONS 2 of them from the same WEAP variable:
      // VOLUME Elevation Curve para reservoirs.
      // Area Elevation Curve.
    65 =>
        array(
          'dataset' => 'curva_vz',
          'weap' => 'Volume Area Elevation Curve',
          'filter' => 'filterByVolumeElevationCurve',
        ),

    66 =>
        array(
          'dataset' => 'curva_az',
          'weap' => 'Volume Area Elevation Curve',
          'filter' => 'filterByAreaElevationCurve',
        ),

    // Otros.
  //    67 =>
  //      array(
  //        'dataset' => 'dss_q_medido',
  //        'weap' => 'Streamflow Data',
  //        'filter' => 'filterByStreamflowData',
  //      ),.
    68 =>
      array(
        'dataset' => 'dss_s_medido',
        'weap' => 'Observed Volume',
        'filter' => 'filterByObservedVolume',
      ),
  );
// @codingStandardsIgnoreEnd

  /**
   * The Header of the WEAP Master CSV File.
   *
   * @var array $header
   */
  protected $header;

  /**
   * The Output Header.
   *
   * @var array
   */
  protected $outputHeader = array(
    'BranchID',
    'Branch',
    'Level1',
    'Level2',
    'Level3',
    'Level4',
    'Year',
    'TimeStep',
    'Variable',
    'Units',
    'Scenario',
    'Alias',
    'Value',
  );

  /**
   * The key assumptions.
   *
   * @var array $keyAssumptions
   *   The Key Assumptions.
   */
  protected $keyAssumptions;

  /**
   * Public Constructor.
   *
   * If the WEAP Master file is not set, returns FALSE.
   *
   * @param int $autodetect_line_endings
   *   A flag to autodetect line endings: 1: TRUE, 0: FALSE.
   */
  public function __construct($autodetect_line_endings) {
    $this->csvMasterFile = variable_get('weap_masterfile_path', '');
    if ($this->setWeapMasterFile() === FALSE) {
      return FALSE;
    }
    $this->setAutodetectLineEndings($autodetect_line_endings);

    // If header has been extracted, set it. Otherwise, let it be set later.
    $this->header = variable_get('dss_admin_weap_masterfile_header', NULL);
  }

  /**
   * A flag to set the system to autodetect line endindgs.
   *
   * @param int $autodetect
   *   Autodetect: 1 TRUE, 0 FALSE.
   */
  public function setAutodetectLineEndings($autodetect = 1) {
    if ((bool) $autodetect) {
      ini_set("auto_detect_line_endings", '1');
    }
    else {
      ini_set("auto_detect_line_endings", '0');
    }
  }

  /**
   * Sets the current WEAP Master File.
   *
   * @return bool
   *   TRUE if WEAP Master file is set, FALSE otherwise.
   */
  protected function setWeapMasterFile() {
    $fid = variable_get('weap_masterfile_fid', FALSE);
    if ($fid) {
      $file = file_load($fid);
      $this->csvMasterFile = drupal_realpath($file->uri);
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Returns the WEAP Master File.
   *
   * @return null|object
   *   Returns the WEAP Master File Object or NULL.
   */
  static public function getWeapMasterFile() {
    $fid = variable_get('weap_masterfile_fid', FALSE);
    if ($fid) {
      $file = file_load($fid);
      return $file ?: NULL;
    }
    return NULL;
  }

  /**
   * Saves the WEAP Master File.
   *
   * @param object $file
   *   The File object to save.
   */
  static public function saveWeapMasterFile($file) {
    $fid = variable_get('weap_masterfile_fid', FALSE);

    // Obtain old fid and delete it if different to the new fid.
    if ($fid && $fid != $file->fid) {
      $file_old = file_load($fid);
      file_delete($file_old);
    }

    // Now attach new fid.
    variable_set('weap_masterfile_fid', $file->fid);
  }

  /**
   * Returns the mappings.
   *
   * @return array
   *   The mappings array.
   */
  protected function getMappings() {
    return $this->mappings;
  }

  /**
   * Lists all DSS variables.
   *
   * @return array
   *   An array of all variables.
   */
  public function getVariables() {
    return array_column($this->getMappings(), 'dataset');
  }

  /**
   * Returns a list of WEAP variable names.
   *
   * @return array
   *   An array of 'WEAP variables' names keyed by 'DSS Names'.
   */
  public function getWeapVariables() {
    return array_column($this->getMappings(), 'weap', 'dataset');
  }

  /**
   * Obtains the filter to use to search for this specific variable.
   *
   * @param string $variable
   *   The variable name.
   *
   * @return string|bool
   *   The filter name if exists for that variable, FALSE otherwise.
   */
  protected function getFilter($variable) {
    $mappings = array_column($this->getMappings(), 'filter', 'dataset');
    return isset($mappings[$variable]) ? $mappings[$variable] : FALSE;
  }

  /**
   * Returns a WEAP Variable name given a DSS Variable name.
   *
   * @param string $variable
   *   The DSS Variable name.
   *
   * @return string|bool
   *   The WEAP Variable Name, if exists, FALSE otherwise.
   */
  public function getWeapVariableName($variable) {
    $mappings = $this->getWeapVariables();
    return isset($mappings[$variable]) ? $mappings[$variable] : FALSE;
  }

  /**
   * Initializes directories.
   *
   * Creates the local directories that will store the import
   * master Databox CSV files.
   */
  public function initializeDirectory() {
    $directories = array(
      self::WEAP_MASTER_DIR,
      self::WEAP_MASTER_DIR_FILES,
    );
    return parent::initializeDirectories($directories);
  }

  /**
   * Obtains the dataset given the WEAP Name.
   *
   * @param string $weap_name
   *   The WEAP Variable Name.
   *
   * @return object|bool
   *   The dataset node object if found, FALSE otherwise.
   */
  public function getDatasetByWeapName($weap_name) {
    $dataset_query = new \EntityFieldQuery();
    $result = $dataset_query->entityCondition('entity_type', 'node')
      ->entitycondition('bundle', 'dataset')
      ->propertyCondition('status', NODE_PUBLISHED)
      ->fieldCondition('field_weap_name', 'value', $weap_name, '=')
      ->execute();
    if (isset($result['node'])) {
      $dataset_nids = array_keys($result['node']);
      $datasets = entity_load('node', $dataset_nids);
      $dataset = reset($datasets);
      return $dataset;
    }
    return FALSE;
  }

  /**
   * Obtains the Dataset given the machine name.
   *
   * @param string $machine_name
   *   The Dataset Variable Machine Name.
   *
   * @return object|bool
   *   The dataset node object if found, FALSE otherwise.
   */
  public function getDatasetByMachineName($machine_name) {
    $dataset_query = new \EntityFieldQuery();
    $result = $dataset_query->entityCondition('entity_type', 'node')
      ->entitycondition('bundle', 'dataset')
      ->propertyCondition('status', NODE_PUBLISHED)
      ->fieldCondition('field_machine_name', 'value', $machine_name, '=')
      ->execute();
    if (isset($result['node'])) {
      $dataset_nids = array_keys($result['node']);
      $datasets = entity_load('node', $dataset_nids);
      $dataset = reset($datasets);
      return $dataset;
    }
    return FALSE;
  }

  /**
   * Extract rows from the CSV File based on a filter.
   *
   * @param string $filter
   *   The filter name to apply.
   *
   * @return mixed
   *   The Results.
   */
  protected function extractRows($filter) {
    $master_file = Reader::createFromPath($this->csvMasterFile);
    module_load_include('inc', 'dss_engine', 'src/DSS/inc/sima_import_weap_databox_csv-filters');
    $results = $master_file
      ->stripBom(FALSE)
      ->addFilter($filter)
      ->fetchAssoc($this->header);
    return $results;
  }

  /**
   * Obtains the key Assumptions.
   */
  public function extractKeyAssumptions() {
    $results = $this->extractRows('filterByKeyAssumptions');
    $key_assumptions = iterator_to_array($results, FALSE);
    variable_set('dss_admin_weap_masterfile_key_assumptions', $key_assumptions);
    $this->keyAssumptions = $key_assumptions;
    return $this;
  }

  /**
   * Sets the Key Assumptions.
   */
  public function setKeyAssumptions() {
    $this->setHeader();
    $this->keyAssumptions = variable_get('dss_admin_weap_masterfile_key_assumptions', array());
    return $this;
  }

  /**
   * Extracts the Header from the WEAP Master File.
   *
   * Only searches in the first 10 initial rows of the file.
   *
   * @return int
   *   The Offset.
   */
  public function extractHeader() {
    if (empty($this->header)) {
      $master_file = Reader::createFromPath($this->csvMasterFile);
      // Extracting the Header.
      $offset = 0;
      $header = array();

      for ($i = 0; $i <= 10; $i++) {
        $header = $master_file->fetchOne($i);
        if ($header['0'] == 'BranchID') {
          $offset = $i + 1;
          break;
        }
      }
      $this->header = array_filter($header);
      variable_set('dss_admin_weap_masterfile_header', $this->header);
    }
    return $this;
  }

  /**
   * Returns the header of the CSV File.
   *
   * @return $this
   */
  public function setHeader() {
    $this->header = variable_get('dss_admin_weap_masterfile_header', array());
    if (count($this->header) <= 0) {
      $header = $this->extractHeader();
    }
    return $this;
  }

  /**
   * Evaluates a Mathematical expression.
   *
   * @param string $expression
   *   The expression to evaluate.
   *
   * @return bool|float
   *   The numeric value after evaluating, FALSE otherwise.
   */
  protected function evalMathExpression($expression) {
    if (preg_match('/([\d\.\s]+)([\+\-\‌​*\/])(\-?[\d\.\s]+)/', $expression, $matches) !== FALSE) {
      $operator = $matches[2];

      switch ($operator) {
        case '+':
          $p = $matches[1] + $matches[3];
          break;

        case '-':
          $p = $matches[1] - $matches[3];
          break;

        case '*':
          $p = $matches[1] * $matches[3];
          break;

        case '/':
          $p = $matches[1] / $matches[3];
          break;
      }

      return $p;
    }
    return FALSE;
  }

  /**
   * Reads an Expression.
   *
   * @param string $expression
   *   The expression to read.
   *
   * @return array
   *   The array.
   */
  protected function readExpression($expression) {
    switch ($expression) {
      case '':
        return [
          ['', '', $expression],
        ];

      // @codingStandardsIgnoreStart
      // Matches: ReadFromFile(Series\Historic\PT L5 Uns C1.csv, , 1970)
      // case (preg_match("/ReadFromFile(\s(.*)\s)/", $expression, $matches) ? true : false) :
      // @codingStandardsIgnoreEnd
      case ((strpos($expression, 'ReadFromFile(') !== FALSE) ? TRUE : FALSE):
        // $sa = $matches;.
          $args = str_replace('ReadFromFile', '', $expression);
        if (preg_match('#\((.*?)\)#', $args, $match) == 1) {
          $args = $match[1];
          if (count($args) == 3) {
            list($file, $d, $year) = explode(',', $args);

            // Now we have the file.
            $csvfile = self::WEAP_MASTER_DIR_FILES . '/' . str_replace('\\', '/', $file);
            $csvfile = str_replace('\\', '/', $csvfile);

            // Reading CSV File.
            if (file_exists($csvfile)) {
              $series_file = Reader::createFromPath($csvfile);
              return $series_file->fetchAll();
            }
          }
        }
        return array();

      case (is_numeric($expression) ? TRUE : FALSE):
        return [
          ['', '', number_format($expression, 2, '.', '')],
        ];

      case (is_numeric($this->evalMathExpression($expression)) ? TRUE : FALSE):
        $value = $this->evalMathExpression($expression);
        return [
          ['', '', number_format($value, 2, '.', '')],
        ];

      default:
        return [];
    }
  }

  /**
   * Reads the Master File and creates the CSV Databox Files.
   *
   * It executes this function according to the mappings.
   */
  public function createDataboxCsvFiles() {
    $offset = $this->extractHeader();

    // Extracting the Key Assumptions.
    $this->extractKeyAssumptions();

    // Building the CSV for each variable.
    $variables = $this->getVariables();
    foreach ($variables as $variable) {
      $this->buildDataboxCsvFileForVariable($variable);
    }

  }

  /**
   * Returns the element based on the combination of Row Levels.
   *
   * @param array $row
   *   The row array.
   *
   * @return string
   *   The combined levels into a single element.
   */
  protected function getBranch($row) {
    $branch = '';
    if (!empty($row['Level 1'])) {
      $branch .= $row['Level 1'];
      if (!empty($row['Level 2'])) {
        $branch .= '\\' . $row['Level 2'];
        if (!empty($row['Level 3'])) {
          $branch .= '\\' . $row['Level 3'];
          if (!empty($row['Level 4...'])) {
            $branch .= '\\' . $row['Level 4...'];
          }
        }
      }
    }
    return $branch;
  }


  /**
   * Builds the Databox CSV file for a single variable.
   *
   * @param string $variable
   *   The variable name.
   *
   * @return bool
   *   TRUE if succeeds, FALSE otherwise.
   */
  public function buildDataboxCsvFileForVariable($variable) {
    // Obtaining the Records for this variable.
    $filter = $this->getFilter($variable);
    $results = $this->extractRows($filter);

    // Creating the Output CSV File.
    $csv = Writer::createFromFileObject(new SplTempFileObject());
    $csv->insertOne($this->outputHeader);
    foreach ($results as $row) {
      $data = $this->readExpression($row['Expression']);

      foreach ($data as $value) {
        // Building the row to insert.
        $line = array(
          $row['BranchID'],
          $this->getBranch($row),
          $row['Level 1'],
          $row['Level 2'],
          $row['Level 3'],
          $row['Level 4...'],
          $value[0],
          $value[1],
          $row['Variable'],
          $row['Unit'],
          $row['Scenario'],
          '',
          $value[2],
        );

        $csv->insertOne($line);
      }
    }

    // Saving the CSV File.
    $csv_file = self::WEAP_MASTER_DIR . "/{$variable}.csv";
    return (bool) file_unmanaged_save_data($csv, $csv_file, FILE_EXISTS_REPLACE);
  }

  /**
   * Imports all CSV Files generated from WEAP Master file into Resources.
   *
   * @param string $variable
   *   The variable name.
   * @param string $weap_model_version
   *   The WEAP Model version.
   *
   * @return bool|\object
   *   The Resource Entity being created.
   *
   * @throws \EntityMetadataWrapperException
   */
  public function importCsvDataboxToResources($variable, $weap_model_version) {

    $import_status = variable_get('dss_engine_resources_databox_from_weap', array());

    if (in_array($variable, $import_status)) {
      // If we found it then it has already been imported.
      return FALSE;
    }

    // Get CSV File.
    $csv_file = self::WEAP_MASTER_DIR . "/{$variable}.csv";

    // Obtaining the Variable Types.
    $vocab = taxonomy_vocabulary_machine_name_load('variable_type');
    $terms = taxonomy_get_tree($vocab->vid);
    $variable_types = array();
    foreach ($terms as $term) {
      $variable_types[$term->name] = $term;
    }

    $csv_term = taxonomy_get_term_by_name('csv', 'format');
    if (empty($csv_term)) {
      // This is not supposed to be here but there is a possibility that we
      // might get the formats vocabulary empty because DKAN did not fill it
      // yet. So we force DKAN to fill it up.
      $previews = dkan_dataset_teaser_get_external_previews();
      $csv_term = taxonomy_get_term_by_name('csv', 'format');
    }
    $csv_term = reset($csv_term);

    // Obtaining correspondent Dataset.
    if ($dataset = $this->getDatasetByMachineName($variable)) {

      // First create the file in the file_managed table.
      $file = file_uri_to_object($csv_file);

      // Here we need to check for geometry.
      $file->delimiter = ',';
      $file->grid = 1;
      $file->graph = 1;
      $file->map = 0;
      $file->embed = 0;

      // Save the file.
      $file = file_save($file);

      // Setting up Creation Date.
      // Entity API cannot set date field values so the 'old' method must
      // be used.
      $creation_date = new DateTime('NOW');
      $timezone = drupal_get_user_timezone();

      // Add Resource CSV File into this Dataset.
      $values = array(
        'type' => 'resource',
        'uid' => 1,
        'status' => 1,
        'comment' => 0,
        'promote' => 0,
        'revision' => 0,

        // We are setting the file_upload field in advance. It does not work
        // setting it up on the entity_metadata_wrapper because we lose
        // the information about graph, grid, map.
        'field_upload' => array(
          LANGUAGE_NONE => array(
            0 => (array) $file,
          ),
        ),
        'field_creation_date' => array(
          LANGUAGE_NONE => array(
            0 => array(
              'value' => date_format($creation_date, 'Y-m-d'),
              'timezone' => $timezone,
              'timezone_db' => $timezone,
              'date_type' => 'datetime',
            ),
          ),
        ),
      );
      $node = entity_create('node', $values);
      $entity = entity_metadata_wrapper('node', $node);

      // Setting name, full name and body.
      $weap_name = $this->getWeapVariableName($variable);
      $name = $weap_name . ' - Caso Extremo';
      $entity->title = $name;
      $entity->field_full_name = $name;
      $entity->body->set(array(
        'value' => t('DSS Variable: %dataset_title (%dataset) - WEAP Variable Name: %weap', array(
          '%dataset_title' => $dataset->title,
          '%dataset' => $variable,
          '%weap' => $weap_name,
        )),
        'format' => 'filtered_html',
      ));

      // Assigning Model Declaration.
      $entity->field_model_declaration = $weap_model_version;

      // Assigning Model Family.
      $model_term = taxonomy_get_term_by_name('Conceptual WEAP', 'model_family');
      $model_term = reset($model_term);
      $entity->field_model_family->set($model_term);

      // Assigning Model Case ID.
      $entity->field_model_case_id = 'Caso Extremo';

      // Assigning entity reference to dataset.
      $entity->field_dataset_ref[0]->set($dataset);

      // Assigning format.
      $entity->field_format->set($csv_term);

      // Setting it up as 'Natural'.
      $entity->field_processing_type->set($variable_types['Natural']);

      // Saving the entity.
      $entity->save();

      // Updating the variable.
      $import_status[$entity->getIdentifier()] = $variable;
      variable_set('dss_engine_resources_databox_from_weap', $import_status);

      // Now, try to generate the DataSource.
      $form_state = array();
      $form_state['values'] = array(
        'FeedsCSVParser' => array(
          'delimiter' => ',',
          'no_headers' => FALSE,
          'encoding' => 'UTF-8',
        ),
        'FeedsFlatstoreProcessor' => array(
          'geolocate' => FALSE,
          'geolocater' => 'google',
          'geolocate_addresses' => 1,
        ),
        'confirm feeds update' => 1,
        'submit' => 'Import',
        'importer_id' => 'dkan_file',
        'op' => 'Import',
      );
      module_load_include('inc', 'dkan_datastore', 'dkan_datastore.pages');
      drupal_form_submit('dkan_datastore_import_tab_form', $form_state, $entity->value());

      // Return Entity.
      return $entity->value();

    }
    return FALSE;
  }

  /**
   * Gets all the Hidropower plants and dams to search for.
   */
  public function getNumberOfPowerPlantsAndDams() {
    // Variable we will take the number of Plants from.
    $hidroplants_and_dams = variable_get('dss_engine_hidroplants_and_dams', array());
    if (count($hidroplants_and_dams) <= 0) {

      // WEAP Variable = 'Storage Capacity'.
      // Setting this variable to be the one used to obtain the list of
      // hydropower plants or dams.
      $variable = 'dss_capacidad';

      // Reading CSV File.
      $csv_file = self::WEAP_MASTER_DIR . "/{$variable}.csv";
      $reader = Reader::createFromPath($csv_file);
      module_load_include('inc', 'dss_engine', 'src/DSS/inc/sima_import_weap_databox_csv-filters');
      $results = $reader
        ->stripBom(FALSE)
        ->fetchAssoc($this->outputHeader);
      foreach ($results as $row) {
        if (array_keys($row) == array_values($row)) {
          continue;
        }

        $hidroplants_and_dams[] = array_slice($row, 2, 4, TRUE);
      }
      // Now store this variable.
      variable_set('dss_engine_hidroplants_and_dams', $hidroplants_and_dams);
    }
    return $hidroplants_and_dams;
  }

  /**
   * Imports a Hydropower or Dam.
   *
   * @param array $project
   *   The project data.
   */
  public function importHydroPowerPlantOrDam($project) {

    // Setting up a persistent variable.
    dss_engine_persistent_variable($project);

    // Capture Data in the Ficha Array.
    $ficha_data = array();

    // Obtaining records for this variable.
    $filter = 'filterByLevels';

    // Get CSV File.
    $results = $this->extractRows($filter);
    foreach ($results as $row) {
      $ficha_data[] = $row;
    }

    // Creating a new SimaFicha For HydroPowerPlants or Dams.
    $ficha = DSSEntity\SimaFicha::newFicha(DSSEntity\SimaFicha::HYDROPOWER_PLANT_OR_DAM, $project, $ficha_data);

    // Resetting the persistent variable.
    dss_engine_persistent_variable(NULL);
  }

}
