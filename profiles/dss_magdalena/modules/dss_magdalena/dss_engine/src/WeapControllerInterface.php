<?php
/**
 * @file
 * Interface for WEAP Controller functionality.
 */

namespace Drupal\dss_magdalena;

/**
 * Defines the Interface for WeapController class.
 *
 * Interface WeapControllerInterface.
 *
 * @package Drupal\dss_magdalena
 */
interface WeapControllerInterface {

  /**
   * Obtains the Case Directory for Import.
   *
   * @param int $cid
   *   The Case ID.
   *
   * @return string
   *   The path to the case directory.
   */
  public function getImportCaseDirectory($cid);

  /**
   * Obtains the Case Directory for Export.
   *
   * @param int $cid
   *   The Case ID.
   *
   * @return string
   *   The path to the case directory.
   */
  public function getExportCaseDirectory($cid);

  /**
   * Checks that the connection settings are correct.
   *
   * @return bool
   *   TRUE if settings are correct, FALSE otherwise.
   */
  public function checkConnectionSettings();

  /**
   * Initializes remote directories.
   */
  public function initializeRemoteDirectories();

  /**
   * Generates the 3 files ready for export to S-WEAP.
   *
   * @param int $cid
   *   The Case ID: nid.
   * @param string $status
   *   The contents of the STATUS file.
   * @param string $masterfile_contents
   *   The contents of the WEAP master file.
   *
   * @return string|bool
   *   The case path if generated successfully, FALSE otherwise.
   */
  public function generateMasterFile($cid, $status = 'NEW', $masterfile_contents = '');

  /**
   * Exports the 3 Case files to the S-WEAP.
   *
   * @param int $cid
   *   The Case ID: nid.
   * @param string $status
   *   The contents of the STATUS file.
   *
   * @return bool
   *   TRUE if succeeds, FALSE otherwise.
   */
  public function exportCaseStudy($cid, $status = 'NEW', $masterfile_contents = '');

  /**
   * Imports the output files for a case from S-WEAP.
   *
   * @param int $cid
   *   The Case ID: nid.
   *
   * @return bool|mixed
   *   The destination dir if succeeds, FALSE otherwise.
   */
  public function importCaseStudy($cid);

  /**
   * Creates the Case Import Directory.
   *
   * @param int $cid
   *   The Case ID.
   *
   * @return bool|string
   *   The Case Import Path or FALSE.
   */
  public function createImportDirectory($cid);

  /**
   * Lists all the Case Studies available for Import.
   *
   * @return array
   *   Returns an array of the form:
   *   [
   *    cid => cid.zip
   *   ]
   */
  public function listCaseStudiesAvailableForImport();

}
