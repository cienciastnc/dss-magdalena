<?php
/**
 * @file
 * Controller for WEAP functionality.
 */

namespace Drupal\dss_magdalena;
use Sima\WeapClient\WeapConnector;


/**
 * The WEAP Controller.
 */
class WeapController implements WeapControllerInterface {

  const WEAP_DIR        = "public://sima_weap";
  const WEAP_DIR_EXPORT = "public://sima_weap/export";
  const WEAP_DIR_IMPORT = "public://sima_weap/import";

  /**
   * Status File.
   */
  const STATUS = "STATUS";

  /**
   * The Case Master File.
   */
  const MASTERFILE = 'masterfile.xlsx';

  /**
   * The WeapConnector Client.
   *
   * @var \Sima\WeapClient\WeapConnector
   */
  protected $client;

  /**
   * Public Constructor.
   *
   * @param \Sima\WeapClient\WeapConnector $client
   *   The WEAP Client.
   */
  public function __construct(WeapConnector $client = NULL) {
    if (isset($client)) {
      $this->client = $client;
    }
    else {
      $this->client = dss_engine_weap_client_load();
    }
  }

  /**
   * Obtains the Case Directory for Import.
   *
   * @param int $cid
   *   The Case ID.
   *
   * @return string
   *   The path to the case directory.
   */
  public function getImportCaseDirectory($cid) {
    return self::WEAP_DIR_IMPORT . '/' . $cid;
  }

  /**
   * Obtains the Case Directory for Export.
   *
   * @param int $cid
   *   The Case ID.
   *
   * @return string
   *   The path to the case directory.
   */
  public function getExportCaseDirectory($cid) {
    return self::WEAP_DIR_EXPORT . '/' . $cid;
  }

  /**
   * Returns the Masterfile URI.
   *
   * @param int $cid
   *   The Case Study ID.
   *
   * @return string
   *   The File URI.
   */
  public function getMasterfileUri($cid) {
    return $this->getExportCaseDirectory($cid) . '/' . self::MASTERFILE;
  }

  /**
   * Creates a local directory.
   *
   * @param string $directory
   *   The local directory.
   *
   * @return bool
   *   TRUE if created successfully, FALSE otherwise.
   */
  protected function createLocalDirectory($directory) {
    if (!file_prepare_directory($directory, FILE_CREATE_DIRECTORY)) {
      return FALSE;
    }
    file_create_htaccess($directory, TRUE);
    return TRUE;
  }


  /**
   * Initializes the Controller Interface.
   *
   * Creates the local directories that will store the export/import case files.
   */
  public function initialize() {
    $directories = array(
      self::WEAP_DIR,
      self::WEAP_DIR_EXPORT,
      self::WEAP_DIR_IMPORT,
    );
    foreach ($directories as $directory) {
      if ($this->createLocalDirectory($directory) === FALSE) {
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Checks that the connection settings are correct.
   *
   * @return bool
   *   TRUE if settings are correct, FALSE otherwise.
   */
  public function checkConnectionSettings() {

    return $this->client->testConnection();

  }

  /**
   * Initializes remote directories.
   */
  public function initializeRemoteDirectories() {
    $this->client->initDirectories();
  }

  /**
   * Generates the master file ready for export to S-WEAP.
   *
   * @param int $cid
   *   The Case ID: nid.
   * @param string $status
   *   The contents of the STATUS file.
   * @param string $masterfile_contents
   *   The contents of the WEAP master file.
   *
   * @return string|bool
   *   The case path if generated successfully, FALSE otherwise.
   */
  public function generateMasterFile($cid, $status = 'NEW', $masterfile_contents = '') {
    // Create case directory.
    $case_path = self::WEAP_DIR_EXPORT . '/' . $cid;
    if ($this->createLocalDirectory($case_path) === FALSE) {
      return FALSE;
    }

    // Create the status FILE.
    $status_file = $case_path . '/' . self::STATUS;
    if (file_unmanaged_save_data($status, $status_file, FILE_EXISTS_REPLACE) === FALSE) {
      return FALSE;
    }

    // Create the Master File.
    $master_file = $case_path . '/' . self::MASTERFILE;
    if (file_unmanaged_save_data($masterfile_contents, $master_file, FILE_EXISTS_REPLACE) === FALSE) {
      return FALSE;
    }

    // If created successfully, return case_path.
    return $case_path;
  }

  /**
   * Exports the 3 Case files to the S-WEAP.
   *
   * @param int $cid
   *   The Case ID: nid.
   * @param string $status
   *   The contents of the STATUS file.
   *
   * @return bool
   *   TRUE if succeeds, FALSE otherwise.
   */
  public function exportCaseStudy($cid, $status = 'NEW', $masterfile_contents = '') {
    // First Export Case Study.
    $case_path = $this->generateMasterFile($cid, $status, $masterfile_contents);
    if ($case_path !== FALSE) {
      // Now export all directory structure.
      // Create Case directory.
      if ($this->client->createCaseDirectory($cid) !== FALSE) {

        // Files to export to WebDAV Server.
        $case_files = array(
          self::STATUS => $case_path . '/' . self::STATUS,
          self::MASTERFILE => $case_path . '/' . self::MASTERFILE,
        );

        foreach ($case_files as $filename => $filepath) {
          $contents = file_get_contents($filepath);
          $webdav_path = $cid;
          try {
            $this->client->exportFile($filename, $contents, $webdav_path);
          }
          catch (\Exception $ex) {
            return FALSE;
          }
        }
        return TRUE;
      }
    }
  }

  /**
   * Imports the output files for a case from S-WEAP.
   *
   * @param int $cid
   *   The Case ID: nid.
   *
   * @return bool|mixed
   *   TRUE if imported correctly, FALSE otherwise.
   */
  public function importCaseStudy($cid) {
    $case_path = $this->createImportDirectory($cid);

    // Get file to import.
    try {
      $contents = $this->client->importFile("{$cid}.zip", $cid);
    }
    catch (\Exception $ex) {
      return FALSE;
    }
    // Saving file.
    $file_path = $case_path . "/{$cid}.zip";
    file_unmanaged_save_data($contents, $file_path);

    return $file_path;
  }

  /**
   * Creates the Case Import Directory.
   *
   * @param int $cid
   *   The Case ID.
   *
   * @return bool|string
   *   The Case Import Path or FALSE.
   */
  public function createImportDirectory($cid) {
    $this->initialize();

    // Create case directory inside the import directory.
    $case_path = self::WEAP_DIR_IMPORT . '/' . $cid;

    // Clean up the case directory first.
    // file_unmanaged_delete_recursive($case_path);
    if ($this->createLocalDirectory($case_path) === FALSE) {
      return FALSE;
    }
    return $case_path;
  }

  /**
   * Lists all the Case Studies available for Import.
   *
   * @return array
   *   Returns an array of the form:
   *   [
   *    cid => cid.zip
   *   ]
   */
  public function listCaseStudiesAvailableForImport() {
    // Obtains the list all files an directories on the Webdav server.
    $hrefs = $this->client->listImportDirectory();
    $import_path = $this->client->getImportPath();
    $valid_cases = [];

    foreach ($hrefs as $href) {
      // Only search for zip files.
      if (substr($href, -3) === 'zip') {

        // Only take entries of the form:
        // http://<import_path>/<CID>/<CID>.zip
        $file = pathinfo($href);
        $cid = intval($file['filename']);

        if ($cid !== 0 && $cid == $file['filename']) {
          $valid_cases[$cid] = $href;
        }
      }
    }
    return $valid_cases;
  }

}
