<?php
/**
 * @file
 * Provides an interface to execute models using Batch API.
 */

use Drupal\dss_magdalena\DSS\Models\SimaModelExecutorController;

/**
 * Provides a batch set to execute models using Batch API.
 *
 * @param string $model_machine_name
 *   The model machine name.
 * @param int $case_id
 *   The Case ID (nid).
 * @param bool $process_datastore
 *   Either to process or not to process the Datastore.
 *
 * @return array
 *   The batch set ready to execute using Batch API.
 */
function dss_engine_model_executor_batch_process($model_machine_name, $case_id, $process_datastore = FALSE) {
  // Name of the function to process will depend on the model name.
  // For example, for WEAP: 'dss_engine_execute_model_weap_batch_process'.
  // $model_batch_process = 'dss_engine_execute_model_' .
  // strtolower($model_machine_name) . '_batch_process';.
  $model_batch_process = 'dss_engine_execute_model_processing_batch_process';

  // Implementing Batch API.
  $batch = array(
    'title' => t('Executing model %model', array('%model' => strtoupper($model_machine_name))),
    'operations' => array(
      array(
        'dss_engine_execute_model_initialize_batch_process', array(
          $model_machine_name,
          $case_id,
          $process_datastore,
        ),
      ),
      array($model_batch_process, array($model_machine_name, $case_id)),
      array('dss_engine_execute_model_finalize_batch_process', array(
        $model_machine_name,
        $case_id,
      ),
      ),
    ),
    'finished' => 'dss_engine_execute_case_process_finished',
    'init_message' => t('Executing model %model', array('%model' => $model_machine_name)),
    'progress_message' => t('Processed @current tasks out of @total.'),
    'error_message' => t('Error executing model %model.', array('%model' => $model_machine_name)),
    'file' => drupal_get_path('module', 'dss_engine') . '/dss_engine.model_execute.inc',
  );
  return $batch;
}

/**
 * Batch process that initializes The execution of a model.
 *
 * @param string $model_machine_name
 *   The model machine name.
 * @param int $case_id
 *   The Case ID.
 * @param bool $process_datastore
 *   Either to process or not to process the Datastore.
 * @param array $context
 *   The context array.
 */
function dss_engine_execute_model_initialize_batch_process($model_machine_name, $case_id, $process_datastore, &$context) {
  if ($executor = SimaModelExecutorController::load($model_machine_name, $case_id)) {
    $uuid = $executor->getUuid();
    $context['results']['uuid'] = $uuid;
    $executor->setProcessDatastore($process_datastore);
    $executor->preInitialize();
    $executor->initialize();
  }
}

/**
 * Batch process for processing a single item in a model.
 *
 * @param string $model_machine_name
 *   The model machine name.
 * @param int $case_id
 *   The Case ID.
 * @param array $context
 *   The context array.
 */
function dss_engine_execute_model_processing_batch_process($model_machine_name, $case_id, &$context) {
  $uuid = isset($context['results']['uuid']) ? $context['results']['uuid'] : FALSE;
  if (!$uuid) {
    return;
  }
  if ($executor = SimaModelExecutorController::load($model_machine_name, $case_id, $uuid)) {
    $items = $executor->getNumberOfItems();

    // Initializing the sandbox.
    if (!isset($context['sandbox']['progress'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['current'] = 0;
      $context['sandbox']['max'] = $items;
    }

    // Processing a single item from the model.
    $key = $context['sandbox']['current'];
    if ($item = $executor->getItem($key)) {
      $success = $executor->processItem($item);
    }

    // Store results for post-processing in the finished callback.
    $context['results']['items'][$key] = array(
      'item' => $key,
      'success' => isset($success) ? $success : FALSE,
    );

    // Updating progress information.
    $context['sandbox']['progress']++;
    $context['sandbox']['current']++;
    $context['message'] = t('Now processing item "%label" for model "%model".', array(
      '%label' => isset($item) ? $item->label() : $key,
      '%model' => $model_machine_name,
    ));

    // Make sure this never goes forever.
    if ($context['sandbox']['progress'] > $context['sandbox']['max']) {
      $context['sandbox']['progress'] = $context['sandbox']['max'];
    }

    // Inform the batch engine that we are not finished,
    // and provide an estimation of the completion level we reached.
    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
  }
}

/**
 * Batch process that finalizes The execution of a model.
 *
 * @param string $model_machine_name
 *   The model machine name.
 * @param int $case_id
 *   The Case ID.
 * @param array $context
 *   The context array.
 */
function dss_engine_execute_model_finalize_batch_process($model_machine_name, $case_id, &$context) {
  $uuid = isset($context['results']['uuid']) ? $context['results']['uuid'] : FALSE;
  if (!$uuid) {
    return;
  }
  if ($executor = SimaModelExecutorController::load($model_machine_name, $case_id, $uuid)) {
    $context['message'] = t('Saving Output Variables for model "%model".', array(
      '%model' => $model_machine_name,
    ));
    $executor->saveOutputVariables();
    $context['results']['model_machine_name'] = $model_machine_name;
    $context['results']['case_id'] = $case_id;
  }
}

/**
 * Batch 'finished' callback.
 *
 * @param array $success
 *   The success array.
 * @param array $results
 *   The results array.
 * @param array $operations
 *   The operations array.
 */
function dss_engine_execute_case_process_finished($success, $results, $operations) {
  // @TODO: Complete showing the results.
  if ($success) {
    $uuid = isset($results['uuid']) ? $results['uuid'] : FALSE;
    if (!$uuid) {
      return;
    }
    $model_machine_name = $results['model_machine_name'];
    $case_id = $results['case_id'];
    if ($executor = SimaModelExecutorController::load($model_machine_name, $case_id, $uuid)) {
      $executor->getSummary($results);
    }
  }

}
