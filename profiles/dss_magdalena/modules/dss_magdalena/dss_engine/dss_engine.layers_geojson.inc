<?php
/**
 * @file
 * Provides access to GeoJson Layers.
 */

use Drupal\dss_magdalena\DSS\Entity\SimaLayer;
use Drupal\dss_magdalena\DSS\Entity\SimaResource;

/**
 * Returns the Resource DataStore in JSON format.
 *
 * @param int $resource_id
 *   The Resource ID.
 *
 * @return string
 *   The Resource Data in JSON.
 */
function dss_engine_resource_data_json($resource_id) {
  $data = NULL;
  if ($resource = SimaResource::load($resource_id)) {
    $layer = $resource->getLayer();

    // Get datastore.
    $datastore = $resource->getDataStoreResultsNew();

    $data = [
      'layer' => $resource->getOriginalGeoJsonLayer(),
      'datastore' => json_encode($datastore),
    ];
  }
  return json_encode($data);
}

/**
 * Returns the Resource DataStore in JSON format.
 *
 * @param string $layer_name
 *   The layer's machine name.
 *
 * @return string
 *   The GeoJson layer, if it matches the layer machine name, NULL otherwise.
 */
function dss_engine_layer_geojson($layer_name) {
  $data = NULL;

  if (substr($layer_name, -8) === '.geojson') {
    $layer_machine_name = str_replace('.geojson', '', $layer_name);
    if ($layer = SimaLayer::loadByMachineName($layer_machine_name)) {
      $layer_data = $layer->getOriginalGeoJson();
    }

    $data = [
      'layer' => isset($layer_data) ? $layer_data : NULL,
    ];
  }
  return json_encode($data);
}
