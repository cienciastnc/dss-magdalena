<?php
/**
 * @file
 * Overrides theme functions defined by other modules.
 */

use Drupal\dss_magdalena\DSS\Entity\SimaProductiveLandUseProjectVariant;
use Drupal\dss_magdalena\DSS\Entity\SimaCatchments;
use Drupal\dss_magdalena\DSS\Entity\Term\SimaLandUseType;

/**
 * Replacement for theme_multifield_table_multiple_value_fields().
 *
 * Each field is printed in a separate cell.
 */
function theme_dss_engine_multifield_table_multiple_value_fields($variables) {
  global $base_path;
  $original_multifield_theme = DRUPAL_ROOT . $base_path . drupal_get_path('module', 'multifield_table') . '/theme/theme.inc';
  include_once $original_multifield_theme;

  // Only use this if we are dealing with field for productive land use.
  // In all other cases, just use the default theme function.
  if ($variables['element']['#field_name'] != SimaProductiveLandUseProjectVariant::FIELD_LAND_USE) {
    return theme_multifield_table_multiple_value_fields($variables);
  }

  // Obtain a list of all Agricultural Land Types for later use.
  $agricultural_land_types = SimaLandUseType::getAgriculturalLandUseTypes();
  $agricultural_tids = [];
  foreach ($agricultural_land_types as $land_type) {
    $agricultural_tids[] = $land_type->getId();
  }

  $element = $variables['element'];
  $output = '';

  if (isset($element['#cardinality']) && ($element['#cardinality'] > 1 || $element['#cardinality'] == FIELD_CARDINALITY_UNLIMITED)) {
    $table_id = drupal_html_id($element['#field_name'] . '_values');
    $order_class = $element['#field_name'] . '-delta-order';
    $required = !empty($element['#required']) ? '<span class="form-required" title="' . t('This field is required.') . '">*</span>' : '';

    $rows = array();

    // Sort items according to '_weight' (needed when the form comes back after
    // preview or failed validation).
    $items = array();
    foreach (element_children($element) as $key) {
      if ($key === 'add_more') {
        $add_more_button = &$element[$key];
      }
      else {
        $items[] = &$element[$key];
      }
    }
    usort($items, '_field_sort_items_value_helper');

    $header = array(
      array(
        'data' => '<label>' . t('!title: !required', array('!title' => $element['#title'], '!required' => $required)) . "</label>",
        'class' => array('field-label'),
      ),
    );
    $id_fields = array(
      '#type' => 'container',
      '#attributes' => array(),
    );

    // Detecting the catchment.
    $first_item = reset($items);
    $catchment_id = $first_item['#entity']->__extra['catchment'];
    if ($catchment = SimaCatchments::load($catchment_id)) {
      $catchment_area = $catchment->getCatchmentArea();
      $percentages = $catchment->getConciseLandUsePercentages();
    }

    // Add the items as table rows.
    foreach ($items as $key => $item) {
      // Removing actions.
      unset($item['actions']);
      // Make sure _weight comes after actions.
      $item['_weight']['#weight'] = 101;
      uasort($item, 'element_sort');
      $item['_weight']['#attributes']['class'] = array($order_class);
      $cells = array(
        array('data' => ''),
      );
      foreach (element_children($item) as $field_name) {
        if ($field_name != 'id') {
          // Only add the header once.
          if ($key == 0) {
            $header[] = array(
              'data' => '<label>' . t('!title', array('!title' => _multifield_table_get_title($item[$field_name]))) . '</label>',
              'class' => array('field-label'),
            );
          }
          $cells[] = array('data' => $item[$field_name]);
        }
        else {
          $id_fields[] = $item[$field_name];
        }
      }

      // Insert new rows into the cells array.
      $cells1 = array_slice($cells, 1, 1);
      $cells2 = array_slice($cells, 2, 1);
      $cells3 = array_slice($cells, 3);

      $header1 = array_slice($header, 1, 1);
      $header2 = array_slice($header, 2, 1);
      $header3 = array_slice($header, 3);

      // Removing Order column.
      unset($header3[1]);
      unset($cells3[1]);

      $land_use_ref = $cells[1]['data']['und']['#default_value'][0];

      $header_new1 = array(
        array(
          'data' => t('<label>Base (Km<sup>2</sup>)</label>'),
          'class' => array('field-label'),
        ),
        array(
          'data' => t('<label>Base (%)</label>'),
          'class' => array('field-label'),
        ),
      );
      $header_new2 = array(
        array(
          'data' => t('<label>Modified Land Use (Km<sup>2</sup>)</label>'),
          'class' => array('field-label'),
        ),
        array(
          'data' => t('<label>Incremented Area (Km<sup>2</sup>)</label>'),
          'class' => array('field-label'),
        ),
      );
      $header_new3 = array(
        array(
          'data' => t('<label>New Irrigated Area (Km<sup>2</sup>)</label>'),
          'class' => array('field-label'),
        ),
      );

      $headers = array_merge(
        $header1,
        $header_new1,
        $header2,
        $header_new2,
        $header3,
        $header_new3
      );

      unset($cells1[0]['data']['und']['#title']);
      unset($cells2[0]['data']['und'][0]['value']['#title']);
      unset($cells3[0]['data']['und'][0]['value']['#title']);
      $land_use_area = $percentages[$land_use_ref] * floatval($catchment_area) / 100;
      $new_percentage = isset($cells2[0]['data']) ? $cells2[0]['data']['und'][0]['value']['#default_value'] : 0;
      $new_land_use_area = floatval($new_percentage) * floatval($catchment_area) / 100;
      $new_percentage_irrigated_area = isset($cells2[1]['data']) ? $cells2[1]['data']['und'][0]['value']['#default_value'] : 0;
      $land_type = isset($cells1[0]['data']) ? $cells1[0]['data']['und']['#default_value'][0] : 0;

      // Only Agricultural land types can have % Irrigated Area.
      if (!in_array($land_type, $agricultural_tids)) {
        $cells3[0]['data']['#attributes']['class'][] = 'form-invisible';
      }

      $cells = array_merge(
        $cells1,
        array(
          array(
            'data' => number_format($land_use_area, 2, '.', ''),
            'class' => array(
              'field-prod-land-use-base-area',
              'field-prod-land-use-justified-right',
            ),
          ),
          array(
            'data' => number_format($percentages[$land_use_ref], 2, '.', ''),
            'class' => array(
              'field-prod-land-use-base-percentage-area',
              'field-prod-land-use-justified-right',
            ),
          ),
        ),
        $cells2,
        array(
          array(
            'data' => number_format($new_land_use_area, 2, '.', ''),
            'class' => array(
              'field-calculated-area',
              'field-prod-land-use-justified-right',
            ),
          ),
          array(
            'data' => number_format($new_land_use_area - $land_use_area, 2, '.', ''),
            'class' => array(
              'field-prod-land-use-incremented-area',
              'field-prod-land-use-justified-right',
            ),
          ),
        ),
        $cells3,
        array(
          array(
            'data' => number_format((floatval($new_land_use_area) - floatval($land_use_area)) * floatval($new_percentage_irrigated_area), 2, '.', ''),
            'class' => array(
              'field-prod-land-use-new-irrigated-area',
              'field-prod-land-use-justified-right',
            ),
          ),
        )
      );

      $rows[] = array(
        'data' => $cells,
      );
    }

    $output = array(
      '#prefix' => '<div class="form-item">',
      '#suffix' => '</div>',
    );
    $output['multifield_table'] = array(
      '#theme' => 'table',
      '#header' => $headers,
      '#rows' => $rows,
      '#attributes' => array(
        'id' => $table_id,
        'class' => array(
          'field-multiple-table',
        ),
      ),
    );
    if (!empty($element['#description'])) {
      $output[] = array(
        '#prefix' => '<div class="description">',
        '#suffix' => '</div>',
        '#markup' => $element['#description'],
      );
    }
    if (!empty($id_fields)) {
      $output[] = $id_fields;
    }

    $output = drupal_render($output);
  }
  else {
    foreach (element_children($element) as $key) {
      $output .= drupal_render($element[$key]);
    }
  }
  return $output;
}
