<?php
/**
 * @file
 * Implements Batch API to fill the Water Balance Datastore with data.
 */

use Drupal\dss_magdalena\DSS\Entity\SimaWaterBalance;

/**
 * Initializes the batch process for Water Balance processing.
 *
 * @param \Drupal\dss_magdalena\DSS\Entity\SimaWaterBalance $sima_water_balance
 *   The Water Balance to process.
 * @param string $action
 *   The action to take: 'INSERT' or 'UPDATE'.
 * @param array $context
 *   The context array.
 */
function dss_engine_water_balance_initialize_batch_process(SimaWaterBalance $sima_water_balance, $action, &$context) {
  // @TODO: Initialize calculations.
}

/**
 * Process Water Balance INPUT variable.
 *
 * @param \Drupal\dss_magdalena\DSS\Entity\SimaWaterBalance $sima_water_balance
 *   The Water Balance to process.
 * @param string $action
 *   The action to take: 'INSERT' or 'UPDATE'.
 * @param array $context
 *   The context array.
 */
function dss_engine_water_balance_input_batch_process(SimaWaterBalance $sima_water_balance, $action, &$context) {
  $context['message'] = t('Finished Calculating Water Balance Input for "%name".', array(
    '%name' => $sima_water_balance->getTitle(),
  ));
  $case_study_id = $sima_water_balance->getCaseStudy()->getId();
  $water_balance_type_id = $sima_water_balance->getWaterBalanceType()->tid;
  $model_id = $sima_water_balance->getModel()->getId();
  $values = dss_engine_water_balance_calculate_values($water_balance_type_id, $model_id, $case_study_id, 'input');
  // Insert data into table.
  if (!empty($values)) {
    $sima_water_balance->setWaterBalanceInput($values);
  }
}

/**
 * Process Water Balance OUTPUT variable.
 *
 * @param \Drupal\dss_magdalena\DSS\Entity\SimaWaterBalance $sima_water_balance
 *   The Water Balance to process.
 * @param string $action
 *   The action to take: 'INSERT' or 'UPDATE'.
 * @param array $context
 *   The context array.
 */
function dss_engine_water_balance_output_batch_process(SimaWaterBalance $sima_water_balance, $action, &$context) {
  $context['message'] = t('Finished Calculating Water Balance Output for "%name".', array(
    '%name' => $sima_water_balance->getTitle(),
  ));
  $case_study_id = $sima_water_balance->getCaseStudy()->getId();
  $water_balance_type_id = $sima_water_balance->getWaterBalanceType()->tid;
  $model_id = $sima_water_balance->getModel()->getId();
  $values = dss_engine_water_balance_calculate_values($water_balance_type_id, $model_id, $case_study_id, 'output');
  // Insert data into table.
  if (!empty($values)) {
    $sima_water_balance->setWaterBalanceOutput($values);
  }
}

/**
 * Process Water Balance STORAGE variable.
 *
 * @param \Drupal\dss_magdalena\DSS\Entity\SimaWaterBalance $sima_water_balance
 *   The Water Balance to process.
 * @param string $action
 *   The action to take: 'INSERT' or 'UPDATE'.
 * @param array $context
 *   The context array.
 */
function dss_engine_water_balance_storage_batch_process(SimaWaterBalance $sima_water_balance, $action, &$context) {
  $context['message'] = t('Finished Calculating Water Balance Storage for "%name".', array(
    '%name' => $sima_water_balance->getTitle(),
  ));
  $case_study_id = $sima_water_balance->getCaseStudy()->getId();
  $water_balance_type_id = $sima_water_balance->getWaterBalanceType()->tid;
  $model_id = $sima_water_balance->getModel()->getId();
  $values = dss_engine_water_balance_calculate_values($water_balance_type_id, $model_id, $case_study_id, 'storage');
  // Insert data into table.
  if (!empty($values)) {
    $sima_water_balance->setWaterBalanceStorage($values);
  }
}

/**
 * Process Water Balance CONSUMPTION variable.
 *
 * @param \Drupal\dss_magdalena\DSS\Entity\SimaWaterBalance $sima_water_balance
 *   The Water Balance to process.
 * @param string $action
 *   The action to take: 'INSERT' or 'UPDATE'.
 * @param array $context
 *   The context array.
 */
function dss_engine_water_balance_consumption_batch_process(SimaWaterBalance $sima_water_balance, $action, &$context) {
  $context['message'] = t('Finished Calculating Water Balance Consumption for "%name".', array(
    '%name' => $sima_water_balance->getTitle(),
  ));
  $case_study_id = $sima_water_balance->getCaseStudy()->getId();
  $water_balance_type_id = $sima_water_balance->getWaterBalanceType()->tid;
  $model_id = $sima_water_balance->getModel()->getId();
  $values = dss_engine_water_balance_calculate_values($water_balance_type_id, $model_id, $case_study_id, 'consumption');
  // Insert data into table.
  if (!empty($values)) {
    $sima_water_balance->setWaterBalanceConsumption($values);
  }
}

/**
 * Finalizes the batch process for Water Balance processing.
 *
 * @param \Drupal\dss_magdalena\DSS\Entity\SimaWaterBalance $sima_water_balance
 *   The Water Balance to process.
 * @param string $action
 *   The action to take: 'INSERT' or 'UPDATE'.
 * @param array $context
 *   The context array.
 *
 * @TODO: Check if this is needed or delete it.
 */
function dss_engine_water_balance_finalize_batch_process(SimaWaterBalance $sima_water_balance, $action, &$context) {
  $context['message'] = t('Finished tasks for "%name".', array(
    '%name' => $sima_water_balance->getTitle(),
  ));
  // @TODO: Calculate Water Balance final tasks.
}

/**
 * Finishes the batch process for Water Balance. Batch 'finished' callback.
 *
 * @param array $success
 *   The success array.
 * @param array $results
 *   The results array.
 * @param array $operations
 *   The operations array.
 *
 * @TODO: Check if this is needed or delete it.
 */
function dss_engine_water_balance_process_finished($success, $results, $operations) {
  // @TODO: Perform final summary.

}

/**
 * Calculate the values of the water balance to graph it.
 *
 * @param int $water_balance_type_id
 *   The Water Balance Type Id.
 * @param int $model_id
 *   The Model Id.
 * @param int $case_study_id
 *   The Case Study Id.
 * @param int $type
 *   The Type of resource.
 *
 * @return array
 *   List of values.
 */
function dss_engine_water_balance_calculate_values($water_balance_type_id, $model_id, $case_study_id, $type) {
  $water_balance_template = SimaWaterBalance::getWaterBalanceTemplateByModelAndType($model_id, $water_balance_type_id);
  switch ($type) {
    case 'input':
      $water_balance_template_factor = $water_balance_template->getWaterBalanceInput();
      break;

    case 'output':
      $water_balance_template_factor = $water_balance_template->getWaterBalanceOutput();
      break;

    case 'storage':
      $water_balance_template_factor = $water_balance_template->getWaterBalanceStorage();
      break;

    case 'consumption':
      $water_balance_template_factor = $water_balance_template->getWaterBalanceConsumption();
      break;
  }
  $resources_list = SimaWaterBalance::listWaterBalanceResources($water_balance_type_id, $model_id, $case_study_id, $type);
  $num = count($water_balance_template_factor);
  for ($i = 0; $i < $num; $i++) {
    if ($resources_list[$type][$i] === FALSE) {
      continue;
    }
    $fields = 'branchid,branch,level_1,level_2,level_3,level_4,year,timestep,variable,scenario';
    $items = $resources_list[$type][$i]->getDataStoreResults($fields);
    $values = [];
    foreach ($items['items'] as $item) {
      $value = $item['value'] * $water_balance_template_factor[$i]->getFactor();
      $values[] = [
        'branchid' => $item['branchid'],
        'branch'   => $item['branch'],
        'level_1'  => $item['level_1'],
        'level_2'  => $item['level_2'],
        'level_3'  => $item['level_3'],
        'level_4'  => $item['level_4'],
        'year'     => $item['year'],
        'timestep' => $item['timestep'],
        'variable' => $item['variable'],
        'units'    => 'Million m^3',
        'scenario'    => $item['scenario'],
        // 'alias'  => $item['alias'],.
        'value'    => $value,
      ];
    }
  }
  return $values;
}
