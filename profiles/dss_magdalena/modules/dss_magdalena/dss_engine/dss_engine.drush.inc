<?php
/**
 * @file
 * DSS Engine Drush Commands.
 */

use Drupal\dss_magdalena\DSS\Entity\SimaDefaultCaseStudy;
use Drupal\dss_magdalena\DSS\Entity\SimaModel;

/**
 * Implements hook_drush_command().
 *
 * @See drush_parse_command() for a list of recognized keys.
 */
function dss_engine_drush_command() {
  $items = array();
  $items['dss-engine-model-execute'] = array(
    'description' => 'Executes a model for a specific case study.',
    'callback' => 'drush_dss_engine_model_execute',
    "arguments" => array(
      "model" => "The Model 'machine name' that will be executed.",
      "cid" => "The Case ID (This is the nid for the Case Study)",
    ),
    "options" => array(
      "skip_transfer" => array(
        "description" => "Skip transfer to WEAP Server if executing WEAP model.",
        "example_value" => "1",
      ),
      "process_datastore" => array(
        "description" => "Process datastore when importing model outputs.",
        "example_value" => "1",
      ),
      "skip_process_models" => array(
        "description" => "If true, do not execute it at all.",
        "example_value" => "1",
      ),
    ),
    'examples' => array(
      'drush dss-engine-model-execute eloha 394' => 'Executes a DSS model "eloha" for a case study with nid = 394.',
      'drush dss-engine-model-execute weap 394 --skip_transfer=1' => 'Executes a DSS model "weap" for a case study with nid = 394 and do not transfer files to S-WEAP server.',
      'drush dss-engine-model-execute weap 394 --skip_transfer=1 --process_datastore=1' => 'Executes a DSS model "weap" for a case study with nid = 394 and do not transfer files to S-WEAP server, but process the datastore.',
    ),
    'aliases' => array('dss-me', 'dss-model-exe', 'dss-model-execute'),
  );
  $items['dss-engine-save-weap_outputs'] = array(
    'description' => 'Executes the Import Queue to save WEAP Outputs for a specific case study.',
    'callback' => 'dss_engine_save_weap_outputs',
    "arguments" => array(
      "cid" => "The Case ID (This is the nid for the Case Study)",
    ),
    'examples' => array(
      'drush dss-engine-save-weap_outputs 394' => 'Saves and executes queue import for case study with nid = 394.',
      'drush dss-engine-save-weap_outputs baseline' => 'Saves and executes queue import for baseline case study.',
    ),
    'aliases' => array('dss-swo', 'dss-save-weap-outputs'),
  );

  $items['dss-engine-create-objectives'] = array(
    'description' => 'Creates fake objectives for a specific case study.',
    'callback' => 'dss_engine_create_objectives_for_case_study',
    "arguments" => array(
      "cid" => "The Case ID (This is the nid for the Case Study)",
    ),
    'examples' => array(
      'drush dss-engine-create-objectives 394' => 'Creates objectives for case study with nid = 394.',
      'drush dss-engine-create-objectives baseline' => 'Creates objectives for baseline case study.',
    ),
    'aliases' => array('dss-co', 'dss-create-obj'),
  );

  return $items;
}

/**
 * Implements hook_drush_help().
 */
function dss_engine_drush_help($section) {
  switch ($section) {
    case 'drush:dss-engine-model-execute':
      return dt('Executes a model for a particular case study.');
  }
}

/**
 * Callback function for drush command dss-engine-model-execute.
 *
 * @param string $model
 *   The model machine name.
 * @param int $cid
 *   The Case Study Node ID (nid).
 *
 * @return bool
 *   The Drush status.
 */
function drush_dss_engine_model_execute($model, $cid) {
  // Read the 'skip_process_models' option, if given.
  $skip_process_models = drush_get_option("skip_process_models");
  if ($skip_process_models) {
    return drush_log(dt('Skipped executing "!model" model for Case Study ID = !case', array(
      '!model' => $model,
      '!case' => $cid,
    )), 'ok');
  }
  $valid_cids = [
    'extreme',
    'baseline',
  ];
  // Perform basic validations.
  if (!SimaModel::isValidModel($model)) {
    return drush_set_error(dt('Please insert a valid Model machine name.'));
  }
  if (!is_numeric($cid) && !in_array($cid, $valid_cids)) {
    return drush_set_error(dt('Please insert a valid integer for cid (Case Study node ID = !cid)', array(
      '!cid' => $cid,
    )));
  }
  // Read the 'skip_transfer' option, if given.
  $skip_transfer = drush_get_option("skip_transfer");

  // Read the 'skip_process_models' option, if given.
  $process_datastore = drush_get_option("process_datastore");

  $case = SimaDefaultCaseStudy::load($cid);

  // Check that the cid given is a for an existent case study.
  if ($case) {
    if ($skip_transfer) {
      // If we are not sending data to S-WEAP (usually because this is the
      // executing during build time) then just set the model status to 100
      // so they can be processed inmediately.
      $case->setModelingStatus($model, 100);
    }
    $status = $case->getCaseOutputs()->getModelingStatus($model);
    // Only process the models if it is ready for processing.
    if ($status == 100) {
      // Process the model using Batch API.
      module_load_include('inc', 'dss_engine', 'dss_engine.model_execute');
      $batch = dss_engine_model_executor_batch_process($model, $case->getId(), $process_datastore);

      // If it is WEAP and we set to skip transfer to S-WEAP.
      if ($model == SimaModel::MODEL_WEAP && $skip_transfer) {
        unset($batch['operations'][2]);
      }

      $batch['progressive'] = FALSE;
      batch_set($batch);
      // Start processing the batch operations.
      drush_backend_batch_process();

      // If we are executing WEAP, we also have to import all case outputs.
      if ($model == SimaModel::MODEL_WEAP) {
        if ($skip_transfer) {
          module_load_include('inc', 'dss_import', 'includes/dss_import_models_configuration');
          $case->setModelingStatus(SimaModel::MODEL_WEAP, SimaModel::STATUS_PROCESSING);
          dss_import_weap_import_resources($case);
        }
        else {
          $case->import();
        }
      }

      // Print results.
      drush_log(dt('Finished executing "!model" model for  !case', array(
        '!model' => $model,
        '!case' => $case->getTitle(),
      )), 'ok');
    }
    else {
      return drush_set_error(dt('Model !model for Case Study ID = !cid cannot be executed. Status = !status', array(
        '!model' => $model,
        '!cid' => $case->getId(),
        '!status' => $status,
      )));
    }
  }
}

/**
 * Saves WEAP Case Outputs by executing the import queue.
 *
 * @param string $cid
 *   The Case ID.
 *
 * @return bool
 *   The drush status.
 */
function dss_engine_save_weap_outputs($cid) {
  $case = SimaDefaultCaseStudy::load($cid);
  if ($case) {
    module_load_include('inc', 'dss_import', 'includes/dss_import_models_configuration');
    if (dss_import_saving_imported_ouputs_weap($case)) {
      $case->setModelingStatus(SimaModel::MODEL_WEAP, SimaModel::STATUS_OK);
      // Print results.
      drush_log(dt('Finished importing ouputs from "!model" model for  !case', array(
        '!model' => SimaModel::MODEL_WEAP,
        '!case' => $case->getTitle(),
      )), 'ok');
    }
    else {
      return drush_set_error(dt('Cannot execute importing queue to save weap outputs for Case Study ID = !cid.', array(
        '!cid' => $case->getId(),
      )));
    }
  }
}

/**
 * Saves WEAP Case Outputs by executing the import queue.
 *
 * @param string $cid
 *   The Case ID.
 *
 * @return bool
 *   The drush status.
 */
function dss_engine_create_objectives_for_case_study($cid) {
  if ($case = SimaDefaultCaseStudy::load($cid)) {
    $case->createObjectives();
    drush_log(dt('Finished creating objectives for  !case', array(
      '!case' => $case->getTitle(),
    )), 'ok');
  }
  else {
    return drush_set_error(dt('Cannot create objectives for Case Study ID = !cid.', array(
      '!cid' => $case->getId(),
    )));
  }
}
