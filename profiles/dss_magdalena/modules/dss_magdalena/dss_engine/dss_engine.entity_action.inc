<?php
/**
 * @file
 * Defines Drupal Entity hooks for the DSS Magdalena.
 */

use Drupal\dss_magdalena\DSS\Entity\SimaAlternative;
use Drupal\dss_magdalena\DSS\Entity\SimaCaseOutputs;
use Drupal\dss_magdalena\DSS\Entity\SimaCaseStudy;
use Drupal\dss_magdalena\DSS\Entity\SimaDataset;
use Drupal\dss_magdalena\DSS\Entity\SimaIntegralPlan;
use Drupal\dss_magdalena\DSS\Entity\SimaResource;
use Drupal\dss_magdalena\DSS\Entity\SimaScenario;
use Drupal\dss_magdalena\DSS\Entity\SimaWaterBalance;
use Drupal\dss_magdalena\WeapController;

/**
 * Implements hook_node_insert().
 */
function dss_engine_node_insert($node) {
  switch ($node->type) {
    case SimaCaseStudy::BUNDLE:
      if (class_exists('\Drupal\dss_magdalena\DSS\Entity\SimaCaseOutputs')) {
        SimaCaseOutputs::loadByCaseStudyCreateIfNeeded($node->nid);
      }
      break;

    case SimaWaterBalance::BUNDLE:
      if ($water_balance = SimaWaterBalance::load($node)) {
        // Create a Batch API process that will run to store data into the
        // Water Balance Datastore.
        $water_balance->setWaterBalanceDataStore();
        $batch = dss_engine_water_balance_batch_process($water_balance, 'INSERT');
        batch_set($batch);
      }
      break;
  }
}

/**
 * Implements hook_node_update().
 */
function dss_engine_node_update($node) {
  switch ($node->type) {
    case SimaWaterBalance::BUNDLE:
      if ($water_balance = SimaWaterBalance::load($node)) {
        $water_balance->setWaterBalanceDataStore();
        // Create a Batch API process that will run to store data into the
        // Water Balance Datastore.
        $batch = dss_engine_water_balance_batch_process($water_balance, 'UPDATE');
        batch_set($batch);
      }
      break;
  }
}

/**
 * Implements hook_node_delete().
 */
function dss_engine_node_delete($node) {
  switch ($node->type) {
    case SimaCaseStudy::BUNDLE:
      if ($case = SimaCaseStudy::load($node->nid)) {
        if ($case->getDefaultCaseStudy()->isDefault()) {
          return;
        }
        if ($case_outputs = $case->getCaseOutputs()) {
          $resources = array_filter($case_outputs->getAllResources());
          foreach ($resources as $resource) {
            $resource->delete();
          }
          // Deleting case Outputs.
          $case_outputs->delete();
        }
      }

      // Delete the remaining input resources.
      $resources = array_filter(SimaResource::listByCaseStudy($node->nid));
      foreach ($resources as $resource) {
        if ($resource->getDataset()->getVariableStatus()->isInputAlternative()) {
          $resource->delete();
        }
      }
      break;

    case SimaCaseOutputs::BUNDLE:
      // Delete the Output Resources.
      if (class_exists('\Drupal\dss_magdalena\DSS\Entity\SimaCaseOutputs')) {
        if ($sima_case_outputs = SimaCaseOutputs::load($node->nid)) {
          // Delete case resource outputs.
          $resources = array_filter($sima_case_outputs->getAllResources());
          foreach ($resources as $resource) {
            $resource->delete();
          }
        }
      }
      break;

    case SimaWaterBalance::BUNDLE:
      if ($water_balance = SimaWaterBalance::load($node)) {
        $water_balance->deleteWaterBalanceDataStore();
      }
      break;
  }
}

/**
 * Implements hook_node_view().
 */
function dss_engine_node_view($node, $view_mode, $langcode) {
  if ($case_output = SimaCaseOutputs::load($node->nid)) {
    $case_study = $case_output->getCaseStudy();
    // Only progress if we have a full node view.
    if ($view_mode !== 'full') {
      return;
    }
    $weap = new WeapController();
    $masterfile_uri = $weap->getMasterfileUri($case_study->getId());

    // Only proceed if the masterfile has been created.
    if (file_exists($masterfile_uri)) {
      $uri = file_create_url($masterfile_uri);
      $node->content['masterfile'] = [
        '#markup' => l(t('Masterfile [XLS Format]'), $uri),
        '#weight' => 5,
      ];
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function dss_engine_form_node_delete_confirm_alter(&$form, &$form_state, $form_id) {
  global $user;
  $nid = $form['nid']['#value'];
  // If we are trying to delete an alternative.
  if ($alternative = \Drupal\dss_magdalena\DSS\Entity\SimaAlternative::load($nid)) {
    // If Im not the owner of this alternative and want to delete it.
    if ($user->uid != $alternative->getAuthor()->getIdentifier()) {
      $user_link = l($alternative->getAuthor()->label(), $alternative->getAuthor()->url->value());
      drupal_set_message(t('You are not the owner of this alternative so you cannot delete it. Current owner is !user', [
        '!user' => $user_link,
      ]), 'error');
    }

    // Check if other Integral Plans are using the same alternative.
    $integral_plans = \Drupal\dss_magdalena\DSS\Entity\SimaIntegralPlan::listEntities();
    $integral_plans_using_alternative = [];
    foreach ($integral_plans as $integral_plan) {
      $ip_uid = $integral_plan->getAuthor()->getIdentifier();
      // Check for alternatives.
      if ($hidropower = $integral_plan->getHydropowerAlternative()) {
        if ($hidropower->getId() == $nid) {
          $integral_plans_using_alternative[] = l($hidropower->getTitle(), '/node/' . $nid);
          if ($user->uid == $alternative->getAuthor()->getIdentifier() && $user->uid != $ip_uid) {
            $user_link = l($integral_plan->getAuthor()->label(), $integral_plan->getAuthor()->url->value());
            drupal_set_message(t('You cannot delete this alternative because user !user is using it in Integral Plan !plan.', [
              '!user' => $user_link,
              '!plan' => l($integral_plan->getTitle(), $integral_plan->getDrupalEntity()->url->value()),
            ]), 'error');
            drupal_goto($alternative->getDrupalEntity()->url->value());
          }
          continue;
        }
      }
      if ($prod_landuse = $integral_plan->getProductiveLandUseAlternative()) {
        if ($prod_landuse->getId() == $nid) {
          $integral_plans_using_alternative[] = l($prod_landuse->getTitle(), '/node/' . $nid);
          if ($user->uid == $alternative->getAuthor()->getIdentifier() && $user->uid != $ip_uid) {
            $user_link = l($integral_plan->getAuthor()->label(), $integral_plan->getAuthor()->url->value());
            drupal_set_message(t('You cannot delete this alternative because user !user is using it in Integral Plan !plan.', [
              '!user' => $user_link,
              '!plan' => l($integral_plan->getTitle(), $integral_plan->getDrupalEntity()->url->value()),
            ]), 'error');
            drupal_goto($alternative->getDrupalEntity()->url->value());
          }
          continue;
        }
      }
      if ($wetland_man = $integral_plan->getWetlandManagementAlternative()) {
        if ($wetland_man->getId() == $nid) {
          $integral_plans_using_alternative[] = l($wetland_man->getTitle(), '/node/' . $nid);
          if ($user->uid == $alternative->getAuthor()->getIdentifier() && $user->uid != $ip_uid) {
            $user_link = l($integral_plan->getAuthor()->label(), $integral_plan->getAuthor()->url->value());
            drupal_set_message(t('You cannot delete this alternative because user !user is using it in Integral Plan !plan.', [
              '!user' => $user_link,
              '!plan' => l($integral_plan->getTitle(), $integral_plan->getDrupalEntity()->url->value()),
            ]), 'error');
            drupal_goto($alternative->getDrupalEntity()->url->value());
          }
        }
      }
    }
    if (count($integral_plans_using_alternative) > 0) {
      $data['items'] = $integral_plans_using_alternative;
      $form['description1'] = [
        '#markup' => t('The following Integral Plans are using this alternative: !plans', [
          '!plans' => theme('item_list', $data),
        ]),
      ];
    }
  }
  if ($case = SimaCaseStudy::load($nid)) {
    if ($case->getDefaultCaseStudy()->isDefault()) {
      drupal_set_message(t('You cannot delete any of the default Case Studies.'), 'error');
      drupal_goto($case->getDrupalEntity()->url->value());
    }
  }
}

/**
 * Implements hook_form_alter().
 */
function dss_engine_form_node_form_alter(&$form, $form_state) {
  $type = $form['#node']->type;
  switch ($type) {
    case SimaCaseStudy::BUNDLE:
    case SimaIntegralPlan::BUNDLE;
    case SimaAlternative::PRODUCTIVE_LANDUSE_ALTERNATIVE:
    case SimaAlternative::HYDROPOWER_ALTERNATIVE:
    case SimaAlternative::WETLAND_MANAGEMENT_PROJECTS:
    case SimaScenario::CLIMATE_SCENARIO:
    case SimaScenario::POPULATION_SCENARIO:
    case SimaScenario::ENERGY_SCENARIO:
    case 'scenario':
      drupal_set_message(t('Advertencia: Si guardas estos cambios y este elemento ya ha sido utilizado en un Caso de Estudio que ya se ha simulado, vas a dejar Cajas de Datos Anómalas, de las cuales se pierde el orígen.
      Es mejor crear un clon y modificar los elementos para utilizarlo en un caso nuevo.'), 'warning');
      break;

    case SimaDataset::BUNDLE:
      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#access' => TRUE,
        '#value' => t('Guardar Variable'),
        '#weight' => 5,
        '#description' => t('Guardar sólo la información ingresada de la variable.'),
        '#submit' => [
          'inline_entity_form_trigger_submit',
          'node_form_submit',
        ],
        '#ief_submit_all' => TRUE,
      ];
      break;
  }
}


/**
 * Defines the Batch process for Water Balance Processing.
 *
 * @param \Drupal\dss_magdalena\DSS\Entity\SimaWaterBalance $sima_water_balance
 *   The Water Balance to process.
 * @param string $action
 *   The action to take: 'INSERT' or 'UPDATE'.
 *
 * @return array
 *   The Batch API Process definition.
 */
function dss_engine_water_balance_batch_process(SimaWaterBalance $sima_water_balance, $action = 'INSERT') {
  // Implementing Batch API.
  $batch = array(
    'title' => t('Processing Datastore for Water Balance %name', array(
      '%name' => $sima_water_balance->getTitle(),
    )),
    'operations' => array(
      array(
        'dss_engine_water_balance_initialize_batch_process',
        array($sima_water_balance, $action),
      ),
      array(
        'dss_engine_water_balance_input_batch_process',
        array($sima_water_balance, $action),
      ),
      array(
        'dss_engine_water_balance_output_batch_process',
        array($sima_water_balance, $action),
      ),
      array(
        'dss_engine_water_balance_storage_batch_process',
        array($sima_water_balance, $action),
      ),
      array(
        'dss_engine_water_balance_consumption_batch_process',
        array($sima_water_balance, $action),
      ),
      array(
        'dss_engine_water_balance_finalize_batch_process',
        array($sima_water_balance, $action),
      ),
    ),
    'finished' => 'dss_engine_water_balance_process_finished',
    'init_message' => t(
      'Initializing Data for Water Balance "%name"',
      array('%name' => $sima_water_balance->getTitle())
    ),
    'progress_message' => t('Processed @current tasks out of @total.'),
    'error_message' => t(
      'Error processing water balance "%name"',
      array('%name' => $sima_water_balance->getTitle())
    ),
    'file' => drupal_get_path('module', 'dss_engine') . '/dss_engine.water_balance_batch.inc',
  );
  return $batch;
}
