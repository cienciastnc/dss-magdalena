<?php
/**
 * @file
 * Evaluates Case Readiness.
 */

use Drupal\dss_magdalena\DSS\Entity\SimaCaseStudy;
use Drupal\dss_magdalena\DSS\Entity\SimaModel;
use Drupal\dss_magdalena\DSS\Entity\Term\SimaVariableStatus;

module_load_include('inc', 'dss_engine', 'dss_engine.model_execute');

/**
 * Evaluates Case Readiness.
 *
 * @param int $case_id
 *   The Case ID.
 * @param string $model_machine_name
 *   The Model machine_name.
 *
 * @return string
 *   The output.
 */
function dss_engine_evaluate_case_readiness($case_id, $model_machine_name) {
  $case = SimaCaseStudy::load($case_id);
  $status = $case->getModelingStatus($model_machine_name);

  // Validate the Case has all required data.
  $validation = $case->validate();
  if ($validation === FALSE) {
    $status = 0;
  }

  // Validate configuration file load.
  $configuration_file_is_loaded = SimaModel::isModelProperlyConfigured($model_machine_name);

  if ($status <= 100) {
    if ($validation) {
      $data = $case->isModelReadyForExecution($model_machine_name);
    }
    else {
      $data = [
        'unset_variables' => [],
        'required_variables' => [],
      ];
    }
    // Calculate new status.
    $unset = count($data['unset_variables']);
    $required = count($data['required_variables']);

    // If the status is not "PROCESSING" or "OK", then recalculate it based
    // on the new variables being set to check if it can be exported.
    $status = $required > 0 ? round(($required - $unset) * 100 / $required) : 0;
    $case->setModelingStatus($model_machine_name, $status);

  }
  $output = dss_engine_model_status_output($case, $model_machine_name, $status);
  $set_message_models = FALSE;
  $items = [];
  switch ($status) {
    case SimaModel::STATUS_OK:
      // Case has already been simulated and is ready for analysis.
      break;

    case SimaModel::STATUS_PROCESSING:
      // Case has already been sent for simulation.
      break;

    case 100:
      // Case is ready for simulation.
      if ($configuration_file_is_loaded) {
        $form = drupal_get_form('dss_engine_execute_case_form', $case, $model_machine_name);
        $output .= drupal_render($form);
      }
      else {
        $set_message_models = TRUE;
      }
      break;

    case ($status < 100):
    case 0:
      // Case is not ready for simulation.
      // Calculating the unset variables.
      $items['items'] = array();
      $items['title'] = t('Unset Variables');

      /** @var \Drupal\dss_magdalena\DSS\Entity\SimaDataset $unset_variable */
      foreach ($data['unset_variables'] as $unset_variable) {
        $items['items'][] = l($unset_variable->getTitle(), 'node/' . $unset_variable->getId());
      }

      // Fix item title according configuration file is loaded.
      if (!$configuration_file_is_loaded) {
        $set_message_models = TRUE;
      }
      break;
  }

  // Show configuration models.
  if ($set_message_models) {
    $configuration_path = '';
    $model = SimaModel::loadByMachineName($model_machine_name);
    $model_name = $model->getTitle();
    switch ($model_machine_name) {
      case SimaModel::MODEL_WEAP:
        $configuration_path = '';
        break;

      case SimaModel::MODEL_FRAGMENTATION:
      case SimaModel::MODEL_DOR_H:
      case SimaModel::MODEL_DOR_W:
      case SimaModel::MODEL_SEDIMENTS:
        $configuration_path = 'fragmentation_dorh';
        break;

      case SimaModel::MODEL_TERRITORIAL_FOOTPRINT:
        $configuration_path = 'territorial_footprint';
        break;

      case SimaModel::MODEL_ELOHA:
        $configuration_path = 'eloha';
        break;
    }
    // Link.
    $link = l($model_name . " Configuration", "/admin/config/dss-magdalena/settings/$configuration_path");
    $message = t("!model Model Configuration is not set correctly. Please set !configuration", [
      '!model' => $model_name,
      '!configuration' => $link,
    ]);
    $output .= '<br>' . $message;
  }

  // Show undefined variables.
  if (!empty($items)) {
    $output .= theme('item_list', $items);
  }

  return $output;
}

/**
 * Prints information about the modeling status for the particular case study.
 *
 * @param \Drupal\dss_magdalena\DSS\Entity\SimaCaseStudy $case
 *   The Case Study.
 * @param string $model_machine_name
 *   The model machine name.
 * @param int $status
 *   The status code.
 *
 * @return null|string
 *   The output to print.
 */
function dss_engine_model_status_output(SimaCaseStudy $case, $model_machine_name, $status) {
  if ($model = SimaModel::loadByMachineName($model_machine_name)) {
    $model_name = $model->getTitle();
  }
  $model_name = isset($model_name) ? $model_name : $model_machine_name;
  $output = theme('dss_engine_case_readiness',
    array(
      'case' => $case->getTitle(),
      'model' => $model_name,
      'status' => SimaModel::getStatusLabel($status, TRUE),
    ));

  // Initialize variable.
  $variable_status = '';

  switch ($model_machine_name) {
    case SimaModel::MODEL_WEAP:
      $variable_status = SimaVariableStatus::NAME_OUTPUT_WEAP;
      break;

    case SimaModel::MODEL_ELOHA:
      $variable_status = SimaVariableStatus::NAME_OUTPUT_ELOHA;
      break;

    case SimaModel::MODEL_TERRITORIAL_FOOTPRINT:
      $variable_status = SimaVariableStatus::NAME_OUTPUT_TERRITORIAL_FOOTPRINT;
      break;

    case SimaModel::MODEL_FRAGMENTATION:
      $variable_status = SimaVariableStatus::NAME_OUTPUT_FRAGMENTATION;
      break;

    case SimaModel::MODEL_DOR_H:
      $variable_status = SimaVariableStatus::NAME_OUTPUT_DOR_H;
      break;

    case SimaModel::MODEL_DOR_W:
      $variable_status = SimaVariableStatus::NAME_OUTPUT_DOR_W;
      break;

    case SimaModel::MODEL_SEDIMENTS:
      $variable_status = SimaVariableStatus::NAME_OUTPUT_SEDIMENTS;
      break;

  }

  $chart = array(
    'id' => 'sima',
    'type' => 'cause_effect_fixed',
    'models' => array(
      array(
        'title' => 'Modelo actual: ' . $model_name,
        'models' => [$model_machine_name],
        'descriptions' => dss_visualizations_model_descriptions($model_machine_name),
      ),
    ),
  );
  $output .= dss_visualizations_draw($chart);

  // @TODO: Use a template for this.
  if ($status === SimaModel::STATUS_OK) {
    // Model Outputs.
    if ($sima_variable_status = SimaVariableStatus::loadByTaxonomyTermName($variable_status)) {
      $path_info = pathinfo($sima_variable_status->getUrlAlias());
      $model_path = $path_info['basename'] . '-' . $sima_variable_status->getId();
      $url = "search/field_resources%3Afield_model_case_study_id/{$case->getId()}/field_variable_status/{$model_path}/type/dataset";
      $label = $case->getDrupalEntity()->label() . ' - ' . $model_name;
      $output .= t('<p><b>Model Outputs:</b></p><p>View Model Outputs for : !link</p>', array(
        '!link' => l($label, $url),
      ));
    }

    // Link to Eloha Model result.
    if ($model_machine_name == SimaModel::MODEL_ELOHA) {
      $label = $case->getDrupalEntity()->label() . ' - Eloha Graph';
      $eloha_link = l($label, "case/{$case->getId()}/model/eloha/graph/");
      $output .= t('<p><b>Model View:</b></p><p>View Model Graph for : !link</p>', array(
        '!link' => $eloha_link,
      ));
    }
  }

  return $output;
}

/**
 * Implements hook_form().
 */
function dss_engine_execute_case_form($form, &$form_state, $case, $model_machine_name) {
  $label = t('Execute Model');
  if ($model_machine_name == SimaModel::MODEL_WEAP) {
    $label = t('Send model to WEAP');
  }

  // Build the Form.
  $form = array();

  /** @var \Drupal\dss_magdalena\DSS\Entity\SimaCaseStudy $case */
  $form['case'] = array('#type' => 'hidden', '#value' => $case->getId());
  $form['model_machine_name'] = array('#type' => 'hidden', '#value' => $model_machine_name);

  // @codingStandardsIgnoreStart
  // $form['process_datastore'] = array(
  //    '#type' => 'radios',
  //    '#title' => t('Process Datastore'),
  //    '#description' => t('Do you want to process the Datastore in WEAP Resources (Cajas de Datos) after importing them? This can take a lot of time. Processing datastore allows to parse the resource (output variable file) and store its contents in the database. If it is not selected, it can be processed later.'),
  //    '#default_value' => variable_get('dss_interface_sima_import_queue_process_datastore', 0),
  //    '#options' => array(
  //      t('No'),
  //      t('Yes'),
  //    ),
  //  );
  // @codingStandardsIgnoreEnd
  $weap_connector = dss_engine_weap_client_load();
  if (($model_machine_name === SimaModel::MODEL_WEAP && $weap_connector->testConnection())) {
    /* 
    //this coment disable weap execution button
    $form['submit'] = array(
       '#type' => 'submit',
       '#value' => $label,
     );
     
     */
   }
   else if($model_machine_name !== SimaModel::MODEL_WEAP){
     $form['submit'] = array(
       '#type' => 'submit',
       '#value' => $label,
     );
   }
  else {
    $form['connection'] = array(
      '#markup' => t('SIMA cannot connect to WEAP Server. To execute the Hydrology model, you need to define WEAP Connection Settings. Please review your !connection_settings', array(
        '!connection_settings' => l(t('Connection Settings'), 'admin/config/dss-magdalena/settings'),
      )),
    );
  }
  return $form;
}

/**
 * Implements hook_form_submit().
 */
function dss_engine_execute_case_form_submit($form, &$form_state) {
  $model_machine_name = $form_state['values']['model_machine_name'];
  $case_id = strtoupper($form_state['values']['case']);
  // $process_datastore = boolval($form_state['values']['process_datastore']);.
  $process_datastore = TRUE;
  $batch = dss_engine_model_executor_batch_process($model_machine_name, $case_id, $process_datastore);
  batch_set($batch);
}

function dss_view_case_study($case_title){
  //datos del la entidad
   $case=SimaCaseStudy::loadCaseByTitle($case_title);
   $id = $case->getID();
  // $titulo=$case->getTitle();
  
  $outputList=$case->getCaseOutputs()->getModelingStatusList();
  $outListencode=json_encode($outputList);

   
  Drupal_add_js("var outStatus = JSON.parse('$outListencode '); ",'inline');
  
  Drupal_add_css('profiles/dss_magdalena/modules/dss_magdalena/dss_engine/css/graphstyles.css');
  drupal_add_css('https://use.fontawesome.com/releases/v5.8.1/css/all.css');
  drupal_add_js('https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js');
  
  $out ='<div class="full_case content_info">';
  

  $out  .= '<div class="info_case">
  <div class="rtecenter" id = "titulo_caso">  </div>';
  
  $out  .='<div  id ="desc_caso" class="col-md-12 rtejustify"></div>
  
  
  </div>';


  //Inicio Datos caso de estudio
  $out .= '<div class="row row_partes" >';	
  $out  .='<div  id ="div_esc" class="col-md-12 partes"><h2 class="rtecenter">Escenarios</h2>
  <div class="img_titulo">
  <img src="/profiles/dss_magdalena/modules/dss_magdalena/dss_interface/images/Imagenes_adm/02-escenarios-1.png"></img>
  </div>
  </div>';
  $out  .='<div id ="div_alt" class="col-md-12 partes"><h2 class="rtecenter">Alternativas de plan integral</h2> 
  <div class="img_titulo">
  <img src="/profiles/dss_magdalena/modules/dss_magdalena/dss_interface/images/Imagenes_adm/03-planes-integrales-1.png"></img>
  </div>
  </div>';
  $out  .= '</div>';

    //fin de full_page
    $out  .= '</div>';





  //$out  .='<h2 class="rtecenter">Graficos :</h2>';
  // $out  .='  <div id="Grafico1"><!-- Plotly chart will be drawn inside this DIV --></div>';
  //Graficos
  $out .= '<div class="row">';	

    //Inicio Fragmentacion
    $out  .='<div class="col-sm-6">';
    $out  .='  <div id="Frag_graph2"><!-- Plotly chart will be drawn inside this DIV --></div>';
    $out  .='</div>';
    //Inicio DorH
    $out  .='<div class="col-sm-6">';
    $out  .='  <div id="Dorh_graph"><!-- Plotly chart will be drawn inside this DIV --></div>';
    $out  .='</div>';


    //Inicio Huella
    $out  .='<div class="col-sm-7">';
    $out  .='  <div id="hue_graph"><!-- Plotly chart will be drawn inside this DIV --></div>';
    $out  .='</div>';
    //Inicio de Sedimentos

    $out  .='<div class="col-sm-4">';
    $out  .='  <div id="sed_grap"><!-- Plotly chart will be drawn inside this DIV --></div>';
    $out  .='</div>';

  //fin del row
  $out  .= '</div>';

  $out .= '<div   class="hidden row  content_info">
  
  <div data-toggle=" collapse" href="#collapseExample" class="well btn col-md-12 rtecenter">
  <div   class="row  content_info" data-toggle="tooltip" data-placement="top" title="Click para mas informacion">
  <h4>Estado de las ejecuciones</h4>
  </div>
  </div>
  ';
  
  $out.='
  <div class="collapse" id="collapseExample">';
    
  foreach ($outputList as $key => $value) {
    $out .='<div class="col-md-6 rtecenter"><a href="/case/'.$id."/model"."/".$key .' " >';
    $out .=$value['name'].': ';
    $out .=SimaModel::getStatusLabel($outputList[$key]['status'], TRUE);
    $out .='</a></div>';
  }
  $out .="</div>";  
  $out .="</div>";





    //$mundo=  json_encode($saludo);
   Drupal_add_js("var id_Nodo = '$id'; ",'inline');
 
   #Drupal_add_js('/profiles/dss_magdalena/modules/contrib/jquery_update/replace/jquery/1.10/jquery.min.js?v=1.10.2)');
   Drupal_add_js(drupal_get_path('module', 'dss_engine')."/js/d3.v4.min.js");
   Drupal_add_js(   drupal_get_path('module', 'dss_engine') . '/js/plotly-latest.min.js');
   Drupal_add_js(  drupal_get_path('module', 'dss_engine') . '/js/toponet.js');
   Drupal_add_js(  drupal_get_path('module', 'dss_engine') . '/js/graph_case.js');
   return $out;
   
 }