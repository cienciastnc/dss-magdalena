<?php
/**
 * @file
 * Handles the relationship between Case Study and Model Status.
 */

use Drupal\dss_magdalena\DSS\Entity\SimaCaseStudy;

/**
 * Implements hook_node_view().
 */
// @codingStandardsIgnoreStart
function dss_engine_node_view($node, $view_mode, $langcode) {
  $case_study = SimaCaseStudy::load($node->nid);

  $modeling_status_list = $case_study->getCaseOutputs()->getModelingStatusList('model_name');
  $items = [];
  foreach ($modeling_status_list as $model_machine_name => $model) {
    $link = l($model['name'], 'case/' . $case_study->getId() . '/model/' . $model_machine_name, array(
      'attributes' => array(
        'title' => $model['name'],
        'html' => TRUE,
      )
    ));
    $status = is_numeric($model['status']) ? "{$model['status']}%" : $model['status'];
    $items[] = $link . ': ' . $status;
  }
  $markup = array(
    'title' => t('Modeling Status'),
    'items' => $items,
    'type' => 'ul',
  );

  $node->content['field_case_modeling_status'] = array(
    '#markup' => theme('item_list', $markup),
    '#weight' => 10,
  );
  return $node;
}
// @codingStandardsIgnoreStart

