<?php
/**
 * @file
 * Defines all the functions that deal with managing DSS Queues.
 */

use Drupal\dss_magdalena\DSS\Queue\SimaImportQueue;
use Drupal\dss_magdalena\DSS\Entity\SimaCaseStudy;
use Drupal\dss_magdalena\DSS\Entity\SimaModel;

/**
 * Implements hook_queue_info().
 */
function dss_engine_queue_info() {
  // Define the worker for the Sima Import Queue.
  $queues = [];
  if (class_exists('\Drupal\dss_magdalena\DSS\Queue\SimaImportQueue')) {
    $queues[SimaImportQueue::QUEUE_NAME] = [
      'title' => SimaImportQueue::QUEUE_TITLE,
      'batch' => array(
        'operations' => array(array(SimaImportQueue::QUEUE_RUNNER, array())),
        'finished' => SimaImportQueue::QUEUE_BATCH_FINISHED,
        'title' => t('Processing item from !queue', array(
          '!queue' => SimaImportQueue::QUEUE_TITLE,
        )),
      ),
      'cron' => array(
        'callback' => SimaImportQueue::QUEUE_CRON_RUNNER,
      ),
    ];
  }
  return $queues;
}

/**
 * Imports a list of Case Studies.
 *
 * @param array $cases
 *   An array of cases cid.
 * @param array $context
 *   The batch api context array.
 */
function dss_engine_sima_import_case_studies($cases, &$context) {
  if (empty($context['sandbox'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current'] = 0;
    $context['sandbox']['max'] = count($cases);
  }

  // Process a maximum of 2 case studies during every step.
  for ($i = 0; $i < 2 && $context['sandbox']['current'] < $context['sandbox']['max']; $i++) {
    $index = $context['sandbox']['current'];
    if ($case_study = SimaCaseStudy::load($cases[$index])) {

      // Import Case Study.
      // Executing the import will fill items in the sima_import_queue that can
      // be processed later. Each one of those items will deal with the import
      // or a particular output resource (caja de datos).
      $case_study->import();
    }
    $context['sandbox']['progress']++;
    $context['sandbox']['current']++;
    $context['message'] = t('Importing Case Study "%name" from S-WEAP Server. Queuing WEAP output resources for importing.', array(
      '%name' => $case_study->getDrupalEntity()->label(),
    ));
  }
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Executes an item in the queue.
 *
 * @param \DrupalQueueInterface $queue
 *   The queue.
 * @param bool $process_datastore
 *   TRUE if we want to process the datastore, FALSE otherwise.
 * @param array $context
 *   The batch API context array.
 */
function dss_engine_sima_import_queue_worker(\DrupalQueueInterface $queue, $process_datastore, &$context) {
  if (empty($context['sandbox'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current'] = 0;
    $context['sandbox']['max'] = $queue->numberOfItems();
    // Store some result for post-processing in the finished callback.
    $context['results'] = array(
      'success' => 0,
    );
  }

  // Process a maximum of 1 items during every step.
  for ($i = 0; $i < 1 && $context['sandbox']['current'] < $context['sandbox']['max']; $i++) {
    // Lease time.
    $item = $queue->claimItem(120);
    if ($item) {

      // Process the item and if successful, delete it.
      $sima_import_queue = new SimaImportQueue($process_datastore);
      if ($success = $sima_import_queue->processItem($item->data)) {
        $num = isset($num) ? $num : 0;
        // Store some result for post-processing in the finished callback.
        $context['results'] = array(
          'success' => $num++,
          'cid' => $item->data['cid'],
        );
      }
      else {
        $message = t('We could not process weap output for file !file from Case Study = !cid', array(
          '!file' => $item->data['filename'],
          '!cid' => $item->data['cid'],
        ));
        drupal_set_message($message, 'error');
      }
      $queue->deleteItem($item);
      $context['message'] = t('Importing WEAP Output for variable %name to Case ID = %cid.', array(
        '%name' => $item->data['dss_machine_name'],
        '%cid' => $item->data['cid'],
      ));
    }
    $context['sandbox']['progress']++;
    $context['sandbox']['current']++;
  }
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }

}

/**
 * Presents results after the batch execution of the queue.
 *
 * @param string $success
 *   The success message.
 * @param array $results
 *   The results.
 * @param array $operations
 *   The operations array.
 */
function dss_engine_sima_import_queue_batch_finished($success, $results, $operations) {
  if ($success) {
    $message = t('Imported %num WEAP outputs successfully.', array(
      '%num' => $results['success'],
    ));
    // Update Case Study WEAP Modeling Status to DONE.
    // @TODO: Fix setting up the status ONLY if it has imported ALL the
    // case outputs. This zip file might contain less outputs than the
    // ones the case needs.
    if (!empty($results['cid'])) {
      $cid = $results['cid'];
      $case = SimaCaseStudy::load($cid);
      $case->setModelingStatus(SimaModel::MODEL_WEAP, SimaModel::STATUS_OK);
    }
  }
  else {
    $message = t('We encountered an error while trying to import WEAP outputs into the system.');
  }
  drupal_set_message($message);
}

/**
 * Executes the Sima Import Queue during cron runs.
 *
 * @param \DrupalQueueInterface $queue
 *   The queue.
 */
function dss_engine_sima_import_queue_cron_worker(\DrupalQueueInterface $queue) {
  // Number of items to process on cron.
  $max = 0;

  // Define whether we want to process the queue on cron.
  $count = $queue->numberOfItems();
  for ($i = 0; $i < $max && $count > 0; $i++) {
    // Lease time.
    $item = $queue->claimItem(120);
    if ($item) {

      // Process the item and if successful, delete it.
      $sima_import_queue = new SimaImportQueue();
      if ($sima_import_queue->processItem($item)) {
        $queue->deleteItem($item);
      }
      else {
      }

      $count--;
    }
  }
}
