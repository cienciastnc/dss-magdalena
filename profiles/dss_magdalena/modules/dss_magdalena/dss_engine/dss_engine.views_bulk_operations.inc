<?php
/**
 * @file
 * Integration with Views Bulk Operations.
 */

use Drupal\dss_magdalena\DSS\Entity\SimaCaseStudy;
use Drupal\dss_magdalena\DSS\Entity\SimaModel;
use Drupal\dss_magdalena\DSS\Entity\Term\SimaVariableStatus;

/**
 * Implements hook_module_implements_alter().
 *
 * Ensure our form_alter runs after VBO.
 */
function dss_engine_module_implements_alter(&$implementations, $hook) {
  if ($hook == 'form_alter') {
    if (isset($implementations['dss_engine'])) {
      $group = $implementations['dss_engine'];
      unset($implementations['dss_engine']);
      $implementations['dss_engine'] = $group;
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Alters the text labels for VBO.
 */
function dss_engine_form_views_form_alter(&$form, &$form_state, $form_id) {
  // Adjust VBO text labels and set button weightings
  // Set button order with #weight.
  if ($form_id == 'views_form_case_study_page' && isset($form['select']) && in_array($form['select']['#title'], ['Operations', 'Operaciones'])) {
    // Adjust text.
    $form['select']['#title'] = t('Analysis and Evaluation');
    $form['select']['submit']['#value'] = t('Evaluate');
    if (isset($form['select']['operation']['#options'])) {
      $form['select']['operation']['#options'][0] = t('Choose Analysis Level');
    }
  }
}

/**
 * Implements hook_views_bulk_operations_form_alter().
 *
 * Adds additional images to the VBO Confirmation Form.
 */
function dss_engine_views_bulk_operations_form_alter(&$form, &$form_state, $vbo) {
  if ($form_state['step'] == 'views_bulk_operations_confirm_form') {
    // Alter the confirmation step of the VBO form.
    $operation = $form_state['values']['operation'];
    $cases = array_filter($form_state['values']['views_bulk_operations']);
    $mcm_models = dss_engine_views_bulk_operations_get_mcm_models($cases);
    $models = array();
    switch ($operation) {
      case 'action::dss_engine_comparative_analysis_hydropower_tier_1':
        unset($mcm_models['weap']);
        unset($mcm_models['eloha']);
        session_set('dss_engine_tier2_mcm_models', $mcm_models);
        $models = array(
          array(
            'title' => t('Tier 1 (!models)', [
              '!models' => implode(', ', $mcm_models),
            ]),
            'models' => array_keys($mcm_models),
            'descriptions' => dss_visualizations_model_descriptions(array_merge(['tier1'], array_keys($mcm_models))),

          ),
          array(
            'title' => t('Tier 1 complete (!models)', [
              '!models' => implode(', ', SimaModel::listTier1Models()),
            ]),
            'models' => array_keys(SimaModel::listTier1Models()),
            'descriptions' => dss_visualizations_model_descriptions(array_merge(['tier1'], array_keys(SimaModel::listTier1Models()))),
          ),
        );
        $options = [
          'analytic' => t('Analytic Evaluation'),
        ];
        $default_value = 'analytic';
        break;

      case 'action::dss_engine_comparative_analysis_hydropower_tier_2':
        session_set('dss_engine_tier2_mcm_models', $mcm_models);
        // Gather all models that have OK status.
        $models = array(
          array(
            'title' => t('Tier 2 (!models)', [
              '!models' => implode(', ', $mcm_models),
            ]),
            'models' => array_keys($mcm_models),
            'descriptions' => dss_visualizations_model_descriptions(array_merge(['tier2'], array_keys($mcm_models))),
          ),
          array(
            'title' => t('Tier 2 complete (!models)', [
              '!models' => implode(', ', SimaModel::listTier2Models()),
            ]),
            'models' => array_keys(SimaModel::listTier2Models()),
            'descriptions' => dss_visualizations_model_descriptions(array_merge(['tier1'], array_keys(SimaModel::listTier2Models()))),
          ),
        );
        $options = [
          'strategic' => t('Strategic Evaluation'),
          'analytic' => t('Analytic Evaluation'),
        ];
        $default_value = 'strategic';
        break;
    }
    // Prepare Graph.
    $chart = array(
      'id' => 'sima',
      'type' => 'cause_effect_fixed',
      'models' => $models,
    );
    $output = dss_visualizations_draw($chart);

    // @TODO: This inline CSS should NOT be here!
    // This should be part of the graph itself.
    drupal_add_css('#sel_graph {display:block; margin: 0 auto;}', 'inline');

    $form['graph'] = array(
      '#markup' => $output,
    );

    $form['sima_evaluation'] = array(
      '#type' => 'radios',
      '#options' => $options,
      '#default_value' => $default_value,
      '#title' => t('Select an evaluation type'),
    );

    // Adding additional submit function.
    array_unshift($form['actions']['submit']['#submit'], 'dss_engine_views_bulk_operations_additional_submit');
  }
}

/**
 * Implements hook_action_info().
 */
function dss_engine_action_info() {
  return array(
    'dss_engine_comparative_analysis_hydropower_tier_1' => array(
      'type' => 'entity',
      'label' => t('Hydropower Tier 1'),
      'configurable' => FALSE,
      'behavior' => array('comparative_analysis_hydropower_tier_1'),
      'triggers' => array('any'),
    ),
    'dss_engine_comparative_analysis_hydropower_tier_2' => array(
      'type' => 'entity',
      'label' => t('Hydropower Tier 2'),
      'configurable' => FALSE,
      'behavior' => array('comparative_analysis_hydropower_tier_2'),
      'triggers' => array('any'),
    ),
  );
}

/**
 * Additional Submit handler for bulk operations form.
 *
 * @param array $form
 *   The form to submit.
 * @param array $form_state
 *   The form state.
 */
function dss_engine_views_bulk_operations_additional_submit($form, &$form_state) {
  // Setting up the evaluation type.
  session_set('sima_evaluation', $form_state['values']['sima_evaluation']);
}

/**
 * Implements hook_action_views_bulk_operations_form().
 */
function dss_engine_comparative_analysis_hydropower_tier_1_views_bulk_operations_form($options) {
  $form = array();
  $form['select_all_models'] = array(
    '#type' => 'checkbox',
    '#title' => t('Select all models'),
    '#default_value' => !empty($options['select_all_models']) ? $options['select_all_models'] : 0,
  );

  return $form;
}

/**
 * Implements hook_action_views_bulk_operations_form().
 */
function dss_engine_comparative_analysis_hydropower_tier_2_views_bulk_operations_form($options) {
  $form = array();
  $form['select_all_models'] = array(
    '#type' => 'checkbox',
    '#title' => t('Select all models'),
    '#default_value' => !empty($options['select_all_models']) ? $options['select_all_models'] : 0,
  );

  return $form;
}

/**
 * Implementation of VBO Action comparative_analysis_hydropower_tier_2.
 *
 * @param object $node
 *   The node object given.
 * @param array $context
 *   The Context for VBO operations.
 *
 * @TODO: This is reusing the same function for tier2. It should be separated.
 */
function dss_engine_comparative_analysis_hydropower_tier_1(&$node, &$context) {
  dss_engine_comparative_analysis_hydropower_tier_2($node, $context);
}


/**
 * Implementation of VBO Action comparative_analysis_hydropower_tier_2.
 *
 * @param object $node
 *   The node object given.
 * @param array $context
 *   The Context for VBO operations.
 */
function dss_engine_comparative_analysis_hydropower_tier_2(&$node, &$context) {
  // Initialize the success.
  if ($context['progress']['current'] == 1) {

    // Set session variable to show the.
    session_set('dss_engine_show_sima_evaluation_blocks', TRUE);
    // Initialize the URL.
    $evaluation = session_get('sima_evaluation', 'analytic');
    session_delete('sima_evaluation');
    switch ($evaluation) {
      case 'strategic':
        session_set('dss_engine_variable_url', 'multi-objectives/field_resources%253A');
        break;

      case 'analytic':
      default:
        session_set('dss_engine_variable_url', 'search/field_resources%253A');
        break;
    }
    dss_engine_persistent_variable(1);
  }

  // Start checking case.
  if ($case = SimaCaseStudy::load($node)) {
    // Make sure the case study validates.
    if ($case->validate()) {
      $statuses = $case->getCaseOutputs()->getModelingStatusList();
      // Analyze case readiness.
      // Only proceed if the status is 102.
      if (dss_engine_comparative_analysis_hydropower_tier_2_compare_cases($statuses, $context['progress']['current'])) {
        $url = session_get('dss_engine_variable_url', 'search/field_resources%253A');
        if ($context['progress']['current'] == $context['progress']['total']) {
          $url .= 'field_model_case_study_id/' . $case->getId() . '/';
        }
        else {
          $url .= 'field_model_case_study_id/' . $case->getId() . '/field_resources%253A';
        }
        session_set('dss_engine_variable_url', $url);
      }
      else {
        // This model is not ready. then record this in the context.
        dss_engine_persistent_variable(NULL);
        dss_engine_persistent_variable(0);
      }
    }
  }

  // Check at the end of the last item.
  if ($context['progress']['current'] == $context['progress']['total']) {
    $result = dss_engine_persistent_variable();
    if (empty($result)) {
      drupal_set_message(t('You cannot proceed until you have selected case studies that have same models status are set as "OK".'), 'warning');
    }
    else {
      $url = session_get('dss_engine_variable_url', 'search/type/dataset');
      if (strpos($url, 'search') !== FALSE) {
        $mcm_models = session_get('dss_engine_tier2_mcm_models', []);
        foreach ($mcm_models as $model => $model_name) {
          if ($var_status = SimaVariableStatus::loadOutputTermByModel($model)) {
            $name = str_replace(' ', '-', strtolower($var_status->getName())) . '-' . $var_status->getId();
            $url .= 'field_variable_status/' . $name . '/';
          }
        }
        $url .= 'type/dataset';
      }
    }
    session_set('dss_engine_variable_url', $url);
    // Deleting session variables.
    session_delete('dss_engine_case_completed_tier_2');
    session_delete('dss_engine_tier2_mcm_models');
  }

}

/**
 * Compares model status for different cases.
 *
 * @param array $statuses
 *   An array of model and their status.
 * @param int $initialize
 *   Value of 1 if it is the first time.
 *
 * @return int
 *   0 if nothing in common, an integer otherwise.
 */
function dss_engine_comparative_analysis_hydropower_tier_2_compare_cases($statuses, $initialize = 0) {
  if ($initialize == 1) {
    $models_completed = [];
    foreach ($statuses as $model => $values) {
      if ($values['status'] == 102) {
        $models_completed[$model] = $model;
      }
    }
    session_set('dss_engine_case_completed_tier_2', $models_completed);
  }

  // If not first time, then read models completed and compare.
  $models_completed = session_get('dss_engine_case_completed_tier_2', []);
  foreach ($statuses as $model => $values) {
    if ($values['status'] == 102 && isset($models_completed[$model])) {
      $models_completed[$model] = $model;
    }
    else {
      if ($models_completed[$model]) {
        unset($models_completed[$model]);
      }
    }
  }
  session_set('dss_engine_case_completed_tier_2', $models_completed);
  return count($models_completed);
}

/**
 * Calculates minimum common number of OK models for each case study.
 *
 * @param array $cids
 *   An array of Case IDs.
 *
 * @return array
 *   An array of models that have OK status.
 */
function dss_engine_views_bulk_operations_get_mcm_models($cids) {
  $model_status = [];
  foreach ($cids as $cid) {
    if ($case = SimaCaseStudy::load($cid)) {
      $model_status[$cid] = $case->getCaseOutputs()->getModelingStatusList();
    }
  }
  // Now that we have the model statuses, get the mcm.
  $mcm_models = array_combine(SimaModel::listModels(), SimaModel::listModels());
  foreach ($model_status as $models) {
    foreach ($models as $name => $data) {
      if ($data['status'] != SimaModel::STATUS_OK) {
        unset($mcm_models[$name]);
      }
      else {
        $mcm_models[$name] = $data['name'];
      }
    }
  }
  return $mcm_models;
}

/**
 * Obtains graph descriptions based on models and tier.
 *
 * @param array $models
 *   An array of models.
 * @param string $tier
 *   The tier.
 *
 * @return array
 *   An array of descriptions keyed by model and tier.
 */
function dss_engine_views_bulk_operations_get_descriptions($models, $tier) {
  $descriptions = [];
  foreach ($models as $model) {
    $description = SimaModel::getModelDescription($model);
    $descriptions[$model] = $description;
  }
  $descriptions[$tier] = SimaModel::getModelDescription($tier);
  return $descriptions;
}
