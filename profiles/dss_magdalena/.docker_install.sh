#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

# Install git (the php image doesn't have it) which is required by composer
apt-get update -yqq
apt-get install git unzip -yqq

# Install composer.
curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Setting up drush make file.
export GITLAB_PROJ="git@gitlab.com:dsstnccolombia/dss-magdalena.git"
export PROJECT_GIT="$PWD/.git"
sed -i -- "s#$GITLAB_PROJ#$PROJECT_GIT#g" build-dss_magdalena.make
sed -i -- "s/branch/revision/g" build-dss_magdalena.make
sed -i -- "s/master/$CI_BUILD_REF/g" build-dss_magdalena.make
