#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

# PGSQL
#apt-get install -y libpq-dev postgresql-client libssh2-php

# Installing PHP Extensions.
#apt-get install -y phpunit php5-pgsql php5-gd php5-curl php5-mcrypt php5-xmlrpc libpng12-dev

# Install phpunit, the tool that we will use for testing.
#curl --location --output /usr/local/bin/phpunit https://phar.phpunit.de/phpunit.phar
#chmod +x /usr/local/bin/phpunit

# Enabling extensions required by Drupal.
# docker-php-ext-install gd pdo_pgsql bcmath
