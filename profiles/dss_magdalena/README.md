# DSS Magdalena
[![build status](https://gitlab.com/dsstnccolombia/dss-magdalena/badges/master/build.svg)](https://gitlab.com/dsstnccolombia/dss-magdalena/builds)

This distribution contains modules that enables the DSS for Magdalena Basin Project,

The modules are packaged into a distribution in order to simplify their setup,
testing, and development.

## Installation

The DSS Magdalena distribution uses a standard [Drush make](https://github.com/drush-ops/drush/blob/master/docs/make.md)
workflow to build and initialize the application.

It is strongly advised to install [Apache Ant](http://ant.apache.org/), the
examples below assume you have done so.

## Important Considerations for your PHP Configuration

Make sure you have the following settings in your php.ini:

```
max_execution_time = 300       ; Maximum execution time of each script, in seconds
max_input_time = 600	        ; Maximum amount of time each script may spend parsing request data
memory_limit = 1024M           ; Maximum amount of memory a script may consume (8MB)
max_input_vars = 6000          ; Maximum number of input variables
max_input_nesting_level = 3048 ; Maximum number of nesting levels

; Maximum size of POST data that PHP will accept.
post_max_size = 64M        ; At least 64MB

; Maximum allowed size for uploaded files.
upload_max_filesize = 64M
```

### Initial Build and Install
* If you are a developer to this project, you do not need to fork this repo. 
  Just clone from this project's GIT URL and create a branch to start your 
  development. If you are just testing this, but are not actively contributing
  to the project, proceed with next step (Forking).
* [Fork](https://gitlab.com/help/workflow/forking_workflow.md) this repo
* Clone the forked repo: `git clone git@gitlab.com:<username>/dss-magdalena.git`
* Create a `develop` branch: `git checkout -b develop`
  * The purpose of this branch is for committing changes locally so that drush
    make can build directly from the working copy
* Add the upstream repo as a remote: `git remote add upstream git@gitlab.com:dsstnccolombia/dss-magdalena.git`
* Copy the `build-dss_magdalena.make` file to `build-dss_magdalena-dev.make`,
  modify according to your local environment:

```
api = 2
core = 7.x
includes[] = drupal-org-core.make

projects[dss_magdalena][type] = "profile"
projects[dss_magdalena][download][type] = "git"
projects[dss_magdalena][download][url] = "file:///path/to/working/copy/.git"
projects[dss_magdalena][download][branch] = "develop"
```

* Copy the `build.properties.dist` file to `build.properties`, modify according
  to your local environment.
  * Note the addition of the `drush.makefile` property.

```
base.url=http://localhost
db.url=pgsql://username:password@host/db
drush.makefile=build-dss_magdalena.make

#sites.subdir=default
#site.mail=test@example.com
#account.name=admin
#account.pass=admin
#account.mail=test@example.com
```

* Build the distro: `ant drush-make`
* Set up your database and webserver
* Install the distro: `ant -Ddrush.nomake=1 install`

### Rebuilding and Re-installing

If you want to re-build and re-install the distro will maintaining your
settings.php file, run the following command in the project root:

`ant`

### Re-installing Without Rebuilding

To reinstall the distro without running Drush make, run the following command
in the project root:

`ant -Ddrush.nomake=1 install`

### Developing Profile Modules

In order to make it easier to develop the modules shipped with the profile,
developers can make changes directly to the modules in `docroot/profile/modules`
so they can be tested in real time and use `ant sync-modules` to bring the
modifications back into the project root.

`ant sync-modules`

The opposite of the use case above is outlined below:

`ant push-modules`
