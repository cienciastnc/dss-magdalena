api = 2
core = 7.x
includes[] = drupal-org-core.make

projects[dss_magdalena][type] = "profile"
projects[dss_magdalena][download][type] = "git"
projects[dss_magdalena][download][url] = "git@gitlab.com:dsstnccolombia/dss-magdalena.git"
projects[dss_magdalena][download][branch] = "master"

