<?php

/**
 * @file
 * Enables modules and site configuration for a dss_magdalena site installation.
 */

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function dss_magdalena_form_install_configure_form_alter(&$form, $form_state) {
  // Pre-populate the site name with the server name.
  $form['site_information']['site_name']['#default_value'] = $_SERVER['SERVER_NAME'];
}

/**
 * Implements hook_install_tasks().
 */
function dss_magdalena_install_tasks() {
  return array(
    'dss_magdalena_additional_setup' => array(
      'display_name' => t('DSS Magdalena final setup tasks'),
      'display' => TRUE,
      'type' => 'batch',
      'run' => INSTALL_TASK_RUN_IF_NOT_COMPLETED,
    ),
  );
}

/**
 * Implements hook_install_tasks_alter().
 */
function dss_magdalena_install_tasks_alter(&$tasks, $install_state) {
  // Set the installation theme.
  _dss_magdalena_set_theme('nuboot_radix');
}

/**
 * Force-set a theme at any point during the execution of the request.
 *
 * Drupal doesn't give us the option to set the theme during the installation
 * process and forces enable the maintenance theme too early in the request
 * for us to modify it in a clean way.
 */
function _dss_magdalena_set_theme($target_theme) {
  if ($GLOBALS['theme'] != $target_theme) {
    unset($GLOBALS['theme']);

    drupal_static_reset();
    $GLOBALS['conf']['maintenance_theme'] = $target_theme;
    _drupal_maintenance_theme();
  }
}

/**
 * DSS Magdalena setup task. Runs a series of helper functions defined below.
 */
function dss_magdalena_additional_setup() {
  return array(
    'operations' => array(
      array('dss_magdalena_theme_config', array()),
      array('dss_magdalena_change_block_titles', array()),
      // @codingStandardsIgnoreStart
      array('dss_magdalena_revert_feature', array('dkan_dataset_content_types', array('field_instance'))),
      array('dss_magdalena_revert_feature', array('dkan_dataset_groups', array('field_base'))),
      // array('dss_magdalena_revert_feature', array('dkan_dataset_groups_perms', array('og_features_permission'))),
      // array('dss_magdalena_revert_feature', array('dkan_permissions', array('roles_permissions'))),
      array('dss_magdalena_revert_feature', array('dkan_sitewide', array('variable'))),
      array('dss_magdalena_revert_feature', array('dss_datamodel', array('field_instance'))),
      array('dss_magdalena_revert_feature', array('dss_variable_dataset', array('field_instance'))),
      array('dss_magdalena_revert_feature', array('open_data_schema_map_dkan', array('open_data_schema_apis'))),
      array('dss_magdalena_revert_feature', array('dkan_sitewide_search_db', array('search_api_index', 'search_api_server'))),
      array('dss_magdalena_revert_feature', array('dss_sitewide_search', array('features_overrides'))),
      // @codingStandardsIgnoreEnd
      array('dss_magdalena_flush_image_styles', array()),
      array('dss_magdalena_colorizer_reset', array()),
      array('dss_magdalena_misc_variables_set', array()),
      array('dss_magdalena_configure_tb_megamenu', array()),
      array('dss_magdalena_install_extra_tasks', array()),
      array('dss_magdalena_relaunch_search_api_server_index', array()),
    ),
  );
}

/**
 * Theme configurator for DSS Magdalena Distro.
 */
function dss_magdalena_theme_config(&$context) {
  $context['message'] = t('Setting theme options.');

  // Set up the themes.
  theme_enable(array(
    'theme_default' => 'nuboot_radix',
    'admin_theme' => 'nuboot_radix',
  ));
  variable_set('node_admin_theme', '1');

  // Update logo of DKAN Theme.
  $theme_name = 'nuboot_radix';
  $var_name = 'theme_' . $theme_name . '_settings';

  $logo_path = drupal_get_path('module', 'dss_interface') . '/images/logo_sima.jpg';
  $favicon_path = drupal_get_path('module', 'dss_interface') . '/images/favicon_sima.ico';
  $theme_settings = array(
    'default_logo' => 0,
    'toggle_logo' => 1,
    'default_favicon' => 0,
    'toggle_favicon' => 1,
    'logo_path' => $logo_path,
    'favicon_path' => $favicon_path,
    'copyright' => array(
      'value' => 'SIMA version 0.1-dev',
      'format' => 'plain_text',
    ),
  );
  variable_set($var_name, $theme_settings);

  // Update theme variables.
  variable_set('theme_default', 'nuboot_radix');
  variable_set('admin_theme', 'nuboot_radix');

  // Disable the default Bartik theme.
  theme_disable(array('bartik'));
  theme_disable(array('seven'));
}

/**
 * Change block titles for selected blocks.
 */
function dss_magdalena_change_block_titles(&$context) {
  $context['message'] = t('Changing block titles for selected blocks');
  db_query("UPDATE {block} SET title ='<none>' WHERE delta = 'main-menu' OR delta = 'login'");
  variable_set('node_access_needs_rebuild', FALSE);
  variable_set('gravatar_size', 190);
}

/**
 * Set the language of aliases as neutral to be generated for all languages.
 */
function dss_magdalena_pathauto_alias_alter(&$alias, array &$context) {
  $context['language'] = LANGUAGE_NONE;
}

/**
 * Reverts feature components that have been overridden in the setup process.
 *
 * @param string $feature
 *   The feature module name.
 * @param array $components
 *   Array of components to revert.
 * @param array $context
 *   The context array.
 */
function dss_magdalena_revert_feature($feature, $components, &$context) {
  $context['message'] = t('Reverting feature %feature_name', array('%feature_name' => $feature));
  features_revert(array($feature => $components));
  cache_clear_all();
}


/**
 * Flush the image styles.
 *
 * @param array $context
 *   The context array.
 */
function dss_magdalena_flush_image_styles(&$context) {
  $context['message'] = t('Flushing image styles');
  cache_clear_all();
  $image_styles = image_styles();
  foreach ($image_styles as $image_style) {
    image_style_flush($image_style);
  }
}

/**
 * Resets colorizer cache.
 *
 * Resets that cache so that background colors and other colorizer settings are
 * not blank at first page view.
 *
 * @param array $context
 *   The context array.
 */
function dss_magdalena_colorizer_reset(&$context) {
  $context['message'] = t('Resetting colorizer cache');
  global $theme_key;
  $instance = $theme_key;

  // Assigning a pre-determined palette.
  $data = [
    'palette' => [
      'top' => "#006941",
      'bottom' => "#42a53b",
      'toolbarbtn' => "#006941",
      'toolbarborder' => "#CDCDCD",
      'bg' => "#FFFFFF",
      'sidebar' => "#3B3B3B",
      'sidebarborders' => "#0a9900",
      'footer' => "#ffffff",
      'titleslogan' => "#3B3B3B",
      'herotitle' => "#FFFFFF",
      'title' => "#3B3B3B",
      'text' => "#3B3B3B",
      'link' => "#0a9900",
      'linkactive' => "#006941",
      'linkhover' => "#024a2f",
    ],
    'stylesheet' => 'public://colorizer/nuboot_radix-006941.css',
  ];
  colorizer_save($instance, $data);
  clearstatcache();
}

/**
 * Set a number of miscellaneous variables.
 *
 * @param array $context
 *   The context array.
 */
function dss_magdalena_misc_variables_set(&$context) {
  $context['message'] = t('Setting misc variables for "DSS Magdalena"');
  variable_set('site_frontpage', 'frontpage');

  // clone_node: nodes omitted (not clone).
  $clone_omitted = array(
    'agricultural_development' => 'agricultural_development',
    'alternatives_resources' => 'alternatives_resources',
    'article' => 'article',
    'page' => 'page',
    'case_outputs' => 'case_outputs',
    'hydropower' => 0,
    'dataset' => 'dataset',
    'demand_sites_and_catchments' => 'demand_sites_and_catchments',
    'document' => 'document',
    'forest_development' => 'forest_development',
    'group' => 'group',
    'mining_development' => 'mining_development',
    'model' => 'model',
    'organization' => 'organization',
    'persona' => 'persona',
    'photography' => 'photography',
    'project' => 'project',
    'relation' => 'relation',
    'resource' => 'resource',
    'river_reaches' => 'river_reaches',
    'ror_hydropower_plant' => 0,
    'variable_vocabulary' => 'variable_vocabulary',
    'wetlands_management' => 'wetlands_management',
    'agricultural_alternatives' => 0,
    'campaign' => 0,
    'case_study' => 0,
    'climate_scenario' => 0,
    'energy_scenario' => 0,
    'forest_exploitation_alternatives' => 0,
    'hydropower_alternatives' => 0,
    'integral_plan' => 0,
    'mining_exploitation_alternatives' => 0,
    'population_scenario' => 0,
    'river_floodplain_connectivity' => 0,
    'scenario' => 0,
    'water_balance' => 0,
  );
  variable_set('clone_omitted', $clone_omitted);

  // Enabling some panel pages by default.
  variable_set('page_manager_node_view_disabled', FALSE);
  variable_set('page_manager_node_edit_disabled', FALSE);
  variable_set('page_manager_user_view_disabled', FALSE);

  // Setting variables to control ajax_throbber.
  variable_set('ajax_throbber_exclude_include', 'include');
  variable_set('ajax_throbber_exclude_paths', 'admin/*');
  variable_set('ajax_throbber_include_paths', "node/*/time-aggregation\r\nnode/*/space-aggregation");
  variable_set('ajax_throbber_selection', 'circle');
  variable_set('ajax_throbber_bg_color', '255,255,255, 0.75');
  variable_set('ajax_throbber_item_color', '0,0,0, 0.8');

  // Setting up variables for views_megarow.
  variable_set('views_megarow_title', "Project Variants");
  variable_set('views_megarow_override_node_edit', "1");
  variable_set('views_megarow_override_user_edit', "1");

  // Setting variables to control Honeypot Anti Spam Module.
  variable_set('honeypot_time_limit', "5");
  variable_set('honeypot_element_name', "url");
  variable_set('honeypot_form_user_register_form', '1');
  variable_set('honeypot_form_user_pass', '1');

  // Enable Shield Module.
  variable_set('shield_enabled', "1");
  variable_set('shield_allow_cli', "1");
  variable_set('shield_ignored_addresses', "");
  variable_set('shield_remote_address', "REMOTE_ADDR");
  variable_set('shield_method', "1");
  variable_set('shield_paths', "");
  variable_set('shield_user', "simatnc");
  variable_set('shield_pass', "magdalena19##");
  variable_set('shield_print', " ");

  // variable_set('jquery_update_jquery_version', '1.7');
  // Disable selected views enabled by contributed modules.
  $views_disable = array(
    'og_extras_nodes' => TRUE,
    'feeds_log' => TRUE,
    'groups_page' => TRUE,
    'og_extras_groups' => TRUE,
    'og_extras_members' => TRUE,
    'dataset' => TRUE,
  );
  variable_set('views_defaults', $views_disable);
}

/**
 * Configures TB Megamenu.
 */
function dss_magdalena_configure_tb_megamenu() {
  $menu_name = 'main-menu';
  $block_config = '{"animation":"none","duration":400,"delay":200,"style":"","auto-arrow":"1","always-show-submenu":"1"}';
  $menu_config = '{"1457":{"rows_content":[],"submenu_config":{"width":"","class":"","group":"0"},"item_config":{"class":"","xicon":"","caption":"","alignsub":"","group":"0","hidewcol":"0","hidesub":"0"}},"1458":{"rows_content":[[{"col_content":[{"mlid":"1463","type":"menu_item","tb_item_config":{}}],"col_config":{"width":"12","class":"","hidewcol":"0","showblocktitle":"1"}}]],"submenu_config":{"width":"325","class":"","group":"0"},"item_config":{"class":"","xicon":"","caption":"","alignsub":"","group":"0","hidewcol":"0","hidesub":"0"}},"1459":{"rows_content":[[{"col_content":[{"mlid":"1464","type":"menu_item","tb_item_config":{}}],"col_config":{"width":"12","class":"","hidewcol":"0","showblocktitle":"1"}}]],"submenu_config":{"width":"","class":"","group":"0"},"item_config":{"class":"","xicon":"","caption":"","alignsub":"","group":"0","hidewcol":"0","hidesub":"0"}},"1460":{"rows_content":[[{"col_content":[{"mlid":"1461","type":"menu_item","tb_item_config":{}}],"col_config":{"width":"6","class":"","hidewcol":"0","showblocktitle":"1"}},{"col_content":[{"mlid":"1471","type":"menu_item","tb_item_config":{}}],"col_config":{"width":"6","class":"","hidewcol":"","showblocktitle":"1"}}]],"submenu_config":{"width":"630","class":"","group":"0"},"item_config":{"class":"","xicon":"","caption":"","alignsub":"","group":"0","hidewcol":"0","hidesub":"0"}},"1461":{"rows_content":[],"submenu_config":{"width":"","class":"","group":"0"},"item_config":{"class":"","xicon":"","caption":"","alignsub":"","group":"0","hidewcol":"0","hidesub":"0"}},"1462":{"rows_content":[],"submenu_config":{"width":"","class":"","group":"0"},"item_config":{"class":"","xicon":"","caption":"","alignsub":"","group":"0","hidewcol":"0","hidesub":"0"}},"1463":{"rows_content":[],"submenu_config":{"width":"","class":"","group":"0"},"item_config":{"class":"","xicon":"","caption":"","alignsub":"","group":"0","hidewcol":"0","hidesub":"0"}},"1464":{"rows_content":[],"submenu_config":{"width":"","class":"","group":"0"},"item_config":{"class":"","xicon":"","caption":"","alignsub":"","group":"0","hidewcol":"0","hidesub":"0"}},"1465":{"rows_content":[[{"col_content":[{"mlid":"1477","type":"menu_item","tb_item_config":{}}],"col_config":{"width":"12","class":"","hidewcol":"0","showblocktitle":"1"}}]],"submenu_config":{"width":"","class":"","group":"1"},"item_config":{"class":"","xicon":"","caption":"","alignsub":"","group":"1","hidewcol":"0","hidesub":"0"}},"1466":{"rows_content":[],"submenu_config":{"width":"","class":"","group":"0"},"item_config":{"class":"","xicon":"","caption":"","alignsub":"","group":"0","hidewcol":"0","hidesub":"0"}},"1467":{"rows_content":[[{"col_content":[{"mlid":"1472","type":"menu_item","tb_item_config":{}}],"col_config":{"width":"12","class":"","hidewcol":"0","showblocktitle":"1"}}]],"submenu_config":{"width":"","class":"","group":"1"},"item_config":{"class":"","xicon":"","caption":"","alignsub":"","group":"1","hidewcol":"0","hidesub":"0"}},"1468":{"rows_content":[],"submenu_config":{"width":"","class":"","group":"0"},"item_config":{"class":"","xicon":"","caption":"","alignsub":"","group":"0","hidewcol":"0","hidesub":"0"}},"1469":{"rows_content":[[{"col_content":[{"mlid":"1479","type":"menu_item","tb_item_config":{}}],"col_config":{"width":"12","class":"","hidewcol":"0","showblocktitle":"1"}}]],"submenu_config":{"width":"","class":"","group":"1"},"item_config":{"class":"","xicon":"","caption":"","alignsub":"","group":"1","hidewcol":"0","hidesub":"0"}},"1470":{"rows_content":[],"submenu_config":{"width":"","class":"","group":"0"},"item_config":{"class":"","xicon":"","caption":"","alignsub":"","group":"0","hidewcol":"0","hidesub":"0"}},"1471":{"rows_content":[[{"col_content":[{"mlid":"1484","type":"menu_item","tb_item_config":{}}],"col_config":{"width":"12","class":"","hidewcol":"0","showblocktitle":"1"}}]],"submenu_config":{"width":"","class":"","group":"1"},"item_config":{"class":"","xicon":"","caption":"","alignsub":"","group":"1","hidewcol":"0","hidesub":"0"}},"1472":{"rows_content":[[{"col_content":[{"mlid":"1488","type":"menu_item","tb_item_config":{}}],"col_config":{"width":"12","class":"","hidewcol":"0","showblocktitle":"1"}}]],"submenu_config":{"width":"","class":"","group":"1"},"item_config":{"class":"","xicon":"","caption":"","alignsub":"","group":"1","hidewcol":"0","hidesub":"0"}},"1473":{"rows_content":[],"submenu_config":{"width":"","class":"","group":"0"},"item_config":{"class":"","xicon":"","caption":"","alignsub":"","group":"0","hidewcol":"0","hidesub":"0"}},"1474":{"rows_content":[],"submenu_config":{"width":"","class":"","group":"0"},"item_config":{"class":"","xicon":"","caption":"","alignsub":"","group":"0","hidewcol":"0","hidesub":"0"}},"1475":{"rows_content":[],"submenu_config":{"width":"","class":"","group":"0"},"item_config":{"class":"","xicon":"","caption":"","alignsub":"","group":"0","hidewcol":"0","hidesub":"0"}},"1476":{"rows_content":[],"submenu_config":{"width":"","class":"","group":"0"},"item_config":{"class":"","xicon":"","caption":"","alignsub":"","group":"0","hidewcol":"0","hidesub":"0"}},"1477":{"rows_content":[],"submenu_config":{"width":"","class":"","group":"0"},"item_config":{"class":"","xicon":"","caption":"","alignsub":"","group":"0","hidewcol":"0","hidesub":"0"}},"1478":{"rows_content":[[{"col_content":[{"mlid":"1485","type":"menu_item","tb_item_config":{}}],"col_config":{"width":"12","class":"","hidewcol":"0","showblocktitle":"1"}}]],"submenu_config":{"width":"","class":"","group":"1"},"item_config":{"class":"","xicon":"","caption":"","alignsub":"","group":"1","hidewcol":"0","hidesub":"0"}},"1479":{"rows_content":[],"submenu_config":{"width":"","class":"","group":"0"},"item_config":{"class":"","xicon":"","caption":"","alignsub":"","group":"0","hidewcol":"0","hidesub":"0"}},"1480":{"rows_content":[],"submenu_config":{"width":"","class":"","group":"0"},"item_config":{"class":"","xicon":"","caption":"","alignsub":"","group":"0","hidewcol":"0","hidesub":"0"}},"1481":{"rows_content":[],"submenu_config":{"width":"","class":"","group":"0"},"item_config":{"class":"","xicon":"","caption":"","alignsub":"","group":"0","hidewcol":"0","hidesub":"0"}},"1482":{"rows_content":[[{"col_content":[{"mlid":"1487","type":"menu_item","tb_item_config":{}}],"col_config":{"width":"12","class":"","hidewcol":"0","showblocktitle":"1"}}]],"submenu_config":{"width":"","class":"","group":"1"},"item_config":{"class":"","xicon":"","caption":"","alignsub":"","group":"1","hidewcol":"0","hidesub":"0"}},"1483":{"rows_content":[],"submenu_config":{"width":"","class":"","group":"0"},"item_config":{"class":"","xicon":"","caption":"","alignsub":"","group":"0","hidewcol":"0","hidesub":"0"}},"1484":{"rows_content":[],"submenu_config":{"width":"","class":"","group":"0"},"item_config":{"class":"","xicon":"","caption":"","alignsub":"","group":"0","hidewcol":"0","hidesub":"0"}},"1485":{"rows_content":[],"submenu_config":{"width":"","class":"","group":"0"},"item_config":{"class":"","xicon":"","caption":"","alignsub":"","group":"0","hidewcol":"0","hidesub":"0"}},"1486":{"rows_content":[],"submenu_config":{"width":"","class":"","group":"0"},"item_config":{"class":"","xicon":"","caption":"","alignsub":"","group":"0","hidewcol":"0","hidesub":"0"}},"1487":{"rows_content":[],"submenu_config":{"width":"","class":"","group":"0"},"item_config":{"class":"","xicon":"","caption":"","alignsub":"","group":"0","hidewcol":"0","hidesub":"0"}},"1488":{"rows_content":[],"submenu_config":{"width":"","class":"","group":"0"},"item_config":{"class":"","xicon":"","caption":"","alignsub":"","group":"0","hidewcol":"0","hidesub":"0"}},"1489":{"rows_content":[],"submenu_config":{"width":"","class":"","group":"0"},"item_config":{"class":"","xicon":"","caption":"","alignsub":"","group":"0","hidewcol":"0","hidesub":"0"}}}';
  $language = 'es';

  db_insert('tb_megamenus')->fields(array(
    'menu_name' => $menu_name,
    'block_config' => $block_config,
    'menu_config' => $menu_config,
    'language' => $language,
  ))->execute();
}

/**
 * Performs extra tasks in the distribution.
 *
 * @param array $context
 *   The context array.
 */
function dss_magdalena_install_extra_tasks(&$context) {
  $context['message'] = t('Performing last initial extra tasks.');

  // Create default list of available formats for Resources/Cajas de Datos.
  dkan_dataset_teaser_get_external_previews();

  // Disable and Re-enable search index to load overriden changes.
}

/**
 * Reload SearchAPI Server and Index for Datasets.
 */
function dss_magdalena_relaunch_search_api_server_index() {
  // Disabling server and index 'datasets'.
  search_api_index_disable('datasets');
  search_api_server_disable('datasets');

  // Re-enabling server.
  $index = search_api_index_load('datasets', TRUE);
  $server = search_api_server_load('datasets', TRUE);
  $server->addIndex($index);
  $server->update(array('enabled' => 1));

  // Re-enabling Index.
  $index->update(array(
    'enabled' => 1,
    'server' => 'datasets',
  ));

  // Clear server.
  search_api_server_clear('datasets');

  // Finally revert feature.
  features_revert(array('dss_sitewide_search' => array('features_overrides')));
  cache_clear_all();
}
