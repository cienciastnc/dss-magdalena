<?php

/**
 * @file
 * Creates a file of all Catchments with land use.
 */

use League\Csv\Writer;
use Drupal\dss_magdalena\DSS\Entity\SimaAlternative;

$nid = drush_get_option("nid");
if (empty($nid)) {
  return drush_set_error("You have to provide the NID of a Productive Land Use Alternative.");
}

/**
 * Obtain productive land use alternative.
 *
 * @var \Drupal\dss_magdalena\DSS\Entity\SimaAlternative $alternative
 */
$alternative = SimaAlternative::load($nid);


if ($alternative === FALSE) {
  return drush_set_error('No Alternative to check.');
}

$data = [];

$projects = $alternative->getFichaProjects();

foreach ($projects as $project) {
  $element['Alternative'] = $alternative->getTitle();
  $element['Catchment'] = $project->getCatchment()->getTitle();
  $element['Title'] = $project->getTitle();
  $element['Description'] = $project->get('body')['value'];
  $element['Type'] = $project->getLandUseProjectType();
  $percentages = $project->getLandUsePercentages();
  foreach ($percentages as $percentage) {
    $name = $percentage->getLandUseType()->getName();
    $element[$name] = $percentage->getPercentage();
    // $irr_perc = $percentage->getIrrigatedPercentageArea();
  }
  $data[] = $element;
}

print_r($data);

$header = array_keys($data[0]);

$csv_data[] = $header;
foreach ($data as $line) {
  $csv_data[] = array_values($line);
}


$csv = Writer::createFromFileObject(new SplTempFileObject());
$csv->insertAll($csv_data);
$destination = 'public://dss_import_productive_landuse_variations.csv';
return file_unmanaged_save_data($csv, $destination);
