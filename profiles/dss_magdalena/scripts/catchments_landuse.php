<?php

/**
 * @file
 * Creates a file of all Catchments with land use.
 */

use Drupal\dss_magdalena\DSS\Entity\SimaController;
use Drupal\dss_magdalena\DSS\Entity\Term\SimaLandUseType;
use League\Csv\Writer;

$bundle = 'demand_sites_and_catchments';

$controller = new SimaController();
/**
 * Catchments.
 *
 * @var \Drupal\dss_magdalena\DSS\Entity\SimaCatchments[] $catchments
 */
$catchments = $controller->listEntities($bundle);

$data[0] = [
  'Catchment',
  'Title',
  'Description',
  'Type',
  'Agricola',
  'AgricolaPermanente',
  'AgricolaTransitorio',
  'Arbusto',
  'Bosque',
  'BosquePlantado',
  'CuerpoAgua',
  'Herbazal',
  'Hidrofita',
  'Otros',
  'Pastos',
  'Urbano',
  'VegetacionSecundaria',
  'Minero',
];
foreach ($catchments as $catchment) {
  $percentages = $catchment->getConciseLandUsePercentages();
  $land_use = [];
  foreach ($percentages as $tid => $percentage) {
    if ($landuse = SimaLandUseType::load($tid)) {
      $land_use[$landuse->getName()] = $percentage;
    }
  }
  $data[] = [
    $catchment->getTitle(),
    NULL,
    NULL,
    NULL,
    $land_use['Agricola'],
    $land_use['AgricolaPermanente'],
    $land_use['AgricolaTransitorio'],
    $land_use['Arbusto'],
    $land_use['Bosque'],
    $land_use['BosquePlantado'],
    $land_use['CuerpoAgua'],
    $land_use['Herbazal'],
    $land_use['Hidrofita'],
    $land_use['Otros'],
    $land_use['Pastos'],
    $land_use['Urbano'],
    $land_use['VegetacionSecundaria'],
    $land_use['Minero'],
  ];
}

$csv = Writer::createFromFileObject(new SplTempFileObject());
$csv->insertAll($data);
$destination = 'public://landuse_total.csv';
return file_unmanaged_save_data($csv, $destination);
