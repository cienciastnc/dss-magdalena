<?php
/**
 * @file
 * Updates Restoration projects with data from file.
 */

use Drupal\dss_magdalena\DSS\Entity\SimaAlternative;
use Drupal\dss_magdalena\DSS\Entity\SimaProductiveLandUseProjectVariant;
use Drupal\dss_magdalena\DSS\Entity\SimaCatchments;
use League\Csv\Reader;

// Create variations of Productive Land Use Projects.
$csv_file = drupal_get_path('module', 'dss_import') . '/import/dss_import_productive_landuse_variations.csv';
$csv_file = Reader::createFromPath($csv_file);
$headers = $csv_file->fetchOne();
$filter = 'filter_by_restoration';
$results = $csv_file
  ->addFilter($filter)
  ->fetchAssoc($headers);

$data = [];
foreach ($results as $key => $result) {
  if ($key == 0) {
    continue;
  }
  $alternative = $result['Alternative'];
  $data[$alternative][] = (array) $result;
}

foreach ($data as $alternative_key => $variations) {
  if ($alternative = SimaAlternative::loadByTitle($alternative_key, SimaAlternative::PRODUCTIVE_LANDUSE_ALTERNATIVE)) {
    foreach ($variations as $pl_variation) {
      if ($catchment = SimaCatchments::loadByName($pl_variation['Catchment'])) {
        $variation = SimaProductiveLandUseProjectVariant::newProductiveLandUseProjectVariant();
        $variation->setTitle($catchment->getTitle() . ' - ' . $pl_variation['Title']);
        $variation->setBody($pl_variation['Description']);
        $variation->setLandUseProjectType($pl_variation['Type']);
        $variation->setCatchment($catchment);
        $variation->setCatchmentArea($catchment->getCatchmentArea());
        $landuses = $catchment->getSimpleLandUsePercentages();
        foreach ($landuses as $key => $landuse) {
          $land_type = $landuse->getLandUseType()->getName();
          if (isset($pl_variation[$land_type])) {
            $percentage = $pl_variation[$land_type];
            $landuses[$key]->setPercentage($percentage);
          }
        }
        $variation->setLandUsePercentage($landuses);
        $variation->save();
        drush_log(dt('Created Project Variation (nid = !nid): !title.', array(
          '!title' => $variation->getTitle(),
          '!nid' => $variation->getId(),
        )), 'ok');
        $alternative->add(SimaAlternative::PRODUCTIVE_LANDUSE_PROJECTS, $variation->getDrupalEntity()
          ->value());
      }
    }
    $alternative->save();
    drush_log(dt('Saved Productive Land Use Alternative (nid = !nid): !title.', array(
      '!title' => $alternative->getTitle(),
      '!nid' => $alternative->getId(),
    )), 'ok');
  }
}

/**
 * Filter by restoration projects.
 */
function filter_by_restoration($row) {
  // Get data from the persistent variable..
  return ($row[4] == 'restoration') ? $row : FALSE;
}
