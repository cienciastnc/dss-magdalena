<?php
/**
 * @file
 * Populates baseline hydropower alternatives to a particular Case Study.
 *
 * This scripts populates all baseline hydro power plants and ROR plants into
 * a hygropower alternative for the case study selected.
 *
 * The Case Study ID for which we will populate all baseline has to be inserted
 * in the variable $cid.
 */

use Drupal\dss_magdalena\DSS\Entity\SimaResource;
use Drupal\dss_magdalena\DSS\Entity\SimaCaseStudy;
use Drupal\dss_magdalena\DSS\Entity\Ficha\SimaFichaHydropowerPlantDam;
use Drupal\dss_magdalena\DSS\Entity\Ficha\SimaFichaRorHydropowerPlant;
use Drupal\dss_magdalena\DSS\Entity\SimaAlternative;
use Drupal\dss_magdalena\DSS\SimaWeapIndex;

// Select Case Study ID.
$cid = 350;

/**
 * Populates the Power Plants.
 *
 * @param int $cid
 *   The Case Study ID.
 */
function dss_populate_baseline_power_plants($cid) {

  if ($case = SimaCaseStudy::load($cid)) {

    // Loading hydropower alternatives.
    $alternatives = $case->getHydropowerAlternatives();

    $default_case = SimaCaseStudy::loadDefault();
    $baseline_hres = SimaResource::loadByCaseStudyAndDataset($default_case->getId(), 'dss_existencia_hres');
    $baseline_hror = SimaResource::loadByCaseStudyAndDataset($default_case->getId(), 'dss_existencia_hror');

    $weap_index = SimaWeapIndex::loadAllRecords();
    $fields = [
      'branch',
      'level1',
      'level2',
      'level3',
      'level4',
    ];

    $hres = $baseline_hres->getDataCsv();
    $hres = $weap_index->combineWithWeap($hres, $fields);
    $hror = $baseline_hror->getDataCsv();
    $hror = $weap_index->combineWithWeap($hror, $fields);

    $reservoir_ids = [];
    foreach ($hres as $project) {
      list($res, $name) = explode('\\', $project['level4']);
      $active = (bool) intval($project['Value']);
      if (!$active) {
        // If project is not active, go to next one.
        continue;
      }
      if ($reservoir = SimaFichaHydropowerPlantDam::loadByTitle($name, SimaFichaHydropowerPlantDam::BUNDLE)) {
        $reservoir_ids[] = $reservoir->getId();
      }
    }
    $alternatives->set(SimaAlternative::HYDROPOWER_PROJECTS, array_unique($reservoir_ids));

    $ror_ids = [];
    foreach ($hror as $project) {
      list($res, $name) = explode('\\', $project['level4']);
      $active = (bool) intval($project['Value']);
      if (!$active) {
        // If project is not active, go to next one.
        continue;
      }
      if ($ror = SimaFichaRorHydropowerPlant::loadByTitle($name, SimaFichaRorHydropowerPlant::BUNDLE)) {
        $ror_ids[] = $ror->getId();
      }
    }
    $alternatives->set(SimaAlternative::ROR_HYDROPOWER_PROJECTS, array_unique($ror_ids));
    $alternatives->save();
    drush_print('Saved Hydropower alternative for Case Study ' . $case->getTitle());
  }
}
