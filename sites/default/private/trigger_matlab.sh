#!/bin/bash
# TRIGGER MATLAB
# Get the control file of execution copy to ROOT directory & trigger
# MATLAB script. Get outputs & trigger import CSV rutine

# Set path for execute MATLAB script
PATH=$PATH:$HOME/bin
export PATH

# Path of input file created
filepath=$1

privatePath=$2

matlabPath=$3

apiUrl=http://localhost:9009/api/fishermanApp/

apiUser=adminTNC

apiPassword=tncADMIN.2020

# Name of control file inside input folder
controlFile="Control_File_SIMA-execution.txt"

# Name of Output folder
outputFile="Outputs"

# Validate if control file has been generated
if echo "$filepath" | grep -q "$controlFile"; then

    controlFileName=Control_File_SIMA.txt

    matlabExec=Ejecutable_Tier1_MATLAB/

    matlabRuntime=MATLAB_Runtime/v97/

    # Copy file control inside of execution and copy to ROOT directory for MATLAB script
    cp $filepath $privatePath$controlFileName

    # Change to directory where is placed the MATLAB script
    cd $matlabPath$matlabExec
    echo "*------------------------------------*"
    echo "Inicia ejecución de MATLAB"
    echo "Ruta del ejecutable MATLAB: $matlabPath$matlabExec"
    echo "Ruta del runtime MATLAB: $matlabPath$matlabRuntime"
    echo "*------------------------------------*"
    # Execute MATLAB script
    sh run_Adapter_SIMA_Tier1.sh $matlabPath$matlabRuntime
fi

# Validate if outputs directory has been created
if echo "$filepath" | grep -q "$outputFile"; then
    echo "*---------------------------------------*"
    echo "Inicia creación de las salidas por MATLAB"
    echo "*---------------------------------------*"
    # Change to ROOT directory
    cd $privatePath

    # Obtain most recent modified files (outputs obtained of MATLAB script)
    recentFiles="$(find $1 -type f -exec stat --format '%Y :%y %n' "{}" \; | sort -nr | cut -d: -f4 | head -1)"

    # Fill array with the path splited by "/"
    declare -a pathArray=($(echo $recentFiles | sed 's/\// /g'))

    # Number of part of the path
    countPath="$(echo ${#pathArray[@]})"

    # Total length minus 1 for error file
    countErrorFile=$(($countPath - 1))

    # Most recent modified file (must be Errors.txt)
    file="$(echo $recentFiles | cut -d '/' -f $countErrorFile)"

    if echo "$file" | grep -q "Errors.txt"; then

        echo "*---------------------------------------*"
        echo "Se han obtenido resultados de la ejecución:"

        #Total minus 3 for user ID path
        userCount=$(($countPath - 4))

        executionCount=$(($countPath - 2))

        # Filter the userId form path obtaind above
        userId="$(echo $recentFiles | cut -d '/' -f $userCount)"

        # Filter the executionId form path obtaind above
        executionIdPath="$(echo $recentFiles | cut -d '/' -f $executionCount)"

        executionId="$(echo $executionIdPath | cut -d '-' -f 2)"

        # Complete user path
        userPath="$userId/Outputs/"

        # Execution path
        executionPath="$executionIdPath/"

        # Full execution path
        definitivePath="${privatePath}${userPath}${executionPath}"

        # Find & count how many narratives folders are in the output
        narrativeCount="$(find $definitivePath -type d -name \*_Narrative_\* -exec echo x \; | wc -l)"

        echo "Id de usuario: $userId"
        echo "Archivos modificados recientemente : $recentFiles"
        echo "Directorio de la ejecución: $executionIdPath"
        echo "ID de la ejecución: $executionId"
        echo "Ruta definitiva de la ejecución: $definitivePath"
        echo "Cantidad de narrativas: $narrativeCount"
        echo "*---------------------------------------*"
        echo "Inicia rutina de importación de las salidas a SIMA"
        sudo php import_outputs.php --user="${userId}" --execution="${executionId}" --narratives="${narrativeCount}" --path="${privatePath}" --apiUrl="${apiUrl}" --apiUser="${apiUser}" --apiPassword="${apiPassword}"
    fi
fi
