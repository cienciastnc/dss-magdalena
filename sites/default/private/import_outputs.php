<?php

/**
 * @file
 * Import execution's outputs to SIMA like
 * datasets & linked to the execution
 */

// Input parameter from execution sh script: user
$user = getopt(null, ["user:"]);
$userId = $user["user"];

// Input parameter from execution sh script: execution
$execution = getopt(null, ["execution:"]);
$executionId = $execution["execution"];

// Input parameter from execution sh script: num of narratives
$narratives = getopt(null, ["narratives:"]);
$narrativeCount = $narratives['narratives'];

// ROOT path private configured in Drupal
$path = getopt(null, ["path:"]);
$rootPath = $path['path'];

// Full URL for SIMA API
$api = getopt(null, ["apiUrl:"]);
$apiUrl = $api['apiUrl'];

// API username
$username = getopt(null, ["apiUser:"]);
$apiUsername = $username['apiUser'];

// API password
$password = getopt(null, ["apiPassword:"]);
$apiPassword = $password['apiPassword'];

// Split by parts of URL
$apiUrlSplit = parse_url($apiUrl);

// Host
$hostUrl = $apiUrlSplit['host'];

//Sheme (protocol)
$shemeUrl = $apiUrlSplit['scheme'];
$shemeUrl .= "://";

//Rest of path URL
$pathUrl = $apiUrlSplit['path'];

// Attach resource request URL
$attachRequestUrl = $shemeUrl . $hostUrl . $pathUrl . "node/";

// Create Node URL
$createNodeUrl = $shemeUrl . $hostUrl . $pathUrl . "entity_node/";

// Edit node URL
$editNodeUrl = $shemeUrl . $hostUrl . $pathUrl . "node/";

// Login API URL
$login_url = $shemeUrl . $hostUrl . $pathUrl . "user/login";
// Login & get token & cookie
$session_ops = getToken($login_url, $apiUsername, $apiPassword);
$cookie_session = $session_ops['cookie'];
$token = $session_ops['token'];
// Import execution's outputs to SIMA
importOutputs($cookie_session, $token, $userId, $executionId, $narrativeCount, $rootPath, $attachRequestUrl, $createNodeUrl, $editNodeUrl);

/*
 * Attach outputs CSV to a existing resource
 *
 * @param String $request       url for authentication.
 * @param String $attach_url    url path for CSV file
 * @param String $cookie        valid cookie for login
 * @param String $token         valid token for login
 *
 */
function attachFileToResource($request, $attach_url, $cookie, $token)
{
    echo "Request :: " . $request;
    echo "Atach URL:: " . $attach_url;
    // Setup file data.
    $file_data = array(
        'files[1]' => curl_file_create($attach_url),
        'field_name' => 'field_upload',
        'attach' => 1,
    );

    // Set up request.
    $curl = curl_init($request);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: multipart/form-data', 'Accept: application/json', 'X-CSRF-Token: ' . $token));
    curl_setopt($curl, CURLOPT_POST, 1); // Do a regular HTTP POST.
    curl_setopt($curl, CURLOPT_POSTFIELDS, $file_data); // Set POST data.
    curl_setopt($curl, CURLOPT_COOKIE, $cookie);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_FAILONERROR, true);

    // Execute request and get response.
    $response = curl_exec($curl);
    if (curl_errno($curl)) {
        echo $error_msg = curl_error($curl);
    }
}
/*
 * Create a dataset & assign it resources
 * previoulsy created in the workflow
 *
 * @param String $url               url for authentication.
 * @param String $cookie            valid cookie for login
 * @param String $token             valid token for login
 * @param String $resources         List of the resources
 * @param String $executionId       Id of the execution
 *
 * @return String $datasetNid       Nid of dataset created
 */
function createDataset($url, $cookie, $token, $resources, $executionId)
{
    $dataset_data = array(
        'type' => 'dataset',
        'title' => 'Salidas de la Ejecución' . $executionId,
        'status' => 1,
        'body[und][0][value]' => 'Salidas de los modelos para la ejecución ' . $executionId,
        'field_machine_name[und][0][value]' => 'salida_ejecucion_' . $executionId,
        'field_type[und][0][tid]' => '29',
        'field_format[und][0][tid]' => '1', //
        'field_author[und][0][value]' => $userId,
    );
    foreach ($resources as $key => $resource) {
        $dataset_data["field_resources[und][$key][target_id]"] = $resource;
    }
    $dataset_data = http_build_query($dataset_data);

    // Set up request.
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Accept: application/json', 'X-CSRF-Token: ' . $token));
    curl_setopt($curl, CURLOPT_POST, 1); // Do a regular HTTP POST.
    curl_setopt($curl, CURLOPT_POSTFIELDS, $dataset_data); // Set POST data.
    curl_setopt($curl, CURLOPT_COOKIE, $cookie);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_FAILONERROR, true);

    // Execute request and get response.
    $response = curl_exec($curl);
    echo "Response ::::" . $response . "\n\n";
    $responseJSON = json_decode($response);
    $datasetNid = $responseJSON->nid;

    if (curl_errno($curl)) {
        echo $error_msg = curl_error($curl);
    } // Create resource
    echo "Response dataset: " . $response . "\n\n";
    return $datasetNid;
}
/*
 * Create a resource with execution
 * outputs
 *
 * @param String $url       url for authentication.
 * @param String $cookie    valid cookie for login
 * @param String $token     valid token for login
 * @param Array  $metadata  name & description of resource
 *
 * @return String $resourceNid Node id of resource
 */
function createResource($url, $cookie, $token, $metadata)
{
    // Setup resource data.
    // A great explanation on how to target each node field can be found on the 'Identifying field names' article linked on the 'Documentation' section.
    $resource_data = array(
        'type' => 'resource',
        'title' => $metadata['name'],
        'status' => 1,
        'field_full_name[und][0][value]' => $metadata['fullname'], // preview
        'field_format[und][0][tid]' => '1', // preview
        'field_processing_type[und][0][tid]' => '29', // preview
        'body[und][0][value]' => $metadata['description'],
        'body[und][0][summary]' => '',
        'body[und][0][format]' => 'full_html',
        'body[und][0][safe_value]' => $metadata['description'],
        'body[und][0][safe_summary]' => '',
    );
    $resource_data = http_build_query($resource_data);
    // Setup request.
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Accept: application/json', 'X-CSRF-Token: ' . $token));
    curl_setopt($curl, CURLOPT_POST, 1); // Do a regular HTTP POST.
    curl_setopt($curl, CURLOPT_POSTFIELDS, $resource_data); // Set POST data.
    curl_setopt($curl, CURLOPT_COOKIE, $cookie);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_FAILONERROR, true);

    // Execute request and get response.
    $response = curl_exec($curl);
    $responseJSON = json_decode($response);
    $resourceNid = $responseJSON->nid;

    if (curl_errno($curl)) {
        echo $error_msg = curl_error($curl);
    }
    $resourceNid = $responseJSON->nid;
    return $resourceNid;
}

/*
 * Attach dataset created to correspond
 * execution by the ID
 *
 * @param String $url               url for authentication.
 * @param String $cookie            valid cookie for login
 * @param String $token             valid token for login
 * @param String $resources         List of the resources
 *
 */
function attachDatasetToExecution($url, $cookie, $token, $datasetId)
{
    echo "Id dataset: " . $datasetId . "\n\n";
    $dataset_data = array(
        "field_salidas[und]" => $datasetId,
    );

    $dataset_data = http_build_query($dataset_data);
    //$data = http_build_query($data);
    // Set up request.
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Accept: application/json', 'X-CSRF-Token: ' . $token));
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($curl, CURLOPT_POSTFIELDS, $dataset_data); // Set POST data.
    curl_setopt($curl, CURLOPT_COOKIE, $cookie);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_FAILONERROR, true);

    // Execute request and get response.
    $response = curl_exec($curl);
    if (curl_errno($curl)) {
        echo $error_msg = curl_error($curl);
    } // Create resource
    echo "Response attach dataset: " . $response . "\n\n";
}

/*
 * Login session in API & get token
 * session for authentication
 *
 * @param String $url url for authentication.
 *
 * @return Array $session_ops cookie & token authentication
 */
function getToken($url, $username, $password)
{
    $user_data = array(
        'username' => $username,
        'password' => $password,
    );
    $user_data = http_build_query($user_data);

    // Setup request.
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Accept: application/json'));

    // Accept JSON response.
    curl_setopt($curl, CURLOPT_POST, 1); // Do a regular HTTP POST.
    curl_setopt($curl, CURLOPT_POSTFIELDS, $user_data); // Set POST data.
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_FAILONERROR, true);

    // Execute request and get response.
    $response = curl_exec($curl);
    if (curl_errno($curl)) {
        //echo $error_msg = curl_error($curl);
    }
    // Process response.
    $logged_user = json_decode($response);

    // Save cookie session to be used on future requests.
    $cookie_session = $logged_user->session_name . '=' . $logged_user->sessid;
    $token = $logged_user->token;
    $session_ops = array("cookie" => $cookie_session, "token" => $token);
    //echo "Response token: " . $response;
    return $session_ops;
}

/*
 * Search number of narratives & prepare
 * the outputs based in models selected
 * in the execution
 *
 * @param Object $execution execution.
 */
function importOutputs($cookie_session, $token, $userId, $executionId, $narrativeCount, $rootPath, $attachRequestUrl, $createNodeUrl, $editNodeUrl)
{
    $resourcesCreated = array();
    $fragmentation = false;
    $footprint = false;
    echo "Cantidad de narrativas :: " . $narrativeCount . "\n\n";
    // BaseLine directory inside of execution's folder
    $baseLineDirectory = "$rootPath$userId/Outputs/execution-$executionId/BaseLine";
    $narrativesFiles = array();
    $errorPath = "$rootPath$userId/Outputs/execution-$executionId/*.txt";
    $errorFiles = glob($errorPath);
    foreach ($errorFiles as $j => $file) {
        $currentFile = explode("$executionId/", $file);
        echo "Archivo split ::" . $currentFile[1];
        $fileObject = array("file" => $currentFile[1], "isFragm" => false, "isFootprint" => false, "isError" => true);
        echo "Error Object";
        print_r($fileObject);
        array_push($narrativesFiles, $fileObject);
    }
    // BaseLine if narrativeDirectory exists
    if (is_dir($baseLineDirectory)) {
        $folderType = 1; //0: narrative/1: baseline;
        $outputFiles = array();
        $iterator = new FilesystemIterator($baseLineDirectory, FilesystemIterator::SKIP_DOTS);
        $filesCount = iterator_count($iterator);
        $outputFiles = checkExecutionModels($userId, $executionId, $rootPath);
        $baseLinePath = "$rootPath$userId/Outputs/execution-$executionId/BaseLine/*.csv";
        $fragmentationDirectory = "$rootPath$userId/Outputs/execution-$executionId/BaseLine/Fragmentation";
        $footprintDirectory = "$rootPath$userId/Outputs/execution-$executionId/BaseLine/Footprints";
        // Outputs files existing in BaseLine folder
        $files = glob($baseLinePath);
        echo "Directorio base line: " . $baseLineDirectory . "\n\n";
        echo " Archivo: base " . $files[0] . "\n\n";

        if (is_dir($fragmentationDirectory)) {
            $fragmentation = true;
            $fragmentationPath = "$rootPath$userId/Outputs/execution-$executionId/BaseLine/Fragmentation/*.csv";
            $fragmentationFiles = glob($fragmentationPath);
            foreach ($fragmentationFiles as $j => $file) {
                $currentFile = explode("Fragmentation/", $file);
                echo "Archivo actual :: " . $currentFile[1];
                $file = array("file" => $currentFile[1], "isFragm" => true, "isFootprint" => false);
                array_push($narrativesFiles, $file);
            }
        }
        if (is_dir($footprintDirectory)) {
            $footprint = true;
            $footprintPathCsv = "$rootPath$userId/Outputs/execution-$executionId/BaseLine/Footprints/*.csv";
            $footprintPathTiff = "$rootPath$userId/Outputs/execution-$executionId/BaseLine/Footprints/*.tiff";
            $footprintCsv = glob($footprintPathCsv);
            $footprintTiff = glob($footprintPathTiff);
            foreach ($footprintCsv as $j => $file) {
                $currentFile = explode("Footprints/", $file);
                echo "Archivo actual :: " . $currentFile[1];
                $file = array("file" => $currentFile[1], "isFootprint" => true, "isFragm" => false);
                array_push($narrativesFiles, $file);
            }
            foreach ($footprintTiff as $j => $file) {
                $currentFile = explode("Footprints/", $file);
                echo "Archivo actual :: " . $currentFile[1];
                $file = array("file" => $currentFile[1], "isFootprint" => true, "isFragm" => false);
                array_push($narrativesFiles, $file);
            }
        }
        foreach ($files as $j => $file) {
            echo "Archivo Base Line: " . $file . "\n\n";
            $currentFile = explode("BaseLine/", $file);
            $file = array("file" => $currentFile[1], "isFragm" => false, "isFootprint" => false);
            echo "Current file: " . $currentFile[1] . "\n\n";
            array_push($narrativesFiles, $file);
        }

        $narrativeOutputs = $narrativesFiles;

        /*
         * Create many resources as outputs
         * exists for narrative's execution
         */
        foreach ($narrativeOutputs as $j => $output) {
            // Create resource
            $resource_url = $createNodeUrl;
            $narrativeName = "";
            $narrativeId = "";
            $isBaseLine = true;
            $metadata = getOutputMetadata($output['file'], $folderType, $fragmentation, $footprint, $narrativeName, $narrativeId);
            print_r($metadata);
            $resourceNid = createResource($resource_url, $cookie_session, $token, $metadata);
            echo "Reource NID ::" . $resourceNid . "\n\n";
            array_push($resourcesCreated, $resourceNid);
            echo "Resource BaseLine NID: " . $resourceNid . "\n\n";
            if ($output['isFragm']) {
                echo "Fragmentation Directory BaseLine" . $fragmentationDirectory;
                echo "isFragmentation BaseLine";
                print_r($output);
                $attach_url = $fragmentationDirectory . "/" . $output['file'];
            } elseif ($output['isFootprint']) {

                $attach_url = $footprintDirectory . "/" . $output['file'];
                echo "Atach resource URL Huella:::" . $attach_url;
            } else {
                if ($output['isError']) {
                    echo "entro archivo de error";
                    $attach_url = "$rootPath$userId" . "/Outputs/execution-$executionId/" . $output['file'];
                } else {
                    $attach_url = $baseLineDirectory . "/" . $output['file'];
                    echo "Atach resource URL:::" . $attach_url;
                }
            }
            $request = $attachRequestUrl . $resourceNid . '/attach_file';
            attachFileToResource($request, $attach_url, $cookie_session, $token);
        } //END foreach
    } //END IF is dir
    $narrativePath = "$rootPath$userId/Outputs/execution-$executionId/*";
    $foldersNarrative = [];
    foreach (glob($narrativePath, GLOB_ONLYDIR) as $dir) {

        $dirname = basename($dir);
        if (strpos($dirname, "Narrative") !== false) {
            $name = substr($dirname, 15);
            $narrative = array("narrativeName" => $name, "narrativeFolder" => $dirname);
            array_push($foldersNarrative, $narrative);
        }
    }
    // Loop many times as narratives exists
    for ($i = 0; $i <= sizeof($foldersNarrative) - 1; $i++) {
        // Narrative directory inside execution folder
        $folderDirectory = $foldersNarrative[$i]['narrativeFolder'];
        $narrativeDirectory = "$rootPath$userId/Outputs/execution-$executionId/$folderDirectory";
        $fragmentationDirectory = "$rootPath$userId/Outputs/execution-$executionId/$folderDirectory/Fragmentation";
        $footprintDirectory = "$rootPath$userId/Outputs/execution-$executionId/$folderDirectory/Footprints";
        // Validate if narrativeDirectory exists
        if (is_dir($narrativeDirectory)) {
            //0: narrative/1: baseline;

            $folderType = 0;
            echo "Tipo de directorio:: " . $folderType . ":: //0 narrativa 1 linea base \n\n";
            echo "Directorio narrativa" . $narrativeDirectory . "\n\n";

            $outputFiles = array();
            $iterator = new FilesystemIterator($narrativeDirectory, FilesystemIterator::SKIP_DOTS);
            $filesCount = iterator_count($iterator);
            $outputFiles = checkExecutionModels($userId, $executionId, $rootPath);
            $narrativePath = "$rootPath$userId/Outputs/execution-$executionId/$folderDirectory/*.csv";

            // Outputs files existing in narrative
            $files = glob($narrativePath);
            $narrativesFiles = array();

            if (is_dir($fragmentationDirectory)) {
                $fragmentation = true;
                $fragmentationPath = "$rootPath$userId/Outputs/execution-$executionId/$folderDirectory/Fragmentation/*.csv";
                $fragmentationDirectory = "$rootPath$userId/Outputs/execution-$executionId/$folderDirectory/Fragmentation";
                $fragmentationFiles = glob($fragmentationPath);
                foreach ($fragmentationFiles as $j => $file) {
                    $currentFile = explode("Fragmentation/", $file);
                    echo "Archivo actual :: " . $currentFile[1];
                    $file = array("file" => $currentFile[1], "isFragm" => true, "isFootprint" => false);
                    array_push($narrativesFiles, $file);
                }
            }
            if (is_dir($footprintDirectory)) {
                $footprint = true;
                $footprintPathCsv = "$rootPath$userId/Outputs/execution-$executionId/$folderDirectory/Footprints/*.csv";
                $footprintPathTiff = "$rootPath$userId/Outputs/execution-$executionId/$folderDirectory/Footprints/*.tiff";
                $footprintDirectory = "$rootPath$userId/Outputs/execution-$executionId/$folderDirectory/Footprints";
                $footprintFiles = glob($footprintPathCsv);
                $footprintTiff = glob($footprintPathTiff);
                foreach ($footprintFiles as $j => $file) {
                    $currentFile = explode("Footprints/", $file);
                    echo "Archivo actual :: " . $currentFile[1];
                    $file = array("file" => $currentFile[1], "isFootprint" => true, "isFragm" => false);
                    array_push($narrativesFiles, $file);
                    print_r($narrativesFiles);
                }
                foreach ($footprintTiff as $j => $file) {
                    $currentFile = explode("Footprints/", $file);
                    echo "Archivo actual :: " . $currentFile[1];
                    $file = array("file" => $currentFile[1], "isFootprint" => true, "isFragm" => false);
                    array_push($narrativesFiles, $file);
                }
            }
            foreach ($files as $j => $file) {
                $currentFile = explode("$folderDirectory/", $file);
                $file = array("file" => $currentFile[1], "isFragm" => false, "isFootprint" => false);
                array_push($narrativesFiles, $file);
            }
            $narrativeOutputs = $narrativesFiles;
            print_r($narrativeOutputs);
            foreach ($narrativeOutputs as $j => $output) {
                // Create resource
                $narrativeName = $foldersNarrative[$i]['narrativeName'];
                $narrativeId = $folderDirectory;
                $resource_url = $createNodeUrl;
                $metadata = getOutputMetadata($output['file'], $folderType, $fragmentation, $footprint, $narrativeName, $narrativeId);
                $resourceNid = createResource($resource_url, $cookie_session, $token, $metadata);
                echo "Resource creado: " . $resourceNid . "\n\n";
                array_push($resourcesCreated, $resourceNid);
                echo "Recursos creados Array:: \n\n";
                print_r($resourcesCreated);
                echo "Resource narrativa NID: " . $resourceNid . "\n\n";
                if ($output['isFragm']) {
                    echo "Fragmentation Directory";
                    print_r($fragmentationDirectory);
                    echo "isFragmentation";
                    print_r($output);
                    $attach_url = $fragmentationDirectory . "/" . $output['file'];
                } elseif ($output['isFootprint']) {
                    print_r($output);

                    $attach_url = $footprintDirectory . "/" . $output['file'];
                    echo "Atach resource URL Huella:::" . $attach_url;
                } else {
                    $attach_url = $narrativeDirectory . "/" . $output['file'];
                    echo "Atach resource URL:::" . $attach_url;
                    echo "Archivo que no es huella, ni fragmentación" . $output['file'];
                }
                $request = $attachRequestUrl . $resourceNid . '/attach_file';
                attachFileToResource($request, $attach_url, $cookie_session, $token);
            }
        }
    } //END FOR
    $dataset_url = $createNodeUrl;
    $datasetId = createDataset($dataset_url, $cookie_session, $token, $resourcesCreated, $executionId);
    $request = $editNodeUrl . $executionId;
    echo $request;
    attachDatasetToExecution($request, $cookie_session, $token, $datasetId);
}

/*
 * Check the control file of execution, get the analysis
 * code & get outputs file based on it
 *
 * @param String $userId        ID of user who runs the execution
 * @param String $executionId   ID of execution sent by user
 * @param String $rootPath      ROOT path of SIMA execution's files
 *
 * @return Array $outputFiles   List of outputs required for the model
 */
function checkExecutionModels($userId, $executionId, $rootPath)
{
    // Path for execution's control file
    $controlFile = "$rootPath$userId/Inputs/execution-$executionId/Control_File_SIMA-execution.txt";

    $fileHandle = fopen($controlFile, 'r');
    $outputFiles = array();
    if ($fileHandle) {
        // Line counter
        $lineCount = 0;

        // Loop trought file's lines
        while (!feof($fileHandle)) {
            // Current line reading
            $line = fgets($fileHandle);

            /*
             * Line for analysis code in control file
             * is 10, when $lineCount is 10 the line
             * will be equal to analysis code
             */
            if ($lineCount == 10) {
                // Split line by white space
                $analysis = explode(" ", $line);

                // Analysys code
                $code = $analysis[1];

                /*
                 * Validate analysys code, depending of
                 * number obtain the model & the
                 * corresponding output files
                 */
                switch ($code) {
                    case 1: //DOR - DORw
                        $outputFiles = array("InstallPower.csv", "0.csv", "AleatoryDefinition.csv", "DOR.csv", "DORw.csv", "Errors.txt", "StatusError.txt");
                        break;
                    case 2: //SAI
                        $outputFiles = array("InstallPower.csv", "0.csv", "AleatoryDefinition.csv", "SAI.csv", "Errors.txt", "StatusError.txt");
                        break;
                    case 3: //DOR -DORw+SAI
                        $outputFiles = array("InstallPower.csv", "0.csv", "DOR.csv", "DORw.csv", "AleatoryDefinition.csv", "SAI.csv", "Errors.txt", "StatusError.txt");
                        break;
                    case 4: //Fragmentation
                        $outputFiles = array("InstallPower.csv", "0.csv", "AleatoryDefinition.csv", "FuncNetwork.csv", "Errors.txt", "StatusError.txt");
                        break;
                    case 5: //Fragmentation+DOR -DORw
                        $outputFiles = array("InstallPower.csv", "0.csv", "DOR.csv", "DORw.csv", "AleatoryDefinition.csv", "FuncNetwork.csv", "Errors.txt", "StatusError.txt");
                        break;
                    case 6: //Fragmentation+SAI
                        $outputFiles = array("InstallPower.csv", "0.csv", "AleatoryDefinition.csv", "FuncNetwork.csv", "Errors.txt", "StatusError.txt");
                        break;
                    case 7: //Fragmentation + DOR + DORw + SAI
                        $outputFiles = array("InstallPower.csv", "0.csv", "AleatoryDefinition.csv", "DOR.csv", "DORw.csv", "FuncNetwork.csv", "Errors.txt", "StatusError.txt");
                        break;
                    default:
                        # code...
                        break;
                }
            }
            $lineCount++;
        }
        fclose($fileHandle);
    }
    return $outputFiles;
}

/*
 * Check the output files generated & set the
 * name & description to use in resource creation
 *
 * @param String $userId        ID of user who runs the execution
 * @param String $executionId   ID of execution sent by user
 * @param String $rootPath      ROOT path of SIMA execution's files
 *
 * @return Array $data          Name & description of output file
 */
function getOutputMetadata($outputFile, $folderType, $fragmentation, $footprint, $narrativeName, $narrativeId)
{
    switch ($outputFile) {
        case 'Errors.txt':
            $data = array("name" => "Bitácora de errores de la ejecución", "description" => "Archivo que contiene los mensajes y estatus de error de la ejecución arrojados por del modelo de MATLAB", "fullname" => "Errors");
            break;
        case 'StatusError.txt':
            $data = array("name" => "Bitácora de errores de proyectos", "description" => "Archivo que contiene los mensajes y estatus de error de los proyectos que son tomados en cuenta en la ejecución del modelo por MATLAB", "fullname" => "Status_Error");
            break;
    }

    // Narratives directory
    if ($folderType == 0) {
        // Exists framentation directory
        if ($fragmentation) {
            switch ($outputFile) {
                case 'DOR.csv':
                    $data = array("name" => "DOR - Narrativa $narrativeName", "description" => "Grado de regulación calculado considerando los proyectos aleatorizados incluidos dentro de la narrativa definida. Los proyectos indicados como línea base también se incluyen de manera fija dentro del análisis (Unidad: %). El índice DOR es la relación entre el volumen de almacenamiento disponible aguas arriba y el volumen de escorrentía media anual en un tramo del río. Por lo tanto, corresponde al porcentaje de la oferta hídrica anual que es retenida en embalses localizados aguas arriba del tramo analizado (Lehner et al., 2011)", "fullname" => "DOR_$narrativeId");
                    break;
                case 'DORw.csv':
                    $data = array("name" => "DORw - Narrativa $narrativeName", "description" => "Grado de regulación ponderado calculado considerando los proyectos aleatorizados incluidos dentro de la narrativa definida. Los proyectos indicados como línea base también se incluyen de manera fija dentro del análisis (Unidad: %). El índice DORw se define como la relación entre el volumen de almacenamiento disponible aguas arriba y el volumen de escorrentía media anual en un tramo del río, ponderado por el porcentaje de escorrentía efectivamente controlada aguas arriba por almacenamiento artificial.", "fullname" => "DORw_$narrativeId");
                    break;
                case 'AleatoryDefinition.csv':
                    $data = array("name" => "Aleatory Definition - Narrativa $narrativeName", "description" => "Matriz que identifica los proyectos activos en la red hidrológica que fueron aleatorizados de acuerdo con la narrativa definida. Los proyectos indicados como línea base también se incluyen de manera fija dentro del análisis (Unidad: N/A). Un proyecto activo se identifica con (1) y un proyecto inactivo con (0).", "fullname" => "Aleatory_Definition_$narrativeId");
                    break;
                case 'InstallPower.csv':
                    $data = array("name" => "Install Power - Narrativa $narrativeName", "description" => "Potencia instalada en la macrocuenca calculado considerando los proyectos aleatorizados incluidos dentro de la narrativa definida. Los proyectos indicados como línea base también se incluyen de manera fija dentro del análisis (Unidad: MW). Corresponde a la sumatoria de las potencias instaladas de cada uno de los proyectos indicados como activos en la aleatorización de acuerdo con la narrativa definida.", "fullname" => "Install_Power_$narrativeId");
                    break;
                case 'FuncNetwork.csv':
                    $data = array("name" => "Fragmentation - Narrativa $narrativeName", "description" => "Resultado de fragmentación de la red fluvial generada por proyectos hidroeléctricos (embalses) considerando los proyectos aleatorizados incluidos dentro de la narrativa definida. Los proyectos indicados como línea base también se incluyen de manera fija dentro del análisis (Unidad: N/A). Cada tramo de río se identifica con un número correspondiente al proyecto que genera la fragmentación (ID proyecto).", "fullname" => "Func_Network_$narrativeId");
                    break;
                case 'SAI.csv':
                    $data = array("name" => "SAI - Narrativa $narrativeName", "description" => "Índice de alteración de sedimentos (SAI por sus siglas en inglés) calculado para los proyectos definidos como parte del periodo histórico y de línea base (Unidad: %). El SAI establece  la relación entre la carga media de sedimentos suspendidos en la ejecución considerando la retención acumulada aguas arriba y la carga media de sedimentos suspendidos en una ejecución de referencia (ejecución sin embalses", "fullname" => "SAI_$narrativeId");
                    break;
                case '0.csv':
                    $data = array("name" => "Arco 0 - Narrativa $narrativeName", "description" => "Índice de alteración de sedimentos (SAI por sus siglas en inglés) calculado para los proyectos definidos como parte del periodo histórico y de línea base (Unidad: %). El SAI establece  la relación entre la carga media de sedimentos suspendidos en la ejecución considerando la retención acumulada aguas arriba y la carga media de sedimentos suspendidos en una ejecución de referencia (ejecución sin embalses", "fullname" => "Arc_0_" . $narrativeId);
                    break;
            }
        }
        // Exists Footprints Directory
        if ($footprint) {
            switch ($outputFile) {

                case 'Area_Volumen.csv':
                    $data = array("name" => "Area Volumen - Narrativa $narrativeName", "description" => "Área y volumen de los embalses considerando los proyectos de análisis (Unidad: m2 / m3).", "fullname" => "Area_Volumen_$narrativeId");
                    break;
                case 'Footprints.csv':
                    $data = array("name" => "Footprints - Narrativa $narrativeName", "description" => "Impactos de huella calculados para los proyectos de análisis (Unidad: En función de unidades del raster).", "fullname" => "Footprints_$narrativeId");
                    break;
                case 'Watermirror.tiff':
                    $data = array("name" => "Water Mirror - Narrativa $narrativeName", "description" => "Archivo raster con el área inundada por el embalse", "fullname" => "Watermirror_$narrativeId");
                    break;
                default:
                    break;
            }
        }
        switch ($outputFile) {
            case 'DOR.csv':
                $data = array("name" => "DOR - Narrativa $narrativeName", "description" => "Grado de regulación calculado considerando los proyectos aleatorizados incluidos dentro de la narrativa definida. Los proyectos indicados como línea base también se incluyen de manera fija dentro del análisis (Unidad: %). El índice DOR es la relación entre el volumen de almacenamiento disponible aguas arriba y el volumen de escorrentía media anual en un tramo del río. Por lo tanto, corresponde al porcentaje de la oferta hídrica anual que es retenida en embalses localizados aguas arriba del tramo analizado (Lehner et al., 2011)", "fullname" => "DOR_$narrativeId");
                break;
            case 'DORw.csv':
                $data = array("name" => "DORw - Narrativa $narrativeName", "description" => "Grado de regulación ponderado calculado considerando los proyectos aleatorizados incluidos dentro de la narrativa definida. Los proyectos indicados como línea base también se incluyen de manera fija dentro del análisis (Unidad: %). El índice DORw se define como la relación entre el volumen de almacenamiento disponible aguas arriba y el volumen de escorrentía media anual en un tramo del río, ponderado por el porcentaje de escorrentía efectivamente controlada aguas arriba por almacenamiento artificial.", "fullname" => "DORw_$narrativeId");
                break;
            case 'AleatoryDefinition.csv':
                $data = array("name" => "Aleatory Definition - Narrativa $narrativeName", "description" => "Matriz que identifica los proyectos activos en la red hidrológica que fueron aleatorizados de acuerdo con la narrativa definida. Los proyectos indicados como línea base también se incluyen de manera fija dentro del análisis (Unidad: N/A). Un proyecto activo se identifica con (1) y un proyecto inactivo con (0).", "fullname" => "Aleatory_Definition_$narrativeId");
                break;
            case 'InstallPower.csv':
                $data = array("name" => "Install Power - Narrativa $narrativeName", "description" => "Potencia instalada en la macrocuenca calculado considerando los proyectos aleatorizados incluidos dentro de la narrativa definida. Los proyectos indicados como línea base también se incluyen de manera fija dentro del análisis (Unidad: MW). Corresponde a la sumatoria de las potencias instaladas de cada uno de los proyectos indicados como activos en la aleatorización de acuerdo con la narrativa definida.", "fullname" => "Install_Power_$narrativeId");
                break;
            case 'FuncNetwork.csv':
                $data = array("name" => "Fragmentation - Narrativa $narrativeName", "description" => "Resultado de fragmentación de la red fluvial generada por proyectos hidroeléctricos (embalses) considerando los proyectos aleatorizados incluidos dentro de la narrativa definida. Los proyectos indicados como línea base también se incluyen de manera fija dentro del análisis (Unidad: N/A). Cada tramo de río se identifica con un número correspondiente al proyecto que genera la fragmentación (ID proyecto).", "fullname" => "Func_Network_$narrativeId");
                break;
            case 'SAI.csv':
                $data = array("name" => "SAI - Narrativa $narrativeName", "description" => "Índice de alteración de sedimentos (SAI por sus siglas en inglés) calculado para los proyectos definidos como parte del periodo histórico y de línea base (Unidad: %). El SAI establece  la relación entre la carga media de sedimentos suspendidos en la ejecución considerando la retención acumulada aguas arriba y la carga media de sedimentos suspendidos en una ejecución de referencia (ejecución sin embalses", "fullname" => "SAI_$narrativeId");
                break;
            default:
                # code...
                break;
        }
    } else { //BaseLine directory
        if ($fragmentation) {
            switch ($outputFile) {
                case 'DOR.csv':
                    $data = array("name" => "DOR - Base Line", "description" => "Grado de regulación calculado para los proyectos definidos como parte del periodo histórico y de línea base (Unidad: %). El índice DOR es la relación entre el volumen de almacenamiento disponible aguas arriba y el volumen de escorrentía media anual en un tramo del río. Por lo tanto, corresponde al porcentaje de la oferta hídrica anual que es retenida en embalses localizados aguas arriba del tramo analizado (Lehner et al., 2011)", "fullname" => "DOR_Base_Line");
                    break;
                case 'DORw.csv':
                    $data = array("name" => "DORw - Base Line", "description" => "Contiene los ArcID y las trayectorias para graficar la salida", "fullname" => "DORw_Base_Line");
                    break;
                case 'AleatoryDefinition.csv':
                    $data = array("name" => "Aleatory Definition - Base Line ", "description" => "Matriz que identifica los proyectos activos en la red hidrológica en el periodo histórico y en línea base (Unidad: N/A). Un proyecto activo se identifica con (1) y un proyecto inactivo con (0). ", "fullname" => "Aleatory_Definition_Base_Line");
                    break;
                case 'InstallPower.csv':
                    $data = array("name" => "Install Power - Base Line  ", "description" => "Potencia instalada en la macrocuenca calculado para los proyectos definidos como parte del periodo histórico y de línea base (Unidad: MW). Corresponde a la sumatoria de las potencias instaladas de cada uno de los proyectos indicados como activos en el periodo histórico y en la línea base.", "fullname" => "Install_Power_Base_Line");
                    break;
                case 'FuncNetwork.csv':
                    $data = array("name" => "Fragmentation - Base Line - Narrativa ", "description" => "Resultado de fragmentación de la red fluvial generada por proyectos hidroeléctricos (embalses) considerando los proyectos aleatorizados incluidos dentro de la narrativa definida. Los proyectos indicados como línea base también se incluyen de manera fija dentro del análisis (Unidad: N/A). Cada tramo de río se identifica con un número correspondiente al proyecto que genera la fragmentación (ID proyecto).", "fullname" => "Func_Network_Base_Line");
                    break;
                case 'SAI.csv':
                    $data = array("name" => "SAI - Base Line", "description" => "Índice de alteración de sedimentos (SAI por sus siglas en inglés) calculado para los proyectos definidos como parte del periodo histórico y de línea base (Unidad: %). El SAI establece  la relación entre la carga media de sedimentos suspendidos en la ejecución considerando la retención acumulada aguas arriba y la carga media de sedimentos suspendidos en una ejecución de referencia (ejecución sin embalses).", "fullname" => "SAI_Base_Line");
                    break;
                case '0.csv':
                    $data = array("name" => "Arco 0 - Base Line", "description" => "Índice de alteración de sedimentos (SAI por sus siglas en inglés) calculado para los proyectos definidos como parte del periodo histórico y de línea base (Unidad: %). El SAI establece  la relación entre la carga media de sedimentos suspendidos en la ejecución considerando la retención acumulada aguas arriba y la carga media de sedimentos suspendidos en una ejecución de referencia (ejecución sin embalses", "fullname" => "Arc_0_Base_Line");
                    break;
            }
        }
        // Exists Footprints Directory
        if ($footprint) {
            switch ($outputFile) {
                case 'Area_Volumen.csv':
                    $data = array("name" => "Area Volumen - Base Line", "description" => "Área y volumen de los embalses definidos como parte del periodo histórico y de línea base (Unidad: m2 / m3).", "fullname" => "Area_Volumen_Base_Line");
                    break;
                case 'Footprints.csv':
                    $data = array("name" => "Footprints - Base Line", "description" => "Impactos de huella calculados para los proyectos definidos como parte del periodo histórico y de línea base (Unidad: En función de unidades del raster). ", "fullname" => "Footprints_Base_Line");
                    break;
                case 'Watermirror.tiff':
                    $data = array("name" => "Water Mirror - Base Line", "description" => "Archivo raster con el área inundada por el embalse. Los pixeles indicados con el número del proyecto indican el área inundada. Los pixeles no inundados se identifican con valor 0. ", "fullname" => "Watermirror_Base_Line");
                    break;
                default:
                    break;
            }
        }

        switch ($outputFile) {
            case 'DOR.csv':
                $data = array("name" => "DOR - Base Line", "description" => "Grado de regulación calculado para los proyectos definidos como parte del periodo histórico y de línea base (Unidad: %). El índice DOR es la relación entre el volumen de almacenamiento disponible aguas arriba y el volumen de escorrentía media anual en un tramo del río. Por lo tanto, corresponde al porcentaje de la oferta hídrica anual que es retenida en embalses localizados aguas arriba del tramo analizado (Lehner et al., 2011)", "fullname" => "DOR_Base_Line");
                break;
            case 'DORw.csv':
                $data = array("name" => "DORw - Base Line", "description" => "Grado de regulación ponderado calculado considerando los proyectos aleatorizados incluidos dentro de la narrativa definida. Los proyectos indicados como línea base también se incluyen de manera fija dentro del análisis (Unidad: %). El índice DORw se define como la relación entre el volumen de almacenamiento disponible aguas arriba y el volumen de escorrentía media anual en un tramo del río, ponderado por el porcentaje de escorrentía efectivamente controlada aguas arriba por almacenamiento artificial.", "fullname" => "DORw_Base_Line");
                break;
            case 'AleatoryDefinition.csv':
                $data = array("name" => "Aleatory Definition - Base Line ", "description" => "Matriz que identifica los proyectos activos en la red hidrológica en el periodo histórico y en línea base (Unidad: N/A). Un proyecto activo se identifica con (1) y un proyecto inactivo con (0). ", "fullname" => "Aleatory_Definition_Base_Line");
                break;
            case 'InstallPower.csv':
                $data = array("name" => "Install Power - Base Line  ", "description" => "Potencia instalada en la macrocuenca calculado para los proyectos definidos como parte del periodo histórico y de línea base (Unidad: MW). Corresponde a la sumatoria de las potencias instaladas de cada uno de los proyectos indicados como activos en el periodo histórico y en la línea base.", "fullname" => "Install_Power_Base_Line");
                break;
            case 'FuncNetwork.csv':
                $data = array("name" => "Fragmentation - Base Line - Narrativa ", "description" => "Resultado de fragmentación de la red fluvial generada por proyectos hidroeléctricos (embalses) considerando los proyectos aleatorizados incluidos dentro de la narrativa definida. Los proyectos indicados como línea base también se incluyen de manera fija dentro del análisis (Unidad: N/A). Cada tramo de río se identifica con un número correspondiente al proyecto que genera la fragmentación (ID proyecto).", "fullname" => "Func_Network_Base_Line");
                break;
            case 'SAI.csv':
                $data = array("name" => "SAI - Base Line", "description" => "Índice de alteración de sedimentos (SAI por sus siglas en inglés) calculado para los proyectos definidos como parte del periodo histórico y de línea base (Unidad: %). El SAI establece  la relación entre la carga media de sedimentos suspendidos en la ejecución considerando la retención acumulada aguas arriba y la carga media de sedimentos suspendidos en una ejecución de referencia (ejecución sin embalses).", "fullname" => "SAI_Base_Line");
                break;
            default:
                # code...
                break;
        }
    }
    return $data;
}
