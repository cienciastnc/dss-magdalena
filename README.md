# DSS Magdalena

This distribution contains modules that enables the DSS for Magdalena Basin Project,

The modules are packaged into a distribution in order to simplify their setup,
testing, and development.

## Important Considerations for your PHP Configuration

Make sure you have the following settings in your php.ini:

```
max_execution_time = 300       ; Maximum execution time of each script, in seconds
max_input_time = 600	        ; Maximum amount of time each script may spend parsing request data
memory_limit = 1024M           ; Maximum amount of memory a script may consume (8MB)
max_input_vars = 6000          ; Maximum number of input variables
max_input_nesting_level = 3048 ; Maximum number of nesting levels

; Maximum size of POST data that PHP will accept.
post_max_size = 64M        ; At least 64MB

; Maximum allowed size for uploaded files.
upload_max_filesize = 64M
